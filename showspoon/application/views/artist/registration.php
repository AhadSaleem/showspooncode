<!DOCTYPE html>
<html lang="en">
    <head>

        <?php $this->load->view('includes/head');?>
        <link href="<?php echo base_url();?>/assets/dist/plugins/iCheck/all.css" rel="stylesheet">
        <link href="<?php echo base_url();?>/assets/cropper.css" rel="stylesheet">
        
        <script src="<?php echo base_url();?>/assets/dist/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/plugins/select2/select2.min.js"></script>
        <!--<script src="<?php echo base_url();?>/assets/dist/js/bootstrap_multiselect.js"></script>-->

        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.time.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.date.js"></script>

        <!--<script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/legacy.js"></script>-->
        <style>
            .ht{
                height: 40px !important;
            }

            img {
                  max-width: 100%; /* This rule is very important, please do not ignore this! */
                }
        </style>
    </head>
    <body>

        <?php $this->load->view('includes/header');?>




        <div  class="container">
            <div class="row m-b-md">
                <div class="col-lg-8 col-lg-offset-2 col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">

                    <h3 class="text-center">Complete Your Registration</h3>

                    <form id="form-1" method="post">
                        <ul class="progresssteps m-t-md m-b-md">
                            <li class="active"> </li>
                            <li> </li>
                            <li> </li>
                            <li> </li>
                            <li> </li>
                        </ul> 
                        
                        <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Musician Name <small>*</small></label>
                                        <input type="text" required name="artist_name"  value="<?php echo isset($artist->name)?$artist->name:'';?>" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Name of Contact Person <small>*</small></label>
                                        <input type="text" required name="contact_person"  value="<?php echo isset($artist->contact_person)?$artist->contact_person:'';?>" class="form-control">
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Genre <small>*</small></label>
                                        <div class="custom_select_box">
                                            <select multiple required class="form-control select" name="genre[]">
                                                <?php
                                                $get_genre = isset($artist->genre)?$artist->genre:'';
                                                $get_genre = explode(',',$get_genre);

                                                if(count($genres)>0){
                                                    foreach($genres as $key=>$row){
                                                        $selected='';
                                                        if(in_array($key,$get_genre)){
                                                            $selected='selected';
                                                        }
                                                        echo '<option '.$selected.' value="'.$key.'">'.$row.'</option>';
                                                    }
                                                }

                                                ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Paypal Account Email (For Payment) <small>*</small></label>
                                        <input type="email" required name="payment_account"  value="<?php echo isset($artist->payment_account)?$artist->payment_account:'';?>" class="form-control">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>City </label>
                                        <div class="custom_select_box">
                                            <select data-placeholder=""  required class="form-control select" name="city_id">
                                                <option></option>

                                                <?php

                                                $city = isset($artist->city_id)?$artist->city_id:'';


                                                if(count($cities)>0){
                                                    foreach($cities as $key=>$row){
                                                        $selected='';
                                                        if($city==$key){
                                                            $selected='selected';
                                                        }
                                                        echo '<option '.$selected.' value="'.$key.'">'.$row.'</option>';
                                                    }
                                                }

                                                ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Mobile Number <small>*</small></label>
                                        <input type="text" required name="mobile"  value="<?php echo isset($artist->mobile)?$artist->mobile:'';?>" class="form-control">
                                    </div>
                                </div>



                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Zip Code <small>*</small></label>
                                        <input type="text" required name="zip"  value="<?php echo isset($artist->zip)?$artist->zip:'';?>" class="form-control">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                    <label>Band Type</label>
                                        <div class="custom_select_box">
                                            <select data-placeholder=""  required class="form-control select" name="band_type_id">
                                                <option></option>
                                                <?php

                                                $band = isset($artist->band_type_id)?$artist->band_type_id:'';

                                                if(count($band_types)>0){
                                                    foreach($band_types as $key=>$row){
                                                        $selected='';
                                                        if($band==$key){
                                                            $selected='selected';
                                                        }
                                                        echo '<option '.$selected.' value="'.$key.'">'.$row.'</option>';
                                                    }
                                                }

                                                ?>
                                            </select>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Short Biography <small>*</small></label>
                                        <textarea  required maxlength="250" name="short_biography" class="form-control ht"><?php echo isset($artist->short_description)?$artist->short_description:'';?></textarea>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Biography</label>
                                        <textarea  name="biography" class="form-control ht"><?php echo isset($artist->biography)?$artist->biography:'';?></textarea>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Image</label>
                                <br>
                                <button type="button" class="btn add_pictures" >Image</button>

                            </div>
                            <div class="clearfix"></div>
                            <div>
<img src="" id="image">
                            </div>
                            <div class="clearfix"></div>



                            <br>
                            <div class="form-group">
                                <input type="submit" id="submit" name="submit" value="Next" class="btn btn-primary pull-right">
                               
                            </div>
                            <input type="hidden" name="hidden_image" id="hidden_image">
                            <input type="hidden" name="image_x" id="image_x" value="">
                            <input type="hidden" name="image_y" id="image_y">
                            <input type="hidden" name="image_width" id="image_width">
                            <input type="hidden" name="image_height" id="image_height">
                            <input type="hidden" name="scalex" id="scalex">
                            <input type="hidden" name="scaley" id="scaley">

                    </form>
                    <div class="clearfix"></div>
</div>
            </div><!-- row -->
        </div><!-- container -->
        <br><br>

<?php $this->load->view('includes/footer');?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/cropper.js"></script>
      

        <script>
 
            $(document).ready(function() {
                $(".select").select2({
                    width: '100%'
                });
                $("#sort").select2({
                    minimumResultsForSearch: -1,
                    width: '100%'
                });


                $(document).on('click','.add_pictures',function(){
                    //alert('click');
                    $('#form-upload').remove();

                    $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input   type="file" name="file[]" value="" accept="image/*" /></form>');

                    $('#form-upload input[name=\'file[]\']').trigger('click');

                    if (typeof timer != 'undefined') {
                        clearInterval(timer);
                    }

                    timer = setInterval(function() {
                        if ($('#form-upload input[name=\'file[]\']').val() != '') {
                            var $fileUpload = $("input[type='file']");
                            //console.log($fileUpload.get(0).files);
                            clearInterval(timer);
                            if (parseInt($fileUpload.get(0).files.length)<=1000){
                                $.ajax({

                                    url: '<?php  echo site_url('artistRegistration/upload_temp');?>',
                                    type: 'post',
                                    dataType: 'json',
                                    data: new FormData($('#form-upload')[0]),
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    beforeSend: function() {
                                        $('#button-upload i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                                        $('#button-upload').prop('disabled', true);
                                    },
                                    complete: function() {
                                        $('#button-upload i').replaceWith('<i class="fa fa-upload"></i>');
                                        $('#button-upload').prop('disabled', false);
                                    },
                                    success: function(json) {
                                        if (json['error']) {
                                            alert(json['error']);
                                        }

                                        if (json['success']) {
                                            ///alert(json['success']);
                                            if(json['file_name']){

                                                $('#image').attr('src',json['file_name']);
                                                $('#hidden_image').val(json['name']);

                                                //$("#image").cropper();
                                                    $('#image').cropper({
                                                      aspectRatio: 1,
                                                      crop: function(e) {
                                                      
                                                    //var   x      =   (e.x);
                                                    $("#image_x").val(e.x);
                                                    $("#image_y").val(e.y);
                                                    $("#image_width").val(e.width);
                            
                                                   $("#image_height").val(e.height);

                                                        //console.log(e.rotate);
                                                    $("#scalex").val(e.scaleX);
                                                     $("#scaley").val(e.scaleY);
                                                      }

                                                    });

                                            }
                                            //$('#button-refresh').trigger('click');
                                        }
                                    },
                                    error: function(xhr, ajaxOptions, thrownError) {
                                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                    }
                                });
                            }else
                            {
                                alert("You can only upload a maximum of 10 files once.");
                            }
                        }
                    }, 500);
                });
                
            });
        </script>
        <script>
            $('#submit').on('click', function () {
                    $("form#form-1").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        //debug:true,
                        rules: {
                            email: {
                                required: true,
                            },
                            password: {
                                required: true,
                                //minlength: 8
                            }

                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            form.submit();
                        }
                    });
                });

        </script>
    </body>
</html>