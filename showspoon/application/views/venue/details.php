<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('includes/head');?>
        <script src="<?php echo base_url();?>/assets/dist/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/plugins/select2/select2.min.js"></script>
        <!--<script src="<?php echo base_url();?>/assets/dist/js/bootstrap_multiselect.js"></script>-->
    </head>
    <body>

        <?php $this->load->view('includes/header');?>


        

        <div class="container-fluid">
            <div class="container">
                <div class="row">

                   <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">

					<div class="media MiddleAvatar">
						<div class="media-left">
						<?php
                            $user_image=site_url('assets/dist/images/noimage2.jpg');
                            if(isset($artist->profile_image) && $artist->profile_image!=''){
                                $user_image=site_url('uploads/users/thumb/'.$artist->profile_image);
                            }
                            ?>
							<img src="<?php echo $user_image;?>" class="media-object middleimg" />
						</div>
					</div><!-- media -->
					<h3 class="text-center"><?php echo isset($artist->name)?$artist->name:'';?></h3>

					<div class="row m-t-lg">
					<?php if(isset($artist->genre) && $artist->genre!=''){
                        $genres_idx = explode(',',$artist->genre);
                        ?>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ArtistTags">
						<?php foreach($genres_idx as $gen){?>
							<span class="badge"><?php echo isset($genres[$gen])?$genres[$gen]:'';?></span>
							<?php }?>
							
						</div><!-- col-lg-12 -->
						<?php }?>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center m-t-md">
							<p>Cover Band <span class="m-l-10 m-r-10"></span> Ole Deviks Vei 10, Oslo</p>
						</div><!-- col-lg-12 -->

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center UserRating ratingstars">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i>
							<i class="fa fa-star-o"></i>
							<span> (3.5)</span>
						</div><!-- col-sm-12 -->

					</div><!-- row -->
				</div><!-- col-lg-12 -->

            <?php if(isset($artist->biography) && $artist->biography!=''){ ?>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t-md">
					<p class="text-muted"><?php echo $artist->biography;?> </p> <!--<a href="" class="Morelink">(+More)</a>-->
				</div><!-- col-lg-12 -->
				<?php }?>


            <?php if(isset($members) && count($members)>0){?>
				<div class="col-lg-12 col-md-12 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 m-t-lg m-t-lg">

					<h3 class="text-center">Band Members</h3>

                <?php foreach($members as $mem){
    $user_image=site_url('assets/dist/images/noimage2.jpg');
    if(isset($mem->image_url) && $mem->image_url!=''){
        $user_image=site_url('uploads/members/thumbs/'.$mem->image_url);
    }
    
                    ?>
					<div class="media MiddleAvatar col-sm-3 col-xs-6">
						<div class="media-left">
							<img src="<?php echo $user_image;?>" class="media-object middleimg center-block" />
							<p class="m-t-sm"><?php echo $mem->name;?> / <span class="artistag"><?php echo $mem->role;?></span></p>
						</div>
					</div><!-- media -->
					<?php }?>
					

				</div><!-- col-lg-8 -->
				<?php }?>

				<div class="col-lg-12 col-md-12 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 m-t-lg m-t-lg ArtistTabs">

					<h3 class="text-center">Media</h3>

					<ul class="nav nav-tabs" role="tablist">
						<li class="active"><a href="#photos" aria-controls="home" role="tab" data-toggle="tab">Photos</a></li>
						<li><a href="#videos" aria-controls="videos" role="tab" data-toggle="tab">Videos</a></li>
						<li><a href="#audio" aria-controls="audio" role="tab" data-toggle="tab">Audio Clips</a></li>
					</ul>

					<div class="tab-content m-t-lg">
						<div role="tabpanel" class="tab-pane active" id="photos">

                        <?php if(isset($gallery) && count($gallery)>0){?>
                        <?php foreach($gallery as $gal){
    $user_image=site_url('assets/dist/images/noimage2.jpg');
    if(isset($gal->url) && $gal->url!=''){
        $user_image=site_url('uploads/gallery/thumbs/'.$gal->url);
    }
                    ?>
							<div class="col-sm-4 col-xs-12">
								<a href="<?php echo 'javascript:;';?>" data-toggle="lightbox">
									<img src="<?php echo $user_image;?>" class="img-responsive  center-block" />
								</a>
							</div><!-- col-sm-4 -->

                        <?php }?>
                        <?php }else{?>
                        <p>No gallery found.</p>
                        <?php }?>
							

						</div>
						<div role="tabpanel" class="tab-pane" id="videos">
						<?php if(isset($videos) && count($videos)>0){?>
                        <?php foreach($videos as $vid){
    $video_data = isset($vid->video_data)?$vid->video_data:'';
    if($video_data!=''){
        $video_data=json_decode($vid->video_data);
    }
    if(isset($video_data->html) && $video_data->html!='')
    {
                    ?>
							<div class="col-sm-6 col-xs-12">
								<div class="embed-responsive embed-responsive-16by9">
									<!--<iframe class="embed-responsive-item" src="//www.youtube.com/embed/Jr4TMIU9oQ4?rel=0"></iframe>-->
									<?php echo $video_data->html;?>
									<?php ?>
								</div>
							</div>
							
							 <?php }}?>
                        <?php }else{?>
                        <p>No video found.</p>
                        <?php }?>
							
						</div>
						<div role="tabpanel" class="tab-pane" id="audio">
							<?php if(isset($audios) && count($audios)>0){?>
                        <?php foreach($audios as $vid){
    $video_data = isset($vid->video_data)?$vid->video_data:'';
    if($video_data!=''){
        $video_data=json_decode($vid->video_data);
    }
    if(isset($video_data->html) && $video_data->html!='')
    {
                    ?>
							<div class="col-sm-6 col-xs-12">
								<div class="embed-responsive embed-responsive-16by9">
									<!--<iframe class="embed-responsive-item" src="//www.youtube.com/embed/Jr4TMIU9oQ4?rel=0"></iframe>-->
									<?php echo $video_data->html;?>
									<?php ?>
								</div>
							</div>
							
							 <?php }}?>
                        <?php }else{?>
                        <p>No audio found.</p>
                        <?php }?>
							<!--<div class="col-sm-6 col-xs-12">
								<iframe width="100%" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/329405148&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
							</div>-->
						</div>
					</div>

				</div><!-- col-lg-8 -->


				<div class="col-lg-12 col-md-12 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 m-t-lg m-t-lg">

					<h3 class="text-center">Reviews</h3>

					<div class="col-sm-12 col-xs-12 m-b-md">
						<div class="media m-b-sm">
							<div class="media-left">
								<span class="NameCircle orange">J</span>
							</div>
							<div class="media-body">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 UserRating ratingstars">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
									<i class="fa fa-star-o"></i>
								</div><!-- col-sm-12 -->
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t-sm">
									<h4 class="media-heading text-md">Jessica R. | July 1-, 2-17</h4>
								</div>
							</div>
						</div><!-- media -->
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam </p>
					</div><!-- col-sm-12 -->

					<div class="col-sm-12 col-xs-12 m-b-md">
						<div class="media m-b-sm">
							<div class="media-left">
								<span class="NameCircle lightblue">B</span>
							</div>
							<div class="media-body">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 UserRating ratingstars">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
									<i class="fa fa-star-o"></i>
								</div><!-- col-sm-12 -->
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t-sm">
									<h4 class="media-heading text-md">Brent L. | July 1-, 2-17</h4>
								</div>
							</div>
						</div><!-- media -->
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam </p>
					</div><!-- col-sm-12 -->

				</div><!-- col-lg-8 -->

				<div class="clearfix"></div>
				<br /><br /><br /><br /><br />
                    


                </div><!-- row -->
            </div><!-- container -->
        </div><!-- container-fluid -->

<?php $this->load->view('includes/footer');?>

        <script>
            $(document).ready(function() {
                $(".select").select2({
                    width: '100%'
                });
                $("#sort").select2({
                    minimumResultsForSearch: -1,
                    width: '100%'
                });
                /*$('.select').multiselect({
                includeSelectAllOption: true,
                selectAllText: 'Check all!',
                numberDisplayed: 0
                });*/
            });
        </script>
        <style>
            .search-btn{
                border-radius: 5px;
                margin-top: 29px;
                padding: 10px 60px;
            }
        </style>
    </body>
</html>