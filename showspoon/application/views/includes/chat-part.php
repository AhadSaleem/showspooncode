<?php
$user_image=site_url('assets/dist/images/noimage2.jpg');
if(isset($partner->profile_image) && $partner->profile_image!=''){

    $user_image=site_url('uploads/users/thumb/'.$partner->profile_image);
}
?><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ListViewv3">
    <div class="panel panel-default">
        <div class="panel-body no-padder">
            <div class="gimage m-t-md">
                <img src="<?php echo $user_image; ?>" class="img-responsive center-block img-circle" style="padding-left:30px;padding-right:30px;"/>
            </div><!-- gimage -->
        </div><!-- panel-body -->
        <div class="panel-body wrapper-xs text-center">
            <h4 class="text-center"><?php echo isset($partner->name)?$partner->name:'';?></h4>
            <h5 class="text-center m-b-none m-t-none"><?php 
                if(isset($partner->genre) && $partner->genre!=''){
                    $partner_genres = explode(',',$partner->genre);
                    $genre_names=[];
                    foreach($partner_genres as $genre)
                    {
                        if(isset($genres[$genre]) && $genres[$genre]!=''){
                            $genre_names[]=$genres[$genre];
                        }
                    }
                    echo implode(' / ',$genre_names);
                }
                ?></h5>

            <div class="col-sm-12 text-center ratingstars">
                <?php 
                $minutes = ($booking->hours*60);
                $artisting_rating = (isset($partner->rating) && $partner->rating>0)?number_format($partner->rating,1):0;
                echo star_rating($artisting_rating);?>
                <!-- <span> (0 Reviews)</span> -->
            </div><!-- col-sm-12 -->
            <hr class="m-t-sm m-b-sm" style="display: inline-table; width: 100%;" />
            <h3 class="text-center m-b-md m-t-none bold">Event Details</h3>
            <h5 class="text-center m-b-xs m-t-none">
                <div class="m-b-sm bold"><?php echo date('D, M d, Y',strtotime($booking->event_date));;?></div>

                <div class="m-b-sm bold"><?php echo date('h:i A',strtotime($booking->name));;?> - <?php echo date('h:i A',strtotime("+$minutes minutes",strtotime($booking->name)));;?> (<?php echo $booking->hours;?> hour)</div>
                <?php if(isset($booking->location) && $booking->location!=''){?>
                <label>Location:</label>
                <div class="m-b-sm bold"><?php echo $booking->location;?></div>
                <?php }?>

                <label>Amount:</label>
                <div class="m-b-sm bold"><?php echo number_format($booking->amount,0);?> NOK , <?php echo $who_pay;?></div>
            </h5>
            <!--<h5 class="text-center m-b-xs m-t-none">Oslo, Norway<br class="m-b-xs"><?php echo isset($booking->event_date)?date("M jS, Y",strtotime($booking->event_date)):'';?></h5>-->
            <div class="clearfix"></div>


            <?php if($session_user['type_id']==$booking->type){?>
            <button id="edit_event" type="button" class="btn btn-primary btn-sm thread_send" data-toggle="modal" data-target="#myModal2">
                Edit Event
            </button>
            <?php }?>


        </div><!-- panel-body -->
    </div><!-- panel -->
</div><!-- ChatSession -->
<!-- Modal -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Event</h4>
            </div>
            <div class="modal-body" style="overflow:inherit;">
                

                <form action="" method="post" id="form-modal">

                    <div class="form-group" style="position:relative;">
                        <label>Event Date</label>
                        <input name="date" id="date" value="<?php echo $booking->event_date?>" type="text" class="form-control pickadate" required />
                    </div><!-- form-group -->
                    <div class="form-group"  style="position:relative;">
                        <label>Start Time</label>
                        <input id="time" value="<?php echo $booking->start_time?>" name="time" type="text" class="form-control pickatime" required />
                    </div><!-- form-group -->

                    <!-- <div class="form-group">
                        <label>How long will the vendor be needed?</label>
                        <select id="hours" class="form-control" name="hours">
                            <?php
    for($v=1;$v<=30;$v++){


        $vp=$v*0.5;    
        if($vp<=1){
            echo '<option value="'.$vp.'">'.$vp.' Hour</option>';    
        }else{
            echo '<option value="'.$vp.'">'.$vp.' Hours</option>';
        }



    }
                            ?>
                        </select>
                    </div> -->

                    <div class="form-group">
                        <label>Amount</label>
                        <input required type="text" id="amount" name="amount" value="<?= $booking->amount;?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <button type="submit" id="btn-modal" class="btn btn-primary btn-sm pull-right">Update</button>
                    </div>
                    <div class="clearfix"></div>





                </form>
 
            </div>
        </div>
    </div>  
    <style>
    .red{border:1px solid #a94442;}
    </style>
    <script>
    $(document).ready(function(){

         $('#form-1').submit(function(event){

              event.preventDefault();
              var message = $('#text-message').val();
              if(message==''){
                 
                 $('#text-message').addClass('red');
                 $('#error').text('This field is required.').css('color','#a94442');
              }else{
                 $.ajax({
                        type:'POST',
                        url: '<?php echo site_url('venueBooking/send_message');?>',
                        data:{
                        message:message,
                        to:'<?php echo isset($to_id)?$to_id:0;?>',book_id:'<?php echo isset($booking->id)?$booking->id:0;?>'
                        },
                        dataType:'json',
                        beforeSend:function(){
                        $('#btn-1').attr('disabled',true);
                        },
                        success:function(data){
                        $('#btn-1').removeAttr('disabled');
                        if(data.data){
                        $('#Message-Box').prepend(data.data);
                        
                        }
                        $('#text-message').val('');
                        },
                        error: function(data){
                        $('#btn-1').removeAttr('disabled');
                        // alert('Something went wrong!');
                        }
                    });
              }
         });
         $('#form-2').submit(function(event){

              event.preventDefault();
              var message = $('#text-message').val();
              if(message==''){
                 
                 $('#text-message').addClass('red');
                 $('#error').text('This field is required.').css('color','#a94442');
              }else{
                 $.ajax({
                        type:'POST',
                        url: '<?php echo site_url('artistBooking/send_message');?>',
                        data:{
                        message:message,
                        to:'<?php echo isset($to_id)?$to_id:0;?>',book_id:'<?php echo isset($booking->id)?$booking->id:0;?>'
                        },
                        dataType:'json',
                        beforeSend:function(){
                        $('#btn-1').attr('disabled',true);
                        },
                        success:function(data){
                        $('#btn-1').removeAttr('disabled');
                        if(data.data){
                        $('#Message-Box').prepend(data.data);
                        
                        }
                        $('#text-message').val('');
                        },
                        error: function(data){
                        $('#btn-1').removeAttr('disabled');
                        // alert('Something went wrong!');
                        }
                    });
              }
         });

         $('#text-message').keyup(function(){
              
              var message = $('#text-message').val();
              if(message!=''){
              $('#error').text('');
              $('#text-message').removeClass('red');
             }
         });
    });
    </script>