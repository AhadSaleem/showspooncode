<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Showspoon</title>
    </head>
    <body>
        <div class="container" style="width:420px;margin:auto;">
        <div class="panel panel-default" style="padding:10px;">
        Welcome to Showspoon! <br><br>

        Hello <?php echo isset($full_name)?$full_name:'';?>!<br><br>
        
        Thank you for joining Showspoon! <br><br>

        If there is anything that we can help you with or you have any feedback, don't hesitate to send as an email. <br>
        All the best, The Showspoon Team <br>
         <br>
        www.showspoon.com <br>
        <br>
        <a href="<?php echo site_url('Account/login');?>"><img src="<?php echo site_url('assets/images/logo-n.png');?>" width="200" alt="logo"></a>
        </div>
        </div>
    </body>
</html>