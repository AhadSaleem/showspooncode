<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('includes/head');?>
        <script src="<?php echo base_url();?>/assets/dist/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/plugins/select2/select2.min.js"></script>
        <!--<script src="<?php echo base_url();?>/assets/dist/js/bootstrap_multiselect.js"></script>-->
    </head>
    <body>

        <?php $this->load->view('includes/header');?>




        <div class="container-fluid ">    <!--ListView-->
            <div class="container">


                <div class="panel">

                    <div class="panel-title">
                        <h2 class="col-sm-12">Members</h2>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel-body">
                        <div id="members_box">
                            <div class="row">
                                <?php if(isset($members) && count($members)>0){

    foreach($members as $row){
        if(isset($row->image_url) && $row->image_url!=''){
            $img_path=base_url().'uploads/members/thumbs/'.$row->image_url;
        }else{
            $img_path=base_url().'assets/images/placeholder.jpg';
        }
                                ?>
                                <div  data-id="<?php echo $row->id;?>" class="col-lg-3 col-md-6 member_list">
                                    <div class="panel panel-body b">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#" data-popup="lightbox">
                                                    <img src="<?php echo $img_path;?>" style="width: 70px; height: 70px;" class="img-circle" alt="">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <p class="text-muted m-b-xs"><?php echo isset($row->name)?$row->name:'';?></p>
                                                <h6 class="media-heading"><?php echo isset($row->alias)?$row->alias:'';?></h6>
                                                <h6 class="media-heading"><?php echo isset($row->role)?$row->role:'';?></h6>
                                                <div class="">
                                                    <a  href="javascript:;" class="edit_btn btn btn-info btn-sm r-2x">Edit</a>
                                                    <a href="javascript:;" class="delete_btn btn btn-danger btn-sm r-2x">Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }}?>
                            </div>
                        </div>

                        <?php if(isset($success) && $success!=''){?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <strong>Success!</strong> <?php echo $success;?>
                        </div>
                        <?php }?>
                        <?php if(isset($error) && $error!=''){?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <strong>Warning! </strong> <?php echo $error;?>
                        </div>
                        <?php }?>

                        <form id="profile" method="post" action="" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <div class="thumbnail">
                                            <img id="member_image" src="<?php echo site_url('assets/dist/images/logo.png');?>" alt="" class="">
                                            <input type="file" name="" id="img_browse">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" required id="name" name="name"  value="" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Alias</label>
                                        <input type="text" required id="alias" name="alias"  value="" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Role</label>
                                        <input type="text" required id="role" name="role"  value="" class="form-control">
                                    </div>
                                </div>
                            </div>





                            <div class="form-group">
                                <input type="submit" id="submit" name="submit" value="Next" class="btn btn-primary pull-right">
                            </div>
                        </form>
                    </div>
                </div>




            </div><!-- container -->
        </div><!-- container-fluid -->


        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>

        <script>
            var orginal_src = '';
            $(document).ready(function () {
                orginal_src=$('#member_image').attr('src');
                $(document).on('click','.delete_btn',function(){
                    var delete_btn = $(this);
                    var row_id = $(this).closest('.member_list').data('id');
                    
                    $.ajax({
                        type:'POST',
                        url: '<?php echo site_url('artistProfile/delete_member');?>',
                        data:{row_id:row_id},
                        dataType:'json',
                        
                        
                        success:function(data){
                            if(data.data){
                               delete_btn.closest('.member_list').remove();
                            }
                            
                        },
                        error: function(data){

                        }
                    });
                });
                function readURL(input) {

                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $('#member_image').attr('src', e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                    }
                }

                $("#img_browse").change(function(){
                    console.log(this);
                    readURL(this);
                });

                // $.validator.setDefaults({ ignore: ":hidden:not(.w-full)" })
                $('#submit').on('click', function () {
                    $("form#profile").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            email: {
                                required: true,
                            },
                            password: {
                                required: true,
                                //minlength: 8
                            }

                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            //form.submit();

                            var formData = new FormData();
                            formData.append('file', $('#img_browse')[0].files[0]);
                            formData.append('name', form.name.value);
                            formData.append('role', form.role.value);
                            formData.append('alias', form.alias.value);
                            console.log(formData);

                            $.ajax({
                                type:'POST',
                                url: '<?php echo site_url('artistProfile/add_member');?>',
                                data:formData,
                                dataType:'json',
                                cache:false,
                                contentType: false,
                                processData: false,
                                success:function(data){
                                    if(data.data){
                                        $('#members_box .row').append(data.data);
                                        $('#member_image').attr('src', orginal_src);
                                    }
                                    $('#profile')[0].reset();
                                },
                                error: function(data){

                                }
                            });

                        }
                    });
                });
            });
        </script>

        <script>
            $(document).ready(function() {
                $(".select").select2({
                    width: '100%'
                });
                $("#sort").select2({
                    minimumResultsForSearch: -1,
                    width: '100%'
                });
                /*$('.select').multiselect({
                includeSelectAllOption: true,
                selectAllText: 'Check all!',
                numberDisplayed: 0
                });*/
            });
        </script>
        <style>
            .search-btn{
                border-radius: 5px;
                margin-top: 29px;
                padding: 10px 60px;
            }
        </style>
    </body>
</html>