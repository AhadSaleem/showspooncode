<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Requests</title>
    <!-- for Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo site_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url(); ?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo site_url(); ?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo site_url('assets/favicon/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">
    <link href="<?php echo site_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/custom-develop.css" rel="stylesheet">
    <!-- new wireframe style -->
    <link href="<?php echo site_url(); ?>assets/css/custom_style.css" rel="stylesheet">
    <!-- for bootstrap live search -->
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.css"> -->
    <!-- for ekko lightbox -->
    <link href="<?php echo site_url(); ?>assets/css/ekko-lightbox.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- for flexslider -->
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/flexslider.css" type="text/css" media="screen" />




    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="<?php echo site_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/select2.min.js"></script>
    <!--<script src="http://165.227.110.185/showspoon//assets/dist/js/bootstrap_multiselect.js"></script>-->
    <style>
    .bold_class{font-weight: 700;}
    .light_class{font-weight: 400;}
    </style>
</head>
<body>

<p id="alert" style="margin: 0;"></p>

<div class="modal fade" id="messageModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Booking Confirmed</h4>
        </div>
        <div class="modal-body">
          <p><?php echo $this->session->flashdata('success'); ?></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
</div>
</div>
<?php if(!empty($this->session->flashdata('success'))) { ?>
<script>
    $(window).on('load',function(){
       $('#messageModal').modal('show');
    });
 </script>
<?php } ?>
    <header class="main-header">
        <!-- Static navbar -->
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>

            <!-- <button type="button" class="navbar-toggle collapsed filter_btn" data-toggle="collapse" data-target="#sidebar" aria-expanded="false" aria-controls="sidebar">
              <i class="fa fa-filter" aria-hidden="true"></i>
            </button> -->


            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <img src="<?php echo site_url(); ?>assets/images/logo-n.png" alt="Logo">
            </a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="search">
                  <!-- search form -->
                  <form id="search" action="<?php echo site_url('venue/search'); ?>" method="post">
                     <input type="search" name="search" placeholder="Search" autocomplete="off">
                  </form>
                  <!-- search form ends-->
              </li>
              <li><a href="<?php echo site_url(); ?>">Artists</a></li>
              <li><a href="<?php echo site_url('venueBooking/booking'); ?>">My bookings</a></li>
              <li><a href="<?php echo site_url('venueBooking/inbox'); ?>">Inbox <span id="my_bookings" class="badge badge_upper inbox-count"></span></a></li>
              <li class="active"><a href="<?php echo site_url(); ?>venueBooking/my_requests">Requests <span id="my_bookings" class="badge badge_upper request-count"></span></a></li>
              <li class="favourite"><a href="<?php echo site_url("venue/favourties"); ?>" class="heart"></a></li>
              <li>
                <div class="inset dropdown">   <?php if($image!=""){$img = site_url()."uploads/users/thumb/".$image; }else{$img = site_url()."assets/blank.png";} ?>               
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img class="profile_img" src="<?php echo $img; ?>"> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url(); ?>venueProfile">Profile</a></li>
                  <li><a href="<?php echo site_url(); ?>account/logout">Logout</a></li>
                </ul>
                </div>
              </li>
      
            </ul>

          </div><!--/.nav-collapse -->

        </div><!--/.container -->
      </nav>

    </header>


    <!-- content area -->
    <div class="container">
      <h1 class="text-center marg_thrty_topper">Requests</h1>
        <div class="row">
            <!-- sidebar -->
            <div class="col-md-3 filter_sidebar">
                <div class="spacer_big">
                    <span class="image-show"></span>
                    <span class="data"></span>
                </div>    
            </div>
            <!-- sidebar ends -->


            <div class="col-md-9">
                
                
                <div class="spacer">
                    <div class="row"> 

                      <div class="col-sm-4 col-sm-push-8">
                           <!--  <form class="form-inline sorting_form">
                            <div class="form-group">
                                <select id="sorting" class="selectpicker">
                                <option>View all invitations</option>
                                <option>Another Option</option>
                              </select>
                            </div>
                          </form> -->
                        </div>

                        <div class="col-sm-8 col-sm-pull-4">
                            <!-- tab menu -->
                            <div class="inbox_tabs_wrap"> 
                                <ul  class="nav nav-pills">
                                    <li class="active">
                                        <a  href="#1a" data-toggle="tab">Sent Requests</a>
                                    </li>
                                    <li>
                                        <a href="#2a" data-toggle="tab">Received Requests</a>
                                    </li>
                                </ul>

                                            
                                  </div>
                            <!-- tab menu ends -->
                        </div>
                        
                    </div>
                </div><!--/.spacer -->

                <!-- table view --> 
                <div class="tab-content clearfix req_table_wrap">
                  <div class="tab-pane" id="2a"> 
                    <span id="artist_request"></span>
                    <div id="pages"></div>
                    </div>

                      <div class="tab-pane active" id="1a">  

                      <?php if(count($requests) > 0){ foreach($requests as $row){ 
                      if($row->status==1){
                        $active_class = "bold_class";
                      }else{
                        $active_class = "light_class";
                      }
                      ?>
                      <div class="req_parent_row clearfix">
                      <div class="req_col_badge">
                        <img src="<?php echo site_url(); ?>uploads/users/thumb/<?php echo $row->profile_image; ?>" class="img-circle" id="view-details" data-id="<?php echo $row->id; ?>" style="width:40px;height:40px;margin-left:10px;">
                        <!-- <span class="badge gold-badge">new</span> -->
                      </div> 
                      <div class="req_col_title <?php echo $active_class; ?>">
                      <?php echo $row->name; ?>
                      </div> 
                      <div class="clearfix-sm"></div>
                      <div class="req_col_date <?php echo $active_class; ?>">
                      <?php echo date('jS M, Y',strtotime($row->event_date)); ?>
                      </div>
                      <div class="req_col_time <?php echo $active_class; ?>">
                       <?php echo $row->start_time.'-'.$row->end_time; ?>
                      </div>
                      <div class="clearfix-sm"></div>
                      <div style="cursor:pointer;" class="req_col_detail <?php echo $active_class; ?> hidden-sm hidden-xs" data-toggle="collapse" data-target="#collapsable_sent_req_one<?php echo $row->id; ?>" data-id="<?php echo $row->id; ?>" id="view-details"> 
                        <i class="fa fa-caret-right" aria-hidden="true"></i> view details
                      </div>


                      <div class="req_col_decision">
                        <?php 
                         $commission = $data->commission;
                         $amount = explode('.',$row->amount);
                         $pay = $amount[0];
                        if($row->btn=="success"){ ?>
                        <div class="float-left mr-2 mt-5p mb-5p req_badge_wid">
                          <span class="m-badge  m-badge--awaitingpayment m-badge--wide">
                          Awaiting Payment
                          </span>
                        </div> 
                        <?php }else{  
                          if($row->btn=="paid"){
                            ?>
                            <div class="float-left mr-2 mt-5p req_badge_wid">
	                            <span class="m-badge  m-badge--success m-badge--wide" style="float:left;">
	                            Booked
	                            </span>
                        	</div>
                            <button type="button" class="btn-danger btn-sm float-left btn btn-wid-sixty cancel" data-artist="<?php echo $row->artist_id; ?>" id="<?php echo $row->id; ?>" data-toggle="modal" data-target="#myModal">Cancel</button>
                            <?php 
                          }else if($row->btn=="cancelled"){ ?>

                          <span class="m-badge  m-badge--cancelled m-badge--wide" style="float:left;">
                            Canceled
                          </span>
                          <?php
                          }
                          else{
                          ?> 
                          <div class="float-left mr-2 req_badge_wid text-left">
                            <span class="m-badge  m-badge--pending m-badge--wide mt-5p">Pending</span>
                          </div>
                          <?php 
                        }
                        } ?>
                        <div class="clearfix-sm"></div>  
                      </div>
                      <div class="req_col_decision_2">                        
                        <?php if($row->btn=="success"){ ?>
                      <div class="clearfix">
                      <div class="float-left">
                        <form action="<?php echo site_url('venueBooking/charge'); ?>" method="POST" onsubmit="return false;">
                        <script
                          src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                          data-key="pk_test_NWhYzPP5DpSh6D2xHGQXwQr8"
                          data-amount="<?php echo $pay * 100; ?>"
                          data-name="<?php echo $row->name; ?>"
                          data-description="Pay Amount"
                          data-image="<?php echo site_url(); ?>uploads/users/thumb/<?php echo $row->profile_image; ?>"
                          data-email="<?php echo $user_email; ?>"
                          data-locale="auto">
                        </script>
                        <input type="hidden" name="amount" value="<?php echo $pay; ?>">
                        <input type="hidden" name="id" value="<?php echo $row->id; ?>">
                        <input type="hidden" name="artist_id" value="<?php echo $row->user_id; ?>">
                      </form>
                      </div>

                      <button type="button" class="btn-danger btn-sm float-left btn btn-wid-sixty cancel" data-artist="<?php echo $row->artist_id; ?>" id="<?php echo $row->id; ?>" data-toggle="modal" data-target="#myModal">Cancel</button>
                    </div><!-- clearfix -->
                        <?php } ?>
                      </div> 


                      <!-- this is repetition of Detai col for smaller screens -->
                      <div class="clearfix-sm"></div>
                      <div style="cursor:pointer;" class="req_col_detail hidden-md hidden-lg" data-toggle="collapse" data-target="#collapsable_sent_req_one<?php echo $row->id; ?>" data-id="<?php echo $row->id; ?>">
                        <i class="fa fa-caret-right" aria-hidden="true"></i> view details
                      </div>

                      <!-- this is repetition of Detai col for smaller screens -->               
                    </div><!-- req_parent_row -->

                    <div class="clearfix"></div>
                      <div id="collapsable_sent_req_one<?php echo $row->id; ?>" class="collapse">
                        <div class="req_parent_row_collapse">
                          <div class="row">
                            <div class="col-sm-4">
                              <div class="event_info_card">
                                <h3>information</h3>
                                <ul>
                                  <li>Name: <strong><a href='<?php echo site_url("venue/details/$row->artist_id"); ?>' style="color:#555;text-decoration:none;"><?php echo $row->name; ?></a></strong></li>
                                  <li>Event date: <strong><?php echo $row->event_date; ?></strong></li>
                                  <li>Set length: <strong><?php echo $row->start_time.' - '.$row->end_time; ?></strong></li>
                                  <?php if($row->amount!="" && $row->artist_currency!=""){ 
                                  ?><li>Artist Amount: <strong><?php echo(convertCurrency($row->artist_rate, $row->artist_currency, $currency))." ".$currency; ?></strong></li><?php
                                  }else{echo '';} 
                                  ?>
                                  <li>Charge Fee: <strong>Charge fee for <?php echo $commission; ?>% is <?php if($row->artist_currency!=""){echo (convertCurrency($row->artist_rate, $row->artist_currency, $currency) / 100) * $commission." ".$currency;} ?></strong></li>
                                  <li>Total: <strong><?php echo @$pay." ".$currency; ?></strong></li>
                                  <li class="seperator">&nbsp;</li>
                                </ul>
                              </div>
                            </div>
                            <?php if($row->status=="paid"){ ?>
                            <div class="col-sm-4">
                              <div class="req_parent_row_collapse_space"></div>
                              <div class="spacer_medium">
                                <h3>address</h3>
                                <p>
                                  <?php echo $row->location; ?>
                                </p>
                              </div>
                            </div>
                            <?php } ?>
                            <div class="col-sm-4">
                              <div class="req_parent_row_collapse_space"></div>
                              <div class="spacer_medium">
                                <h3>notes</h3>
                                <p class="smallpara">
                                  <?php echo $row->message; ?>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div><!-- collapsable_row_one -->

                    <?php }}else{echo '<p style="text-align:center;">No requests found</p>';} ?>


                    </div>

                    <div class="tab-pane" id="3a">
                      <?php if(count($decline) > 0){ foreach($decline as $row){ ?>
                      <div class="req_parent_row clearfix">
                      <div class="req_col_badge">
                        <!-- <span class="badge gold-badge">new</span> -->
                      </div> 
                      <div class="req_col_title">
                      <?php echo $row->name; ?>
                      </div>
                      <div class="req_col_date">
                      <?php echo date('jS M, Y',strtotime($row->event_date)); ?>
                      </div>
                      <div class="req_col_time">
                       <!--  9:00 pm -->
                      </div>
                      <div class="req_col_detail" data-toggle="collapse" data-target="#collapsable_sent_req_one<?php echo $row->artist_id; ?>" data-id="<?php echo $row->artist_id; ?>">
                        <i class="fa fa-caret-right" aria-hidden="true"></i> view details
                      </div>
                      <div class="req_col_decision">
                        <button class="btn btn-danger">
                          Declined
                        </button>
                      </div>
                      <div class="req_col_menudots">
                        <div class="dropdown">
                        <i class="fa fa-ellipsis-h dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>
                        <ul class="dropdown-menu dropdown-menu-right">
                          <li><a href="javascript:;">Delete</a></li>
                        </ul>
                      </div><!-- dropdown -->
                      </div>                   
                    </div><!-- req_parent_row -->

                    <div class="clearfix"></div>
                      <div id="collapsable_sent_req_one<?php echo $row->artist_id; ?>" class="collapse">
                        <div class="req_parent_row_collapse">
                          <div class="row">
                            <div class="col-sm-4">
                              <div class="event_info_card">
                                <h3>information</h3>
                                <ul>
                                  <li>Name: <strong><?php echo $row->name; ?></strong></li>
                                  <li>Event date: <strong><?php echo $row->event_date; ?></strong></li>
                                  <li>Set length: <strong><?php echo $row->start_time.' - '.$row->end_time; ?></strong></li>
                                  <li class="seperator">&nbsp;</li>
                                </ul>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="req_parent_row_collapse_space"></div>
                              <div class="spacer_medium">
                                <h3>contact</h3>
                                <p>
                                  Ben Masters
                                  <br/>
                                  Phone no: 318 884 4435
                                </p>
                              </div>
                              <div class="spacer_medium">
                                <h3>address</h3>
                                <p>
                                  <?php echo $row->location; ?>
                                </p>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="req_parent_row_collapse_space"></div>
                              <div class="spacer_medium">
                                <h3>notes</h3>
                                <p class="smallpara">
                                  <?php echo $row->message; ?>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div><!-- collapsable_row_one -->

                    <?php }}else{echo '<p style="text-align:center;">No archive found</p>';} ?>
                    </div>
                    <div class="tab-pane" id="4a">
                        <p>
                         data for Blocked
                       </p>
                    </div>
                </div>
                <!-- table view ends -->

            </div>

        </div><!--/.row-->
    </div>
    <!-- content area ends -->


    <!-- extra space -->
    <div class="clearfix spacer_hundred">&nbsp;</div> 
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Cancel Request</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to cancel the request?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger decline" id="btn-ok">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
      
</div>
</div>

<div class="modal fade" id="cancel_success" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Request Canceled</h4>
        </div>
        <div class="modal-body">
          <p>The request has been canceled.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
</div>
</div>

<div class="modal fade" id="accept_request" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Request Accepted</h4>
        </div>
        <div class="modal-body">
          <p id="accept-msg"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
</div>
</div>
    <!-- extra space ends-->
    <script src="<?php echo site_url(); ?>assets/js/pnotify.min.js"></script>
`
    <footer class="main-footer" style="position:fixed;bottom:0;left:0;right:0;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-left">
                    <p class="copyright">© <?php echo date('Y'); ?> Showspoon. All rights reserved.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                    <ul class="legallinks text-right">
                        <a href="<?php echo site_url('venue/privacy_policy'); ?>">Privacy Policy</a><span class="sep "> |</span>
                        <a href="<?php echo site_url('venue/terms'); ?>">Terms of Use </a><span class="sep "> |</span>
                        <a href="<?php echo site_url('venue/contact'); ?>">Contact Us</a>
                    </ul>
                </div>
            </div>
            <!-- row -->
        </div>
    </footer>

<div id="payment" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Payment</h4>
      </div>
      <div class="modal-body">
        <form method="post" id="paymentFrm" enctype="multipart/form-data" action="<?php echo site_url('venueBooking/check'); ?>">
                        <div class="form-group">
                        <?php if (validation_errors()): ?>
                        <div class="alert alert-danger" role="alert">
                            <strong>Oops!</strong>
                            <?php echo validation_errors() ;?> 
                        </div>  
                            <?php endif ?>
                        </div>
                        <div class="form-group"><span id="success"></span></div>
                        <input type="hidden" name="hidden" id="hidden" value="">

                         <div class="form-group">
                            <input type="number" name="card_num" id="card_num" class="form-control" placeholder="Card Number" autocomplete="off" value="<?php echo set_value('card_num'); ?>" required>
                        </div>
                       
                        
                        <div class="row">

                            <div class="col-sm-8">
                                 <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="exp_month" maxlength="2" class="form-control" id="card-expiry-month" placeholder="MM" value="<?php echo set_value('exp_month'); ?>" required>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="exp_year" class="form-control" maxlength="4" id="card-expiry-year" placeholder="YYYY" required="" value="<?php echo set_value('exp_year'); ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" name="cvc" id="card-cvc" maxlength="3" class="form-control" autocomplete="off" placeholder="CVC" value="<?php echo set_value('cvc'); ?>" required>
                                </div>
                            </div>
                        </div>
                        

                       

                        <div class="form-group text-right">
                          <button class="btn btn-secondary" type="reset">Reset</button>
                          <button type="submit" id="payBtn" class="btn btn-success">Submit Payment</button>
                        </div>
                    </form> 
      <!-- <form class="form" id="transaction_form1" action="">
      <div class="form-group">
      <div class="col-md-4">
      <label>Card Number</label>
      <input type="text" class="form-control" name="number" id="number" data-stripe="number">
      <span id="error-number"></span>
      </div>
      </div>
      <div class="form-group">
      <div class="col-md-4">
      <label>CVC</label>
      <input type="text" class="form-control" id="cvc" name="cvc" data-stripe="cvc">
      <span id="error-cvc"></span>
      </div>
      </div>
      <div class="form-group">
      <div class="col-md-4">
      <label>Expiration MM / YY</label>
      <input type="text" class="form-control" name="mmyy" id="mmyy" data-stripe="mm-yy">
      <span id="error-mmyy"></span>
      </div>
      </div>
      <div class="form-group">
      <div class="col-md-12">
      <label>Amount</label>
      <input type="text" class="form-control" name="amount" id="amount" data-stripe="amount">
      <span id="error-amount"></span>
      </div>
      </div>
      <div class="form-group">
      <div class="col-md-offset-4 col-md-4">
      <label style="visibility:hidden;">Payment</label>
      <input type="submit" class="btn btn-primary" value="Payment" id="submit">
      </div>
      </div>
      <p id="msgSuccess"></p>
      </form> -->
      </div>
    </div>
  </div>
</div>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
        //set your publishable key
        Stripe.setPublishableKey('pk_test_NWhYzPP5DpSh6D2xHGQXwQr8');
      
        //callback to handle the response from stripe
        function stripeResponseHandler(status, response) {
            if (response.error) {
                //enable the submit button
                $('#payBtn').removeAttr("disabled");
                //display the errors on the form
                // $('#payment-errors').attr('hidden', 'false');
                $('#payment-errors').addClass('alert alert-danger');
                $("#payment-errors").html(response.error.message);
            } else {
                var form$ = $("#paymentFrm");
                //get token id
                var token = response['id']; 
                //insert the token into the form
                form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");  
                //submit form to the server
                //form$.get(0).submit();
                $.ajax({
                  url:'<?php echo site_url('venueBooking/check'); ?>',
                  type:'POST',
                  data:form$.serialize(),
                  beforeSend: function(){$('#success').html('<h4>Loading...</h4>');},
                  success: function(data){
                    $('#success').html('');
                    $('#success').html(data); 
                    $('#payBtn').attr("disabled", false);
                    form$.get(0).reset();
                    setTimeout(function(){
                      $('#success').html('');
                      $("#payment").removeClass("in");
                      $(".modal-backdrop").remove();
                      $('.modal').modal('hide');

                    },1000); location.reload();
                  } 
                });
            }
        } 
        
        $(document).ready(function() {
        $(".stripe-button-el span").remove();
            $("button.stripe-button-el").removeAttr('style').css({
                "display":"block",
                "width":"60px",
                "border-radius":"3px",
                "padding":"5px 10px",
                "float":"left",
                "margin-right":"5px",
                "background":"#5cb85c",
                "text-transform":"capitalize",
                "border":"1px solid #4cae4c",
                "text-align":"center",
                "cursor":"pointer",
                "line-height":"1.5",
                "color":"white",
                "font-size":"12px" }).html("Pay"); 
          $.ajax({
            url:'<?php echo site_url("venue/getHeartCount"); ?>',
            type:'POST',
            success:function(data){
              setTimeout(function(){
              $('li.favourite a.heart').text(data);
              },500);
            }
          });
            //on form submit
            $("#paymentFrm").submit(function(event) { 
                
                //disable the submit button to prevent repeated clicks
                $('#payBtn').attr("disabled", true);

                //create single-use token to charge the user
                Stripe.createToken({
                    number: $('#card_num').val(),
                    cvc: $('#card-cvc').val(),
                    exp_month: $('#card-expiry-month').val(),
                    exp_year: $('#card-expiry-year').val()
                }, stripeResponseHandler);
                
                //submit from callback
                return false;
            });

            $(document).on('click','.payment',function(){
                 var id = $(this).data('id');  
                 $('#hidden').attr('value',id);
            });
        });
    </script>
    <!-- for bootstrap select -->
    <script src="<?php echo site_url(); ?>assets/js/bootstrap-select.js"></script> 
    
    <style>
    .red{border:1px solid #a94442;}
    </style>
    <!-- for back to top -->
    <script type="text/javascript">
     $(document).ready(function(){

       
      $('#transaction_form1').submit(function(event){

      	  event.preventDefault();
      	  var $form = $(this);
          var number = $form.find('#number').val();
          var cvc = $form.find('#cvc').val();
          var mmyy = $form.find('#mmyy').val();
          var amount = $form.find('#amount').val();
      	  if(number==''){

      	  	 $('#error-number').text('This field is required').css('color','#a94442');
      	  	 $form.find('#number').addClass('red');
      	  }
      	  if(cvc==''){

      	  	 $('#error-cvc').text('This field is required').css('color','#a94442');
      	  	 $form.find('#cvc').addClass('red');
      	  }
      	  if(mmyy==''){

      	  	 $('#error-mmyy').text('This field is required').css('color','#a94442');
      	  	 $form.find('#mmyy').addClass('red');
      	  }
      	  if(amount==''){

      	  	 $('#error-amount').text('This field is required').css('color','#a94442');
      	  	 $form.find('#amount').addClass('red');
      	  }
      	  else{
      	  	$.ajax({
      	  		url    :   '<?php echo site_url('venueBooking/payment'); ?>',
      	  		type   :   'POST',
      	  		data   :    $form.serialize(), 
      	  		success:   function(data){
      	  			
      	  			$('#msgSuccess').html(data);
      	  			$form[0].reset();
      	  			setTimeout(function(){
      	  			  $('#payment').modal('hide');
      	  				$('#msgSuccess').html('');
      	  			},700);
      	  			//alert(data);
      	  		}
      	  	});
      	  }
      });

      $('#number').bind('change , keyup',function(){

      	  $('#number').removeClass('red');
      	  $('#error-number').text('');
      });

      $('#cvc').bind('change , keyup',function(){

      	  $('#cvc').removeClass('red');
      	  $('#error-cvc').text('');
      });

      $('#mmyy').bind('change , keyup',function(){

      	  $('#mmyy').removeClass('red');
      	  $('#error-mmyy').text('');
      });

      $('#amount').bind('change , keyup',function(){

      	  $('#amount').removeClass('red');
      	  $('#error-amount').text('');
      });


       function getArtistRequestCount()
       {
          $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistRequestCount"); ?>',
                        type    :  'POST',
                        dataType:  'JSON',
                        success :  function(data){

                          if(data.status=='request' && data.cnt > 0 && data.is_read==0){
                            $('.request-count').html(data.cnt);
                          }
                        }
                   });
       }
       getArtistRequestCount();
        // var clear = setInterval(function(){
        // getArtistRequestCount(); 
        // },100);
       function getArtistInboxCount(){
        $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistInboxCount"); ?>',
                        type    :  'POST',
                        dataType:  'JSON',
                        success :  function(data){ 

                          if(data.status=='message' && data.cnt > 0 && data.is_read==0){
                            $('.inbox-count').html(data.cnt);
                          }
                        }
                   });
      }
      getArtistInboxCount();
       function getArtistNotification(){
         $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistNotification"); ?>',
                        type    :  'POST',
                        success :  function(data){

                            $('#alert').html(data);
                        }
                   });
      }
      getArtistNotification();

      // var clear = setInterval(function(){
      //   getArtistNotification(); 
      // },100);
      

      $(document).on('click','.close',function(){
        var id = $(this).data('id');
        var mr = $(this).data('mr');
        $.ajax({
                        url     :  '<?php echo site_url("venueBooking/deleteArtistNotification"); ?>',
                        type    :  'POST',
                        data    :  {id:id},
                        success :  function(data){
                          if(mr=='request'){
                            $('.request-count').text('');
                          }else if(mr=='message'){
                            $('.inbox-count').text('');
                          }
                        }
                   });
      });

      function artistRequest(page){
        $.ajax({
                        url     :  "<?php echo site_url(); ?>venueBooking/getArtistRequests/"+page,
                        method  :  "GET",
                        dataType:  "JSON",
                        success :  function(data){

                            $('#artist_request').html(data.data);
                            $('#pages').html(data.links);
                        }
                   });
      }

      artistRequest(1);  
      

     $(document).on("click", ".pagination li a", function(event){
      event.preventDefault();
      var page = $(this).data("ci-pagination-page");
      artistRequest(page);
     });


      // setInterval(function(){
      //   artistRequest();
      // },2000);

      $(document).on('click','#view-details',function(){
        $(this).parent().find('.bold_class').removeClass('bold_class');
             var id = $(this).data('id');  

             $.ajax({
                        url     :  '<?php echo site_url("venueBooking/get_artist_image"); ?>',
                        type    :  'POST',
                        data    :  {id:id},
                        success :  function(data){
                            $('#label' + id).remove();
                            $('.data').hide();
                            $('.image-show').html(data);
                        }
                   });
        });

      $(document).on('click','.status',function(){

          var status  = $(this).data('status');
          var from_id = $(this).data('user');
          var to_id   = $(this).data('new'); 
          var name = $(this).data('name');  
          $.ajax({
            url     : '<?php echo site_url("venueBooking/btn_status"); ?>',
            type    : 'POST',
            data    : {status:status,from_id:from_id},
            success : function(data){ 
            	setTimeout(function(){location.reload();},5000);
              $('#accept-msg').html('Thank you for accepting the request. Now ('+name+') will have make the payment within hours in order to confirm the booking.');
              $('#accept_request').modal();
            }
          });
      });
      $(document).on('click','.cancel',function(){
          var id = $(this).attr('id');  
          var artist = $(this).data('artist'); 
          $('#btn-ok').attr('data-id',id); 
          $('#btn-ok').attr('data-artist',artist);
      });
      $(document).on('click','.decline',function(){

          var id = $(this).data('id'); 
          var artist = $(this).data('artist'); 
          $.ajax({
            url     : '<?php echo site_url("venueBooking/venueDecline"); ?>',
            type    : 'POST',
            data    : {id:id,artist:artist},
            success : function(data){
            	setTimeout(function(){location.reload();},1000);
              $('#cancel_success').modal();
              //$('#remove'+id).remove();
            }
          });
      });

      function hide(){ 
          $.ajax({
            url     : '<?php echo site_url(); ?>venueBooking/hide',
            type    : 'POST',
            data    : {type:1},
            success : function(data){ 
              $('.data').html(data);
            }
          });
        }
        window.onload = hide();
         $.ajax({
          url  : '<?php echo site_url("venueBooking/auto_send"); ?>',
          type : 'POST',
          success : function(data){
            //alert(data);
          }
        });
     

});
    </script>

    <!-- for ekko lightbox  -->
    <script src="<?php echo site_url(); ?>assets/js/ekko-lightbox.js"></script>
    <script type="text/javascript">
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });
    </script>

    <!-- for flexslider -->
    <!-- FlexSlider -->
  <script defer src="<?php echo site_url(); ?>assets/js/jquery.flexslider.js"></script>

  <script type="text/javascript">
    $(function(){
      SyntaxHighlighter.all();
    });
    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>
<!-- for header shrink -->
    <script type="text/javascript">
        $(document).on("scroll", function(){
        if
          ($(document).scrollTop() > 50){
              $(".main-header").addClass("shrink");
            }
            else
            {
                $(".main-header").removeClass("shrink");
            }
        });
    </script>

  <!-- for message modal  -->


  <!-- Modal -->
  <div class="modal fade message_modal" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="clearfix member_box">
                <figure>
                    <img src="<?php echo site_url(); ?>assets/images/male.jpg" alt="Member">
                </figure>
                <figcaption>
                   Band Name Here
                </figcaption>
            </div>
        </div>
        <div class="modal-body">
          <form class="">
              <textarea class="form-control" placeholder="Write your message here..."></textarea>
              <div class="pull-right">
                  <input type="submit" name="submit" value="Send Message" class="profile_save_btn_small">
              </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


</body>

</html>
<?php 
function convertCurrency($amount, $from, $to){
@$conv_id = "{$from}_{$to}";
$string = @file_get_contents("http://free.currencyconverterapi.com/api/v3/convert?q=$conv_id&compact=ultra");
$json_a = @json_decode($string, true);

return @$amount * round($json_a[$conv_id], 2);
}
?>

