<!DOCTYPE html>
<!-- saved from url=(0049)http://165.227.110.185/showspoon/artist/dashboard -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Venue Bookings</title>
    <link href="<?php echo site_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/custom-develop.css" rel="stylesheet">
    <!-- new wireframe style -->
    <link href="<?php echo site_url(); ?>assets/css/custom_style.css" rel="stylesheet">
    <!-- for bootstrap live search -->
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.css"> -->
    <!-- for ekko lightbox -->
    <link href="<?php echo site_url(); ?>assets/css/ekko-lightbox.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- for flexslider -->
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/flexslider.css" type="text/css" media="screen" />




    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="<?php echo site_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/select2.min.js"></script>
    <!--<script src="http://165.227.110.185/showspoon//assets/dist/js/bootstrap_multiselect.js"></script>-->
</head>

<body>


<p id="alert" style="margin: 0;"></p>

    <header class="main-header">
        <!-- Static navbar -->
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>

            <!-- <button type="button" class="navbar-toggle collapsed filter_btn" data-toggle="collapse" data-target="#sidebar" aria-expanded="false" aria-controls="sidebar">
              <i class="fa fa-filter" aria-hidden="true"></i>
            </button> -->


            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <img src="<?php echo site_url(); ?>assets/images/logo-n.png" alt="Logo">
            </a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="search">
                  <!-- search form -->
                  <form id="search">
                     <input type="search" placeholder="Search">
                  </form>
                  <!-- search form ends-->
              </li>
              <li class="active"><a href="<?php echo site_url('artistBooking/booking'); ?>">My bookings</a></li>
              <li><a href="<?php echo site_url('artistBooking/inbox'); ?>">Inbox <span id="my_bookings" class="badge badge_upper">2</span></a></li>
              <li><a href="<?php echo site_url(); ?>artistBooking/my_requests">Requests <span id="my_bookings" class="badge badge_upper request-count"></span></a></li>
              <li>
                <div class="inset dropdown">                  
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img class="profile_img" src="http://rs775.pbsrc.com/albums/yy35/PhoenyxStar/link-1.jpg~c200"> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url(); ?>artistProfile">Profile</a></li>
                  <li><a href="<?php echo site_url(); ?>account/logout">Logout</a></li>
                </ul>
                </div>
              </li>
      
            </ul>

          </div><!--/.nav-collapse -->

        </div><!--/.container -->
      </nav>

    </header>




    <!-- content area -->
    <div class="container">

      <h1 class="text-center marg_thrty_topper">Bookings <small>(Artist)</small></h1>
        <div class="row">
            <!-- sidebar -->
            <div class="col-md-3 filter_sidebar">
              <div class="left_card_top_spacer"></div>
                <div class="spacer_big">
                   <span class="image-show"></span>
                   <span class="data"></span>
                </div>    
            </div>
            <!-- sidebar ends -->


            <div class="col-md-9">
                
                
                <div class="spacer">
                    <div class="row"> 

                      <div class="col-sm-4 col-sm-push-8">
                            <form class="form-inline sorting_form">
                            <!-- <div class="form-group">
                              <label class="control-label" for="sorting">Sort by:</label>
                            </div> -->
                            <div class="form-group">
                              <!-- <select id="sorting" class="selectpicker" data-live-search="true" title=""> -->
                                <select id="sorting" class="selectpicker">
                                <option>View all invitations</option>
                                <option>Another Option</option>
                              </select>
                            </div>
                          </form><!--/.sorting_form-->
                        </div>

                        <div class="col-sm-8 col-sm-pull-4">
                            <!-- tab menu -->
                            <div class="inbox_tabs_wrap"> 
                                <ul  class="nav nav-pills">
                                    <li class="active">
                                        <a  href="#1a" data-toggle="tab">Bookings</a>
                                    </li>
                                </ul>

                                            
                                  </div>
                            <!-- tab menu ends -->
                        </div>
                        
                    </div>
                </div><!--/.spacer -->

                <!-- table view --> 
                <div class="tab-content clearfix req_table_wrap">

                      <div class="tab-pane active" id="1a">
                      <?php if(count($requests) > 0){ foreach($requests as $row){  ?>
                      <div class="req_parent_row clearfix">
                      <div class="req_col_badge">
                        <!-- <span class="badge gold-badge">new</span> -->
                      </div> 
                      <div class="req_col_title">
                      <?php echo $row->name; ?>
                      </div> 
                      <div class="req_col_date">
                      <?php echo date('jS M, Y',strtotime($row->event_date)); ?>
                      </div>
                      <div class="req_col_time">
                       <!--  9:00 pm -->
                      </div>
                      <div class="req_col_detail" id="view-details" data-toggle="collapse" data-target="#collapsable_sent_req_one<?php echo $row->venue_id; ?>" data-id="<?php echo $row->venue_id; ?>">
                        <i class="fa fa-caret-right" aria-hidden="true"></i> view details
                      </div>
                      <div class="req_col_decision">
                        <button class="btn btn-success">
                          Booked
                        </button>
                      </div>
                      <div class="req_col_menudots">
                        <div class="dropdown">
                        <i class="fa fa-ellipsis-h dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>
                        <ul class="dropdown-menu dropdown-menu-right">
                          <li><a href="#">HTML</a></li>
                          <li><a href="#">CSS</a></li>
                          <li><a href="#">JavaScript</a></li>
                        </ul>
                      </div><!-- dropdown -->
                      </div>                   
                    </div><!-- req_parent_row -->

                    <div class="clearfix"></div>
                      <div id="collapsable_sent_req_one<?php echo $row->venue_id; ?>" class="collapse">
                        <div class="req_parent_row_collapse">
                          <div class="row">
                            <div class="col-sm-4">
                              <div class="event_info_card">
                                <h3>information</h3>
                                <ul>
                                  <li>Name: <strong><?php echo $row->name; ?></strong></li>
                                  <li>Event date: <strong><?php echo $row->event_date; ?></strong></li>
                                  <li>Set length: <strong><?php echo $row->start_time.' - '.$row->end_time; ?></strong></li>
                                  <li class="seperator">&nbsp;</li>
                                </ul>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="req_parent_row_collapse_space"></div>
                              <div class="spacer_medium">
                                <h3>contact</h3>
                                <p>
                                  Ben Masters
                                  <br/>
                                  Phone no: 318 884 4435
                                </p>
                              </div>
                              <div class="spacer_medium">
                                <h3>address</h3>
                                <p>
                                  <?php echo $row->location; ?>
                                </p>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="req_parent_row_collapse_space"></div>
                              <div class="spacer_medium">
                                <h3>notes</h3>
                                <p class="smallpara">
                                  <?php echo $row->message; ?>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div><!-- collapsable_row_one -->

                    <?php }}else{echo '<p style="text-align:center;">No booking found</p>';} ?>


                    </div>
                </div>
                <!-- table view ends -->

            </div>

        </div><!--/.row-->
    </div>
    <!-- content area ends -->


    <!-- extra space -->
    <div class="clearfix" style="height: 300px;"></div> 
    <!-- extra space ends-->


    <script src="<?php echo site_url(); ?>assets/js/pnotify.min.js"></script>

    <footer class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-left">
                    <p class="copyright">© 2017 Showspoon. All rights reserved.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                    <ul class="legallinks text-right">
                        <a href="#">Privacy Policy</a><span class="sep "> |</span>
                        <a href="#">Terms of Use </a><span class="sep "> |</span>
                        <a href="#">Contact Us</a>
                    </ul>
                </div>
            </div>
            <!-- row -->
        </div>
    </footer>


    <!-- for bootstrap select -->
    <script src="<?php echo site_url(); ?>assets/js/bootstrap-select.js"></script>
    <!-- for smooth scroll -->
    <script type="text/javascript">
        // Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
    </script>

    <!-- for back to top -->
    <script type="text/javascript">
     $(document).ready(function(){
     //   function getArtistRequestCount()
     //   {
     //      $.ajax({
     //                    url     :  '<?php echo site_url("venueBooking/getArtistRequestCount"); ?>',
     //                    type    :  'POST',
     //                    success :  function(data){

     //                        $('.request-count').html(data);
     //                    }
     //               });
     //   }
     //    var clear = setInterval(function(){
     //    getArtistRequestCount(); 
     //    },100);

     //   function getArtistNotification(){
     //     $.ajax({
     //                    url     :  '<?php echo site_url("venueBooking/getArtistNotification"); ?>',
     //                    type    :  'POST',
     //                    success :  function(data){

     //                        $('#alert').html(data);
     //                    }
     //               });
     //  }
     //  // getVenueNotification();

     //  var clear = setInterval(function(){
     //    getArtistNotification(); 
     //  },100);
      

     //  $(document).on('click','.close',function(){
     //    var id = $(this).data('id');
     //    $.ajax({
     //                    url     :  '<?php echo site_url("venueBooking/deleteArtistNotification"); ?>',
     //                    type    :  'POST',
     //                    data    :  {id:id},
     //                    success :  function(data){

     //                        clearInterval(clear);
     //                    }
     //               });
     //  });

     //  function artistRequest(page){
     //    $.ajax({
     //                    url     :  "<?php echo site_url(); ?>venueBooking/getArtistRequests/"+page,
     //                    method  :  "GET",
     //                    dataType:  "JSON",
     //                    success :  function(data){

     //                        $('#artist_request').html(data.data);
     //                        $('#pages').html(data.links);
     //                    }
     //               });
     //  }

     //  artistRequest(1);  
      

     // $(document).on("click", ".pagination li a", function(event){
     //  event.preventDefault();
     //  var page = $(this).data("ci-pagination-page");
     //  artistRequest(page);
     // });


     //  // setInterval(function(){
     //  //   artistRequest();
     //  // },2000);

      $('#view-details').click(function(){

             var id = $(this).data('id'); 

             $.ajax({
                        url     :  '<?php echo site_url("artistBooking/get_venue_booked_image"); ?>',
                        type    :  'POST',
                        data    :  {id:id},
                        success :  function(data){
                            $('#label' + id).remove();
                            $('.data').hide();
                            $('.image-show').html(data);
                        }
                   });
        });

      $(document).on('click','.status',function(){

          var status = $(this).data('status');
          $.ajax({
            url     : '<?php echo site_url("venueBooking/btn_status"); ?>',
            type    : 'POST',
            data    : {status:status},
            success : function(data){
              $('#'+status).hide();
              $('#accept_btn'+status).hide();
              $('.accept-btn'+status).html('<a href="#" class="action_btn_white req_accepted">Accepted</a>');
              $('.tmp-btn' + status).html('<button class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> accepted </button>'); 
            }
          });
      });


      function hide(){ 
          $.ajax({
            url     : '<?php echo site_url(); ?>artistBooking/artist_booked_image',
            type    : 'POST',
            data    : {type:2,id:"<?php echo $row->user_id; ?>"},
            success : function(data){ 
              $('.data').html(data);
            }
          });
        }
        window.onload = hide();

     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('.back-to-top').fadeIn();
            } else {
                $('.back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('.back-to-top').click(function () {
            $('.back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('.back-to-top').tooltip('show');

});
    </script>

    <!-- for ekko lightbox  -->
    <script src="assets/js/ekko-lightbox.js"></script>
    <script type="text/javascript">
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });
    </script>

    <!-- for flexslider -->
    <!-- FlexSlider -->
  <script defer src="<?php echo site_url(); ?>assets/js/jquery.flexslider.js"></script>

  <script type="text/javascript">
    $(function(){
      SyntaxHighlighter.all();
    });
    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>
<!-- for header shrink -->
    <script type="text/javascript">
        $(document).on("scroll", function(){
        if
          ($(document).scrollTop() > 100){
              $(".main-header").addClass("shrink");
            }
            else
            {
                $(".main-header").removeClass("shrink");
            }
        });
    </script>

  <!-- for message modal  -->


  <!-- Modal -->
  <div class="modal fade message_modal" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="clearfix member_box">
                <figure>
                    <img src="<?php echo site_url(); ?>assets/images/male.jpg" alt="Member">
                </figure>
                <figcaption>
                   Band Name Here
                </figcaption>
            </div>
        </div>
        <div class="modal-body">
          <form class="">
              <textarea class="form-control" placeholder="Write your message here..."></textarea>
              <div class="pull-right">
                  <input type="submit" name="submit" value="Send Message" class="profile_save_btn_small">
              </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


</body>

</html>
