<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class notification extends MY_Controller {

    private $loggedintime;
    private $user_data;
    private $data;

    function __construct() {

        parent::__construct();


        $this->data['page_title'] = "";
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model(array('Account_model','Artist_model','Venue_model','Booking_model','Notification_model'));
        //$this->session->set_userdata('showspoon_artist.is_complete',1);
        $this->data['session_user']=$this->user_data;
    }

    public function index() {





        //$this->load->view('venue/dashboard', $this->data);
    }
    
    public function get_notifications(){
         
        $this->load->model('Notification_model');
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
      
        

        if($user_id>0){
       
            $filter=[
                
                'user_id'=>$user_id,

            ];

            $html='';

            $chat = $this->Notification_model->get_artist_notifications($filter);

            //dd($chat);
            $counter=0;
            if(count($chat)>0){
                foreach($chat as $row){
                    if($counter==0){
                        //$this->session->set_userdata('last_id',$row->id);
                        $_SESSION['last_id']=$row->id;
                    }
                    if($row->from==$user_id){
                        $chat=(array)$row;
                        $chat['session_user']=$this->user_data;
                        //$html .= $this->load->view('includes/chat-left',['chat'=>$chat,'session_user'=>$this->user_data],true);
                    }else{
                        $chat=(array)$row;
                        $chat['session_user']=$this->user_data;
                        $html .= $this->load->view('includes/chat-right',['chat'=>$chat,'session_user'=>$this->user_data],true);
                    }
                    $counter++;
                }
             
            }
            // $last_id = $this->session->userdata('last_id')?$this->session->userdata('last_id'):0;

            echo json_encode(array('data'=>$html,'last_id'=>$last_id));
            exit();
        }
        echo json_encode(array('data'=>false));
    }

    
}

