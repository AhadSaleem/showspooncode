<!DOCTYPE html>
<!-- saved from url=(0049)http://165.227.110.185/showspoon/artist/dashboard -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Showspoon</title>
    <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/custom-develop.css" rel="stylesheet">
    <!-- new wireframe style -->
    <link href="<?php echo base_url(); ?>assets/css/custom_style.css" rel="stylesheet">
    <!-- for bootstrap live search -->
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.css"> -->
    <!-- for ekko lightbox -->
    <link href="<?php echo base_url(); ?>assets/css/ekko-lightbox.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- for flexslider -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flexslider.css" type="text/css" media="screen" />




    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>
    <style>
    .navbar-toggle{background:none !important;border:1px solid #ab7b15 !important;}
    .navbar-toggle > .icon-bar{background:#ab7b15 !important;}
    .spacer_medium {
     margin: 0 0 90px 0;
     }
    </style>
    <!-- for Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo site_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url(); ?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo site_url(); ?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo site_url('assets/favicon/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">
    <!--<script src="http://165.227.110.185/showspoon//assets/dist/js/bootstrap_multiselect.js"></script>-->
</head>

<body class="no-body-padd">

    <header class="main-header">
      <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">

        <!-- navbar for login link only -->
        <div class="clearfix">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
          <div class="float-left">
            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <img src="<?php echo site_url('assets/images/logo-n.png'); ?>" alt="Logo">
            </a>
          </div>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <div class="float-center">
          <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo site_url('about'); ?>">About</a></li>
          <li><a href="<?php echo site_url('showspoon-works'); ?>">How Showspoon Works</a></li>
          <li><a href="<?php echo site_url('contact'); ?>">Contact Us</a></li>
          <li><a href="<?php echo site_url('account/login'); ?>">Login</a></li>
          </ul>
          <!-- <div class="float-right">
            <a class="nav-btn" href="<?php echo base_url(); ?>account/login">Login</a>
          </div> -->
        </div>
      </div>
    </div>
        <!-- navbar for login link only ends -->
        
        <!-- <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo site_url(); ?>">
              <img src="<?php echo site_url(); ?>assets/images/logo-n.png" alt="Logo">
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo base_url(); ?>Account/login">Login</a></li>
          </ul>
        </div> -->

      </div>
    </nav>
</header> 

    <div class="spacer_medium"></div>
    <!-- space just for index ends-->

    <div class="container">
      <h2>Showspoon terms of use</h2>
      <p class="text-justify">
       These Terms of Service (“Terms“) govern your use of the services, software and websites (the “Service”) provided by Showspoon AS. (“Showspoon”). Our Privacy Policy explains the way we collect and use your information. By using the Service you agree to be bound by these Terms and our Privacy Policy. If you’re using our Service on behalf of an organization or entity (“Organization”), then you are agreeing to these Terms on behalf of that Organization and you represent and warrant that you have the authority to bind the Organization to these Terms. In that case, “you” and “your” refers to that Organization.
       </p>
       <p class="text-justify">
        Your Content The Service allows you to create tasks and associated information, text, files and other materials (together the “Content”) and to share that Content with others. You retain ownership of your Content, but by uploading it onto the Service, you are granting us a license to use, copy, reproduce, process, adapt, publish, transmit, host and display that Content for the purpose of (i) providing you the Service and associated support; and (ii) analyzing and improving the operation of the Service.
       </p>
       <p class="text-justify">
        We reserve the right to remove Content on the Service that violates our Acceptable Use Policy or these Terms or that we otherwise reasonably believe may create liability for Showspoon.
       </p>
       <p class="text-justify">
        Your Obligations You must provide accurate information when you create your Showspoon account. You are responsible for safeguarding the password and for all activities that occur under your account. You should notify Showspoon immediately if you become aware of any breach of security or unauthorized use of your account. You must comply with our Acceptable Use Policy at all times when using the Service. You may never use another user’s account without permission. You may not disassemble, decompile or reverse engineer the Service or attempt or assist anyone else to do so, unless such restriction is prohibited by law. Our Service is not intended for use by persons under the age of 13. By using the Service you are representing to us that you are over the age of 13.
       </p>
       <p class="text-justify">
        Your Use of Showspoon Software As part of the Service, we provide downloadable client software (the “Software”) for your use in connection with the Service. This Software may update automatically and if such Software is designed for use on a specific mobile or desktop operating system, then a compatible system is required for use. So long as you comply with these Terms, we grant you a limited, nonexclusive, nontransferable, revocable license to use the Software, solely to access the Service; provided, however, that this license does not constitute a sale of the Software or any copy thereof, and as between Showspoon and You, Showspoon retains all right, title and interest in the Software. If you are using our API to develop an application based on our Service, you will be subject to our API Terms.
       </p>
       <p class="text-justify">
        Your Use of Third Party Applications If you elect to utilize any third party application in connection with your use of the Service, by doing so you are consenting to your Content being shared with such third party application. To understand how such third party application provider utilizes your Content and other information, you should review their privacy policy.
       </p>
       <p class="text-justify">
        Security We will use industry standard technical and organizational security measures in connection with the storage, processing and transfer of your Content that are designed to protect the integrity of that Content and to guard against unauthorized or unlawful access to, use of, or processing of such Content.
       </p>
       <p class="text-justify">
        Termination You are free to stop using our Service at any time. We also reserve the right to suspend or end the Service at any time at our discretion and without notice. We may also terminate or suspend your access to the Service at any time if you are not complying with these Terms or if you are using the Service in a manner that we believe may cause us financial or legal liability.
       </p>
       <p class="text-justify">
        Showspoon's Intellectual Property Rights The Service (excluding Content provided by users) constitutes Showspoon's intellectual property and will remain the exclusive property of Showspoon. Any feedback, comments, or suggestions you may provide regarding the Service is entirely voluntary and we will be free to use such feedback, comments or suggestions as we see fit and without any obligation to you.
       </p>
       <p class="text-justify">
        Copyright Showspoon respects the intellectual property rights of others and we expect our users to do the same. We respond to notices of alleged copyright infringement if they comply with the law, and such notices should be reported to our Copyright Agent using the process set forth in our DMCA Policy.
       </p>
       <p class="text-justify">
        Modifications to the Service The Service may be modified from time to time, often without prior notice to you. Your continued use of the Service constitutes your acceptance of such modifications. If you are not satisfied with a modification we make to the Service, your sole remedy is to terminate your use of the Service.
       </p>
       <p class="text-justify">
        Indemnification You agree to defend, indemnify and hold harmless Showspoon and its licensee and licensors, and their employees, contractors, agents, officers and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney’s fees) arising from your use of and access to the Service or the Software, or from or in connection with any Content uploaded to the Service through your account by a third party using your account with your knowledge or consent.
       </p>
       <p class="text-justify"> 
        No Warranty THE SERVICE IS PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS WITHOUT ANY WARRANTY OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SHOWSPOON IS NOT RESPONSIBLE FOR ANY THIRD PARTY CONTENT THAT YOU DOWNLOAD OR OTHERWISE OBTAIN THROUGH THE USE OF THE SERVICE OR FOR ANY DAMAGE OR LOSS OF DATA THAT MAY RESULT. WE DO NOT WARRANT, ENDORSE, GUARANTEE, OR ASSUME RESPONSIBILITY FOR ANY THIRD PARTY APPLICATION OR SERVICE THAT PROVIDES ACCESS TO OUR SERVICE (E.G., ANY THIRD PARTY APPLICATION DEVELOPED USING SHOWSPOON'S API).
       </p>
       <p class="text-justify">
        Limitation of Liability TO THE MAXIMUM EXTENT PERMITTED BY LAW, IN NO EVENT SHALL SHOWSPOON, ITS AFFILIATES, AGENTS, DIRECTORS, EMPLOYEES OR SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING WITHOUT LIMITATION DAMAGES FOR LOSS OF PROFITS, GOODWILL, DATA OR OTHER INTANGIBLE LOSSES THAT RESULT FROM THE USE OF, OR INABILITY TO USE, THE SERVICE, WHETHER OR NOT SHOWSPOON HAS BEEN WARNED OF THE POSSIBILITY OF SUCH DAMAGES AND EVEN IF A REMEDY FAILS OF ITS ESSENTIAL PURPOSE.
       </p>
       <p class="text-justify">
        Dispute Resolution Informal Efforts - You agree that prior to filing any claim against Showspoon relating to or arising out of these Terms you will first contact us at support@showspoon.com to provide us with an effort to resolve the issue in an informal manner.
       </p>
       <p class="text-justify">
        No Class Actions - ALL CLAIMS MUST BE BROUGHT ON AN INDIVIDUAL BASIS AND NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY PURPORTED CLASS, CONSOLIDATED, OR REPRESENTATIVE PROCEEDING. CLASS ARBITRATIONS, CLASS ACTIONS, PRIVATE ATTORNEY GENERAL ACTIONS, AND CONSOLIDATION WITH OTHER ARBITRATIONS ARE NOT ALLOWED.
       </p>
       <p class="text-justify">
        Judicial Forum - In the event that the agreement to arbitrate is found not to apply to your claim, then you and Showspoon agree that any judicial proceedings will be brought in the district court of Oslo, Norway. and both parties consent to venue and personal jurisdiction there.
       </p>
       <p class="text-justify">
        Governing Law These Terms will be governed by the laws of the State of California, without regard to its conflict of laws principles. The application of the United Nations Convention on Contracts for the International Sale of Goods is expressly excluded.
       </p>
       <p class="text-justify">
        Data Transfer In connection with providing you the Service Showspoon may transfer, store and process your Content in Norway or in any other country in which Showspoon or its agents maintain facilities. By using the Service you consent to this transfer, processing and storage of your Content.
       </p>
       <p class="text-justify">
        Entire Agreement; Severability; Waiver These Terms constitute the entire agreement between you and Showspoon concerning the Service replace any prior or contemporaneous agreements, terms or conditions applicable to your use of the Service. If a provision of these Terms is found to be unenforceable, the remaining provisions of these Terms will remain in full force and effect and an enforceable term will be substituted reflecting as closely as possible our original intent. Showspoon's failure to enforce any provision of these Terms shall not be deemed a waiver of its right to do so later.
       </p>
       <p class="text-justify">
        Assignment These Terms and any rights and licenses granted hereunder, may not be transferred or assigned by you, but may be assigned by us without restriction. Any attempted transfer or assignment by you will be null and void.
       </p>
       <p class="text-justify">
        Notices We may provide you with legal notices and notices related to your account via email using the email address associated with your account. We may provide you with other marketing or business-related information, including information about Service updates or changes via e-mail.


      </p>
    </div>



    
<div class="spacer_hundred"></div>
<?php $this->load->view('includes/footer');?>

    


    <script type="text/javascript">
        $(document).on("scroll", function(){
        if
          ($(document).scrollTop() > 20){
              $(".main-header").addClass("shrink");
            }
            else
            {
                $(".main-header").removeClass("shrink");
            }
        });
    </script>
    
</body>

</html>