<?php
$chat = (object)$chat;
$session_user = isset($session_user)?(object)$session_user:[];
$user_type = isset($session_user->type_id)?$session_user->type_id:0;
$user_image=site_url('assets/dist/images/noimage2.jpg');

//dd($chat,false);

if(isset($chat->profile_image) && $chat->profile_image!='' && $user_type==1){

    $user_image=site_url('uploads/users/thumb/'.$chat->profile_image);
}elseif(isset($chat->profile_image) && $chat->profile_image!='' && $user_type==2){

    $user_image=site_url('uploads/users/thumb/'.$chat->profile_image);
}
$sent = isset($chat->sent)?$chat->sent:date('Y-m-d H:i:s',time());
$update_event = isset($chat->update_event)?$chat->update_event:0;
//dd($chat,false);
?>
<li class="message right appeared">
    <div class="avatar"><img src="<?php echo $user_image;?>" class=" img-responsive  img-circle"></div>
    <div class="text_wrapper">
        <div class="text"><?php if($update_event==1){
    if($session_user->type_id==$booking->type){
         echo '<h5 class="m-t-none"><strong>You have update the event</strong></h5>';
    }else{
         echo '<h5 class="m-t-none"><strong>Event has been updated.</strong></h5>';
    }
    
               
                               
}?><?php echo isset($chat->message)?nl2br($chat->message):'';?></div>
    </div>
    <time datetime="2009-11-13T20:00"><?php echo date('jS M, Y h:i A',strtotime($sent));?></time>
</li>
<div class="row msg_container base_sent hidden">
    <div class="col-xs-11 col-md-11 no-gutter">
        <div class="messages msg_sent">
              <p><?php echo isset($chat->message)?nl2br($chat->message):'';?></p>
            <time datetime="2009-11-13T20:00"><?php echo date('jS M, Y h:i A',strtotime($sent));?></time>
        </div>
    </div>
    <div class="col-md-1 col-xs-1 avatar no-gutter">
        <img src="<?php echo $user_image;?>" class=" img-responsive">
    </div>
</div>