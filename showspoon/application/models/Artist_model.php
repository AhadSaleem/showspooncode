<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class artist_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->helper('site');
    }
    private $artist=['email','short_description','rating','profile_image','artist_company_name','created_datetime','registration_no','is_complete','is_active','user_id','is_delete','city_id','name','genre','band_type_id'];

    private $artist_details=['zip','biography','location','contact_person','mobile','phone','payment_account','facebook','twitter','instragram','artist_id'];
    private $artist_member=['name','role','alias','image_url','artist_id'];
    private $artist_gallery=['artist_id','set_featured','url'];
    private $artist_media=['artist_id','type','url','caption','is_active','video_data'];
    public function get_artists($data=[],$offset=0,$limit=10,$order='artist_id',$sort='DESC') {
        $where='';
        if(isset($data['city_id']) && is_numeric($data['city_id'])){
            $where .= " and city_id=".(int)$data['city_id']." ";
        }
        if(isset($data['band_type_id']) && is_numeric($data['band_type_id'])) {
            $where .= " and band_type_id=".(int)$data['band_type_id']." ";
        }
        if(isset($data['genre']) && is_array($data['genre'])){
            if(count($data['genre'])>0){
                $where .= " and (";
                foreach($data['genre'] as $key=>$val){
                    if($key==0){
                        $where .= " FIND_IN_SET($val,genre)>0 ";            
                    }else{
                        $where .= " OR FIND_IN_SET($val,genre)>0 ";            
                    }
                }
                $where .= " ) ";
            }
        }
        $query=$this->db->query("select * from artist where is_delete=0 and is_active=1 and is_complete>0 $where order by $order $sort limit $offset,$limit");
        return $query->result();
    }
    public function update_bid($array,$user_id)
    {
        $this->db->where('user_id',$user_id)->update('users',$array);
    }
    public function check_bid($user_id)
    {
        $this->db->select('bid');
        $this->db->where('user_id',$user_id);
        $sql = $this->db->get('users');
        return @$sql->row()->bid;
    }
    public function step1($user_id,$step)
    {
        $this->db->where('user_id',$user_id)->update('users',array('bid'=>$step));
    }
    public function step2($user_id,$step)
    {
        $this->db->where('user_id',$user_id)->update('users',array('bid'=>$step));
    }
    public function step3($user_id,$step)
    {
        $this->db->where('user_id',$user_id)->update('users',array('bid'=>$step));
    }
    public function step4($user_id,$step)
    {
        $this->db->where('user_id',$user_id)->update('users',array('bid'=>$step));
    }
    public function get_artists_count($data=[]) {
        $where='';
        if(isset($data['city_id']) && is_numeric($data['city_id'])) {
            $where .= " and city_id=".(int)$data['city_id']." ";
        }
        if(isset($data['band_type_id']) && is_numeric($data['band_type_id'])) {
            $where .= " and band_type_id=".(int)$data['band_type_id']." ";
        }
        if(isset($data['genre']) && is_array($data['genre'])) {
            if(count($data['genre'])>0){
                $where .= " and (";
                foreach($data['genre'] as $key=>$val){
                    if($key==0){
                        $where .= " FIND_IN_SET($val,genre)>0 ";            
                    }else{
                        $where .= " OR FIND_IN_SET($val,genre)>0 ";            
                    }
                }
                $where .= " ) ";
            }
        }
        $query=$this->db->query("select count(artist_id) as total from artist where is_delete=0 and is_complete>0 and is_active=1 $where order by artist_id asc");
        $row = $query->row();
        return isset($row->total)?$row->total:0;
    }
    public function get_artist_details($id=0) {

        $query=$this->db->query("select *,A.artist_id as row_id, AD.zip,AD.biography from artist A
        left join artist_details AD on A.artist_id = AD.artist_id
        where A.is_delete=0 and A.is_active=1 and A.user_id=".(int)$id." limit 1");
        return $query->row();
    }
    public function get_artist_gallery_count($id)
    {
        $query="select COUNT(*) AS cnt from artist_gallery where artist_id=".(int)$id." order by id asc";
        $query=$this->db->query($query);
        return $query->row();
    }
     public function count_all($id)
     {
      $query = $this->db->where('artist_id',$id)->get("artist_gallery");
      return $query->num_rows();
     }
     public function get_artist_galleryy($id=0,$limit, $start) {
        
        $this->db->select("*");
        $this->db->from("artist_gallery");
        $this->db->where(array('artist_id'=>$id));
        $this->db->order_by("id", "ASC");
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        $gallery = $query->result();
        $output = "";
        $output .= '<ul id="lightgallery" class="row image-gallery">';
        if(isset($gallery) && count($gallery)>0){ 
         foreach($gallery as $gal){
         if(isset($gal->url) && $gal->url!='' && file_exists('./uploads/gallery/thumbs/'.$gal->url)){
         $user_image=site_url('uploads/gallery/thumbs/'.$gal->url);
         $full_image=site_url('uploads/gallery/'.$gal->url);
        
        $output .= '<li class="col-xs-6 col-sm-4 col-md-3"  data-src="'.$user_image.'" data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
        <a href="'.site_url('uploads/gallery/thumbs/'.$gal->url).'" data-lightbox="example-set" data-title="Click the right half of the image to move forward.">
            <img class="img-responsive" src="'.site_url('uploads/gallery/thumbs/'.$gal->url).'">
        </a>
        </li>';
        }
           }
        } 
        else
        {
            $output .= '<p class="text-center">No gallery found.</p>';
        }

        $output .= '</ul>';
        return $output;
    }
    public function get_artist_video_count($id)
    {
        $query="select COUNT(*) AS vd_cnt from artist_media where artist_id=".(int)$id." AND type='YouTube' order by id asc";
        $query=$this->db->query($query);
        return $query->row();
    }
     public function count_videos($id)
     {
      $query = $this->db->where(array('artist_id'=>$id,'type'=>'YouTube'))->get("artist_media");
      return $query->num_rows();
     }
     public function get_artist_video($id=0,$limit, $start)
     {
        $this->db->select("*");
        $this->db->from("artist_media");
        $this->db->where(array('artist_id'=>$id,'type'=>'YouTube'));
        $this->db->order_by("id", "ASC");
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        $videos = $query->result();
        $output = "";
        $output .= '<ul id="lightgallery" class="row image-gallery">';
        $output .= '<div role="tabpanel" class="tab-pane" id="videos">';
                     if(isset($videos) && count($videos)>0){
                     foreach($videos as $vid){
                        $video_data = isset($vid->video_data)?$vid->video_data:'';
                        if($video_data!=''){
                            $video_data=json_decode($vid->video_data);
                        }
                        if(isset($video_data->html) && $video_data->html!='')
                        {
                        $output .= '<div class="col-md-4 col-sm-6 col-xs-12 m-b-md">
                            <div class="embed-responsive embed-responsive-16by9">'.$video_data->html.'</div>
                        </div>';

                        }}
                        }else{
                        $output .= '<p class="text-center">No video found.</p>';
                     }
         
        $output .= '</div></ul>';
        return $output;
     }
     public function get_featured_artist(){
        $where = '';
        $q = $this->db->query("select receiver_id from rating");
        $row = $q->result();
        if(count($row) > 0 && $row!=""){
        $where .= " and ( ";
        foreach($row as $key=>$val){
            if($key==0){
                $where .= " R.receiver_id=$val->receiver_id ";
            }else{
                $where .= " OR R.receiver_id=$val->receiver_id";
            }
        }
        $where .= " ) ";
        }
        $sql = $this->db->query("select R.*, FORMAT(AVG(R.rating),1) as avg_rate,A.artist_id as artist_id,A.name as artist_name,A.profile_image as profile_image from rating R inner join artist A on A.user_id=R.receiver_id where R.type=2 $where group by R.receiver_id order by avg_rate DESC");
        return $sql->result();
    } 
    public function get_similar_vanues($explode)
    {
        $where = "";
        if(isset($explode)) {
            if(@count($explode)>0){
                $where .= " and (";
                    foreach($explode as $key=>$val){
                        if($key==0){
                            $where .= " FIND_IN_SET($val,genre)>0 ";            
                        }else{
                            $where .= " OR FIND_IN_SET($val,genre)>0 ";            
                        }
                    }
                $where .= " ) ";
            }
        }
        $query=$this->db->query("select * from artist where is_delete=0 and is_active=1 and is_complete>0 $where order by artist_id asc");
        return $query->result();
    }
    public function get_recently_booked($id=0)
    {
        $query = $this->db->query("select * from bookings inner join artist on (artist.user_id = bookings.from_id )
        where bookings.is_delete=0 and bookings.is_active=1 and bookings.is_complete=1 and bookings.from_id=".(int)$id."");
        return $query->result();
    }
    public function get_artist_details_by_id($id=0) {
        $query = "select *,A.artist_id as row_id from artist A
        left join artist_details AD on A.artist_id = AD.artist_id inner join cities C on C.CityID=A.city_id
        where A.is_delete=0 and A.is_active=1 and A.artist_id=".(int)$id." limit 1";
        $query=$this->db->query($query);
        return $query->row();
    }
    
    public function get_artist_members($id=0) {
        $query="select * from artist_member where artist_id=".(int)$id." and is_active=1 order by id asc";
        $query=$this->db->query($query);
        return $query->result();
    }
    public function get_artist_gallery($id=0) {
        $query="select * from artist_gallery where artist_id=".(int)$id." order by id asc";
        $query=$this->db->query($query);
        return $query->result();
    }
    public function get_artist_videos($id=0,$types=[]) {
        $where='';

        if(count($types)>0){
            $where .=' and ( ';
            foreach($types as $key=>$row){
                if($key==0){
                    $where .= " type='".$row."' ";
                }else{
                    $where .= " or type='".$row."' ";    
                }

            }
            $where .=' ) ';
        }
        $query="select * from artist_media where artist_id=".(int)$id." $where order by id asc";

        $query=$this->db->query($query);
        return $query->result();
    }


    public function get_artist_media($id=0,$type='') {
        $where='';
        if($type!=''){
            $where .= " and  video_type='".$type."' ";
        }
        $query="select * from artist_media where artist_id=".(int)$id." and is_active=1 $where order by id asc";
        $query=$this->db->query($query);
        return $query->result();
    }
    public function get_artist_favorties($id=0,$type='') {

        $query="select * from favorties where user_id=".(int)$id."  order by id asc";
        $query=$this->db->query($query);
        return $query->result();
    }

    public function get_genres(){
        $data=[];
        $query = $this->db->query("select * from genre order by name asc");
        $results = $query->result();
        if(count($results)>0){
            foreach($results as $row){
                $data[$row->genre_id]=$row->name;
            }
        }
        return $data;
    }
    public function get_band_types(){
        $data=[];
        $query = $this->db->query("select * from artist_band_type order by name asc");
        $results = $query->result();
        if(count($results)>0){
            foreach($results as $row){
                $data[$row->id]=$row->name;
            }
        }
        return $data;
    }

    public function ci_custom_pagination($url, $total_rows, $per_page, $uri_segment) {


        //pagination settings
        $config['base_url'] = $url;
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $per_page;
        $config["uri_segment"] = $uri_segment;
        $choice = $config["total_rows"] / $config["per_page"];
        //$config["num_links"] = floor($choice);
        $config["num_links"] = 4;
        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination pagination-md">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&larr;';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&rarr;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        return $config;
    }

    public function create_artist($data=[]) {

        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->artist)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0){
            $this->db->insert('artist',$insert);
            return $this->db->insert_id();
        }
        return false;


    }

    public function update_artist($data=[],$id=0) {  
        $this->db->where('user_id',$id)->update('artist',$data);
        // $id=0;
        // if(isset($data['user_id']) && $data['user_id']>0){
        //     $id=$data['user_id'];
        // }
        // $insert=[];
        // foreach($data as $key=>$val){
        //     if(isset($data[$key]) && in_array($key,$this->artist)){
        //         $insert[$key]=$data[$key];
        //     }
        // } 
        // if(count($insert)>0){ 

        //     return $this->db->where('user_id',$id)->update('artist',$insert);

        // }
        // return false;


    }
    public function create_artist_details($data=[]) {

        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->artist_details)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0){
            $this->db->insert('artist_details',$insert);
            return $this->db->insert_id();
        }
        return false;


    }
    public function update_artist_details($data=[],$id=0) {
        return $this->db->where('artist_id',$id)->update('artist_details',$data);
        // $id=0;
        // if(isset($data['artist_id']) && $data['artist_id']>0){
        //     $id=$data['artist_id'];
        // }
        // $insert=[];
        // foreach($data as $key=>$val){
        //     if(isset($data[$key]) && in_array($key,$this->artist_details)){
        //         $insert[$key]=$data[$key];
        //     }
        // }
        // if(count($insert)>0){

        //     return $this->db->where('artist_id',$id)->update('artist_details',$insert);

        // }
        // return false;


    }

    public function create_member($data=[]) {

        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->artist_member)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0){
            $this->db->insert('artist_member',$insert);
            return $this->db->insert_id();
        }
        return false;


    }
    public function update_member($data=[]) {

        $id=0; 
        if(isset($data['id']) && $data['id']>0){
            $id=$data['id'];
        }
        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->artist_member)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0){
            $this->db->where('id',$id)->update('artist_member',$insert);
            return true;
        }
        return false;


    }
    public function get_member_by_id($id=0) {
        $query="select * from artist_member where id=".(int)$id." limit 1";
        $query=$this->db->query($query);
        return $query->row();
    }
    public function get_gallery_by_id($id=0) {
        $query="select * from artist_gallery where id=".(int)$id." limit 1";
        $query=$this->db->query($query);
        return $query->row();
    }
    public function get_video_by_id($id=0) {
        $query="select * from artist_media where id=".(int)$id." limit 1";
        $query=$this->db->query($query);
        return $query->row();
    }
    public function add_picture($data=[]) {
         
         $this->db->insert('artist_gallery',$data);
         return $this->db->insert_id();

        // $insert=[];
        // foreach($data as $key=>$val){
        //     if(isset($data[$key]) && in_array($key,$this->artist_gallery)){
        //         $insert[$key]=$data[$key];
        //     }
        // }
        // if(count($insert)>0){
        //     $this->db->insert('artist_gallery',$insert);
        //     return $this->db->insert_id();
        // }
        // return false;


    }
    public function create_video($data=[]) {

        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->artist_media)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0){
            $this->db->insert('artist_media',$insert);
            return $this->db->insert_id();
        }
        return false;


    }

    //Recent work
    public function get_artist_lat_long($user_id)
    {
        $query = $this->db->query("SELECT * FROM artist WHERE user_id = ".(int)$user_id."");
        return $query->row();
    }
    public function getVenueRadius($radius,$lat,$long)
    {
        $query = "SELECT *, ( 3959 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) 
        * cos( radians( longitude ) - radians($long) ) + sin( radians($lat) ) * sin(radians(latitude)) ) ) AS distance 
        FROM venues 
        HAVING distance < $radius
        ORDER BY distance";
        $query = $this->db->query($query);
        return $query->result();
    } 

    public function getALLArtist($offset,$per_page,$id)
    {
        $this->db->select("*");
        $this->db->from("artist");
        $this->db->where(array('artist.is_delete'=>0,'artist.is_active'=>1));
        $this->db->order_by("artist.artist_id", "ASC");
        $this->db->limit($offset,$per_page);
        $query = $this->db->get();
        $result = $query->result();
        $sql = $this->db->query("select favourites from users where users.user_id=$id");
        $obj = $sql->row()->favourites;
        $arr = explode(',',$obj);
        $output = "";
        $output .= '<div class="ListView search-listing" style="padding-bottom:0;"><div class="showAjaxVenues"></div>
                    <div class="row listing_wrap">';
        foreach($result as $venue) { 
        $like_active='';        
        //$user_favourites = isset($user_favourites)?$user_favourites:[];
        $artist_id = isset($venue->artist_id)?$venue->artist_id:0;
        if(in_array($artist_id,$arr)){
            $like_active='active';   
        }
        $output .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="" style="height:330px;">
                            <div class="panel panel-default">
                                <div class="panel-body no-padder">
                                    <div class="gimage">
                                        <a href="javascript:;" class="likebtn '.$like_active.'" data-id="'.$venue->artist_id.'">
                                            <i class="fa fa-heart-o"></i>
                                            <i class="fa fa-heart"></i>
                                        </a>

                                        <a href="'.site_url().'venue/details/'.$venue->artist_id.'">
                                            <img src="'.site_url().'uploads/users/thumb/'.$venue->profile_image.'" class="img-responsive center-block">
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body wrapper-xs text-center">
                                <h4 class="text-center"><a href="'.site_url().'venue/details/'.$venue->artist_id.'">'.$venue->name.'</a></h4>';

                    $names = array();
                    $explode = explode(',',$venue->genre); 
                    foreach($explode as $exp)
                    {   
                        $exp_names = $this->Artist_model->getGenreNames($exp);
                        foreach($exp_names as $exp_name)
                        {
                            $names[] = $exp_name->name;
                        }
                    }
                    $query = $this->db
                             ->select('Format(AVG(rating),1) AS avg')
                             ->from('rating')
                             ->where(array('receiver_id'=>$venue->user_id,'type'=>2))
                             ->get();
                    $row = $query->row();
                    if(count($row) > 0){$rating = $row->avg;}else{$rating = 0.0;}
        $output .=   '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';   
        $output .=   '<div class="col-sm-12 text-center ratingstars">
                                        '.star_rating($rating).'';
        if($row->avg!="" && count($row->avg) > 0){
            $rate = explode('.',$row->avg);
            $output .= '<span> ('.$rate[0].')</span>';
        }
        $output .=   '</div>';
        $output .=   '<a href="'.site_url().'venueBooking/book/'.$venue->artist_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                                </div>
                            </div>
                        </div>';
                         } 
        $output .=  '</div></div>';
        return $output;
    }
    public function _getrate($id){
        $query = $this->db
                 ->select('Format(AVG(rating),1) AS avg')
                 ->from('rating')
                 ->where(array('receiver_id'=>$id,'type'=>1))
                 ->get();
        return $query->row();
    }

    public function getCount()
    {
        $query = $this->db->get('venues');
        return $query->num_rows();
    }
    public function getArtistCount()
    {
        $query = $this->db->get('artist');
        return $query->num_rows();
    }
    public function getGenre()
    {
        $query = $this->db->query("select * from genre");
        return $query->result();
    }
    public function getGenreCount()
    {
        $query = $this->db->query("select COUNT(*) AS count from genre");
        return $query->row();
    }
    public function getCityCount()
    {
        $query = $this->db->query("select COUNT(*) AS city from cities");
        return $query->row();
    }
    public function getAllGenre()
    {
        $query = $this->db->query("select * from genre");
        return $query->result();
    }
    public function getCities()
    {
        $query = $this->db->query("select * from cities");
        return $query->result();
    }
    public function getVenues()
    {
        $query=$this->db->query("select * from venues where venues.is_delete=0 and venues.is_active=1 and venues.is_complete>0 order by venues.venue_id DESC limit 0,10");
        return $query->result();
    } 

    public function get_capacity($start2,$limit2)
    {
        $query = $this->db->query("select * from venues limit $start2 , $limit2");
        return $query->result();
    }
    public function getVenueDetails()
    {
        $query = $this->db
                 ->select('*')
                 ->from('venue_details')
                 ->order_by('capacity','asc')
                 ->limit(5)
                 ->get();
        return $query->result();
    } 
    
    public function getArtistDetails()
    {
        $query = $this->db
                 ->select('*')
                 ->from('artist_details')
                 ->order_by('capacity','asc')
                 ->limit(5)
                 ->get();
        return $query->result();
    } 
    public function getGenreNames($id)
    {
         $query = $this->db
                  ->select('*')
                  ->from('genre')
                  ->where('genre_id',$id)
                  ->get();
        return $query->result();
    }
    public function search_count($search_value){
        $query = $this->db->query("select * from venues where name like '%$search_value%'");
        return $query->num_rows();
    }
    public function get_search_results($search_value,$offset,$per_page)
    {
        $query = $this->db->query("select * from venues where name like '%$search_value%' limit $per_page , $offset");
        return $query->result();
    }
    public function get_search_with_zipcode($search_value)
    {
        $query = $this->db->query("select * from venue_details where zip like '%$search_value%'");
        return $query->result();
    }
    public function get_zipcode_result($id)
    {
        $query = $this->db->query("select * from venues where venue_id = ".(int)$id."");
        return $query->result();
    }
    public function get_search_artist_results($search_value)
    {
        $query = $this->db->query("select * from artist where name like '%$search_value%' or address like '%$search_value%'");
        return $query->result();
    }
    public function getAjaxVenueCityDetails($city_id)
    {
        //foreach($city_id as $id){
        $query=$this->db->query("select * from venues where venues.is_delete=0 and venues.is_active=1 and venues.is_complete>0 and city_id=".(int)$city_id." order by venues.venue_id DESC");//}
        return $query->result();
    }
    public function getAjaxVenueCapacityDetails($capacity)
    {
        $query=$this->db->query("select * from venues where venues.is_delete=0 and venues.is_active=1 and venues.is_complete>0 and capacity=".(int)$capacity." order by venues.venue_id DESC");
        return $query->result();
    }
    public function get_data()
    {
        $query=$this->db->query("select * from venues where venues.is_delete=0 and venues.is_active=1 and venues.is_complete>0 order by venues.venue_id DESC");
        return $query->result();
    }
    public function get_newest_venues()
    {
        $query=$this->db->query("select * from venues where venues.is_delete=0 and venues.is_active=1 order by venues.venue_id DESC");
        return $query->result();
    }
    public function get_newest_venue($data_genre,$data_city)
    {
                $where = "";
                if($data_genre!=""){
                $where .= " and ( ";
                    foreach($data_genre as $key=>$val){
                        if($key==0){
                            $where .= " FIND_IN_SET($val,genre)>0 ";            
                        }else{
                            $where .= " OR FIND_IN_SET($val,genre)>0 ";            
                        }
                    }
                $where .= " ) ";
                }

                if($data_city!=""){
                $where .= " and ( ";
                    foreach($data_city as $key=>$val){
                        if($key==0){
                            $where .= " city_id = $val ";            
                        }else{
                            $where .= " OR city_id = $val ";            
                        }
                    }
                $where .= " ) "; 
               } 

        $query=$this->db->query("select * from venues where venues.is_delete=0 and venues.is_active=1 $where order by venues.venue_id DESC");
        return $query->result();
    } 
    public function get_new_rating($data_genre,$data_city)
    {
                $where = "";
                if($data_genre!=""){
                $where .= " and ( ";
                    foreach($data_genre as $key=>$val){
                        if($key==0){
                            $where .= " FIND_IN_SET($val,genre)>0 ";            
                        }else{
                            $where .= " OR FIND_IN_SET($val,genre)>0 ";            
                        }
                    }
                $where .= " ) ";
                }

                if($data_city!=""){
                $where .= " and ( ";
                    foreach($data_city as $key=>$val){
                        if($key==0){
                            $where .= " city_id = $val ";            
                        }else{
                            $where .= " OR city_id = $val ";            
                        }
                    }
                $where .= " ) "; 
               } 

        $query=$this->db->query("select * from venues where venues.is_delete=0 and venues.is_active=1 $where order by venues.rating DESC");
        return $query->result();
    }
    public function get_relevant_venues($data_genre,$data_city)
    {
        $where = "";
                if($data_genre!=""){
                $where .= " and ( ";
                    foreach($data_genre as $key=>$val){
                        if($key==0){
                            $where .= " FIND_IN_SET($val,genre)>0 ";            
                        }else{
                            $where .= " OR FIND_IN_SET($val,genre)>0 ";            
                        }
                    }
                $where .= " ) ";
                }

                if($data_city!=""){
                $where .= " and ( ";
                    foreach($data_city as $key=>$val){
                        if($key==0){
                            $where .= " city_id = $val ";            
                        }else{
                            $where .= " OR city_id = $val ";            
                        }
                    }
                $where .= " ) "; 
               } 

        $query=$this->db->query("select * from venues where venues.is_delete=0 and venues.is_active=1 $where order by venues.genre DESC");
        return $query->result();
    }
    public function get_venuesprice_result($data_genre,$data_city)
    {
        $where = "";
                if($data_genre!=""){
                $where .= " and ( ";
                    foreach($data_genre as $key=>$val){
                        if($key==0){
                            $where .= " FIND_IN_SET($val,genre)>0 ";            
                        }else{
                            $where .= " OR FIND_IN_SET($val,genre)>0 ";            
                        }
                    }
                $where .= " ) ";
                }

                if($data_city!=""){
                $where .= " and ( ";
                    foreach($data_city as $key=>$val){
                        if($key==0){
                            $where .= " city_id = $val ";            
                        }else{
                            $where .= " OR city_id = $val ";            
                        }
                    }
                $where .= " ) "; 
               } 

        $query=$this->db->query("select * from venues left join venue_details on venues.venue_id = venue_details.venue_id where venues.is_delete=0 and venues.is_active=1 $where order by venue_details.venue_rate DESC");
        return $query->result();
    }
    public function get_venuedistance_result($data_genre,$data_city,$lat,$long)
    {
        $where = "";
                if($data_genre!=""){
                $where .= " and ( ";
                    foreach($data_genre as $key=>$val){
                        if($key==0){
                            $where .= " FIND_IN_SET($val,genre)>0 ";            
                        }else{
                            $where .= " OR FIND_IN_SET($val,genre)>0 ";            
                        }
                    }
                $where .= " ) ";
                }

                if($data_city!=""){
                $where .= " and ( ";
                    foreach($data_city as $key=>$val){
                        if($key==0){
                            $where .= " city_id = $val ";            
                        }else{
                            $where .= " OR city_id = $val ";            
                        }
                    }
                $where .= " ) "; 
               } 
        $query = "SELECT *, ( 3959 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) 
        * cos( radians( longitude ) - radians($long) ) + sin( radians($lat) ) * sin(radians(latitude)) ) ) AS distance 
        FROM venues 
        HAVING venues.is_delete=0 and venues.is_active=1 $where
        ORDER BY distance ASC";
        $query = $this->db->query($query);
        return $query->result();
    }
    public function get_venue_distance_result($lat,$long)
    {
        $query = "SELECT *, ( 3959 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) 
        * cos( radians( longitude ) - radians($long) ) + sin( radians($lat) ) * sin(radians(latitude)) ) ) AS distance 
        FROM venues 
        HAVING distance < 500 AND venues.is_delete=0 and venues.is_active=1 
        ORDER BY distance ASC";
        $query = $this->db->query($query);
        return $query->result();
    }
    public function getSearchCountt($genre_id,$city_id,$radius,$lat,$long,$zip){
        $where = '';
        $part  = '';
        if(isset($zip) && $zip!=""){
        $sql_zip = $this->db->query("select venue_id from venue_details where zip like '%$zip%'");
        $zip_row = $sql_zip->result(); 
        if($zip_row!="" && count($zip_row) > 0){
            $where .= " and ( ";
            foreach($zip_row as $key=>$val){
                if($key==0){
                    $where .= " venues.venue_id=$val->venue_id ";
                }else{
                    $where .= " or venues.venue_id=$val->venue_id ";
                }
            }
            $where .= " ) ";
        }
        }
        if($genre_id!="" && count($genre_id) > 0){
            $where .= " and ( ";
            
            foreach($genre_id as $key=>$val){
                if($key==0){
                    $where .= " FIND_IN_SET($val,genre)>0 ";        
                }else{
                    $where .= " OR FIND_IN_SET($val,genre)>0 ";
                }
            
            }
            $where .= " ) "; 
            }
        if($city_id!="" && count($city_id) > 0){
            $where .= " and ( ";
            
            foreach($city_id as $key=>$val){
                if($key==0){
                    $where .= " city_id = $val ";        
                }else{
                    $where .= " OR city_id = $val ";
                }
            
            }
            $where .= " ) ";
          }
          if($lat!="" && $long!=""){

            $part .= ",( 3959 * acos (cos ( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($long) ) + sin ( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance ";
            $having = " HAVING distance < $radius ";
       
          }else{$part = ''; $having = '';}  
          $sql = $this->db->query("select * $part from venues where is_active=1 $where $having");
          return $sql->num_rows();
    }
    public function getSearchCount($genre_id,$city_id,$radius,$lat,$long,$zip){
        $where = '';
        $part  ='';
        if($zip!=""){
        $sql_zip = $this->db->query("select artist_id from artist_details where zip like '%$zip%'");
        $zip_row = $sql_zip->result(); 
        if($zip_row!="" && count($zip_row) > 0){
            $where .= " and ( ";
            foreach($zip_row as $key=>$val){
                if($key==0){
                    $where .= " artist.artist_id=$val->artist_id ";
                }else{
                    $where .= " or artist.artist_id=$val->artist_id ";
                }
            }
            $where .= " ) ";
        }
        }
        if($genre_id!="" && count($genre_id) > 0){
            $where .= " and ( ";
            
            foreach($genre_id as $key=>$val){
                if($key==0){
                    $where .= " FIND_IN_SET($val,genre)>0 ";        
                }else{
                    $where .= " OR FIND_IN_SET($val,genre)>0 ";
                }
            
            }
            $where .= " ) "; 
            }
        if($city_id!="" && count($city_id) > 0){
            $where .= " and ( ";
            
            foreach($city_id as $key=>$val){
                if($key==0){
                    $where .= " city_id = $val ";        
                }else{
                    $where .= " OR city_id = $val ";
                }
            
            }
            $where .= " ) ";
          }
        if($lat!="" && $long!=""){

            $part .= ",( 3959 * acos (cos ( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($long) ) + sin ( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance ";
            $having = " HAVING distance < $radius ";
       
          }else{$part = ''; $having = '';}  
          $sql = $this->db->query("select * $part from artist where is_active=1 $where $having");
          return $sql->num_rows();
    }
    public function getAjaxArtistDetails($genre_id,$city_id,$lat,$long,$radius,$sorting,$latitude,$longitude,$zip,$offset,$per_page,$id)
    {   
        $output = "";
        $where = "";
        $part = "";
        $join = "";
        $distance = "";
        $rating = "";
        $avg = "";
        $group = "";
        $fav = "";
        $q = "";
        $popular = "";  
        $sql = $this->db->query("select favourites from users where user_id=$id");
        $arr = explode(',',$sql->row()->favourites);
        if($sorting==""){
            $order = " order by genre DESC ";
            // $group = " group by artist.user_id ";
            // $rating .= " left join rating on rating.receiver_id=artist.user_id "; 
            // $avg = " , AVG(rating.rating) as avg_rate ";
            // $order = " order by rating.rating DESC ";
        }
        if($sorting=="Most Relevant"){
            $order = " order by genre DESC ";
        }
        if($sorting=="Rating"){ 
            $group = " group by artist.user_id ";
            $rating .= " left join rating on rating.receiver_id=artist.user_id "; 
            $avg = " , AVG(rating.rating) as avg_rate ";
            $order = " order by avg_rate DESC ";  
        }
        if($sorting=="Newest Venue"){
            $order = " order by artist.artist_id DESC ";
        } 
        if($sorting=="Price"){
            $join .= " left join artist_details on artist.artist_id = artist_details.artist_id ";
            $order = " order by artist_details.artist_rate ASC ";
        }
        if($sorting=="Distance"){
            $distance  .= " , ( 3959 * acos (cos ( radians($latitude) ) * cos( radians( artist.latitude ) ) * cos( radians( artist.longitude ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians( artist.latitude ) ) ) ) AS distance ";
            $order = " order by distance ASC ";
        }
        if($sorting=="Popularity"){
            $q = " LEFT JOIN users ON FIND_IN_SET(artist.artist_id,users.favourites) > 0 ";
            $fav = " , COUNT(users.favourites) AS a_count "; 
            $group = " GROUP BY artist.artist_id "; 
            $order = " ORDER BY a_count DESC ";
        }
           if($genre_id!="" && count($genre_id) > 0){
            $where .= " and ( ";
            
            foreach($genre_id as $key=>$val){
                if($key==0){
                    $where .= " FIND_IN_SET($val,genre)>0 ";        
                }else{
                    $where .= " OR FIND_IN_SET($val,genre)>0 ";
                }
            
            }
            $where .= " ) "; 
            }
            if($city_id!="" && count($city_id) > 0){
            $where .= " and ( ";
            
            foreach($city_id as $key=>$val){
                if($key==0){
                    $where .= " city_id = $val ";        
                }else{
                    $where .= " OR city_id = $val ";
                }
            
            }
            $where .= " ) ";
          }
          if($zip!=""){ 
            $sql_zip = $this->db->query("select artist_id from artist_details where zip like '%$zip%'");
            $zip_row = $sql_zip->result(); 
            if($zip_row!="" && count($zip_row) > 0){
                $where .= " and ( ";
                foreach($zip_row as $key=>$val){
                    if($key==0){
                        $where .= " artist.artist_id=$val->artist_id ";
                    }else{
                        $where .= " or artist.artist_id=$val->artist_id ";
                    }
                }
                $where .= " ) ";
            }
            }
          if($lat!="" && $long!=""){
    
            $part .= " , ( 3959 * acos (cos ( radians($lat) ) * cos( radians( artist.latitude ) ) * cos( radians( artist.longitude ) - radians($long) ) + sin ( radians($lat) ) * sin( radians( artist.latitude ) ) ) ) AS distance ";
            $having = " HAVING distance < $radius ";  
       
          }else{$part = ''; $having = '';} 
         //return "select * $part $distance from artist $join where artist.is_delete=0 and artist.is_active=1 $where $having $order"; die;
         $query=$this->db->query("select * $part $fav $avg $distance from artist $join $q $rating where artist.is_delete=0 and artist.is_active=1 $where $group $having $order limit $per_page , $offset");
         $output .= '<div class="ListView search-listing" style="padding-bottom:0;">
                    <div class="row listing_wrap">';
        foreach($query->result() as $value)
        {
        $like_active='';        
        $user_favourites = isset($user_favourites)?$user_favourites:[];
        $artist_id = isset($value->artist_id)?$value->artist_id:0;
        if(in_array($artist_id,$arr)){
            $like_active='active';   
        }
        $output .= '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="" style="height:330px;">
                    <div class="panel panel-default"><div class="panel-body no-padder">
                    <div class="gimage"><a href="javascript:;" class="likebtn '.$like_active.'" data-id="'.$value->artist_id.'">
                    <i class="fa fa-heart-o"></i><i class="fa fa-heart"></i></a>
                    <a href="'.site_url().'Venue/details/'.$value->artist_id.'" rel="tab"><img src="'.site_url().'uploads/users/thumb/'.$value->profile_image.'" class="img-responsive center-block"></a>
                    </div>
                    </div>
                    <div class="panel-body wrapper-xs text-center"><h4 class="text-center">
                    <a href="'.site_url().'Venue/details/'.$value->artist_id.'">'.$value->name.'</a></h4>';
        $names = array();
        $explode = explode(',',$value->genre); 
        foreach($explode as $exp)
        {   
            $this->load->model('Artist_model');
            $exp_names = $this->Artist_model->getGenreNames($exp);
            foreach($exp_names as $exp_name)
            {
                $names[] = $exp_name->name;
            }
        }  
         $query = $this->db
                 ->select('Format(AVG(rating),1) AS avg')
                 ->from('rating')
                 ->where(array('receiver_id'=>$value->user_id,'type'=>2))
                 ->get();
                    $row = $query->row();
                    if(count($row) > 0){$rating = $row->avg;}else{$rating = 0.0;}
        $output .=   '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';   
        $output .=   '<div class="col-sm-12 text-center ratingstars">
                                        '.star_rating($rating).'';
        if($row->avg!="" && count($row->avg) > 0){
            $rate = explode('.',$row->avg);
            $output .= '<span> ('.$row->avg.')</span>';
        }
        $output .=   '</div>';
        $output .=   '<a href="'.site_url().'venueBooking/book/'.$value->artist_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                                </div>
                            </div>
                        </div>';
                         } 
        $output .=  '</div></div></div></div>';
        return $output;
    }
    public function get_ajax_venue_details_count($genre_id)
    {
        $where = '';
        if($genre_id!="" && count($genre_id) > 0){
            $where .= " ( ";
            
            foreach($genre_id as $key=>$val){
                if($key==0){
                    $where .= " FIND_IN_SET($val,genre)>0 ";        
                }else{
                    $where .= " OR FIND_IN_SET($val,genre)>0 ";
                }
            
            }
            $where .= " ) "; 
            } 
            $sql = $this->db->select("COUNT(*) AS pagec")->from("venues")->where($where)->get();
            return $sql->row();
    }
    public function getCountryID($id){
        $sql = $this->db->query("select CountryID,currency from cities where CityID=$id limit 1");
        return $sql->row();
    }
    public function getAjaxVenueDetails($genre_id,$city_id,$lat,$long,$radius,$sorting,$latitude,$longitude,$id,$zip,$offset,$per_page)
    {   
        $output = "";
        $where = "";
        $part = "";
        $join = "";
        $distance = "";
        $rating = "";
        $avg = "";
        $group = "";
        $fav = "";
        $q = ""; 
        $sql = $this->db->query("select favourites from users where user_id=$id");
        $arr = explode(',',$sql->row()->favourites); 
        if($sorting==""){
            $order = "order by genre DESC";
        // $group = " group by venues.user_id ";
        // $rating .= " left join rating on rating.receiver_id=venues.user_id "; 
        // $avg = " , AVG(rating.rating) as avg_rate ";
        // $order = " order by rating.rating DESC ";
        }
        if($sorting=="Most Relevant"){
            $order = "order by genre DESC";
        }
        if($sorting=="Rating"){ 
            $group = " group by venues.user_id ";
            $rating .= " left join rating on rating.receiver_id=venues.user_id "; 
            $avg = " , AVG(rating.rating) as avg_rate ";
            $order = " order by avg_rate DESC ";  
        }
        if($sorting=="Newest Venue"){
            $order = "order by venue_id DESC";
        } 
        if($sorting=="Price"){
            $join .= " left join venue_details on venues.venue_id = venue_details.venue_id ";
            $order = "order by venue_details.venue_rate ASC";
        }
        if($sorting=="Distance"){
            $distance  .= " , ( 3959 * acos( cos( radians($latitude) ) * cos( radians( venues.latitude ) ) 
                        * cos( radians( venues.longitude ) - radians($longitude) ) + sin( radians($latitude) ) * sin(radians(venues.latitude)) ) ) AS distance ";
            $order = "order by distance ASC";
        }
        if($sorting=="Popularity"){
            $q = " left join users on FIND_IN_SET(venues.venue_id,users.favourites) > 0 ";
            $fav = " , count(users.favourites) as v_count "; 
            $group = " GROUP BY venues.venue_id "; 
            $order = " order by v_count DESC ";
        }
           if($genre_id!="" && count($genre_id) > 0){
            $where .= " and ( ";
            
            foreach($genre_id as $key=>$val){
                if($key==0){
                    $where .= " FIND_IN_SET($val,genre)>0 ";        
                }else{
                    $where .= " OR FIND_IN_SET($val,genre)>0 ";
                }
            
            }
            $where .= " ) "; 
            }
            if($city_id!="" && count($city_id) > 0){
            $where .= " and ( ";
            
            foreach($city_id as $key=>$val){
                if($key==0){
                    $where .= " city_id = $val ";        
                }else{
                    $where .= " OR city_id = $val ";
                }
            
            }
            $where .= " ) ";
          }
          if($zip!=""){
            $sql_zip = $this->db->query("select venue_id from venue_details where zip like '%$zip%'");
            $zip_row = $sql_zip->result(); 
            if($zip_row!="" && count($zip_row) > 0){
                $where .= " and ( ";
                foreach($zip_row as $key=>$val){
                    if($key==0){
                        $where .= " venues.venue_id=$val->venue_id ";
                    }else{
                        $where .= " or venues.venue_id=$val->venue_id ";
                    }
                }
                $where .= " ) ";
            }
            }
          if($lat!="" && $long!=""){
            $part .= " , ( 3959 * acos (cos ( radians($lat) ) * cos( radians( venues.latitude ) ) * cos( radians( venues.longitude ) - radians($long) ) + sin ( radians($lat) ) * sin( radians( venues.latitude ) ) ) ) AS distance ";
            $having = " HAVING distance < $radius ";
       
          }else{$part = ''; $having = '';}
          //return "select * $avg $part $distance from venues $rating $join where venues.is_delete=0 and venues.is_active=1 $where $having $order"; exit;
        $query=$this->db->query("select * $fav $avg $part $distance from venues $rating $join $q where venues.is_delete=0 and venues.is_active=1 $where $group $having $order limit $per_page , $offset"); 
        $output .= '<div class="ListView search-listing" style="padding-bottom:0;">
                    <div class="row listing_wrap">';
        foreach($query->result() as $value)
        {
        $like_active='';        
        //$user_favourites = isset($user_favourites)?$user_favourites:[];
        $venue_id = isset($value->venue_id)?$value->venue_id:0;
        if(in_array($venue_id,$arr)){
            $like_active='active';   
        }
        $output .= '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="" style="height:330px;">
                    <div class="panel panel-default"><div class="panel-body no-padder">
                    <div class="gimage"><a href="javascript:;" class="likebtn '.$like_active.'" data-id="'.$value->venue_id.'">
                    <i class="fa fa-heart-o"></i><i class="fa fa-heart"></i></a>
                    <a href="'.site_url().'artist/details/'.$value->venue_id.'" rel="tab"><img src="'.site_url().'uploads/users/thumb/'.$value->profile_image.'" class="img-responsive center-block"></a>
                    </div>
                    </div>
                    <div class="panel-body wrapper-xs text-center"><h4 class="text-center">
                    <a href="'.site_url().'artist/details/'.$value->venue_id.'">'.$value->name.'</a></h4>';
        $names = array();
        $explode = explode(',',$value->genre); 
        foreach($explode as $exp)
        {   
            $this->load->model('Artist_model');
            $exp_names = $this->Artist_model->getGenreNames($exp);
            foreach($exp_names as $exp_name)
            {
                $names[] = $exp_name->name;
            } //'.site_url().'artistBooking/book/'.$value->venue_id.'
        }  
         $query = $this->db
                 ->select('Format(AVG(rating),1) AS avg')
                 ->from('rating')
                 ->where(array('receiver_id'=>$value->user_id,'type'=>1))
                 ->get();
        $row = $query->row();
                    if(count($row) > 0){$rating = $row->avg;}else{$rating = 0.0;}
        $output .=   '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';   
        $output .=   '<div class="col-sm-12 text-center ratingstars">
                                        '.star_rating($rating).'';
        if($row->avg!="" && count($row->avg) > 0){
            $rate = explode('.',$row->avg);
            $output .= '<span> ('.$row->avg.')</span>';
        }
        $output .=   '</div>';
        $output .=   '<a href="'.site_url().'artistBooking/book/'.$value->venue_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                                </div>
                            </div>
                        </div>';
                         } 
        $output .=  '</div></div></div></div>';
        return $output;
    } 
    public function getFavourties($id=0){
        $where = '';
        $this->db->select('*');
        $this->db->where('user_id',$id);
        $sql = $this->db->get('users');
        $row = $sql->row();
        $result = explode(',',$row->favourites);
        foreach($result as $key=>$val){
          if($key==0){
            $where .= " venue_id=$val ";
          }else{
            $where .= " or venue_id=$val ";
          }
        }
       $sql = $this->db->query("select * from venues where $where");
       return $sql->result();
    }
    public function sorting($data_genre,$data_city,$sorting,$lat,$long)
    {    
        $where = "";
        if($sorting=="Newest Venue"){
          
        if(count($data_genre) > 0 && count($data_city) > 0){
            $order = "";
        }else{
            $order = " order by venues.venue_id desc ";
        }
           if($data_genre!="" && count($data_genre) > 0){
            $where .= " and ( ";
            
            foreach($data_genre as $key=>$val){
                if($key==0){
                    $where .= " FIND_IN_SET($val,genre)>0 ";        
                }else{
                    $where .= " OR FIND_IN_SET($val,genre)>0 ";
                }
            
            }
            $where .= " ) $order "; 
            }
            else{
                $where .= " order by venues.venue_id desc ";
            }
            if($data_city!="" && count($data_city) > 0){
            $where .= " and ( ";
            
            foreach($data_city as $key=>$val){
                if($key==0){
                    $where .= " city_id = $val ";        
                }
                else{
                    $where .= " OR city_id = $val ";
                }
            
            }
            $where .= " ) order by venues.venue_id desc ";
          } 

       }   

       else if($sorting=="Rating"){
        if(count($data_genre) > 0 && count($data_city) > 0){
            $order = "";
        }else{
            $order = " order by venues.rating desc ";
        }
           if($data_genre!="" && count($data_genre) > 0){
            $where .= " and ( ";
            
            foreach($data_genre as $key=>$val){
                if($key==0){
                    $where .= " FIND_IN_SET($val,genre)>0 ";        
                }else{
                    $where .= " OR FIND_IN_SET($val,genre)>0 ";
                }
            
            }
            $where .= " ) $order "; 
            }else{
                $where .= " order by venues.rating desc ";
            }
            if($data_city!="" && count($data_city) > 0){
            $where .= " and ( ";
            
            foreach($data_city as $key=>$val){
                if($key==0){
                    $where .= " city_id = $val ";        
                }else{
                    $where .= " OR city_id = $val ";
                }
            
            }
            $where .= " ) order by venues.rating desc ";
          } 
       } 

       else if($sorting=="Most Relevant"){
        if(count($data_genre) > 0 && count($data_city) > 0){
            $order = "";
        }else{
            $order = " order by venues.genre desc ";
        }
           if($data_genre!="" && count($data_genre) > 0){
            $where .= " and ( ";
            
            foreach($data_genre as $key=>$val){
                if($key==0){
                    $where .= " FIND_IN_SET($val,genre)>0 ";        
                }else{
                    $where .= " OR FIND_IN_SET($val,genre)>0 ";
                }
            
            }
            $where .= " ) $order "; 
            }else{
                $where .= " order by venues.genre desc ";
            }
            if($data_city!="" && count($data_city) > 0){
            $where .= " and ( ";
            
            foreach($data_city as $key=>$val){
                if($key==0){
                    $where .= " city_id = $val ";        
                }else{
                    $where .= " OR city_id = $val ";
                }
            
            }
            $where .= " ) order by venues.genre desc ";
          } 
       } 
       
       else if($sorting=="Price"){
        if(count($data_genre) > 0 && count($data_city) > 0){
            $order = "";
            $new = " left join venue_details on venues.venue_id = venue_details.venue_id ";
        }else{
            $order = " order by venue_rate ";
            $new = " left join venue_details on venues.venue_id = venue_details.venue_id ";
        }
           if($data_genre!="" && count($data_genre) > 0){
            $where .= " and ( ";
            
            foreach($data_genre as $key=>$val){
                if($key==0){
                    $where .= " FIND_IN_SET($val,genre)>0 ";        
                }else{
                    $where .= " OR FIND_IN_SET($val,genre)>0 ";
                }
            
            }
            $where .= " ) $order "; 
            }else{
                $where .= "";
            }
            if($data_city!="" && count($data_city) > 0){
            $where .= " and ( ";
            
            foreach($data_city as $key=>$val){
                if($key==0){
                    $where .= " city_id = $val ";        
                }else{
                    $where .= " OR city_id = $val ";
                }
            
            }
            $where .= " ) order by venue_rate desc ";
          } 
       }
       
       else if($sorting=="Distance"){
        if(count($data_genre) > 0 && count($data_city) > 0){
            $order = "";
        }else{
            $order = " order by distance ASC ";
        }
           if($data_genre!="" && count($data_genre) > 0){
            $where .= " and ( ";
            
            foreach($data_genre as $key=>$val){
                if($key==0){
                    $where .= " FIND_IN_SET($val,genre)>0 ";        
                }else{
                    $where .= " OR FIND_IN_SET($val,genre)>0 ";
                }
            
            }
            $where .= " ) $order "; 
            }else{
                $where .= " order by distance ASC ";
            }
            if($data_city!="" && count($data_city) > 0){
            $where .= " and ( ";
            
            foreach($data_city as $key=>$val){
                if($key==0){
                    $where .= " city_id = $val ";        
                }else{
                    $where .= " OR city_id = $val ";
                }
            
            }
            $where .= " ) order by distance ASC ";
          } 
       }
       if($sorting=="Price"){
        $query=$this->db->query("select * from venues $new where venues.is_delete=0 and venues.is_active=1 $where");
        return $query->result();}
        else if($sorting=="Distance"){
            $query = "SELECT *, ( 3959 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) 
                    * cos( radians( longitude ) - radians($long) ) + sin( radians($lat) ) * sin(radians(latitude)) ) ) AS distance 
                    FROM venues $where HAVING venues.is_delete=0 and venues.is_active=1";
                    $query = $this->db->query($query);
                    return $query->result();
        }else{
            $query=$this->db->query("select * from venues where venues.is_delete=0 and venues.is_active=1 $where");
            return $query->result();
        }
    }
    public function get_newest_artist()
    {  //artist_id IN (SELECT artist_id FROM artist WHERE created_datetime = (SELECT MAX(created_datetime) FROM artist)) ORDER BY artist_id DESC   
        $query=$this->db->query("select * from artist where artist.is_delete=0 and artist.is_active=1 and artist.is_complete>0 order by artist.artist_id DESC");
        return $query->result();
    } 
    public function get_artist_popularity()
    {
        $query = "SELECT *, SUM(user_id) AS total
                    FROM artist
                    GROUP BY user_id
                    ORDER BY SUM(user_id) DESC";
        $query = $this->db->query($query);
        return $query->result();
    }
    public function get_relevant_venue($genre)
    {   
        $where = "";
        $where .= " where ( ";
                    foreach($genre as $key=>$val){ 
                        if($key==0){
                            $where .= " FIND_IN_SET($val,genre)>0 ";            
                        }else{
                            $where .= " OR FIND_IN_SET($val,genre)>0 ";            
                        }
                    }
                $where .= " ) "; 
        $query=$this->db->query("select * from venues $where and is_delete=0 and is_active=1");
        return $query->result();
    }
    public function get_price()
    {
        $query = $this->db->query("select venue_id,venue_rate from venue_details");
        return $query->result();
    }
    public function get_venueprice_result($data=[])
    {   
        $where = "";
        $where .= " where ( ";
        foreach($data as $key=>$id){ 
            if($key==0){
                $where .= " venue_id = $id ";
            }else{
                $where .= " OR venue_id = $id ";
            }
       }
       $where .= " ) "; 
       $query=$this->db->query("select * from venues $where and is_delete=0 and is_active=1");
       return $query->result();
    }

    public function get_rating()
    {
        $query=$this->db->query("select *, count(rating) as rate from venues group by rating");
        return $query->result();
    } 
    public function get_ratingg()
    {
        $query=$this->db->query("select *, count(rating) as rate from artist group by rating");
        return $query->result();
    }
    public function get_prices()
    {
        $query=$this->db->query("select *, max(venue_rate) as price from venue_details");
        return $query->result();
    }
    public function get_recrods($data=[])
    {
        $where="";
        if(isset($data['city_id']) && is_numeric($data['city_id'])) {
            if(count($data['city_id'])>0){
            $where .= "and (";
            foreach($data['city_id'] as $key=>$val)
            {
                if($key==0){
                $where .= "city_id=".(int)$val." ";
               }
               else{
                $where .= " and city_id=".(int)$val." ";
               }
            } 
            $where .= " ) ";
        }
    }  
        if(isset($data['genre']) && is_array($data['genre'])) {
            if(count($data['genre'])>0){
                $where .= " and (";
                    foreach($data['genre'] as $key=>$val){
                        if($key==0){
                            $where .= " FIND_IN_SET($val,genre)>0 ";            
                        }else{
                            $where .= " OR FIND_IN_SET($val,genre)>0 ";            
                        }
                    }
                $where .= " ) ";
            }
        }
        $query=$this->db->query("select * from venues where is_delete=0 and is_active=1 and is_complete=0 $where");
        return $query->result();
    }
    public function insert_payment_info($id=0)
    {
        $this->db->insert('payment_info',array('user_id'=>$id));
    }
    public function update_payment_info($id=0,$payment_info)
    {
        $this->db->where('user_id',$id)->update('payment_info',$payment_info);
    }
    public function get_venue_payment_info($id=0)
    {
        $query="select * from payment_info where user_id=".(int)$id."";
        $query=$this->db->query($query);
        return $query->row();
    }
    public function override_img($data=[])
    {
        $this->db->where($data)->update('artist',array('profile_image'=>''));
    }
    public function override_edit_img($data=[])
    {
        $this->db->where($data)->update('artist_member',array('image_url'=>''));
    }
    public function cancellation_policy(){
        $this->db->select('*');
        $sql = $this->db->get('cancellation_policy');
        return $sql->row();
    }
   
}
