<?php $this->load->view('admin/includes/admin_header'); ?>
<body>

<!-- Main navbar -->
<div class="navbar navbar-default header-highlight">
<div class="navbar-header">
<a class="navbar-brand" href="<?php echo site_url('admin_dashboard'); ?>"><img src="<?php echo site_url('assets/images/logo-n.png'); ?>" alt=""></a>

<ul class="nav navbar-nav visible-xs-block">
<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
</ul>
</div>

<div class="navbar-collapse collapse" id="navbar-mobile">
<ul class="nav navbar-nav navbar-right">
<li class="dropdown dropdown-user">
<a class="dropdown-toggle" data-toggle="dropdown">
<img src="<?php echo site_url(); ?>assets/images/placeholder.jpg" alt="">
<span><?php echo ($this->session->userdata('f_name'))?$this->session->userdata('f_name'):'Admin'; ?></span>
<i class="caret"></i>
</a>

<ul class="dropdown-menu dropdown-menu-right">
<li><a href="<?php echo site_url('admin_dashboard/admin_profile'); ?>"><i class="icon-user-plus"></i> My profile</a></li>
<li><a href="<?php echo site_url('admin_login/logout'); ?>"><i class="icon-switch2"></i> Logout</a></li>
</ul>
</li>
</ul>
</div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

<!-- Page content -->
<div class="page-content">

<!-- Main sidebar -->
<div class="sidebar sidebar-main">
<div class="sidebar-content">

<!-- Main navigation -->
<div class="sidebar-category sidebar-category-visible">
<div class="category-content no-padding">
<ul class="navigation navigation-main navigation-accordion">

<!-- Main -->
<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
<li class="active"><a href="<?php echo site_url('admin_dashboard'); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/artist'); ?>"><i class="icon-width"></i> <span>Artist</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/venue'); ?>"><i class="icon-width"></i> <span>Venues</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/transection'); ?>"><i class="icon-width"></i> <span>Transactions</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/admin'); ?>"><i class="icon-user"></i> <span>Admin</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/reviews'); ?>"><i class="icon-comments"></i> <span>Reviews</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/settings'); ?>"><i class="icon-user"></i> <span>Settings</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/admin_profile'); ?>"><i class="icon-user-plus"></i> <span>Profile</span></a></li>
</ul>
</div>
</div>
<!-- /main navigation -->

</div>
</div>
<!-- /main sidebar -->


<!-- Main content -->
<div class="content-wrapper">

<!-- Content area -->
<div class="content">

<!-- Main charts -->
<div class="row">
<div class="col-lg-12">

<!-- Traffic sources -->
<div class="panel panel-flat">
<div class="panel-heading">
<h5 class="panel-title">Welcome to dashboard</h5>
</div>
<div class="container-fluid">
<div class="row">
<div class="col-lg-3">
<div class="panel bg-teal-400">
<div class="panel-body" style="padding-right:0;">
<div class="col-lg-6">
<h3 class="no-margin">Artist</h3>
<div class="text-muted text-size-small"><h5>Total <?php echo ($artist->artist_count) > 0 && $artist->artist_count!=""?$artist->artist_count:0; ?></h5></div>
</div>
<div class="col-lg-6" style="padding-right:0;"><i class="icon-user" style="display:block;font-size:70px;"></i></div>
</div>
</div>
</div>
<div class="col-lg-3">
<div class="panel bg-pink-400">
<div class="panel-body" style="padding-right:0;">
<div class="col-lg-6">
<h3 class="no-margin">Venue</h3>
<div class="text-muted text-size-small"><h5>Total <?php echo ($venue->venue_count) > 0 && $venue->venue_count!=""?$venue->venue_count:0; ?></h5></div>
</div>
<div class="col-lg-6" style="padding-right:0;"><i class="icon-user" style="display:block;font-size:70px;"></i></div>
</div>
</div>
</div>
<div class="col-lg-3">
<div class="panel bg-blue-400">
<div class="panel-body" style="padding-right:0;">
<div class="col-lg-6">
<h3 class="no-margin">Admin</h3>
<div class="text-muted text-size-small"><h5>Total <?php echo ($admin->admin_count) > 0 && $admin->admin_count!=""?$admin->admin_count:0; ?></h5></div>
</div>
<div class="col-lg-6" style="padding-right:0;"><i class="icon-user" style="display:block;font-size:70px;"></i></div>
</div>
</div>
</div>
<div class="col-lg-3">
<div class="panel bg-teal-400">
<div class="panel-body" style="padding-right:0;">
<div class="col-lg-6">
<h3 class="no-margin">Transactions</h3>
<div class="text-muted text-size-small"><h5>Total <?php echo ($transaction->transaction_count) > 0 && $transaction->transaction_count!=""?$transaction->transaction_count:0; ?></h5></div>
</div>
<div class="col-lg-6" style="padding-right:0;"><i class="icon-cash3" style="display:block;font-size:70px;"></i></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Footer -->
<div class="footer text-muted">
&copy; <?php echo date('Y'); ?>. <a href="#">Showspoon</a> by <a href="http://www.mashkraft.com" target="_blank">Mashkraft</a>
</div>
<!-- /footer -->

</div>
<!-- /content area -->

</div>
<!-- /main content -->

</div>
<!-- /page content -->

</div>
<!-- /page container -->
<?php $this->load->view('admin/includes/admin_footer'); ?>
</body>
</html>

