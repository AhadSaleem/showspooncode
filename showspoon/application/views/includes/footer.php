 <?php //dd($session_user); ?>

<script src="<?php echo base_url();?>/assets/js/pnotify.min.js"></script>
<script>
    var myInterval2;
    var interval_delay2 = 5000;
    var is_interval_running2 = false; //Optional
    $(document).ready(function(){
        myInterval2 = setInterval(Push_Notifications, interval_delay2);
        $(window).focus(function () {
            clearInterval(myInterval2); // Clearing interval if for some reason it has not been cleared yet
            if  (!is_interval_running2) //Optional
                myInterval2 = setInterval(Push_Notifications, interval_delay2);
        }).blur(function () {
            clearInterval(myInterval2); // Clearing interval on window blur
            is_interval_running2 = false; //Optional
        });
        function Push_Notifications(){
             <?php if(isset($session_user['type_id']) && $session_user['type_id']==1){
$notification_url='artist/get_notifications';
 }else{
$notification_url='venue/get_notifications';
  }?>
            is_interval_running2 = true;
            $.ajax({
                type:'POST',
                url: '<?php echo site_url($notification_url);?>',
                data:{user_id:"<?php echo isset($session_user['user_id'])?>"},
                dataType:'json',
                beforeSend:function(){
                    // $('#btn-1').attr('disabled',true);
                },
                success:function(data){
                    if(data.data){
                        for(var q=0; q<data.data.length; q++){

                        new PNotify({
                            title: 'Success notice',
                            text: data.data[q]['message'],
                            icon: 'icon-checkmark3',
                            type: 'success'
                        });    
                    }
                }
                },
                error: function(data){
                }
            });
        }
    });
</script>




<footer class="main-footer" style="position:fixed;bottom:0;left:0;right:0;">
   <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-left">
                    <p class="copyright">&copy; <?php echo date('Y'); ?> Showspoon. All rights reserved.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                    <ul class="legallinks text-right">
                        <a class="text-white" href="<?php echo site_url();?>privacy-policy">Privacy Policy</a><span class="sep whitetext "> |</span>
                        <a class="text-white"  href="<?php echo site_url();?>terms">Terms of Use </a><span class="sep whitetext "> |</span>
                        <a class="text-white" href="<?php echo site_url();?>contact" class="tabnav">Contact Us</a>
                    </ul>
            </div>
        </div><!-- row -->
    </div><!-- container -->
</footer>