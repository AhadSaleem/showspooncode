<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class managers extends CI_Controller {

    private $loggedintime;
    private $admin_data;

    function __construct() {

        parent::__construct();
        $this->load->model(array('admin_model','managers_model'));
        $this->data['title'] = 'admin';
        $this->data['page_title'] = "admin";
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        //$this->data['session_user']=$this->user_data;
        if (!$this->session->userdata('ShowspoonAdmin_logged_in')) {
            
            redirect(site_url('admin_login'));
        }
        //$this->admin_data = admin_data();
        $this->admin_data = $this->session->userdata('ShowspoonAdmin_logged_in');


    }

    public function index() {

        
        $this->data['title'] = 'Admins - Showspoon';
        $this->data['success'] = $this->session->flashdata('success')?$this->session->flashdata('success'):'';
        $this->data['error'] = $this->session->flashdata('error')?$this->session->flashdata('error'):'';
        $this->data['admins'] = $this->managers_model->get_managers();
        $this->load->view('admin/managers/index', $this->data);
    }
    public function edit($id=0) {

        $this->data['title'] = 'Edit Admins - Showspoon';

        $this->data['admin'] = $this->managers_model->get_managers($id);
        if(!isset($this->data['admin']->id)){
            show_404();
        }
        if ($this->input->post()) {
            $email = $this->input->post('email')?$this->input->post('email'):'';
            $row = $this->managers_model->get_manager_by_email($email);
            if((isset($row->id,$row->email) && $email==$row->email) || (!isset($row->id))){
            $fillable = ['f_name','l_name','email','is_delete','is_active','type','password'];
            $update=[];
            foreach($this->input->post() as $key=>$val){
                if(in_array($key,$fillable)){
                    if($key=='password' && $val!=''){
                        $update[$key]=($val!='')?md5($val):'';    
                    }elseif($key=='type'){
                        $update[$key]=($val!='')?$val:2;    
                    }else{
                        $update[$key]=($val!='')?$val:'';    
                    }
                }

            }
            if(count($update)>0){
                $update['id']=$id;
                

                $this->managers_model->update_manager($update);
                //dd($update);
            }

            $this->session->set_flashdata('success', 'Record has been successfully updated.');
            redirect('managers');
            }else{
                $this->data['error']= 'This email already exists in our records.';
            }
        }
        $this->load->view('admin/managers/edit', $this->data);
    }
    
    public function create() {

        $this->data['title'] = 'Add Admins - Showspoon';


        if ($this->input->post()) {
            $email = $this->input->post('email')?$this->input->post('email'):'';
            $row = $this->managers_model->get_manager_by_email($email);
            if(!isset($row->id)){
            $fillable = ['f_name','l_name','email','is_delete','is_active','type','password'];
            $update=[];
            foreach($this->input->post() as $key=>$val){
                if(in_array($key,$fillable)){
                    if($key=='password'){
                        $update[$key]=($val!='')?md5($val):'';    
                    }elseif($key=='type'){
                        $update[$key]=($val!='')?$val:2;    
                    }else{
                        $update[$key]=($val!='')?$val:'';    
                    }
                }

            }
            
            if(isset($update['email']))
            if(count($update)>0){
                
                
               $update = $this->managers_model->create_manager($update);
                
            }
            //dd($update);

            $this->session->set_flashdata('success', 'Record has been successfully added.');
            redirect('managers');
            }else{
                $this->data['error']= 'This email already exists in our records.';
            }
        }
        $this->load->view('admin/managers/create', $this->data);
    }

}



