<?php
$chat = (object)$chat;
$session_user = isset($session_user)?(object)$session_user:[];
$user_image=site_url('assets/dist/images/noimage2.jpg');

if(isset($session_user->profile_image) && $session_user->profile_image!=''){

    $user_image=site_url('uploads/users/thumb/'.$session_user->profile_image);
}
$sent = isset($sent)?$sent:date('Y-m-d H:i:s',time());
$update_event = isset($chat->update_event)?$chat->update_event:0;
//dd($chat,false);
?>

<div class="col-md-3"><img src="<?php echo $user_image;?>" class=" img-responsive img-circle" alt="" style="width:200px;height:180px;"> </div>
<div class="col-md-9" style="min-height:200px;margin-bottom:30px;">
<div class="panel-group" style="margin-top:30px;">
    <div class="panel panel-default">
      <div class="panel-heading"><strong>Date&Time:</strong>  <?php echo date('jS M, Y h:i A',strtotime($sent));?></div>
      <div class="panel-body"><strong>Message:</strong> <?php echo isset($chat->message)?nl2br($chat->message):'';?></div>
    </div>
</div>
</div>

<!-- <li class="message left appeared">
    <div class="avatar"><img src="<?php echo $user_image;?>" class=" img-responsive img-circle"></div>
    <div class="text_wrapper">
        <div class="text">  <?php if($update_event==1){
    if($session_user->type_id==$booking->type){
         echo '<h5 class="m-t-none"><strong>You have update the event</strong></h5>';
    }else{
         echo '<h5 class="m-t-none"><strong>Event has been updated.</strong></h5>';
    }
    
               
                               
}?><?php echo isset($chat->message)?nl2br($chat->message):'';?></div>
    </div>
    <time datetime="2009-11-13T20:00"><?php echo date('jS M, Y h:i A',strtotime($sent));?></time>
</li>
<div class="row msg_container base_receive hidden">
    <div class="col-md-1 col-xs-1 avatar no-gutter">
        <img src="<?php echo $user_image;?>" class=" img-responsive">
    </div>
    <div class="col-md-11 col-xs-11 no-gutter">
        <div class="messages msg_receive">
           
            <p>
          
            <?php echo isset($chat->message)?nl2br($chat->message):'';?></p>
            <time datetime="2009-11-13T20:00"><?php echo date('jS M, Y h:i A',strtotime($sent));?></time>
        </div>
    </div>
</div> -->