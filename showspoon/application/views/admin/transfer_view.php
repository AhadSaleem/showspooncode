
<?php $this->load->view('admin/includes/admin_header'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<body>

<!-- Main navbar -->
<div class="navbar navbar-default header-highlight">
<div class="navbar-header">
<a class="navbar-brand" href="<?php echo site_url('admin_dashboard'); ?>"><img src="<?php echo site_url(); ?>admin_assets/images/logo_light.png" alt=""></a>

<ul class="nav navbar-nav visible-xs-block">
<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
</ul>
</div>

<div class="navbar-collapse collapse" id="navbar-mobile">
<ul class="nav navbar-nav navbar-right">
<li class="dropdown dropdown-user">
<a class="dropdown-toggle" data-toggle="dropdown">
<img src="<?php echo site_url(); ?>assets/images/placeholder.jpg" alt="">
<span><?php echo ($this->session->userdata('f_name'))?$this->session->userdata('f_name'):'Admin'; ?></span>
<i class="caret"></i>
</a>

<ul class="dropdown-menu dropdown-menu-right">
<li><a href="<?php echo site_url('admin_dashboard/admin_profile'); ?>"><i class="icon-user-plus"></i> My profile</a></li>
<li><a href="<?php echo site_url('admin_login/logout'); ?>"><i class="icon-switch2"></i> Logout</a></li>
</ul>
</li>
</ul>
</div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

<!-- Page content -->
<div class="page-content">

<!-- Main sidebar -->
<div class="sidebar sidebar-main">
<div class="sidebar-content">

<!-- Main navigation -->
<div class="sidebar-category sidebar-category-visible">
<div class="category-content no-padding">
<ul class="navigation navigation-main navigation-accordion">

<!-- Main -->
<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
<li><a href="<?php echo site_url('admin_dashboard'); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/artist'); ?>"><i class="icon-width"></i> <span>Artist</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/venue'); ?>"><i class="icon-width"></i> <span>Venues</span></a></li>
<li class="active"><a href="<?php echo site_url('admin_dashboard/transection'); ?>"><i class="icon-width"></i> <span>Transactions</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/admin'); ?>"><i class="icon-user"></i> <span>Admin</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/settings'); ?>"><i class="icon-user"></i> <span>Settings</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/admin_profile'); ?>"><i class="icon-user-plus"></i> <span>Profile</span></a></li>
</ul>
</div>
</div>
<!-- /main navigation -->

</div>
</div>
<!-- /main sidebar -->


<!-- Main content -->
<div class="content-wrapper">

<!-- Content area -->
<div class="content">

<!-- Main charts -->
<div class="row">
<div class="col-lg-12">
<form action='<?php echo site_url("admin_dashboard/transfer_payment");//echo site_url("admin_dashboard/transfer/$id"); ?>' method="post" id="paymentFrm">
<div class="panel panel-flat">
<div class="panel-heading">
<h5 class="panel-title">Transfer Payment</h5>
<div class="heading-elements">
<ul class="icons-list">
<li><a data-action="collapse"></a></li>
<li><a data-action="reload"></a></li>
<li><a data-action="close"></a></li>
</ul>
</div>
</div>

<div class="panel-body">
<?php if($this->session->flashdata('msg')){ ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('msg'); ?>
</div>
<?php } ?>
<div class="form-group">
<label>Account Number:</label>
<input type="text" name="number" id="number" class="form-control" placeholder="Enter Account Number" value="<?php echo $row->cc_number; ?>" readonly="readonly">
<?php echo form_error('number','<div style="color:#a94442;">', '</div>'); ?>
</div>
<div class="form-group">
<label>CVC:</label>
<input type="text" name="cvc" id="cvc" class="form-control" placeholder="Enter CVC" value="<?php echo $row->ccv; ?>" readonly="readonly">
<?php echo form_error('cvc','<div style="color:#a94442;">', '</div>'); ?>
</div>
<div class="form-group">
<label>Exp Month:</label>
<input type="text" name="exp_month" id="exp_month" class="form-control" placeholder="Enter Exp Month" value="<?php echo $row->exp_month; ?>" readonly="readonly">
<?php echo form_error('exp_month','<div style="color:#a94442;">', '</div>'); ?>
</div>
<div class="form-group">
<label>Exp Year:</label>
<input type="text" name="exp_year" id="exp_year" class="form-control" placeholder="Enter Exp Year" value="<?php echo $row->exp_year; ?>" readonly="readonly">
<?php echo form_error('exp_year','<div style="color:#a94442;">', '</div>'); ?>
</div>
<div class="form-group">
<label>Amount:</label>
<input type="text" name="amount" id="amount" class="form-control" placeholder="Enter Amount" value="<?php echo $row->amount; ?>" readonly="readonly">
<?php echo form_error('amount','<div style="color:#a94442;">', '</div>'); ?>
</div>
<div class="text-right">
<button type="submit" name="submit" value="submit" id="payBtn" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
</div>
</div>
</div>
</form>
</div>
</div>
<!-- Footer -->
<div class="footer text-muted">
&copy; <?php echo date('Y'); ?>. <a href="#">Showspoon</a> by <a href="http://www.mashkraft.com" target="_blank">Mashkraft</a>
</div>
<!-- /footer -->

</div>
<!-- /content area -->

</div>
<!-- /main content -->

</div>
<!-- /page content -->

</div>
<!-- /page container -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<?php $this->load->view('admin/includes/admin_footer'); ?>
<script type="text/javascript">
        //set your publishable key
        Stripe.setPublishableKey('pk_test_NWhYzPP5DpSh6D2xHGQXwQr8');
      
        //callback to handle the response from stripe
        function stripeResponseHandler(status, response) {
            if (response.error) {
                //enable the submit button
                $('#payBtn').removeAttr("disabled");
            } else {
                var form$ = $("#paymentFrm");
                //get token id
                var token = response['id']; 
                //insert the token into the form
                form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");  
                form$.append('amount',$('#amount').val());
                $.ajax({
                  url:'<?php echo site_url('admin_dashboard/transfer_payment'); ?>',
                  type:'POST',
                  data:form$.serialize(),
                  success: function(data){ alert(data);
                    $('#payBtn').attr("disabled", false);
                    form$.get(0).reset();
                  }
                });
            }
        } 
        
        $(document).ready(function() {  
            //on form submit
            $("#paymentFrm").submit(function(event) { 
                
                //disable the submit button to prevent repeated clicks
                $('#payBtn').attr("disabled", true);

                //create single-use token to charge the user
                Stripe.createToken({
                    //amount: $('#amount').val()
                    number: $('#number').val(),
                    cvc: $('#cvc').val(),
                    exp_month: $('#exp_month').val(),
                    exp_year: $('#exp_year').val(),
                }, stripeResponseHandler);
                
                //submit from callback
                return false;
            });
        });
    </script>
</body>
</html>
