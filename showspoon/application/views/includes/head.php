<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Showspoon</title>

		<link href="<?php echo base_url();?>assets/css/components.css?>" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url();?>assets/css/bootstrap.css?>" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"> 
		<link href="<?php echo base_url();?>assets/css/font-awesome.css?>" rel="stylesheet">
		<link href="<?php echo base_url();?>assets/plugins/select2/select2.min.css?>" rel="stylesheet">
		<link href="<?php echo base_url();?>assets/css/custom.css?>" rel="stylesheet">
		<link href="<?php echo base_url();?>assets/css/custom-develop.css?>" rel="stylesheet">
		<link href="<?php echo base_url();?>assets/css/custom_style.css?>" rel="stylesheet">
		<link href="<?php echo base_url();?>assets/images/icon.png" rel="icon" type="image/icon">
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->