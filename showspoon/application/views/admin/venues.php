<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('admin/includes/head');?>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/pages/datatables_advanced.js"></script>
    </head>
    <body class="navbar-top">

        <!-- Main navbar -->
        <?php $this->load->view('admin/includes/header');?>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <?php $this->load->view('admin/includes/sidebar');?>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Venues</span> </h4>
                            </div>

                            
                        </div>

                        
                    </div>
                    <!-- /page header -->

                   <div class="content">

					<!-- Page length options -->
					<div class="panel panel-flat">
						

						

						<table class="table datatable-show-all">
							
								<thead>
								<tr>
									<th>Artist Name</th>
									<th>Email</th>
									<th>Status</th>
									<th class="text-center">Actions</th>
								</tr>
							</thead>

								<tbody>
								
								<tr>
									<?php if(count($venues) > 0) : ?>
										<?php foreach ($venues as  $venue):?>
										
										<td><?php echo $venue->name;?></td>
										<td><?php echo $venue->email;?></td>
										<td>
											<?php
												
											if ($venue->is_active == 1) {
												echo anchor("admin/btnstatus1/{$venue->user_id}/{$venue->is_active}", 'Active',['class'=>'btn btn-success','style'=>'width:50%;']);
													}
												else if($venue->is_active == 0){
													echo anchor("admin/btnstatus1/{$venue->user_id}/{$venue->is_active}", 'Inactive',['class'=>'btn btn-warning','style'=>'width:50%;']);
													}
											$venue->is_active;?>
										
										</td>
										
										<td class="text-center">
											<ul class="icons-list">
												<li class="dropdown">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown">
														<i class="icon-menu9"></i>
													</a>

													<ul class="dropdown-menu dropdown-menu-right">
														<li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
														<li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>
														<li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
													</ul>
												</li>
											</ul>
										</td>
									
									</tr>
									<?php endforeach; ?>
									<?php else :  ?>
										<tr>
											<td>No Record Found.!</td>
										</tr>
									<?php endif; ?>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- /page length options -->




					<!-- Footer -->
					 <?php $this->load->view('admin/includes/footer');?>
					<!-- /footer -->

				</div>
                    
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
