<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>admin_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>admin_assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>admin_assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>admin_assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>admin_assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->


    <!-- Theme JS files -->
    <script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/core/app.js"></script>
    <!-- /theme JS files -->

</head>

<body>
    <!-- Page container -->
    <div class="page-container login-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content"> 

                    <!-- Simple login form -->
                    <form action='<?php echo site_url("admin_login/recover_password/$row->id"); ?>' method="post">
                        <div class="panel panel-body login-form">
                            <div class="text-center">
                                <div class="icon-object border-slate-300 text-slate-300"><i class="icon-lock"></i></div>
                                <h5 class="content-group">Reset your Password <small class="display-block">Enter your credentials below</small></h5>
                            </div>

                            <div class="form-group has-feedback has-feedback-left">
                                <input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo $row->email; ?>" readonly>
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                                <?php echo form_error('email','<div style="color:#a94442;">', '</div>'); ?>
                            </div>
                            <div class="form-group has-feedback has-feedback-left">
                                <input type="password" class="form-control" name="password" placeholder="Password">
                                <div class="form-control-feedback">
                                    <i class="icon-lock text-muted"></i>
                                </div>
                                <?php echo form_error('password','<div style="color:#a94442;">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <button type="submit" name="reset" value="reset" class="btn btn-primary btn-block">Reset <i class="icon-circle-right2 position-right"></i></button>
                            </div>
                        </div>
                    </form>
                    <!-- /simple login form -->


                    <!-- Footer -->
                    <div class="footer text-muted">
                        &copy; <?php echo date('Y'); ?>. <a href="#">Showspoon</a> by <a href="http://www.mashkraft.com" target="_blank">Mashkraft</a>
                    </div>
                    <!-- /footer -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->

</body>
</html>