<!DOCTYPE html>
<!-- saved from url=(0049)http://165.227.110.185/showspoon/artist/dashboard -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Showspoon</title>
    <link rel="icon" type="image/icon" href="<?php echo site_url('assets/images/icon.png'); ?>">
    <link href="<?php echo site_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/custom-develop.css" rel="stylesheet">
    <!-- new wireframe style -->
    <link href="<?php echo site_url(); ?>assets/css/custom_style.css" rel="stylesheet">
    <!-- for bootstrap live search -->
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.css"> -->
    <!-- for ekko lightbox -->
    <link href="<?php echo site_url(); ?>assets/css/ekko-lightbox.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- for flexslider -->
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/flexslider.css" type="text/css" media="screen" />




    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="<?php echo site_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/select2.min.js"></script>
    <style>
    .navbar-toggle{background:none !important;border:1px solid #ab7b15 !important;}
    .navbar-toggle > .icon-bar{background:#ab7b15 !important;}
    .error{border:1px solid #a94442;}
    </style>
    <!-- for Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo site_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url(); ?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo site_url(); ?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo site_url('assets/favicon/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">
    <!--<script src="http://165.227.110.185/showspoon//assets/dist/js/bootstrap_multiselect.js"></script>-->
</head>

<body class="no-body-padd">
    <header class="main-header">
      <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">

        <!-- navbar for login link only -->
        <div class="clearfix">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
          <div class="float-left">
            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <img src="<?php echo site_url('assets/images/logo-n.png'); ?>" alt="Logo">
            </a>
          </div>
        </div>
          <!-- <div class="float-right">
            <a class="nav-btn" href="<?php echo base_url(); ?>account/login">Login</a>
          </div> -->
          <div class="collapse navbar-collapse" id="myNavbar">
          <div class="float-center">
          <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo site_url('about'); ?>">About</a></li>
          <li><a href="<?php echo site_url('showspoon-works'); ?>">How Showspoon Works</a></li>
          <li><a href="<?php echo site_url('contact'); ?>">Contact Us</a></li>
          <li><a href="<?php echo site_url('account/login'); ?>">Login</a></li>
          </ul>
          </div>
        </div>
        </div>
        <!-- navbar for login link only ends -->

        
        <!-- <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo site_url(); ?>">
              <img src="<?php echo site_url(); ?>assets/images/logo-n.png" alt="Logo">
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo site_url(); ?>Account/login">Login</a></li>
          </ul>
        </div> -->
      </div>
    </nav>
</header>  
    <!-- content area -->

    
    <div class="login_bg">
      <div class="container">
        <div class="row loginbox vertical-align">
                <div class="loginContainer">
          <h3 class="text-center m-t-lg m-b-lg">Reset your password</h3>
                    <form id="login" action="<?php echo site_url(); ?>account/recover_password/<?php echo $id; ?>" method="post">
                        <div class="well well-md m-b-none npt">
                          <?php if($this->session->flashdata('error')){ ?>
                           <div class="alert alert-danger alert-dismissible">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <strong>Warning!</strong> <?php echo $this->session->flashdata('error'); ?>
                            </div>
                            <?php } ?> 
                            <?php if($this->session->flashdata('success')){ ?>
                           <div class="alert alert-success alert-dismissible">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                            </div>
                            <?php } ?> 
                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control" value="<?php echo $email; ?>" placeholder="Your Email" readonly/>
                                <span id="emailMsg"></span>
                            </div><!-- form-group --> 
                            <div class="form-group">
                                <input type="password" name="password" id="password" class="form-control" placeholder="New Password" />
                                <span id="passwordMsg"></span>
                            </div><!-- form-group --> 
                            <div class="form-group">
                                <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Confirm Password" />
                                <span id="confirmPasswordMsg"></span>
                            </div><!-- form-group -->
                            <div class="form-group">
                                <input type="submit" id="submit" name="reset" value="Reset" class="btn btn-primary btn-block">
                            </div><!-- form-group --> 
                        </div>
                    </form>
                </div><!-- col-lg-4 -->
            </div><!-- row -->
      </div>
    </div>
    <!-- content area ends -->


    <!-- extra space -->
    <!-- <div class="clearfix" style="height: 100px;"></div> -->
    <!-- extra space ends-->


    <script src="<?php echo site_url(); ?>assets/js/pnotify.min.js"></script>

    <footer class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-left">
                    <p class="copyright">© <?php echo date('Y'); ?> Showspoon. All rights reserved.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                    <ul class="legallinks text-right">
                        <a href="<?php echo site_url('privacy-policy'); ?>">Privacy Policy</a><span class="sep "> |</span>
                        <a href="<?php echo site_url('terms'); ?>">Terms of Use </a><span class="sep "> |</span>
                        <a href="<?php echo site_url('contact'); ?>">Contact Us</a>
                    </ul>
                </div>
            </div>
            <!-- row -->
        </div>
    </footer>


    <!-- for bootstrap select -->
    <script src="<?php echo site_url(); ?>assets/js/bootstrap-select.js"></script>
    <!-- for smooth scroll -->
    <script type="text/javascript">
        // Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
   
   $(document).ready(function(){

   	  $('#login').submit(function(event){
          if($('#email').val()==""){

          	$('#email').addClass('error');
          	$('#emailMsg').text('This field is required').css('color','#a94442');
          }
          if($('#password').val()==""){

            $('#password').addClass('error');
            $('#passwordMsg').text('This field is required').css('color','#a94442');
          }
          if($('#confirm_password').val()==""){

            $('#confirm_password').addClass('error');
            $('#confirmPasswordMsg').text('This field is required').css('color','#a94442');
          }
          if($('#email').val()!="" && $('#password').val()!="" && $('#confirm_password').val()!=""){
          	$('#login').submit();
          }
   	  	  event.preventDefault();
   	  });

   	  $('#email').bind('change , keyup',function(){

   	  	 if($('#email').val()!=""){

   	  	 	$('#email').removeClass('error');
          	$('#emailMsg').text('');
   	  	 }
   	  });
      $('#password').bind('change , keyup',function(){

         if($('#password').val()!=""){

          $('#password').removeClass('error');
            $('#passwordMsg').text('');
         }
      });
      $('#confirm_password').bind('change , keyup',function(){

         if($('#confirm_password').val()!=""){

          $('#confirm_password').removeClass('error');
            $('#confirmPasswordMsg').text('');
         }
      });
   });
    </script>

</body>

</html>