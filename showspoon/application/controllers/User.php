<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class artist extends MY_Controller {

    private $loggedintime;
    private $user_data;
    private $data;

    function __construct() {

        parent::__construct();

        $this->data['title'] = 'admin';
        $this->data['page_title'] = "admin";
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model(array('Account_model','Artist_model','Venue_model','Notification_model'));
        $this->data['session_user']=$this->user_data;
        //$this->session->set_userdata('showspoon_artist.is_complete',1);

      
        /*$this->user_data = array_merge($this->user_data,['is_complete'=>0]);
        $this->session->set_userdata('showspoon_artist',$this->user_data);
        dd($this->user_data);*/
        //dd($this->user_data);
     

    }

    public function add_favourite()
    {
        if ($this->session->userdata('showspoon_artist')) {
        
    }
    public function index() {

        /*$query = $this->db->query("select * from abc");
        $query =$query->row();

        //log_message('info','hello...');
        die();
        $string='<div class="form-control">test message</div>';
        $string=html_escape($string);
        echo $string;
        die();*/
        // $this->data['title'] = 'Dashboard - Showspoon';



        $this->load->view('venue/dashboard', $this->data);
    }
    public function dashboard(){
        //redirect('artistProfile');
        $this->search();
    }
    public function search()
    {

        $this->data['genres'] = $this->Artist_model->get_genres();
        $this->data['cities'] = $this->Account_model->get_cities();
        $this->data['band_types'] = $this->Artist_model->get_band_types();

        $this->load->library("pagination");
        $genre = $this->input->get('genre')?$this->input->get('genre'):[];
        $band = $this->input->get('band')?$this->input->get('band'):'';
        $city = $this->input->get('city')?$this->input->get('city'):'';
        $sort = $this->input->get('sort')?$this->input->get('sort'):'name';
        $filter=array(
            'genre'=>$genre,
            'city_id'=>$city,
            'band_type_id'=>$band,
        );
        if($sort=='name'){
            $sort='name';
        }elseif($sort=='rating'){
            $sort='rating';
        }
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
        $count_record=$this->Venue_model->get_venues_count($filter);
        $config["base_url"] = site_url() . "/artist/search/";
        $config["total_rows"] = $count_record; 
        $config["per_page"] = 9;
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);

        $custom_pagination = $this->Artist_model->ci_custom_pagination($config["base_url"], $config["total_rows"], $config["per_page"], $config["uri_segment"]);

        $offset = $page*$config["per_page"];
        $this->pagination->initialize($custom_pagination);
        $this->data['artists']=$this->Venue_model->get_venues($filter,$offset,$config["per_page"],$sort,'ASC');
        //dd($this->data['artists']);
        $this->data["links"] = $this->pagination->create_links();
        $this->load->view('artist/search', $this->data);
    }


    public function logout(){

        $this->session->unset_userdata('showspoon_artist');
        @session_destroy();
        redirect('account/artist-login');
    }
    public function details($id=0){
        
        $this->load->model('Comments_model','comments');
        $venue = $this->Venue_model->get_venue_details_by_id($id);
        
        //dd($venue);
        
        if(!isset($venue->user_id)){
            show_404();
        }
        $this->data['venue']=$venue;
        $this->data['genres'] = $this->Artist_model->get_genres();
        $this->data['cities'] = $this->Account_model->get_cities();
        
        $user_id = isset($venue->user_id)?$venue->user_id:0;
        $this->data['gallery']=$this->Venue_model->get_venue_gallery($user_id);
        $this->data['videos']=$this->Venue_model->get_venue_videos($user_id,['YouTube']);
        $this->data['audios']=$this->Venue_model->get_venue_videos($user_id,['Vimeo']);
        $this->data['sound_cloud']=$this->Venue_model->get_venue_videos($user_id,['soundcloud']);
        
        $this->data['comments']=$this->comments->get_comments_venue($user_id);
        
        $this->load->view('artist/venue_details', $this->data);
    }
    public function get_artist_notifications(){
        /*$filter=[
            'to_id'=$user_id,
            'is_read'=0,
        ];
        $results = $this->Notification_model->get_artist_notifications($filter,0,10);*/
        
        
    }
}
