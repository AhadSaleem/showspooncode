<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account extends MY_Controller {

    private $loggedintime;
    private $admin_data;
    private $data=[];

    function __construct() {

        parent::__construct();

        $this->data['title'] = "Account";
        $this->data['page_title'] = "Account";
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model(array('Account_model','Artist_model','Venue_model'));
        //$this->lang->load('account','english');

        require_once(APPPATH . 'libraries/Facebook/autoload.php');

        $this->fb = new Facebook\Facebook([
            'app_id' => FACEBOOK_CLIENT_ID,
            'app_secret' => FACEBOOK_CLIENT_SECRET,
            'default_graph_version' => 'v2.2',
        ]);

        /*if (!$this->session->userdata('ShowspoonAdmin_logged_in')) {
            $this->session->set_flashdata('message', '');
            redirect(site_url('admin_login'));
        }

        $this->admin_data = $this->session->userdata('ShowspoonAdmin_logged_in');*/


    }

    public function index(){
        if ($this->session->userdata('showspoon_artist')) {
            redirect('artist/dashboard');
        }elseif ($this->session->userdata('showspoon_venue')) {
            redirect('venue/dashboard');
        }
        //$this->load->view('login');
        $this->load->view('landing_page');
    }
    public function _index(){
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() !== FALSE)
        {
            $email = $this->input->post('email')?$this->input->post('email'):'';
            $password = $this->input->post('password')?$this->input->post('password'):'';
            $row = $this->Account_model->get_auth($email,md5($password));
            if(isset($row->user_id))
            {

                if($row->status==0){
                    if($row->is_delete==0){
                        if($row->is_active==1){
                            $artist = $this->Artist_model->get_artist_details($row->user_id);
                            $sess_array = array(
                                'user_id' => isset($row->user_id)?$row->user_id:0,
                                'user_email' => isset($row->user_email)?$row->user_email:'',
                                'type_id' => isset($row->type_id)?$row->type_id:0,
                                'name' => isset($artist->name)?$artist->name:'',
                                'band_type_id' => isset($artist->band_type_id)?$artist->band_type_id:0,
                                'city_id' => isset($artist->city_id)?$artist->city_id:0,
                                'is_complete' => isset($artist->is_complete)?$artist->is_complete:0,
                                'artist_company_name' => isset($artist->artist_company_name)?$artist->artist_company_name:'',

                            );
                            $this->session->set_userdata('showspoon_artist', $sess_array);
                            //dd($sess_array);

                            //echo 'a';
                            redirect('artist/dashboard');
                        }else{
                            $this->data['error']=$this->lang->line('error_email_inactive',false);
                        }
                    }else{
                        $this->data['error']=$this->lang->line('error_email_delete',false);
                    }

                }else{
                    $this->data['error']=$this->lang->line('error_email_disable',false);
                }

            }else
            {
                $this->data['error']=$this->lang->line('error_auth',false);
            }
        }

        $this->load->view('artist/login', $this->data);
    }
    public function login()
    {
        //$this->Artist_Session();
        if ($this->session->userdata('showspoon_artist')) {
            redirect('artist');
        }elseif ($this->session->userdata('showspoon_venue')) {
            redirect('venue');
        }
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() !== FALSE)
        {
            $email = $this->input->post('email')?$this->input->post('email'):'';
            $password = $this->input->post('password')?$this->input->post('password'):'';
            $row = $this->Account_model->get_auth($email,md5($password));
            if(isset($row->user_id))
            { 

                if($row->status==0){
                    if($row->is_delete==0){
                        if($row->is_active==1){
                            if($row->type_id==1){
                                $artist = $this->Artist_model->get_artist_details($row->user_id);
                                $sess_array = array(
                                    'user_id' => isset($row->user_id)?$row->user_id:0,
                                    'user_email' => isset($row->user_email)?$row->user_email:'',
                                    'type_id' => isset($row->type_id)?$row->type_id:0,
                                    'profile_image' => isset($artist->profile_image)?$artist->profile_image:'',
                                    'name' => isset($artist->name)?$artist->name:'',
                                    'band_type_id' => isset($artist->band_type_id)?$artist->band_type_id:0,
                                    'city_id' => isset($artist->city_id)?$artist->city_id:0,
                                    'is_complete' => isset($artist->is_complete)?$artist->is_complete:0,
                                    'artist_company_name' => isset($artist->artist_company_name)?$artist->artist_company_name:'',

                                );
                                $this->session->set_userdata('showspoon_artist', $sess_array);

                                redirect('artist/dashboard');
                            }else{
                                $venue = $this->Venue_model->get_venue_details($row->user_id);
                                $sess_array = array(
                                    'user_id' => isset($row->user_id)?$row->user_id:0,
                                    'user_email' => isset($row->user_email)?$row->user_email:'',
                                    'type_id' => isset($row->type_id)?$row->type_id:0,
                                    'name' => isset($venue->name)?$venue->name:'',
                                    'profile_image' => isset($venue->profile_image)?$venue->profile_image:'',
                                    'city_id' => isset($venue->city_id)?$venue->city_id:0,
                                    'is_complete' => isset($venue->is_complete)?$venue->is_complete:0,
                                    'location' => isset($venue->location)?$venue->location:'',

                                );
                                $this->session->set_userdata('showspoon_venue', $sess_array);

                                redirect('venue/dashboard');
                            }
                        }else{
                            $this->data['error']=$this->lang->line('error_email_inactive',false);
                        }
                    }else{
                        $this->data['error']=$this->lang->line('error_email_delete',false);
                    }

                }else{
                    $this->data['error']=$this->lang->line('error_email_disable',false);
                }

            }else
            {
                $this->session->set_flashdata('error_auth','Invalid email or password');
                //$this->data['error']=$this->lang->line('error_auth',false);
            }
        }

        $this->load->view('artist/login', $this->data);
    }
    public function venue_login()
    {
        if ($this->session->userdata('showspoon_venue')) {
            redirect('venue');
        }
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() !== FALSE)
        {
            $email = $this->input->post('email')?$this->input->post('email'):'';
            $password = $this->input->post('password')?$this->input->post('password'):'';
            $row = $this->Account_model->get_auth($email,md5($password));
            if(isset($row->user_id))
            {

                if($row->status==0){
                    if($row->is_delete==0){
                        if($row->is_active==1){
                            $venue = $this->Venue_model->get_venue_details($row->user_id);
                            $sess_array = array(
                                'user_id' => isset($row->user_id)?$row->user_id:0,
                                'user_email' => isset($row->user_email)?$row->user_email:'',
                                'type_id' => isset($row->type_id)?$row->type_id:0,
                                'venue_name' => isset($venue->name)?$venue->name:'',
                                'profile_image' => isset($venue->profile_image)?$venue->profile_image:0,
                                'city_id' => isset($venue->city_id)?$venue->city_id:0,
                                'is_complete' => isset($venue->is_complete)?$venue->is_complete:0,
                                'location' => isset($venue->location)?$venue->location:'',

                            );
                            $this->session->set_userdata('showspoon_venue', $sess_array);
                            //dd($sess_array);

                            //echo 'a';
                            redirect('venue/dashboard');
                        }else{
                            $this->data['error']=$this->lang->line('error_email_inactive',false);
                        }
                    }else{
                        $this->data['error']=$this->lang->line('error_email_delete',false);
                    }

                }else{
                    $this->data['error']=$this->lang->line('error_email_disable',false);
                }

            }else
            {
                $this->data['error']=$this->lang->line('error_auth',false);
            }
        }

        $this->load->view('venue/login', $this->data);
    }

    public function forgot_password()
    {
        if($this->input->post("reset")=="Reset"){
            
            $email = $this->input->post('email');
            $row = $this->Account_model->check_reset_password($email);
            if(count($row) > 0){

                    $user_data = array(
                        'data'=>$row

                    );
                $email_body = $this->load->view('email/reset_password',$user_data,true);
                $this->Send_Mail($email,FROM_EMAIL,FROM_NAME,'Forgot Password',$email_body);
                $this->session->set_flashdata('success','Your email has been successfully send, check your mailbox.');
                redirect('account/forgot_password');
            }else{
               $this->session->set_flashdata('error','Invalid email address.');
               redirect('account/forgot_password');
            }
        }else{

            $this->load->view('forgot_password');
        }
    }
    public function recover_password($id=0)
    {   
        $email = $this->Account_model->get_email_for_recover($id);
        $data['id'] = $id;
        $data['email'] = $email;
        if($this->input->post("reset")=="Reset"){

            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $confirm_password = $this->input->post('confirm_password');
            if($password!=$confirm_password){
                $this->session->set_flashdata('error','Please confirm your password');
                redirect("account/recover_password/$id");
            }else{
            $this->Account_model->recover_password($email,md5($password));
            $this->session->set_flashdata('success','Your password has been changed successfully.');
            redirect("account/login");
           }
        }
        else{

            $this->load->view('recover_password',$data);
        }
    }
    public function signup()
    {
        if ($this->session->userdata('showspoon_artist')) {
            redirect('artist');
        }
        if($this->input->post("submit")=="Sign Up"){
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() !== FALSE)
        {
            $name = $this->input->post('name')?$this->input->post('name'):'';
            $email = $this->input->post('email')?$this->input->post('email'):'';
            $type = $this->input->post('type')?$this->input->post('type',true):1;
            $password = $this->input->post('password')?$this->input->post('password'):'';
            $row = $this->Account_model->get_user_by_email($email);
            if(!isset($row->user_id)){
                $token = mt_rand(1000,100000).time();
                $token = md5($token);
                $insert=array(

                    'user_token'=>$token,
                    'type_id'=>$type,
                    'user_email'=>$this->input->post('email')?$this->input->post('email'):'',
                    'user_password'=>$this->input->post('password')?md5($this->input->post('password')):'',
                );

                $insert_id = $this->Account_model->create_user($insert);
                if($type==1){
                    $artist=array(
                        'name'=>$name,
                        'user_id'=>$insert_id,
                        'email'=>$email,
                        'one'=>1,
                        'two'=>2,
                        'three'=>3,
                        'four'=>4,
                        'created_datetime'=>date('Y-m-d H:i:s',time()),
                    );
                    $artist_id = $this->Artist_model->create_artist($artist);
                    $artist_details=['artist_id'=>$artist_id];

                    $this->Artist_model->create_artist_details($artist_details);
                    $this->Artist_model->insert_payment_info($insert_id);


                    $news_link = site_url('account/user/'.$insert_id.'/?key='.$token);
                    $user_data = array(
                        'full_name'=>$name,
                        'link'=>$news_link,

                    );
                    $email_body = $this->load->view('email/signup', $user_data,true);

                    $this->Send_Mail($email,FROM_EMAIL,FROM_NAME,'Signup',$email_body);
                    //$this->Send_Mail($email,FROM_EMAIL,FROM_NAME,'Signup',$email_body);
                    $this->data['success']=$this->lang->line('account_created',false);
                    $this->session->set_flashdata('success','Thank you! Please check your email to confirm your account.');
                }else{
                    $venue=array(
                        'name'=>$name,
                        'user_id'=>$insert_id,
                        'email'=>$email,
                        'one'=>1,
                        'two'=>2,
                        'three'=>3,
                        'created_datetime'=>date('Y-m-d H:i:s',time()),
                    );
                    $artist_id = $this->Venue_model->create_venue($venue);
                    $this->Venue_model->insert_payment_info($insert_id);


                    $news_link = site_url('account/venue_user/'.$insert_id.'/?key='.$token);
                    $user_data = array(
                        'full_name'=>$name,
                        'link'=>$news_link,

                    );
                    $email_body = $this->load->view('email/signup', $user_data,true);


                    $this->Send_Mail($email,FROM_EMAIL,FROM_NAME,'Signup',$email_body);
                    $this->data['success']=$this->lang->line('account_created',false);
                    $this->session->set_flashdata('success','Thank you! Please check your email to confirm your account.');
                }

            }
            else{
                if(isset($row->status) && $row->status==1){
                    $this->data['error']=$this->lang->line('error_email_disable',false);
                }else if(isset($row->is_delete) && $row->is_delete==1){
                    $this->data['error']=$this->lang->line('error_email_delete',false);
                }else{
                    $this->data['error']=$this->lang->line('error_email_exists',false);   
                }

            }

        }
        redirect('account');
    }
    else{
        $this->load->view('artist/login', $this->data);
      }
    }
    public function sign_up()
    {
        if ($this->session->userdata('showspoon_artist')) {
            redirect('artist');
        }
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() !== FALSE)
        {
            $name = $this->input->post('name')?$this->input->post('name'):'';
            $email = $this->input->post('email')?$this->input->post('email'):'';
            $type = $this->input->post('type')?$this->input->post('type',true):1;
            $password = $this->input->post('password')?$this->input->post('password'):'';
            $row = $this->Account_model->get_user_by_email($email);
            if(!isset($row->user_id)){
                $token = mt_rand(1000,100000).time();
                $token = md5($token);
                $insert=array(

                    'user_token'=>$token,
                    'type_id'=>$type,
                    'user_email'=>$this->input->post('email')?$this->input->post('email'):'',
                    'user_password'=>$this->input->post('password')?md5($this->input->post('password')):'',
                );

                $insert_id = $this->Account_model->create_user($insert);
                if($type==1){
                    $artist=array(
                        'name'=>$name,
                        'user_id'=>$insert_id,
                        'email'=>$email,
                        'one'=>1,
                        'two'=>2,
                        'three'=>3,
                        'four'=>4,
                        'created_datetime'=>date('Y-m-d H:i:s',time()),
                    );
                    $artist_id = $this->Artist_model->create_artist($artist);
                    $artist_details=['artist_id'=>$artist_id];

                    $this->Artist_model->create_artist_details($artist_details);



                    $news_link = site_url('account/user/'.$insert_id.'/?key='.$token);
                    $user_data = array(
                        'full_name'=>$name,
                        'link'=>$news_link,

                    );
                    $email_body = $this->load->view('email/signup', $user_data,true);

                    $this->Send_Mail($email,FROM_EMAIL,FROM_NAME,'Signup',$email_body);
                    $this->data['success']=$this->lang->line('account_created',false);
                    $this->session->set_flashdata('success','Thank you! Please check your email to confirm your account.'); 
                }else{
                    $venue=array(
                        'name'=>$name,
                        'user_id'=>$insert_id,
                        'email'=>$email,
                        'one'=>1,
                        'two'=>2,
                        'three'=>3,
                        'created_datetime'=>date('Y-m-d H:i:s',time()),
                    );
                    $artist_id = $this->Venue_model->create_venue($venue);



                    $news_link = site_url('account/venue_user/'.$insert_id.'/?key='.$token);
                    $user_data = array(
                        'full_name'=>$name,
                        'link'=>$news_link,

                    );

                    $email_body = $this->load->view('email/signup', $user_data,true);
                    $this->Send_Mail($email,FROM_EMAIL,FROM_NAME,'Signup',$email_body);
                    $this->data['success']=$this->lang->line('account_created',false);
                    $this->session->set_flashdata('success','Thank you! Please check your email to confirm your account.');
                }

            }
            else{
                if(isset($row->status) && $row->status==1){
                    $this->data['error']=$this->lang->line('error_email_disable',false);
                }else if(isset($row->is_delete) && $row->is_delete==1){
                    $this->data['error']=$this->lang->line('error_email_delete',false);
                }else{
                    $this->data['error']=$this->lang->line('error_email_exists',false);   
                }

            }

        } 

         $this->load->view('artist/signup', $this->data);
    }
    public function venue_signup()
    {
        if ($this->session->userdata('showspoon_venue')) {
            redirect('venue');
        }
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() !== FALSE)
        {
            $name = $this->input->post('name')?$this->input->post('name'):'';
            $email = $this->input->post('email')?$this->input->post('email'):'';
            $password = $this->input->post('password')?$this->input->post('password'):'';
            $row = $this->Account_model->get_user_by_email($email);
            if(!isset($row->user_id)){
                $token = mt_rand(1000,100000).time();
                $token = md5($token);
                $insert=array(

                    'user_token'=>$token,
                    'type_id'=>2,
                    'user_email'=>$this->input->post('email')?$this->input->post('email'):'',
                    'user_password'=>$this->input->post('password')?md5($this->input->post('password')):'',
                );

                $insert_id = $this->Account_model->create_user($insert);
                $venue=array(
                    'name'=>$name,
                    'user_id'=>$insert_id,
                    'email'=>$email,
                    'created_datetime'=>date('Y-m-d H:i:s',time()),
                );
                $artist_id = $this->Venue_model->create_venue($venue);



                $news_link = site_url('account/venue_user/'.$insert_id.'/?key='.$token);
                $user_data = array(
                    'full_name'=>$name,
                    'link'=>$news_link,

                );
                $email_body = $this->load->view('email/signup', $user_data,true);


                $this->Send_Mail($email,FROM_EMAIL,FROM_NAME,'Signup',$email_body);
                $this->data['success']=$this->lang->line('account_created',false);

            }else{
                if(isset($row->status) && $row->status==1){
                    $this->data['error']=$this->lang->line('error_email_disable',false);
                }else if(isset($row->is_delete) && $row->is_delete==1){
                    $this->data['error']=$this->lang->line('error_email_delete',false);
                }else{
                    $this->data['error']=$this->lang->line('error_email_exists',false);   
                }

            }

        }
        $this->load->view('venue/signup', $this->data);
    }

    public function user($id=0){
        $key=$this->input->get('key')?$this->input->get('key',true):'';
        if($key!='' && $id>0){
            $msg= $this->lang->line('error_invalid_url',false);
            $row = $this->Account_model->get_user_by_id($id);
            if(isset($row->user_id)){
                $row = $this->Account_model->get_user_by_id($id);

                if($row->user_token==$key){
                    $update=['user_id'=>$row->user_id,'is_active'=>1,'user_token'=>md5(time())];
                    $this->Account_model->update_user($update);
                    $artist = $this->Artist_model->get_artist_details($row->user_id);
                    //dd($artist);
                    $user_data = array(
                        'full_name'=>isset($artist->name)?$artist->name:'',


                    );
                    $email_body = $this->load->view('email/welcome', $user_data,true);


                    $this->Send_Mail($row->user_email,FROM_EMAIL,FROM_NAME,'Signup',$email_body);


                    $sess_array = array(
                        'user_id' => isset($row->user_id)?$row->user_id:0,
                        'user_email' => isset($row->user_email)?$row->user_email:'',
                        'type_id' => isset($row->type_id)?$row->type_id:0,
                        'name' => isset($artist->name)?$artist->name:'',
                        'band_type_id' => isset($artist->band_type_id)?$artist->band_type_id:0,
                        'city_id' => isset($artist->city_id)?$artist->city_id:0,
                        'is_complete' => isset($artist->is_complete)?$artist->is_complete:0,
                        'artist_company_name' => isset($artist->artist_company_name)?$artist->artist_company_name:'',
                    );
                    $this->session->set_userdata('showspoon_artist', $sess_array);
                    //dd($sess_array);

                    //echo 'a';
                    redirect('artist/dashboard');


                }else{
                    show_error($msg);        
                }

            }else{

                show_error($msg);    
            }
        }else{
            show_404();
        }
    }

    public function venue_user($id=0){
        $key=$this->input->get('key')?$this->input->get('key',true):'';
        if($key!='' && $id>0){
            $msg= $this->lang->line('error_invalid_url',false);
            $row = $this->Account_model->get_user_by_id($id);
            if(isset($row->user_id)){
                $row = $this->Account_model->get_user_by_id($id);

                if($row->user_token==$key){
                    $update=['user_id'=>$row->user_id,'is_active'=>1,'user_token'=>md5(time())];
                    $this->Account_model->update_user($update);
                    $venue = $this->Venue_model->get_venue_details($row->user_id);
                    //dd($artist);
                    $user_data = array(
                        'full_name'=>isset($venue->name)?$venue->name:'',


                    );
                    $email_body = $this->load->view('email/welcome', $user_data,true);
                    $this->Send_Mail($row->user_email,FROM_EMAIL,FROM_NAME,'Signup',$email_body);



                    $sess_array = array(
                        'user_id' => isset($row->user_id)?$row->user_id:0,
                        'user_email' => isset($row->user_email)?$row->user_email:'',
                        'type_id' => isset($row->type_id)?$row->type_id:0,
                        'venue_name' => isset($venue->name)?$venue->name:'',
                        'profile_image' => isset($venue->profile_image)?$venue->profile_image:0,
                        'city_id' => isset($venue->city_id)?$venue->city_id:0,
                        'is_complete' => isset($venue->is_complete)?$venue->is_complete:0,
                        'location' => isset($venue->location)?$venue->location:'',

                    );
                    $this->session->set_userdata('showspoon_venue', $sess_array);
                    //dd($sess_array);

                    //echo 'a';
                    redirect('venue/dashboard');


                }else{
                    show_error($msg);        
                }

            }else{

                show_error($msg);    
            }
        }else{
            show_404();
        }
    }


    public function facebook_signup() {

        $user=$this->input->get('type')?$this->input->get('type'):1;
        $page=$this->input->get('page')?$this->input->get('page'):'signup';

        if($user==1){
            $session_type = 'showspoon_artist';
        }else{
            $session_type = 'showspoon_venue';
        }



        $helper = $this->fb->getRedirectLoginHelper();

        try {

            $session = $helper->getAccessToken();
        } catch (FacebookRequestException $ex) {
            // When Facebook returns an error
        } catch (Exception $ex) {
            // When validation fails or other local issues
        }

        if (isset($session)) {


            try {
                // Returns a `Facebook\FacebookResponse` object
                $response = $this->fb->get('/me?fields=id,first_name,last_name,email,gender', $session);
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            $user_profile = $response->getGraphUser();


            $email = $user_profile['email'];
            $first_name = $user_profile['first_name'];
            $last_name = $user_profile['last_name'];
            $gender = $user_profile['gender'];
            //$request_page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 'signup';
            //$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : 'client';

            //$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : 'client-signup';
            $result = $this->Account_model->get_user_by_email($email);


            if (!isset($result->user_id) || (isset($result->user_id) && $result->status==0 && $result->is_delete==0)) {


                //dd($result);
                $data = array(
                    'user_email' => $email,
                    'is_active' => 1,
                    //'first_name' => $first_name,
                    //'last_name' => $last_name,
                    //'type' => $user_type,
                    //'ip_address' => $this->input->ip_address(),
                );
                $profile_id = isset($user_profile['id']) ? $user_profile['id'] : 0;
                $prof_pic = 'http://graph.facebook.com/' . $profile_id . '/picture?type=normal';

                if ($prof_pic != '') {

                    $pic_name = time() . mt_rand(0, 50000);
                    $main_image = $pic_name . '.jpg';

                    copy($prof_pic, './uploads/users/' . $main_image);
                    copy($prof_pic, './uploads/users/thumb/' . $main_image);
                    $data['user_image'] = $main_image;
                    //$data['image_thumb']=$thumb_image;
                    if(isset($result->user_id) && $result->user_image!=''){
                        @unlink('./uploads/users/'.$result->user_image);
                        @unlink('./uploads/users/thumb/'.$result->user_image);
                    }

                }

                if (isset($result->user_id)) {
                    $data['user_id'] = $result->user_id;

                    $this->Account_model->update_user($data);
                    $user_id = $result->user_id;
                } else {

                    if($user==1){
                        $data['type_id']=1;
                    }else{
                        $data['type_id']=2;
                    }


                    $user_id = $result = $this->Account_model->create_user($data);
                    if ($user == 1) {
                        $artist=array(
                            'name'=>$first_name.' '.$last_name,
                            'user_id'=>$user_id,
                            'email'=>$email,
                            'created_datetime'=>date('Y-m-d H:i:s',time()),
                        );
                        $artist_id = $this->Artist_model->create_artist($artist);
                        $artist_details=['artist_id'=>$artist_id];
                        $this->Artist_model->create_artist_details($artist_details);
                    }else{
                        $venue=array(
                            'name'=>$first_name.' '.$last_name,
                            'user_id'=>$user_id,
                            'email'=>$email,
                            'created_datetime'=>date('Y-m-d H:i:s',time()),
                        );
                        $artist_id = $this->Venue_model->create_venue($venue);
                    }
                    $user_data = array(
                        'full_name'=>$first_name.' '.$last_name
                    );
                    $email_body = $this->load->view('email/welcome', $user_data,true);
                    $this->Send_Mail($email,FROM_EMAIL,FROM_NAME,'Signup',$email_body);

                }



                $row = $this->Account_model->get_user_by_id($user_id);

                if($user==1){
                    $artist = $this->Artist_model->get_artist_details($row->user_id);
                    $sess_array = array(
                        'user_id' => isset($row->user_id)?$row->user_id:0,
                        'user_email' => isset($row->user_email)?$row->user_email:'',
                        'type_id' => isset($row->type_id)?$row->type_id:0,
                        'name' => isset($artist->name)?$artist->name:'',
                        'band_type_id' => isset($artist->band_type_id)?$artist->band_type_id:0,
                        'city_id' => isset($artist->city_id)?$artist->city_id:0,
                        'is_complete' => isset($artist->is_complete)?$artist->is_complete:0,
                        'artist_company_name' => isset($artist->artist_company_name)?$artist->artist_company_name:'',

                    );
                    $this->session->set_userdata('showspoon_artist', $sess_array);
                    redirect('artist/dashboard');

                }else{
                    $venue = $this->Venue_model->get_venue_details($row->user_id);
                    $sess_array = array(
                        'user_id' => isset($row->user_id)?$row->user_id:0,
                        'user_email' => isset($row->user_email)?$row->user_email:'',
                        'type_id' => isset($row->type_id)?$row->type_id:0,
                        'venue_name' => isset($venue->name)?$venue->name:'',
                        'profile_image' => isset($venue->profile_image)?$venue->profile_image:0,
                        'city_id' => isset($venue->city_id)?$venue->city_id:0,
                        'is_complete' => isset($venue->is_complete)?$venue->is_complete:0,
                        'location' => isset($venue->location)?$venue->location:'',

                    );
                    $this->session->set_userdata('showspoon_venue', $sess_array);

                    redirect('venue/dashboard');
                }


            }else
            {
                if(isset($row->status) && $row->status==1){

                    $this->session->set_flashdata('error', $this->lang->line('error_email_disable',false));
                }else if(isset($row->is_delete) && $row->is_delete==1){

                    $this->session->set_flashdata('error', $this->lang->line('error_email_delete',false));
                }


                $this->account_redirect($user,$page);



            }
        } else {
            if (isset($_REQUEST['error_reason']) && $_REQUEST['error_reason'] == 'user_denied') {
                $this->session->set_flashdata('error', $this->lang->line('error_went_wrong',false));
                $this->account_redirect($user,$page);
            }
            $helper = $this->fb->getRedirectLoginHelper();
            $permissions = ['email']; // Optional permissions
            $loginUrl = $helper->getLoginUrl(site_url('/account/facebook_signup/?type='.$user.'&page='.$page), $permissions);
            header("Location: $loginUrl");
            exit();

        }
    }

    private function account_redirect($user=1,$page='signup'){

        if($page=='signup'){
            redirect('account/signup');
        } else {
            redirect('account/login');
        }

    }


    public function google_signup($old_user = 0, $artist_id = 0) {


        @session_start();

        $user=$this->input->get('user')?$this->input->get('user'):'';
        $page=$this->input->get('page')?$this->input->get('page'):'';

        $user_type = 0;


        include_once APPPATH . '/libraries/Google/autoload.php';

        //Insert your cient ID and secret
        //You can get it from : https://console.developers.google.com/
        $client_id = GOOGLE_API_KEY;
        $client_secret = GOOGLE_API_SECRET;
        $redirect_uri = base_url() . 'account/google_signup/?user='.$user.'&page='.$page;


        $client = new Google_Client();
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_uri);
        $client->addScope("email");
        $client->addScope("profile");


        $service = new Google_Service_Oauth2($client);


        if (isset($_GET['code'])) {
            $client->authenticate($_GET['code']);
            unset($_SESSION['access_token']);
            $_SESSION['access_token'] = $client->getAccessToken();
            header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
            exit;
        }

        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $client->setAccessToken($_SESSION['access_token']);
        } else {
            $authUrl = $client->createAuthUrl();
        }

        if (isset($authUrl)) {
            //show login url
            header("Location: $authUrl");
        } else {
            $user = $service->userinfo->get(); //get user info






            $get_access_token = $_SESSION['access_token'];
            $resutl_token = json_decode($get_access_token);
            $resutl_token->access_token;
            $access_token = $resutl_token->access_token;


            $email = $user->email;
            if (isset($user->email)) {
                $result = $this->Account_model->get_user_by_email($email);


                if (!isset($result->user_id) || (isset($result->user_id) && $result->status==0 && $result->is_delete==0)) {


                    $data = array(
                        'user_email' => $email,
                        'is_active' => 1,
                        //'first_name' => $first_name,
                        //'last_name' => $last_name,
                        //'type' => $user_type,
                        //'ip_address' => $this->input->ip_address(),
                    );
                    $prof_pic = isset($user->picture) ? $user->picture : '';
                    if ($prof_pic != '') {

                        $pic_name = time() . mt_rand(0, 50000);
                        $main_image = $pic_name . '.jpg';
                        $thumb_image = $pic_name . '_thumb.jpg';
                        copy($prof_pic, './uploads/users/' . $main_image);
                        copy($prof_pic, './uploads/users/thumb/' . $thumb_image);
                        $data['image'] = $main_image;
                        //$data['image_thumb']=$thumb_image;
                    }
                    if (isset($result->user_id)) {
                        $data['user_id'] = $result->user_id;

                        $this->Account_model->update_user($data);
                        $user_id = $result->user_id;
                    } else {

                        if($user=='artist'){
                            $data['type_id']=1;
                        }else{
                            $data['type_id']=2;
                        }


                        $user_id = $result = $this->Account_model->create_user($data);
                        if ($user == 'artist') {
                            $artist=array(
                                'name'=>$first_name.' '.$last_name,
                                'user_id'=>$user_id,
                                'email'=>$email,
                                'created_datetime'=>date('Y-m-d H:i:s',time()),
                            );
                            $artist_id = $this->Artist_model->create_artist($artist);
                        }else{
                            $venue=array(
                                'name'=>$first_name.' '.$last_name,
                                'user_id'=>$user_id,
                                'email'=>$email,
                                'created_datetime'=>date('Y-m-d H:i:s',time()),
                            );
                            $artist_id = $this->Venue_model->create_venue($venue);
                        }
                        $user_data = array(
                            'full_name'=>$first_name.' '.$last_name
                        );
                        $email_body = $this->load->view('email/welcome', $user_data,true);
                        $this->Send_Mail($email,FROM_EMAIL,FROM_NAME,'Signup',$email_body);

                    }



                    $row = $this->Account_model->get_user_by_id($user_id);
                    //dd($row);

                    if($user=='artist'){
                        $artist = $this->Artist_model->get_artist_details($row->user_id);
                        $sess_array = array(
                            'user_id' => isset($row->user_id)?$row->user_id:0,
                            'user_email' => isset($row->user_email)?$row->user_email:'',
                            'type_id' => isset($row->type_id)?$row->type_id:0,
                            'name' => isset($artist->name)?$artist->name:'',
                            'band_type_id' => isset($artist->band_type_id)?$artist->band_type_id:0,
                            'city_id' => isset($artist->city_id)?$artist->city_id:0,
                            'is_complete' => isset($artist->is_complete)?$artist->is_complete:0,
                            'artist_company_name' => isset($artist->artist_company_name)?$artist->artist_company_name:'',

                        );
                        $this->session->set_userdata('showspoon_artist', $sess_array);
                        redirect('artist/dashboard');

                    }else{
                        $venue = $this->Venue_model->get_venue_details($row->user_id);
                        $sess_array = array(
                            'user_id' => isset($row->user_id)?$row->user_id:0,
                            'user_email' => isset($row->user_email)?$row->user_email:'',
                            'type_id' => isset($row->type_id)?$row->type_id:0,
                            'venue_name' => isset($venue->name)?$venue->name:'',
                            'profile_image' => isset($venue->profile_image)?$venue->profile_image:0,
                            'city_id' => isset($venue->city_id)?$venue->city_id:0,
                            'is_complete' => isset($venue->is_complete)?$venue->is_complete:0,
                            'location' => isset($venue->location)?$venue->location:'',

                        );
                        $this->session->set_userdata('showspoon_venue', $sess_array);

                        redirect('venue/dashboard');
                    }


                }else
                {
                    if(isset($row->status) && $row->status==1){

                        $this->session->set_flashdata('error', $this->lang->line('error_email_disable',false));
                    }else if(isset($row->is_delete) && $row->is_delete==1){

                        $this->session->set_flashdata('error', $this->lang->line('error_email_delete',false));
                    }


                    $this->account_redirect($user,$page);



                }



            } else {
                $this->session->set_flashdata('error', $this->lang->line('error_went_wrong',false));

                $this->account_redirect($user,$page);
            }
        }
    }

    public function email_exists()
    {
        $email = ($this->input->post('email'))?$this->input->post('email'):'';
        $data  = $this->Account_model->email_exists($email);
        echo $data;
    }
    public function logout(){

        $this->session->unset_userdata('showspoon_artist');
        $this->session->unset_userdata('showspoon_venue');
        @session_destroy();
        redirect('account/login');
    }
    public function privacy_policy(){

        $this->load->view('privacy_policy');
    }
    public function terms(){
        $this->load->view('terms');
    }

    public function contact(){
        $this->load->library('form_validation');
        if($this->input->post('submit')=="Send"){
            $this->form_validation->set_rules('name', 'name', 'required');
            $this->form_validation->set_rules('email', 'email', 'valid_email|required');
            $this->form_validation->set_rules('message', 'message', 'required');
            $this->form_validation->set_rules('g-recaptcha-response', 'g-recaptcha-response', 'callback_captcha_validation');
            if($this->form_validation->run() == FALSE){
                $this->load->view('contact');
            }else{
            
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $message = $this->input->post('message');
            $user_data = array(
                        'name'=>$name,
                        'email'=>$email,
                        'message'=>$message

                    ); //support@showspoon.com
                $email_body = $this->load->view('email/contact_us',$user_data,true);
                $this->Send_Mail('ahad.mashkraft@gmail.com',FROM_EMAIL,FROM_NAME,'Contact Email',$email_body);
                $this->session->set_flashdata('success','Your message has been sent successfully.');
                redirect("account/contact");
        }
       }else{
        $this->load->view('contact');
       }
    }
    public function captcha_validation(){
        $secret_key = '6LcbK1sUAAAAAEwFrQkPFnXZu8GVvzFONgaYQrSE'; // change this to yours
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . $secret_key . '&response='.$_POST['g-recaptcha-response'];
        $response = @file_get_contents($url);
        $data = json_decode($response, true);
        if($data['success'])
        {
            return true;
        }
        else
        {
            $this->form_validation->set_message('captcha_validation', 'Please confirm you are human');
            return false;
        }
    }
    public function about(){

        $this->load->view('about');
    }
    public function showspoon_works(){

        $this->load->view('showspoon_works');
    }
}

