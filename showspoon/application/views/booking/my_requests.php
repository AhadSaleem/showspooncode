<!DOCTYPE html>
<html lang="en">
    <head>

        <?php $this->load->view('includes/head');?>
        <link href="<?php echo base_url();?>/assets/dist/plugins/iCheck/all.css" rel="stylesheet">
        <script src="<?php echo base_url();?>/assets/dist/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/plugins/select2/select2.min.js"></script>
        <!--<script src="<?php echo base_url();?>/assets/dist/js/bootstrap_multiselect.js"></script>-->

        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.time.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.date.js"></script>

        <!--<script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/legacy.js"></script>-->
    </head>
    <body>
    

        <?php $this->load->view('includes/header');
        $class = $this->router->fetch_class();
        $method = $this->router->fetch_method();

        ?>
<form id="paypal" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" ></form>



        <div class="container-fluid ListViewv2 lightgrey">
            <div class="container">
                <div class="row">

                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 SideBar">
                        <br class="hidden-sm hidden-xs" />
                        <br class="hidden-sm hidden-xs" />
                        <br class="hidden-sm hidden-xs" />
                        <a href="<?php echo site_url($class.'/my_requests');?>" class="btn btn-default m-b-sm active">Invitation Sent</a>
                        <div class="clearfix hidden-sm hidden-xs"></div>
                        <a href="<?php echo site_url($class.'/request_received');?>" class="btn btn-default m-b-sm">Request Received</a>
                        <br /><br />
                    </div><!-- Sidebar -->

                    <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12 InviteList">
                        <h3 class="m-t-none m-b-lg">Invitation Sent</h3>
                        <div class="row">
                        <div id="message"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive">
                                    <table class="table bookings">
                                        <tbody>
                                            <?php
                                            if(isset($requests) && count($requests)>0){
                                                foreach($requests as $row){
                                                    //dd($row);

                                            ?>
                                            <tr class="m-t-md">

                                                <td>
                                                    <?php
                                                    $user_image=site_url('assets/dist/images/noimage2.jpg');
                                                    if(isset($row->profile_image) && $row->profile_image!=''){

                                                        $user_image=site_url('uploads/users/thumb/'.$row->profile_image);
                                                    }
                                                    $booking_date = isset($row->event_date)?date('jS M, Y',strtotime($row->event_date)):'';
                                                    $time= isset($row->time)?$row->time:'';
                                                    $amount= isset($row->amount)?number_format($row->amount,0):'';
                                                 
                                if ($row->who_pay == 1 && $row->type==1 || $row->who_pay==1 && $row->type==2) {
                                                        $will_pay = "I will pay";
                                                    }
                                                    if($row->who_pay==2 && $row->type==1){
                                                        $will_pay = "Venue will pay";
                                                    }
                                                    if($row->who_pay==1 && $row->type==2){
                                                        $will_pay = "I will pay";
                                                    }
                                                    if($row->who_pay==2 && $row->type==2){
                                                        $will_pay = "Artist will pay";
                                                    }
                                                
                                                    ?>
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <a href="#">
                                                                <img class="media-object img-circle" src="<?php echo $user_image;?>">
                                                            </a>
                                                        </div>
                                                        <div class="media-body" style="width:auto;">
                                                            <h4 class="media-heading text-left m-b-xs l-h"><?php echo isset($row->name)?$row->name:''; ?></h4>
                                                            <?php
                                                    $party_genres = [];
                                                    //echo $class;
                                                    //dd($row);
                                                    if($row->type==2){
                                                        
                                                        $party_genres = $row->genre;
                                                    }else{
                                                        $party_genres = $row->genre;
                                                    }
                                                    if(isset($party_genres) && $party_genres!=''){
                                                        $genrex = explode(',',$party_genres);

                                                        foreach($genrex as $gen){
                                                            $gen_name=isset($genres[$gen])?$genres[$gen]:'';
                                                            if($gen_name!=''){
                                                                echo '<span class="badge cbadge">'.$gen_name.'</span> ';    
                                                            }

                                                        }
                                                    }
                                                            ?>

                                                        </div>
                                                    </div>
                                                </td>
                                                <td><p class="m-b-none"><?php echo $booking_date;?><br /><?php echo $time;?></p></td>
                                                <td><p class="m-b-none">Request Sent</p></td>
                                                <td><p class="m-b-none text-center">$<?php echo $amount;?>
                                                    <small class="col-sm-12 no-gutter text-sm m-t-n-xs"><?php echo $will_pay;?></small></p></td>
                                                <td><a href="<?php echo site_url($class.'/chat/'.$row->id);?>" class="btn btn-default">Chat</a></td>
                                                <td><a  data-id="<?php echo $row->from_id?>"
                                                data-id1 = "<?php echo $row->id?>" class="btn btn-default payment">Payment</a></td>
                                            </tr>
                                            <?php }}else{?>
                                            <tr>
                                                <td colspan="5" align="center">No record found.</td>
                                            </tr>
                                            <?php }?>



                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div><!-- InviteList -->
                </div><!-- row -->
            </div><!-- container -->
        </div><!-- container-fluid -->

<?php $this->load->view('includes/footer');?>

        <script src="<?php echo base_url(); ?>assets/dist/plugins/iCheck/icheck.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
        <script>
            $(".payment").click(function(){

               var from_id =  $(this).data("id");
               var book_id = $(this).data("id1");

               $.ajax({
                   url: '<?php echo site_url($class.'/payment')?>',
                   type: 'POST',
                   dataType: 'json',
                   data: {from_id:from_id,book_id:book_id},
                   success:function(data){
                   // alert(data.data);
                    if(data.data){

                        $('#paypal').html('<div>'+data.data+'</div>');
                        $('#paypal_submit').trigger('click');
                            
                        }
                       
                   }
               });                
            });
        </script>
        <script>
            function backPage(form_hide,form_show){
                $('#'+form_hide).hide();
                $('#'+form_show).show();
            }
            $(document).ready(function () {


                $('.pickadate').pickadate({
                    formatSubmit: 'yyyy/mm/dd',
                    format: 'yyyy-mm-dd',
                    today: '',
                    close: '',
                    clear: '',
                    min: [<?php echo date('Y');?>,<?php echo date('m');?>,<?php echo date('d',strtotime('+1 day',time()));?>]
                });
                $('.pickatime').pickatime();
                $('#btn-1').on('click', function () {
                    $("form#form-1").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            email: {
                                required: true,
                            }                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            //form.submit();
                            $('#form-1').hide();
                            $('#form-2').show();
                            $('input').iCheck({
                                checkboxClass: 'icheckbox_square',
                                radioClass: 'iradio_square',
                                increaseArea: '20%' // optional
                            });
                        }
                    });
                });

                $('#btn-2').on('click', function () {
                    $("form#form-2").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            amount: {
                                required: true,
                                number:true
                            }
                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            //form.submit();
                            $('#form-2').hide();
                            $('#form-3').show();
                        }
                    });
                });

                $('#btn-3').on('click', function () {
                    $("form#form-3").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            amount: {
                                required: true,
                                number:true
                            }
                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            //form.submit();
                            //$('#form-2').hide();
                            //$('#form-3').show();
                            var booking_name = $('#name').val();
                            var booking_date = $('#date').val();
                            var booking_time = $('#time').val();
                            var booking_hours = $('#hours').val();
                            var booking_description = $('#description').val();
                            var booking_amount = $('#amount').val();
                            var booking_pay = $('.who_pay:checked').val();



                            $('#hidden_name').val(booking_name);
                            $('#hidden_date').val(booking_date);
                            $('#hidden_time').val(booking_time);
                            $('#hidden_hours').val(booking_hours);
                            $('#hidden_amount').val(booking_amount);
                            $('#hidden_who_pay').val(booking_pay);
                            $('#hidden_venue').val(<?php echo isset($venue->user_id)?$venue->user_id:0;?>);
                            $('#hidden_form').submit();


                        }
                    });
                });
            });
        </script>
        <style>
            .search-btn{
                border-radius: 5px;
                margin-top: 29px;
                padding: 10px 60px;
            }
        </style>
        
    </body>
</html>