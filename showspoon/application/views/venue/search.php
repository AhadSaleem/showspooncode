<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('includes/head');?>
        <script src="<?php echo base_url();?>/assets/dist/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/plugins/select2/select2.min.js"></script>
        <!--<script src="<?php echo base_url();?>/assets/dist/js/bootstrap_multiselect.js"></script>-->
    </head>
    <body>

        <?php $this->load->view('includes/header');?>


        <div class="container">
            <div class="row">
                <?php $this->load->view('includes/parts/artist-search-form');?>					
            </div><!-- row -->
        </div><!-- container -->

        <div class="container-fluid ListView search-listing">
            <div class="container">
                <div class="row">

                    <?php


                    if(isset($artists) && count($artists)>0){

                        foreach($artists as $artist){
                    ?>
                    <?php $this->load->view('includes/parts/artist-part',['artist'=>$artist]); ?>					

                    <?php }?>
                    <div class="clearfix"></div>
                    <div class="text-center">
                        <?php if(isset($links) && $links!=''){
                        echo $links;
                    }?>
                    </div>
                    <?php }else{?>
                    <div class="col-sm-12">
                        <p>No artist found.</p>
                    </div>
                    <?php }?>



                </div><!-- row -->
            </div><!-- container -->
        </div><!-- container-fluid -->

<?php $this->load->view('includes/footer');?>

        <script>
            $(document).ready(function() {
                $('.likebtn').on('click',function(e){
                    e.preventDefault();
                    var obj_this=$(this);
                    if($(this).hasClass('active')){
                        var btn_action='remove';
                    }else{
                        var btn_action='add';
                    }
                    var row_id = $(this).data('id');
                    $.ajax({
                        type:'POST',
                        url: '<?php echo site_url('venue/add_favourites');?>',
                        data:{row_id:row_id,action:btn_action},
                        dataType:'json',


                        success:function(data){
                            if(data.data){
                                var old_count = $('li a.heart').text();
                                if(obj_this.hasClass('active')){
                                    old_count--;
                                    $('li a.heart').text(old_count);
                                    //obj_this.text('Add to Favourite');
                                    obj_this.removeClass('active');
                                }else{
                                    obj_this.addClass('active');
                                    old_count++;
                                    $('li a.heart').text(old_count);
                                    //obj_this.text('Remove Favourite');
                                }
                            }

                        },
                        error: function(data){

                        }
                    });
                });
                $(".select").select2({
                    width: '100%'
                });
                $("#sort").select2({
                    minimumResultsForSearch: -1,
                    width: '100%'
                });
                /*$('.select').multiselect({
                includeSelectAllOption: true,
                selectAllText: 'Check all!',
                numberDisplayed: 0
                });*/
            });
        </script>
        <style>
            .search-btn{
                border-radius: 5px;
                margin-top: 23px;
                padding: 10px 60px;
            }
        </style>
    </body>
</html>