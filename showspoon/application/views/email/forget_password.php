<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Showspoon</title>
        <link href="<?php echo site_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css">
        <link href="<?php echo site_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="<?php echo site_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo site_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo site_url(); ?>assets/css/select2.min.css" rel="stylesheet">
        <link href="<?php echo site_url(); ?>assets/css/custom-develop.css" rel="stylesheet">
    </head>
    <body>
        <div class="container" style="width:420px;margin:auto;">
        <div class="panel panel-default" style="padding:10px;">
        
        <a href="<?php echo site_url(); ?>admin_login/recover_password/<?php echo $data->id; ?>" class="btn btn-warning btn-block">Click here to recover your password</a>
        <br><br>



        Thank you, <br>
        Showspoon Team <br>
         <br>
        www.showspoon.com <br>
        <br>
        <a href="<?php echo site_url('Account/login');?>"><img src="<?php echo site_url('assets/images/logo-n.png');?>" width="200" alt="logo"></a>
    </div>
    </div>
    </body>
</html>