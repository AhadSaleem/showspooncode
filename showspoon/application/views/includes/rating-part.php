<?php //dd($comment);
$name=isset($comment->name)?$comment->name:'';
?>
   <div class="col-sm-12 col-xs-12 m-b-md">
    <div class="media m-b-sm">
        <div class="media-left">
            <span class="NameCircle orange"><?php echo First_Letter($name);?></span>
        </div>
        <div class="media-body">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 UserRating ratingstars">
               <?php echo star_rating($comment->rating);?>
                
            </div><!-- col-sm-12 -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t-sm">
                <h4 class="media-heading text-md"><?php echo $name;?> | <?php echo date('M d, Y',strtotime($comment->created_datetime));?></h4>
            </div>
        </div>
    </div><!-- media -->
    <p><?php echo isset($comment->comment)?$comment->comment:'';?> </p>
</div>