<!DOCTYPE html>
<!-- saved from url=(0049)http://165.227.110.185/showspoon/artist/dashboard -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Artist Details</title>
    <!-- for Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo site_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url(); ?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo site_url(); ?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo site_url('assets/favicon/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">
    <style type="text/css" media="screen">
.back-to-top:hover{text-decoration:none;}
.star-rating {
  font-size: 0;
  white-space: nowrap;
  display: inline-block;
  /* width: 250px; remove this */
  height: 50px;
  overflow: hidden;
  position: relative;
  background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjREREREREIiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
  background-size: contain;
}
.star-rating i {
  opacity: 0;
  position: absolute;
  left: 0;
  top: 0;
  height: 100%;
  /* width: 20%; remove this */
  z-index: 1;
  background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjRkZERjg4IiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
  background-size: contain;
}
.star-rating input {
  -moz-appearance: none;
  -webkit-appearance: none;
  opacity: 0;
  display: inline-block;
  /* width: 20%; remove this */
  height: 100%;
  margin: 0;
  padding: 0;
  z-index: 2;
  position: relative;
}
.star-rating input:hover + i,
.star-rating input:checked + i {
  opacity: 1;
}
.star-rating i ~ i {
  width: 40%;
}
.star-rating i ~ i ~ i {
  width: 60%;
}
.star-rating i ~ i ~ i ~ i {
  width: 80%;
}
.star-rating i ~ i ~ i ~ i ~ i {
  width: 100%;
}
::after,
::before {
  height: 100%;
  padding: 0;
  margin: 0;
  box-sizing: border-box;
  text-align: center;
  vertical-align: middle;
}

.star-rating.star-5 {width: 250px;}
.star-rating.star-5 input,
.star-rating.star-5 i {width: 20%;}
.star-rating.star-5 i ~ i {width: 40%;}
.star-rating.star-5 i ~ i ~ i {width: 60%;}
.star-rating.star-5 i ~ i ~ i ~ i {width: 80%;}
.star-rating.star-5 i ~ i ~ i ~ i ~i {width: 100%;}

.star-rating.star-3 {width: 150px;}
.star-rating.star-3 input,
.star-rating.star-3 i {width: 33.33%;}
.star-rating.star-3 i ~ i {width: 66.66%;}
.star-rating.star-3 i ~ i ~ i {width: 100%;}
    </style>
    <link href="<?php echo site_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/custom-develop.css" rel="stylesheet">
    <!-- new wireframe style -->
    <link href="<?php echo site_url(); ?>assets/css/custom_style.css" rel="stylesheet">
    <!-- for bootstrap live search -->
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.css"> -->
    <!-- for lightbox -->
    <link href="<?php echo site_url(); ?>assets/css/lightgallery.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/lightbox.min.css" rel="stylesheet">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="<?php echo site_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/select2.min.js"></script>
    <!--<script src="http://165.227.110.185/showspoon//assets/dist/js/bootstrap_multiselect.js"></script>-->
</head>

<body>
<span id="alert"></span>


    <header class="main-header">
        <!-- Static navbar -->
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>

            <!-- <button type="button" class="navbar-toggle collapsed filter_btn" data-toggle="collapse" data-target="#sidebar" aria-expanded="false" aria-controls="sidebar">
              <i class="fa fa-filter" aria-hidden="true"></i>
            </button> -->


            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <img src="<?php echo site_url(); ?>assets/images/logo-n.png" alt="Logo">
            </a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="search">
                  <!-- search form -->
                  <form id="search" onsubmit="return false;">
                     <input type="search" placeholder="Search">
                  </form>
                  <!-- search form ends-->
              </li>
              <li><a href="<?php echo site_url(); ?>">Artists</a></li>
              <li><a href="<?php echo site_url('venueBooking/booking'); ?>">My bookings</a></li>
              <li><a href="<?php echo site_url('venueBooking/inbox'); ?>">Inbox <span id="my_bookings" class="badge badge_upper inbox-count"></span></a></li>
              <li><a href="<?php echo site_url('venueBooking/my_requests'); ?>">Requests <span id="my_bookings" class="badge badge_upper request-count"></span></a></li>
              <li class="favourite"><a href="<?php echo site_url("venue/favourties"); ?>" class="heart"></a></li>
              <li>
                <div class="inset dropdown"> <?php if($image!=""){$img = site_url()."uploads/users/thumb/".$image; }else{$img = site_url()."assets/blank.png";} ?>                 
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img class="profile_img" src="<?php echo $img; ?>"> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url('venueProfile'); ?>">Profile</a></li>
                  <li><a href="<?php echo site_url('account/logout'); ?>">Logout</a></li>
                </ul>
                </div>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container -->
      </nav>

    </header>

     <!-- content area -->
    <div class="">
        <!-- banner area -->
        <div class="banner_area">
            <figure class="mainbanner">
                <?php
                    $user_image=site_url('assets/images/noimage2.jpg');
                    if(isset($venue->profile_image) && $venue->profile_image!=''){
                        $user_image=site_url('uploads/users/thumb/'.$venue->profile_image);
                    }
                    ?>
                <img src="<?php echo $user_image; ?>" alt="Banner">
            </figure>
            <div class="profile_action_area">
                <figure class="profile_pic">
                    <img src="<?php echo $user_image; ?>" alt="Banner">
                </figure>
                <div class="spacer">
                    <a href="<?php echo site_url(); ?>venueBooking/book/<?php echo $venue->artist_id; ?>" class="btn btn-primary">
                        Booking Request
                    </a>
                </div>
                <div class="spacer">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" type="button">
                        Send a Message
                    </button>
                </div>
                <div class="spacer" style="position:relative;">
                    <div class="share_artist_area" style="position:absolute;top:0;width:100%;">                
                <ul>
      
                    <li>
                        <!-- <a href="<?php echo site_url(); ?>">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a> -->
                        <div class="fb-share-button" data-href="http://165.227.110.185/showspoon2/venue/details/<?php echo $url_id; ?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F165.227.110.185%2Fshowspoon2%2Fartist%2Fdetails%2F33&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
                    </li>
                    <li>
                        <!-- <a href="#">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a> -->
                        <div style="height:23px;position:relative;top:6px;">
                        <a class="twitter-share-button"
                          href="https://twitter.com/share"
                          data-size="small"
                          data-text="Showspoon"
                          data-url="http://165.227.110.185/showspoon2/venue/details/<?php echo $url_id; ?>"
                          data-related="twitterapi,twitter">
                        Tweet
                        </a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>                     
                    </li>
                    <!-- <li>
                        <a href="#">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </a>
                    </li> -->
                </ul>
            </div>
                </div>
            </div>
     
        </div>
        <!-- banner area ends  -->
        <div class="container">
            <div class="profile_content_area">
                <div class="row">
                    <!-- left content area -->
                    <div class="col-lg-9 ">
                        <div class="profile_lead_area">
                            <div class="row">
                                <div class="col-lg-7 col-lg-offset-5">
                                    <div class="spacer">
                                        <h1 class="profile_title">
                                            <?php 
                                            $fav = explode(',',$favourites); 
                                            if(in_array($venue->artist_id,$fav)){
                                              ?><i class="fa fa-heart" aria-hidden="true"></i><?php 
                                            }else{echo '';}
                                            ?>
                                            <?php echo isset($venue->name)?$venue->name:'';?>
                                        </h1>
                                        <div class="clearfix spacer text-capitalize ArtistTags_wrap">
                                            <div class="ArtistTags">
                                                <?php if(isset($venue->genre) && $venue->genre!=''){
                                                $genres_idx = explode(',',$venue->genre);
                                                foreach($genres_idx as $gen){?>
                                                <a href="<?php echo site_url('artist/dashboard'); ?>"><span class="badge m-b-sm"><?php echo isset($genres[$gen])?$genres[$gen]:'';?></span></a>
                                                <?php } } ?>
                                            </div>
                                        </div>
                                    </div><!--/.spacer-->
                                    <div class="profile_specs_area_lead">
                                        <div class="row">
                                          <?php if($venue->capacity!=""){ ?>
                                            <div class="col-sm-6">
                                                <div class="spacer_mini">
                                                    capacity <span class="text-black"><?php echo $venue->capacity; ?></span>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <?php if($average->avg!=""){ ?>
                                            <div class="col-sm-6">
                                                <div class="spacer_mini">
                                                    <?php for($i=1;$i<=$average->avg;$i++){ ?><i class="fa fa-star" aria-hidden="true"></i><?php } ?>  <span class="text-black"><?php echo $average->avg; ?> average rating</span>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <?php if($venue->age!=""){ ?>
                                            <div class="col-sm-6">
                                                <div class="spacer_mini">
                                                    age  <span class="text-black"><?php echo $venue->age ;?></span>
                                                </div>    
                                            </div>
                                            <?php } ?>
                                            <?php if($venue->CityName!=""){ ?>
                                            <div class="col-sm-6">
                                                <div class="spacer_mini">
                                                    city  <span class="text-black"><?php echo $venue->CityName; ?></span>
                                                </div>    
                                            </div>
                                            <?php } ?> 
                                            <?php if($venue->address!=""){ ?>
                                            <div class="col-sm-6">
                                                <div class="spacer_mini">
                                                    address  <span class="text-black"><?php echo $venue->address; ?></span>
                                                </div>    
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div><!-- profile_specs_area_lead -->
                                    <div class="profile_specs_area_sub">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="profile_specs_area_sub_handle second"> 
                                                    <div class="spacer">
                                                        <h3 class="text-capitalize">
                                                            followers
                                                        </h3>
                                                    </div>
                                                    <ul class="ico_list">
                                                      <?php if($venue->instragram!=""){ ?>
                                                        <li>
                                                            <span><i class="fa fa-instagram" aria-hidden="true"></i></span>
                                                            <?php 
                                                            // $url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20html%20where%20url=%27https://www.instagram.com/USERNAME/%27%20and%20xpath=%27/html/body/script[1]%27&format=json";
                                                            //     $otherPage = 'instagram';
                                                            //     $response = file_get_contents("https://www.instagram.com/$otherPage/?__a=1");
                                                            //     if ($response !== false) {
                                                            //         $data = json_decode($response, true);
                                                            //         if ($data !== null) {
                                                            //             $follows = $data['user']['follows']['count'];
                                                            //             $followedBy = $data['user']['followed_by']['count'];
                                                            //             echo @number_format($follows);
                                                            //         }
                                                            //     }

                                                            ?>
                                                            <a href="<?php echo $venue->instragram; ?>" target="_blank"><small>Followers</small></a>
                                                        </li>
                                                        <?php }else{echo '-';} ?>
                                                        <?php if($venue->facebook!=""){ ?>
                                                        <li>
                                                            <span><i class="fa fa-facebook" aria-hidden="true"></i></span>
                                                            <?php 
                                                            $feed_url = "https://graph.facebook.com/$venue->facebook/?fields=fan_count,talking_about_count,name&access_token=1788504024704883|c555699cb45bf777721e04641309573a";
                                                            $c = curl_init();
                                                            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
                                                            curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
                                                            curl_setopt($c, CURLOPT_URL, $feed_url);
                                                            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
                                                            $data = curl_exec($c);
                                                            curl_close($c);
                                                            $result = json_decode($data); 
                                                            echo @number_format($result->fan_count);
                                                            ?>
                                                            <a href="<?php echo $venue->facebook; ?>" target="_blank"><small>Likes</small></a>
                                                        </li>
                                                        <?php }else{echo '-';} ?>
                                                        <?php if($venue->twitter!=""){ ?>
                                                        <li>
                                                            <span><i class="fa fa-twitter" aria-hidden="true"></i></span>
                                                            <?php 
                                                                function buildBaseString($baseURI, $method, $params) {
                                                                $r = array();
                                                                ksort($params);
                                                                foreach ($params as $key => $value) {
                                                                    $r[] = "$key=" . rawurlencode($value);
                                                                }
                                                                return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
                                                            }
                                                            function buildAuthorizationHeader($oauth) {
                                                                $r = 'Authorization: OAuth ';
                                                                $values = array();
                                                                foreach ($oauth as $key => $value)
                                                                    $values[] = "$key=\"" . rawurlencode($value) . "\"";
                                                                $r .= implode(', ', $values);
                                                                return $r;
                                                            }
                                                            function returnTweet($screen_name, $limit){
                                                            $oauth_access_token = T_OAUTH_ACCESS_TOKEN;
                                                            $oauth_access_token_secret = T_OAUTH_ACCESS_TOKEN_SECRET;
                                                            $consumer_key = T_CONSUMER_KEY;
                                                            $consumer_secret = T_CONSUMER_SECRET;
                                                            $twitter_timeline = "user_timeline";  //  mentions_timeline / user_timeline / home_timeline / retweets_of_me
                                                            //  create request
                                                            $request = array(
                                                                'screen_name' => "$screen_name",
                                                                'count' => "$limit",
                                                            );
                                                            $oauth = array(
                                                                'oauth_consumer_key' => $consumer_key,
                                                                'oauth_nonce' => time(),
                                                                'oauth_signature_method' => 'HMAC-SHA1',
                                                                'oauth_token' => $oauth_access_token,
                                                                'oauth_timestamp' => time(),
                                                                'oauth_version' => '1.0'
                                                             );
                                                            $oauth = array_merge($oauth, $request);

                                                            //  do some magic
                                                            $base_info = buildBaseString("https://api.twitter.com/1.1/statuses/$twitter_timeline.json", 'GET', $oauth);
                                                            $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
                                                            $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
                                                            $oauth['oauth_signature'] = $oauth_signature;

                                                            //  make request
                                                            $header = array(buildAuthorizationHeader($oauth), 'Expect:');
                                                            $options = array(CURLOPT_HTTPHEADER => $header,
                                                                             CURLOPT_HEADER => false,
                                                                             CURLOPT_URL => "https://api.twitter.com/1.1/statuses/$twitter_timeline.json?" . http_build_query($request),
                                                                             CURLOPT_RETURNTRANSFER => true,
                                                                             CURLOPT_SSL_VERIFYPEER => false);

                                                            $feed = curl_init();
                                                            curl_setopt_array($feed, $options);
                                                            $json = curl_exec($feed);
                                                            curl_close($feed);
                                                            return $json;
                                                            }
                                                            $exp_name = explode('/',$venue->twitter);
                                                            $name = $exp_name[3];
                                                            $data = returnTweet($name, '100');
                                                            $data_tw_feed = json_decode($data); 
                                                            echo number_format($data_tw_feed[0]->user->followers_count);
                                                            ?>
                                                            <a href="<?php echo $venue->twitter; ?>" target="_blank"><small>Followers</small></a>
                                                        </li>
                                                        <?php }else{echo '-';} ?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="profile_specs_area_sub_handle second">
                                                    <div class="spacer">
                                                        <h3 class="text-capitalize">
                                                            links
                                                        </h3>
                                                    </div>
                                                    <ul class="ico_list">
                                                      <?php if($venue->website!=""){ ?>
                                                        <li>
                                                            <span><i class="fa fa-globe" aria-hidden="true"></i></span>
                                                            <a href="<?php echo $venue->website; ?>" target="_blank"><small>website link</small></a>
                                                        </li>
                                                        <?php }else{echo '-';} ?>
                                                        <?php if($venue->soundcloud!=""){ ?>
                                                        <li>
                                                            <span><i class="fa fa-soundcloud" aria-hidden="true"></i></span>
                                                            <a href="<?php echo $venue->soundcloud; ?>" target="_blank"><small>SoundCloud</small></a>
                                                        </li>
                                                        <?php }else{echo '-';} ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div><!--/.row-->
                                    </div><!-- profile_specs_area_sub -->
                                </div>
                            </div>
                        </div><!--/.profile_lead_area-->
                        <!-- quick nav -->
                        <div class="quick_nav clearfix">
                            <span class="pull-left">quick nav:</span>
                            <ul>
                                <li>
                                    <a href="#quick_about">about</a>
                                </li>
                                <li>
                                    <a href="#quick_gallery">gallery</a>
                                </li>
                                <li>
                                    <a href="#quick_tech_rider">Technical rider</a>
                                </li>
                                <li>
                                    <a href="#quick_rates">rates</a>
                                </li>                            
                                <li>
                                    <a href="#quick_policy">policy</a>
                                </li>
                                <li>
                                    <a href="#quick_reviews">reivews</a>
                                </li>
                            </ul>
                        </div>
                        <!-- quick nav end -->
                        <a class="jumper-link" name="quick_about">&nbsp;</a>
                        <div class="spacer_big">
                            <div class="linear_title clearfix">
                                <h1>about the artist</h1>
                                <div class="back_to_top">
                                   <a id="" href="#" class="back-to-top back-to-top" role="button"  data-toggle="tooltip" data-placement="left">Back to Top&nbsp; <span class="glyphicon glyphicon-chevron-up"></span></a>
                                </div>
                            </div><!-- linear_title -->
                            <div class="content_area">
                                <?php 
                                if($venue->biography!=""){ ?>
                                <p><?php echo $venue->biography; ?></p>
                                <?php }else{
                                  ?><p class="text-center"><?php echo 'No description found'; ?></p><?php 
                                }
                                ?>
                            </div>
                        </div><!-- spacer_big -->




                        <a class="jumper-link" name="quick_gallery">&nbsp;</a>
                        <div class="spacer_big">
                            <div class="linear_title clearfix">
                                <h1>Image gallery <small>(<?php echo $artist_gallery_count->cnt; ?>)</small></h1>
                                <div class="back_to_top">
                                   <a id="" href="#" class="back-to-top back-to-top" role="button"  data-toggle="tooltip" data-placement="left">Back to Top&nbsp; <span class="glyphicon glyphicon-chevron-up"></span></a>
                                </div>
                            </div><!-- linear_title -->
                            <div class="content_area">
                            <span class="image_gallery"></span>
                            <span class="image_page"></span> 
                            </div>
                        </div>



                        <a class="jumper-link" name="quick_gallery">&nbsp;</a>
                        <div class="spacer_big">
                            <div class="linear_title clearfix">
                                <h1>Videos <small>(<?php echo $video_count->vd_cnt; ?>)</small></h1>
                                <div class="back_to_top">
                                   <a id="" href="#" class="back-to-top back-to-top" role="button"  data-toggle="tooltip" data-placement="left">Back to Top&nbsp; <span class="glyphicon glyphicon-chevron-up"></span></a>
                                </div>
                            </div><!-- linear_title -->
                            <div class="content_area">
                            <span class="video"></span>
                            <span class="video_page"></span> 
                            </div>
                        </div>




                        <a class="jumper-link" name="quick_tech_rider">&nbsp;</a>
                        <div class="spacer_big">
                            <div class="linear_title clearfix">
                                <h1>technical rider</h1>
                                <div class="back_to_top">
                                   <a id="" href="#" class="back-to-top back-to-top" role="button"  data-toggle="tooltip" data-placement="left">Back to Top&nbsp; <span class="glyphicon glyphicon-chevron-up"></span></a>
                                </div>
                            </div><!-- linear_title -->
                            <div class="content_area">
                                <?php 
                                if($venue->technical_rider!=""){ ?>
                                <p><?php echo $venue->technical_rider; ?></p>
                                <?php }else{
                                  ?><p class="text-center"><?php echo 'No rider found'; ?></p><?php 
                                }
                                ?>
                            </div>
                        </div><!-- spacer_big -->




                         <a class="jumper-link" name="quick_rates">&nbsp;</a>
                        <div class="spacer_big">
                            <div class="linear_title clearfix">
                                <h1>Rates</h1>
                                <div class="back_to_top">
                                   <a id="" href="#" class="back-to-top back-to-top" role="button"  data-toggle="tooltip" data-placement="left">Back to Top&nbsp; <span class="glyphicon glyphicon-chevron-up"></span></a>
                                </div>
                            </div><!-- linear_title -->
                            <div class="content_area">
                                <div class="row">
                                    <!-- <div class="col-sm-6">
                                        <div class="spacer rate_box">
                                            Ticket Sales
                                            <strong>33%</strong>
                                            <span class="grey-text">
                                                Cut of Sales
                                            </span>
                                        </div>
                                    </div> -->
                                    <div class="col-sm-12">
                                        <div class="spacer rate_box">
                                           <!--  Rental -->
                                            <?php 
                                            function convertCurrency($amount, $from, $to){
                                              $conv_id = "{$from}_{$to}";
                                              $string = file_get_contents("http://free.currencyconverterapi.com/api/v3/convert?q=$conv_id&compact=ultra");
                                              $json_a = json_decode($string, true);

                                              return $amount * round($json_a[$conv_id], 2);
                                            }
                                            if(isset($venue->artist_rate) && $venue->artist_rate!="" && $venue->artist_currency!=""){ 
                                              ?><strong><?php echo(convertCurrency($venue->artist_rate, $venue->artist_currency, $currency))." ".$currency; ?> <small> / day</small></strong><?php 
                                            }else{echo '<p class="text-center">No rate found</p>';}
                                            ?> 
                                            
                                            <!-- <strong>$400 <small>/ hour</small></strong> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- spacer_big -->




                         <a class="jumper-link" name="quick_policy">&nbsp;</a>
                        <div class="spacer_big">
                            <div class="linear_title clearfix">
                                <h1>cancellation policy</h1>
                                <div class="back_to_top">
                                   <a id="" href="#" class="back-to-top back-to-top" role="button"  data-toggle="tooltip" data-placement="left">Back to Top&nbsp; <span class="glyphicon glyphicon-chevron-up"></span></a>
                                </div>
                            </div><!-- linear_title -->
                            <div class="content_area">
                                <?php $policy = cancellation_policy(); if($policy->c_policy!=""){ ?>
                                <p><?php echo $policy->c_policy; ?></p>
                                <?php }else{
                                  ?><p><?php echo 'No cancellation policy found'; ?></p><?php 
                                }

                                ?>
                            </div>
                        </div><!-- spacer_big -->




                        <a class="jumper-link" name="quick_reviews">&nbsp;</a>
                        <div class="spacer_big">
                            <div class="linear_title clearfix">
                                <h1>reviews <small>(<?php echo $review_count->rc; ?>)</small></h1>
                                <div class="back_to_top">
                                   <a id="" href="#" class="back-to-top back-to-top" role="button"  data-toggle="tooltip" data-placement="left">Back to Top&nbsp; <span class="glyphicon glyphicon-chevron-up"></span></a>
                                </div>
                            </div><!-- linear_title -->
                            <div class="content_area">
                              <?php /*if($average->avg > 0){ ?>
                               <div class="average_rating_box clearfix">
                                   <strong><?php echo isset($average->avg)?$average->avg:0; ?>/5</strong>
                                   average rating
                               </div><!-- average_rating_box -->
                               <?php }else{echo '';}*/ ?>
                               <!-- media  -->
                               <div class="review_box">
                                <div class="row">
                                    <?php foreach($comments as $comment){ ?> 
                                    <div class="col-md-2 col-sm-4">
                                        <figure>
                                            <img src="<?php echo site_url(); ?>uploads/users/thumb/<?php echo $comment->profile_image; ?>" alt="Thumb">
                                        </figure>
                                        <figcaption>
                                            <?php echo isset($comment->name)?$comment->name:''; ?>
                                            <ul class="rating_star">
                                                <li>
                                                <?php echo star_rating($comment->rating); ?>
                                                </li>
                                            </ul>
                                        </figcaption>
                                    </div> 
                                    <div class="col-md-10 col-sm-8">
                                        <h3>
                                            <?php echo isset($comment->comment)?$comment->comment:''; ?>
                                        </h3>
                                        <p style="text-align:justify;"><?php echo $comment->short_description; ?></p>
                                        <small><?php echo date('M d, Y',strtotime($comment->created_datetime));?></small>
                                    </div>
                                    <?php } ?>
                                </div>
                                </div>
                               <!-- media ends -->
                            </div>
                        </div><!-- spacer_big --> 
                        <form action="#">
                        <p id="comment_alert"></p>
                        <h3>Please Rate</h3>
                        <span class="star-rating star-5">
                          <input type="radio" name="rating" value="1"><i></i>
                          <input type="radio" name="rating" value="2"><i></i>
                          <input type="radio" name="rating" value="3"><i></i>
                          <input type="radio" name="rating" value="4"><i></i>
                          <input type="radio" name="rating" value="5"><i></i>
                        </span>
                        <p id="successStars"></p>
                        <div class="form-group"> 
                        <label class="control-label"><h3>Add Reviews</h3></label>
                        <textarea name="comment" class="form-control" id="comment" rows="7" style="resize:none;" required="required"></textarea>
                        <span id="errorMsg"></span>
                        </div>
                        <div class="form-group">
                        <button type="button" id="comment_btn" class="btn btn-primary">Submit</button>
                        </div>
                        </form>
                    </div>
                    <!-- left content area ends  -->
                    


                    <!-- right bar -->
                    <div class="col-lg-3">
                        <div class="profile_spacer"></div>
                        <div class="spacer_sixty">
                            <h4 class="text-capitalize">
                                featured artist
                            </h4>
                            <div class="row">
                                <?php if(isset($featured) && $featured!="") {
                                foreach($featured as $item){
                                ?>
                                <div class="col-sm-6">
                                    <div class="venue_box">
                                        <figure>
                                            <img src="<?php echo site_url(); ?>uploads/users/thumb/<?php echo $item->profile_image; ?>" alt="Thumb">
                                        </figure>
                                        <figcaption>
                                            <a href="<?php echo $item->artist_id; ?>"><?php echo $item->artist_name; ?></a>
                                        </figcaption>
                                    </div>
                                </div><!-- col-sm-6 -->
                                <?php }} ?>
                            </div>
                        </div><!-- spacer_sixty -->
                        <div class="spacer_sixty">
                            <h4 class="text-capitalize">
                                Similar venues
                            </h4>
                            <div class="row">
                                <?php 
                                $explode = explode(',',$venue->genre); 
                                $this->load->model('Venue_model');
                                $data = $this->Artist_model->get_similar_vanues($explode);
                                foreach($data as $item)
                                {
                                    ?>
                                    <div class=" col-sm-6">
                                    <div class="venue_box">
                                        <figure>
                                            <img src="<?php echo site_url(); ?>uploads/users/thumb/<?php echo $item->profile_image; ?>" alt="Thumb">
                                        </figure>
                                        <figcaption>
                                            <a href="<?php echo $item->artist_id; ?>"><?php echo $item->name; ?></a>
                                        </figcaption>
                                    </div>
                                    </div>
                                    <?php 
                                }
                                ?>
                            </div>
                        </div><!-- spacer_sixty -->

                        <?php if(count($recently_booked) > 0 && $recently_booked!=""){ ?>
                        <div class="spacer_sixty">
                            <h4 class="text-capitalize">
                                Recently Booked
                            </h4>
                            <div class="row">
                                <?php foreach($recently_booked as $booked){ ?>
                                <div class="col-sm-6">
                                    <div class="venue_box">
                                        <figure>
                                            <img src="<?php echo site_url(); ?>uploads/users/thumb/<?php echo $booked->profile_image; ?>" alt="Thumb">
                                        </figure>
                                        <figcaption>
                                            <a href="#"><?php echo $booked->name; ?></a>
                                        </figcaption>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div><!-- spacer_sixty -->
                        <?php } ?>
                    </div>
                    <!-- right bar ends -->
                </div>
            </div>
        </div>
    </div> 
    <!-- content area ends -->


    <!-- extra space -->
    <div class="clearfix" style="height: 50px;"></div>
    <!-- extra space ends-->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Send a Message</h3>
      </div>
      <div class="modal-body">
        <form action="" method="post" id="message_send_form">
        <div class="form-group">
        <span id="send_msg"></span>
        </div>
        <input type="hidden" name="artist_id" id="artist_id" value="<?php echo $venue->user_id; ?>">
        <input type="hidden" name="user_id" id="user_id" value="<?php echo $sender; ?>">
        <div class="form-group">
        <input type="text" name="subject" id="subject" class="form-control" placeholder="Your subject">
        <span id="subject-error"></span>
        </div>
        <div class="form-group">
        <textarea name="message_body" id="message_body" class="form-control" placeholder="Your message"></textarea>
        <span id="message-error"></span>
        </div>
        <div class="form-group">
        <button type="submit" name="send" class="btn btn-primary" value="Send">Send</button>
        </div>
        </form>
      </div>
    </div>

  </div>
</div>

    <script src="<?php echo site_url(); ?>assets/js/pnotify.min.js"></script>
    <div class="spacer_hundred"></div>
    <footer class="main-footer" style="position:fixed;bottom:0;left:0;right:0;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-left">
                    <p class="copyright">© <?php echo date('Y'); ?> Showspoon. All rights reserved.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                    <ul class="legallinks text-right">
                        <a href="<?php echo site_url('privacy-policy'); ?>">Privacy Policy</a><span class="sep "> |</span>
                        <a href="<?php echo site_url('terms'); ?>">Terms of Use </a><span class="sep "> |</span>
                        <a href="<?php echo site_url('contact'); ?>">Contact Us</a>
                    </ul>                
                  </div>
            </div>
            <!-- row -->
        </div>
    </footer>  
    <style>.red{border:1px solid #a94442;}</style>
    <input type="hidden" id="id" value="<?php echo $id; ?>"> 
    <script> 
    $(document).ready(function(){ 
      $.ajax({
      url:'<?php echo site_url("venue/getHeartCount"); ?>',
      type:'POST',
      success:function(data){
        setTimeout(function(){
        $('li.favourite a.heart').text(data);
        },500);
      }
    });
    $('#message_send_form').submit(function(e){
      if($('#subject').val()==""){
        $('#subject').addClass('red');
        $('#subject-error').text('This field is required.').css('color','#a94442');
      }
      if($('#message_body').val()==""){
        $('#message_body').addClass('red');
        $('#message-error').text('This field is required.').css('color','#a94442');
      }
      if($('#subject').val()!="" && $('#message_body').val()!=""){
        $.ajax({
          url:'<?php echo site_url("venue/send_message"); ?>',
          type:'POST',
          data:$(this).serialize(),
          success:function(data){
            $('#send_msg').html(data);
            $('#message_send_form')[0].reset();
            setTimeout(function(){
              $('#send_msg').html('');
              $("#myModal").removeClass("in");
              $(".modal-backdrop").remove();
              $('.modal').modal('hide');
            },2000);
          }
        });
      }
      e.preventDefault();
      });
    $('#subject').bind('keyup , change',function(){
      $('#subject').removeClass('red');
      $('#subject-error').text('');
    });
    $('#message_body').bind('keyup , change',function(){
      $('#subject').removeClass('red');
      $('#message-error').text('');
    });
          function getVenueRequestCount(){
          $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistRequestCount"); ?>',
                        type    :  'POST',
                        dataType:  'JSON',
                        success :  function(data){

                          if(data.status=='request' && data.cnt > 0 && data.is_read==0){
                            $('.request-count').html(data.cnt);
                          }
                        }
                   });
       }
       getVenueRequestCount(); 
        // var clear = setInterval(function(){
        // getVenueRequestCount(); 
        // },100);
      function getArtistInboxCount(){
        $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistInboxCount"); ?>',
                        type    :  'POST',
                        dataType:  'JSON',
                        success :  function(data){ 

                          if(data.status=='message' && data.cnt > 0 && data.is_read==0){
                            $('.inbox-count').html(data.cnt);
                          }
                        }
                   });
      }
      getArtistInboxCount();
      function getVenueNotification(){
         $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistNotification"); ?>',
                        type    :  'POST',
                        success :  function(data){

                            $('#alert').html(data);
                        }
                   });
      }
      getVenueNotification();
      $(document).on('click','.close',function(){
        var id = $(this).data('id');
        var mr = $(this).data('mr');
        $.ajax({
                        url     :  '<?php echo site_url("venueBooking/deleteArtistNotification"); ?>',
                        type    :  'POST',
                        data    :  {id:id},
                        success :  function(data){
                          if(mr=='request'){
                            $('.request-count').text('');
                          }else if(mr=='message'){
                            $('.inbox-count').text('');
                          }
                        }
                   });
      });
      $('input[type="radio"]').change(function(){
        var rating  = $(this).val(); 
        $.ajax({
          url:'<?php echo site_url("venue/please_rate"); ?>',
          type:'POST',
          data:{venue_id:'<?php echo $venue->user_id; ?>',sender_id:'<?php echo $sender; ?>',rating:rating},
          success:function(data){
            $('#successStars').html(data);
            setTimeout(function(){
              $('#successStars').html('');
            },2000);
          }
        });
      });
          $('#comment_btn').click(function(){
             if($('#comment').val()==""){$('#errorMsg').text('This field is required').css('color','#a94442');} 
             //if(!$('input[type="radio"]').is(':checked')){$('#errorStars').text('Rating is required').css('color','#a94442');}
             //$('input[type="radio"]').each(function(){
                 
                 if($('#comment').val()!="")
                 {
                    var comment = $('#comment').val();
                    var rating  = $(this).val();
                    var receive = $('#id').val();
                    $.ajax({
                                url       :    '<?php echo site_url("Venue/reviews"); ?>',
                                type      :    'POST',
                                data      :    {comment:comment,rating:rating,receive:receive,sender:'<?php echo $sender; ?>',type:2},
                                success   :    function(result)
                                {
                                    $('#comment').val('');
                                    $('#comment_alert').html(result);
                                }
                          }); 
                 }
             //});
          }); 
          $('#comment').bind('keyup , change',function(){

              $('#errorMsg').text('');
          });
          $('input[type="radio"]').change(function(){
              
              $('#errorStars').text('');
          });
          
          function get_gallery(page){ 
          var id = $('#id').val();  
          $.ajax({
                     url     :  '<?php echo site_url(); ?>Venue/get_gallery/' + page,
                     type    :  'POST',
                     dataType:  'JSON',
                     data    :  {id:id},
                     success :  function(data){

                         $('.image_gallery').html(data.gallery);
                         $('.image_page').html(data.pagination_link);
                     }
                });
      }
      get_gallery(1);
      function get_videos(video){ 
          var id = $('#id').val();  
          $.ajax({
                     url     :  '<?php echo site_url(); ?>Venue/get_videos/' + video,
                     type    :  'POST',
                     dataType:  'JSON',
                     data    :  {id:id},
                     success :  function(data){

                         $('.video').html(data.video);
                         $('.video_page').html(data.video_link);
                     }
                });
      }
      get_videos(1);
      $(document).on("click", ".pagination li a", function(event){
      event.preventDefault();
      var page = $(this).data("ci-pagination-page");
      get_gallery(page);
     });
    });
    </script>
    <!-- for bootstrap select -->
    <script src="<?php echo site_url(); ?>assets/js/bootstrap-select.js"></script>
    <!-- for smooth scroll -->
    <script type="text/javascript">
        // Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
    </script>

    <!-- for back to top -->
    <script type="text/javascript">
    $(document).ready(function(){

      $(document).on('keyup','input[type="search"]',function(event){

        var search_value = $('input[type="search"]').val();
        if(search_value!="" && event.keyCode==13)
        {
           $('.append-genre-cross').html('');
           $('.selected_genre').text(''); 
           $('.append-city-cross').html('');
           $('.selected_city').text('');
           $('input[type="checkbox"]').removeAttr("checked");
           $.ajax({
                      url     :  '<?php echo base_url(); ?>Artist/search_result',
                      type    :  'POST',
                      data    :  {search_value:search_value},
                      success :  function(search){

                         $('.listing_wrap').html('');
                         $('.showAjaxVenues').html(search); 
                      }
                 });
        }
    });

     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('.back-to-top').fadeIn();
            } else {
                $('.back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('.back-to-top').click(function () {
            $('.back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('.back-to-top').tooltip('show');

});
    </script>

    <!-- for lightbox  -->
    <script type="text/javascript">
        $(document).ready(function(){
            $('#lightgallery').lightGallery();
        });
        </script>
    <script src="<?php echo site_url(); ?>assets/js/lightgallery-all.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/lightbox-plus-jquery.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/jquery.mousewheel.min.js"></script>


    <!-- for header shrink -->
    <script type="text/javascript">
        $(document).on("scroll", function(){
        if
          ($(document).scrollTop() > 20){
              $(".main-header").addClass("shrink");
            }
            else
            {
                $(".main-header").removeClass("shrink");
            }
        });
    </script>
 <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.12';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <script type="text/javascript">
    function genericSocialShare(url){
        window.open(url,'sharer','toolbar=0,status=0,width=648,height=395');
        return true;
    }
    </script>
</body>
</html>


<?php  
function fbLikeCount($id,$appid,$appsecret){
  $json_url ='https://graph.facebook.com/'.$id.'?access_token='.$appid.'|'.$appsecret.'&fields=fan_count';
  $json = file_get_contents($json_url);
  $json_output = json_decode($json);
  //Extract the likes count from the JSON object
  if($json_output->fan_count){
    return $fan_count = $json_output->fan_count; 
  }else{
    return 0;
  }
}
// $fb_likes = fbLikeCount('194363491178909','194363491178909','34029a9990b8f254ae82d0cb2b20ae39');
// echo @number_format($fb_likes);
function fbPageLikeCounter($pageUid, $appid,$appsecret)
{
    $result = json_decode(file_get_contents('https://graph.facebook.com/'.$pageUid.'?access_token='.$appid.'|'.$appsecret));
    if($result->likes) {
      return $result->likes;
    } else {
      return 0;
    }
}
?>
