<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('includes/head');?>
        <link href="<?php echo base_url();?>/assets/dist/plugins/iCheck/all.css" rel="stylesheet">
        <script src="<?php echo base_url();?>/assets/dist/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/plugins/select2/select2.min.js"></script>
        <!--<script src="<?php echo base_url();?>/assets/dist/js/bootstrap_multiselect.js"></script>-->
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.time.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.date.js"></script>
        <!--<script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/legacy.js"></script>-->
        <style>
        .mar{
        margin: 30px auto;
        font-size: 16px;
        font-family: verdana;
        }
        </style>
    </head>
    <body>
        <?php $this->load->view('includes/header');?>
        <div  class="container">
            <div class="col-lg-8 col-lg-offset-2 col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">
                <h3 class="text-center">Registration Completed</h3>
                <div class="clearfix"></div>
                <form style="" id="form-2" method="post">
                    <ul class="progresssteps m-t-md m-b-md">
                        <li class=""> </li>
                        <li class=""> </li>
                        <li class=""> </li>
                        <li > </li>
                        <li class="active"> </li>
                    </ul>
                </form>
                <br>
                
                <h4 class="text-center">Thank you.! You have successfully registered your account. </h4>
                <br><br>
                <?php echo anchor("artistRegistration/socialmedia","Back",array("class"=>"btn btn-primary")) ?>
                <?php echo anchor("artist/dashboard","Continue",array("class"=>"btn btn-primary pull-right")) ?>
                </div><!-- row -->
                </div><!-- container -->
                <?php $this->load->view('includes/footer');?>
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
            </body>
        </html>