<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class VenueRegistration extends MY_Controller {

    private $loggedintime;
    private $user_data;
    private $data;

    function __construct() {

        parent::__construct();

        $this->user_data=$this->Venue_Session();
        $this->data['title'] = 'admin';
        $this->data['page_title'] = "admin";
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model(['Artist_model','Venue_model','Account_model']);
        $this->session->set_userdata('showspoon_artist.is_complete',1);
        $this->lang->load('account','english');
        $this->lang->load('artist','english');
        $this->data['session_user']=$this->user_data;


        /*$this->user_data = array_merge($this->user_data,['is_complete'=>0]);
        $this->session->set_userdata('showspoon_artist',$this->user_data);
        dd($this->user_data);*/
        //dd('sf');


    }

    public function index() {


        $this->profile_steps(1);
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $artist = $this->Venue_model->get_venue_details($user_id);
        //dd($artist);
        if(!isset($artist->user_id)){
            show_404();
        }
        $this->data['success'] = $this->session->flashdata('success');
        $this->data['error'] = $this->session->flashdata('error');
        $this->form_validation->set_rules('venue_name', 'Artist Name', 'required');
        $this->form_validation->set_rules('city_id', 'City', 'required');
        $this->form_validation->set_rules('capacity', 'Band Type', 'required');
        $this->form_validation->set_rules('payment_account', 'Payment Account', 'required');
        if ($this->form_validation->run() !== FALSE)
        {

            $address=$this->input->post('address')?$this->input->post('address'):'';
            $genre = $this->input->post('genre')?$this->input->post('genre'):'';
            $genre = implode(',',$genre);
            $artist_fields=[
                'user_id'=>$user_id,
                'genre'=>($genre!='')?$genre:'',
                'location'=>($address!='')?$address:'',
                'name'=>$this->input->post('venue_name')?$this->input->post('venue_name'):'',
                'short_description'=>$this->input->post('short_description')?$this->input->post('short_description'):'',
                'city_id'=>$this->input->post('city_id')?$this->input->post('city_id'):'',
                'age'=>$this->input->post('age')?$this->input->post('age'):'',
            ];

            $image_x = $this->input->post('image_x');
            $image_y =  $this->input->post('image_y');
            $image_width =  $this->input->post('image_width');
            $image_height =  $this->input->post('image_height');
            $scalex =  $this->input->post('scalex');
            $scaley =$this->input->post('scaley');
            $hidden_image =$this->input->post('hidden_image');
            if (!empty($hidden_image)) {
                $image_src =  './uploads/temp/'.$hidden_image;

            $image_user = './uploads/users/'.$hidden_image;
            $image_dest = './uploads/users/thumb/'.$hidden_image;
            

            copy($image_src, $image_dest);
            copy($image_src, $image_user);
          
           //dd($_POST);
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                   
                    $config['source_image']    = './uploads/users/thumb/'.$hidden_image;
                    $config['maintain_ratio']  = false;
                    $config['x_axis']          = round($image_x);
                    $config['y_axis']          = round($image_y);
                    $config['width']           = round($image_width);
                    $config['height']          = round($image_height);

                    $this->image_lib->initialize($config);

                    if ( ! $this->image_lib->crop())
                    {
                            echo $this->image_lib->display_errors();
                            die();
                    }
                    if(isset($artist->profile_image) && $artist->profile_image!=''){
                         @unlink($image_src =  './uploads/temp/'.$hidden_image);
                    }

                    $artist_fields['profile_image']=$hidden_image;
                   
            }
            $this->Venue_model->update_venue($artist_fields);

            $artist_details_fields=[
                'venue_id'=>isset($artist->row_id)?$artist->row_id:0,
                'address'=>$address,
                
                'contact_person'=>$this->input->post('contact_person')?$this->input->post('contact_person'):'',
                'capacity'=>$this->input->post('capacity')?$this->input->post('capacity'):'',
                'phone'=>$this->input->post('phone')?$this->input->post('phone'):'',
                'zip'=>$this->input->post('zip')?$this->input->post('zip'):'',
                'payment_account'=>$this->input->post('payment_account')?$this->input->post('payment_account'):'',
                'mobile'=>$this->input->post('mobile')?$this->input->post('mobile'):'',
                'description'=>$this->input->post('description')?$this->input->post('description'):'',
            ];
            $this->Venue_model->update_venue_details($artist_details_fields);
            //dd($this->input->post());
            $this->session->set_flashdata('success',$this->lang->line('profile_update',false));
            
            //dd($this->input->post());
            if($this->Venue_Profile()<$this->total_steps){

                $this->Venue_Session_Update(1);
                   
            }
             redirect('VenueRegistration/media');
        }

        $this->data['genres'] = $this->Artist_model->get_genres();
        $this->data['cities'] = $this->Account_model->get_cities();
        $this->data['band_types'] = $this->Artist_model->get_band_types();



        $this->data['artist']=$artist;
        $this->load->view('venue/profile1', $this->data);
    }
    
    public function search()
    {
        

        $this->data['genres'] = $this->Artist_model->get_genres();
        $this->data['cities'] = $this->Account_model->get_cities();
        $this->data['band_types'] = $this->Artist_model->get_band_types();

        $this->load->library("pagination");
        $genre = $this->input->get('genre')?$this->input->get('genre'):[];
        $band = $this->input->get('band')?$this->input->get('band'):'';
        $city = $this->input->get('city')?$this->input->get('city'):'';
        $sort = $this->input->get('sort')?$this->input->get('sort'):'name';
        $filter=array(
            'genre'=>$genre,
            'city_id'=>$city,
            'band_type_id'=>$band,
        );
        if($sort=='name'){
            $sort='name';
        }elseif($sort=='rating'){
            $sort='rating';
        }
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
        $count_record=$this->Artist_model->get_artists_count($filter);
        $config["base_url"] = site_url() . "/artist/search/";
        $config["total_rows"] = $count_record; 
        $config["per_page"] = 9;
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);

        $custom_pagination = $this->Artist_model->ci_custom_pagination($config["base_url"], $config["total_rows"], $config["per_page"], $config["uri_segment"]);

        $offset = $page*$config["per_page"];
        $this->pagination->initialize($custom_pagination);
        $this->data['artists']=$this->Artist_model->get_artists($filter,$offset,$config["per_page"],$sort,'ASC');
        //dd($this->data['artists']);
        $this->data["links"] = $this->pagination->create_links();
        $this->load->view('venue/search', $this->data);
    }


    public function Finished(){
        $this->load->view("venue/finished");
    }

    


  

    public function socialmedia() {

        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $artist = $this->Artist_model->get_artist_details($user_id);
        //dd($artist);
        if(!isset($artist->user_id)){
            show_404();
        }
        $this->data['success'] = $this->session->flashdata('success');
        $this->data['error'] = $this->session->flashdata('error');






        //dd($this->data['videos']);
        $this->load->view('venue/socialmedia', $this->data);
    }
    public function media() {

        $this->profile_steps(2);
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $artist = $this->Venue_model->get_venue_details($user_id);
        //dd($artist);
        if(!isset($artist->user_id)){
            show_404();
        }
        $this->data['success'] = $this->session->flashdata('success');
        $this->data['error'] = $this->session->flashdata('error');
        /*$this->form_validation->set_rules('name', 'Artist Name', 'required');
        $this->form_validation->set_rules('city_id', 'City', 'required');
        $this->form_validation->set_rules('band_type_id', 'Band Type', 'required');
        $this->form_validation->set_rules('payment_account', 'Payment Account', 'required');*/




        $this->data['gallery']=$this->Venue_model->get_venue_gallery($user_id);
        $this->data['videos']=$this->Venue_model->get_venue_videos($user_id,['YouTube','Vimeo']);
        $this->data['sound_cloud']=$this->Venue_model->get_venue_videos($user_id,['soundcloud','spotify']);

        //dd($this->data['videos']);
        $this->load->view('venue/media1', $this->data);
    }
    public function upload_gallery() {


        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        //dd($_FILES);
        $json = array();
        $directory = FCPATH . 'uploads/gallery';
        // Check user has permission

        // Check its a directory
        if (!is_dir($directory)) {
            $json['error'] = 'Folder does not exists.';
        }

        if (!$json) {
            /*print_r($json);
            print_r($_FILES['file']);
            die();*/
            if(count($_FILES['file']['name'])>0){
                if (!empty($_FILES['file']['name'][0]) && is_file($_FILES['file']['tmp_name'][0])) {
                    $total_files =count($_FILES['file']['name']);
                    for($v=0; $v<$total_files; $v++){
                        // Sanitize the filename
                        $filename = basename(html_entity_decode($_FILES['file']['name'][$v], ENT_QUOTES, 'UTF-8'));

                        $file_arr = explode('.',$filename);

                        $file_ext = end($file_arr);
                        $orignal_name = seo_Url($file_arr[0]);
                        $filename = time().'_'.mt_rand(1000,100000).$v.'.'.$file_ext;

                        // Validate the filename length
                        if ((mb_strlen($filename) < 3) || (mb_strlen($filename) > 255)) {
                            $json['error'] = 'File Name too large max length 254, min length 3';
                        }

                        // Allowed file extension types
                        $allowed = array(
                            'jpg',
                            'jpeg',
                            'gif',
                            'png'
                        );

                        if (!in_array(mb_strtolower(mb_substr(strrchr($filename, '.'), 1)), $allowed)) {
                            $json['error'] = 'Invalid file extension';
                        }

                        // Allowed file mime types
                        $allowed = array(
                            'image/jpeg',
                            'image/pjpeg',
                            'image/png',
                            'image/x-png',
                            'image/gif'
                        );

                        if (!in_array($_FILES['file']['type'][$v], $allowed)) {
                            $json['error'] = 'Invalid File extension.';
                        }

                        // Check to see if any PHP files are trying to be uploaded
                        $content = file_get_contents($_FILES['file']['tmp_name'][$v]);

                        if (preg_match('/\<\?php/i', $content)) {
                            $json['error'] = 'File type error.';
                        }

                        // Return any upload error
                        if ($_FILES['file']['error'][$v] != UPLOAD_ERR_OK) {
                            $json['error'] = 'Something went wrong.';
                        }
                    }
                } else {
                    $json['error'] = 'error in file upload';
                }
            }
        }
        $view='';
        if (!$json) {

            if(count($_FILES['file']['name'])>0){
                $this->load->library('image_lib');
                $total_files =count($_FILES['file']['name']);
                for($v=0; $v<$total_files; $v++){
                    $filename = basename(html_entity_decode($_FILES['file']['name'][$v], ENT_QUOTES, 'UTF-8'));
                    $file_arr = explode('.',$filename);
                    $file_ext = end($file_arr);
                    $orignal_name = seo_Url($file_arr[0]);
                    $filename = $orignal_name.'-'.mt_rand(1000,100000).$v.'.'.$file_ext;
                    if(move_uploaded_file($_FILES['file']['tmp_name'][$v], $directory . '/' . $filename))
                    {

                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $directory . '/' . $filename;
                        $config['new_image'] = $directory . '/thumbs/' . $filename;
                        $config['create_thumb'] = FALSE;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = 300;
                        $config['height'] = 200;
                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();
                        //$this->upload->do_upload();
                        $this->image_lib->clear();
                        $insert=array(
                            'url'=>$filename,
                            'venue_id'=>$user_id,

                        );

                        $ins_id = $this->Venue_model->add_picture($insert);
                        $img_path=site_url('uploads/gallery/thumbs/'.$filename);
                        $view .= $this->load->view('includes/artist-gallery-part',['id'=>$ins_id,'img_path'=>$img_path],true);
                    }
                }
                $json['view']=$view;

                $json['success'] = "Image uploaded successfully.";
            }
        }

        echo json_encode($json);
    }
    public function delete_gallery()
    {
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $row_id = $this->input->post('row_id')?$this->input->post('row_id'):0;
        if($row_id>0 && $user_id>0){
            $row = $this->Venue_model->get_gallery_by_id($row_id);
            
            if(isset($row->venue_id) && $row->venue_id==$user_id){
                if($row->url!=''){
                    $img_path='./uploads/gallery/thumbs/'.$row->url;    
                    @unlink($img_path);
                    $img_path='./uploads/gallery/'.$row->url;    
                    @unlink($img_path);

                }
                $this->db->where('id',$row_id)->delete('venue_gallery');

            }
            echo json_encode(array('data'=>$row_id));
            exit();
        }
        echo json_encode(array('data'=>false));
    }
    public function add_video()
    {
        $this->load->library('upload');
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $url=$this->input->post('url')?$this->input->post('url'):'';
        $caption=$this->input->post('caption')?$this->input->post('caption'):'';
        $type=$this->input->post('type')?$this->input->post('type'):'';

        if ($type == 'YouTube') {

            @$json = file_get_contents('https://www.youtube.com/oembed?url=' . $url . '&format=json');
        } elseif ($type == 'Vimeo') {
            @$json = file_get_contents('https://vimeo.com/api/oembed.xml?url=' . $url . '&format=json');
        } elseif ($type == 'soundcloud') {
            //@$json = file_get_contents('https://vimeo.com/api/oembed.xml?url=' . $url . '&format=json');
            @$getValues = file_get_contents('http://soundcloud.com/oembed?format=js&url=' . $url . '&iframe=true&maxheight=100');
            //Clean the Json to decode
            $json = substr($getValues, 1, -2);
            //json decode to convert it as an array

        }
        $video_data='';



        $insert=[
            'url'=>$url,
            'caption'=>$caption,
            'type'=>$type,
            'venue_id'=>$user_id,
            'vide_data'=>$video_data
        ];
        if(isset($json) && $json!=''){
            //$json = json_decode($json);

            $video_data = ($json);
            $insert['video_data']=$video_data;


        }
        //dd($video_data);

        $html='';
        $insert_id = $this->Venue_model->create_video($insert);
        if($insert_id>0){
            if($type=='soundcloud'){
                $html=$this->load->view('includes/artist-soundcloud-part',['id'=>$insert_id,'title'=>$caption,'url'=>$url,'video_data'=>$video_data],true);    
            }elseif($type=='spotify'){
                $html=$this->load->view('includes/artist-spotify-part',['id'=>$insert_id,'title'=>$caption,'url'=>$url,'video_data'=>$video_data],true);    
            }else{
                $html=$this->load->view('includes/artist-video-part',['id'=>$insert_id,'title'=>$caption,'url'=>$url,'type'=>$type,'video_data'=>$video_data],true); 
            }

        }
        echo json_encode(array('data'=>$html));
    }
    public function delete_video(){
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $row_id = $this->input->post('id')?$this->input->post('id'):0;

        if($row_id>0 && $user_id>0){
            $row = $this->Venue_model->get_video_by_id($row_id);

            if(isset($row->venue_id) && $row->venue_id==$user_id){

                $this->db->where('id',$row_id)->delete('venue_media');

            }
            echo json_encode(array('data'=>$row_id));
            exit();
        }
        echo json_encode(array('data'=>false));
    }
    
  

     private function profile_steps($step=1){

         //dd($this->Venue_Profile());
        if($this->Venue_Profile()==0 && $step!=1){
            redirect('venueProfile');
        }/*elseif($this->Artist_Profile()==1 && $step>2){
            redirect('artistProfile/members');
        }elseif($this->Artist_Profile()==2 && $step>3){
            redirect('artistProfile/media');
        }elseif($this->Artist_Profile()==3 && $step>4){
            redirect('artistProfile/socialmedia');
        }*/
       


    }

    public function upload_temp() {


        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        //dd($_FILES);
        $json = array();
        $directory = FCPATH . 'uploads/temp';
        // Check user has permission

        // Check its a directory
        if (!is_dir($directory)) {
            $json['error'] = 'Folder does not exists.';
        }

        if (!$json) {
            /*print_r($json);
            print_r($_FILES['file']);
            die();*/
            if(count($_FILES['file']['name'])>0){
                if (!empty($_FILES['file']['name'][0]) && is_file($_FILES['file']['tmp_name'][0])) {
                    $total_files =count($_FILES['file']['name']);
                    for($v=0; $v<$total_files; $v++){
                        // Sanitize the filename
                        $filename = basename(html_entity_decode($_FILES['file']['name'][$v], ENT_QUOTES, 'UTF-8'));

                        $file_arr = explode('.',$filename);

                        $file_ext = end($file_arr);
                        $orignal_name = seo_Url($file_arr[0]);
                        $filename = time().'_'.mt_rand(1000,100000).$v.'.'.$file_ext;

                        // Validate the filename length
                        if ((mb_strlen($filename) < 3) || (mb_strlen($filename) > 255)) {
                            $json['error'] = 'File Name too large max length 254, min length 3';
                        }

                        // Allowed file extension types
                        $allowed = array(
                            'jpg',
                            'jpeg',
                            'gif',
                            'png'
                        );

                        if (!in_array(mb_strtolower(mb_substr(strrchr($filename, '.'), 1)), $allowed)) {
                            $json['error'] = 'Invalid file extension';
                        }

                        // Allowed file mime types
                        $allowed = array(
                            'image/jpeg',
                            'image/pjpeg',
                            'image/png',
                            'image/x-png',
                            'image/gif'
                        );

                        if (!in_array($_FILES['file']['type'][$v], $allowed)) {
                            $json['error'] = 'Invalid File extension.';
                        }

                        // Check to see if any PHP files are trying to be uploaded
                        $content = file_get_contents($_FILES['file']['tmp_name'][$v]);

                        if (preg_match('/\<\?php/i', $content)) {
                            $json['error'] = 'File type error.';
                        }

                        // Return any upload error
                        if ($_FILES['file']['error'][$v] != UPLOAD_ERR_OK) {
                            $json['error'] = 'Something went wrong.';
                        }
                    }
                } else {
                    $json['error'] = 'error in file upload';
                }
            }
        }
        $view='';
        if (!$json) {

            if(count($_FILES['file']['name'])>0){
                $this->load->library('image_lib');
                $total_files =count($_FILES['file']['name']);
                for($v=0; $v<$total_files; $v++){
                    $filename = basename(html_entity_decode($_FILES['file']['name'][$v], ENT_QUOTES, 'UTF-8'));
                    $file_arr = explode('.',$filename);
                    $file_ext = end($file_arr);
                    $orignal_name = seo_Url($file_arr[0]);
                    $filename = $orignal_name.'-'.mt_rand(1000,100000).$v.'.'.$file_ext;
                    if(move_uploaded_file($_FILES['file']['tmp_name'][$v], $directory . '/' . $filename))
                    {

                    }
                }
                $json['file_name']=site_url('uploads/temp/'.$filename);
                $json['name']=($filename);

                $json['success'] = "Image uploaded successfully.";
            }
        }

        echo json_encode($json);
    }
}
