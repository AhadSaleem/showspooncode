   
    <?php $this->load->view('admin/includes/admin_header'); ?>
	<body>

	<!-- Main navbar -->
	<div class="navbar navbar-default header-highlight">
	<div class="navbar-header">
	<a class="navbar-brand" href="<?php echo site_url('admin_dashboard'); ?>"><img src="<?php echo site_url('assets/images/logo-n.png'); ?>" alt=""></a>

	<ul class="nav navbar-nav visible-xs-block">
	<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
	<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
	</ul>
	</div>

	<div class="navbar-collapse collapse" id="navbar-mobile">
	<ul class="nav navbar-nav navbar-right">
	<li class="dropdown dropdown-user">
	<a class="dropdown-toggle" data-toggle="dropdown">
	<img src="<?php echo site_url(); ?>assets/images/placeholder.jpg" alt="">
	<span><?php echo ($this->session->userdata('f_name'))?$this->session->userdata('f_name'):'Admin'; ?></span>
	<i class="caret"></i>
	</a>

	<ul class="dropdown-menu dropdown-menu-right">
	<li><a href="<?php echo site_url('admin_dashboard/admin_profile'); ?>"><i class="icon-user-plus"></i> My profile</a></li>
	<li><a href="<?php echo site_url('admin_login/logout'); ?>"><i class="icon-switch2"></i> Logout</a></li>
	</ul>
	</li>
	</ul>
	</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

	<!-- Page content -->
	<div class="page-content">

	<!-- Main sidebar -->
	<div class="sidebar sidebar-main">
	<div class="sidebar-content">

	<!-- Main navigation -->
	<div class="sidebar-category sidebar-category-visible">
	<div class="category-content no-padding">
	<ul class="navigation navigation-main navigation-accordion">

	<!-- Main -->
	<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
	<li><a href="<?php echo site_url('admin_dashboard'); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
	<li><a href="<?php echo site_url('admin_dashboard/artist'); ?>"><i class="icon-width"></i> <span>Artist</span></a></li>
	<li><a href="<?php echo site_url('admin_dashboard/venue'); ?>"><i class="icon-width"></i> <span>Venues</span></a></li>
	<li><a href="<?php echo site_url('admin_dashboard/transection'); ?>"><i class="icon-width"></i> <span>Transactions</span></a></li>
	<li><a href="<?php echo site_url('admin_dashboard/admin'); ?>"><i class="icon-user"></i> <span>Admin</span></a></li>
	<li class="active"><a href="<?php echo site_url('admin_dashboard/reviews'); ?>"><i class="icon-comments"></i> <span>Reviews</span></a></li>
	<li><a href="<?php echo site_url('admin_dashboard/settings'); ?>"><i class="icon-user"></i> <span>Settings</span></a></li>
	<li><a href="<?php echo site_url('admin_dashboard/admin_profile'); ?>"><i class="icon-user-plus"></i> <span>Profile</span></a></li>
	</ul>
	</div>
	</div>
	<!-- /main navigation -->

	</div>
	</div>
	<!-- /main sidebar -->  
	<!-- Main content -->
	<div class="content-wrapper">

	<!-- Content area -->
	<div class="content">

	<!-- Main charts -->
	<div class="row">
	<div class="col-lg-12">
	<!-- Basic datatable -->
	<div class="panel panel-flat">
	<div class="panel-heading">
	<h5 class="panel-title">Reviews</h5>
	<div class="heading-elements">
	<ul class="icons-list">
	<li><a data-action="collapse"></a></li>
	<li><a data-action="reload"></a></li>
	<li><a data-action="close"></a></li>
	</ul>
	</div>
	</div>
	<table class="table datatable-basic">
	<thead>
	<tr>
	<th>From</th>
	<th>To</th>
	<th>Comment</th>
	<th>Created Date</th>
	<th>Status</th>
	<th class="text-center">Actions</th>
	</tr>
	</thead>
	<tbody> 
    <?php if($reviews!=""){ foreach($reviews as $row){ ?>
	<tr>
	<td>
	<?php 
	if($row->type==1){echo isset($row->from_name)?$row->from_name.' <b>(Artist)</b>':'';}
    else{echo isset($row->to_name)?$row->to_name.' <b>(Venue)</b>':'';}
	?>
	</td>
	<td>
	<?php 
	if($row->type==2){echo isset($row->from_name)?$row->from_name.' <b>(Artist)</b>':'';}
	else{echo isset($row->to_name)?$row->to_name.' <b>(Venue)</b>':'';}
	?>
	</td>
	<td width="400px;"><?php echo isset($row->comment)?$row->comment:''; ?></td>
	<td><?php $date = date('j M Y',strtotime($row->created_datetime)); echo isset($date)?$date:''; ?></td>
	<td><?php if($row->is_active!="" && $row->is_active==1){echo '<span class="label label-success">Active</span>'; }else{echo '<span class="label label-danger">Inactive</span>';} ?></td>
	<td class="text-center">
	<ul class="icons-list">
	<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown">
	<i class="icon-menu9"></i>
	</a>

	<ul class="dropdown-menu dropdown-menu-right"> 
	<li>
    <?php if($row->is_active==1){ ?>
	<a href="<?php echo site_url(); ?>admin_dashboard/reviews_status/<?php echo $row->id; ?>/0">
	<i class="icon-pencil7"></i> 
	Inactive
    </a>
    <?php }else{
    ?>
	<a href="<?php echo site_url(); ?>admin_dashboard/reviews_status/<?php echo $row->id; ?>/1">
	<i class="icon-pencil7"></i> 
	Active
    </a>
    <?php
    } 
    ?>
	</li>
	</ul>
	</li>
	</ul>
	</td>
	</tr>
	<?php }}else{echo '';} ?>
	</tbody>
	</table>
	</div>
	<!-- /basic datatable -->
	</div>
	</div>
	<!-- Footer -->
	<div class="footer text-muted">
	&copy; <?php echo date('Y'); ?>. <a href="#">Showspoon</a> by <a href="http://www.mashkraft.com" target="_blank">Mashkraft</a>
	</div>
	<!-- /footer -->

	</div>
	<!-- /content area -->

	</div>
	<!-- /main content -->

	</div>
	<!-- /page content -->

	</div>
	<!-- /page container -->
    <?php $this->load->view('admin/includes/admin_footer'); ?>
	</body>
	</html>
