<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class venue extends MY_Controller {

    private $loggedintime;
    private $user_data;
    private $data;

    function __construct() {

        parent::__construct();

        $this->data['title'] = 'admin';
        $this->data['page_title'] = "admin";
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model(['Artist_model','Venue_model','Account_model','Notification_model','Booking_model']);
        $this->user_data=$this->Venue_Session();

        $logout = $this->uri->segment(2)?$this->uri->segment(2):'';
        if($logout!='logout'){
            if($this->Venue_Profile()<$this->total_steps){
                redirect('venueProfile');
            }
        }

    }



    public function index() {

        /*$query = $this->db->query("select * from abc");
        $query =$query->row();

        //log_message('info','hello...');
        die();
        $string='<div class="form-control">test message</div>';
        $string=html_escape($string);
        echo $string;
        die();*/
        // $this->data['title'] = 'Dashboard - Showspoon';


        //$this->search();
        //$this->load->view('venue/dashboard', $this->data);
    }
    public function dashboard(){ 
        //$this->index();
         //$data['venue_details'] = $this->Artist_model->getAtristDetails();
         //$data['venues'] = $this->Artist_model->getArtist(); 
         $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
         $this->data['image'] = isset($this->user_data['profile_image'])?$this->user_data['profile_image']:'';
         $this->data['genres'] = $this->Artist_model->getGenre(); 
         $this->data['cities'] = $this->Artist_model->getCities();
         $this->data['location'] = $this->Venue_model->get_venue_lat_long($user_id);
         $this->load->view('search/venue_dash',$this->data);
    }
    public function get_radius()
    {
        $output = "";
        $radius = $this->input->post('radius');
        $lat    = $this->input->post('latitude');
        $long   = $this->input->post('longitude');
        $data   = $this->Venue_model->getArtistRadius($radius,$lat,$long);
        if(count($data) > 0)
        {
            foreach($data as $value)
            {
            $output .= '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="">
                        <div class="panel panel-default"><div class="panel-body no-padder">
                        <div class="gimage"><a href="javascript:;" class="likebtn " data-id="14">
                        <i class="fa fa-heart-o"></i><i class="fa fa-heart"></i></a>
                        <a href="'.site_url().'Artist/details/'.$value->artist_id.'"><img src="'.base_url().'uploads/users/thumb/'.$value->profile_image.'" class="img-responsive center-block"></a>
                        </div>
                        </div>
                        <div class="panel-body wrapper-xs text-center"><h4 class="text-center">
                        <a href="'.site_url().'Artist/details/'.$value->artist_id.'">'.$value->name.'</a></h4>';
            $names = array();
            $explode = explode(',',$value->genre); 
            foreach($explode as $exp)
            {   
                $this->load->model('Artist_model');
                $exp_names = $this->Artist_model->getGenreNames($exp);
                foreach($exp_names as $exp_name)
                {
                    $names[] = $exp_name->name;
                }
            }  
            $output .=  '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';
            $output .=  '<div class="col-sm-12 text-center ratingstars"><i class="fa fa-star-o"></i> 
                        <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> 
                        <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> 
                        <span> ('.$value->rating.')</span></div><a href="'.site_url().'artistBooking/book/'.$value->artist_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                        </div>
                        </div>
                        </div>';
            }
        }
        else
        {
            $output .= "result not found";
        }
        echo $output;
    }
    public function get_genre()
    {   
        $output= "";
        if($this->input->post('genre'))
        {
        $data  = $this->Artist_model->getGenre(0,$this->input->post('genre'));
        foreach($data as $item)
        {
            $output .= '<div class="checkbox"><label><input type="checkbox" value="'.$item->name.'" data-id="'.$item->genre_id.'" data-genre="" class="genre">'.$item->name.'</label></div>';
        }
        }
        else
        {
        $start = $this->input->post('start');
        $limit = $this->input->post('limit');
        $count = $this->Artist_model->getGenreCount();
        $data  = $this->Artist_model->getGenre($start,$limit);
        $output .= '<input type="hidden" class="count" value="'.$count->count.'">';
        foreach($data as $item)
        {
            $output .= '<div class="checkbox"><label><input type="checkbox" value="'.$item->name.'" data-id="'.$item->genre_id.'" data-genre="" class="genre">'.$item->name.'</label></div>';
        }
        }
        echo $output;
        //echo json_encode($data);
    }
    public function get_city()
    {
        $output= "";
        if($this->input->post('city'))
        {
        $data  = $this->Artist_model->getCities(0,$this->input->post('city'));
        foreach($data as $item)
        {
            $output .= '<div class="checkbox"><label><input type="checkbox" value="'.$item->CityName.'" data-id="'.$item->CityID.'" data-genre="" class="city">'.$item->CityName.'</label></div>';
        }
        }
        else
        {
        $start1 = $this->input->post('start1');
        $limit1 = $this->input->post('limit1');
        $count = $this->Artist_model->getCityCount();
        $data  = $this->Artist_model->getCities($start1,$limit1);
        $output .= '<input type="hidden" class="count1" value="'.$count->city.'">';
        foreach($data as $item)
        {
            $output .= '<div class="checkbox"><label><input type="checkbox" value="'.$item->CityName.'" data-id="'.$item->CityID.'" data-genre="" class="city">'.$item->CityName.'</label></div>';
        }
        }
        echo $output;
        //echo json_encode($data);
    }
    public function getALLVenue()
    {
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $this->load->library('pagination');
        $output = "";
        $config['base_url'] ='#';
        $config['total_rows'] = $this->Artist_model->getArtistCount();
        $config['per_page'] = 12;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination" id="links">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li>';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li>';
        $config["last_tag_close"] = '</li>';
        $config['next_link'] = '&raquo;';
        $config["next_tag_open"] = '<li>';
        $config["next_tag_close"] = '</li>';
        $config["prev_link"] = "&laquo;";
        $config["prev_tag_open"] = "<li>";
        $config["prev_tag_close"] = "</li>";
        $config["cur_tag_open"] = "<li class='active'><a href='#'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li>";
        $config["num_tag_close"] = "</li>";
        //$config["num_links"] = 1;
        $this->pagination->initialize($config);
        $page_link = $this->pagination->create_links();
        $page = $this->uri->segment(3);
        $start = ($page - 1) * $config["per_page"];
        $output = array(
           'pagination_link'  => $page_link,
           'data'   => $this->Artist_model->getALLArtist($config['per_page'],$start,$user_id)
          );
          echo json_encode($output);
    }
    public function search_with_zipcode()
     {
        $output = "";
        $zipsearch = $this->input->post('zipsearch')?$this->input->post('zipsearch'):'';
        $data = $this->Venue_model->get_search_with_zipcode($zipsearch);
        if(count($data) > 0)
        {
            foreach($data as $value)
            {
                $result = $this->Venue_model->get_zipcode_result($value->artist_id);
                foreach($result as $value)
                {
            $output .= '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="">
                        <div class="panel panel-default"><div class="panel-body no-padder">
                        <div class="gimage"><a href="javascript:;" class="likebtn " data-id="14">
                        <i class="fa fa-heart-o"></i><i class="fa fa-heart"></i></a>
                        <a href="'.site_url().'Artist/details/'.$value->artist_id.'"><img src="'.base_url().'uploads/users/thumb/'.$value->profile_image.'" class="img-responsive center-block"></a>
                        </div>
                        </div>
                        <div class="panel-body wrapper-xs text-center"><h4 class="text-center">
                        <a href="'.site_url().'Artist/details/'.$value->artist_id.'">'.$value->name.'</a></h4>';
            $names = array();
            $explode = explode(',',$value->genre); 
            foreach($explode as $exp)
            {   
                $this->load->model('Artist_model');
                $exp_names = $this->Artist_model->getGenreNames($exp);
                foreach($exp_names as $exp_name)
                {
                    $names[] = $exp_name->name;
                }
            }  
            $output .=  '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';
            $output .=  '<div class="col-sm-12 text-center ratingstars"><i class="fa fa-star-o"></i> 
                        <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> 
                        <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> 
                        <span> ('.$value->rating.')</span></div><a href="'.site_url().'artistBooking/book/'.$value->artist_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                        </div>
                        </div>
                        </div>';
            }
        }
    }
        else
        {
            $output .= "result not found";
        }
        echo $output;
     }
     public function getAjaxArtistDetails()
     { 
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $genre_id  = $this->input->post('arr'); 
        $city_id   = $this->input->post('arr1');
        $lat       = $this->input->post('lat'); 
        $lat = @end($lat);
        $long      = $this->input->post('long'); 
        $long = @end($long);
        $latitude  = $this->input->post('latitude'); 
        $longitude = $this->input->post('longitude');
        $radius    = $this->input->post('radius'); 
        $radius = @end($radius);
        $zip    = $this->input->post('zip'); 
        $zip = @end($zip);
        $sorting   = $this->input->post('sorting');  
        $this->load->library('pagination');
        $config['base_url'] ='#';
        $config['total_rows'] = $this->Artist_model->getSearchCount($genre_id,$city_id,$radius,$lat,$long,$zip);
        $config['per_page'] = 12;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination" id="link">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li>';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li>';
        $config["last_tag_close"] = '</li>';
        $config['next_link'] = '&raquo;';
        $config["next_tag_open"] = '<li>';
        $config["next_tag_close"] = '</li>';
        $config["prev_link"] = "&laquo;";
        $config["prev_tag_open"] = "<li>";
        $config["prev_tag_close"] = "</li>";
        $config["cur_tag_open"] = "<li class='active'><a href='#'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li>";
        $config["num_tag_close"] = "</li>"; 
        //$config["num_links"] = 1;
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $start = ($page - 1) * $config["per_page"];
        $venue_details = $this->Artist_model->getAjaxArtistDetails($genre_id,$city_id,$lat,$long,$radius,$sorting,$latitude,$longitude,$zip,$config['per_page'],$start,$user_id); 
        //echo '<pre>'; print_r($venue_details); exit;
        $output = array( 
           'page_search_link'  => $this->pagination->create_links(),
           'output' => $venue_details
           //echo '<pre>'; print_r($output);
          );
        echo json_encode($output);
     }
     public function search_result()
     {
        $output = "";
        $search_value = $this->input->post('search_value')?$this->input->post('search_value'):'';
        $data = $this->Artist_model->get_search_artist_results($search_value);
        if(count($data) > 0)
        {
            foreach($data as $value)
            {
            $output .= '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="">
                        <div class="panel panel-default"><div class="panel-body no-padder">
                        <div class="gimage"><a href="javascript:;" class="likebtn " data-id="14">
                        <i class="fa fa-heart-o"></i><i class="fa fa-heart"></i></a>
                        <a href="'.site_url().'Venue/details/'.$value->artist_id.'"><img src="'.base_url().'uploads/users/thumb/'.$value->profile_image.'" class="img-responsive center-block"></a>
                        </div>
                        </div>
                        <div class="panel-body wrapper-xs text-center"><h4 class="text-center">
                        <a href="'.site_url().'Venue/details/'.$value->artist_id.'">'.$value->name.'</a></h4>';
            $names = array();
            $explode = explode(',',$value->genre); 
            foreach($explode as $exp)
            {   
                $this->load->model('Artist_model');
                $exp_names = $this->Artist_model->getGenreNames($exp);
                foreach($exp_names as $exp_name)
                {
                    $names[] = $exp_name->name;
                }
            }  
            $output .=  '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';
            $output .=  '<div class="col-sm-12 text-center ratingstars"><i class="fa fa-star-o"></i> 
                        <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> 
                        <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> 
                        <span> ('.$value->rating.')</span></div><a href="'.site_url().'artistBooking/book/'.$value->artist_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                        </div>
                        </div>
                        </div>';
            }
        }
        else
        {
            $output .= "result not found";
        }
        echo $output;
     }
     public function sorting()
     {
        $output = "";
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0; 
        $location = $this->Venue_model->get_venue_lat_long($user_id);
        $data_genre = $this->input->post("data_genre")?$this->input->post("data_genre"):[];
        $data_city  = $this->input->post("data_city")?$this->input->post("data_city"):[]; 
        $sorting = $this->input->post('sorting')?$this->input->post('sorting'):'';
        $result = $this->Venue_model->sorting($data_genre,$data_city,$sorting,$location->latitude,$location->longitude);  //print_r($result); die;

        $output .= '<div class="showAjaxVenues"></div>
                    <div class="row listing_wrap">'; 
        foreach($result as $venue) { 
        $output .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="" style="height:330px;">
                            <div class="panel panel-default">
                                <div class="panel-body no-padder">
                                    <div class="gimage">
                                        <a href="javascript:;" class="likebtn " data-id="14">
                                            <i class="fa fa-heart-o"></i>
                                            <i class="fa fa-heart"></i>
                                        </a>

                                        <a href="'.site_url().'Venue/details/'.$venue->artist_id.'">
                                            <img src="'.base_url().'uploads/users/thumb/'.$venue->profile_image.'" class="img-responsive center-block">
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body wrapper-xs text-center">
                                <h4 class="text-center"><a href="'.site_url().'Venue/details/'.$venue->artist_id.'">'.$venue->name.'</a></h4>';

                    $names = array();
                    $explode = explode(',',$venue->genre); 
                    foreach($explode as $exp)
                    {   
                        $exp_names = $this->Artist_model->getGenreNames($exp);
                        foreach($exp_names as $exp_name)
                        {
                            $names[] = $exp_name->name;
                        }
                    }  
        $output .=   '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';   
        $output .=   '<div class="col-sm-12 text-center ratingstars">
                                        <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span> ('.$venue->rating.')</span>
                                    </div>
                                    <a href="'.site_url().'artistBooking/book/'.$venue->artist_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                                </div>
                            </div>
                        </div>';
                         } 
        echo $output;
     }
     public function data()
     {
        $output = "";
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0; 
        $location = $this->Venue_model->get_venue_lat_long($user_id);
        $sorting = $this->input->post('sorting');
        $data_genre = $this->input->post("data_genre")?$this->input->post("data_genre"):[];
        $data_city  = $this->input->post("data_city")?$this->input->post("data_city"):[];  
        if($sorting=="Newest Venue")
        { 
           $result = $this->Venue_model->get_newest_venue($data_genre,$data_city);
           $output .= '<div class="showAjaxVenues"></div>
                    <div class="row listing_wrap">'; 
        foreach($result as $venue) { 
        $output .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="" style="height:330px;">
                            <div class="panel panel-default">
                                <div class="panel-body no-padder">
                                    <div class="gimage">
                                        <a href="javascript:;" class="likebtn " data-id="14">
                                            <i class="fa fa-heart-o"></i>
                                            <i class="fa fa-heart"></i>
                                        </a>

                                        <a href="'.site_url().'Artist/details/'.$venue->artist_id.'">
                                            <img src="'.base_url().'uploads/users/thumb/'.$venue->profile_image.'" class="img-responsive center-block">
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body wrapper-xs text-center">
                                <h4 class="text-center"><a href="'.site_url().'Artist/details/'.$venue->artist_id.'">'.$venue->name.'</a></h4>';

                    $names = array();
                    $explode = explode(',',$venue->genre); 
                    foreach($explode as $exp)
                    {   
                        $exp_names = $this->Artist_model->getGenreNames($exp);
                        foreach($exp_names as $exp_name)
                        {
                            $names[] = $exp_name->name;
                        }
                    }  
        $output .=   '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';   
        $output .=   '<div class="col-sm-12 text-center ratingstars">
                                        <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span> ('.$venue->rating.')</span>
                                    </div>
                                    <a href="'.site_url().'artistBooking/book/'.$venue->artist_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                                </div>
                            </div>
                        </div>';
                         } 
        echo $output;
        }
        else if($sorting=="Rating")
        {
            $output1 = "";
            $result = $this->Artist_model->get_new_rating($data_genre,$data_city);
            $output1 .= '<div class="showAjaxVenues"></div>
                    <div class="row listing_wrap">'; 
        foreach($result as $venue) { 
        $output1 .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="">
                            <div class="panel panel-default">
                                <div class="panel-body no-padder">
                                    <div class="gimage">
                                        <a href="javascript:;" class="likebtn " data-id="14">
                                            <i class="fa fa-heart-o"></i>
                                            <i class="fa fa-heart"></i>
                                        </a>

                                        <a href="'.site_url().'Artist/details/'.$venue->artist_id.'">
                                            <img src="'.base_url().'uploads/users/thumb/'.$venue->profile_image.'" class="img-responsive center-block">
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body wrapper-xs text-center">
                                <h4 class="text-center"><a href="'.site_url().'Artist/details/'.$venue->artist_id.'">'.$venue->name.'</a></h4>';

                    $names = array();
                    $explode = explode(',',$venue->genre); 
                    foreach($explode as $exp)
                    {   
                        $exp_names = $this->Artist_model->getGenreNames($exp);
                        foreach($exp_names as $exp_name)
                        {
                            $names[] = $exp_name->name;
                        }
                    }  
        $output1 .=   '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';   
        $output1 .=   '<div class="col-sm-12 text-center ratingstars">
                                        <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span> ('.$venue->rating.')</span>
                                    </div>
                                    <a href="'.site_url().'artistBooking/book/'.$venue->artist_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                                </div>
                            </div>
                        </div>';
                         } 
        echo $output1;
        }
        else if($sorting=="Most Relevant")
        {   
            $output2 = "";
            $result = $this->Artist_model->get_relevant_venues($data_genre,$data_city); 
            $output2 .= '<div class="showAjaxVenues"></div>
                    <div class="row listing_wrap">'; 
        foreach($result as $venue) { 
        $output2 .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="" style="height:330px;">
                            <div class="panel panel-default">
                                <div class="panel-body no-padder">
                                    <div class="gimage">
                                        <a href="javascript:;" class="likebtn " data-id="14">
                                            <i class="fa fa-heart-o"></i>
                                            <i class="fa fa-heart"></i>
                                        </a>

                                        <a href="'.site_url().'Artist/details/'.$venue->artist_id.'">
                                            <img src="'.site_url().'uploads/users/thumb/'.$venue->profile_image.'" class="img-responsive center-block">
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body wrapper-xs text-center">
                                <h4 class="text-center"><a href="'.site_url().'Artist/details/'.$venue->artist_id.'">'.$venue->name.'</a></h4>';

                    $names = array();
                    $explode = explode(',',$venue->genre); 
                    foreach($explode as $exp)
                    {   
                        $exp_names = $this->Artist_model->getGenreNames($exp);
                        foreach($exp_names as $exp_name)
                        {
                            $names[] = $exp_name->name;
                        }
                    }  
        $output2 .=   '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';   
        $output2 .=   '<div class="col-sm-12 text-center ratingstars">
                                        <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span> ('.$venue->rating.')</span>
                                    </div>
                                    <a href="'.site_url().'artistBooking/book/'.$venue->artist_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                                </div>
                            </div>
                        </div>';
                         } 
        echo $output2;
        }
        else if($sorting=="Price")
        {
            $output3 = "";
            $result  = $this->Artist_model->get_venuesprice_result($data_genre,$data_city); 
            $output3 .= '<div class="showAjaxVenues"></div>
                    <div class="row listing_wrap">'; 
        foreach($result as $venue) { 
        $output3 .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="" style="height:330px;">
                            <div class="panel panel-default">
                                <div class="panel-body no-padder">
                                    <div class="gimage">
                                        <a href="javascript:;" class="likebtn " data-id="14">
                                            <i class="fa fa-heart-o"></i>
                                            <i class="fa fa-heart"></i>
                                        </a>

                                        <a href="'.site_url().'Artist/details/'.$venue->artist_id.'">
                                            <img src="'.site_url().'uploads/users/thumb/'.$venue->profile_image.'" class="img-responsive center-block">
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body wrapper-xs text-center">
                                <h4 class="text-center"><a href="'.site_url().'Artist/details/'.$venue->artist_id.'">'.$venue->name.'</a></h4>';

                    $names = array();
                    $explode = explode(',',$venue->genre); 
                    foreach($explode as $exp)
                    {   
                        $exp_names = $this->Artist_model->getGenreNames($exp);
                        foreach($exp_names as $exp_name)
                        {
                            $names[] = $exp_name->name;
                        }
                    }  
        $output3 .=   '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';   
        $output3 .=   '<div class="col-sm-12 text-center ratingstars">
                                        <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span> ('.$venue->rating.')</span>
                                    </div>
                                    <a href="'.site_url().'artistBooking/book/'.$venue->artist_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                                </div>
                            </div>
                        </div>';
                         } 
        echo $output3;
    }
    else if($sorting=="Distance")
        {
            $output4 = "";
            $result  = $this->Artist_model->get_venuedistance_result($data_genre,$data_city,$location->latitude,$location->longitude);
            $output4 .= '<div class="showAjaxVenues"></div>
                    <div class="row listing_wrap">'; 
        foreach($result as $venue) { 
        $output4 .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="" style="height:330px;">
                            <div class="panel panel-default">
                                <div class="panel-body no-padder">
                                    <div class="gimage">
                                        <a href="javascript:;" class="likebtn " data-id="14">
                                            <i class="fa fa-heart-o"></i>
                                            <i class="fa fa-heart"></i>
                                        </a>

                                        <a href="'.site_url().'Artist/details/'.$venue->venue_id.'">
                                            <img src="'.site_url().'uploads/users/thumb/'.$venue->profile_image.'" class="img-responsive center-block">
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body wrapper-xs text-center">
                                <h4 class="text-center"><a href="'.site_url().'Artist/details/'.$venue->venue_id.'">'.$venue->name.'</a></h4>';

                    $names = array();
                    $explode = explode(',',$venue->genre); 
                    foreach($explode as $exp)
                    {   
                        $exp_names = $this->Artist_model->getGenreNames($exp);
                        foreach($exp_names as $exp_name)
                        {
                            $names[] = $exp_name->name;
                        }
                    }  
        $output4 .=   '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';   
        $output4 .=   '<div class="col-sm-12 text-center ratingstars">
                                        <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span> ('.$venue->rating.')</span>
                                    </div>
                                    <a href="'.site_url().'artistBooking/book/'.$venue->venue_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                                </div>
                            </div>
                        </div>';
                         } 
        echo $output4;
    }
     }
     public function sorting_result()
     {
        $output = "";
        $sorting = $this->input->post('sorting');
        if($sorting=="Newest Venue")
        {
            $result = $this->Artist_model->get_newest_artist();
            $output .= '<div class="showAjaxVenues"></div>
                        <div class="row listing_wrap">'; 
        foreach($result as $venue) { 
        $output .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="" style="height:330px;">
                            <div class="panel panel-default">
                                <div class="panel-body no-padder">
                                    <div class="gimage">
                                        <a href="javascript:;" class="likebtn " data-id="14">
                                            <i class="fa fa-heart-o"></i>
                                            <i class="fa fa-heart"></i>
                                        </a>

                                        <a href="'.site_url().'Venue/details/'.$venue->artist_id.'">
                                            <img src="'.site_url().'uploads/users/thumb/'.$venue->profile_image.'" class="img-responsive center-block">
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body wrapper-xs text-center">
                                <h4 class="text-center"><a href="'.site_url().'Venue/details/'.$venue->artist_id.'">'.$venue->name.'</a></h4>';

                    $names = array();
                    $explode = explode(',',$venue->genre); 
                    foreach($explode as $exp)
                    {   
                        $exp_names = $this->Artist_model->getGenreNames($exp);
                        foreach($exp_names as $exp_name)
                        {
                            $names[] = $exp_name->name;
                        }
                    }  
        $output .=   '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';   
        $output .=   '<div class="col-sm-12 text-center ratingstars">
                                        <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span> ('.$venue->rating.')</span>
                                    </div>
                                    <a href="'.site_url().'artistBooking/book/'.$value->artist_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                                </div>
                            </div>
                        </div>';
                         } 
        echo $output;
        }
        else if($sorting=="Rating")
        {
            $output1 = "";
            $result = $this->Artist_model->get_ratingg();
            $output1 .= '<div class="showAjaxVenues"></div>
                    <div class="row listing_wrap">'; 
        foreach($result as $venue) { 
        $output1 .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="">
                            <div class="panel panel-default">
                                <div class="panel-body no-padder">
                                    <div class="gimage">
                                        <a href="javascript:;" class="likebtn " data-id="14">
                                            <i class="fa fa-heart-o"></i>
                                            <i class="fa fa-heart"></i>
                                        </a>

                                        <a href="'.site_url().'Venue/details/'.$venue->artist_id.'">
                                            <img src="'.base_url().'uploads/users/thumb/'.$venue->profile_image.'" class="img-responsive center-block">
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body wrapper-xs text-center">
                                <h4 class="text-center"><a href="'.site_url().'Venue/details/'.$venue->artist_id.'">'.$venue->name.'</a></h4>';

                    $names = array();
                    $explode = explode(',',$venue->genre); 
                    foreach($explode as $exp)
                    {   
                        $exp_names = $this->Artist_model->getGenreNames($exp);
                        foreach($exp_names as $exp_name)
                        {
                            $names[] = $exp_name->name;
                        }
                    }  
        $output1 .=   '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';   
        $output1 .=   '<div class="col-sm-12 text-center ratingstars">
                                        <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span> ('.$venue->rating.')</span>
                                    </div>
                                    <a href="'.site_url().'artistBooking/book/'.$value->artist_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                                </div>
                            </div>
                        </div>';
                         } 
        echo $output1;
        }
        else if($sorting=="Popularity")
        {
            $output2 = "";
            $result = $this->Artist_model->get_artist_popularity();
            $output2 .= '<div class="showAjaxVenues"></div>
                    <div class="row listing_wrap">'; 
        foreach($result as $venue) { 
        $output2 .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="" style="height:330px;">
                            <div class="panel panel-default">
                                <div class="panel-body no-padder">
                                    <div class="gimage">
                                        <a href="javascript:;" class="likebtn " data-id="14">
                                            <i class="fa fa-heart-o"></i>
                                            <i class="fa fa-heart"></i>
                                        </a>

                                        <a href="'.site_url().'Venue/details/'.$venue->artist_id.'">
                                            <img src="'.site_url().'uploads/users/thumb/'.$venue->profile_image.'" class="img-responsive center-block">
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body wrapper-xs text-center">
                                <h4 class="text-center"><a href="'.site_url().'Venue/details/'.$venue->artist_id.'">'.$venue->name.'</a></h4>';

                    $names = array();
                    $explode = explode(',',$venue->genre); 
                    foreach($explode as $exp)
                    {   
                        $exp_names = $this->Artist_model->getGenreNames($exp);
                        foreach($exp_names as $exp_name)
                        {
                            $names[] = $exp_name->name;
                        }
                    }  
        $output2 .=   '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';   
        $output2 .=   '<div class="col-sm-12 text-center ratingstars">
                                        <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span> ('.$venue->rating.')</span>
                                    </div>
                                    <a href="'.site_url().'artistBooking/book/'.$value->artist_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                                </div>
                            </div>
                        </div>';
                         } 
        echo $output2;
        }
        
     }
    public function search(){
         $data['image'] = $this->user_data['profile_image']?$this->user_data['profile_image']:'';
         $data['genres'] = $this->Artist_model->getGenre(); 
         $data['cities'] = $this->Artist_model->getCities();
         $search = $this->input->post('search');
         $this->load->library("pagination");
         $config = array();
         $config["base_url"] = site_url('artist/search');
         $config["total_rows"] = $this->Artist_model->search_count($search);
         $config["per_page"] = 2;
         $config["uri_segment"] = 3;
         $config["use_page_numbers"] = TRUE;
         $config["full_tag_open"] = '<ul class="pagination">';
         $config["full_tag_close"] = '</ul>';
         $config["first_tag_open"] = '<li>';
         $config["first_tag_close"] = '</li>';
         $config["last_tag_open"] = '<li>';
         $config["last_tag_close"] = '</li>';
         $config['next_link'] = '&raquo;';
         $config["next_tag_open"] = '<li>';
         $config["next_tag_close"] = '</li>';
         $config["prev_link"] = "&laquo";
         $config["prev_tag_open"] = "<li>";
         $config["prev_tag_close"] = "</li>";
         $config["cur_tag_open"] = "<li class='active'><a href='#'>";
         $config["cur_tag_close"] = "</a></li>";
         $config["num_tag_open"] = "<li>";
         $config["num_tag_close"] = "</li>";
         //$config["num_links"] = 1;
         $this->pagination->initialize($config);
         $page = ($this->uri->segment(3))?$this->uri->segment(3):1;
         $start = ($page - 1) * $config["per_page"];
         $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0; 
         $sql = $this->db->query("select favourites from users where user_id=$user_id");
         $data['favourites'] = $sql->row()->favourites;
         $this->data['genres'] = $this->Artist_model->getGenre(); 
         $this->data['cities'] = $this->Artist_model->getCities();
         $this->data['location'] = $this->Venue_model->get_venue_lat_long($user_id);
         $data['data'] = $this->Venue_model->get_search_results($search,$config["per_page"],$start);
         //$data['links'] = $this->pagination->create_links();
         $this->load->view('venue/venue_search',$data);
    }

    public function logout(){
        $this->session->unset_userdata('showspoon_venue');
        redirect('account/venue-login');
    }

    public function details($id=0){
        
        $this->load->model('Comments_model','comments');
        $artist = $this->Artist_model->get_artist_details_by_id($id); 
        $sender = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $this->data['image'] = $this->user_data['profile_image']?$this->user_data['profile_image']:0;
        $sql = $this->db->query("select * from venues inner join venue_details on venue_details.venue_id = venues.venue_id where venues.user_id = $sender");
        $this->data['currency'] = $sql->row()->venue_currency;
        //dd($this->data['genres']);

        if(!isset($artist->user_id)){
            show_404();
        }

        //$this->data['user_favourites']=$this->Get_Favourties();
        $sql = $this->db->query("select favourites from users where user_id=$sender");
        $row = $sql->row()->favourites;
        $this->data['venue']=$artist;
        $this->data['favourites'] = $row;
        $this->data['genres'] = $this->Artist_model->get_genres();
        $this->data['cities'] = $this->Account_model->get_cities();
        $this->data['band_types'] = $this->Artist_model->get_band_types();
        $user_id = isset($artist->user_id)?$artist->user_id:0;
        $this->data['id'] = $artist->user_id;
        $this->data['sender'] = $sender;
        $this->data['url_id'] = $id;
        $this->data['artist_gallery_count']=$this->Artist_model->get_artist_gallery_count($user_id);
        $this->data['video_count']=$this->Artist_model->get_artist_video_count($user_id);
        $this->data['gallery']=$this->Artist_model->get_artist_gallery($user_id);
        $this->data['videos']=$this->Artist_model->get_artist_videos($user_id,['YouTube']);
        $this->data['audios']=$this->Artist_model->get_artist_videos($user_id,['Vimeo']);
        $this->data['sound_cloud']=$this->Artist_model->get_artist_videos($user_id,['soundcloud']);
        $this->data['members']=$this->Artist_model->get_artist_members($user_id);
        $this->data['review_count'] = $this->comments->get_review_count($user_id);
        $this->data['featured'] = $this->Artist_model->get_featured_artist();
        $this->data['recently_booked'] = $this->Artist_model->get_recently_booked($user_id); 
        $this->data['average'] = $this->comments->get_avg_review($artist->user_id,$sender);
        $this->data['comments']=$this->comments->get_comments_artist($user_id);

        $this->load->view('venue/artist', $this->data);
    }
    public function get_gallery()
    {
          $user_id = $this->input->post('id'); 
          // $gallery =$this->Venue_model->get_venue_gallery($user_id,$config["per_page"],$start);
          $this->load->library("pagination");
          $config = array();
          $config["base_url"] = "#";
          $config["total_rows"] = $this->Artist_model->count_all($user_id);
          $config["per_page"] = 10;
          $config["uri_segment"] = 3;
          $config["use_page_numbers"] = TRUE;
          $config["full_tag_open"] = '<ul class="pagination">';
          $config["full_tag_close"] = '</ul>';
          $config["first_tag_open"] = '<li>';
          $config["first_tag_close"] = '</li>';
          $config["last_tag_open"] = '<li>';
          $config["last_tag_close"] = '</li>';
          $config['next_link'] = '&gt;';
          $config["next_tag_open"] = '<li>';
          $config["next_tag_close"] = '</li>';
          $config["prev_link"] = "&lt;";
          $config["prev_tag_open"] = "<li>";
          $config["prev_tag_close"] = "</li>";
          $config["cur_tag_open"] = "<li class='active'><a href='#'>";
          $config["cur_tag_close"] = "</a></li>";
          $config["num_tag_open"] = "<li>";
          $config["num_tag_close"] = "</li>";
          $config["num_links"] = 1;
          $this->pagination->initialize($config);
          $page = $this->uri->segment(3);
          $start = ($page - 1) * $config["per_page"];
          if($config["total_rows"]){

            $links = $this->pagination->create_links();
          }
          else
          {
            $links = '';
          }
          $output = array(
           'pagination_link'  => $links,
           'gallery'   => $this->Artist_model->get_artist_galleryy($user_id,$config["per_page"],$start)
          );
          echo json_encode($output);
    }
    public function get_videos()
    {
          $user_id = $this->input->post('id'); 
          // $gallery =$this->Venue_model->get_venue_gallery($user_id,$config["per_page"],$start);
          $this->load->library("pagination");
          $config = array();
          $config["base_url"] = "#";
          $config["total_rows"] = $this->Artist_model->count_videos($user_id);
          $config["per_page"] = 10;
          $config["uri_segment"] = 3;
          $config["use_page_numbers"] = TRUE;
          $config["full_tag_open"] = '<ul class="pagination">';
          $config["full_tag_close"] = '</ul>';
          $config["first_tag_open"] = '<li>';
          $config["first_tag_close"] = '</li>';
          $config["last_tag_open"] = '<li>';
          $config["last_tag_close"] = '</li>';
          $config['next_link'] = '&gt;';
          $config["next_tag_open"] = '<li>';
          $config["next_tag_close"] = '</li>';
          $config["prev_link"] = "&lt;";
          $config["prev_tag_open"] = "<li>";
          $config["prev_tag_close"] = "</li>";
          $config["cur_tag_open"] = "<li class='active'><a href='#'>";
          $config["cur_tag_close"] = "</a></li>";
          $config["num_tag_open"] = "<li>";
          $config["num_tag_close"] = "</li>";
          $config["num_links"] = 1;
          $this->pagination->initialize($config);
          $video = $this->uri->segment(3);
          $start = ($video - 1) * $config["per_page"];
          if($config["total_rows"]){

            $video_links = $this->pagination->create_links();
          }
          else
          {
            $video_links = '';
          }
          $output = array(
           'video_link'  => $video_links,
           'video'   => $this->Artist_model->get_artist_video($user_id,$config["per_page"],$start,['YouTube'])
          );
          echo json_encode($output);
    }
    public function add_favourites(){

        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $action=$this->input->post('action')?$this->input->post('action'):'';
        $id=$this->input->post('row_id')?$this->input->post('row_id'):0;
        if($id>0){
            $arr = $this->Get_Favourties();
            if($action=='add'){
                $arr[]=$id;    
            }else{
                if(in_array($id,$arr)){
                    $key = array_search($id,$arr);
                    unset($arr[$key]);
                }
            }

            array_unique($arr);
            $arr = array_values($arr);
            $arr = implode(',',$arr);
            $this->db->where('user_id',$user_id)->update('users',['favourites'=>$arr]);
            echo json_encode(array('data'=>'success'));
            exit();
        }
        echo json_encode(array('data'=>'fail'));
    }
    public function favourties(){
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $this->data['image'] = $this->user_data['profile_image']?$this->user_data['profile_image']:0;
        $this->data['genres'] = $this->Artist_model->getGenre(); 
        $this->data['cities'] = $this->Artist_model->getCities();
        $this->data['location'] = $this->Venue_model->get_venue_lat_long($user_id);
        $this->load->view('venue/venue_favourties',$this->data);
    }
    public function getHeartCount(){ 
        $where = ""; 
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $sql = $this->db->query("select * from users where user_id=$user_id");
        $row = $sql->row();
        if($row->favourites!=""){
        $exp = explode(',',$row->favourites);
        foreach($exp as $key=>$val){
            if($key==0){
                $where .= " where (artist.artist_id = $val) ";
            }else{
                $where .= " or (artist.artist_id = $val) "; 
            }
        }
        $query = $this->db->query("select COUNT(*) as heart from artist $where");
        echo $query->row()->heart;
    }else{echo 0;}
    }
    public function getALLFavourites(){
        $output = "";
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $data = $this->Venue_model->getFavourties($user_id); 
        $this->db->where("user_id",$user_id);
        $sql = $this->db->get('users');
        $arr = explode(',',$sql->row()->favourites);
        if(isset($data) && $data!=""){
        $output .= '<div class="ListView search-listing" style="padding-bottom:0;"><div class="showAjaxVenues"></div>
                    <div class="row listing_wrap">';
        foreach($data as $value)
        {
        $like_active='';        
        //$user_favourites = isset($user_favourites)?$user_favourites:[];
        $venue_id = isset($value->artist_id)?$value->artist_id:0;
        if(in_array($venue_id,$arr)){
            $like_active='active';   
        }
        $output .= '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="" style="height:330px;">
                    <div class="panel panel-default"><div class="panel-body no-padder">
                    <div class="gimage"><a href="javascript:;" class="likebtn '.$like_active.'" data-id="'.$value->artist_id.'">
                    <i class="fa fa-heart-o"></i><i class="fa fa-heart"></i></a>
                    <a href="'.site_url().'venue/details/'.$value->artist_id.'" rel="tab"><img src="'.site_url().'uploads/users/thumb/'.$value->profile_image.'" class="img-responsive center-block"></a>
                    </div>
                    </div>
                    <div class="panel-body wrapper-xs text-center"><h4 class="text-center">
                    <a href="'.site_url().'venue/details/'.$value->artist_id.'">'.$value->name.'</a></h4>';
        $names = array();
        $explode = explode(',',$value->genre); 
        foreach($explode as $exp)
        {   
            $this->load->model('Artist_model');
            $exp_names = $this->Artist_model->getGenreNames($exp);
            foreach($exp_names as $exp_name)
            {
                $names[] = $exp_name->name;
            } //'.site_url().'artistBooking/book/'.$value->venue_id.'
        }  
        $query = $this->db
                 ->select('Format(AVG(rating),1) AS avg')
                 ->from('rating')
                 ->where(array('receiver_id'=>$value->user_id,'type'=>2))
                 ->get();
        $row = $query->row();
        if(count($row) > 0){$rating = $row->avg;}else{$rating = 0.0;} 
        $output .=  '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';
        $output .=   '<div class="col-sm-12 text-center ratingstars">
                                        '.star_rating($rating).'';
        if($row->avg!="" && count($row->avg) > 0){
            $rate = explode('.',$row->avg);
            $output .= '<span> ('.$row->avg.')</span>';
        }
        $output .=   '</div>';
        $output .=   '<a href="'.site_url().'venueBooking/book/'.$value->artist_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                                </div>
                            </div>
                        </div>';
        }
        $output .= '</div></div>';
    }else{$output .= "No result found";}
        echo $output;
    }
    public function reviews()
    {
        $output = '';
        $comment = $this->input->post('comment');
        $rating  = $this->input->post('rating');
        $receive = $this->input->post('receive');
        $sender  = $this->input->post('sender');
        $type_id = $this->input->post('type');
        $comment_array = array(
                                  'sender_id'    =>   $sender,
                                  'receiver_id'  =>   $receive,
                                  'comment'      =>   $comment,
                                  'is_read'      =>   0,
                                  'is_active'    =>   0,
                                  'type'         =>   $type_id,
                                  'rating'       =>   $rating
                              );
        $this->load->model('Comments_model','comments');
        $check = $this->comments->check_venue_review($sender);
        if($check > 0){

            $output .= '<div class="alert alert-danger">You have already posted your review.</div>';
        }else{
        
        $this->comments->insert_venue_comments($comment_array);
        $output .= '<div class="alert alert-success">Your review is awaiting moderation.</div>';
      }
      echo $output;
    }
    public function please_rate(){
        $output = '';
        $receive = $this->input->post('venue_id');
        $sender  = $this->input->post('sender_id');
        $rating  = $this->input->post('rating');
        $this->db->select('*');
        $this->db->where(['sender_id'=>$sender,'receiver_id'=>$receive,'type'=>2]);
        $sql = $this->db->get('rating');
        if($sql->num_rows() > 0){

            $output .= '<div class="text-danger">You have already rated this artist.</div>';
        }else{
        $insert = array(
              'sender_id'=>$sender,
              'receiver_id'=>$receive,
              'type'=>2,
              'rating'=>$rating
        ); 
        $this->db->insert('rating',$insert);
        $output .= '<div class="text-success">Your have successfully rated this artist.</div>';
      }
      echo $output;
    }
    public function send_message(){
        $output = '';
        $artist_id = $this->input->post('artist_id');
        $user_id   = $this->input->post('user_id');
        $subject   = $this->input->post('subject');
        $message   = $this->input->post('message_body');
        $artist_sql = $this->db->query("select name,email from artist where user_id=$artist_id");
        $venue_sql = $this->db->query("select name,email from venues where user_id=$user_id");
        $msg_array = array(
             'from'    => $user_id,
             'to'      => $artist_id,
             'subject' => $subject,
             'message' => $message
        );
        $this->db->insert('chat',$msg_array);
        $insert_id = $this->db->insert_id();
        if($insert_id){
            $notification=[
                'type'=>2,
                'message'=>'New message has been received',
                'from_id'=>$user_id,
                'to_id'=>$artist_id,
                'status'=>'message',
                'chat_id'=>$insert_id
            ]; 
            $notification_id =$this->Notification_model->create_notification($notification);
            $email_data = array(
                         'artist'=>$artist_sql->row()->name,
                         'venue'=>$venue_sql->row()->name
                      );
            $venue_email_body = $this->load->view('email/send_message_from_venue', $email_data,true);   
            $this->Send_Mail($artist_sql->row()->email,FROM_EMAIL,FROM_NAME,'New Message',$venue_email_body);
            $output .= '<div class="alert alert-success">Your message has been send.</div>';
        }else{
            $output .= '<div class="alert alert-danger">Message failed to send.</div>';
        }
        echo $output;
    }
     public function get_notifications(){
         
        $this->load->model('Notification_model');
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
      
        

        if($user_id>0){
       
            $filter=[
                
                'to_id'=>$user_id,
                'is_read'=>0,

            ];

    

            $chat = $this->Notification_model->get_venue_notifications($filter);

            //dd($chat);
            $counter=0;
            $return=array();
            if(count($chat)>0){
                foreach($chat as $row){

                    if ($row->is_read==0) {
                        $this->db->set('is_read',1);
                        $this->db->where('id', $row->id);
                        $this->db->update('notifications');
                    }
                    $return[]=(array)$row;
                    $counter++;
                }
             
            }
            // $last_id = $this->session->userdata('last_id')?$this->session->userdata('last_id'):0;

            echo json_encode(array('data'=>$return));
            exit();
        }
        echo json_encode(array('data'=>false));
    }
    public function privacy_policy()
    {
        $this->data['image'] = $this->user_data['profile_image']?$this->user_data['profile_image']:'';
        $this->load->view('venue/venue_privacy_policy',$this->data);
    }
    public function terms()
    {
        $this->data['image'] = $this->user_data['profile_image']?$this->user_data['profile_image']:'';
        $this->load->view('venue/venue_terms',$this->data);
    }

    public function contact()
    {
        $this->data['image'] = $this->user_data['profile_image']?$this->user_data['profile_image']:'';
        if($this->input->post('submit')=="Send"){
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $message = $this->input->post('message');
            $user_data = array(
                        'name'=>$name,
                        'email'=>$email,
                        'message'=>$message

                    );
                $email_body = $this->load->view('email/contact_us',$user_data,true);
                $this->Send_Mail('support@showspoon.com',FROM_EMAIL,FROM_NAME,'Contact Email',$email_body);
                $this->session->set_flashdata('success','Your message has been sent successfully.');
                redirect("venue/contact");
        }else{
        $this->load->view('venue/venue_contact',$this->data);
        }
    }
    public function city_list_data(){
        $where  = '';
        $output = '';
        $country = $this->input->post('id');
        if($country !="" && count($country) > 0){
            $where = " where CountryID = ".$country;
        }
        $sql = $this->db->query("select * from cities $where order by CityName ASC"); 
        foreach($sql->result() as $row){
            $output .= '<div class="checkbox city-checkbox"><label><input type="checkbox" value="'.$row->CityName.'" data-id="'.$row->CityID.'" class="city">'.$row->CityName.'</label></div>';
        }
        echo json_encode(array('data'=>$output));
    }
}

