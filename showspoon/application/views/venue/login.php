<?php /*echo validation_errors();
echo isset($error)?$error:'';*/
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('includes/head');?>
        

     
    </head>
    <body style="background:url('<?php echo base_url();?>/assets/dist/images/home_bg.jpg') center top no-repeat; background-size:cover;">


       <?php $this->load->view('includes/header2');?>
        <div class="container">
            <div class="row loginbox vertical-align">

                <div class="loginContainer">

                    <!--<div class="row pt20 pb20" style="background:#e5e5e5;">
                        <div class="col-sm-12 col-xs-12">
                            <img src="<?php echo base_url();?>/assets/dist/images/logo.png" class="img-responsive center-block" />
                        </div>
                    </div>-->

                    <h3 class="text-center m-t-lg m-b-lg">Login to Your Account</h3>
                <form method="post">
                    <div class="well well-md m-b-none npt">
                       <?php if(isset($error) && $error!=''){?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <strong>Warning! </strong><?php echo $error;?>
                        </div>
                        <?php }?>
                        <?php if(isset($success) && $success!=''){?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <strong>Warning! </strong><?php echo $success;?>
                        </div>
                        <?php }?>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Your Email" />
                        </div><!-- form-group -->
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password" />
                        </div><!-- form-group -->
                        <div class="form-group">
                            <input type="submit" id="submit" value="Login" class="btn btn-primary btn-block">
                        </div><!-- form-group -->	
                        <p class="text-center text-muted text-sm">OR LOGIN USING</p>
                            <div class="form-group">
                                <a href="<?php echo site_url('account/facebook_signup/?user=venue&page=venue_login');?>" class="btn btn-facebook btn-block"><i class="fa fa-facebook m-r-sm"></i>Login via Facebook</a>
                                <a href="<?php echo site_url('account/google_signup/?user=venue&page=venue_login');?>" class="btn btn-gmail btn-block disabled"><i class="fa fa-google-plus m-r-sm"></i>Login via Gmail</a>
                            </div><!-- form-group -->
                    </div>
                    </form>




                </div><!-- col-lg-4 -->

            </div><!-- row -->
        </div><!-- container -->



        <script src="<?php echo base_url();?>/assets/dist/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/js/bootstrap.min.js"></script>

    </body>
</html>