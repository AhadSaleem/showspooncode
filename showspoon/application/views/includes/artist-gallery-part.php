<div  data-id="<?php echo $id;?>" class="col-lg-4 col-md-4 col-sm-4 col-xs-4 artist_gallery artist_gallery<?php echo $id; ?>">
    <div class="panel panel-body b wrapper-xs ">
        <div class="media">
            <div class="media-center text-center">
                <div class="img-block">
                    <div class="img-block-center">
                        <img src="<?php echo $img_path;?>" class="img-responsive" alt="">        
                    </div>
                </div>



            </div>
            <div class="gallery_overlay">
                <div class="gallery_overlay_inner">
                    <div class="gallery_delete">
                        <a href="javascript:;" class="delete_gallery btn btn-danger btn-sm r-2x" data-id="<?php echo $id; ?>">Delete</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>