<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('includes/head');?>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.2.0/ekko-lightbox.css" rel="stylesheet">
        <script src="<?php echo base_url();?>/assets/dist/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/plugins/select2/select2.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.2.0/ekko-lightbox.js"></script>
    </head>
    <body>

        <?php
        //dd($venue,false);
        $this->load->view('includes/header');?>




        <div class="container-fluid artist_details">
            <div class="container">
                <div class="row">

                    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">

                        <div class="media MiddleAvatar">
                            <div class="media-left">
                                <?php
                                $user_image=site_url('assets/dist/images/noimage2.jpg');
                                if(isset($venue->profile_image) && $venue->profile_image!=''){
                                    $user_image=site_url('uploads/users/thumb/'.$venue->profile_image);
                                }
                                ?>
                                <img src="<?php echo $user_image;?>" class="media-object middleimg" />
                            </div>
                        </div><!-- media -->
                        <h3 class="text-center"><?php echo isset($venue->name)?$venue->name:'';?></h3>

                        <div class="row m-t-sm">
                            <?php if(isset($venue->genre) && $venue->genre!=''){
                            $genres_idx = explode(',',$venue->genre);
                            ?>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ArtistTags m-t-sm">
                                <?php foreach($genres_idx as $gen){?>
                                <span class="badge m-b-sm"><?php echo isset($genres[$gen])?$genres[$gen]:'';?></span>
                                <?php }?>

                            </div><!-- col-lg-12 -->
                            <?php }?>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center m-t-md">
                                <p><!--Cover Band--> <span class="m-l-10 m-r-10"></span> <?php echo $venue->address;?></p>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center UserRating ratingstars">
                                <?php echo star_rating($venue->rating);?>
                                <span> (<?php echo round($venue->rating);?> Reviews)</span>
                            </div><!-- col-sm-12 -->

                        </div><!-- row -->
                    </div><!-- col-lg-12 -->

                    <?php if(isset($venue->description) && $venue->description!=''){ ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t-md">
                        <p class="text-muted text-center"><?php echo $venue->description;?> </p> <!--<a href="" class="Morelink">(+More)</a>-->
                    </div><!-- col-lg-12 -->
                    <?php }?>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 m-t-lg m-t-lg ArtistTabs">

                        <h3 class="text-center">Media</h3>

                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#photos" aria-controls="home" role="tab" data-toggle="tab">Photos</a></li>
                            <li><a href="#videos" aria-controls="videos" role="tab" data-toggle="tab">Videos</a></li>
                            <li><a href="#audio" aria-controls="audio" role="tab" data-toggle="tab">Audio Clips</a></li>
                        </ul>

                        <div class="tab-content m-t-lg">
                            <div role="tabpanel" class="tab-pane active" id="photos">
                                <div id="container22"></div>
                                <?php if(isset($gallery) && count($gallery)>0){?>
                                <?php foreach($gallery as $gal){
                                $full_image=$user_image=site_url('assets/dist/images/noimage2.jpg');
                                if(isset($gal->url) && $gal->url!='' && file_exists('./uploads/gallery/thumbs/'.$gal->url)){
                                    $user_image=site_url('uploads/gallery/thumbs/'.$gal->url);
                                    $full_image=site_url('uploads/gallery/'.$gal->url);
                                
                                ?>
                                <div class="item">
                                    <a href="<?php echo $full_image;?>" data-toggle="lightbox" data-gallery="example-gallery" class="">
                                        <div class="box-centered">
                                            <div class="box-center-inner">

                                                <img src="<?php echo $user_image;?>" class="img-responsive  center-block" />

                                            </div>
                                        </div>
                                    </a>

                                </div><!-- col-sm-4 -->

                                <?php }}?>
                                <?php }else{?>
                                <p class="text-center">No gallery found.</p>
                                <?php }?>


                            </div>
                            <div role="tabpanel" class="tab-pane" id="videos">
                                <?php if(isset($videos) && count($videos)>0){?>
                                <?php foreach($videos as $vid){
                                $video_data = isset($vid->video_data)?$vid->video_data:'';
                                if($video_data!=''){
                                    $video_data=json_decode($vid->video_data);
                                }
                                if(isset($video_data->html) && $video_data->html!='')
                                {
                                ?>
                                <div class="col-md-4 col-sm-6 col-xs-12 m-b-md">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <!--<iframe class="embed-responsive-item" src="//www.youtube.com/embed/Jr4TMIU9oQ4?rel=0"></iframe>-->
                                        <?php echo $video_data->html;?>
                                        <?php ?>
                                    </div>
                                </div>

                                <?php }}?>
                                <?php }else{?>
                                <p class="text-center">No video found.</p>
                                <?php }?>

                            </div>
                            <div role="tabpanel" class="tab-pane" id="audio">
                                <?php if(isset($sound_cloud) && count($sound_cloud)>0){?>
                                <?php foreach($sound_cloud as $vid){
    $video_data = isset($vid->video_data)?$vid->video_data:'';
    if($video_data!=''){
        $video_data=json_decode($vid->video_data);
    }
    if(isset($video_data->html) && $video_data->html!='')
    {
                                ?>
                                <div class="col-md-4 col-sm-6 col-xs-12 m-b-md">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <!--<iframe class="embed-responsive-item" src="//www.youtube.com/embed/Jr4TMIU9oQ4?rel=0"></iframe>-->
                                        <?php echo $video_data->html;?>
                                        <?php ?>
                                    </div>
                                </div>

                                <?php }}?>
                                <?php }else{?>
                                <p class="text-center">No audio found.</p>
                                <?php }?>

                            </div>
                        </div>

                    </div><!-- col-lg-8 -->

                    <div class="clearfix"></div>
                    <?php

                    ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 m-t-lg m-t-lg">

                        <h3 class="text-center">Reviews</h3>

                        <?php
                        if(isset($comments) && count($comments)>0){
                            foreach($comments as $comment){
                                $this->load->view('includes/rating-part',['comment'=>$comment]);
                            }


                        }else{
                            echo '<p class="text-center m-t-md">No review found.</p>'; 
                        }
                        ?>



                    </div><!-- col-lg-8 -->


                    <div class="clearfix"></div>
                    <br /><br /><br /><br /><br />



                </div><!-- row -->
            </div><!-- container -->
        </div><!-- container-fluid -->

        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
                        <a href="<?php echo site_url('artistBooking/book/'.$venue->venue_id);?>" class="btn btn-primary detail_book">Book Now</a>
                        <?php
                        $user_favourites = isset($user_favourites)?$user_favourites:[];
                        $venue_id = isset($venue->venue_id)?$venue->venue_id:0;
                        if(in_array($venue_id,$user_favourites)){
                        ?>
                        <a href="javascript:;" data-id="<?php echo $venue->venue_id;?>" class="likebtn add_favourites btn btn-white active">Remove Favourite</a>
                        <?php }else{?>
                        <a href="javascript:;" data-id="<?php echo $venue->venue_id;?>" class="likebtn add_favourites btn btn-white ">Add to Favourite</a>
                        <?php }?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 SocialLinks">
                        <a href="#"><i class="fa fa-facebook-square fa-3x"></i></a>
                        <a href="#"><i class="fa fa-twitter fa-3x"></i></a>
                        <a href="#"><i class="fa fa-youtube-play fa-3x"></i></a>
                    </div>
                </div>
            </div>
        </footer>

        <script src="//masonry.desandro.com/masonry.pkgd.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.0.4/jquery.imagesloaded.js"></script>

        <script>

            $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });
            $(function () {

                var $container = $('#container22').masonry({
                    itemSelector: '.item',
                    columnWidth: 228
                });

                // reveal initial images
                $container.masonryImagesReveal($('#photos').find('.item'));
            });

            $.fn.masonryImagesReveal = function ($items) {
                var msnry = this.data('masonry');
                var itemSelector = msnry.options.itemSelector;
                // hide by default
                $items.hide();
                // append to container
                this.append($items);
                $items.imagesLoaded().progress(function (imgLoad, image) {
                    // get item
                    // image is imagesLoaded class, not <img>, <img> is image.img
                    var $item = $(image.img).parents(itemSelector);
                    // un-hide item
                    $item.show();
                    // masonry does its thing
                    msnry.appended($item);
                });

                return this;
            };
            $(document).ready(function() {
                $('.likebtn').on('click',function(){
                    var obj_this=$(this);
                    if($(this).hasClass('active')){
                        var btn_action='remove';
                    }else{
                        var btn_action='add';
                    }
                    var row_id = $(this).data('id');
                    $.ajax({
                        type:'POST',
                        url: '<?php echo site_url('artist/add_favourites');?>',
                        data:{row_id:row_id,action:btn_action},
                        dataType:'json',


                        success:function(data){
                            if(data.data){
                                if(obj_this.hasClass('active')){
                                    obj_this.text('Add to Favourite');
                                    obj_this.removeClass('active');
                                }else{
                                    obj_this.addClass('active');
                                    obj_this.text('Remove Favourite');
                                }
                            }

                        },
                        error: function(data){

                        }
                    });
                });


                $(".select").select2({
                    width: '100%'
                });
                $("#sort").select2({
                    minimumResultsForSearch: -1,
                    width: '100%'
                });
                /*$('.select').multiselect({
                includeSelectAllOption: true,
                selectAllText: 'Check all!',
                numberDisplayed: 0
                });*/
            });
        </script>
        <style>
            .item {
                width: 228px;
                float: left;
                padding:5px;
            }
            .item img {
                display: block;
                width: 100%;
            }
            .search-btn{
                border-radius: 5px;
                margin-top: 29px;
                padding: 10px 60px;
            }
        </style>
    </body>
</html>