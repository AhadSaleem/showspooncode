<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Showspoon</title>
        <link href="<?php echo site_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css">
        <link href="<?php echo site_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="<?php echo site_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo site_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo site_url(); ?>assets/css/select2.min.css" rel="stylesheet">
        <link href="<?php echo site_url(); ?>assets/css/custom-develop.css" rel="stylesheet">
    </head>
    <body>
        <div class="container" style="width:420px;margin:auto;">
        <div class="panel panel-default" style="padding:10px;">
        
        <h3>Payment Received</h3>
        <br>
        <p><strong>Receiver name </strong><?php echo $output->artist_name; ?></p>
        <p><strong>Receiver email </strong><?php echo $output->artist_email; ?></p>
        <p><strong>Sender name </strong><?php echo $output->venue_name; ?></p>
        <p><strong>Sender email </strong><?php echo $output->venue_email; ?></p>
        <p><strong>Booking date </strong><?php echo date('j M Y',strtotime($output->datetime)); ?></p> 
        <p><strong>Mail send </strong><?php echo date('j M Y'); ?></p>

        Thank you, <br>
        Showspoon Team <br>
         <br>
        www.showspoon.com <br>
        <br>
        <a href="<?php echo site_url('Account/login');?>"><img src="<?php echo site_url('assets/images/logo-n.png');?>" width="200" alt="logo"></a>
    </div>
    </div>
    </body>
</html>