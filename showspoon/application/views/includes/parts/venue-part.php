<?php //dd($artist);?>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="<?php echo isset($artist->user_id)?$artist->user_id:0;?>">
    <div class="panel panel-default">
        <div class="panel-body no-padder">
            <div class="gimage">
                <?php
                $like_active='';
                
                $user_favourites = isset($user_favourites)?$user_favourites:[];
                $venue_id = isset($artist->venue_id)?$artist->venue_id:0;
                if(in_array($venue_id,$user_favourites)){
                    $like_active='active';   
                }
                ?>
                <a href="javascript:;" class="likebtn <?php echo $like_active;?>" data-id="<?php echo $artist->venue_id;?>">
                    <i class="fa fa-heart-o"></i>
                    <i class="fa fa-heart"></i>
                </a>
                
                <a href="<?php echo site_url('artist/details/'.$artist->venue_id);?>">
                <?php if(isset($artist->profile_image) && $artist->profile_image!=''){?>
                <img src="<?php echo site_url('uploads/users/thumb/'.$artist->profile_image);?>" class="img-responsive center-block" />
                <?php }else{?>
                <img src="<?php echo base_url();?>/assets/dist/images/noimage.jpg" class="img-responsive center-block" />
                <?php }?>
                </a>
            </div><!-- gimage -->
        </div><!-- panel-body -->
        <div class="panel-body wrapper-xs text-center">
           <?php
            $name=isset($artist->name)?$artist->name:'';
            if($name==''){
                $name=isset($artist->email)?$artist->email:'';
            }
            
            ?>
            <h4 class="text-center"><a href="<?php echo site_url('artist/details/'.$artist->venue_id);?>"><?php echo $name;?></a></h4>

            <h5 class="text-center m-b-none m-t-none">
                <?php 
                if(isset($artist->genre) && $artist->genre!=''){
                    $artist_genres = explode(',',$artist->genre);
                    $genre_names=[];
                    foreach($artist_genres as $genre)
                    {
                        if(isset($genres[$genre]) && $genres[$genre]!=''){
                            $genre_names[]=$genres[$genre];
                        }
                    }
                    echo implode(' / ',$genre_names);
                }
                ?>
            </h5>
            <!--<h5 class="text-center m-t-xs"><?php echo isset($band_types[$artist->band_type_id])?$band_types[$artist->band_type_id]:'';?></h5>-->
            <div class="col-sm-12 text-center ratingstars">
               <?php 
                $artisting_rating = (isset($artist->rating) && $artist->rating>0)?number_format($artist->rating,1):0;
                echo star_rating($artisting_rating);?>
                <span> (<?php echo $artisting_rating;?>)</span>
            </div><!-- col-sm-12 -->
            <a href="<?php echo site_url('artistBooking/book/'.$artist->venue_id);?>" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
        </div><!-- panel-body -->
    </div><!-- panel -->
</div><!-- col-lg-3 -->