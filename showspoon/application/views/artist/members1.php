<!DOCTYPE html>
<html lang="en">
    <head>

        <?php $this->load->view('includes/head');?>
        <link href="<?php echo base_url();?>/assets/dist/plugins/iCheck/all.css" rel="stylesheet">
        <script src="<?php echo base_url();?>/assets/dist/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/plugins/select2/select2.min.js"></script>
        <!--<script src="<?php echo base_url();?>/assets/dist/js/bootstrap_multiselect.js"></script>-->

        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.time.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.date.js"></script>

        <!--<script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/legacy.js"></script>-->
    </head>
    <body>
        <?php $this->load->view('includes/header');?>
        <div  class="container">
            <div class="row m-b-md">
                <div class="col-lg-8 col-lg-offset-2 col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">

                    <h3 class="text-center">Add Members</h3>
                    <div class="clearfix"></div>
                    <form style="" id="form-2" method="post">
                        <ul class="progresssteps m-t-md m-b-md">
                            <li class=""> </li>
                            <li class="active"> </li>
                            <li class=""> </li>
                            <li> </li>
                            <li> </li>
                        </ul> 
                    </form>
                        <div class="clearfix m-b-md"></div>
                        <div id="members_box">
                            <div class="row">
                                <?php if(isset($members) && count($members)>0){

    foreach($members as $row){
        if(isset($row->image_url) && $row->image_url!=''){
            $img_path=base_url().'uploads/members/thumbs/'.$row->image_url;
        }else{
            $img_path=base_url().'assets/images/placeholder.jpg';
        }
                                ?>
                                <div  data-id="<?php echo $row->id;?>" class="col-lg-3 col-md-6 member_list">
                                    <div class="panel panel-body b">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#" data-popup="lightbox">
                                                    <img src="<?php echo $img_path;?>" style="width: 70px; height: 70px;" class="img-circle" alt="">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <p class="text-muted m-b-xs"><?php echo isset($row->name)?$row->name:'';?></p>
                                                <h6 class="media-heading"><?php echo isset($row->alias)?$row->alias:'';?></h6>
                                                <h6 class="media-heading"><?php echo isset($row->role)?$row->role:'';?></h6>
                                                
                                            </div>
                                            <br><br>
                                            <div class="pull-right">
                                                    <a  href="javascript:;" class="edit_btn btn btn-info btn-sm r-2x">Edit</a>
                                                    <a href="javascript:;" class="delete_btn btn btn-danger btn-sm r-2x">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }}?>
                            </div>
                        </div>
                        
                        <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">Add Member</button>

                        <!--<a href=""  class="btn btn-primary pull-right" >Next</a>-->
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add Member</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form id="profile" method="post" action="" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" required id="name" name="name"  value="" class="form-control">
                                            </div>


                                            <div class="form-group">
                                                <label>Alias</label>
                                                <input type="text" required id="alias" name="alias"  value="" class="form-control">
                                            </div>


                                            <div class="form-group">
                                                <label>Role</label>
                                                <input type="text" required id="role" name="role"  value="" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <div id="member_image" class="thumbnail col-md-6" style="display:none;">
                                                    <img   src="<?php echo site_url('assets/dist/images/logo.png');?>" alt="" class="">

                                                </div>
                                                <div class="clearfix"></div>
                                                <input type="file" name="" id="img_browse">

                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="form-group">
                                                <input type="submit" id="submit" name="submit" value="Submit" class="btn btn-primary pull-right">
                                            </div>
                                        </form>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-sm-12">
                            <a class="btn btn-primary btn-lg pull-right m-l-xs" href="<?php echo site_url('artistRegistration/media');?>"  role="tab" >Next</a>
                        
                       <?= anchor("artistRegistration","Back",array('class'=>'btn btn-primary btn-md')) ?>
                            </div>
                    </div>
                        <div id="myModal_edit" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit Member</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form id="profile_edit" method="post" action="" enctype="multipart/form-data">





                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" required id="name_edit" name="name"  value="" class="form-control">
                                            </div>


                                            <div class="form-group">
                                                <label>Alias</label>
                                                <input type="text" required id="alias_edit" name="alias"  value="" class="form-control">
                                            </div>


                                            <div class="form-group">
                                                <label>Role</label>
                                                <input type="text" required id="role_edit" name="role"  value="" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <div id="member_image_edit" class="thumbnail col-md-6" style="display:none;">
                                                    <img   src="<?php echo site_url('assets/dist/images/logo.png');?>" alt="" class="">

                                                </div>
                                                <div class="clearfix"></div>
                                                <input type="file" name="" id="img_browse_edit">

                                            </div>

                                            <div class="clearfix"></div>




                                            <div class="form-group">
                                                <input type="submit" id="submit_edit" name="submit" value="Update" class="btn btn-primary pull-right">
                                            </div>
                                        </form>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <br><br>
                        
                        
                        <!-- <a href="javascript:;" onclick="backPage('form-1','form-1')" class="btn btn-primary btn-lg pull-right">Back</a> -->
                    </form>
                    <div class="clearfix"></div>
                </div>
            </div><!-- row -->
        </div><!-- container -->

        <?php $this->load->view('includes/footer');?>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
        <script>
            var orginal_src = '';
            var glob_row=0;
            $(document).ready(function () {
                orginal_src=$('#member_image img').attr('src');
                $(document).on('click','.delete_btn',function(){
                    var delete_btn = $(this);
                    var row_id = $(this).closest('.member_list').data('id');

                    $.ajax({
                        type:'POST',
                        url: '<?php echo site_url('artistProfile/delete_member');?>',
                        data:{row_id:row_id},
                        dataType:'json',


                        success:function(data){
                            if(data.data){
                                delete_btn.closest('.member_list').remove();
                            }

                        },
                        error: function(data){

                        }
                    });
                });
                $(document).on('click','.edit_btn',function(){
                    var delete_btn = $(this);
                    var row_id = $(this).closest('.member_list').data('id');
                    glob_row=row_id;

                    $.ajax({
                        type:'POST',
                        url: '<?php echo site_url('artistProfile/edit_member');?>',
                        data:{row_id:row_id},
                        dataType:'json',


                        success:function(data){

                            $('#myModal_edit').modal('show');
                            $('#name_edit,#role_edit,#alias_edit').val('');
                            if(data.data.name){$('#name_edit').val(data.data.name);}
                            if(data.data.role){$('#role_edit').val(data.data.role);}
                            if(data.data.alias){$('#alias_edit').val(data.data.alias);}
                            if(data.image_url && data.image_url!=''){$('#member_image_edit').show().find('img').attr('src',data.image_url);}

                        },
                        error: function(data){

                        }
                    });
                });
                function readURL(input,pth) {

                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $(pth).show();
                            $(pth+' img').attr('src', e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                    }
                }

                $("#img_browse").change(function(){

                    readURL(this,'#member_image');
                });
                $("#img_browse_edit").change(function(){

                    readURL(this,'#member_image_edit');
                });


                $('#submit').on('click', function () {
                    $("form#profile").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            email: {
                                required: true,
                            },
                            password: {
                                required: true,
                                //minlength: 8
                            }

                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            //form.submit();

                            var formData = new FormData();
                            formData.append('file', $('#img_browse')[0].files[0]);
                            formData.append('name', form.name.value);
                            formData.append('role', form.role.value);
                            formData.append('alias', form.alias.value);
                            console.log(formData);

                            $.ajax({
                                type:'POST',
                                url: '<?php echo site_url('artistProfile/add_member');?>',
                                data:formData,
                                dataType:'json',
                                cache:false,
                                contentType: false,
                                processData: false,
                                success:function(data){
                                    if(data.data){
                                        $('#members_box .row').append(data.data);
                                        $('#member_image').hide();
                                        $('#member_image img').attr('src', orginal_src);
                                    }
                                    $('#profile')[0].reset();
                                    $('#myModal').modal('hide');
                                },
                                error: function(data){

                                }
                            });

                        }
                    });
                });

                $('#submit_edit').on('click', function () {
                    $("form#profile_edit").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            email: {
                                required: true,
                            },
                            password: {
                                required: true,
                                //minlength: 8
                            }

                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            //form.submit();

                            var formData = new FormData();
                            formData.append('file', $('#img_browse_edit')[0].files[0]);
                            formData.append('name', form.name.value);
                            formData.append('role', form.role.value);
                            formData.append('alias', form.alias.value);
                            formData.append('id', glob_row);
                            console.log(formData);

                            $.ajax({
                                type:'POST',
                                url: '<?php echo site_url('artistProfile/edit_member_form');?>',
                                data:formData,
                                dataType:'json',
                                cache:false,
                                contentType: false,
                                processData: false,
                                success:function(data){
                                    if(data.data){
                                        $('#members_box .row [data-id="'+glob_row+'"]').html(data.data);
                                        $('#member_image_edit').hide();
                                        $('#member_image_edit img').attr('src', orginal_src);
                                    }
                                    $('#profile_edit')[0].reset();
                                    $('#myModal_edit').modal('hide');
                                },
                                error: function(data){

                                }
                            });

                        }
                    });
                });
            });
        </script>

        <script>
            $(document).ready(function() {
                $(".select").select2({
                    width: '100%'
                });
                $("#sort").select2({
                    minimumResultsForSearch: -1,
                    width: '100%'
                });
                /*$('.select').multiselect({
                includeSelectAllOption: true,
                selectAllText: 'Check all!',
                numberDisplayed: 0
                });*/
            });
        </script>
        <style>
            .search-btn{
                border-radius: 5px;
                margin-top: 29px;
                padding: 10px 60px;
            }
        </style>
    </body>
</html>