<?php /*echo validation_errors();
echo isset($error)?$error:'';*/
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('includes/head');?>



    </head>
    <body style="background:url('<?php echo base_url();?>/assets/dist/images/loginpage.jpg') center top no-repeat; background-size:cover;">

        <?php $this->load->view('includes/header2');?>
        <div class="container">
            <div class="row loginbox vertical-align">

                <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0 loginContainer">

                    <!--<div class="row pt20 pb20" style="background:#e5e5e5;">
<div class="col-sm-12 col-xs-12">
<img src="<?php echo base_url();?>/assets/dist/images/logo.png" class="img-responsive center-block" />
</div>
</div>-->

                    <h3 class="text-center m-t-lg m-b-lg">Login to Your Account</h3>
                    <form id="login" method="post">
                        <div class="well well-md m-b-none npt">
                            <?php if(isset($error) && $error!=''){?>
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <strong>Warning! </strong><?php echo $error;?>
                            </div>
                            <?php }?>
                            <?php if(isset($success) && $success!=''){?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <strong>Warning! </strong><?php echo $success;?>
                            </div>
                            <?php }?>
                            <div class="form-group">
                                <input value="<?php echo $this->input->post('email');?>" type="email" name="email" class="form-control" placeholder="Your Email" />
                            </div><!-- form-group -->
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Password" />
                            </div><!-- form-group -->
                            <div class="form-group">
                                <input type="submit" id="submit" value="Login" class="btn btn-primary btn-block">
                            </div><!-- form-group -->	
                            <p class="text-center text-muted text-sm">Or Login Using</p>
                            <div class="form-group">
                                <a href="<?php echo site_url('account/facebook_signup/?user=artist&page=artist_login');?>" class="btn btn-facebook btn-block"><i class="fa fa-facebook m-r-sm"></i>Login via Facebook</a>
                                <!--<a href="<?php echo site_url('account/google_signup/?user=artist&page=artist_login');?>" class="btn btn-gmail btn-block disabled"><i class="fa fa-google-plus m-r-sm"></i>Login via Gmail</a>-->
                            </div><!-- form-group -->
                            <p class="small text-center">New to Showspoon? <a href="<?php echo site_url('account/signup'); ?>">Click here</a> to Sign Up</p>
                        </div>
                    </form>




                </div><!-- col-lg-4 -->

            </div><!-- row -->
        </div><!-- container -->
    
    <?php $this->load->view('includes/footer');?>

        <script src="<?php echo base_url();?>/assets/dist/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
        <script>
            $(document).ready(function () {

                // $.validator.setDefaults({ ignore: ":hidden:not(.w-full)" })
                $('#submit').on('click', function () {
                    $("form#login").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            email: {
                                required: true,
                            },
                            password: {
                                required: true,
                                //minlength: 8
                            },


                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            form.submit();
                        }
                    });
                });
            });
        </script>

    </body>
</html>