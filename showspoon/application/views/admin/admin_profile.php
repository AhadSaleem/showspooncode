
<?php $this->load->view('admin/includes/admin_header'); ?>
<body>

<!-- Main navbar -->
<div class="navbar navbar-default header-highlight">
<div class="navbar-header">
<a class="navbar-brand" href="<?php echo site_url('admin_dashboard'); ?>"><img src="<?php echo site_url('assets/images/logo-n.png'); ?>" alt=""></a>

<ul class="nav navbar-nav visible-xs-block">
<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
</ul>
</div>

<div class="navbar-collapse collapse" id="navbar-mobile">
<ul class="nav navbar-nav navbar-right">
<li class="dropdown dropdown-user">
<a class="dropdown-toggle" data-toggle="dropdown">
<img src="<?php echo site_url(); ?>assets/images/placeholder.jpg" alt="">
<span><?php echo ($this->session->userdata('f_name'))?$this->session->userdata('f_name'):'Admin'; ?></span>
<i class="caret"></i>
</a>

<ul class="dropdown-menu dropdown-menu-right">
<li><a href="<?php echo site_url('admin_dashboard/admin_profile'); ?>"><i class="icon-user-plus"></i> My profile</a></li>
<li><a href="<?php echo site_url('admin_login/logout'); ?>"><i class="icon-switch2"></i> Logout</a></li>
</ul>
</li>
</ul>
</div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

<!-- Page content -->
<div class="page-content">

<!-- Main sidebar -->
<div class="sidebar sidebar-main">
<div class="sidebar-content">

<!-- Main navigation -->
<div class="sidebar-category sidebar-category-visible">
<div class="category-content no-padding">
<ul class="navigation navigation-main navigation-accordion">
<!-- Main -->
<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
<li><a href="<?php echo site_url('admin_dashboard'); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/artist'); ?>"><i class="icon-width"></i> <span>Artist</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/venue'); ?>"><i class="icon-width"></i> <span>Venues</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/transection'); ?>"><i class="icon-width"></i> <span>Transactions</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/admin'); ?>"><i class="icon-user"></i> <span>Admin</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/reviews'); ?>"><i class="icon-comments"></i> <span>Reviews</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/settings'); ?>"><i class="icon-user"></i> <span>Settings</span></a></li>
<li class="active"><a href="<?php echo site_url('admin_dashboard/admin_profile'); ?>"><i class="icon-user-plus"></i> <span>Profile</span></a></li>
</ul>
</div>
</div>
<!-- /main navigation -->

</div>
</div>
<!-- /main sidebar -->


<!-- Main content -->
<div class="content-wrapper">

<!-- Content area -->
<div class="content">

<!-- Main charts -->
<div class="row">
<div class="col-lg-12">
<form action="<?php echo site_url(); ?>admin_dashboard/admin_profile" method="post">
<div class="panel panel-flat">
<div class="panel-heading">
<h5 class="panel-title">Profile</h5>
<div class="heading-elements">
<ul class="icons-list">
<li><a data-action="collapse"></a></li>
<li><a data-action="reload"></a></li>
<li><a data-action="close"></a></li>
</ul>
</div>
</div>

<div class="panel-body">
<?php if($this->session->flashdata('msg')){ ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('msg'); ?>
</div>
<?php } ?>
<div class="form-group">
<label>First Name:</label>
<input type="text" name="f_name" class="form-control" placeholder="First Name" value="<?php echo $profile->f_name; ?>">
</div>
<div class="form-group">
<label>Last Name:</label>
<input type="text" name="l_name" class="form-control" placeholder="Last Name" value="<?php echo $profile->l_name; ?>">
</div>
<div class="form-group">
<label>Email Address:</label>
<input type="email" name="email" class="form-control" placeholder="Email Address" value="<?php echo $profile->email; ?>">
</div>
<div class="form-group">
<label>Password:</label>
<input type="password" name="password" class="form-control" placeholder="Password" value="<?php echo $profile->password; ?>">
</div>
<div class="text-right">
<button type="submit" name="submit" value="submit" class="btn btn-primary">Update Profile <i class="icon-arrow-right14 position-right"></i></button>
</div>
</div>
</div>
</form>
</div>
</div>
<!-- Footer -->
<div class="footer text-muted">
&copy; <?php echo date('Y'); ?>. <a href="#">Showspoon</a> by <a href="http://www.mashkraft.com" target="_blank">Mashkraft</a>
</div>
<!-- /footer -->

</div>
<!-- /content area -->

</div>
<!-- /main content -->

</div>
<!-- /page content -->

</div>
<!-- /page container -->
<?php $this->load->view('admin/includes/admin_footer'); ?>
</body>
</html>
