
<?php $this->load->view('admin/includes/admin_header'); ?>
<style>.text{padding:10px 0px;}</style>
<body>

<!-- Main navbar -->
<div class="navbar navbar-default header-highlight">
<div class="navbar-header">
<a class="navbar-brand" href="<?php echo site_url('admin_dashboard'); ?>"><img src="<?php echo site_url('assets/images/logo-n.png'); ?>" alt=""></a>

<ul class="nav navbar-nav visible-xs-block">
<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
</ul>
</div>

<div class="navbar-collapse collapse" id="navbar-mobile">
<ul class="nav navbar-nav navbar-right">
<li class="dropdown dropdown-user">
<a class="dropdown-toggle" data-toggle="dropdown">
<img src="<?php echo site_url(); ?>assets/images/placeholder.jpg" alt="">
<span><?php echo ($this->session->userdata('f_name'))?$this->session->userdata('f_name'):'Admin'; ?></span>
<i class="caret"></i>
</a>

<ul class="dropdown-menu dropdown-menu-right">
<li><a href="<?php echo site_url('admin_dashboard/admin_profile'); ?>"><i class="icon-user-plus"></i> My profile</a></li>
<li><a href="<?php echo site_url('admin_login/logout'); ?>"><i class="icon-switch2"></i> Logout</a></li>
</ul>
</li>
</ul>
</div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

<!-- Page content -->
<div class="page-content">

<!-- Main sidebar -->
<div class="sidebar sidebar-main">
<div class="sidebar-content">

<!-- Main navigation -->
<div class="sidebar-category sidebar-category-visible">
<div class="category-content no-padding">
<ul class="navigation navigation-main navigation-accordion">

<!-- Main -->
<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
<li><a href="<?php echo site_url('admin_dashboard'); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
<li class="active"><a href="<?php echo site_url('admin_dashboard/artist'); ?>"><i class="icon-width"></i> <span>Artist</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/venue'); ?>"><i class="icon-width"></i> <span>Venues</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/transection'); ?>"><i class="icon-width"></i> <span>Transactions</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/admin'); ?>"><i class="icon-user"></i> <span>Admin</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/reviews'); ?>"><i class="icon-comments"></i> <span>Reviews</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/settings'); ?>"><i class="icon-user"></i> <span>Settings</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/admin_profile'); ?>"><i class="icon-user-plus"></i> <span>Profile</span></a></li>
</ul>
</div>
</div>
<!-- /main navigation -->

</div>
</div>
<!-- /main sidebar -->


<!-- Main content -->
<div class="content-wrapper">

<!-- Content area -->
<div class="content">

<!-- Main charts -->
<div class="row">
<div class="col-lg-12">
<div class="panel panel-flat">
<div class="panel-heading">
<h5 class="panel-title">Artist Details</h5>
<div class="heading-elements">
<ul class="icons-list">
<li><a data-action="collapse"></a></li>
<li><a data-action="reload"></a></li>
<li><a data-action="close"></a></li>
</ul>
</div>
</div>

<div class="panel-body">
<div class="row">
<div class="col-md-4">
<div class="col-md-12 text"><strong>Artist Band Type: </strong> <?php echo isset($details->band_name)?$details->band_name:'No band found'; ?> </div>
<div class="col-md-12 text"><strong>Artist Genres: </strong> <?php echo (implode(',',$genres)!="")?implode(',',$genres):'No genre found'; ?> </div>
<div class="col-md-12 text"><strong>Artist City: </strong> <?php echo isset($details->city)?$details->city:'No city found'; ?> </div>
<div class="col-md-12 text"><strong>Artist Address: </strong> <?php echo isset($details->address)?$details->address:'No address found'; ?> </div>
<div class="col-md-12 text"><strong>Artist Website: </strong> <?php echo isset($details->website)?$details->website:'No website found'; ?> </div>
<div class="col-md-12 text"><strong>Artist Rate: </strong> <?php echo isset($details->rating)?$details->rating:'No rating found'; ?> </div>
<div class="col-md-12 text"><strong>Artist Email: </strong> <?php echo isset($details->email)?$details->email:'No email found'; ?> </div>
<div class="col-md-12 text"><strong>Artist Status: </strong> <?php if(@$details->is_active==1){echo '<span class="label label-success">Enable</span>';}else{echo '<span class="label label-danger">Disable</span>';} ?> 
</div>
<div class="col-md-12 text text-justify"><strong>Artist Technical Rider: </strong> <?php echo isset($details->rider)&&$details->rider!=""?$details->rider:'No technical rider found'; ?></div>
</div>
<div class="col-md-4">
<div class="col-md-12 text"><strong>Artist Zip Code: </strong> <?php echo isset($details->zip)?$details->zip:'No zip code found'; ?></div>
<div class="col-md-12 text text-justify"><strong>Artist Biography: </strong> <?php echo isset($details->biography)?$details->biography:'No biography found'; ?></div>
<div class="col-md-12 text"><strong>Artist Capacity: </strong> <?php echo isset($details->capacity)?$details->capacity:'No capacity found'; ?></div>
<div class="col-md-12 text"><strong>Artist Age: </strong> <?php echo isset($details->capacity)?$details->capacity:'No age found'; ?></div>
<div class="col-md-12 text"><strong>Artist Amount: </strong> <?php echo isset($details->rate)?$details->rate:'No amount found'; ?></div>
<div class="col-md-12 text"><strong>Contact Person: </strong> <?php echo isset($details->mobile)?$details->contact_person:'No contact person found'; ?></div>
<div class="col-md-12 text"><strong>Artist Mobile: </strong> <?php echo isset($details->mobile)?$details->mobile:'No mobile found'; ?></div>
<!-- <div class="col-md-12 text"><strong>Artist Payment Account: </strong> <?php echo isset($details->payment_account)?$details->payment_account:'No payment account found'; ?></div> -->
</div>
<div class="col-md-4"> 
<div class="col-md-12 text"><strong>Facebook: </strong> <?php echo isset($details->facebook)&&$details->facebook!=""?$details->facebook:'No url found'; ?></div>
<div class="col-md-12 text"><strong>Twitter: </strong> <?php echo isset($details->twitter)&&$details->twitter!=""?$details->twitter:'No url found'; ?></div>
<div class="col-md-12 text"><strong>Instagram: </strong> <?php echo isset($details->instragram)&&$details->instragram!=""?$details->instragram:'No url found'; ?></div>
<!-- <div class="col-md-12 text"><strong>SoundCloud: </strong> <?php echo isset($details->soundcloud)&&$details->soundcloud!=""?$details->soundcloud:'No url found'; ?></div> -->
</div>
</div>
<div class="col-md-4 text">
<?php if(@$details->profile_image!=""&&isset($details->profile_image)){ ?>
<p><strong>Profile Image: </strong> </p>
<img src="<?php echo site_url(); ?>uploads/users/thumb/<?php echo $details->profile_image; ?>" class="img img-thumbnail img-responsive" style="height:350px;width:100%;">
<?php }else{echo 'No image found';} ?>
</div>
</div>
<div class="panel-heading">
<h5 class="panel-title">Artist Member</h5>
</div>
<div class="panel-body">
<div class="row">
<?php 
if(count($member) > 0){
foreach($member as $member){
	?>
    <div class="col-md-4"> 
    <img src="<?php echo site_url(); ?>uploads/members/thumbs/<?php echo $member->image_url; ?>" class="img img-thumbnail img-responsive" style="height:300px;width:100%;margin-bottom:20px;">
    <p><strong>Name: </strong><span><?php echo isset($member->artist_name)?$member->artist_name:''; ?></span></p>
    <p><strong>Role: </strong><span><?php echo isset($member->artist_role)?$member->artist_role:''; ?></span></p>
    <p><strong>Alias: </strong><span><?php echo isset($member->artist_alias)?$member->artist_alias:''; ?></span></p>
    </div>
    
	<?php 
  }
}else{
	echo '<div class="col-md-4">No member found</div>';
}
?>
</div>
</div>
<div class="panel-heading">
<h5 class="panel-title">Artist Gallery</h5>
</div>
<div class="panel-body">
<div class="row">
<?php
if(count($gallery) > 0){ 
foreach($gallery as $images){
	?>
    <div class="col-md-4"> 
    <img src="<?php echo site_url(); ?>uploads/gallery/<?php echo $images->url; ?>" class="img img-thumbnail img-responsive" style="height:300px;width:100%;margin-bottom:20px;">
    </div>
	<?php 
  }
}else{
	echo '<div class="col-md-4">No gallery found</div>';
}
?>
</div>
</div>
<div class="panel-heading">
<h5 class="panel-title">Artist Media</h5>
</div>
<div class="panel-body">
<div class="row">
<?php 
if(count($media) > 0){
foreach($media as $video){
	$obj=json_decode($video->video_data);
	?>
    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12 pr15 whitetext single_video">
    <div class="embed-responsive embed-responsive-16by9">
    <?php echo isset($obj->html)?$obj->html:''; ?>
    </div> 
    <p><span class="width-90 ellipse pull-left"><?php echo isset($video->caption)?$video->caption:''; ?></span> 
    </div>
	<?php 
  }
 }else{
	echo '<div class="col-md-4">No media found</div>';
}
?>
</div>
</div>
</div>
</div>
</div>
<!-- Footer -->
<div class="footer text-muted">
&copy; <?php echo date('Y'); ?>. <a href="#">Showspoon</a> by <a href="http://www.mashkraft.com" target="_blank">Mashkraft</a>
</div>
<!-- /footer -->

</div>
<!-- /content area -->

</div>
<!-- /main content -->

</div>
<!-- /page content -->

</div>
<!-- /page container -->
<?php $this->load->view('admin/includes/admin_footer'); ?>
</body>
</html>
