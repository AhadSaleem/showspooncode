<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class chat_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->helper('site');
    }
    private $chat=['from','to','message','sent','book_id','recd','update_event'];




    public function get_artist_chat($data=[],$offset=0,$limit=500,$order='id',$sort='DESC') {
        $where='';

        if(isset($data['from']) && is_numeric($data['from'])) {
            $where .= " and from=".(int)$data['from']." ";
        }
        if(isset($data['recd']) && is_numeric($data['recd'])) {
            $where .= " and recd=".(int)$data['recd']." ";
        }
        if(isset($data['to']) && is_numeric($data['to'])) {
            $where .= " and to=".(int)$data['to']." ";
        }
        if(isset($data['book_id']) && is_numeric($data['book_id'])) {
            $where .= " and B.book_id=".(int)$data['book_id']." ";
        }
        if(isset($data['user_id']) && is_numeric($data['user_id'])) {
            $where .= " and (B.from!=".(int)$data['user_id']." )";
        }
        if(isset($data['last_id']) && is_numeric($data['last_id'])) {
            $where .= " and id>".(int)$data['last_id']." ";
        }
        $join='';
        if(isset($data['type']) && is_numeric($data['type']) && $data['type']==1) {
            $join .= " inner join venues V on (V.user_id = B.to || V.user_id = B.from) ";
        }elseif(isset($data['type']) && is_numeric($data['type']) && $data['type']==2) {
            $join .= " inner join artist V on (V.user_id = B.from || V.user_id = B.to)  ";
        }

         
        $query=$this->db->query("select B.*,V.name as name,V.profile_image from chat B
        $join where 1 $where order by $order $sort limit $offset,$limit");
        //inner join artist A on A.user_id = B.from 
        
        return $query->result();
    }
    public function get_artist_chat_count($data=[]) {
        $where='';
        if(isset($data['from']) && is_numeric($data['from'])) {
            $where .= " and from=".(int)$data['from']." ";
        }
         if(isset($data['recd']) && is_numeric($data['recd'])) {
            $where .= " and recd=".(int)$data['recd']." ";
        }
        if(isset($data['to']) && is_numeric($data['to'])) {
            $where .= " and to=".(int)$data['to']." ";
        }
        if(isset($data['book_id']) && is_numeric($data['book_id'])) {
            $where .= " and book_id=".(int)$data['book_id']." ";
        }


        
        $query=$this->db->query("select count(id) as total from chat B
        inner join venues V on V.user_id = B.to 
        inner join artist A on A.user_id = B.from ");

        $row = $query->row();
        return isset($row->total)?$row->total:0;
    }


    
    public function get_venue_chat($data=[],$offset=0,$limit=500,$order='id',$sort='DESC') {
        $where='';
       if(isset($data['from']) && is_numeric($data['from'])) {
            $where .= " and from=".(int)$data['from']." ";
        }
         if(isset($data['recd']) && is_numeric($data['recd'])) {
            $where .= " and recd=".(int)$data['recd']." ";
        }
        if(isset($data['to']) && is_numeric($data['to'])) {
            $where .= " and to=".(int)$data['to']." ";
        }
        if(isset($data['book_id']) && is_numeric($data['book_id'])) {
            $where .= " and B.book_id=".(int)$data['book_id']." ";
        }
        if(isset($data['id']) && is_numeric($data['id'])) {
            $where .= " and id>".(int)$data['id']." ";
        }
        if(isset($data['last_id']) && is_numeric($data['last_id'])) {
            $where .= " and id>".(int)$data['last_id']." ";
        }
        if(isset($data['user_id']) && is_numeric($data['user_id'])) {
            $where .= " and (B.from!=".(int)$data['user_id']." )";
        }
        $join='';
        if(isset($data['type']) && is_numeric($data['type']) && $data['type']==1) {
            $join .= " inner join artist A on (A.user_id = B.from || A.user_id = B.to) ";
        }elseif(isset($data['type']) && is_numeric($data['type']) && $data['type']==2) {
            $join .= " inner join venues A on (A.user_id = B.to || A.user_id = B.from) ";
        }
        

        "select B.*,A.name as name,A.profile_image from chat B
        $join where 1 $where order by $order $sort limit $offset,$limit";
        $query=$this->db->query("select B.*,A.name as name,A.profile_image from chat B
        $join where 1 $where order by $order $sort limit $offset,$limit"); //inner join artist A on A.user_id = B.to 
        //dd($query->result());
        return $query->result();
    }
    public function get_venue_chat_count($data=[]) {
        $where='';
        if(isset($data['from']) && is_numeric($data['from'])) {
            $where .= " and from=".(int)$data['from']." ";
        }
         if(isset($data['recd']) && is_numeric($data['recd'])) {
            $where .= " and recd=".(int)$data['recd']." ";
        }
        if(isset($data['to']) && is_numeric($data['to'])) {
            $where .= " and to=".(int)$data['to']." ";
        }
        if(isset($data['book_id']) && is_numeric($data['book_id'])) {
            $where .= " and B.book_id=".(int)$data['book_id']." ";
        }
        if(isset($data['last_id']) && is_numeric($data['last_id'])) {
            $where .= " and id>".(int)$data['last_id']." ";
        }

        $query=$this->db->query("select count(id) as total from chat B
        inner join artist A on A.artist_id = B.to 
        "); //inner join artist A on A.user_id = B.to 

        $row = $query->row();
        return isset($row->total)?$row->total:0;
    }


    public function get_chat_by_id($id=0) {
        $query=$this->db->query("select * from chat A where  A.id=".(int)$id." limit 1");
        return $query->row();
    }
    public function get_chat_details($id=0) {
        $query=$this->db->query("select *,A.venue_id as row_id from venues A
        left join venue_details AD on A.venue_id = AD.venue_id
        where A.is_delete=0 and A.is_active=1 and A.user_id=".(int)$id." limit 1");
        return $query->row();
    }
    public function get_chat_details_by_id($id=0,$user='artist') {
        if($user=='venue'){
            $query=$this->db->query("select B.*,V.name as user_name from bookings B
        left join venues V on V.venue_id = B.to_id
        where B.is_delete=0 and B.is_active=1 and B.id=".(int)$id." limit 1");    
        }else{
            $query=$this->db->query("select B.*,A.name as user_name from bookings B
        left join artist A on A.artist_id = B.to_id
        where B.is_delete=0 and B.is_active=1 and B.id=".(int)$id." limit 1");    
        }

        return $query->row();
    }





    public function create_chat($data=[]) {

        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->chat)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0){
            
            $insert['sent']=date('Y-m-d H:i:s',time());
            $this->db->insert('chat',$insert);
            return  $this->db->insert_id();

        }
        return false;


    }

    public function update_chat($data=[]) {
        //dd($data);
        $id=0;
        if(isset($data['to']) && $data['to']>0){
            $this->db->where('to',(int)$data['to']);
        }
        /*if(isset($data['recd']) && $data['recd']>0){
            $this->db->where('recd',(int)$data['recd']);
        }*/
        if(isset($data['from']) && $data['from']>0){
            $this->db->where('from',(int)$data['from']);
        }
        if(isset($data['id']) && $data['id']>0){
            $this->db->where('id',(int)$data['id']);
        }
        if(isset($data['book_id']) && is_numeric($data['book_id'])) {
            $this->db->where('book_id',(int)$data['book_id']);
        }
        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->chat)){
                $insert[$key]=$data[$key];
            }
        }
        //dd($insert);
        if(count($insert)>0){
            return $this->db->update('chat',$insert);
        }
        return false;


    }





}
