<?php //echo validation_errors();?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('includes/head');?>
    </head>
    <body style="background:url('<?php echo base_url();?>/assets/images/loginpage.jpg') center top no-repeat; background-size:cover;">


        <?php $this->load->view('includes/header2');?>
        <div class="container">
            <div class="row loginbox vertical-align">

                <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0 loginContainer">

                    <!--<div class="row pt20 pb20" style="background:#e5e5e5;">
<div class="col-sm-12 col-xs-12">
<img src="<?php echo base_url();?>/assets/dist/images/logo.png" class="img-responsive center-block" />
</div>
</div>
-->


                    <h3 class="text-center m-t-lg m-b-sm">Signup to Your Account</h3>


                    <div class="well well-md m-b-none npt">
                        <?php if(isset($error) && $error!=''){?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <strong>Warning! </strong><?php echo $error;?>
                        </div>
                        <?php }?>
                        <?php if(isset($success) && $success!=''){?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <strong>Warning! </strong><?php echo $success;?>
                        </div>
                        <?php }?>

                        <?php if($this->session->flashdata('success')){?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <strong>Warning! </strong><?php echo $this->session->flashdata('success');?>
                        </div>
                        <?php }?>

                        <form id="register" method="post">
                            <?php
                            $type = $this->input->get('type')?$this->input->get('type',true):1;

                            ?>
                            <div class="form-group text-center">
                                <label class="">
                                    <input class="user_type" <?php if($type==1){echo 'checked';}?> type="radio" name="type" value="1"> <span class="small">Musician</span>
                                </label> &nbsp; 
                                <label>
                                    <input class="user_type"  <?php if($type==2){echo 'checked';}?> type="radio" name="type" value="2"> <span class="small">Venue</span>
                                </label>
                            </div><!-- form-group -->
                            <div class="form-group">
                                <input required type="email" name="email" id="email" class="form-control" placeholder="Your Email" required="required" />
                            </div><!-- form-group --> 
                            <span id="emailMsg"></span>
                            <div class="form-group">
                                <input required type="password" id="password" name="password" class="form-control" placeholder="Password" required="required" />
                            </div><!-- form-group -->
                            <div class="form-group">
                                <input required type="password" id="rpassword" name="rpassword" class="form-control" placeholder="Repeat Password" required="required"/>
                            </div><!-- form-group -->
                            <span id="passwordMsg"></span>
                            <div class="form-group">
                                <input type="submit" id="submit" class="btn btn-primary btn-block" value="Signup">
                            </div><!-- form-group -->
                            <p class="text-center text-muted text-sm">or Sign Up using</p>
                            <div class="form-group">
                                <a href="<?php echo site_url('account/facebook_signup/?page=signup&type='.$type);?>" class="btn btn-facebook btn-block"><i class="fa fa-facebook m-r-sm"></i>Signup via Facebook</a>
                                <!--<a href="<?php echo site_url('account/google_signup/?page=signup&type='.$type);?>" class="btn btn-gmail btn-block disabled"><i class="fa fa-google-plus m-r-sm"></i>Signup via Gmail</a>-->
                            </div><!-- form-group -->
                            <p class="small text-center">Already have an account? <a href="<?php echo site_url('account/login'); ?>">Click here</a> to Login</p>
                        </form>
                    </div>




                </div><!-- col-lg-4 -->

            </div><!-- row -->
        </div><!-- container -->


        <div class="footer-bottom m-t-lg">
            <?php $this->load->view('includes/footer');?>
        </div>
        <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
        <script>
            $(document).ready(function () {

                $('.user_type').on('change',function(){
                    var user_type = $(this).val();
                    var social_lnk = '<?php echo site_url('account/google_signup/?');?>';

                    if(user_type==1){
                        $('.btn-facebook').attr('href',social_lnk+'page=signup&type=1');
                    }else{
                        $('.btn-facebook').attr('href',social_lnk+'page=signup&type=2');
                    }
                });
                // $.validator.setDefaults({ ignore: ":hidden:not(.w-full)" })
                $('#submit').on('click', function () {
                    $("form#register").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            email: {
                                required: true,
                            },
                            password: {
                                required: true,
                                //minlength: 8
                            },
                            rpassword: {
                                required: true,
                                equalTo: '#password'
                                //minlength: 8
                            }

                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            form.submit();
                        }
                    });
                });

                $('#email').bind('keyup , change',function(){

                var email = $('#email').val(); 
                $.ajax({
                            url      :  '<?php echo site_url("Account/email_exists"); ?>',
                            type     :  'POST',
                            data     :  {email:email},
                            success  :  function(data)
                            {
                                 if(data=='Email Already Exists')
                                 {
                                    $('#emailMsg').html(data+'<br><br>').css('color','#d9a432');
                                    $('#submit').prop('disabled',true);
                                    $('#password').prop('disabled',true);
                                    $('#rpassword').prop('disabled',true);
                                 }
                                 else
                                 {
                                    $('#emailMsg').html('');
                                    $('#submit').prop('disabled',false);
                                    $('#password').prop('disabled',false);
                                    $('#rpassword').prop('disabled',false);
                                 }
                            }
                      });
                   });

                $('#password , #rpassword').keyup(function(){

                       if($('#password').val()==$('#rpassword').val())
                       {
                           $('#passwordMsg').html('');
                           $('#submit').prop('disabled',false);
                       }
                       else
                       {
                           $('#passwordMsg').html('Password Do not Match'+'<br><br>').css('color','#d9a432');
                           $('#submit').prop('disabled',true);
                       }
                });
            });
        </script>

    </body>
</html>