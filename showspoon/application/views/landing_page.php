<!DOCTYPE html>
<!-- saved from url=(0049)http://165.227.110.185/showspoon/artist/dashboard -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Showspoon</title>
    <link rel="icon" href="<?php echo site_url(); ?>assets/images/icon.png"> 
    <link href="<?php echo site_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/custom-develop.css" rel="stylesheet">
    <!-- new wireframe style -->
    <link href="<?php echo site_url(); ?>assets/css/custom_style.css" rel="stylesheet">
    <!-- for bootstrap live search -->
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.css"> -->
    <!-- for lightbox -->
    <link href="<?php echo site_url(); ?>assets/css/lightgallery.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- for flexslider 
    <link rel="stylesheet" href="assets/css/flexslider.css" type="text/css" media="screen" />
    -->
    <!-- for Roboto -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="<?php echo site_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/select2.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/bootstrap.min.js"></script>
    <!--<script src="http://165.227.110.185/showspoon//assets/dist/js/bootstrap_multiselect.js"></script>-->
    <style>
    .navbar-toggle{background:none !important;border:1px solid #ab7b15 !important;}
    .navbar-toggle > .icon-bar{background:#ab7b15 !important;}
    .error{border:1px solid #d9a432;}
    @import url("https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
      label{
        position: relative;
        cursor: pointer;
        color: #fff;
        font-size: 18px;
        margin-left:3px;
      }

      input[type="checkbox"]{
        position: absolute;
        right: 9000px;
      }

      /*Check box*/
      input[type="checkbox"] + .label-text:before{
        content: "\f096";
        font-family: "FontAwesome";
        speak: none;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        -webkit-font-smoothing:antialiased;
        width: 1em;
        display: inline-block;
        margin-right: 5px;
        font-size: 18px;
        vertical-align: middle;
      }

      input[type="checkbox"]:checked + .label-text:before{
        content: "\f14a";
        color: #d9a432;
        animation: effect 250ms ease-in;
      }

      input[type="checkbox"]:disabled + .label-text{
        color: #aaa;
      }

      input[type="checkbox"]:disabled + .label-text:before{
        content: "\f0c8";
        color: #ccc;
      }
      @keyframes effect{
        0%{transform: scale(0);}
        25%{transform: scale(1.3);}
        75%{transform: scale(1.4);}
        100%{transform: scale(1);}
      }
    </style>

    <!-- for Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo site_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url(); ?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo site_url(); ?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo site_url('assets/favicon/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">
</head>
<body class="no-body-padd">

<header class="main-header landing-page">
      <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <!-- navbar for login link only -->
        <div class="clearfix">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
          <div class="float-left">
            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <img src="<?php echo site_url('assets/images/logo-n.png'); ?>" alt="Logo">
            </a>
          </div>
        </div>
         <!--  <div class="float-right">
            <a class="nav-btn" href="<?php echo site_url(); ?>account/login">Login</a>
          </div> -->
          <div class="collapse navbar-collapse" id="myNavbar">
          <div class="float-center">
          <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo site_url('about'); ?>">About</a></li>
          <li><a href="<?php echo site_url('showspoon-works'); ?>">How Showspoon Works</a></li>
          <li><a href="<?php echo site_url('contact'); ?>">Contact Us</a></li>
          <li><a href="<?php echo site_url('account/login'); ?>">Login</a></li>
          </ul>
          </div>
          </div>
        </div>
      </div>
    </nav>
</header>  


<!-- hero section -->
    <div class="hero">
        <figure class="landing_thumb">
            <img src="<?php echo site_url(); ?>assets/images/drums.jpg" alt="Landing Thumb">
        </figure>
        <div class="banner_caption_area">
            <div class="">
                <h1>The Professional Platform for booking your own music venues</h1>
                <form class="landing_form" action="<?php echo site_url('account/signup'); ?>" method="post">
                    <!-- top radio buttons -->
                    <?php if($this->session->flashdata('success')){?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <strong>Success! </strong><?php echo $this->session->flashdata('success');?>
                        </div>
                        <?php }?>
                    <div class="form-group radio_buttons spacer_ten">
                        <div class="row">
                            <div class="col-xs-6">
                                <label class="">
                                    <input class="user_type" checked="" type="radio" name="type" value="1"> I am an Artist
                                </label>
                            </div>
                            <div class="col-xs-6 text-right">
                                <label>
                            <input class="user_type" type="radio" name="type" value="2"> I am a Venue
                        </label>
                            </div>
                        </div>
                    </div>
                    <!-- top radio buttons end -->
                    <!-- form -->
                    <div class="input-group spacer landing_mail_field">
                      <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                      <input type="text" class="form-control" id="email" name="email" placeholder="Enter your email address" aria-describedby="sizing-addon1">
                    </div>
                    <span id="emailMsg"></span>
                    <div class="input-group spacer landing_pass_field">
                      <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-lock" aria-hidden="true"></i></span>
                      <input type="password" name="password" id="password" class="form-control" placeholder="Enter your password" aria-describedby="sizing-addon2">
                    </div>
                    <span id="passwordMsg"></span>
                    <div class="form-check">
                      <label>
                        <input type="checkbox" name="terms_use" id="terms_use" value="0"> <span class="label-text agreement_lable">I accept the Terms of Use and the Privacy Policy of Showspoon.</span>
                      </label>
                    </div>
                    <span id="termsuseMsg"></span>
                    <div class="spacer" style="margin-top: 10px;">
                        <input type="submit" name="submit" id="signup_btn" value="Sign Up" class="signup_btn_gold">
                    </div>
                    <!-- form ends -->
                </form>
            </div>
        </div>
    </div>
<!-- hero section end -->  
    
     <!-- landing content area -->
      <!-- why showspoon section -->
      <!-- <div class="whyshowspoon">
          <div class="container landing_container">
              <h1 class="small_border_title">
                  Why use Showspoon?
              </h1>
              <div class="spacer_sev_five"></div>
              <div class="row">
                  <div class="col-sm-4">
                      <div class="why_box">
                          <figure>
                              <img src="assets/images/why_ico_1.png" alt="Location">
                          </figure>
                          <figcaption>
                              total freedom
                          </figcaption>
                          <p>Any up-an-coming band can easily look for venues and perform in new cities</p>
                      </div>
                  </div>
                  <div class="col-sm-4">
                      <div class="why_box">
                          <figure>
                              <img src="assets/images/why_ico_2.png" alt="Megaphone">
                          </figure>
                          <figcaption>
                              increased visibility
                          </figcaption>
                          <p>Any music venue can be more visible to other bands and look for bands to hire</p>
                      </div>
                  </div>
                  <div class="col-sm-4">
                      <div class="why_box">
                          <figure>
                              <img src="assets/images/why_ico_3.png" alt="Snapping">
                          </figure>
                          <figcaption>
                              easy to book
                          </figcaption>
                          <p>An easy way to book venues or bands</p>
                      </div>
                  </div>
              </div>
          </div>
      </div> -->
      <!-- why showspoon section ends-->

      <!-- artist section -->
      <!-- <div class="artist_section">
          <div class="container landing_container">
              <h1 class="small_border_title gold">
                  How Does it Work for Artists?
              </h1>
              <p>Bands can search for venues in their area and book them</p>
              <div class="spacer_sev_five"></div>
              <div class="row">
                  <div class="col-sm-6">
                      <div class="work_step_box">
                          <figure>
                              <img src="<?php echo site_url(); ?>assets/images/home_ico_1.png" alt="Home Icon">
                          </figure>
                          <figcaption>
                              1 | Sign up to <br/> Showspoon
                          </figcaption>
                      </div>
                  </div>

                  <div class="col-sm-5 col-sm-offset-1">
                      <div class="work_step_box">
                          <figure>
                              <img src="<?php echo site_url(); ?>assets/images/home_ico_2.png" alt="Home Icon">
                          </figure>
                          <figcaption>
                              3 | Send a request <br/> to the venue
                          </figcaption>
                      </div>
                  </div>

                  <div class="col-sm-6">
                      <div class="work_step_box">
                          <figure>
                              <img src="<?php echo site_url(); ?>assets/images/home_ico_3.png" alt="Home Icon">
                          </figure>
                          <figcaption>
                              2 | Search for a <br/> venue to perform
                          </figcaption>
                      </div>
                  </div>

                  <div class="col-sm-5 col-sm-offset-1">
                      <div class="work_step_box">
                          <figure>
                              <img src="<?php echo site_url(); ?>assets/images/home_ico_4.png" alt="Home Icon">
                          </figure>
                          <figcaption>
                              4 | Confirm the <br/> booking
                          </figcaption>
                      </div>
                  </div>

              </div>
              <div class="spacer">
                <div class="row">
                  <div class="col-sm-6">
                    <a class="signup_btn_gold left-aligned" href="<?php echo site_url(); ?>account/sign_up">Sign Up</a>
                  </div>
                </div>
            </div>
          </div>
      </div> -->
      <!-- artist section ends -->


      <!-- venue section -->
      <!-- <div class="venue_section">
          <div class="container landing_container">
              <h1 class="small_border_title white">
                  How Does it Work for Venues?
              </h1>
              <p class="text-white">Locations can search for bands & musicians in their preferred music genres.</p>
              <div class="spacer_sev_five"></div>
              <div class="row">
                  <div class="col-sm-6">
                      <div class="work_step_box">
                          <figure>
                              <img src="<?php echo site_url(); ?>assets/images/home_ico_1.png" alt="Home Icon">
                          </figure>
                          <figcaption>
                              1 | Sign up to <br/> Showspoon
                          </figcaption>
                      </div>
                  </div>

                  <div class="col-sm-5 col-sm-offset-1">
                      <div class="work_step_box">
                          <figure>
                              <img src="<?php echo site_url(); ?>assets/images/home_ico_2.png" alt="Home Icon">
                          </figure>
                          <figcaption>
                              3 | Send a request <br/> to the artist
                          </figcaption>
                      </div>
                  </div>

                  <div class="col-sm-6">
                      <div class="work_step_box">
                          <figure>
                              <img src="<?php echo site_url(); ?>assets/images/home_ico_3.png" alt="Home Icon">
                          </figure>
                          <figcaption>
                              2 | Search for an artist <br/> you want to hire
                          </figcaption>
                      </div>
                  </div>

                  <div class="col-sm-5 col-sm-offset-1">
                      <div class="work_step_box">
                          <figure>
                              <img src="<?php echo site_url(); ?>assets/images/home_ico_4.png" alt="Home Icon">
                          </figure>
                          <figcaption>
                              4 | Confirm the <br/> booking
                          </figcaption>
                      </div>
                  </div>

              </div>
              <div class="spacer">
                <div class="row">
                  <div class="col-sm-6">
                    <a class="signup_btn_gold left-aligned" href="<?php echo site_url(); ?>account/sign_up/?type=2">Sign Up</a>
                  </div>
                </div>
            </div>
          </div>
      </div> -->
      <!-- venue section ends -->
    <!-- landing content ends -->


    <!-- extra space -->
    <!-- <div class="clearfix" style="height: 300px;"></div> -->
    <!-- extra space ends-->


    <!-- <script src="assets/js/pnotify.min.js"></script> -->

    <footer class="main-footer" style="position:fixed;bottom:0;right:0;left:0;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-left">
                    <p class="copyright">© <?php echo date('Y'); ?> Showspoon. All rights reserved.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                    <ul class="legallinks text-right">
                        <a href="<?php echo site_url();?>privacy-policy"">Privacy Policy</a><span class="sep"> |</span>
                        <a href="<?php echo site_url();?>terms">Terms of Use </a><span class="sep"> |</span>
                        <a href="<?php echo site_url();?>contact">Contact Us</a>
                    </ul>
                </div>
            </div>
            <!-- row -->
        </div>
    </footer>


   


    
    <!-- for bootstrap select -->
    <script src="<?php echo site_url(); ?>assets/js/bootstrap-select.js"></script>


     <!-- for header shrink -->
    <script type="text/javascript">
        $(document).on("scroll", function(){
        if
          ($(document).scrollTop() > 20){
              $(".main-header").addClass("shrink");
            }
            else
            {
                $(".main-header").removeClass("shrink");
            }
        });

        $(document).ready(function(){

        $('#email').bind('keyup , change',function(){

                var email = $('#email').val();
                $.ajax({
                            url      :  '<?php echo site_url("Account/email_exists"); ?>',
                            type     :  'POST',
                            data     :  {email:email},
                            success  :  function(data)
                            {
                                 if(data=='Email Already Exists'){
                                    $('#emailMsg').html('Email Already Exists'+'<br><br>').css('color','#d9a432');
                                    $('#signup_btn').prop('disabled',true);
                                 }
                                 else{
                                    //$('#emailMsg').html('');
                                    $('#signup_btn').prop('disabled',false);
                                 }
                            }
                      });
        });
        });
        $(document).ready(function(){

      $('.landing_form').submit(function(event){
          if($('#email').val()==""){

            $('#email').addClass('error');
            $('#emailMsg').text('This field is required.').css('color','#d9a432');
          }
          if( !validateEmail($('#email').val())){
            $('#email').addClass('error');
            $('#emailMsg').text('Enter valid email address.').css('color','#d9a432');
          }
          if($('#password').val()==""){

            $('#password').addClass('error');
            $('#passwordMsg').text('This field is required.').css('color','#d9a432');
          }
          if($('#terms_use').val()==0){

            $('#termsuseMsg').text('This field is required.').css('color','#d9a432');
          }
          if($('#email').val()!="" && $('#password').val()!="" && $('#terms_use').val()==1 && validateEmail($('#email').val())){
            $('.landing_form').submit();
          }
          event.preventDefault();
      });

      function validateEmail($email) {
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      return emailReg.test( $email );
      }

      $('#email').bind('change , keyup',function(){

         if($('#email').val()!=""){

          $('#email').removeClass('error');
            $('#emailMsg').text('');
         }
      });
      $('#password').bind('change , keyup',function(){

         if($('#password').val()!=""){

          $('#password').removeClass('error');
            $('#passwordMsg').text('');
         }
      });
      $('#terms_use').change('change',function(){
         if($(this).is(':checked')){
            $(this).val(1); 
            $('#termsuseMsg').text('');
         }else{
            $(this).val(0);
            // $('#termsuseMsg').text('This field is required.');
         }
      });
   });
    </script>


</body>

</html>