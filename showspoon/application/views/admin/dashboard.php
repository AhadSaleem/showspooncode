<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('admin/includes/head');?>
    </head>
    <body class="navbar-top">

        <!-- Main navbar -->
        <?php $this->load->view('admin/includes/header');?>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <?php $this->load->view('admin/includes/sidebar');?>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Content area -->
                    <div class="content">




                        <!-- Footer -->
                        <?php $this->load->view('admin/includes/footer');?>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
