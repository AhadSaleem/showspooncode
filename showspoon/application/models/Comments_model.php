<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class comments_model extends CI_Model {

    protected $comment = ['sender_id','receiver_id','created_datetime','comment','is_read','is_active','type','rating'];
    function __construct() {
        parent::__construct();


        $this->load->helper('site');
    }


    public function insert_artist_comments($comment_array)
    {
        $this->db->insert('comments',$comment_array);
    }
    public function insert_venue_comments($comment_array)
    {
        $this->db->insert('comments',$comment_array);
    }
    public function check_venue_review($id)
    {
        $this->db->select('*');
        $this->db->where(array('sender_id'=>$id));
        $query = $this->db->get('comments');
        return $query->num_rows();
    }
    public function check_artist_review($id)
    {
        $this->db->select('*');
        $this->db->where(array('sender_id'=>$id));
        $query = $this->db->get('comments');
        return $query->num_rows();
    }
    public function create_comment($data=[]) {

        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->comment)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0){
            $insert['created_datetime']=date('Y-m-d H:i:s',time());
            $this->db->insert('comments',$insert);
            return $this->db->insert_id();
        }
        return false;


    }
    public function update_comment($data=[]) {

        $id=0;
        if(isset($data['id']) && $data['id']>0){
            $id=$data['id'];
        }
        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->comment)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0){

            return $this->db->where('id',$id)->update('comments',$insert);

        }

        return false;


    }
    public function update_comments($data=[]) {

        $venue_id=0;
        $artist_id=0;
        if(isset($data['artist_id']) && $data['artist_id']>0){
            $artist_id=$data['artist_id'];
        }
        if(isset($data['venue_id']) && $data['venue_id']>0){
            $venue_id=$data['venue_id'];
        }
        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->comment)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0 && ($venue_id>0 || $artist_id>0)){

            if($venue_id>0){
                $this->db->where('venue_id',$venue_id);
            }
            if($artist_id>0){
                $this->db->where('artist_id',$artist_id);
            }
            return $this->db->update('comments',$insert);

        }

        return false;


    }

    public function get_comment_by_id($id=0) {
        $query="select * from comments where id=".(int)$id." limit 1";
        $query=$this->db->query($query);
        return $query->row();
    }
    public function get_comments_artist($id=0,$data=[]) {
        $where='';
        // $where .= " and C.receiver_id='".(int)$id."' ";
        
        // if(isset($data['is_active']) && $data['is_active']!=''){
        //     $where .= " and C.is_active='".(int)$data['is_active']."' ";
        // }
        // if(isset($data['is_read']) && $data['is_read']!=''){
        //     $where .= " and C.is_read='".(int)$data['is_read']."' ";
        // }
        $query="select C.*,V.name as name,V.short_description as short_description,V.profile_image as profile_image from comments C 
        left join venues V on V.user_id = C.sender_id
        where type=2 and receiver_id=$id and C.is_active=1 order by C.id desc";
        $query=$this->db->query($query);
        return $query->result();
    }
    public function get_comments_venue($id=0,$data=[]) {
        $where='';
        // $where .= " and C.receiver_id='".(int)$id."' ";
        
        // if(isset($data['is_active']) && $data['is_active']!=''){
        //     $where .= " and C.is_active=1";
        // }
        // if(isset($data['is_read']) && $data['is_read']!=''){
        //     $where .= " and C.is_read='".(int)$data['is_read']."' ";
        // }
        $query="select C.*,V.name as name,V.profile_image as image,V.short_description as description from comments C 
        left join artist V on V.user_id = C.sender_id
        where type=1 and receiver_id=$id and C.is_active=1 order by C.id desc";
        $query=$this->db->query($query);
        return $query->result();
    }
    public function review_count($id=0)
    {
        $query = $this->db
                 ->select('COUNT(*) AS rc')
                 ->from('comments')
                 ->where(array('receiver_id'=>$id,'is_active'=>1))
                 ->get();
        return $query->row();
    }
    public function avg_review($id=0)
    {
        $query = $this->db
                 ->select('Format(AVG(rating),1) AS avg')
                 ->from('rating')
                 ->where(array('receiver_id'=>$id,'type'=>1))
                 ->get();
        return $query->row();
    }
    public function get_review_count($id=0)
    {
        $query = $this->db
                 ->select('COUNT(*) AS rc')
                 ->from('comments')
                 ->where(array('receiver_id'=>$id,'is_active'=>1))
                 ->get();
        return $query->row();
    }
    public function get_avg_review($id=0,$user_id=0)
    {
        $query = $this->db
                 ->select('Format(AVG(rating),1) AS avg')
                 ->from('rating')
                 ->where(array('receiver_id'=>$id,'type'=>2))
                 ->get();
        return $query->row();
    }
    

}
