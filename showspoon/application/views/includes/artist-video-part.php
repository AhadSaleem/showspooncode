<?php 
$id = isset($id)?$id:0;
$url = isset($url)?$url:'';


$obj = '';
if($video_data!=''){
    $obj=json_decode($video_data);
}
?>
<div class="col-lg-6 col-md-4 col-sm-6 col-xs-12 pr15 whitetext single_video">
    <div class="embed-responsive embed-responsive-16by9">

        <?php echo isset($obj->html)?$obj->html:'';?>
    </div> 
    <p><span class="width-90 ellipse pull-left"><?php echo isset($title)?$title:'';?></span>
        <a  data-toggle="modal" data-target="#modal_vid_<?php echo $id;?>" href=""   class="pull-right"><i class="fa fa-trash-o"></i></a></p>
    <div class="modal fade" id="modal_vid_<?php echo $id;?>" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete video</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete?</p>
                </div>
                <div class="modal-footer">
                    <a href='javascript:void(0)'  class="btn btn-default" data-dismiss="modal" onClick
                       ="deleteFunctionvideo(this,<?php echo $id;?>)" >Yes</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div> 
</div><!-- col-md-4 -->

<script>
</script>
