<!DOCTYPE html>
<html lang="en">
    <head>

        <?php $this->load->view('includes/head');?>
        <link href="<?php echo base_url();?>/assets/dist/plugins/iCheck/all.css" rel="stylesheet">
        <script src="<?php echo base_url();?>/assets/dist/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/plugins/select2/select2.min.js"></script>
        <!--<script src="<?php echo base_url();?>/assets/dist/js/bootstrap_multiselect.js"></script>-->

        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.time.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.date.js"></script>

        <!--<script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/legacy.js"></script>-->
    </head>
    <body>
    

        <?php $this->load->view('includes/header');
        $class = $this->router->fetch_class();
        $method = $this->router->fetch_method();

        ?>
<form id="paypal" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" ></form>



        <div class="container-fluid ListViewv2 lightgrey">
            <div class="container">
                <div class="row">

                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 SideBar">
                       
                    </div><!-- Sidebar -->

                    <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12 InviteList">
                       
                        <div class="row">
                       
                            <div class="alert alert-danger alert-dismissable">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <strong>Danger!</strong> you dont made a successful payment.
                            </div>
                        </div>
                    </div><!-- InviteList -->
                </div><!-- row -->
            </div><!-- container -->
        </div><!-- container-fluid -->

        <?php $this->load->view('includes/footer');?>
    </body>
</html>