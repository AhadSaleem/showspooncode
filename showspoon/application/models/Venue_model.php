<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class venue_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->helper('site');
    }
    private $venue=['name','email','location','genre','profile_image','age','document','is_complete','is_active','user_id','is_delete','city_id','short_description'];
    private $venue_details=['venue_id','description','latitude','longitude','capacity','website','contact_person','mobile','phone','zip','address','payment_account'];
    
    private $venue_gallery=['venue_id','set_featured','url'];
    private $venue_media=['venue_id','type','url','caption','is_active','video_data'];
    
    public function get_venues($data=[],$offset=0,$limit=10,$order='venue_id',$sort='DESC') {
        $where='';
        if(isset($data['city_id']) && is_numeric($data['city_id'])) {
            $where .= " and city_id=".(int)$data['city_id']." ";
        }
        if(isset($data['band_type_id']) && is_numeric($data['band_type_id'])) {
            $where .= " and band_type_id=".(int)$data['band_type_id']." ";
        }
        if(isset($data['genre']) && is_array($data['genre'])) {
            if(count($data['genre'])>0){
                $where .= " and (";
                    foreach($data['genre'] as $key=>$val){
                        if($key==0){
                            $where .= " FIND_IN_SET($val,genre)>0 ";            
                        }else{
                            $where .= " OR FIND_IN_SET($val,genre)>0 ";            
                        }
                    }
                $where .= " ) ";
            }
        }
        $query=$this->db->query("select * from venues where is_delete=0 and is_active=1 and is_complete>0 $where order by $order $sort limit $offset,$limit");
        return $query->result();
    }
    public function get_venues_count($data=[]) {
        $where='';
        if(isset($data['city_id']) && is_numeric($data['city_id'])) {
            $where .= " and city_id=".(int)$data['city_id']." ";
        }
        if(isset($data['band_type_id']) && is_numeric($data['band_type_id'])) {
            $where .= " and band_type_id=".(int)$data['band_type_id']." ";
        }
        if(isset($data['genre']) && is_array($data['genre'])) {
            if(count($data['genre'])>0){
                $where .= " and (";
                    foreach($data['genre'] as $key=>$val){
                        if($key==0){
                            $where .= " FIND_IN_SET($val,genre)>0 ";            
                        }else{
                            $where .= " OR FIND_IN_SET($val,genre)>0 ";            
                        }
                    }
                $where .= " ) ";
            }
        }
        $query=$this->db->query("select count(venue_id) as total from venues where is_delete=0 and is_active=1 and is_complete>0 $where order by venue_id asc");
        $row = $query->row();
        return isset($row->total)?$row->total:0;
    }
    public function get_similar_vanues($explode)
    {
        $where = "";
        if(isset($explode)) {
            if(@count($explode)>0){
                $where .= " and (";
                    foreach($explode as $key=>$val){
                        if($key==0){
                            $where .= " FIND_IN_SET($val,genre)>0 ";            
                        }else{
                            $where .= " OR FIND_IN_SET($val,genre)>0 ";            
                        }
                    }
                $where .= " ) ";
            }
        }
        $query=$this->db->query("select * from venues where is_delete=0 and is_active=1 and is_complete>0 $where order by venue_id asc");
        return $query->result();
    }
    public function get_newest_venue($data_genre,$data_city)
    {
                $where = "";
                if($data_genre!=""){
                $where .= " and ( ";
                    foreach($data_genre as $key=>$val){
                        if($key==0){
                            $where .= " FIND_IN_SET($val,genre)>0 ";            
                        }else{
                            $where .= " OR FIND_IN_SET($val,genre)>0 ";            
                        }
                    }
                $where .= " ) ";
                }

                if($data_city!=""){
                $where .= " and ( ";
                    foreach($data_city as $key=>$val){
                        if($key==0){
                            $where .= " city_id = $val ";            
                        }else{
                            $where .= " OR city_id = $val ";            
                        }
                    }
                $where .= " ) "; 
               } 

        $query=$this->db->query("select * from artist where artist.is_delete=0 and artist.is_active=1 $where order by artist.artist_id DESC");
        return $query->result();
    } 
    public function sorting($data_genre,$data_city,$sorting,$lat,$long)
    {    
        $where = "";
        if($sorting=="Newest Venue"){
          
        if(count($data_genre) > 0 && count($data_city) > 0){
            $order = "";
        }else{
            $order = " order by artist.artist_id desc ";
        }
           if($data_genre!="" && count($data_genre) > 0){
            $where .= " and ( ";
            
            foreach($data_genre as $key=>$val){
                if($key==0){
                    $where .= " FIND_IN_SET($val,genre)>0 ";        
                }else{
                    $where .= " OR FIND_IN_SET($val,genre)>0 ";
                }
            
            }
            $where .= " ) $order "; 
            }
            else{
                $where .= " order by artist.artist_id desc ";
            }
            if($data_city!="" && count($data_city) > 0){
            $where .= " and ( ";
            
            foreach($data_city as $key=>$val){
                if($key==0){
                    $where .= " city_id = $val ";        
                }
                else{
                    $where .= " OR city_id = $val ";
                }
            
            }
            $where .= " ) order by artist.artist_id desc ";
          } 

       }  

       else if($sorting=="Rating"){
        if(count($data_genre) > 0 && count($data_city) > 0){
            $order = "";
        }else{
            $order = " order by artist.rating desc ";
        }
           if($data_genre!="" && count($data_genre) > 0){
            $where .= " and ( ";
            
            foreach($data_genre as $key=>$val){
                if($key==0){
                    $where .= " FIND_IN_SET($val,genre)>0 ";        
                }else{
                    $where .= " OR FIND_IN_SET($val,genre)>0 ";
                }
            
            }
            $where .= " ) $order "; 
            }else{
                $where .= " order by artist.rating desc ";
            }
            if($data_city!="" && count($data_city) > 0){
            $where .= " and ( ";
            
            foreach($data_city as $key=>$val){
                if($key==0){
                    $where .= " city_id = $val ";        
                }else{
                    $where .= " OR city_id = $val ";
                }
            
            }
            $where .= " ) order by artist.rating desc ";
          } 
       } 
       
       else if($sorting=="Most Relevant"){
        if(count($data_genre) > 0 && count($data_city) > 0){
            $order = "";
        }else{
            $order = " order by artist.genre desc ";
        }
           if($data_genre!="" && count($data_genre) > 0){
            $where .= " and ( ";
            
            foreach($data_genre as $key=>$val){
                if($key==0){
                    $where .= " FIND_IN_SET($val,genre)>0 ";        
                }else{
                    $where .= " OR FIND_IN_SET($val,genre)>0 ";
                }
            
            }
            $where .= " ) $order "; 
            }else{
                $where .= " order by artist.genre desc ";
            }
            if($data_city!="" && count($data_city) > 0){
            $where .= " and ( ";
            
            foreach($data_city as $key=>$val){
                if($key==0){
                    $where .= " city_id = $val ";        
                }else{
                    $where .= " OR city_id = $val ";
                }
            
            }
            $where .= " ) order by artist.genre desc ";
          } 
       } 
       
       else if($sorting=="Price"){
        if(count($data_genre) > 0 && count($data_city) > 0){
            $order = "";
            $new = " left join artist_details on artist.artist_id = artist_details.artist_id ";
        }else{
            $order = " order by venue_rate ";
            $new = " left join artist_details on artist.artist_id = artist_details.artist_id ";
        }
           if($data_genre!="" && count($data_genre) > 0){
            $where .= " and ( ";
            
            foreach($data_genre as $key=>$val){
                if($key==0){
                    $where .= " FIND_IN_SET($val,genre)>0 ";        
                }else{
                    $where .= " OR FIND_IN_SET($val,genre)>0 ";
                }
            
            }
            $where .= " ) $order "; 
            }else{
                $where .= "";
            }
            if($data_city!="" && count($data_city) > 0){
            $where .= " and ( ";
            
            foreach($data_city as $key=>$val){
                if($key==0){
                    $where .= " city_id = $val ";        
                }else{
                    $where .= " OR city_id = $val ";
                }
            
            }
            $where .= " ) order by artist_rate desc ";
          } 
       }
       
       else if($sorting=="Distance"){
        if(count($data_genre) > 0 && count($data_city) > 0){
            $order = "";
        }else{
            $order = " order by distance ASC ";
        }
           if($data_genre!="" && count($data_genre) > 0){
            $where .= " and ( ";
            
            foreach($data_genre as $key=>$val){
                if($key==0){
                    $where .= " FIND_IN_SET($val,genre)>0 ";        
                }else{
                    $where .= " OR FIND_IN_SET($val,genre)>0 ";
                }
            
            }
            $where .= " ) $order "; 
            }else{
                $where .= " order by distance ASC ";
            }
            if($data_city!="" && count($data_city) > 0){
            $where .= " and ( ";
            
            foreach($data_city as $key=>$val){
                if($key==0){
                    $where .= " city_id = $val ";        
                }else{
                    $where .= " OR city_id = $val ";
                }
            
            }
            $where .= " ) order by distance ASC ";
          } 
       }
       if($sorting=="Price"){
        $query=$this->db->query("select * from venues $new where artist.is_delete=0 and artist.is_active=1 $where");
        return $query->result();}
        else if($sorting=="Distance"){
            $query = "SELECT *, ( 3959 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) 
                    * cos( radians( longitude ) - radians($long) ) + sin( radians($lat) ) * sin(radians(latitude)) ) ) AS distance 
                    FROM artist 
                    HAVING venues.is_delete=0 and venues.is_active=1 $where ";
                    $query = $this->db->query($query);
                    return $query->result();
        }else{
            $query=$this->db->query("select * from artist where artist.is_delete=0 and artist.is_active=1 $where");
            return $query->result();
        }
    }
    public function get_new_rating($data_genre,$data_city)
    {
                $where = "";
                if($data_genre!=""){
                $where .= " and ( ";
                    foreach($data_genre as $key=>$val){
                        if($key==0){
                            $where .= " FIND_IN_SET($val,genre)>0 ";            
                        }else{
                            $where .= " OR FIND_IN_SET($val,genre)>0 ";            
                        }
                    }
                $where .= " ) ";
                }

                if($data_city!=""){
                $where .= " and ( ";
                    foreach($data_city as $key=>$val){
                        if($key==0){
                            $where .= " city_id = $val ";            
                        }else{
                            $where .= " OR city_id = $val ";            
                        }
                    }
                $where .= " ) "; 
               } 

        $query=$this->db->query("select * from artist where artist.is_delete=0 and artist.is_active=1 $where order by artist.rating DESC");
        return $query->result();
    }
    public function get_relevant_venues($data_genre,$data_city)
    {
        $where = "";
                if($data_genre!=""){
                $where .= " and ( ";
                    foreach($data_genre as $key=>$val){
                        if($key==0){
                            $where .= " FIND_IN_SET($val,genre)>0 ";            
                        }else{
                            $where .= " OR FIND_IN_SET($val,genre)>0 ";            
                        }
                    }
                $where .= " ) ";
                }

                if($data_city!=""){
                $where .= " and ( ";
                    foreach($data_city as $key=>$val){
                        if($key==0){
                            $where .= " city_id = $val ";            
                        }else{
                            $where .= " OR city_id = $val ";            
                        }
                    }
                $where .= " ) "; 
               } 

        $query=$this->db->query("select * from artist where artist.is_delete=0 and artist.is_active=1 $where order by artist.genre DESC");
        return $query->result();
    }
    public function update_bid($array,$user_id)
    {
        $this->db->where('user_id',$user_id)->update('users',$array);
    }
    public function step1($user_id,$step)
    {
        $this->db->where('user_id',$user_id)->update('users',array('bid'=>$step));
    }
    public function step2($user_id,$step)
    {
        $this->db->where('user_id',$user_id)->update('users',array('bid'=>$step));
    }
    public function step3($user_id,$step)
    {
        $this->db->where('user_id',$user_id)->update('users',array('bid'=>$step));
    }
    public function step4($user_id,$step)
    {
        $this->db->where('user_id',$user_id)->update('users',array('bid'=>$step));
    }
    public function get_venue_details($id=0) {
        $query=$this->db->query("select *,A.venue_id as row_id from venues A
        left join venue_details AD on A.venue_id = AD.venue_id
        where A.is_delete=0 and A.is_active=1 and A.user_id=".(int)$id." limit 1");
        return $query->row();
    }
    public function get_venue_details_by_id($id=0) { //,V.venue_id as row_id ,VD.venue_rate as venue_rate , Format(AVG(V.rating),1) as rating
        $query=$this->db->query("select * from venues V 
        inner join venue_details VD on V.venue_id = VD.venue_id
        inner join cities C on C.CityID=V.city_id
        where V.is_delete=0 and V.is_active=1 and V.venue_id=".(int)$id." limit 1");
        return $query->row();
    }
    public function get_type($id=0)
    {
        $query = $this->db
                 ->select('*')
                 ->from('users')
                 ->where('user_id',$id)
                 ->get();
        return $query->row();
    }
    public function getFavourties($id=0){
        $where = '';
        $this->db->select('*');
        $this->db->where('user_id',$id);
        $sql = $this->db->get('users');
        $row = $sql->row();
        $result = explode(',',$row->favourites);
        foreach($result as $key=>$val){
          if($key==0){
            $where .= " artist_id=$val ";
          }else{
            $where .= " or artist_id=$val ";
          }
        }
       $sql = $this->db->query("select * from artist where $where");
       return $sql->result();
    }
    public function getALLVenue($offset,$per_page,$id)
    {  
        $this->db->select("*");
        $this->db->from("venues");
        $this->db->where(array('venues.is_delete'=>0,'venues.is_active'=>1,'venues.is_complete'=>1));
        $this->db->order_by("venues.rating", "DESC");
        $this->db->limit($offset,$per_page);
        $query = $this->db->get();
        $result = $query->result();
        $sql = $this->db->query("select favourites from users where users.user_id=$id");
        $obj = $sql->row()->favourites;
        $arr = explode(',',$obj);

        $output = "";
        $output .= '<div class="ListView search-listing" style="padding-bottom:0;"><div class="showAjaxVenues"></div>
                    <div class="row listing_wrap">';
        foreach($result as $venue) {  
        $like_active='';       
        //$user_favourites = isset($user_favourites)?$user_favourites:[];
        $venue_id = isset($venue->venue_id)?$venue->venue_id:0; 
        if(in_array($venue_id,$arr)){
            $like_active='active';    
        }else{$like_active='';}
        $output .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 artist-block" data-id="" style="height:330px;">
                            <div class="panel panel-default" >
                                <div class="panel-body no-padder">
                                    <div class="gimage">
                                        <a href="javascript:;" class="likebtn '.$like_active.'" data-id="'.$venue->venue_id.'">
                                            <i class="fa fa-heart-o"></i>
                                            <i class="fa fa-heart"></i>
                                        </a>

                                        <a href="'.site_url().'Artist/details/'.$venue->venue_id.'">
                                            <img src="'.site_url().'uploads/users/thumb/'.$venue->profile_image.'" class="img-responsive center-block">
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body wrapper-xs text-center">
                                <h4 class="text-center"><a href="'.site_url().'artist/details/'.$venue->venue_id.'">'.$venue->name.'</a></h4>';

                    $names = array();
                    $explode = explode(',',$venue->genre); 
                    foreach($explode as $exp)
                    {   
                        $exp_names = $this->Artist_model->getGenreNames($exp);
                        foreach($exp_names as $exp_name)
                        {
                            $names[] = $exp_name->name;
                        }
                    }  
                    $query = $this->db
                             ->select('Format(AVG(rating),1) AS avg')
                             ->from('rating')
                             ->where(array('receiver_id'=>$venue->user_id,'type'=>1))
                             ->get();
                    $row = $query->row();
                    if(count($row) > 0){$rating = $row->avg;}else{$rating = 0.0;}
        $output .=   '<h5 class="text-center m-b-none m-t-none">'.implode(' / ',$names).'</h5>';   
        $output .=   '<div class="col-sm-12 text-center ratingstars">
                                        '.star_rating($rating).'';
        if($row->avg!="" && count($row->avg) > 0){
            $rate = explode('.',$row->avg);
            $output .= '<span> ('.$rate[0].')</span>';
        }
        $output .=   '</div>';
        $output .= '<a href="'.site_url().'artistBooking/book/'.$venue->venue_id.'" class="btn btn-default m-t-sm m-b-sm">Send Request</a>
                                </div>
                            </div>
                        </div>';
                         } 
        $output .=  '</div></div>';
        return $output;
    }

    public function check_bid($user_id)
    {
        $query = $this->db->select('*')->from('users')->where('user_id',$user_id)->get();
        return @$query->row()->bid;
    }
    public function get_venue_lat_long($user_id)
    {
        $query = $this->db->query("SELECT * FROM venues WHERE user_id = ".(int)$user_id."");
        return $query->row();
    }
    public function getArtistRadius($radius,$lat,$long)
    {
        $query = "SELECT *, ( 3959 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) 
        * cos( radians( longitude ) - radians($long) ) + sin( radians($lat) ) * sin(radians(latitude)) ) ) AS distance 
        FROM artist 
        HAVING distance < $radius
        ORDER BY distance";
        $query = $this->db->query($query);
        return $query->result();
    } 
    public function get_search_results($search_value,$offset,$per_page){
        $query = $this->db->query("select * from artist where name like '%$search_value%' limit $per_page , $offset");
        return $query->result();
    }
    public function get_search_with_zipcode($search_value)
    {
        $query = $this->db->query("select * from artist_details where zip like '%$search_value%'");
        return $query->result();
    }
    public function get_zipcode_result($id)
    {
        $query = $this->db->query("select * from artist where artist_id = ".(int)$id."");
        return $query->result();
    }
    public function get_featured_venues()
    {
        $where = '';
        $q = $this->db->query("select receiver_id from rating");
        $row = $q->result();
        if(count($row) > 0 && $row!=""){
        $where .= " and ( ";
        foreach($row as $key=>$val){
            if($key==0){
                $where .= " R.receiver_id=$val->receiver_id ";
            }else{
                $where .= " OR R.receiver_id=$val->receiver_id";
            }
        }
        $where .= " ) ";
        }
        $sql = $this->db->query("select R.*, FORMAT(AVG(R.rating),1) as avg_rate,V.venue_id as venue_id,V.name as venue_name,V.profile_image as profile_image from rating R inner join venues V on V.user_id=R.receiver_id where R.type=1 $where group by R.receiver_id order by avg_rate DESC");
        return $sql->result();
    } 
    public function get_recently_booked($id=0)
    {
        $query = $this->db->query("select * from bookings inner join venues on (venues.user_id = bookings.from_id )
        where bookings.is_delete=0 and bookings.is_active=1 and bookings.is_complete=1 and bookings.from_id=".(int)$id."");
        return $query->result();
    }
    public function get_gallery_by_id($id=0) {
        $query="select * from venue_gallery where id=".(int)$id." limit 1";
        $query=$this->db->query($query);
        return $query->row();
    }
    
     function count_all($id)
     {
      $query = $this->db->where('venue_id',$id)->get("venue_gallery");
      return $query->num_rows();
     }
     function count_videos($id)
     {
      $query = $this->db->where(array('venue_id'=>$id,'type'=>'YouTube'))->get("venue_media");
      return $query->num_rows();
     }
     public function get_venue_video($id=0,$limit, $start)
     {
        $this->db->select("*");
        $this->db->from("venue_media");
        $this->db->where(array('venue_id'=>$id,'type'=>'YouTube'));
        $this->db->order_by("id", "ASC");
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        $videos = $query->result();
        $output = "";
        $output .= '<ul id="lightgallery" class="row image-gallery">';
        $output .= '<div role="tabpanel" class="tab-pane" id="videos">';
                     if(isset($videos) && count($videos)>0){
                     foreach($videos as $vid){
                        $video_data = isset($vid->video_data)?$vid->video_data:'';
                        if($video_data!=''){
                            $video_data=json_decode($vid->video_data);
                        }
                        if(isset($video_data->html) && $video_data->html!='')
                        {
                        $output .= '<div class="col-md-4 col-sm-6 col-xs-12 m-b-md" style="padding-left:0;">
                            <div class="embed-responsive embed-responsive-16by9">'.$video_data->html.'</div>
                        </div>';

                        }}
                        }else{
                        $output .= '<p class="text-center">No video found.</p>';
                     }
         
        $output .= '</div></ul>';
        return $output;
     }
     public function get_venue_galleryy($id=0) {
        $query="select * from venue_gallery where venue_id=".(int)$id." order by id asc";
        $query=$this->db->query($query);
        return $query->result();
    }
    public function get_venue_gallery($id=0,$limit, $start) {
        
        $this->db->select("*");
        $this->db->from("venue_gallery");
        $this->db->where(array('venue_id'=>$id));
        $this->db->order_by("id", "ASC");
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        $gallery = $query->result();
        $output = "";
        $output .= '<ul id="lightgallery" class="row image-gallery">';
        if(isset($gallery) && count($gallery)>0){ 
         foreach($gallery as $gal){
         if(isset($gal->url) && $gal->url!='' && file_exists('./uploads/gallery/thumbs/'.$gal->url)){
         $user_image=site_url('uploads/gallery/thumbs/'.$gal->url);
         $full_image=site_url('uploads/gallery/'.$gal->url);
        
        $output .= '<li class="col-xs-6 col-sm-4 col-md-3"  data-src="'.$user_image.'" data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
        <a href="'.site_url('uploads/gallery/thumbs/'.$gal->url).'" data-lightbox="example-set" data-title="Click the right half of the image to move forward.">
            <img class="img-responsive" src="'.site_url('uploads/gallery/thumbs/'.$gal->url).'">
        </a>
        </li>';
        }
           }
        } 
        else
        {
            $output .= '<p class="text-center">No gallery found</p>';
        }

        $output .= '</ul>';
        return $output;
    }
    public function get_venue_gallery_count($id)
    {
        $query="select COUNT(*) AS cnt from venue_gallery where venue_id=".(int)$id." order by id asc";
        $query=$this->db->query($query);
        return $query->row();
    }
    public function get_venue_video_count($id)
    {
        $query="select COUNT(*) AS vd_cnt from venue_media where venue_id=".(int)$id." AND type='YouTube' order by id asc";
        $query=$this->db->query($query);
        return $query->row();
    }
    
    public function create_venue($data=[]) {

        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->venue)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0){
            $this->db->insert('venues',$insert);
            $ven_id = $this->db->insert_id();
            $this->db->insert('venue_details',['venue_id'=>$ven_id]);
            
            return $ven_id;
            
        }
        return false;


    }

    public function update_venue($data=[],$id) {
    
    $this->db->where('user_id',$id)->update('venues',$data);

        // $id=0;
        // if(isset($data['user_id']) && $data['user_id']>0){
        //     $id=$data['user_id'];
        // }
        // $insert=[];
        // foreach($data as $key=>$val){
        //     if(isset($data[$key]) && in_array($key,$this->venue)){
        //         $insert[$key]=$data[$key];
        //     }
        // }
        // if(count($insert)>0){

        //     $this->db->where('user_id',$id)->update('venues',$insert);
            

        // }
        // return false;


    }
    
    public function update_venue_details($id,$data=[]) {
        $this->db->where('venue_id',$id)->update('venue_details',$data);
        // $id=0;
        // if(isset($data['venue_id']) && $data['venue_id']>0){
        //     $id=$data['venue_id'];
        // }
        // $insert=[];
        // foreach($data as $key=>$val){
        //     if(isset($data[$key]) && in_array($key,$this->venue_details)){
        //         $insert[$key]=$data[$key];
        //     }
        // }
        // if(count($insert)>0){

        //     return $this->db->where('venue_id',$id)->update('venue_details',$insert);

        // }
        // return false;


    }
    
    public function get_video_by_id($id=0) {
        $query="select * from venue_media where id=".(int)$id." limit 1";
        $query=$this->db->query($query);
        return $query->row();
    }
    
    
    
    
    public function get_venue_videos($id=0,$types=[]) {
        $where='';

        if(count($types)>0){
            $where .=' and ( ';
            foreach($types as $key=>$row){
                if($key==0){
                    $where .= " type='".$row."' ";
                }else{
                    $where .= " or type='".$row."' ";    
                }

            }
            $where .=' ) ';
        }
        $query="select * from venue_media where venue_id=".(int)$id." $where order by id asc";

        $query=$this->db->query($query);
        return $query->result();
    }


    public function get_venue_media($id=0,$type='') {
        $where='';
        if($type!=''){
            $where .= " and  video_type='".$type."' ";
        }
        $query="select * from venue_media where venue_id=".(int)$id." and is_active=1 $where order by id asc";
        $query=$this->db->query($query);
        return $query->result();
    }
    public function add_picture($data=[]) {
 
         $this->db->insert('venue_gallery',$data);
         return $this->db->insert_id();
         // $insert=[];
        // foreach($data as $key=>$val){
        //     if(isset($data[$key]) && in_array($key,$this->venue_gallery)){
        //         $insert[$key]=$data[$key];
        //     }
        // }
        // if(count($insert)>0){
        //     $this->db->insert('venue_gallery',$insert);
        //     return $this->db->insert_id();
        // }
        // return false;


    }
    public function create_video($data=[]) {

        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->venue_media)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0){
            $this->db->insert('venue_media',$insert);
            return $this->db->insert_id();
        }
        return false;


    }

    public function transaction($transaction){
        $this->db->insert('transaction', $transaction);
        return $this->db->insert_id();
    }
    public function agreement($agreement){
        $this->db->insert('chat',$agreement);
    }
    public function pdf_generation($user_id){

                       /* echo "SELECT Orders.OrderID, Customers.CustomerName, Orders.OrderDate
                FROM Orders
                INNER JOIN Customers ON Orders.CustomerID=Customers.CustomerID;";

                */

        $query =  $this->db->query("SELECT bookings.*,artist.name from artist INNER JOIN bookings on bookings.to_id = artist.user_id WHERE artist.user_id = bookings.to_id");       
        //dd($query);
    }
    public function insert_payment_info($id=0)
    {
        $this->db->insert('payment_info',array('user_id'=>$id));
    }
    public function update_payment_info($id=0,$payment_info)
    {
        $this->db->where('user_id',$id)->update('payment_info',$payment_info);
    }
    public function get_venue_payment_info($id=0)
    {
        $query="select * from payment_info where user_id=".(int)$id."";
        $query=$this->db->query($query);
        return $query->row();
    }
    public function override_img($data=[])
    {
        $this->db->where($data)->update('venues',array('profile_image'=>''));
    }
}
