<?php




if (!function_exists('admin_data')) {

    function admin_data() {

        $ci = & get_instance();
        $sess = $ci->session->userdata('Insightsadmin_logged_in');
        $user_id = isset($sess['user_id']) ? $sess['user_id'] : 0;

        $query = $ci->db->query("SELECT A.*,C.* FROM in_admin A
            left join in_countries C on C.CNT_ISO=A.admin_country
            WHERE admin_id='" . $user_id . "' limit 1");
        return $row = $query->row();
    }

}
if (!function_exists('create_slug')) {

    function create_slug($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

}
if (!function_exists('seo_Url')) {

    function seo_Url($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

}
if (!function_exists('dd')) {

    function dd($print,$die=true) {

        echo '<pre>';
        print_r($print);
        echo '</pre>';
        if($die==true){
            die();    
        }

    }

}
if (!function_exists('star_rating')) {
    function star_rating($rating=0,$total=5) {
        $html='';
        $floor_value = floor($rating);
        $remaining = $rating-$floor_value;
        for($i=0; $i<5; $i++){
            
            if($i<$floor_value){
                $html .= '<i class="fa fa-star"></i>';    
                
            }elseif($remaining>0 && $i==$floor_value){
                $html .= '<i class="fa fa-star-half-o"></i>';
                
                
            }elseif($i>=$floor_value || $rating==0){
                $html .= '<i class="fa fa-star-o"></i>';
                
            }
            
        }
        return $html;


    }
}
if (!function_exists('First_Letter')) {
    function First_Letter($string='') {
        $html='';
        if($string!=''){
            $html = substr($string,0,1);
        }
        
        return $html;


    }
}

?>