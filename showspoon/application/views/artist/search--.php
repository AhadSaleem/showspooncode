<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('includes/head');?>
        <script src="<?php echo base_url();?>/assets/dist/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/plugins/select2/select2.min.js"></script>
        <!--<script src="<?php echo base_url();?>/assets/dist/js/bootstrap_multiselect.js"></script>-->
    </head>
    <body>

        <?php $this->load->view('includes/header');?>


        <div class="container">
            <div class="row">
               <?php $this->load->view('includes/parts/artist-search-form');?>					
            </div><!-- row -->
        </div><!-- container -->

        <div class="container-fluid ListView">
            <div class="container">
                <div class="row">

                   <?php


                    if(isset($artists) && count($artists)>0){
                        
                        foreach($artists as $artist){
                    ?>
                    <?php $this->load->view('includes/parts/artist-part',['artist'=>$artist]);?>					

                    <?php }}else{?>
                    <div class="col-sm-12">
                        <p>No artist found.</p>
                    </div>
                    <?php }?>
                    


                </div><!-- row -->
            </div><!-- container -->
        </div><!-- container-fluid -->



        <script>
            $(document).ready(function() {
                $(".select").select2({
                    width: '100%'
                });
                $("#sort").select2({
                    minimumResultsForSearch: -1,
                    width: '100%'
                });
                /*$('.select').multiselect({
                includeSelectAllOption: true,
                selectAllText: 'Check all!',
                numberDisplayed: 0
                });*/
            });
        </script>
        <style>
            .search-btn{
                border-radius: 5px;
                margin-top: 29px;
                padding: 10px 60px;
            }
        </style>
    </body>
</html>