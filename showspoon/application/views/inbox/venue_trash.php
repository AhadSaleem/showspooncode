
<!DOCTYPE html>
<!-- saved from url=(0049)http://165.227.110.185/showspoon/artist/dashboard -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Trash</title>
    <!-- for Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo site_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url(); ?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo site_url(); ?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo site_url('assets/favicon/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">
    <link href="<?php echo site_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/custom-develop.css" rel="stylesheet">
    <!-- new wireframe style -->
    <link href="<?php echo site_url(); ?>assets/css/custom_style.css" rel="stylesheet">
    <!-- for bootstrap live search -->
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.css"> -->
    <!-- for ekko lightbox -->
    <link href="<?php echo site_url(); ?>assets/css/ekko-lightbox.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- for flexslider -->
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/flexslider.css" type="text/css" media="screen" />




    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="<?php echo site_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/select2.min.js"></script>
    <!--<script src="http://165.227.110.185/showspoon//assets/dist/js/bootstrap_multiselect.js"></script>-->
</head>

<body>

<p id="alert"></p>

    <header class="main-header">
        <!-- Static navbar -->
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>

            <!-- <button type="button" class="navbar-toggle collapsed filter_btn" data-toggle="collapse" data-target="#sidebar" aria-expanded="false" aria-controls="sidebar">
              <i class="fa fa-filter" aria-hidden="true"></i>
            </button> -->


            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <img src="<?php echo site_url(); ?>assets/images/logo-n.png" alt="Logo">
            </a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="search">
                  <!-- search form -->
                  <form id="search" action="<?php echo site_url('venue/search'); ?>" method="post">
                     <input type="search" name="search" placeholder="Search">
                  </form>
                  <!-- search form ends-->
              </li>
              <li><a href="<?php echo site_url(); ?>">Artists</a></li>
              <li><a href="<?php echo site_url('venueBooking/booking'); ?>">My bookings</a></li>
              <li class="active"><a href="<?php echo site_url('venueBooking/inbox'); ?>">Inbox <span id="my_bookings" class="badge badge_upper inbox-count"></span></a></li>
              <li><a href="<?php echo site_url('venueBooking/my_requests'); ?>">Requests <span id="my_bookings" class="badge badge_upper request-count"></span></a></li>
              <li class="favourite"><a href="<?php echo site_url("venue/favourties"); ?>" class="heart"></a></li>
              <li>
                <div class="inset dropdown">                  
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img class="profile_img" src="<?php echo site_url(); ?>uploads/users/thumb/<?php echo $image; ?>"> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url('venueProfile'); ?>">Profile</a></li>
                  <li><a href="<?php echo site_url('account/logout'); ?>">Logout</a></li>
                </ul>
                </div>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container -->
      </nav>

    </header>

    <!-- content area -->
    <div class="container">
      <h1 class="text-center marg_thrty_topper">Trash</h1>
        <div class="row">
            <!-- sidebar -->
            <div class="col-md-3 filter_sidebar">
                <div class="spacer_big">
                    <div class="inbox_left_panel">
                      <a href="javascript:;" class="btn btn-primary btn-block" data-toggle="modal" data-target="#my_modal" role="button">COMPOSE</a>
                        <hr />
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="<?php echo site_url('venueBooking/inbox'); ?>"><span class="badge pull-right" id="inbox_count"></span> Inbox </a>
                            </li>
                            <li><a href="<?php echo site_url('venueBooking/message'); ?>">Sent Message</a></li>
                            <li class="active"><a href="<?php echo site_url('venueBooking/trash'); ?>"><span class="badge pull-right" id="trash_count"></span>Trash</a></li>
                        </ul>
                    </div>
                </div>    
            </div>
            <!-- sidebar ends -->

          <div id="trash_data"></div>
        </div><!--/.row-->
    </div>
    <!-- content area ends -->
   <div id="my_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Send a Message</h3>
      </div>
      <div class="modal-body">
        <form action="" method="post" id="message_send_form1">
        <input type="hidden" name="from" id="from" value="<?php echo $id; ?>">
        <div class="form-group">
        <span id="send_msg1"></span>
        </div>
        <div class="form-group">
        <select class="form-control select" name="to">
        <option value="">Select</option>
        <?php 
        foreach($list as $item){
          ?><option id="to" value="<?php echo $item->user_id; ?>"><?php echo $item->name; ?></option><?php 
        }
        ?>
        </select>
        <span id="list-error"></span>
        </div>
        <div class="form-group">
        <input type="text" name="subject" id="subject1" class="form-control" placeholder="Your subject">
        <span id="subject-error1"></span>
        </div>
        <div class="form-group">
        <textarea name="message_body" id="message_body1" class="form-control" placeholder="Your message"></textarea>
        <span id="message-error1"></span>
        </div>
        <div class="form-group">
        <button type="submit" name="send" class="btn btn-primary" value="Send">Send</button>
        </div>
        </form>
      </div>
    </div>

  </div>
</div>

    <!-- extra space -->
    <div class="clearfix" style="height: 100px;"></div>
    <!-- extra space ends-->


    <script src="<?php echo site_url(); ?>assets/js/pnotify.min.js"></script>

    <footer class="main-footer" style="position:fixed;bottom:0;left:0;right:0;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-left">
                    <p class="copyright">© <?php echo date('Y'); ?> Showspoon. All rights reserved.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                   <ul class="legallinks text-right">
                        <a href="<?php echo site_url('artist/privacy_policy'); ?>">Privacy Policy</a><span class="sep "> |</span>
                        <a href="<?php echo site_url('artist/terms'); ?>">Terms of Use </a><span class="sep "> |</span>
                        <a href="<?php echo site_url('artist/contact'); ?>">Contact Us</a>
                    </ul>
                </div>
            </div>
            <!-- row -->
        </div>
    </footer>


    <!-- for bootstrap select -->
    <script src="<?php echo site_url(); ?>assets/js/bootstrap-select.js"></script>
    <!-- for smooth scroll -->
    <script type="text/javascript">
        // Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
    </script>

    <!-- for back to top -->
    <script type="text/javascript">
     $(document).ready(function(){
      function getArtistRequestCount()
       {
          $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistRequestCount"); ?>',
                        type    :  'POST',
                        dataType:  'JSON',
                        success :  function(data){

                          if(data.status=='request' && data.cnt > 0 && data.is_read==0){
                            $('.request-count').html(data.cnt);
                          }
                        }
                   });
       }
       getArtistRequestCount();
        // var clear = setInterval(function(){
        // getArtistRequestCount(); 
        // },100);
       function getArtistInboxCount(){
        $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistInboxCount"); ?>',
                        type    :  'POST',
                        dataType:  'JSON',
                        success :  function(data){ 

                          if(data.status=='message' && data.cnt > 0 && data.is_read==0){
                            $('.inbox-count').html(data.cnt);
                          }
                        }
                   });
      }
      getArtistInboxCount();
       function getArtistNotification(){
         $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistNotification"); ?>',
                        type    :  'POST',
                        success :  function(data){

                            $('#alert').html(data);
                        }
                   });
      }
      getArtistNotification();
      $(document).on('click','.close',function(){
        var id = $(this).data('id');
        var mr = $(this).data('mr');
        $.ajax({
                        url     :  '<?php echo site_url("venueBooking/deleteArtistNotification"); ?>',
                        type    :  'POST',
                        data    :  {id:id},
                        success :  function(data){
                          if(mr=='request'){
                            $('.request-count').text('');
                          }else if(mr=='message'){
                            $('.inbox-count').text('');
                          }
                        }
                   });
      });
      $.ajax({
      url:'<?php echo site_url("venue/getHeartCount"); ?>',
      type:'POST',
      success:function(data){
        setTimeout(function(){
        $('li.favourite a.heart').text(data);
        },500);
      }
    });
      $(".select").select2({
                    width: '100%'
                });
      $("#sort").select2({
        minimumResultsForSearch: -1,
        width: '100%'
    });
       $('#message_send_form1').submit(function(e){
        
      if($('#subject1').val()==""){
        $('#subject-error1').text('This field is required.').css('color','#a94442');
      }
      if($('.select').val()==""){
        $('#list-error').text('This field is required.').css('color','#a94442');
      }
      if($('#message_body1').val()==""){
        $('#message-error1').text('This field is required.').css('color','#a94442');
      }
      if($('#subject1').val()!="" && $('#message_body1').val()!=""){
        $.ajax({
          url:'<?php echo site_url("venueBooking/sendMessage"); ?>',
          type:'POST',
          data:$(this).serialize(),
          success:function(data){ 
            $('#send_msg1').html(data);
            $('#message_send_form1')[0].reset();
            setTimeout(function(){
              $('#send_msg1').html('');
              $("#my_modal").removeClass("in");
              $(".modal-backdrop").remove();
              $('.modal').modal('hide');
              location.reload();
            },2000);
          }
        });
      }
      e.preventDefault();
      });
      $('#subject1').bind('keyup , change',function(){
      $('#subject-error1').text('');
    });
      $('.select').bind('keyup , change',function(){
      $('#list-error').text('');
    });
    $('#message_body1').bind('keyup , change',function(){
      $('#message-error1').text('');
    });
     $.ajax({
        url : '<?php echo site_url("venueBooking/getInboxCount"); ?>',
        type: 'POST',
        success: function(data){
          if(data==0)
          $('#inbox_count').html('');
          else
          $('#inbox_count').html(data);
        }
      });
     trash_result(1);
     function trash_result(page){
      $.ajax({
        url : '<?php echo site_url("venueBooking/getTrashResult"); ?>',
        type: 'POST',
        data: {page:page},
        dataType: 'JSON',
        success: function(data){

          //$('#trash_count').html(data.trash_count);
          $('#trash_data').html(data.output);
        }
      });
     }
     $(document).on('click','#next',function(){
      var page = $(this).data('id'); 
      trash_result(page);
     });
     $(document).on('click','#prev',function(){
      var page = $(this).data('id');
      trash_result(page);
     });
     var id = new Array();
     $(document).on('change','.check_box',function(){
      if($(this).is(':checked')){
        id.push($(this).val());
      }else{
        for(var i=0;i<id.length;i++){
          if(id[i] == $(this).val()){
          id.splice(i, 1);
          }
        }
      }
      if(id.length > 0){
        $('#trash').css('display','block');
      }else{
        $('#trash').css('display','none');
      } 
     });
     $(document).on('click','#trash',function(){
        if(confirm('Are you sure you want to delete this?')){
        $.ajax({
          url : '<?php echo site_url("venueBooking/trashForever"); ?>',
          type: 'POST',
          dataType: 'JSON',
          data: {id:id},
          success : function(data){
            if(data.result){ 
            for(var d=0;d<id.length;d++){
            $('#remove'+id[d]).fadeOut('slow');
            $('#trash').fadeOut('slow');
            }
            id.length = 0; 
            setTimeout(function(){
              location.reload();
            },1000);
            }
          }
        });
      }else{
        return false;
      }
     });
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('.back-to-top').fadeIn();
            } else {
                $('.back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('.back-to-top').click(function () {
            $('.back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('.back-to-top').tooltip('show');

});
    </script>

    <!-- for ekko lightbox  -->
    <script src="<?php echo site_url(); ?>assets/js/ekko-lightbox.js"></script>
    <script type="text/javascript">
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });
    </script>

    <!-- for flexslider -->
    <!-- FlexSlider -->
  <script defer src="<?php echo site_url(); ?>assets/js/jquery.flexslider.js"></script>

  <script type="text/javascript">
    $(function(){
      SyntaxHighlighter.all();
    });
    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>

  <!-- for header shrink -->
    <script type="text/javascript">
        $(document).on("scroll", function(){
        if
          ($(document).scrollTop() > 20){
              $(".main-header").addClass("shrink");
            }
            else
            {
                $(".main-header").removeClass("shrink");
            }
        });
    </script>


  <!-- for message modal  -->


  <!-- Modal -->
  <div class="modal fade message_modal" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="clearfix member_box">
                <figure>
                    <img src="<?php echo site_url(); ?>assets/images/male.jpg" alt="Member">
                </figure>
                <figcaption>
                   Band Name Here
                </figcaption>
            </div>
        </div>
        <div class="modal-body">
          <form class="">
              <textarea class="form-control" placeholder="Write your message here..."></textarea>
              <div class="pull-right">
                  <input type="submit" name="submit" value="Send Message" class="profile_save_btn_small">
              </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


</body>

</html>