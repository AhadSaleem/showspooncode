<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('includes/head');?>
        <script src="<?php echo base_url();?>/assets/dist/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/plugins/select2/select2.min.js"></script>
        <!--<script src="<?php echo base_url();?>/assets/dist/js/bootstrap_multiselect.js"></script>-->
    </head>
    <body>
        <style>
            .btn-primary,.btn-primary:hover, .btn-primary:focus, .btn-primary:focus:active{
                padding: 6px 12px;
            }
            .search-btn{
                border-radius: 5px;
                margin-top: 29px;
                padding: 10px 60px;
            }
        </style>

        <?php $this->load->view('includes/header');?>




        <div class="container-fluid ">    <!--ListView-->
            <div class="container">


                <div class="panel">

                    <!--<div class="panel-title">
<h2 class="col-sm-12">Media</h2>
</div>-->
                    <div class="clearfix"></div>

                    <div class="panel-body">
                        <?php if(isset($success) && $success!=''){?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <strong>Success!</strong> <?php echo $success;?>
                        </div>
                        <?php }?>
                        <?php if(isset($error) && $error!=''){?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <strong>Warning! </strong> <?php echo $error;?>
                        </div>
                        <?php }?>
                        <ul class="nav nav-tabs" role="tablist">
                            <li class=""><a href="<?php echo site_url('venueProfile');?>" role="tab" >Profile</a></li>
                            
                            <li class="active"><a href="javascript:;"  role="tab" >Media</a></li>
                            <!--<li><a href="<?php echo site_url('venueProfile/socialmedia');?>"  role="tab" >Social Media</a></li>-->
                        </ul>
                        <div class="clearfix m-b-md"></div>

                        <h4>Pictures</h4>
                        <div id="members_box">
                            <div class="row">
                                <?php if(isset($gallery) && count($gallery)>0){

    foreach($gallery as $row){
        if(isset($row->url) && $row->url!=''){
            $img_path=base_url().'uploads/gallery/thumbs/'.$row->url;
        }else{
            $img_path=base_url().'assets/images/placeholder.jpg';
        }
        $this->load->view('includes/artist-gallery-part',['id'=>$row->id,'img_path'=>$img_path]);
    }}?>
                            </div>
                        </div>



                        <button type="button" class="btn btn-primary add_pictures" >Add Pictures</button>



                        <div class="clearfix"></div>
                        <hr>
                        <h4>Videos</h4>
                        <div id="videos_box">
                            <div class="row">
                                <?php if(isset($videos) && count($videos)>0){

    foreach($videos as $row){

        $this->load->view('includes/artist-video-part',['id'=>$row->id,'url'=>$row->url,'title'=>$row->caption,'type'=>$row->type,'video_data'=>$row->video_data]);
    }}?>
                            </div>
                        </div>

                        <button type="button" class="btn btn-primary add_video" data-toggle="modal" data-target="#myModal" >Add Video</button>
                        <div class="clearfix"></div>


                        <div class="clearfix"></div>
                        <hr>
                        <h4>Soundcloud</h4>
                        <div id="soundcloud_box">
                            <div class="row">
                                <?php if(isset($sound_cloud) && count($sound_cloud)>0){

    foreach($sound_cloud as $row){

        if(isset($row->type) && $row->type=='spotify'){
            $this->load->view('includes/artist-spotify-part',['id'=>$row->id,'url'=>$row->url,'title'=>$row->caption,'type'=>$row->type,'video_data'=>$row->video_data]);
        }else{
            $this->load->view('includes/artist-soundcloud-part',['id'=>$row->id,'url'=>$row->url,'title'=>$row->caption,'type'=>$row->type,'video_data'=>$row->video_data]);    
        }
        
    }}?>
                            </div>
                        </div>

                        <button type="button" class="btn btn-primary add_soundcloud" data-toggle="modal" data-target="#myModal_soundcloud" >Add Audio</button>
                        <div class="clearfix"></div>

                        <!--<a href=""  class="btn btn-primary pull-right" >Next</a>-->
                    </div>
                </div>


                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Video</h4>
                            </div>
                            <div class="modal-body">
                                <form id="profile" method="post" action="" >





                                    <div class="form-group">
                                        <label>Type</label>
                                        <select required id="video_type_id" name="type" class="form-control select2">
                                            <option value="YouTube">Youtube</option>
                                            <option value="Vimeo">Vimeo</option>
                                        </select>

                                    </div>


                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" required id="caption" name="caption"  value="" class="form-control">
                                    </div>


                                    <div class="form-group">
                                        <label>Link</label>
                                        <input type="text" required id="url" name="url"  value="" class="form-control">
                                    </div>



                                    <div class="clearfix"></div>




                                    <div class="form-group">
                                        <input type="submit" id="submit" name="submit" value="Submit" class="btn btn-primary pull-right">
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>
                </div>

                <div id="myModal_soundcloud" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Add Soundcloud</h4>
                            </div>
                            <div class="modal-body">
                                <form id="form_soundcloud" method="post" action="" >

                                    <div class="form-group">
                                        <label>Type</label>
                                        <select required id="audio_type_id" name="type" class="form-control select2">
                                            <option value="soundcloud">Soundcloud</option>
                                            <option value="spotify">Spotify</option>
                                        </select>

                                    </div>

                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" required id="caption" name="caption"  value="" class="form-control">
                                    </div>


                                    <div class="form-group">
                                        <label>Link</label>
                                        <input type="url" required id="url" name="url"  value="" class="form-control">
                                    </div>



                                    <div class="clearfix"></div>




                                    <div class="form-group">
                                        <input type="submit" id="submit_soundcloud" name="submit" value="Submit" class="btn btn-primary pull-right">
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>
                </div>


            </div><!-- container -->
        </div><!-- container-fluid -->

    <?php $this->load->view('includes/footer');?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>
        <script type="application/javascript">
            var response;
            $.validator.addMethod(
                "uniqueUserName",
                function (value, element) {

                    value.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);
                    if (RegExp.$3.indexOf('youtu') > -1) {
                        var type = 'youtube';
                    } else if (RegExp.$3.indexOf('vimeo') > -1) {
                        var type = 'vimeo';
                    }
                    if (RegExp.$6 == '' || RegExp.$6 == 'undefined') {
                        //alert(RegExp.$6);
                        response = false;
                        // $('#video_link_error').html("This is not a valid  video  url");
                        //return false;
                    } else {
                        response = true;
                    }
                    return response;
                },
                "This is not a valid video URL"
            );
            $.validator.addMethod(
                "uniqueurl",
                function (value, element) {

                    value.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);
                    if (RegExp.$3.indexOf('youtu') > -1) {
                        var type = 'youtube';
                    } else if (RegExp.$3.indexOf('vimeo') > -1) {
                        var type = 'vimeo';
                    }
                    var videotype = $("#video_type_id").val();
                    if (videotype == 'Vimeo' && type == 'youtube') {
                        response = false;
                    } else if (videotype == 'YouTube' && type == 'vimeo') {
                        // $('#video_link_error').html("This is not a valid  Youtube url");
                        response = false;
                    } else {
                        $('#video_link_error').html("");
                        response = true;
                    }

                    return response;
                },
                "This is not a valid video URL type"
            );
            var bit;
             $.validator.addMethod(
                "checkAudio",
                function (value, element) {
                   var audio_type='';
                    value.match(/(http:|https:|)\/\/(player.|www.)?(soundcloud\.com|spoti(fy\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);
                    if (value.indexOf('spotify') > -1) {
                         audio_type = 'spotify';
                    } else if (value.indexOf('soundcloud') > -1) {
                         audio_type = 'soundcloud';
                    }
                  
                    var videotype = $("#audio_type_id").val();
                      //alert(type+'===='+videotype);
                     console.log(bit+'==>'+videotype+'==>'+audio_type);
                    if (videotype == 'soundcloud' && audio_type == 'spotify') {
                        bit = false;
                        console.log('not valid 1');
                    } else if (videotype == 'spotify' && audio_type == 'soundcloud') {
                        // $('#video_link_error').html("This is not a valid  Youtube url");
                        bit = false;
                        console.log('not valid 2');
                    } else {
                        $('#video_link_error').html("");
                        bit = true;
                        console.log('valid');
                    }
                    console.log(bit+'==>'+videotype+'==>'+audio_type);

                    return bit;
                },
                "This is not a valid URL type"
            );
            
            
        </script>

        <script>
            var orginal_src = '';
            var glob_row=0;
            $(document).ready(function () {
                orginal_src=$('#member_image img').attr('src');
                $(document).on('click','.delete_btn',function(){
                    var delete_btn = $(this);
                    var row_id = $(this).closest('.artist_gallery').data('id');

                    $.ajax({
                        type:'POST',
                        url: '<?php echo site_url('venueProfile/delete_gallery');?>',
                        data:{row_id:row_id},
                        dataType:'json',


                        success:function(data){
                            if(data.data){
                                delete_btn.closest('.artist_gallery').remove();
                            }

                        },
                        error: function(data){

                        }
                    });
                });
                $(document).on('click','.add_pictures',function(){
                    //alert('click');
                    $('#form-upload').remove();

                    $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input  multiple type="file" name="file[]" value="" accept="image/*" /></form>');

                    $('#form-upload input[name=\'file[]\']').trigger('click');

                    if (typeof timer != 'undefined') {
                        clearInterval(timer);
                    }

                    timer = setInterval(function() {
                        if ($('#form-upload input[name=\'file[]\']').val() != '') {
                            var $fileUpload = $("input[type='file']");
                            //console.log($fileUpload.get(0).files);
                            clearInterval(timer);
                            if (parseInt($fileUpload.get(0).files.length)<=1000){
                                $.ajax({

                                    url: '<?php  echo site_url('venueProfile/upload_gallery');?>',
                                    type: 'post',
                                    dataType: 'json',
                                    data: new FormData($('#form-upload')[0]),
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    beforeSend: function() {
                                        $('#button-upload i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                                        $('#button-upload').prop('disabled', true);
                                    },
                                    complete: function() {
                                        $('#button-upload i').replaceWith('<i class="fa fa-upload"></i>');
                                        $('#button-upload').prop('disabled', false);
                                    },
                                    success: function(json) {
                                        if (json['error']) {
                                            alert(json['error']);
                                        }

                                        if (json['success']) {
                                            ///alert(json['success']);
                                            if(json['view']){
                                                $('#members_box .row').append(json['view']);
                                            }
                                            //$('#button-refresh').trigger('click');
                                        }
                                    },
                                    error: function(xhr, ajaxOptions, thrownError) {
                                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                    }
                                });
                            }else
                            {
                                alert("You can only upload a maximum of 10 files once.");
                            }
                        }
                    }, 500);
                });


                $(document).on('click','.edit_btn',function(){
                    var delete_btn = $(this);
                    var row_id = $(this).closest('.member_list').data('id');
                    glob_row=row_id;

                    $.ajax({
                        type:'POST',
                        url: '<?php echo site_url('venueProfile/edit_member');?>',
                        data:{row_id:row_id},
                        dataType:'json',


                        success:function(data){

                            $('#myModal_edit').modal('show');
                            $('#name_edit,#role_edit,#alias_edit').val('');
                            if(data.data.name){$('#name_edit').val(data.data.name);}
                            if(data.data.role){$('#role_edit').val(data.data.role);}
                            if(data.data.alias){$('#alias_edit').val(data.data.alias);}
                            if(data.image_url && data.image_url!=''){$('#member_image_edit').show().find('img').attr('src',data.image_url);}

                        },
                        error: function(data){

                        }
                    });
                });
                function readURL(input,pth) {

                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $(pth).show();
                            $(pth+' img').attr('src', e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                    }
                }

                $("#img_browse").change(function(){

                    readURL(this,'#member_image');
                });
                $("#img_browse_edit").change(function(){

                    readURL(this,'#member_image_edit');
                });


                $('#submit').on('click', function () {
                    $("form#profile").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            type: {
                                required: true,
                            },
                            caption: {
                                required: true,
                                //minlength: 8
                            },
                            url: {
                                required: true,
                                uniqueUserName: true,
                                uniqueurl: true,
                            }

                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            //form.submit();

                            var formData = new FormData();

                            formData.append('url', form.url.value);
                            formData.append('caption', form.caption.value);
                            formData.append('type', form.type.value);
                            console.log(formData);

                            $.ajax({
                                type:'POST',
                                url: '<?php echo site_url('venueProfile/add_video');?>',
                                data:formData,
                                dataType:'json',
                                cache:false,
                                contentType: false,
                                processData: false,
                                success:function(data){
                                    if(data.data){
                                        $('#videos_box .row').append(data.data);

                                    }
                                    $('#profile')[0].reset();
                                    $('#myModal').modal('hide');
                                },
                                error: function(data){

                                }
                            });

                        }
                    });
                });
                $('#submit_soundcloud').on('click', function () {
                    $("form#form_soundcloud").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {

                            url: {
                                required: true,
                                checkAudio: true,

                            }

                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            //form.submit();

                            var formData = new FormData();

                            formData.append('url', form.url.value);
                            formData.append('caption', form.caption.value);
                            formData.append('type', form.type.value);

                            console.log(formData);

                            $.ajax({
                                type:'POST',
                                url: '<?php echo site_url('venueProfile/add_video');?>',
                                data:formData,
                                dataType:'json',
                                cache:false,
                                contentType: false,
                                processData: false,
                                success:function(data){
                                    if(data.data){
                                        $('#soundcloud_box .row').append(data.data);

                                    }
                                    $('#form_soundcloud')[0].reset();
                                    $('#myModal_soundcloud').modal('hide');
                                },
                                error: function(data){

                                }
                            });

                        }
                    });
                });


                $('#submit_edit').on('click', function () {
                    $("form#profile_edit").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            email: {
                                required: true,
                            },
                            password: {
                                required: true,
                                //minlength: 8
                            }

                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            //form.submit();

                            var formData = new FormData();
                            formData.append('file', $('#img_browse_edit')[0].files[0]);
                            formData.append('name', form.name.value);
                            formData.append('role', form.role.value);
                            formData.append('alias', form.alias.value);
                            formData.append('id', glob_row);
                            console.log(formData);

                            $.ajax({
                                type:'POST',
                                url: '<?php echo site_url('venueProfile/edit_member_form');?>',
                                data:formData,
                                dataType:'json',
                                cache:false,
                                contentType: false,
                                processData: false,
                                success:function(data){
                                    if(data.data){
                                        $('#members_box .row [data-id="'+glob_row+'"]').html(data.data);
                                        $('#member_image_edit').hide();
                                        $('#member_image_edit img').attr('src', orginal_src);
                                    }
                                    $('#profile_edit')[0].reset();
                                    $('#myModal_edit').modal('hide');
                                },
                                error: function(data){

                                }
                            });

                        }
                    });
                });
            });
            function deleteFunctionvideo(obj,id){
                //console.log(obj);
                // alert(id);
                $.ajax({
                    type:'POST',
                    url: '<?php echo site_url('venueProfile/delete_video');?>',
                    data:{id:id},
                    dataType:'json',

                    success:function(data){
                        if(data.data){
                            obj.closest('.single_video').remove();
                        }


                    },
                    error: function(data){

                    }
                });
            }
        </script>

        <script>
            $(document).ready(function() {
                $(".select").select2({
                    width: '100%'
                });
                $("#sort").select2({
                    minimumResultsForSearch: -1,
                    width: '100%'
                });
                /*$('.select').multiselect({
                includeSelectAllOption: true,
                selectAllText: 'Check all!',
                numberDisplayed: 0
                });*/
            });
        </script>
        
    </body>
</html>