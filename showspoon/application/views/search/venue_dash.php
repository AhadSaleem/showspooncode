<!DOCTYPE html>
<!-- saved from url=(0049)http://165.227.110.185/showspoon/artist/dashboard -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Venue Dashboard</title>
    <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/custom-develop.css" rel="stylesheet">
    <!-- new wireframe style -->
    <link href="<?php echo base_url(); ?>assets/css/custom_style.css" rel="stylesheet">
    <!-- for bootstrap live search -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-select.css">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.1/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyD_2wo1TYT-WVpx2RVSbyvguEnMjpFi3ps" type="text/javascript"></script>



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>
    <!--<script src="http://165.227.110.185/showspoon//assets/dist/js/bootstrap_multiselect.js"></script>-->
    <style>
    #myGenreList .genre-checkbox{ display:none;}
    #myCityList .city-checkbox{ display:none;}
    </style>
    <!-- for Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo site_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url(); ?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo site_url(); ?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo site_url('assets/favicon/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">
</head>

<body>
<p id="alert"></p>
<div class="content_container"></div>
<div class="send_request"></div>
<section class="canvas" id="canvas">
    <header class="main-header">
        <!-- Static navbar -->
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>

            <button type="button" class="navbar-toggle collapsed filter_btn" data-toggle="collapse" data-target="#sidebar" aria-expanded="false" aria-controls="sidebar">
              <i class="fa fa-filter" aria-hidden="true"></i>
            </button>


            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <img src="<?php echo site_url(); ?>assets/images/logo-n.png" alt="Logo">
            </a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="search">
                  <!-- search form -->
                  <form id="search" action="<?php echo site_url('venue/search'); ?>" method="post">
                     <input type="search" name="search" placeholder="Search" autocomplete="off">
                  </form>
                  <!-- search form ends-->
              </li>
              <li><a href="<?php echo site_url(); ?>">Artists</a></li>
              <li><a href="<?php echo site_url('venueBooking/booking'); ?>">My bookings</a></li>
              <li><a href="<?php echo site_url(); ?>venueBooking/inbox">Inbox <span id="my_bookings" class="badge badge_upper inbox-count"></span></a></li>
              <li><a href="<?php echo site_url(); ?>venueBooking/my_requests">Requests <span id="my_bookings" class="badge badge_upper request-count"></span></a></li>
              <li class="favourite"><a href="<?php echo site_url("venue/favourties"); ?>" class="heart"></a></li>
              <li>
                <div class="inset dropdown">   <?php if($image!=""){$img = site_url()."uploads/users/thumb/".$image; }else{$img = site_url()."assets/blank.png";} ?>               
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img class="profile_img" src="<?php echo $img; ?>"> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url('venueProfile'); ?>">Profile</a></li>
                  <li><a href="<?php echo site_url('account/logout'); ?>">Logout</a></li>
                </ul>
                </div>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container -->
      </nav>

    </header>



    <!-- space just for index -->

    <div class="spacer_medium"></div>
    <!-- space just for index ends-->
    <span id="loader"></span>
    <div class="container">
      <h1 class="text-center marg_thrty_topper">
        Browse Artists
      </h1>
        <div class="row">
            <!-- sidebar -->
            <div class="col-lg-3  filter_sidebar navbar-collapse collapse" id="sidebar">
              <div class="scrollable">
                <div class="spacer">
                    <div class="title_bar clearfix">
                        <h3>genre</h3>
                        <a class="morecontent more-genre" id="loadMoreGenre">more</a>
                        
                    </div><!-- /.title_bar -->
                    <div class="scroll_box_db">
                    <div class="checkbox_options_wrap" id="myGenreList">
                      <!-- <span class="genre-show"></span> -->
                      <?php
                        foreach($genres as $item){

                         ?> 
                          <div class="checkbox genre-checkbox"><label><input type="checkbox" value="<?php echo $item->name; ?>" data-id="<?php echo $item->genre_id; ?>" data-genre="" class="genre"><?php echo $item->name; ?></label></div>
                         <?php 
                      }
                        ?>
                        <!--/.checkbox-->
                    </div><!--/.checkbox_options_wrap-->
                  </div>
                </div><!--/.spacer-->
                 <div class="spacer">
                    <div class="title_bar clearfix">
                        <h3>country</h3>
                    </div><!-- /.title_bar -->
                    <div class="checkbox_options_wrap" style="margin: 0 0 0 0px;">
                     <select class="form-control country">
                     <option value="">Select Country</option>
                     <option value="1">Norway</option>
                     <option value="2">Sweden</option>
                     <option value="3">Denmark</option>
                     <option value="4">United Kingdom</option>
                     </select>
                    </div>
                </div>
                <div class="spacer">
                    <div class="title_bar clearfix">
                        <h3>city</h3>
                        <a class="morecontent more-city" id="loadMoreCity">more</a>
                    </div><!-- /.title_bar -->
                    <div class="scroll_box_db">
                    <div class="checkbox_options_wrap" id="myCityList">
                      <!-- <span class="city-show"></span>-->
                      <?php
                        foreach($cities as $item){

                         ?> 
                          <div class="checkbox city-checkbox" id="city-checkbox"><label><input type="checkbox" value="<?php echo $item->CityName; ?>" data-id="<?php echo $item->CityID; ?>" data-genre="" class="city"><?php echo $item->CityName; ?></label></div>
                         <?php 
                      }
                        ?>
                    </div><!--/.checkbox_options_wrap-->
                  </div>
                </div><!--/.spacer-->

                <div class="spacer">
                    <div class="title_bar clearfix">
                        <h3>radius</h3>
                        <!-- <a class="morecontent" >more</a> -->
                    </div><!-- /.title_bar -->
                    <div class="checkbox_options_wrap">
                        <div class="radio"> 
                           <label>
                             <input type="radio" class="radius" name="optradio" value="10" data-lat="<?php echo $location->latitude; ?>" data-long="<?php echo $location->longitude; ?>"> 10 Km
                           </label>
                        </div><!--/.checkbox-->
                        <div class="radio">
                           <label>
                             <input type="radio" class="radius" name="optradio" value="50" data-lat="<?php echo $location->latitude; ?>" data-long="<?php echo $location->longitude; ?>"> 50 Km
                           </label>
                        </div><!--/.checkbox-->
                        <div class="radio">
                           <label>
                             <input type="radio" class="radius" name="optradio" value="100" data-lat="<?php echo $location->latitude; ?>" data-long="<?php echo $location->longitude; ?>"> 100 Km
                           </label>
                        </div><!--/.checkbox-->     
                        <div class="radio">
                           <label>
                             <input type="radio" class="radius" name="optradio" value="500" data-lat="<?php echo $location->latitude; ?>" data-long="<?php echo $location->longitude; ?>"> 500 Km
                           </label>
                        </div><!--/.checkbox-->                     
                    </div><!--/.checkbox_options_wrap-->
                </div><!--/.spacer-->

                <div class="spacer zipsearch">
                    <div class="form-group has-default has-feedback">
                      <input type="text" class="form-control" id="zipsearch" placeholder="Enter Zip code">
                      <span class="glyphicon glyphicon-search form-control-feedback"></span>
                    </div>
                </div> 

                <!-- for new buttons -->
                <div class="sidebar_filter_buttons" id="theFixed">
                <div class="row">
                  <div class="col-xs-6">
                    <button class="btn btn-primary search_filter">
                      Search
                    </button>
                  </div>
                  <div class="col-xs-6">
                    <button class="btn btn-primary reset">
                      Reset
                    </button>
                  </div>
                </div>
              </div>
                <!-- for new buttons end-->  
              </div><!-- scrollable -->
            </div>
            <!-- sidebar ends -->


            <div class="col-lg-9">

                <div class="spacer">
                    <div class="row">
                      <div class="col-md-4 col-sm-5">
                            <form class="form-inline sorting_form" style="float:left;">
                            <div class="form-group">
                              <label class="control-label" for="sorting">Sort by:</label>
                            </div>
                            <div class="form-group">
                              <select id="sorting" class="selectpicker" data-live-search="false" title="">
                                <option value="Most Relevant">Most Relevant</option>
                                <option value="Rating">Rating</option>
                                <option value="Price">Price</option>
                                <option value="Newest Artist">Newly Added</option>
                                <option value="Distance">Distance</option>
                                <option value="Popularity">Popularity</option>
                              </select>
                            </div>
                          </form><!--/.sorting_form-->
                        </div>
                        <div class="col-md-8 col-sm-7">
                            <div class="spacer_mini tags_wrap"> 
                                <span class="selected_genre"></span>
                                <span class="append-genre-cross"></span>
                            </div>
                            <div class="spacer_mini tags_wrap"> 
                                <span class="artist_count"></span>
                                <span class="selected_city"></span>
                                <span class="append-city-cross"></span>
                            </div>
                        </div>
                    </div>
                </div><!--/.spacer -->

                <!-- listing content starts -->
                <span class="allVenues"></span>
                <span class="pages"></span>
                <span class="page_search_link"></span>
                <!-- listing content ends  -->

            </div>
        </div><!--/.row-->
    </div>


  <div class="spacer_hundred"></div>


    <script src="<?php echo site_url(); ?>assets/js/pnotify.min.js"></script>

    <footer class="main-footer" style="position:fixed;bottom:0;left:0;right:0;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-left">
                    <p class="copyright">© <?php echo date('Y'); ?> Showspoon. All rights reserved.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                   <ul class="legallinks text-right">
                        <a href="<?php echo site_url('venue/privacy_policy'); ?>">Privacy Policy</a><span class="sep "> |</span>
                        <a href="<?php echo site_url('venue/terms'); ?>">Terms of Use </a><span class="sep "> |</span>
                        <a href="<?php echo site_url('venue/contact'); ?>">Contact Us</a>
                    </ul>
                </div>
            </div>
            <!-- row -->
        </div>
    </footer> 
</section>

    <!-- for bootstrap select -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-select.js"></script>
    <script type="text/javascript">
        $(document).on("scroll", function(){
        if
          ($(document).scrollTop() > 20){
              $(".main-header").addClass("shrink");
            }
            else
            {
                $(".main-header").removeClass("shrink");
            }
        });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
    $(document).on('change','.country',function(){
      var id = $(this).val(); 
      $.ajax({
        url : '<?php echo site_url("venue/city_list_data"); ?>',
        type : 'POST',
        dataType : 'JSON',
        data : {id:id},
        success : function(data){  
          $('#myCityList').html(data.data);
          c=5;
          $('#myCityList .city-checkbox:lt('+c+')').show();
        }
      });
    });
     $.ajax({
      url:'<?php echo site_url("venue/getHeartCount"); ?>',
      type:'POST',
      success:function(data){
        setTimeout(function(){
        $('li.favourite a.heart').text(data);
        },500);
      }
    });
    function getVenueRequestCount(){
          $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistRequestCount"); ?>',
                        type    :  'POST',
                        dataType:  'JSON',
                        success :  function(data){
                          if(data.status=='request' && data.cnt > 0 && data.is_read==0){
                            $('.request-count').html(data.cnt);
                          }
                        }
                   });
       }
       getVenueRequestCount(); 
        // var clear = setInterval(function(){
        // getVenueRequestCount(); 
        // },100);
      function getArtistInboxCount(){
        $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistInboxCount"); ?>',
                        type    :  'POST',
                        dataType:  'JSON',
                        success :  function(data){ 

                          if(data.status=='message' && data.cnt > 0 && data.is_read==0){
                            $('.inbox-count').html(data.cnt);
                          }
                        }
                   });
      }
      getArtistInboxCount();
      function getVenueNotification(){
         $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistNotification"); ?>',
                        type    :  'POST',
                        success :  function(data){

                            $('#alert').html(data);
                        }
                   });
      }
      getVenueNotification();
    $(document).on('click','.close',function(){
        var id = $(this).data('id');
        var mr = $(this).data('mr');
        $.ajax({
                        url     :  '<?php echo site_url("venueBooking/deleteArtistNotification"); ?>',
                        type    :  'POST',
                        data    :  {id:id},
                        success :  function(data){

                          if(mr=='request'){
                            $('.request-count').text('');
                          }else if(mr=='message'){
                            $('.inbox-count').text('');
                          }
                        }
                   });
      });
    size_li = $("#myGenreList .genre-checkbox").size();
    x=5;
    $('#myGenreList .genre-checkbox:lt('+x+')').show();
    
    $('#loadMoreGenre').click(function () {
      $(this).toggleClass('active');
      if($(this).hasClass('active')){
        
        x= (x+5 <= size_li) ? x+5000 : size_li;
        $('#myGenreList .genre-checkbox:lt('+x+')').show();
       }else{
       
        $('#myGenreList .genre-checkbox').hide();
        x=5;
        $('#myGenreList .genre-checkbox:lt('+x+')').show();
       }
    });


    size = $("#myCityList .city-checkbox").size();
     c=5;
    $('#myCityList .city-checkbox:lt('+c+')').show();
    
    $('#loadMoreCity').click(function () {
      $(this).toggleClass('active');
      if($(this).hasClass('active')){
        
        c= (c+5 <= size) ? c+5000 : size;
        $('#myCityList .city-checkbox:lt('+c+')').show();
       }else{
       
        $('#myCityList .city-checkbox').hide();
        c=5;
        $('#myCityList .city-checkbox:lt('+c+')').show();
       }
    });



    // allVenues(1);
    // function allVenues(page)
    // {
    //      $.ajax({
    //                  url      :    '<?php echo site_url(); ?>Venue/getALLVenue/' +page,
    //                  type     :    'POST',
    //                  dataType :    'JSON',
    //                  //data     :    {page:page},
    //                  success  :    function(data)
    //                  {
    //                       $('.allVenues').html(data.data);
    //                       $('.pages').html(data.pagination_link);
    //                  }
    //            });
    // }
    

  $(document).on('click','.likebtn',function(e){
  e.preventDefault(); 
  var obj_this=$(this); 
  if($(this).hasClass('active')){
  var btn_action='remove';
  }else{
  var btn_action='add';
  }
  var row_id = $(this).data('id'); 
  $.ajax({
  type:'POST',
  url: '<?php echo site_url('venue/add_favourites');?>',
  data:{row_id:row_id,action:btn_action},
  dataType:'json',


  success:function(data){
  if(data.data){
  var old_count = $('li a.heart').text();
  if(obj_this.hasClass('active')){
  old_count--;
  $('li.favourite a.heart').text(old_count);
  //obj_this.text('Add to Favourite');
  obj_this.removeClass('active');
  }else{
  obj_this.addClass('active');
  old_count++;
  $('li.favourite a.heart').text(old_count);
  //obj_this.text('Remove Favourite');
  }
  }

  },
  error: function(data){

  }
  });
  });

   $(document).on('click','.reset',function(){

      arr.length = 0;
      arr1.length = 0;
      lat_array.length = 0;
      long_array.length = 0;
      radius_array.length = 0;
      zip.length = 0;
      $('.append-genre-cross').html('');
      $('.append-city-cross').html('');
      $('.selected_genre').html('');
      $('.selected_city').html('');
      $('input:radio').removeAttr('checked');
      $('input:checkbox').removeAttr('checked');
      getResult(arr,arr1,lat_array,long_array,radius_array,1,'','','',zip);
   });
    var zip = [];
    $(document).on('change',function(event){

        var zipsearch = $('#zipsearch').val();
        zip.push(zipsearch);
        $('#zipsearch').val('');
        // if(zipsearch!="" && event.keyCode==13)
        // {
        //    $('.append-genre-cross').html('');
        //    $('.selected_genre').text(''); 
        //    $('.append-city-cross').html('');
        //    $('.selected_city').text('');
        //    $('input[type="checkbox"]').removeAttr("checked");
        //    $.ajax({
        //               url     :  '<?php echo site_url(); ?>Venue/search_with_zipcode',
        //               type    :  'POST',
        //               data    :  {zipsearch:zipsearch},
        //               success :  function(zip){
        //                  $('#zipsearch').val('');
        //                  $('.listing_wrap').html('');
        //                  $('.pages').html('');
        //                  $('.showAjaxVenues').html(zip); 
        //               }
        //          });
        // }
    });
// $(document).on("click","a[rel='tab']",function(e){
// //e.preventDefault(); 
// pageurl = $(this).attr('href');

// $.ajax({
//   url:pageurl+'?rel=tab',
//   success: function(data){
//   $('#canvas').hide();
//   $('.content_container').html(data);
//   }
//       });

// //to change the browser URL to 'pageurl'
// if(pageurl!=window.location){
// window.history.pushState({path:pageurl},'',pageurl);    
// }
// return false;  
// });

/* the below code is to override back button to get the ajax content without reload*/
// $(window).bind('popstate', function() {
// $.ajax({url:location.pathname+'?rel=tab',success: function(data){
// $('.content_container').html('');
// $('#canvas').show();
// }});
// });

// $(document).on("click","a[rel='req']",function(e){
// //e.preventDefault(); 
// pageurl = $(this).attr('href'); 

// $.ajax({
//   url:pageurl+'?rel=req',
//   success: function(data){
//   $('.content_container').hide();
//   $('#canvas').hide();
//   $('.send_request').html(data);
//   }
//       });

// //to change the browser URL to 'pageurl'
// if(pageurl!=window.location){
// window.history.pushState({path:pageurl},'',pageurl);    
// }
// return false;  
// });

/* the below code is to override back button to get the ajax content without reload*/
// $(window).bind('popstate', function() {
// $.ajax({url:location.pathname+'?rel=req',success: function(data){
// $('.send_request').html('');
// $('.content_container').show();
// }});
// });
      var lat_array  = [];
      var long_array = [];
      var radius_array = [];
      $('.radius').on('click',function(){

           if($(this).is(':checked'))
           {  
              var radius = $(this).val(); 
              var latitude  = $(this).data('lat');
              var longitude = $(this).data('long');
              lat_array.push(latitude);
              long_array.push(longitude); 
              radius_array.push(radius);
           } 
      });

    $(document).on('keyup','input[type="search"]',function(event){

        var search_value = $('input[type="search"]').val();
        if(search_value!="" && event.keyCode==13)
        {
           $('.append-genre-cross').html('');
           $('.selected_genre').text(''); 
           $('.append-city-cross').html('');
           $('.selected_city').text('');
           $('input[type="checkbox"]').removeAttr("checked");
           $.ajax({
                      url     :  '<?php echo site_url(); ?>Venue/search_result',
                      type    :  'POST',
                      data    :  {search_value:search_value},
                      success :  function(search){
                         
                         $('input[type="search"]').val('');
                         $('.listing_wrap').html('');
                         $('.showAjaxVenues').html(search); 
                         $('.pages').html('');
                      }
                 });
        }
    });

                 var arr = [];

                 $('.genre').click(function(){ 
                  
                  var id = $(this).data('id');
                  var genre = $(this).val();
                  if($(this).is(':checked')){   
                    $('.selected_genre').text(' Selected Genre: ');
                    arr.push($(this).data('id')); 
                    $(this).prop('class','rem'+id);
                    $('.append-genre-cross').append(' <span class="tag label label-primery remove'+id+'"><span>'+genre+'</span><a><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></a></span> ');
                  }
                    else{
                    if(arr.length==1)
                    {
                        $('.selected_genre').text('');
                    }
                    for(var i = 0; i < arr.length; i++)
                    {
                        if(arr[i] == $(this).data('id'))
                            arr.splice(i, 1); 
                    } 
                    $('.remove' + id).remove();
                    getResult(arr,arr1,lat_array,long_array,radius_array,1,'','','',zip);
                   // getResult(arr,arr1,lat_array,long_array,radius_array);
                   } 

                   $('.remove' + id).click(function(){ 

                    if(arr.length==1)
                    {
                        $('.selected_genre').text('');
                    }
                    
                    arr = $.grep(arr, function(value) {
                    return value != id;
                    });

                      $('.remove' + id).remove(); 
                      $('.rem' + id).prop('checked',false);
                      getResult(arr,arr1,lat_array,long_array,radius_array,1,'','','',zip);
                      //getResult(arr,arr1,lat_array,long_array,radius_array);
                 }); 
                    
                 });

                 var arr1 = [];


                 $(document).on('click','.city',function(){ 
                    var city_id = $(this).data('id'); 
                    var city = $(this).val(); 
                    if($(this).is(':checked')){
                    $('.selected_city').text(' Selected City: ');
                    arr1.push($(this).data('id')); 
                    $(this).addClass('rem1'+city_id);
                    $('.append-city-cross').append(' <span class="tag label label-primery remove1'+city_id+'"><span>'+city+'</span><a><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></a></span> ');
                    }
                    else{ 
                    if(arr1.length==1)
                    {
                        $('.selected_city').text('');
                    }
                    for(var i = 0; i < arr1.length; i++)
                    {
                        if(arr1[i] == $(this).data('id'))
                            arr1.splice(i, 1);
                    } 
                    $('.remove1' + city_id).remove();
                    getResult(arr,arr1,lat_array,long_array,radius_array,1,'','','',zip);
                   }

                   $('.remove1' + city_id).click(function(){ 
                  
                    if(arr1.length==1)
                    {
                        $('.selected_city').text('');
                    }

                    arr1 = $.grep(arr1, function(value1){
                    return value1 != city_id;
                    });

                      $('.remove1' + city_id).remove(); 
                      $('.rem1' + city_id).attr('checked',false);
                      getResult(arr,arr1,lat_array,long_array,radius_array,1,'','','',zip);
                   }); 
                    
                  });
      
      $(document).on('change','#sorting',function(){

          var sorting = $(this).val(); 
          var latitude  = $('.radius').data('lat');
          var longitude = $('.radius').data('long');
          getResult(arr,arr1,lat_array,long_array,radius_array,1,sorting,latitude,longitude,zip);
      });


        $('.likebtn').on('click',function(e){
                            e.preventDefault(); 
                            var obj_this=$(this);
                            if($(this).hasClass('active')){
                                var btn_action='remove';
                            }else{
                                var btn_action='add';
                            }
                            var row_id = $(this).data('id');
                            $.ajax({
                                type:'POST',
                                url: '<?php echo site_url('venue/add_favourites');?>',
                                data:{row_id:row_id,action:btn_action},
                                dataType:'json',


                                success:function(data){
                                    if(data.data){
                                        var old_count = $('li a.heart').text();
                                        if(obj_this.hasClass('active')){
                                            old_count--;
                                            $('li a.heart').text(old_count);
                                            //obj_this.text('Add to Favourite');
                                            obj_this.removeClass('active');
                                        }else{
                                            obj_this.addClass('active');
                                            old_count++;
                                            $('li a.heart').text(old_count);
                                            //obj_this.text('Remove Favourite');
                                        }
                                    }

                                },
                                error: function(data){

                                }
                            });
                });

      $(document).on('click','.search_filter',function(){ 
       if(arr!="" || arr1!="" || radius_array!="" || zip!=""){ 
          //getResult(arr,arr1,lat_array,long_array,radius_array);
          getResult(arr,arr1,lat_array,long_array,radius_array,1,'','','',zip);
       } 
        });
      getResult(arr,arr1,lat_array,long_array,radius_array,1,'','','',zip);
      $(document).on("click", ".pagination li a", function(event){
      event.preventDefault();
      var page = $(this).data("ci-pagination-page");
      getResult(arr,arr1,lat_array,long_array,radius_array,page,'','','',zip);
     });
      function getResult(arr,arr1,lat_array,long_array,radius_array,page,sorting,latitude,longitude,zip){
       
        $.ajax({
                url      :    '<?php echo site_url(); ?>venue/getAjaxArtistDetails/'+page,
                type     :    'POST',
                dataType :    'JSON',
                data     :    {arr:arr,arr1:arr1,lat:lat_array,long:long_array,radius:radius_array,page:page,sorting:sorting,latitude:latitude,longitude:longitude,zip:zip}, 
                beforeSend : function(){
                   $('#loader').html('<div style="margin: 0px; padding: 0px; position: fixed; right: 0px; top: 0px; width: 100%; height: 100%; background-color: rgb(102, 102, 102); z-index: 30001; opacity: 0.8;"><p style="position: absolute; color: White; top: 50%; left: 50%;"><img src="<?php echo site_url(); ?>assets/loading.gif" style="width:70%; height:70%;"></p></div>');
                },
                success  :    function(result){   
                     if(result!=""){
                     $('.pages').html('');
                     $('#loader').html('');
                     //$('.listing_wrap').html(result.output); 
                     $('.allVenues').html(result.output); 
                     $('.pages').html(result.page_search_link);
                     
                   }else{
                    $('.pages').html('');
                    $('.listing_wrap').html('<p style="text-align:center;">No search found.</p>');
                    $('#loader').html('');
                   }
                 
                 }
             });
      }  
    });  
    </script>
</body>

</html>