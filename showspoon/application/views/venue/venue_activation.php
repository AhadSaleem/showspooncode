<!DOCTYPE html>
<!-- saved from url=(0049)http://165.227.110.185/showspoon/artist/dashboard -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Venue Profile</title>
    <!-- for Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo site_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url(); ?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo site_url(); ?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo site_url('assets/favicon/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">
    <link href="<?php echo site_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/custom-develop.css" rel="stylesheet">
    <!-- new wireframe style -->
    <link href="<?php echo site_url(); ?>assets/css/custom_style.css" rel="stylesheet">
    <!-- for bootstrap live search -->
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.css"> -->
    <!-- for ekko lightbox -->
    <link href="<?php echo site_url(); ?>assets/css/ekko-lightbox.css" rel="stylesheet">
    <style type="text/css" media="screen">
    .red{border:1px solid red;}  
    .event_none{pointer-events:none;}
    .event_auto{pointer-events:auto;}
    </style>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDP0ELg7Uelr_j2FivO-4L9KJMn7CBXTEo&libraries=places"></script>
    <!-- for flexslider -->
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/flexslider.css" type="text/css" media="screen" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="<?php echo site_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/select2.min.js"></script>
    <!--<script src="http://165.227.110.185/showspoon//assets/dist/js/bootstrap_multiselect.js"></script>-->
</head>

<body>
<p id="alert"></p>

    <header class="main-header">
        <!-- Static navbar -->
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>

            <!-- <button type="button" class="navbar-toggle collapsed filter_btn" data-toggle="collapse" data-target="#sidebar" aria-expanded="false" aria-controls="sidebar">
              <i class="fa fa-filter" aria-hidden="true"></i>
            </button> -->

            <a class="navbar-brand" href="<?php if($bid < 4){echo site_url('venueProfile');}else{echo site_url();} ?>">
                <img src="<?php echo site_url(); ?>assets/images/logo-n.png" alt="Logo">
            </a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="search">
                  <!-- search form -->
                  <form id="search" action="<?php if($bid < 4){echo site_url('venueProfile');}else{echo site_url('venue/search');} ?>" method="post">
                     <input type="search" name="search" placeholder="Search" autocomplete="off">
                  </form>
                  <!-- search form ends-->
              </li>
              <li><a href="<?php if($bid < 4){echo site_url('venueProfile');}else{echo site_url();} ?>">Artists</a></li>
              <li><a href="<?php if($bid < 4){echo site_url('venueProfile');}else{echo site_url('venueBooking/booking');} ?>">My bookings</a></li>
              <li><a href="<?php if($bid < 4){echo site_url('venueProfile');}else{echo site_url('venueBooking/inbox');} ?>">Inbox <span id="my_bookings" class="badge badge_upper inbox-count"></span></a></li>
              <li><a href="<?php if($bid < 4){echo site_url('venueProfile');}else{echo site_url('venueBooking/my_requests');} ?>">Requests <span id="my_bookings" class="badge badge_upper request-count"></span></a></li>
              <li class="favourite"><a href="<?php if($bid < 4){echo site_url('venueProfile');}else{echo site_url("venue/favourties");} ?>" class="heart" style="color:#fff;"></a></li>
              <li>
                <div class="inset dropdown">  <?php if($image!=""){$img = site_url()."uploads/users/thumb/".$image; }else{$img = site_url()."assets/blank.png";} ?>                 
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img class="profile_img" src="<?php echo $img; ?>"> <span class="caret"></span><?php if(isset($artist->name) && $artist->name!=""){echo $artist->name;}else{echo '';} ?></a>
                <ul class="dropdown-menu">
                  <!-- <li><b><?=$id;?></b> &nbsp; <?=$name; ?> </li> -->
                  <li><a href="<?php echo site_url('venueProfile'); ?>">Profile</a></li>
                  <li><a href="<?php echo site_url('account/logout'); ?>">Logout</a></li>
                </ul>
                </div>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container -->
      </nav>

    </header>

    <div class="spacer_hundred"></div>

    <!-- content area -->
    <div class="container">
        <!-- Steps Wizard --> 
        <div class="row">
  <?php if(isset($error) && $error!=''){?>
  <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
      </button>
      <strong>Warning! </strong> <?php echo $error;?>
  </div>
  <?php }?>
<?php if($bid < 4){ ?> 
  <div class="stepwizard" style="margin: 0 auto;">
    <ul class="stepwizard-row setup-panel" role="tablist">
	    <li role="presentation" class="stepwizard-step" style="width:25%;">
		    <a href="#step-1" aria-controls="step-1" role="tab" data-toggle="tab" class="btn btn-primary btn-circle <?php if($bid==0){echo 'btn-primary';}else{echo '';} ?>" type="button">1</a>
		    <p>Venue Profile</p>
	    </li>
	    <li role="presentation" class="stepwizard-step" style="width:25%;">
		    <a href="#step-2" aria-controls="step-2" role="tab" data-toggle="tab" class="<?php if($artist->one==1){echo 'event_none';}else{echo 'event_auto';} ?> btn btn-default btn-circle <?php if($bid==1){echo 'btn-primary';}else{echo '';} ?>" type="button">2</a>
		    <p>Media</p>
	    </li>
	    <li role="presentation" class="stepwizard-step" style="width:25%;">
		    <a href="#step-3" aria-controls="step-3" role="tab" data-toggle="tab" class="<?php if($artist->two==2){echo 'event_none';}else{echo 'event_auto';} ?> btn btn-default btn-circle <?php if($bid==2){echo 'btn-primary';}else{echo '';} ?>" type="button">3</a>
		    <p>Social Media</p>
	    </li>
	    <li role="presentation" class="stepwizard-step" style="width:25%;">
		    <a href="#step-4" aria-controls="step-4" role="tab" data-toggle="tab" class="<?php if($artist->three==3){echo 'event_none';}else{echo 'event_auto';} ?> btn btn-default btn-circle <?php if($bid==3){echo 'btn-primary';}else{echo '';} ?>" type="button">4</a>
		    <p>Payment Information</p>
	    </li>
    </ul>
  </div> <!-- edit_profile_tabs --> 


<?php }else{
	?>
	<h1 class="text-center marg_thrty_topper">Edit profile</h1>
	<div class="edit_profile_tabs">
    <ul class="nav nav-tabs" role="tablist">
	    <li role="presentation" class="active"><a href="#step-1" aria-controls="step-1" role="tab" data-toggle="tab">Venue Profile</a></li>
	    <li role="presentation"><a href="#step-2" aria-controls="step-2" role="tab" data-toggle="tab">Media</a></li>
	    <li role="presentation"><a href="#step-3" aria-controls="step-3" role="tab" data-toggle="tab">Social Media</a></li>
	    <li role="presentation"><a href="#step-4" aria-controls="step-4" role="tab" data-toggle="tab">Payment Information</a></li>
    </ul>
	<?php
}

?>

<div class="tab-content">
	<div role="tabpanel" class="tab-pane fade in active" id="step-1">
	<form id="venue_profile_active" method="post" action="<?php echo site_url(); ?>venueProfile" enctype="multipart/form-data">
      <div class="col-md-6 col-md-offset-3">
        <div class="col-md-12">
          <h3>Venue Profile</h3>
          <div class="form-group">
          <label>Venue Name <small>*</small></label>
          <input type="text" name="venue_name" id="artist_name"  value="<?php echo isset($artist->name)?$artist->name:'';?>" class="form-control">
          <span id="artist-name"></span>
          </div>
          <div class="form-group">
          <label>Genre <small>*</small></label>
          <div class="custom_select_box">
          <select multiple class="form-control select" name="genre[]" id="artist_genre">
          <?php
          $get_genre = isset($artist->genre)?$artist->genre:'';
          $get_genre = explode(',',$get_genre);

          if(count($genres)>0){
              foreach($genres as $key=>$row){
                  $selected='';
                  if(in_array($key,$get_genre)){
                      $selected='selected';
                  }
                  echo '<option '.$selected.' value="'.$key.'">'.$row.'</option>';
              }
          }

          ?>
          </select>
           <span id="artist-genre"></span>
          </div>
          </div>
          <div class="form-group">
          <label>Age Limit <small>*</small></label>
          <input type="text" name="age"  value="<?php echo isset($artist->age)?$artist->age:'';?>" class="form-control" id="artist_age">
          <span id="artist-age"></span>
         </div>
         <div class="form-group">
              <label>Name of Contact Person <small>*</small></label>
              <input type="text" name="contact_person"  value="<?php echo isset($artist->contact_person)?$artist->contact_person:'';?>" class="form-control" id="name_of_contact_person">
              <span id="name-of-contact-person"></span>
          </div>
          <div class="form-group">
          <label>Mobile Number <small>*</small></label>
          <input type="text" name="mobile"  value="<?php echo isset($artist->mobile)?$artist->mobile:'';?>" class="form-control" id="mobile_number" onkeyup="phone_number(this);">
          <span id="mobile-number"></span>
          </div>
          <div class="form-group">
          <label>Capacity <small>*</small></label>
          <input type="text" name="capacity"  value="<?php echo isset($artist->capacity)?$artist->capacity:'';?>" class="form-control" id="artist_capacity">
          <span id="artist-capacity"></span>
          </div>
          <!-- <div class="form-group">
          <label>Email Account (For Payment) <small>*</small></label>
          <input type="email" id="artist_email" name="payment_account"  value="<?php echo isset($artist->payment_account)?$artist->payment_account:'';?>" class="form-control">
          <span id="artist_email"></span>
          </div> -->
          <div class="form-group">
          <label>Country <small>*</small></label>
          <div class="custom_select_box">
              <select data-placeholder=""  id="country_id" class="form-control" name="country_id">
                  <option value="">Select Country</option>
                  <?php 
                  $this->load->model('Artist_model');
                 if($artist->city_id!=""){
                  $row = $this->Artist_model->getCountryId($artist->city_id);
                  $cid = $row->CountryID;
                }else{$cid = "";}
                  ?>
                  <option value="1" <?php if($cid==1){echo 'selected';}else{echo '';}?>>Norway</option>
                  <option value="2" <?php if($cid==2){echo 'selected';}else{echo '';}?>>Sweden</option>
                  <option value="3" <?php if($cid==3){echo 'selected';}else{echo '';}?>>Denmark</option>
                  <option value="4" <?php if($cid==4){echo 'selected';}else{echo '';}?>>United Kingdom</option>
              </select>
              <span id="country-id"></span>
              <input type="hidden" name="venue_currency" id="currency" value="<?php echo isset($artist->venue_currency)?$artist->venue_currency:''; ?>">
          </div>

      </div>
          <div class="form-group">
          <label>City <small>*</small></label>
          <div class="custom_select_box">
              <select data-placeholder="" class="form-control" name="city_id" id="city_id">
                  <option value="">Select City</option>

                  <?php

                  $city = isset($artist->city_id)?$artist->city_id:'';


                  if(count($cities)>0){
                      foreach($cities as $key=>$row){
                          $selected='';
                          if($city==$key){
                              $selected='selected';
                          }
                          echo '<option '.$selected.' value="'.$key.'">'.$row.'</option>';
                      }
                  }

                  ?>
              </select>
              <span id="city-id"></span>
          </div>

      </div>
          <div class="form-group">
            <label class="control-label">Rate Per Day <small>*</small></label>
            <input type="text" id="artist_rate" name="venue_rate" class="form-control" value="<?php echo isset($artist->venue_rate)?$artist->venue_rate:'';?>" placeholder="" onkeyup="phone_number(this);">
            <span id="artist-rate"></span>
          </div>
          <div class="form-group">
            <label class="control-label">Address</label>
            <input id="artist_address" type="text" size="50" name="address" class="form-control" placeholder="Enter your address" autocomplete="on" runat="server" value="<?php echo isset($artist->address)?$artist->address:'';?>" />
            <input type="hidden" name="hidden_address" id="city2" value="<?php echo $artist->address; ?>" />
            <input type="hidden" name="hidden_lat" id="cityLat" value="<?php echo $artist->latitude?>" />
            <input type="hidden" name="hidden_long" id="cityLng" value="<?php echo $artist->longitude; ?>" />
            <!-- <textarea id="artist_address" name="address" class="form-control" placeholder="Enter your address"><?php echo isset($artist->address)?$artist->address:'';?></textarea> -->
            <span id="artist-address"></span>
          </div>
          <div class="form-group">
              <label>Zip Code <small>*</small></label>
              <input type="text" name="zip" id="artist_zip" value="<?php echo isset($artist->zip)?$artist->zip:'';?>" class="form-control">
              <span id="artist-zip"></span>
          </div>
          <!-- <div class="form-group">
              <label>Website <small>*</small></label>
              <input type="text" id="artist_website" name="website"  value="<?php echo isset($artist->website)?$artist->website:'';?>" class="form-control">
              <span id="artist-website"></span>
          </div> -->
          <div class="form-group">
          <label>Venue Description <small>*</small></label>
          <textarea name="description" id="artist_desc" class="form-control"><?php echo isset($artist->description)?$artist->description:'';?></textarea>
          <span id="artist-desc"></span>
          </div>
          <div class="form-group">
          <label>Technical Rider <small>*</small></label>
          <textarea name="technical_rider" id="technical_rider" class="form-control"><?php echo isset($artist->technical_rider)?$artist->technical_rider:'';?></textarea>
          <span id="artist-rider"></span>
          </div>
          <div class="form-group">
          <div class="col-md-6" style="padding-left:0;">
          <label class="btn btn-default btn-file">
            Upload Profile Image <input type="file" name="profile_image" class="form-control image_file" accept='image/*' style="display: none;">
            </label>
          </div>
          </div>
          <div>
          <span id="image"></span>
          </div><br><br><br>
          


          <?php if($artist->profile_image!=""){ ?>
          <div  id="data-img" data-id="<?php echo $artist->venue_id; ?>" class="col-lg-4 col-md-4 col-sm-4 col-xs-4 artist_gallery artist_gallery<?php echo $artist->venue_id; ?>" style="padding-left:0;">
          <div class="panel panel-body b wrapper-xs ">
          <div class="media">
          <div class="media-center text-center">
          <div class="img-block">
          <div class="img-block-center">
          <img src="<?php echo site_url(); ?>uploads/users/thumb/<?php echo $artist->profile_image; ?>" class="img-responsive" alt="">        
          </div>
          </div>
          </div>
          <div class="gallery_overlay">
          <div class="gallery_overlay_inner">
          <div class="gallery_delete"> 
          <button type="button" class="btn btn-danger btn-sm r-2x delete-img" data-id="<?php echo $artist->venue_id; ?>">Delete</button>
          <!-- <button type="button" class="delete_gallery btn btn-danger btn-sm r-2x delete-img" data-id="<?php echo $artist->venue_id; ?>">Delete</button> -->
          </div>
          </div>
          </div>
          </div>
          </div>
          </div>
          <?php }else{echo '';} ?>


          <div class="col-md-12"></div>

          <div class="form-group">
          <div class="col-md-6" style="padding-left:0;">
          <!-- <input type="submit" id="submit" name="submit" value="Update" class="btn btn-primary pull-left"> -->
          </div>
          <?php if($bid < 4){ ?>
          <div class="col-md-6" style="padding-right:0;">
            <input type="submit" id="submit" name="submit" value="Next" class="btn btn-primary pull-right">
          <!-- <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button> -->
          </div>
          <?php }else{
          	?>
          	<div class="col-md-6" style="padding-right:0;">
            <input type="submit" id="submit" name="submit" value="Update" class="btn btn-primary pull-right">
            <!-- <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button> -->
            </div>
          	<?php 
          } ?>
          </div>
        </div>
      </div>

      </form>
	</div>
	<div role="tabpanel" class="tab-pane fade " id="step-2">
	<form method="post" action="<?php echo site_url('VenueProfile/upload_gallery'); ?>" enctype="multipart/form-data">
      <div class="col-md-6 col-md-offset-3">
        <div class="col-md-12">
          <h3>Upload Pictures</h3>
          <div class="form-group">
            <div id="members_box">
            <div class="row" id="row">
            <?php if(isset($gallery) && count($gallery)>0){

            foreach($gallery as $row){
            if(isset($row->url) && $row->url!=''){
            $img_path=base_url().'uploads/gallery/thumbs/'.$row->url;
            }else{
            $img_path=base_url().'assets/images/placeholder.jpg';
            }
            $this->load->view('includes/artist-gallery-part',['id'=>$row->id,'img_path'=>$img_path]);
            }}?>
            </div>
            </div>
            <p id="photo_ext"></p>
            <p id="photo_size"></p>
          <label class="btn btn-default btn-file">
          Add Pictures <input type="file" name="file[]" id="photo" style="display: none;" multiple>
          </label>
          
          <!-- <input type="submit" name="submit" value="Submit" class="btn btn-primary pull-right"> -->
            <!-- <button type="button" class="btn btn-primary add_pictures" >Add Pictures</button> -->
          </div>
          <hr>
          <h3>Add Videos</h3>
          <div class="form-group">
            <div id="videos_box">
            <div class="row">
            <?php if(isset($videos) && count($videos)>0){

            foreach($videos as $row){
               
                $this->load->view('includes/artist-video-part',['id'=>$row->id,'url'=>$row->url,'title'=>$row->caption,'type'=>$row->type,'video_data'=>$row->video_data]);
            }}?> <span id="ajax_video"></span>
            </div>
            </div>
            <button type="button" class="btn btn-default add_video" data-toggle="modal" data-target="#addVideo" >Add Video</button>
          </div>
          <hr>
          <h3>Add Soundcloud</h3>
          <div class="form-group">
         <div id="soundcloud_box">
         <div class="row">
        <?php if(isset($sound_cloud) && count($sound_cloud)>0){

        foreach($sound_cloud as $row){

            $this->load->view('includes/artist-soundcloud-part',['id'=>$row->id,'url'=>$row->url,'title'=>$row->caption,'type'=>$row->type,'video_data'=>$row->video_data]);
            }}?> <span id="ajax_soundcloud"></span>
          </div>
          </div>
           <button type="button" class="btn btn-default add_soundcloud" data-toggle="modal" data-target="#myModal_soundcloud" >Add Soundcloud</button>
          </div>
          <?php if($bid < 3){ ?>
          <hr>
          <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
          <button class="btn btn-primary nextBtn pull-right" type="button" id="media_bid">Next</button>
          <?php 
         }else{echo '';} ?>
        </div>
      </div>
  </form>
	</div>
	<div role="tabpanel" class="tab-pane fade " id="step-3">
	<form method="post" action="<?php echo site_url('VenueProfile/socialmedia'); ?>" id="media">
      <div class="col-md-6 col-md-offset-3">
        <div class="col-md-12">
          <h3>Social Media</h3>
          <div class="form-group">
          <label>Facebook</label>
          <input type="url" name="facebook" id="facebook" value="<?php echo isset($artist->facebook)?$artist->facebook:'';?>" class="form-control">
          <span id="facebook-error"></span>
          </div>
          <div class="form-group">
          <label>Twitter</label>
          <input type="url" name="twitter" id="twitter" value="<?php echo isset($artist->twitter)?$artist->twitter:'';?>" class="form-control">
          <span id="twitter-error"></span>
          </div>
          <div class="form-group">
          <label>Instagram</label>
          <input type="url" name="instragram" id="instragram" value="<?php echo isset($artist->instagram)?$artist->instagram:'';?>" class="form-control">
          <span id="instragram-error"></span>
          </div>
         <!--  <div class="form-group">
          <label>SoundCloud</label>
          <input type="url" name="soundcloud" id="soundcloud" value="<?php echo isset($artist->soundcloud)?$artist->soundcloud:'';?>" class="form-control">
          <span id="soundcloud-error"></span>
          </div> -->
          <?php if($bid < 4){ ?>
          <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
          <button class="btn btn-primary nextBtn pull-right" type="submit">Next</button>
          <?php }else{
          	?>
			<div class="form-group">
            <input type="submit" name="submit" value="Update" class="btn btn-primary pull-left"> <br><br><br>
            </div>
          	<?php 
          }?>
          <!-- <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button> -->
        </div>
      </div>
  </form>
	</div>
	<div role="tabpanel" class="tab-pane fade " id="step-4">
	<form method="post" action="<?php echo site_url('VenueProfile/paymentinfo'); ?>" id="payment">
      <div class="col-md-6 col-md-offset-3">
        <div class="col-md-12">
          <div class="coffee_cup">
            <img src="<?php echo site_url(); ?>assets/images/coffee.png" alt="Congratulations">
          </div>
          <h3 class="text-center">
            Payment Information
          </h3>
          <div class="form-group">
          <label>Account number</label>
          <input type="number" name="account_number" id="account_number" value="<?php echo isset($payment_info->account_number)?$payment_info->account_number:''; ?>" min="0" class="form-control">
          <span id="account-number"></span>
          </div>
          <div class="form-group">
          <label>Swift code</label>
          <input type="number" name="swift_code" id="swift_code" value="<?php echo isset($payment_info->swift_code)?$payment_info->swift_code:''; ?>" min="0" class="form-control">
          <span id="swift-code"></span>
          </div>
          <div class="form-group">
          <label>Account name</label>
          <input type="text" name="account_name" id="account_name" value="<?php echo isset($payment_info->account_name)?$payment_info->account_name:''; ?>" class="form-control">
          <span id="account-name"></span>
          </div>
          <div class="form-group">
          <label>Bank</label> 
          <select name="bank" class="form-control" id="bank">
          <option value="">Select Bank</option>
          <option value="Andebu Sparebank" <?php echo (@$payment_info->bank=='Andebu Sparebank')?'selected':''; ?>>Andebu Sparebank</option>
          <option value="Arendal og Omegns Sparekasse" <?php echo (@$payment_info->bank=='Arendal og Omegns Sparekasse')?'selected':''; ?>>Arendal og Omegns Sparekasse</option>
          <option value="Askim & Spydeberg Sparebank" <?php echo (@$payment_info->bank=='Askim & Spydeberg Sparebank')?'selected':''; ?>>Askim & Spydeberg Sparebank</option>
          <option value="Aurland Sparebank" <?php echo (@$payment_info->bank=='Aurland Sparebank')?'selected':''; ?>>Aurland Sparebank</option>
          <option value="Aurskog Sparebank" <?php echo (@$payment_info->bank=='Aurskog Sparebank')?'selected':''; ?>>Aurskog Sparebank</option>
          <option value="Berg Sparebank" <?php echo (@$payment_info->bank=='Berg Sparebank')?'selected':''; ?>>Berg Sparebank</option>
          <option value="Bien Sparebank" <?php echo (@$payment_info->bank=='Bien Sparebank')?'selected':''; ?>>Bien Sparebank</option>
          <option value="Birkenes Sparebank" <?php echo (@$payment_info->bank=='Birkenes Sparebank')?'selected':''; ?>>Birkenes Sparebank</option>
          <option value="Bjugn Sparebank" <?php echo (@$payment_info->bank=='Bjugn Sparebank')?'selected':''; ?>>Bjugn Sparebank</option>
          <option value="Blanker Sparebank" <?php echo (@$payment_info->bank=='Blanker Sparebank')?'selected':''; ?>>Blanker Sparebank</option>
          <option value="Bud, Fræna og Hustad Sparebank" <?php echo (@$payment_info->bank=='Bud, Fræna og Hustad Sparebank')?'selected':''; ?>>Bud, Fræna og Hustad Sparebank</option>
          <option value="Cultura Sparebank" <?php echo (@$payment_info->bank=='Cultura Sparebank')?'selected':''; ?>>Cultura Sparebank</option>
          <option value="DNB" <?php echo (@$payment_info->bank=='DNB')?'selected':''; ?>>DNB</option>
          <option value="Drangedal Sparebank" <?php echo (@$payment_info->bank=='Drangedal Sparebank')?'selected':''; ?>>Drangedal Sparebank</option>
          <option value="Eidsberg Sparebank" <?php echo (@$payment_info->bank=='Eidsberg Sparebank')?'selected':''; ?>>Eidsberg Sparebank</option>
          <option value="Etne Sparebank" <?php echo (@$payment_info->bank=='Etne Sparebank')?'selected':''; ?>>Etne Sparebank</option>
          <option value="Etnedal Sparebank" <?php echo (@$payment_info->bank=='Etnedal Sparebank')?'selected':''; ?>>Etnedal Sparebank</option>
          <option value="Evje og Hornnes Sparebank" <?php echo (@$payment_info->bank=='Evje og Hornnes Sparebank')?'selected':''; ?>>Evje og Hornnes Sparebank</option>
          <option value="Fana Sparebank" <?php echo (@$payment_info->bank=='Fana Sparebank')?'selected':''; ?>>Fana Sparebank</option>
          <option value="Flekkefjord Sparebank" <?php echo (@$payment_info->bank=='Flekkefjord Sparebank')?'selected':''; ?>>Flekkefjord Sparebank</option>
          <option value="Fornebu Sparebank" <?php echo (@$payment_info->bank=='Fornebu Sparebank')?'selected':''; ?>>Fornebu Sparebank</option>
          <option value="Gildeskål Sparebank" <?php echo (@$payment_info->bank=='Gildeskål Sparebank')?'selected':''; ?>>Gildeskål Sparebank</option>
          <option value="Gjerstad Sparebank" <?php echo (@$payment_info->bank=='Gjerstad Sparebank')?'selected':''; ?>>Gjerstad Sparebank</option>
          <option value="Grong Sparebank" <?php echo (@$payment_info->bank=='Grong Sparebank')?'selected':''; ?>>Grong Sparebank</option>
          <option value="Grue Sparebank" <?php echo (@$payment_info->bank=='Andebu Sparebank')?'selected':''; ?>>Grue Sparebank</option>
          <option value="Haltdalen Sparebank" <?php echo (@$payment_info->bank=='Haltdalen Sparebank')?'selected':''; ?>>Haltdalen Sparebank</option>
          <option value="Harstad Sparebank" <?php echo (@$payment_info->bank=='Harstad Sparebank')?'selected':''; ?>>Harstad Sparebank</option>
          <option value="Haugesund Sparebank" <?php echo (@$payment_info->bank=='Haugesund Sparebank')?'selected':''; ?>>Haugesund Sparebank</option>
          <option value="Hegra Sparebank" <?php echo (@$payment_info->bank=='Hegra Sparebank')?'selected':''; ?>>Hegra Sparebank</option>
          <option value="Helgeland Sparebank" <?php echo (@$payment_info->bank=='Helgeland Sparebank')?'selected':''; ?>>Helgeland Sparebank</option>
          <option value="Hemne Sparebank" <?php echo (@$payment_info->bank=='Hemne Sparebank')?'selected':''; ?>>Hemne Sparebank</option>
          <option value="Hjartdal og Gransherad Sparebank" <?php echo (@$payment_info->bank=='Hjartdal og Gransherad Sparebank')?'selected':''; ?>>Hjartdal og Gransherad Sparebank</option>
          <option value="Hjelmeland Sparebank" <?php echo (@$payment_info->bank=='Hjelmeland Sparebank')?'selected':''; ?>>Hjelmeland Sparebank</option>
          <option value="Høland og Setskog Sparebank" <?php echo (@$payment_info->bank=='Høland og Setskog Sparebank')?'selected':''; ?>>Høland og Setskog Sparebank</option>
          <option value="Hønefoss Sparebank" <?php echo (@$payment_info->bank=='Hønefoss Sparebank')?'selected':''; ?>>Hønefoss Sparebank</option>
          <option value="Indre Sogn Sparebank" <?php echo (@$payment_info->bank=='Indre Sogn Sparebank')?'selected':''; ?>>Indre Sogn Sparebank</option>
          <option value="Jernbanepersonalets Sparebank" <?php echo (@$payment_info->bank=='Jernbanepersonalets Sparebank')?'selected':''; ?>>Jernbanepersonalets Sparebank</option>
          <option value="Jæren Sparebank" <?php echo (@$payment_info->bank=='Jæren Sparebank')?'selected':''; ?>>Jæren Sparebank</option>
          <option value="Klæbu Sparebank" <?php echo (@$payment_info->bank=='Klæbu Sparebank')?'selected':''; ?>>Klæbu Sparebank</option>
          <option value="Kvinesdal Sparebank" <?php echo (@$payment_info->bank=='Kvinesdal Sparebank')?'selected':''; ?>>Kvinesdal Sparebank</option>
          <option value="Larvikbanken" <?php echo (@$payment_info->bank=='Larvikbanken')?'selected':''; ?>>Larvikbanken</option>
          <option value="Lillesands Sparebank" <?php echo (@$payment_info->bank=='Lillesands Sparebank')?'selected':''; ?>>Lillesands Sparebank</option>
          <option value="LillestrømBanken" <?php echo (@$payment_info->bank=='LillestrømBanken')?'selected':''; ?>>LillestrømBanken</option>
          <option value="Lofoten Sparebank" <?php echo (@$payment_info->bank=='Lofoten Sparebank')?'selected':''; ?>>Lofoten Sparebank</option>
          <option value="Luster Sparebank" <?php echo (@$payment_info->bank=='Luster Sparebank')?'selected':''; ?>>Luster Sparebank</option>
          <option value="Hemne Sparebank" <?php echo (@$payment_info->bank=='Hemne Sparebank')?'selected':''; ?>>Hemne Sparebank</option>
          <option value="Marker Sparebank" <?php echo (@$payment_info->bank=='Marker Sparebank')?'selected':''; ?>>Marker Sparebank</option>
          <option value="Meldal Sparebank" <?php echo (@$payment_info->bank=='Meldal Sparebank')?'selected':''; ?>>Meldal Sparebank</option>
          <option value="MelhusBanken" <?php echo (@$payment_info->bank=='MelhusBanken')?'selected':''; ?>>MelhusBanken</option>
          <option value="Nesset Sparebank" <?php echo (@$payment_info->bank=='Nesset Sparebank')?'selected':''; ?>>Nesset Sparebank</option>
          <option value="Odal Sparebank" <?php echo (@$payment_info->bank=='Odal Sparebank')?'selected':''; ?>>Odal Sparebank</option>
          <option value="Ofoten Sparebank" <?php echo (@$payment_info->bank=='Ofoten Sparebank')?'selected':''; ?>>Ofoten Sparebank</option>
          <option value="Oppdalsbanken" <?php echo (@$payment_info->bank=='Oppdalsbanken')?'selected':''; ?>>Oppdalsbanken</option>
          <option value="Orkdal Sparebank" <?php echo (@$payment_info->bank=='Orkdal Sparebank')?'selected':''; ?>>Orkdal Sparebank</option>
          <option value="Rindal Sparebank" <?php echo (@$payment_info->bank=='Rindal Sparebank')?'selected':''; ?>>Rindal Sparebank</option>
          <option value="RørosBanken" <?php echo (@$payment_info->bank=='RørosBanken')?'selected':''; ?>>RørosBanken</option>
          <option value="Sandnes Sparebank" <?php echo (@$payment_info->bank=='Sandnes Sparebank')?'selected':''; ?>>Sandnes Sparebank</option>
          <option value="Selbu Sparebank" <?php echo (@$payment_info->bank=='Andebu Sparebank')?'selected':''; ?>>Selbu Sparebank</option>
          <option value="Skagerak Sparebank" <?php echo (@$payment_info->bank=='Skagerak Sparebank')?'selected':''; ?>>Skagerak Sparebank</option>
          <option value="Skudenes & Aakra Sparebank" <?php echo (@$payment_info->bank=='Skudenes & Aakra Sparebank')?'selected':''; ?>>Skudenes & Aakra Sparebank</option>
          <option value="Skue Sparebank" <?php echo (@$payment_info->bank=='Skue Sparebank')?'selected':''; ?>>Skue Sparebank</option>
          <option value="Soknedal Sparebank" <?php echo (@$payment_info->bank=='Soknedal Sparebank')?'selected':''; ?>>Soknedal Sparebank</option>
          <option value="SpareBank 1 Gudbrandsdal" <?php echo (@$payment_info->bank=='SpareBank 1 Gudbrandsdal')?'selected':''; ?>>SpareBank 1 Gudbrandsdal</option>
          <option value="SpareBank 1 Hallingdal Valdres" <?php echo (@$payment_info->bank=='SpareBank 1 Hallingdal Valdres')?'selected':''; ?>>SpareBank 1 Hallingdal Valdres</option>
          <option value="SpareBank 1 Lom og Skjåk" <?php echo (@$payment_info->bank=='SpareBank 1 Lom og Skjåk')?'selected':''; ?>>SpareBank 1 Lom og Skjåk</option>
          <option value="SpareBank 1 Modum" <?php echo (@$payment_info->bank=='SpareBank 1 Modum')?'selected':''; ?>>SpareBank 1 Modum</option>
          <option value="SpareBank 1 Nord-Norge" <?php echo (@$payment_info->bank=='SpareBank 1 Nord-Norge')?'selected':''; ?>>SpareBank 1 Nord-Norge</option>
          <option value="SpareBank 1 Nordvest" <?php echo (@$payment_info->bank=='SpareBank 1 Nordvest')?'selected':''; ?>>SpareBank 1 Nordvest</option>
          <option value="SpareBank 1 Ringerike Hadeland" <?php echo (@$payment_info->bank=='SpareBank 1 Ringerike Hadeland')?'selected':''; ?>>SpareBank 1 Ringerike Hadeland</option>
          <option value="SpareBank 1 SMN" <?php echo (@$payment_info->bank=='SpareBank 1 SMN')?'selected':''; ?>>SpareBank 1 SMN</option>
          <option value="SpareBank 1 Søre Sunnmøre" <?php echo (@$payment_info->bank=='SpareBank 1 Søre Sunnmøre')?'selected':''; ?>>SpareBank 1 Søre Sunnmøre</option>
          <option value="SpareBank 1 Telemark" <?php echo (@$payment_info->bank=='SpareBank 1 Telemark')?'selected':''; ?>>SpareBank 1 Telemark</option>
          <option value="SpareBank 1 Østfold Akershus" <?php echo (@$payment_info->bank=='SpareBank 1 Østfold Akershus')?'selected':''; ?>>SpareBank 1 Østfold Akershus</option>
          <option value="SpareBank 1 Østlandet" <?php echo ($payment_info->bank=='SpareBank 1 Østlandet')?'selected':''; ?> >SpareBank 1 Østlandet</option>
          <option value="Sparebanken DIN" <?php echo (@$payment_info->bank=='Sparebanken DIN')?'selected':''; ?>>Sparebanken DIN</option>
          <option value="Sparebanken Møre" <?php echo (@$payment_info->bank=='Sparebanken Møre')?'selected':''; ?>>Sparebanken Møre</option>
          <option value="Sparebanken Narvik" <?php echo (@$payment_info->bank=='Sparebanken Narvik')?'selected':''; ?>>Sparebanken Narvik</option>
          <option value="Sparebanken Narvik" <?php echo (@$payment_info->bank=='Sparebanken Narvik')?'selected':''; ?>>Sparebanken Sogn og Fjordane</option>
          <option value="Sparebanken Sør" <?php echo (@$payment_info->bank=='Sparebanken Sør')?'selected':''; ?>>Sparebanken Sør</option>
          <option value="Sparebanken Vest" <?php echo (@$payment_info->bank=='Sparebanken Vest')?'selected':''; ?>>Sparebanken Vest</option>
          <option value="Sparebanken Øst" <?php echo (@$payment_info->bank=='Sparebanken Øst')?'selected':''; ?>>Sparebanken Øst</option>
          <option value="Spareskillingbanken" <?php echo (@$payment_info->bank=='Spareskillingbanken')?'selected':''; ?>>Spareskillingbanken</option>
          <option value="Stadsbygd Sparebank" <?php echo (@$payment_info->bank=='Stadsbygd Sparebank')?'selected':''; ?>>Stadsbygd Sparebank</option>
          <option value="Strømmen Sparebank" <?php echo (@$payment_info->bank=='Strømmen Sparebank')?'selected':''; ?>>Strømmen Sparebank</option>
          <option value="Sunndal Sparebank" <?php echo (@$payment_info->bank=='Sunndal Sparebank')?'selected':''; ?>>Sunndal Sparebank</option>
          <option value="Surnadal Sparebank" <?php echo (@$payment_info->bank=='Surnadal Sparebank')?'selected':''; ?>>Surnadal Sparebank</option>
          <option value="Søgne og Gripstad Sparebank" <?php echo (@$payment_info->bank=='Søgne og Gripstad Sparebank')?'selected':''; ?>>Søgne og Gripstad Sparebank</option>
          <option value="Tinn Sparebank" <?php echo (@$payment_info->bank=='Tinn Sparebank')?'selected':''; ?>>Tinn Sparebank</option>
          <option value="Tolga-Os Sparebank" <?php echo (@$payment_info->bank=='Tolga-Os Sparebank')?'selected':''; ?>>Tolga-Os Sparebank</option>
          <option value="Totens Sparebank" <?php echo (@$payment_info->bank=='Totens Sparebank')?'selected':''; ?>>Totens Sparebank</option>
          <option value="Trøgstad Sparebank" <?php echo (@$payment_info->bank=='Trøgstad Sparebank')?'selected':''; ?>>Trøgstad Sparebank</option>
          <option value="Tysnes Sparebank" <?php echo (@$payment_info->bank=='Tysnes Sparebank')?'selected':''; ?>>Tysnes Sparebank</option>
          <option value="Valle Sparebank" <?php echo (@$payment_info->bank=='Valle Sparebank')?'selected':''; ?>>Valle Sparebank</option>
          <option value="Vegarshei Sparebank" <?php echo (@$payment_info->bank=='Vegarshei Sparebank')?'selected':''; ?>>Vegarshei Sparebank</option>
          <option value="Vestre Slidre Sparebank" <?php echo (@$payment_info->bank=='Vestre Slidre Sparebank')?'selected':''; ?>>Vestre Slidre Sparebank</option>
          <option value="Vik Sparebank" <?php echo (@$payment_info->bank=='Vik Sparebank')?'selected':''; ?>>Vik Sparebank</option>
          <option value="Voss Sparebank" <?php echo (@$payment_info->bank=='Voss Sparebank')?'selected':''; ?>>Voss Sparebank</option>
          <option value="Ørland Sparebank" <?php echo (@$payment_info->bank=='Ørland Sparebank')?'selected':''; ?>>Ørland Sparebank</option>
          <option value="Ørskog Sparebank" <?php echo (@$payment_info->bank=='Ørskog Sparebank')?'selected':''; ?>>Ørskog Sparebank</option>
          <option value="Åfjord Sparebank" <?php echo (@$payment_info->bank=='Åfjord Sparebank')?'selected':''; ?>>Åfjord Sparebank</option>
          <option value="Aasen Sparebank" <?php echo (@$payment_info->bank=='Aasen Sparebank')?'selected':''; ?>>Aasen Sparebank</option>
          </select>
          <span id="bankk"></span>
          </div>
          <div class="text-center spacer_top">
          	<?php if($bid < 4){ ?>
            <button class="btn btn-success btn-lg" type="submit">Get Started Now!</button>
            <?php }else{
            	?><button class="btn btn-primary btn-lg" type="submit">Update</button><?php
            } ?>
          </div>
        </div>
      </div>
  </form>
	</div>	
</div>
</div>
</div>
        <!-- Steps Wizard ends-->
        
    </div>
    <!-- content area ends -->


    <!-- extra space -->
    <div class="clearfix" style="height: 100px;"></div>
    <!-- extra space ends-->


    <script src="<?php echo site_url(); ?>assets/js/pnotify.min.js"></script>

    <footer class="main-footer" style="position:fixed;bottom:0;left:0;right:0;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-left">
                    <p class="copyright">© <?php echo date('Y'); ?> Showspoon. All rights reserved.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                    <ul class="legallinks text-right">
                        <a href="<?php echo site_url('venue/privacy_policy'); ?>">Privacy Policy</a><span class="sep "> |</span>
                        <a href="<?php echo site_url('venue/terms'); ?>">Terms of Use </a><span class="sep "> |</span>
                        <a href="<?php echo site_url('venue/contact'); ?>">Contact Us</a>
                    </ul>                
                  </div>
            </div>
            <!-- row -->
        </div>
    </footer>
<!--Add Members-->
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Add Member</h4>
</div>
<div class="modal-body">
<form id="profile" method="post" action="<?php echo site_url('artistProfile/add_member'); ?>" enctype="multipart/form-data">
<div class="form-group">
<label>Name</label>
<input type="text" required id="name" name="name"  value="" class="form-control">
<span class="msg1"></span>
</div>


<div class="form-group">
<label>Alias</label>
<input type="text" required id="alias" name="alias"  value="" class="form-control">
<span class="msg2"></span>
</div>


<div class="form-group">
<label>Role</label>
<input type="text" required id="role" name="role"  value="" class="form-control">
<span class="msg3"></span>
</div>

<div class="form-group">
<div id="member_image" class="thumbnail col-md-6" style="display:none;">
<img src="<?php echo site_url('assets/images/logo-n.png');?>" alt="" class="">

</div>
<div class="clearfix"></div>
<input type="file" name="file" id="img_browse">
<span id="show_img"></span>
</div>
<div class="clearfix"></div>
<div class="form-group">
<input type="submit" id="member" name="submit" value="Submit" class="btn btn-primary pull-right">
</div>
</form>
</div>

</div>

</div>
</div>
<!--End Add Members-->


<!--Edit Member-->
<div id="myModal_edit" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit Member</h4>
</div>
<div class="modal-body">
<form id="profile_edit" method="post" action="<?php echo site_url('ArtistProfile/edit_member_form'); ?>" enctype="multipart/form-data">
<input type="hidden" name="id" id="hidden" value="">
<div class="form-group">
<label>Name</label>
<input type="text" required id="name_edit" name="name"  value="" class="form-control">
</div>
<div class="form-group">
<label>Alias</label>
<input type="text" required id="alias_edit" name="alias"  value="" class="form-control">
</div>
<div class="form-group">
<label>Role</label>
<input type="text" required id="role_edit" name="role"  value="" class="form-control">
</div>
<div class="form-group">
<div id="member_image_edit" class="thumbnail col-md-6" style="display:none;">
<img src="<?php echo site_url('assets/dist/images/logo.png');?>" alt="" class="">
</div>
<div class="clearfix"></div>
<input type="file" name="file" id="img_browse_edit">
<span id="edit_image"></span>
</div>
<div class="clearfix"></div>
<div class="form-group">
<input type="submit" id="submit_edit" name="submit" value="Update" class="btn btn-primary pull-right">
</div>
</form>
</div>

</div>

</div>
</div>
<!--End Edit Members-->
<!--Add Videos-->
<div id="addVideo" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Add Video</h4>
</div>
<div class="modal-body">
<form id="add_video_form" method="post" action="<?php //echo site_url('VenueProfile/add_video'); ?>" >
<div class="form-group">
<label>Type</label>
<select required id="video_type_id" name="type" class="form-control select2">
<option value="YouTube">Youtube</option>
<option value="Vimeo">Vimeo</option>
</select>
</div>
<div class="form-group">
<label>Title</label>
<input type="text" id="caption" name="caption"  value="" class="form-control">
<span id="video_caption"></span>
</div>
<div class="form-group">
<label>Link</label>
<input type="text" id="url" name="url"  value="" class="form-control youtube">
<span id="video_error"></span>
<span id="video_url"></span>
</div>
<div class="clearfix"></div>
<div class="form-group">
<input type="submit" id="add_video" name="submit" value="Submit" class="btn btn-primary pull-right">
</div>
</form>
</div>

</div>

</div>
</div>
<!--End Add Video-->
<!--Add Soundcloud-->
<div id="myModal_soundcloud" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Add Soundcloud</h4>
</div>
<div class="modal-body">
<form id="form_soundcloud" method="post" action="<?php echo site_url('VenueProfile/add_video'); ?>">
<div class="form-group">
<label>Type</label>
<select id="audio_type_id" name="type" class="form-control select2">
<option value="soundcloud">SoundCloud</option>
<option value="spotify">Spotify</option>
</select>
</div>
<div class="form-group">
<label>Title</label>
<input type="text" id="soundcloud_caption" name="caption"  value="" class="form-control">
<span id="soundcloud-caption"></span>
</div>
<div class="form-group">
<label>Link</label>
<input type="url" id="soundcloud_url" name="url"  value="" class="form-control soundcloud">
<span id="soundcloud_error"></span>
<span id="soundcloud-url"></span>
</div>
<div class="clearfix"></div>
<div class="form-group">
<input type="submit" id="submit_soundcloud" name="submit" value="Submit" class="btn btn-primary pull-right">
</div>
</form>
</div>

</div>

</div>
</div>
<!--End Soundcloud-->
    <!-- for bootstrap select -->
    <script src="<?php echo site_url(); ?>assets/js/bootstrap-select.js"></script>
    <!-- for smooth scroll -->
    <script type="text/javascript">
        // Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
    </script>

    

   
<!-- for header shrink -->
    <script type="text/javascript">
    $('#mobile_number').bind('keyup , blur',function(e) {  
    var a = $('#mobile_number').val();
    if(a!=""){
        var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
        if (filter.test(a)) {
            $('#mobile-number').html('');
            $('#mobile_number').removeClass('red');
        }else{
            $('#mobile-number').html('Enter a valid phone number').css('color','red'); 
            $('#mobile_number').addClass('red');   
        }
    }
    });
     $("#artist_rate").bind('keyup , blur',function() {
    var price = $("#artist_rate").val();
    var validatePrice = function(price) {
      return /^(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(price);
    }
    if(validatePrice(price)==false){
      $("#artist-rate").html("Enter a valid price").css('color','red');
      $("#artist_rate").addClass('red');
    }else{
      $("#artist-rate").html("");
      $("#artist_rate").removeClass('red');
    }
   });
        $(document).on("scroll", function(){
        if
          ($(document).scrollTop() > 20){
              $(".main-header").addClass("shrink");
            }
            else
            {
                $(".main-header").removeClass("shrink");
            }
        });
    </script>


<!-- for activation wizard steps 

  https://www.bootply.com/m8WD6ufvR6
-->
<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
      url:'<?php echo site_url("venue/getHeartCount"); ?>',
      type:'POST',
      success:function(data){
        setTimeout(function(){
        $('li.favourite a.heart').text(data);
        },500);
      }
    });
    $('#country_id').change(function(){
    var country = $(this).val(); 
    if(country==1){
      $('#currency').val('NOK');
      $('#artist_rate').attr('placeholder','NOK');
    }
    else if(country==2){
      $('#currency').val('SEK');
      $('#artist_rate').attr('placeholder','SEK');
    }
    else if(country==3){
      $('#currency').val('DKK');
      $('#artist_rate').attr('placeholder','DKK');
    }
    else if(country==4){
      $('#currency').val('GBP');
      $('#artist_rate').attr('placeholder','GBP');
    }
    else{
      $('#currency').val('');
      $('#artist_rate').attr('placeholder','');
    }
    $.ajax({
      url : '<?php echo site_url("venueProfile/city_list_data"); ?>',
      type : 'POST',
      data : {country:country},
      success : function(data){
        $('#city_id').html(data);
      }
    });
  });
    function getVenueRequestCount(){
          $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistRequestCount"); ?>',
                        type    :  'POST',
                        dataType:  'JSON',
                        success :  function(data){ 

                          if(data.status=='request' && data.cnt > 0 && data.is_read==0){
                            $('.request-count').html(data.cnt);
                          }
                        }
                   });
       }
       getVenueRequestCount(); 
        // var clear = setInterval(function(){
        // getVenueRequestCount(); 
        // },100);
      function getArtistInboxCount(){
        $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistInboxCount"); ?>',
                        type    :  'POST',
                        dataType:  'JSON',
                        success :  function(data){ 

                          if(data.status=='message' && data.cnt > 0 && data.is_read==0){
                            $('.inbox-count').html(data.cnt);
                          }
                        }
                   });
      }
      getArtistInboxCount();
      function getVenueNotification(){
         $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistNotification"); ?>',
                        type    :  'POST',
                        success :  function(data){

                            $('#alert').html(data);
                        }
                   });
      }
      getVenueNotification();
      $(document).on('click','.close',function(){
        var id = $(this).data('id');
        var mr = $(this).data('mr');
        $.ajax({
                        url     :  '<?php echo site_url("venueBooking/deleteArtistNotification"); ?>',
                        type    :  'POST',
                        data    :  {id:id},
                        success :  function(data){
                          if(mr=='request'){
                            $('.request-count').text('');
                          }else if(mr=='message'){
                            $('.inbox-count').text('');
                          }
                        }
                   });
      });
    $('#media_bid').click(function(){
    var id = '<?php echo $id; ?>';  
    $.ajax({
      url  : '<?php echo site_url("venueProfile/update_media_bid"); ?>',
      type : 'POST',
      data : {id:id}, 
      success : function(data){

      }
    });
    });
  });
  $(document).ready(function () {


     $('#venue_profile_active').submit(function(event){
      
      if($('#artist_name').val()==""){
          $('#artist_name').addClass('red');
          $('#artist-name').text('This field is required').css('color','red');
         }
         if($('#artist_genre').val()==""){
          $('#artist_genre').addClass('red');
          $('#artist-genre').text('This field is required').css('color','red');
         }
         if($('#mobile_number').val()==""){
          $('#mobile_number').addClass('red');
          $('#mobile-number').text('This field is required').css('color','red');
         }
         if($('#name_of_contact_person').val()==""){
          $('#name_of_contact_person').addClass('red');
          $('#name-of-contact-person').text('This field is required').css('color','red');
         }
         // if($('#artist_email').val()==""){
         //  $('#artist_email').addClass('red');
         //  $('#artist-email').text('This field is required').css('color','red');
         // }
         if($('#country_id').val()==""){
          $('#country_id').addClass('red');
          $('#country-id').text('This field is required').css('color','red');
         }
         if($('#city_id').val()==""){
          $('#city_id').addClass('red');
          $('#city-id').text('This field is required').css('color','red');
         }
         if($('#artist_address').val()==""){
          $('#artist_address').addClass('red');
          $('#artist-address').text('This field is required').css('color','red');
         }
         if($('#artist_zip').val()==""){
          $('#artist_zip').addClass('red');
          $('#artist-zip').text('This field is required').css('color','red');
         }
         // if($('#artist_website').val()==""){
         //  $('#artist_website').addClass('red');
         //  $('#artist-website').text('This field is required').css('color','red');
         // }
         if($('#artist_desc').val()==""){
          $('#artist_desc').addClass('red');
          $('#artist-desc').text('This field is required').css('color','red');
         }
         if($('#artist_capacity').val()==""){
          $('#artist_capacity').addClass('red');
          $('#artist-capacity').text('This field is required').css('color','red');
         }
         if($('#artist_age').val()==""){
          $('#artist_age').addClass('red');
          $('#artist-age').text('This field is required').css('color','red');
         }
         if($('#artist_rate').val()==""){
          $('#artist_rate').addClass('red');
          $('#artist-rate').text('This field is required').css('color','red');
         }
         if($('#technical_rider').val()==""){
          $('#technical_rider').addClass('red');
          $('#artist-rider').text('This field is required').css('color','red');
         }
         
         if($('#artist_name').val()!=""&&$('#artist_genre').val()!=""&&$('#mobile_number').val()!=""&&$('#name_of_contact_person').val()!=""&&$('#city_id').val()!=""&&$('#artist_address').val()!=""&&$('#artist_zip').val()!=""&&$('#artist_desc').val()!=""&&$('#artist_capacity').val()!=""&&$('#artist_age').val()!=""&&$('#artist_rate').val()!=""&&$('#technical_rider').val()!=""){
          $('#venue_profile_active').submit();
         }
      event.preventDefault();
   });

    $('#artist_name').bind('keyup , change' , function(){
          if($('#artist_name').val()!=""){
          $('#artist_name').removeClass('red');
          $('#artist-name').text('');
        }
     });
    $('#technical_rider').bind('keyup , change' , function(){
          if($('#technical_rider').val()!=""){
          $('#technical_rider').removeClass('red');
          $('#artist-ride').text('');
        }
     });
    $('#artist_age').bind('keyup , change' , function(){
          if($('#artist_age').val()!=""){
          $('#artist_age').removeClass('red');
          $('#artist-age').text('');
        }
     });
    $('#artist_genre').bind('keyup , change' , function(){
          if($('#artist_genre').val()!=""){
          $('#artist_genre').removeClass('red');
          $('#artist-genre').text('');
        }
     });
    $('#mobile_number').bind('keyup , change' , function(){
          if($('#mobile_number').val()!=""){
          $('#mobile_number').removeClass('red');
          $('#mobile-number').text('');
        }
     });
    $('#name_of_contact_person').bind('keyup , change' , function(){
          if($('#name_of_contact_person').val()!=""){
          $('#name_of_contact_person').removeClass('red');
          $('#name-of-contact-person').text('');
        }
     });
    $('#artist_email').bind('keyup , change' , function(){
          if($('#artist_email').val()!=""){
          $('#artist_email').removeClass('red');
          $('#artist-email').text('');
        }
     });
    $('#country_id').bind('keyup , change' , function(){
          if($('#country_id').val()!=""){
          $('#country_id').removeClass('red');
          $('#country-id').text('');
        }
     });
    $('#city_id').bind('keyup , change' , function(){
          if($('#city_id').val()!=""){
          $('#city_id').removeClass('red');
          $('#city-id').text('');
        }
     });
    $('#artist_address').bind('keyup , change' , function(){
          if($('#artist_address').val()!=""){
          $('#artist_address').removeClass('red');
          $('#artist-address').text('');
        }
     });
    $('#artist_zip').bind('keyup , change' , function(){
          if($('#artist_zip').val()!=""){
          $('#artist_zip').removeClass('red');
          $('#artist-zip').text('');
        }
     });
    $('#artist_website').bind('keyup , change' , function(){
          if($('#artist_website').val()!=""){
          $('#artist_website').removeClass('red');
          $('#artist-website').text('');
        }
     });
    $('#artist_desc').bind('keyup , change' , function(){
          if($('#artist_desc').val()!=""){
          $('#artist_desc').removeClass('red');
          $('#artist-desc').text('');
        }
     });
    $('#artist_capacity').bind('keyup , change' , function(){
          if($('#artist_capacity').val()!=""){
          $('#artist_capacity').removeClass('red');
          $('#artist-capacity').text('');
        }
     });
    $('#artist_rate').bind('keyup , change' , function(){
          if($('#artist_rate').val()!=""){
          $('#artist_rate').removeClass('red');
          $('#artist-rate').text('');
        }
     });


     $('#payment').submit(function(event){
         
         if($('#account_number').val()==""){
          $('#account_number').addClass('red');
          $('#account-number').text('This field is required').css('color','red');
         }
         if($('#bank_name').val()==""){
          $('#bank_name').addClass('red');
          $('#bank-name').text('This field is required').css('color','red');
         }
         if($('#swift_code').val()==""){
          $('#swift_code').addClass('red');
          $('#swift-code').text('This field is required').css('color','red');
         }
         if($('#account_name').val()==""){
          $('#account_name').addClass('red');
          $('#account-name').text('This field is required').css('color','red');
         }
         if($('#bank').val()==""){
          $('#bank').addClass('red');
          $('#bankk').text('This field is required').css('color','red');
         }
         if($('#account_number').val()!=""&&('#bank_name').val()!=""&&('#swift_code').val()!=""&&('#account_name').val()!=""&&('#bank').val()!=""){
          $('#payment').submit();
         }
         event.preventDefault();
     });

     $('#account_number').bind('keyup , change' , function(){
          if($('#account_number').val()!=""){
          $('#account_number').removeClass('red');
          $('#account-number').text('');
        }
     });
     $('#bank_name').bind('keyup , change' , function(){
          if($('#bank_name').val()!=""){
          $('#bank_name').removeClass('red');
          $('#bank-name').text('');
        }
     });
     $('#swift_code').bind('keyup , change' , function(){
          if($('#swift_code').val()!=""){
          $('#swift_code').removeClass('red');
          $('#swift-code').text('');
        }
     });
     $('#account_name').bind('keyup , change' , function(){
          if($('#account_name').val()!=""){
          $('#account_name').removeClass('red');
          $('#account-name').text('');
        }
     });
     $('#bank').bind('keyup , change' , function(){
          if($('#bank').val()!=""){
          $('#bank').removeClass('red');
          $('#bankk').text('');
        }
     });

     $('#media').submit(function(event){
         
         if($('#facebook').val()==""){
          $('#facebook').addClass('red');
          $('#facebook-error').text('This field is required').css('color','red');
         }
         if($('#twitter').val()==""){
          $('#twitter').addClass('red');
          $('#twitter-error').text('This field is required').css('color','red');
         }
         if($('#instragram').val()==""){
          $('#instragram').addClass('red');
          $('#instragram-error').text('This field is required').css('color','red');
         }
         // if($('#soundcloud').val()==""){
         //  $('#soundcloud').addClass('red');
         //  $('#soundcloud-error').text('This field is required').css('color','red');
         // }
         if($('#facebook').val()!=""&&$('#twitter').val()!=""&&$('#instragram').val()!=""){
          $.ajax({
            url : '<?php echo site_url("venueProfile/socialmedia"); ?>',
            type : 'POST',
            data : $('#media').serialize(),
            success : function(data){
              //$('#media')[0].reset();
            }
          });
         }
         event.preventDefault();
     });

     $('#facebook').bind('keyup , change' , function(){
          if($('#facebook').val()!=""){
          $('#facebook').removeClass('red');
          $('#facebook-error').text('');
        }
     });
     $('#twitter').bind('keyup , change' , function(){
          if($('#twitter').val()!=""){
          $('#twitter').removeClass('red');
          $('#twitter-error').text('');
        }
     });
     $('#instragram').bind('keyup , change' , function(){
          if($('#instragram').val()!=""){
          $('#instragram').removeClass('red');
          $('#instragram-error').text('');
        }
     });
     $('#soundcloud').bind('keyup , change' , function(){
          if($('#soundcloud').val()!=""){
          $('#soundcloud').removeClass('red');
          $('#soundcloud-error').text('');
        }
     });


     $('#add_video_form').submit(function(event){
        var caption = $('#caption').val();
        var url = $('#url').val();
        if(caption==""){
          $('#caption').addClass('red');
          $('#video_caption').text('This field is required').css('color','red');

        }
        if(url==""){
          $('#url').addClass('red');
          $('#video_url').text('This field is required').css('color','red');

        }
        if(caption!="" && url!=""){
          $.ajax({
            url : '<?php echo site_url("venueProfile/add_video"); ?>',
            type : 'POST',
            dataType : 'JSON',
            data : $('#add_video_form').serialize(),
            success : function(data){

              $('#ajax_video').html(data.data);
              $('#addVideo').modal('hide');
              $('#add_video_form')[0].reset();
            }
          });
        }
        event.preventDefault();
     });

     $('#caption').bind('keyup , change' , function(){
          if($('#caption').val()!=""){
          $('#caption').removeClass('red');
          $('#video_caption').text('');
        }
     });

     $('#url').bind('keyup , change' , function(){
          if($('#url').val()!=""){
          $('#url').removeClass('red');
          $('#video_url').text('');
        }
     });



     $('#form_soundcloud').submit(function(event){
        var caption = $('#soundcloud_caption').val();
        var url = $('#soundcloud_url').val();
        if(caption==""){
          $('#soundcloud_caption').addClass('red');
          $('#soundcloud-caption').text('This field is required').css('color','red');

        }
        if(url==""){
          $('#soundcloud_url').addClass('red');
          $('#soundcloud-url').text('This field is required').css('color','red');

        }
        if(caption!=""&&url!=""){
          $.ajax({
            url : '<?php echo site_url("venueProfile/add_video"); ?>',
            type : 'POST',
            dataType : 'JSON',
            data : $('#form_soundcloud').serialize(),
            success : function(data){
          
              $('#ajax_soundcloud').html(data.data);
              $('#myModal_soundcloud').modal('hide');
              $('#form_soundcloud')[0].reset();
            }
          });
          //$('#form_soundcloud').submit();
        }
        event.preventDefault();
     });

     $('#soundcloud_caption').bind('keyup , change' , function(){
          if($('#soundcloud_caption').val()!=""){
          $('#soundcloud_caption').removeClass('red');
          $('#soundcloud-caption').text('');
        }
     });

     $('#soundcloud_url').bind('keyup , change' , function(){
          if($('#soundcloud_url').val()!=""){
          $('#soundcloud_url').removeClass('red');
          $('#soundcloud-url').text('');
        }
     });


     $('#photo').on('change',function(){ 

         var len   = $('#photo')[0].files.length;
         var name  = $('#photo')[0].files[0].name;
         var ext   = name.split('.').pop().toLowerCase();
         var size  = $('#photo')[0].files[0].size;
         if($.inArray(ext,['png','jpg','jpeg']) == -1){

            $('#photo_ext').text('Please select image in jpg or png').css('color','red');
            return false;
         }
         if(Math.round(size / (1024 * 1024)) > 2){

            $('#photo_size').text('Please select image size less than 2 MB').css('color','red');
            return false;
         }
         else{
         for(var i=0;i<len;i++){ 
         var file = $('#photo')[0].files[i];
         var form_data = new FormData();
         form_data.append('file',file);
         $.ajax({

               type    :  'POST', 
               url     :  '<?php echo site_url("venueProfile/uploadPhotos"); ?>',
               data    :  form_data,
               contentType: false,
               cache: false,
               processData: false,
               success :  function(data){
                // $('#row').html('');
                // $('#row').html(data);
                 $('#members_box > .row').html(data);
             }
         });
       }
     }

     });


     $('.youtube').bind('keyup , change',function(){
          
          var expr = '';
          var url  = $(this).val();
          if($('#video_type_id').val()=="YouTube"){
          expr = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
        }else{
          expr = /vimeo.*(?:\/|clip_id=)([0-9a-z]*)/i;
        }

          if(url.match(expr)){

             $('#add_video').prop('disabled',false);
             $('#video_error').text('');
             return true;
          }else{
            $('#add_video').prop('disabled',true);
            $('#video_error').text('Please enter valid url').css('color','red');
            $('#video_url').text('');
            return false;
          }
     });

     $('.soundcloud').bind('keyup , change',function(){
          
          var expr = '';
          var url  = $(this).val();
          if($('#audio_type_id').val()=="soundcloud"){
          expr = /^https?:\/\/(soundcloud\.com|snd\.sc)\/(.*)$/;
        }else{
          expr = /^(spotify:|https:\/\/[a-z]+\.spotify\.com\/)/;
        }
          if(url.match(expr)){

             $('#submit_soundcloud').prop('disabled',false);
             $('#soundcloud_error').text('');
             return true;
          }else{
            $('#submit_soundcloud').prop('disabled',true);
            $('#soundcloud_error').text('Please enter valid url').css('color','red');
            return false;
          }
     });

     
     $(document).on('click','.edit_btn',function(){
      var row_id = $(this).data('id');

     $.ajax({
          type:'POST',
          url: '<?php echo site_url('artistProfile/edit_member');?>',
          data:{row_id:row_id},
          dataType:'json',


          success:function(data){

              $('#myModal_edit').modal('show');
              $('#name_edit,#role_edit,#alias_edit').val('');
              if(data.data.id){$('#hidden').val(data.data.id);}
              if(data.data.name){$('#name_edit').val(data.data.name);}
              if(data.data.role){$('#role_edit').val(data.data.role);}
              if(data.data.alias){$('#alias_edit').val(data.data.alias);}
              if(data.image_url && data.image_url!=''){$('#member_image_edit').show().find('img').attr('src',data.image_url);}

          },
          error: function(data){

          }
      });
  });

$(document).on('click','.delete-img',function(){
                  
                    var row_id = $(this).data('id');

                    $.ajax({
                        type:'POST',
                        url: '<?php echo site_url("venueProfile/override_img");?>',
                        data:{row_id:row_id},
                        //dataType:'json',


                        success:function(data){
                            //if(data){ 
                                //$('.artist_gallery').remove();
                                // location.reload();
                                $('.artist_gallery' + row_id).remove();
                            //}

                        },
                        error: function(data){

                        }
                    });
                });


$(document).on('click','.delete_gallery',function(){
                  
                    var row_id = $(this).data('id');

                    $.ajax({
                        type:'POST',
                        url: '<?php echo site_url('venueProfile/delete_gallery');?>',
                        data:{row_id:row_id},
                        dataType:'json',


                        success:function(data){
                            if(data.data){
                                $('.artist_gallery'+row_id).remove();
                                //location.reload();
                            }

                        },
                        error: function(data){

                        }
                    });
                });

     $(document).on('click','.delete_btn',function(){

                    var row_id = $(this).data('id');

                    $.ajax({
                        type:'POST',
                        url: '<?php echo site_url('artistProfile/delete_member');?>',
                        data:{row_id:row_id},
                        dataType:'json',


                        success:function(data){
                            if(data.data){
                                $('.member_list').remove();
                            }

                        },
                        error: function(data){

                        }
                    });
                });
 

     $('.image_file').on('change',function(){

         var file = $('.image_file')[0].files[0]; 
         var name = $('.image_file')[0].files[0].name;
         var size = $('.image_file')[0].files[0].size;
         if (!name.match(/(?:jpg|png|jpeg)$/)) {
              $('#image').html("<br><br><br> Image should be smaller than 2 MB and saved as JPG or PNG files.").css('color','#b30000');
              $('#submit').prop('disabled',true);
              return false;
          }
         else if(Math.round(size / (1024 * 1024)) > 2) { 

             $('#image').html("<br><br><br> Please select image size less than 2 MB").css('color','#b30000');
             $('#submit').prop('disabled',true);
             $('#data-img').show();
             return false;
          } 
         else{
         $('#submit').prop('disabled',false);
         var preview = document.getElementById('image');
         var reader = new FileReader();
         reader.addEventListener("load", function () {
         $('#data-img').hide();
         $('#image').html('<br><br><br><img src="'+reader.result+'" class="img img-thumbnail" style="width:200px;height:200px;">');
        }, false);

        if(file){
          reader.readAsDataURL(file);
        }
      }
     });


      $('#img_browse_edit').on('change',function(){
          $('#member_image_edit').hide();
         var file = $('#img_browse_edit')[0].files[0];
         var edit = new FileReader();
         edit.addEventListener("load", function () {
         $('#edit_image').html('<br><br><br><img src="'+edit.result+'" class="img img-thumbnail" style="width:200px;height:200px;">');
        }, false);

        if(file){
          edit.readAsDataURL(file);
        }
     });

     $('#img_browse').on('change',function(){

         var file = $('#img_browse')[0].files[0]; 
         var preview = document.getElementById('show_img');
         var reader = new FileReader();
         reader.addEventListener("load", function () {
         //preview.src = reader.result;
         $('#show_img').html('<br><br><br><img src="'+reader.result+'" class="img img-thumbnail" style="width:200px;height:200px;">');
        }, false);

        if(file){
          reader.readAsDataURL(file);
        }
     });

    $(".select").select2({
                    width: '100%'
                });
    $("#sort").select2({
        minimumResultsForSearch: -1,
        width: '100%'
    });
  var navListItems = $('ul.setup-panel li a'),
          allWells = $('.tab-pane'),
          allNextBtn = $('.nextBtn'),
        allPrevBtn = $('.prevBtn');

  //allWells.hide();

  navListItems.click(function (e) { 
      e.preventDefault();
      var $target = $($(this).attr('href')),
              $item = $(this);

      if (!$item.hasClass('disabled')) {
          navListItems.removeClass('btn-primary').addClass('btn-default');
          $item.addClass('btn-primary');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
      }
  });
  
  allPrevBtn.click(function(){
      var curStep = $(this).closest(".tab-pane"),
          curStepBtn = curStep.attr("id"),
          prevStepWizard = $('ul.setup-panel li a[href="#' + curStepBtn + '"]').parent().prev().children("a");

          prevStepWizard.removeAttr('disabled').trigger('click');
  });

  allNextBtn.click(function(){
      var curStep = $(this).closest(".tab-pane"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('ul.setup-panel li a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url']"),
          isValid = true;

      $(".form-group").removeClass("has-error");
      for(var i=0; i<curInputs.length; i++){
          if (!curInputs[i].validity.valid){
              isValid = false;
              $(curInputs[i]).closest(".form-group").addClass("has-error");
          }
      }

      if (isValid)
          nextStepWizard.removeAttr('disabled').trigger('click');
  });

  $('ul.setup-panel li a.btn-primary').trigger('click');

});
function deleteFunctionvideo(obj,id){
//console.log(obj);
// alert(id);
$.ajax({
type:'POST',
url: '<?php echo site_url("venueProfile/delete_video");?>',
data:{id:id},
dataType:'json',

success:function(data){
if(data.data){
obj.closest('.single_video').remove();
}
},
error: function(data){

}
});
}
function initialize() {
var input = document.getElementById('artist_address');
var autocomplete = new google.maps.places.Autocomplete(input);
google.maps.event.addListener(autocomplete, 'place_changed', function () {
var place = autocomplete.getPlace();
document.getElementById('city2').value = place.name;
document.getElementById('cityLat').value = place.geometry.location.lat();
document.getElementById('cityLng').value = place.geometry.location.lng();
});
}
google.maps.event.addDomListener(window, 'load', initialize); 
</script>    
</body>
</html>