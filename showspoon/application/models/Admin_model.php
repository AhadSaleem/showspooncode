<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class admin_model extends CI_Model {

    function __construct() {
        parent::__construct();

       
        $this->load->helper('site');
    }


    //******************************* lOGIN FUNCTION*******************************//
    function check_auth($username, $pass, $table_name) {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where('email', $username);
        $this->db->where('password', $pass);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
    //Recent Work Start
    public function checkAdminLogin($admin_login=[])
    {
        $this->db->select();
        $this->db->where($admin_login);
        $sql = $this->db->get('admin');
        if($sql->num_rows() == 1){
            return $sql->row();
        }
    }
    public function getArtist()
    {
        $this->db->select('*');
        $this->db->where('is_delete',0);
        $this->db->order_by('user_id','desc');
        $sql = $this->db->get('artist');
        return $sql->result();
    }
    public function getArtistCount(){
        $this->db->select('COUNT(*) AS artist_count');
        $sql = $this->db->get('artist');
        return $sql->row();
    }
    public function getGenres($genre){
        $output  = array();
        $explode = explode(',',$genre);
        foreach($explode as $exp){
            $sql = $this->db->query("select * from genre where genre_id=".(int)$exp."");
            $output[] = $sql->row()->name;
        }
        return isset($output)?$output:'';
    }
    public function getArtistDetails($id=0){
        $sql = $this->db->query("select A.*,AB.name as band_name,AC.CityName as city,AD.zip as zip,AD.biography as biography,AD.capacity as capacity,AD.age as age,AD.artist_rate as rate,AD.technical_rider as rider,AD.contact_person as contact_person,AD.mobile as mobile,AD.payment_account as payment_account,AD.facebook as facebook,AD.twitter as twitter,AD.instragram as instragram,AD.soundcloud as soundcloud from artist A inner join artist_band_type AB on AB.id=A.band_type_id inner join cities AC on AC.CityID=A.city_id inner join artist_details AD on AD.artist_id=A.artist_id where A.artist_id=".(int)$id."");
        return $sql->row();
    }
    public function getArtistMember($id=0){
        $this->db->select('artist_member.name as artist_name,artist_member.role as artist_role,artist_member.image_url as image_url,artist_member.alias as artist_alias');
        $this->db->where('artist.artist_id',$id);
        $this->db->join('artist','artist.user_id=artist_member.artist_id');
        $sql = $this->db->get('artist_member');
        return $sql->result();
    }
    public function getArtistGallery($id=0){
        $this->db->select('url');
        $this->db->where('artist.artist_id',$id);
        $this->db->join('artist','artist.user_id=artist_gallery.artist_id');
        $sql = $this->db->get('artist_gallery');
        return $sql->result();
    }
    public function getArtistMedia($id=0){
        $this->db->select('*');
        $this->db->where(array('artist.artist_id'=>$id));
        $this->db->join('artist','artist.user_id=artist_media.artist_id');
        $sql = $this->db->get('artist_media');
        return $sql->result();
    }
    public function getAdminProfile($id=0)
    {
        $this->db->select('*');
        $this->db->where('id',$id);
        $sql = $this->db->get('admin');
        return $sql->row()?$sql->row():'';
    }
    public function getVenue()
    {
        $this->db->select('*');
        $this->db->where('is_delete',0);
        $this->db->order_by('user_id','desc');
        $sql = $this->db->get('venues');
        return $sql->result();
    }
    public function getVenueCount(){
        $this->db->select('COUNT(*) AS venue_count');
        $sql = $this->db->get('venues');
        return $sql->row();
    }
    public function getVenueDetails($id=0){
        $sql = $this->db->query("select V.*,VC.CityName as city,VD.zip as zip,VD.capacity as capacity,VD.venue_rate as rate,VD.technical_rider as rider,VD.contact_person as contact_person,VD.mobile as mobile,VD.payment_account as payment_account,VD.facebook as facebook,VD.twitter as twitter,VD.instagram as instragram,VD.soundcloud as soundcloud from venues V inner join cities VC on VC.CityID=V.city_id inner join venue_details VD on VD.venue_id=V.venue_id where V.venue_id=".(int)$id."");
        return $sql->row();
    }
    public function getVenueGallery($id=0){
        $this->db->select('url');
        $this->db->where('venues.venue_id',$id);
        $this->db->join('venues','venues.user_id=venue_gallery.venue_id');
        $sql = $this->db->get('venue_gallery');
        return $sql->result();
    }
    public function getVenueMedia($id=0){
        $this->db->select('*');
        $this->db->where(array('venues.venue_id'=>$id));
        $this->db->join('venues','venues.user_id=venue_media.venue_id');
        $sql = $this->db->get('venue_media');
        return $sql->result();
    }
    public function getReviews(){
        $sql = $this->db->query("select C.*,A.name as from_name,V.name as to_name from comments C inner join artist A on (A.user_id=C.sender_id || A.user_id=C.receiver_id) inner join venues V on (V.user_id=C.sender_id || V.user_id=C.receiver_id) group by C.id order by C.id desc");
        return $sql->result()?$sql->result():'';
    }
    public function getTransactions(){
        $sql = $this->db->query("select B.id as booking_id,B.event_date as event_date,A.name as from_name,V.name as to_name,T.transaction_id as transaction_id,T.token as token,T.type as t_type,T.status as status,T.name as t_name,T.amount as t_amount,T.payment_datetime as t_datetime from transaction T inner join bookings B on B.id=T.booking_id inner join artist A on (A.user_id=T.user_id || A.user_id=T.user) inner join venues V on (V.user_id=T.user_id || V.user_id=T.user) group by T.transaction_id order by T.transaction_id desc");
        return $sql->result()?$sql->result():'';
    }
    public function getTransactionRow($id=0){
        $sql = $this->db->query("select B.event_date as event_date,T.transaction_id as transaction_id,T.status as status,T.name as t_name,T.amount as t_amount,T.token as token,T.type as t_type,T.medium as t_medium,T.payment_datetime as t_datetime,A.name as from_name,V.name as to_name from bookings B inner join transaction T on T.booking_id=B.id inner join artist A on (A.user_id=B.from_id || A.user_id=B.to_id) inner join venues V on (V.user_id=B.from_id || V.user_id=B.to_id) where T.transaction_id=$id");
        return $sql->row()?$sql->row():'';
    }
    public function getTransactionsRow($id=0){
        $this->db->where('transaction_id',$id);
        $sql = $this->db->get('transaction');
        return $sql->row()?$sql->row():'';
    }
    public function getCommission(){
        $this->db->select('commission');
        $sql = $this->db->get('settings');
        return $sql->row();
    }
    public function getTransactionCount(){
        $this->db->select('COUNT(*) AS transaction_count');
        $sql = $this->db->get('transaction');
        return $sql->row();
    }
    public function check_reset_password($email){
        $this->db->select('*');
        $this->db->where('email',$email);
        $sql = $this->db->get('admin');
        return $sql->row()?$sql->row():'';
    }
    public function getAdminDetails(){
        $this->db->select('*');
        $this->db->order_by('id','DESC');
        $sql = $this->db->get('admin');
        return $sql->result();
    }
    public function getAdminCount(){
        $this->db->select('COUNT(*) AS admin_count');
        $sql = $this->db->get('admin');
        return $sql->row();
    }
    public function getAdminEmail($id=0){
        $this->db->select('*');
        $this->db->where('id',$id);
        $sql = $this->db->get('admin');
        return $sql->row()?$sql->row():'';
    }
    public function checkAdminEmail($email){
        $this->db->select('*');
        $this->db->where('email',$email);
        $sql = $this->db->get('admin');
        if($sql->num_rows() > 0){
            return $sql->num_rows();
        }
    }
    public function createAdmin($data=[]){
        $this->db->insert('admin',$data);
        $id = $this->db->insert_id();
        return $id;
    }
    public function updateAdminProfile($id=0,$admin=[])
    {
        $this->db->where('id',$id);
        $update = $this->db->update('admin',$admin);
        if($update){
            return TRUE;
        }
    }
    //Recent Work End
    public function user_login_history($data=array())
    {

       /* $where = $end_date = $start_date = '';

        if(isset($data['start_date']) && $data['start_date']!='')
        {
            $start_date = $data['start_date'];
            $start_date =date('Y-m-d',strtotime($start_date));

            $where .= " and DATE(H.history_datetime) >= '".$start_date."' ";

        }
        if(isset($data['end_date']) && $data['end_date']!='')
        {
            $end_date = $data['end_date'];
            $end_date =date('Y-m-d',strtotime($end_date));
            $where .= " and DATE(H.history_datetime) <= '".$end_date."' ";

        }
        if(isset($data['org_id']))
        {
            $where .= " and O.organisation_id='".$data['org_id']."'";
        }*/

        $query=$this->db->query("select H.*,U.user_fname,U.user_lname,U.user_email,U.company from in_user_login_history H 
        inner join in_user U on U.user_id = H.history_userid order by H.history_id desc");
        return $query->result();
    }
    
    public function get_news() {
        $query = $this->db->query("SELECT N.* FROM in_news AS N where N.is_delete=0
          ORDER BY N.news_id desc");
        return $query->result();
    }
    public function get_news_by_id($id=0) {
        $query = $this->db->query("SELECT N.* FROM in_news AS N where N.is_delete=0 and news_id = ?
          limit 1",array($id));
        return $query->row();
    }
    
    
    public function get_user_count_hisotry($internval=7)
    {
        $query = $this->db->query("Select * from in_report  order by  DATE(`daily_date`) DESC limit $internval");

      

        $result = $query->result_array();

        return $result;
    }
    public function get_user_login_hisotry_month($internval=7)
    {

        
        $query = $this->db->query("Select count(history_id) as total,(`history_datetime`) as cur_date from in_user_login_history group by MONTH(`history_datetime`) order by  MONTH(`history_datetime`) DESC limit $internval");

        $result = $query->result_array();

        return $result;
    }
    
    public function get_pages_history($internval=7)
    {

        $query = $this->db->query("Select count(page_id) as total,(`created_datetime`) as cur_date from in_pages group by DATE(`created_datetime`) order by  DATE(`created_datetime`) DESC limit $internval");

        $result = $query->result_array();

        return $result;
    }
    public function get_pages_history_month($internval=7)
    {

        $query = $this->db->query("Select count(page_id) as total,(`created_datetime`) as cur_date from in_pages group by MONTH(`created_datetime`) order by  MONTH(`created_datetime`) DESC limit $internval");

        $result = $query->result_array();

        return $result;
    }
    public function get_likes_history_month($internval=7)
    {

        $query = $this->db->query("Select count(like_id) as total,(`created_datetime`) as cur_date from in_likes  group by MONTH(`created_datetime`) order by  MONTH(`created_datetime`) DESC limit $internval");
        $result = $query->result_array();

        return $result;
    }
    public function get_users_history($internval=7)
    {

        $query = $this->db->query("Select count(user_id) as total,(`created_datetime`) as cur_date from in_user where created_datetime>DATE_SUB(created_datetime, INTERVAL $internval DAY) group by DAY(`created_datetime`) order by  DATE(`created_datetime`) desc limit $internval");

        $result = $query->result_array();

        return $result;
    }
    public function get_users_history_month($internval=7)
    {

        $query = $this->db->query("Select count(user_id) as total,(`created_datetime`) as cur_date from in_user where created_datetime>DATE_SUB(created_datetime, INTERVAL $internval MONTH) group by MONTH(`created_datetime`) order by  MONTH(`created_datetime`) desc limit $internval");

        $result = $query->result_array();

        return $result;
    }

    public function get_total_users()
    {
        $query = $this->db->query("select user_id from in_user where user_delete=0 order by user_id desc");
        return $query->num_rows();
    }
    public function get_total_logins()
    {
        $query = $this->db->query("select history_id from in_user_login_history order by history_id desc");
        return $query->num_rows();
    }
    public function get_total_pages()
    {
        $query = $this->db->query("select page_id from in_pages order by page_id desc");
        return $query->num_rows();
    }
    
    public function get_users() {
        $query = $this->db->query("SELECT u.* FROM in_user AS u where u.user_delete=0
          ORDER BY u.user_id desc");
        return $query->result();
    }
    public function get_user_by_email($email='') {
        $query = $this->db->query("Select * from in_user where user_email=? and user_delete=0 limit 1",array($email));
        return $query->row();
    }
    public function get_user_by_id($id=0) {
        $query = $this->db->query("Select * from in_user where user_id=? and user_delete=0 limit 1",array($id));
        return $query->row();
    }
    public function get_pages() {
        $query = $this->db->query("SELECT 
        (select count(why_id) as why_total from in_why W where W.p_id = P.page_id and W.why_delete=0) as why_total,
        (select count(how_id) as how_total from in_how H where H.p_id = P.page_id and H.how_delete=0) as how_total,
        (select count(when_id) as when_total from in_when WW where WW.p_id = P.page_id and WW.when_delete=0) as when_total,
        P.* from in_pages P where P.is_delete=0 order by P.page_id desc");
        return $query->result();
    }
    public function get_page_by_id($id=0) {
        $query = $this->db->query("Select * from in_pages where page_id=? and is_delete=0 limit 1",array($id));
        return $query->row();
    }
    public function get_page_by_code($code=0) {
        $query = $this->db->query("Select * from in_pages where page_code=? and is_delete=0 limit 1",array($code));
        return $query->row();
    }
    public function validate_slug($slug='')
    {
        $query = $this->db->query("Select page_id,slug from in_pages where slug=?  limit 1",array($slug));
        return $query->row();
    }
    
    
    
    public function get_page($page_id=0) {
        $query = $this->db->query("SELECT * from in_pages where page_id = ? and is_delete=0 limit 1",array($page_id));
        return $query->row();
    }
    public function get_how($lang='en',$page_id=0,$limit=999,$offset=0) {
        $query = $this->db->query("SELECT * from in_how where p_id = ? and how_delete=0 and language=? order by how_id asc limit ?, ?",array($page_id,$lang,$offset,$limit));
        return $query->result();
    }
    public function get_help($lang='en',$page_id=0,$limit=999,$offset=0) {
        $query = $this->db->query("SELECT * from in_help where p_id = ? and help_delete=0 and language=? order by help_id desc limit ?, ?",array($page_id,$lang,$offset,$limit));
        return $query->result();
    }
    
    public function get_when($lang='en',$page_id=0,$limit=999,$offset=0) {
        $query = $this->db->query("SELECT * from in_when where p_id = ? and when_delete=0 and language=? order by when_id desc limit ?, ?",array($page_id,$lang,$offset,$limit));
        return $query->result();
    }
     public function get_why($lang='en',$page_id=0,$limit=999) {
        $query = $this->db->query("SELECT * from in_why where p_id = ? and why_delete=0 and language=? order by why_id desc limit ?",array($page_id,$lang,$limit));
        return $query->result();
    }
    public function get_pages_limit($limit=999,$offset=0) {
        $query = $this->db->query("SELECT * from in_pages where  is_delete=0 order by page_id desc limit ?, ?",array($offset,$limit));
        return $query->result();
    }
    
    public function get_record_limit($limit=999,$offset=0) {
        $query = $this->db->query("SELECT * from in_report   order by report_id desc limit ?, ?",array($offset,$limit));
        return $query->result();
    }
    public function get_sent_limit($limit=999,$offset=0) {
        $query = $this->db->query("SELECT * from in_sent   order by sent_id desc limit ?, ?",array($offset,$limit));
        return $query->result();
    }
    public function get_login_history($limit=999,$offset=0) {
        $query = $this->db->query("SELECT U.user_fname,U.user_lname,H.history_datetime,U.user_email,U.company from in_user_login_history H
        inner join in_user U on U.user_id = H.history_userid
        where  U.user_delete=0 order by H.history_datetime desc limit ?, ?",array($offset,$limit));
        return $query->result();
    }
    
    
    
    


    public function get_language_dropdown() {
        $this->db->from('in_languages');
        // $is_active = array('1');
        $this->db->where('is_active', '1');
        $this->db->order_by('id');
        $result = $this->db->get();
        $return = array();
        if ($result->num_rows() > 0) {
            //$return[''] = 'Velg kontotype';
            foreach ($result->result_array() as $row) {
                $return[$row['id']] = $row['name'];
            }
        }
        return $return;
    }

    /*     * ***********************rehan created****************************** */

    
    


    public function get_lc_history($internval=7)
    {

        $query = $this->db->query("Select count(newsfeed_id) as total,(`newsfeed_publisheddate`) as cur_date from in_newsfeed 
where social_media_id=0 and  
newsfeed_publisheddate>DATE_SUB(newsfeed_publisheddate, INTERVAL $internval DAY) 
group by DAY(`newsfeed_publisheddate`) 
order by  DATE(`newsfeed_publisheddate`) desc limit $internval");

        $result = $query->result_array();

        return $result;
    }
    public function get_lc_history_month($internval=7)
    {

        $query = $this->db->query("Select count(newsfeed_id) as total,(`newsfeed_publisheddate`) as cur_date from in_newsfeed 
where social_media_id=0 and  
newsfeed_publisheddate>DATE_SUB(newsfeed_publisheddate, INTERVAL $internval MONTH) 
group by MONTH(`newsfeed_publisheddate`) 
order by  DATE(`newsfeed_publisheddate`) desc limit $internval");

        $result = $query->result_array();

        return $result;
    }

    public function get_act_history($internval=7)
    {

        $query = $this->db->query("Select count(content_id) as total,(`content_createddatetime`) as cur_date from in_content where content_messagetypeid=2 and  content_createddatetime>DATE_SUB(content_createddatetime, INTERVAL $internval DAY) group by DAY(`content_createddatetime`) order by  DATE(`content_createddatetime`) desc limit $internval");

        $result = $query->result_array();

        return $result;
    }

    public function get_act_history_month($internval=7)
    {

        $query = $this->db->query("Select count(content_id) as total,(`content_createddatetime`) as cur_date from in_content where content_messagetypeid=2 and  content_createddatetime>DATE_SUB(content_createddatetime, INTERVAL $internval MONTH) group by MONTH(`content_createddatetime`) order by  DATE(`content_createddatetime`) desc limit $internval");

        $result = $query->result_array();

        return $result;
    }
    public function map_data()
    {
        $query = $this->db->query("select in_countries.CNT_ISO,user_id,
(select history_country from in_user_login_history where history_userid=user_id  and (history_latitude!='' || history_latitude!='undefined' ) order by history_id desc limit 1) as country,
(select history_city from in_user_login_history where history_userid=user_id and (history_latitude!='' || history_latitude!='undefined' ) order by history_id desc limit 1) as user_pro

 from in_user 
left join in_countries on in_countries.CNT_name=(select history_country from in_user_login_history where history_userid=user_id order by history_id desc limit 1) 
");
        $result = $query->result_array();

        return $result;
    }

    public function get_total_lc_content() {
        $query = $this->db->query("Select count(newsfeed_id) as total_content from in_newsfeed where social_media_id=0");
        $result = $query->result();
        return $result[0]->total_content;
    }

    public function get_total_act_content() {
        $query = $this->db->query("Select count(content_id) as total_content from in_content where content_messagetypeid=2");
        $result = $query->result();
        return $result[0]->total_content;
    }
    public function get_total_programme() {
        $query = $this->db->query("Select count(programme_id) as total_content from in_programme");
        $result = $query->result();
        return $result[0]->total_content;
    }

    /*public function get_total_users() {
        $query = $this->db->query("Select count(user_id) as total_userid from in_user");
        $result = $query->result();
        return $result[0]->total_userid;
    }*/
    public function get_total_likes() {
        $query = $this->db->query("Select count(like_id) as total_userid from in_likes");
        $result = $query->result();
        return $result[0]->total_userid;
    }

    public function get_total_organization() {
        $query = $this->db->query("Select count(organisation_id) as total_orgid from in_organisation ");
        $result = $query->result();
        return $result[0]->total_orgid;
    }

    public function get_total_view() {
        $query = $this->db->query("Select count(logid) as total_log from in_newsviews ");
        $result = $query->result();
        return $result[0]->total_log;
    }

    public function monthly_news() {
        $query = $this->db->query("SELECT count(content_id) as totalcost, Month(content_createddatetime) as month from in_content group by Month(content_createddatetime) ");
        return $query->result();
    }

    public function weekely_news() {
        $query = $this->db->query("SELECT count(content_id) as totalmeals, date(content_createddatetime) as order_date from in_content where  WEEK (content_createddatetime) = WEEK( current_date )   group by day (content_createddatetime)");
        return $query->result();
    }

    public function follower_monthly() {
        $id = $this->login_user_ID['user_id'];
        $query = $this->db->query("SELECT count(user_id) as totalfollower, Month(user_createddatetime) as month from in_user  group by Month(user_createddatetime)");
        return $query->result();
    }

    public function get_all_organization() {
        $query = $this->db->query("Select * from in_organisation order by organisation_createddatetime Desc ");
        return $query->result();
    }

    public function insert_organization() {
        if ($_FILES ['company_logo']['name']) {
            $string = preg_replace('/\s+/', '', $_FILES ['company_logo']['name']); // removing white spaces
            $log_name = time() . "_" . mt_rand() . "_" . $string; // changing file name
            $fileName = $log_name;
            $upload_dir = site_url("upload/organizationlogos/");
            move_uploaded_file($_FILES ['company_logo']["tmp_name"], ("upload/organizationlogos") . "/" . ($fileName));
        } else {
            $fileName = '';
        }


        /* if ($this->input->post('ok')) {
            $ok = $this->input->post('ok');
        } else {
            $ok = 'Ok';
        }
        if ($this->input->post('no')) {
            $no = $this->input->post('no');
        } else {
            $no = 'No';
        }
        if ($this->input->post('yes')) {
            $yes = $this->input->post('yes');
        } else {
            $yes = 'Yes';
        }
        if ($this->input->post('agendo')) {
            $agendo = $this->input->post('agendo');
        } else {
            $agendo = 'Agendo';
        }
        $lang_arr = array(
            'news_from' => 'News from',
            'more' => 'More',
            'home' => 'Home',
            'content' => 'Content',
            'message' => 'Message',
            'event' => 'Event',
            'user_profile' => 'User Profile',
            'location' => 'Location',
            'duration' => 'Duration',
            'admin' => 'Admin',
            'contact' => 'Contact',
            'email' => 'Eamil',
            'enroll' => 'Enroll',
            'logout' => 'Logout',
            'feedback' => 'Feedback',
            'detail' => 'Detail',
            'thanku_rate_content' => 'Thank you for rating this content'
        );
        $lang_box = array();
        $lang_box[] = $ok;
        $lang_box[] = $no;
        $lang_box[] = $yes;
        $lang_box[] = $agendo;
        foreach ($lang_arr as $key => $val) {
            $lang_box[] = $this->input->post($key);
        }
        $lang_str = '';
        if (count($lang_box) > 0) {
            $lang_str = implode(',', $lang_box);
        }
        */
        echo '<meta charset="utf-8" />';
        $lang_str=$lang_rus=$lang_tata='';
        $data_eng  = $this->input->post('lang_eng');
        $data_rus  = $this->input->post('lang_rus');
        $data_tata  = $this->input->post('lang_tata');
        if($data_eng && is_array($data_eng))
        {
            $lang_str= implode(',',$data_eng);
        }
        if($data_rus && is_array($data_rus))
        {
            $lang_str_rus= implode(',',$data_rus);
        }
        if($data_tata && is_array($data_tata))
        {
            $lang_str_tata = implode(',',$data_tata);
        }
        $rss = $this->input->post('rss_en') . ',' . $this->input->post('rss_tt') . ',' . $this->input->post('rss_ru');

        $data = array(
            'organisation_password' => md5($this->input->post('Password')),
            'organisation_name' => $this->input->post('name'),
            'organisation_email' => $this->input->post('supportemail'),
            'organisation_info' => $this->input->post('detail'),
            'organisation_country' => $this->input->post('country'),
            'organisation_twitter' => $this->input->post('twitter'),
            'organisation_contactnumber' => $this->input->post('supportphone'),
            'organisation_website' => $this->input->post('website'),
            'organisation_address' => $this->input->post('address'),
            'organisation_city' => $this->input->post('city'),
            'organisation_facebook' => $this->input->post('facebook'),
            'organisation_instagram' => $this->input->post('instagram'),
            //'organisation_rss' => $this->input->post('rss'),
            'organisation_rss' => $rss,
            'organisation_colourcode' => $this->input->post('colorvalue'),
            'organisation_active' => 1,
            'organisation_createddatetime' => date('Y-m-d H:i:s'),
            'organisation_updateddatetime' => date('Y-m-d H:i:s'),
            'organisation_image' => $fileName,
            'organisation_languagemapping' => $lang_str,
            'organisation_languagemapping_rus' => $lang_str_rus,
            'organisation_languagemapping_tata' => $lang_str_tata,
        );
        if ($_FILES ['org_logo']['name']) {
            $string = preg_replace('/\s+/', '', $_FILES ['org_logo']['name']); // removing white spaces
            $log_name = time() . "_" . mt_rand() . "_" . $string; // changing file name
            $fileName2 = $log_name;
            $upload_dir = site_url("upload/organizationlogos/");
            move_uploaded_file($_FILES ['org_logo']["tmp_name"], ("upload/organizationlogos") . "/" . ($fileName2));
            $data['org_logo']=$fileName2;
        } 
        $this->db->insert('in_organisation', $data);
        table_log('organisations');
    }

    public function update_organization() {
        if ($_FILES ['company_logo']['name']) {
            $string = preg_replace('/\s+/', '', $_FILES ['company_logo']['name']); // removing white spaces
            $log_name = time() . "_" . mt_rand() . "_" . $string; // changing file name
            $fileName = $log_name;
            $upload_dir = site_url("upload/organizationlogos/");
            move_uploaded_file($_FILES ['company_logo']["tmp_name"], ("upload/organizationlogos") . "/" . ($fileName));
        } else {
            $fileName = $this->input->post('image_url');
        }
        /*$key_map = array('Ok', 'No', 'Yes', 'Agenda', 'News_from', 'More', 'Home', 'Content', 'Message', 'Event', 'User_Profile', 'Location', 'Duration', 'Admin', 'Contact', 'Eamil', 'Enroll', 'Logout', 'Feedback', 'Detail', 'thanku_rate_content');
        foreach ($key_map as $row) {
            if ($this->input->post($row)) {
                $key[] = $this->input->post($row);
            } else {
                $key[] = $row;
            }
        }
        $lmapping = implode(',', $key);*/
        // print_r($_REQUEST);

        echo '<meta charset="utf-8" />';
        $lang_str=$lang_rus=$lang_tata='';
        $data_eng  = $this->input->post('lang_eng');
        $data_rus  = $this->input->post('lang_rus');
        $data_tata  = $this->input->post('lang_tata');
        if($data_eng && is_array($data_eng))
        {
            $lang_str= implode(',',$data_eng);
        }
        if($data_rus && is_array($data_rus))
        {
            $lang_str_rus= implode(',',$data_rus);
        }
        if($data_tata && is_array($data_tata))
        {
            $lang_str_tata = implode(',',$data_tata);
        }
        $rss = $this->input->post('rss_en') . ',' . $this->input->post('rss_tt') . ',' . $this->input->post('rss_ru');

        $data = array(
            'organisation_name' => $this->input->post('name'),
            'organisation_info' => $this->input->post('detail'),
            'organisation_country' => $this->input->post('country'),
            'organisation_twitter' => $this->input->post('twitter'),
            'organisation_contactnumber' => $this->input->post('supportphone'),
            'organisation_website' => $this->input->post('website'),
            'organisation_address' => $this->input->post('address'),
            'organisation_city' => $this->input->post('city'),
            'organisation_facebook' => $this->input->post('facebook'),
            'organisation_instagram' => $this->input->post('instagram'),
            'organisation_rss' => $rss,
            'organisation_colourcode' => $this->input->post('colorvalue'),
            'organisation_active' => $this->input->post('active'),
            'organisation_updateddatetime' => date('Y-m-d H:i:s',time()),
            'organisation_image' => $fileName,
            'organisation_languagemapping' => $lang_str,
            'organisation_languagemapping_rus' => $lang_str_rus,
            'organisation_languagemapping_tata' => $lang_str_tata,
        );
        if ($_FILES ['org_logo']['name']) {
            $string = preg_replace('/\s+/', '', $_FILES ['org_logo']['name']); // removing white spaces
            $log_name = time() . "_" . mt_rand() . "_" . $string; // changing file name
            $fileName2 = $log_name;
            $upload_dir = site_url("upload/organizationlogos/");
            move_uploaded_file($_FILES ['org_logo']["tmp_name"], ("upload/organizationlogos") . "/" . ($fileName2));
            $data['org_logo']=$fileName2;
        } 
        $this->db->where('organisation_id', $this->uri->segment(3));
        $this->db->update('in_organisation', $data);
        table_log('organisations');
    }

    public function viewprogramme() {

        $query = $this->db->query("Select * from in_programme order by programme_createddatetime desc ");
        $result = $query->result();
        return $query->result();
    }

    public function get_organization_byid($id) {
        $query = $this->db->query("Select * from in_organisation where organisation_id=$id  ");
        return $query->result();
    }

    public function get_allorganisation_byidname() {
        $query = $this->db->query("Select organisation_id,organisation_name from in_organisation order by organisation_createddatetime Desc ");
        return $query->result();
    }

    public function get_programme_byid($id) {
        $query = $this->db->query("Select * from in_programme where programme_id=$id  ");
        return $query->result();
    }

    public function get_programmemembers($id) {
        $query = $this->db->query("Select * from in_user_programme,in_user where user_programme_userid=user_id and user_programme_programmeid=$id  ");
        return $query->result();
    }

    public function update_programme() {
        $programme_id = $this->input->post('programid');
        $programme_organisationid = $this->input->post('organisation');
        $programme_name = $this->input->post('title');
        $programme_startdatetime = $this->input->post('startdate');
        $programme_enddatetime = $this->input->post('enddate');
        $programme_info = $this->input->post('pdescription');
        $programme_sponsor = $this->input->post('sponsors');
        $programme_hashtags = $this->input->post('hashtags');
        $programme_location = $this->input->post('location');
        $programme_locationradius = $this->input->post('radius');
        $programme_active = 1;
        //$programme_createddatetime = date('Y-m-d H:i:s');
        $programme_updateddatetime = date('Y-m-d H:i:s');
        $data = array(
            'programme_organisationid' => $programme_organisationid,
            'programme_name' => $programme_name,
            'programme_startdatetime' => $programme_startdatetime,
            'programme_enddatetime' => $programme_enddatetime,
            'programme_info' => $programme_info,
            'programme_sponsor' => $programme_sponsor,
            'programme_hashtags' => $programme_hashtags,
            'programme_location' => $programme_location,
            'programme_locationradius' => $programme_locationradius,
            'programme_active' => $programme_active,
            //'programme_createddatetime' => $programme_createddatetime,
            'programme_updateddatetime' => $programme_updateddatetime,
            'programme_langid' => $this->input->post('lang_id'),
        );

        $this->db->where('programme_id', $programme_id);
        $this->db->update('in_programme', $data);
        table_log('in_programme');
        $text = trim($_POST['delegates']); // remove the last \n or whitespace character
        //$text = nl2br($text);
        /*$text = explode("\n", $text);
        foreach ($text as $row) {
            $key = explode(",", $row);
            $query = $this->db->query("Select user_id,user_email from in_user where user_email='" . $key[0] . "'");
            $result = $query->result();
            $userid = isset($result[0]->user_id)?$result[0]->user_id:0;
            if ($query->num_rows() == 0) {
                $this->db->insert('in_user', array('user_email' => $key[0], 'user_adminemail' => $key[1], 'user_fname' => $key[2], 'user_lname' => $key[3], 'user_university' => $key[4], 'user_phonenumber' => $key[5], 'user_langid' => $this->input->post('lang_id'), 'user_organisationid' => $programme_organisationid, 'user_createddatetime' => date('Y-m-d H:i:s'), 'user_updateddatetime' => date('Y-m-d H:i:s')));
                $userid = $this->db->insert_id();
                table_log('users');
                $this->db->insert('in_user_programme', array('user_programme_programmeid' => $programme_id, 'user_programme_userid' => $userid, 'user_programme_active' => 1, 'user_programme_updateddatetime' => date('Y-m-d H:i:s')));
            } else {

                $result = '';
                $result = $query->result();
                $userid = $result[0]->user_id;

                // update user table
                $this->db->where('user_id', $userid);
                $this->db->update('in_user', array('user_email' => $key[0], 'user_adminemail' => $key[1], 'user_fname' => $key[2], 'user_lname' => $key[3], 'user_university' => $key[4], 'user_phonenumber' => $key[5], 'user_organisationid' => $programme_organisationid, 'user_updateddatetime' => date('Y-m-d H:i:s')));

                table_log('users');

                // now check if the user has an entry in in_user_programme if so update it
                $queryprogrammeusers = $this->db->query("Select * from in_user_programme,in_user where user_programme_userid=user_id and user_programme_programmeid=$programme_id and user_email='" . $key[0] . "'");
                if ($queryprogrammeusers->num_rows() == 0) {
                    $this->db->insert('in_user_programme', array('user_programme_programmeid' => $programme_id, 'user_programme_userid' => $userid, 'user_programme_active' => 1, 'user_programme_createddatetime' => date('Y-m-d H:i:s'), 'user_programme_updateddatetime' => date('Y-m-d H:i:s')));
                } else {

                    $result = $query->result();
                    $userid = $result[0]->user_id;
                    $this->db->where('user_programme_userid', $userid);

                    $this->db->update('in_user_programme', array('user_programme_programmeid' => $programme_id, 'user_programme_userid' => $userid, 'user_programme_active' => 1, 'user_programme_createddatetime' => date('Y-m-d H:i:s'), 'user_programme_updateddatetime' => date('Y-m-d H:i:s')));
                }
            }
        }*/
    }

    public function set_organisation($documentfile) {
        $programme_organisationid = $this->input->post('organisation');
        $programme_name = $this->input->post('title');
        $programme_startdatetime = $this->input->post('startdate');
        $programme_enddatetime = $this->input->post('enddate');
        $programme_info = $this->input->post('pdescription');
        $programme_sponsor = $this->input->post('sponsors');
        $programme_hashtags = $this->input->post('hashtags');
        $programme_location = $this->input->post('location');
        $programme_locationradius = $this->input->post('radius');
        $programme_active = 1;
        $programme_createddatetime = date('Y-m-d H:i:s');
        $programme_updateddatetime = date('Y-m-d H:i:s');
        $data = array(
            'programme_organisationid' => $programme_organisationid,
            'programme_name' => $programme_name,
            'programme_startdatetime' => $programme_startdatetime,
            'programme_enddatetime' => $programme_enddatetime,
            'programme_info' => $programme_info,
            'programme_sponsor' => $programme_sponsor,
            'programme_hashtags' => $programme_hashtags,
            'programme_location' => $programme_location,
            'programme_locationradius' => $programme_locationradius,
            'programme_active' => $programme_active,
            'programme_createddatetime' => $programme_createddatetime,
            'programme_updateddatetime' => $programme_updateddatetime,
            'programme_csv' => $documentfile,
            'programme_langid' => $this->input->post('lang_id'),
        );
        $this->db->insert('in_programme', $data);
        $programmeid = $this->db->insert_id();
        if ($documentfile != '') {
            $upload_dir = site_url("upload/programmecsv/");
            define('CSV_PATH', $upload_dir);
            echo $csv_file = CSV_PATH . "/" . $documentfile;

            if (($getfile = fopen($csv_file, "r")) !== FALSE) {
                while (($data = fgetcsv($getfile, 1100000, ",")) !== FALSE) {
                    $result = $data;
                    $col1 = $result[0];
                    $col2 = $result[1];
                    $col3 = $result[2];
                    $col4 = $result[3];
                    $col5 = $result[4];
                    $col6 = $result[5];
                    $query = $this->db->query("Select user_id,user_email from in_user where user_email='" . $col1 . "'");
                    if ($query->num_rows() == 0) {
                        $this->db->insert('in_user', array('user_email' => $col1, 'user_adminemail' => $col2, 'user_fname' => $col3, 'user_lname' => $col4, 'user_university' => $col5, 'user_phonenumber' => $col6, 'user_langid' => $this->input->post('lang_id'), 'user_organisationid' => $programme_organisationid, 'user_createddatetime' => date('Y-m-d H:i:s'), 'user_updateddatetime' => date('Y-m-d H:i:s')));
                        $userid = $this->db->insert_id();
                        table_log('users');
                        $this->db->insert('in_user_programme', array('user_programme_programmeid' => $programmeid, 'user_programme_userid' => $userid, 'user_programme_active' => 1, 'user_programme_createddatetime' => date('Y-m-d H:i:s'), 'user_programme_updateddatetime' => date('Y-m-d H:i:s')));
                    } else {
                        $result = '';
                        $result = $query->result();
                        $userid = $result[0]->user_id;
                        $this->db->insert('in_user_programme', array('user_programme_programmeid' => $programmeid, 'user_programme_userid' => $userid, 'user_programme_active' => 1, 'user_programme_createddatetime' => date('Y-m-d H:i:s'), 'user_programme_updateddatetime' => date('Y-m-d H:i:s')));
                    }
                }
            }
        } else {
            $text = trim($_POST['delegates']); // remove the last \n or whitespace character
            if($text!=''){
                //$text = nl2br($text);
                $text = explode("\n", $text);
                foreach ($text as $row) {
                    $key = explode(",", $row);
                    $query = $this->db->query("Select user_id,user_email from in_user where user_email='" . $key[0] . "'");
                    if ($query->num_rows() == 0) {
                        $this->db->insert('in_user', array('user_email' => $key[0], 'user_adminemail' => $key[1], 'user_fname' => $key[2], 'user_lname' => $key[3], 'user_university' => $key[4], 'user_phonenumber' => $key[5], 'user_langid' => $this->input->post('lang_id'), 'user_organisationid' => $programme_organisationid, 'user_createddatetime' => date('Y-m-d H:i:s'), 'user_updateddatetime' => date('Y-m-d H:i:s')));
                        $userid = $this->db->insert_id();
                        table_log('users');
                        $this->db->insert('in_user_programme', array('user_programme_programmeid' => $programmeid, 'user_programme_userid' => $userid, 'user_programme_active' => 1, 'user_programme_createddatetime' => date('Y-m-d H:i:s'), 'user_programme_updateddatetime' => date('Y-m-d H:i:s')));
                    } else {
                        $result = '';
                        $result = $query->result();
                        $userid = $result[0]->user_id;
                        $this->db->insert('in_user_programme', array('user_programme_programmeid' => $programmeid, 'user_programme_userid' => $userid, 'user_programme_active' => 1, 'user_programme_createddatetime' => date('Y-m-d H:i:s'), 'user_programme_updateddatetime' => date('Y-m-d H:i:s')));
                    }
                }

            }
        }
    }



    public function get_eventenrollment($id) {


        $query = $this->db->query("SELECT * from in_user_events as events,in_user as users where users.user_id=events.user_id and event_id in (Select content_event_id from in_content_event where content_contentid =$id) ORDER BY events.insert_time desc");
        return $query->result();
    }

    public function delete_eventenrollment($id) {

        $this->db->query("delete from in_user_events where reg_id=$id ORDER BY insert_time desc");
    }

    public function get_users_details($id) {
        $query = $this->db->query("SELECT u.*,organisation_name As orgname FROM in_user AS u LEFT OUTER JOIN  in_organisation AS org ON org.organisation_id=u.user_organisationid WHERE u.user_id=$id");
        return $query->result();
    }

    /* -------------GET Login History---------------- */

    public function get_loginhistory($id) {
        $query = $this->db->query("SELECT * FROM `in_user_login_history` WHERE history_userid=$id ORDER BY history_id DESC");
        return $query->result();
    }

    /*     * *****************************notifications************************************ */

    public function viewnotifications() {

        $query = $this->db->query("Select *,SUBSTR(`newsfeed_body`,1, 80) AS short_des from in_newsfeed where social_media_id=5  order by `newsfeed_publisheddate` desc ");
        $result = $query->result();
        return $query->result();
    }

    public function registernotification() {

        /*$data = array(
            'notification_title' => $this->input->post('title'),
            'notification_description' => $this->input->post('description'),
            'notification_programmeid' => $this->input->post('programmeid'),
            'notification_organisationid' => $this->input->post('organisationid'),
            'notification_sentdate' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('in_notifications', $data);*/




        $created_time=date('Y-m-d H:i:s',time());
        $data_arr = array(
            'newsfeed_publisheddate' => date('Y-m-d H:i:s'),
            'newsfeed_body' => $this->input->post('description'),
            'newsfeed_title' => $this->input->post('title'),
            'newsfeed_langid' => $this->input->post('lang_id'),
            'newsfeed_active' => '1',
            'newsfeed_organisationid' => $this->input->post('organisationid'),
            'newsfeed_programmeid' => $this->input->post('programmeid'),
            'newsfeed_createddatetime' => date('Y-m-d H:i:s'),
            'newsfeed_updateddatetime' => date('Y-m-d H:i:s'),
            'social_media_id' => '5',
            'last_post_created_time' => $created_time,
        );
        $this->db->insert('in_newsfeed', $data_arr);


        // table_log('newsfeed');
        if ($this->input->post('title') != "" && $this->input->post('description') != "") {
            $message = array
                (
                'message' => $this->input->post('description'),
                'title' => $this->input->post('title'),
            );
            // $registeredids = $this->getregisteredids($this->input->post('programmeid'));


            $calculation=array(
                'program_id'=>$this->input->post('programmeid'),
                'lat'=>$this->input->post('lat'),
                'lng'=>$this->input->post('lng'),
                'lang_id'=>$this->input->post('lang_id'),
                'radius'=>$this->input->post('radius'),

            );


            $registeredids = $this->get_distance_idx($calculation);


            $pushnotification = $this->sendPushNotification($registeredids, $message);
        }
    }

    /*     * *****************************content************************************ */

    public function get_lc_content($limit=5)
    {
        $query = $this->db->query("Select programme_id,programme_name,programme_updateddatetime,programme_createddatetime from in_programme where programme_active = 1 order by `programme_id` desc limit $limit ");
        $result = $query->result();
        return $result;
    }

    public function get_act_content($limit=5)
    {
        $query = $this->db->query("Select content_id,content_title,content_updateddatetime,content_createddatetime from in_content where content_messagetypeid = 2 order by `content_id` desc limit $limit ");
        $result = $query->result();
        return $result;
    }

    public function get_enrolled_students($limit=10)
    {
        $cur_date=date('Y-m-d',time());
        /*$query = $this->db->query("SELECT programme_id,programme_name,(select count(user_programme_id) as total from in_user_programme where user_programme_programmeid=programme_id) as total FROM `in_programme` order by total desc limit $limit");*/
        $query = $this->db->query("SELECT P.programme_id,P.programme_name,
(SELECT count(UE.reg_id) FROM `in_user_events` UE
inner join in_content_event CE on CE.content_event_id=UE.event_id
inner join in_content C on C.content_id=CE.content_contentid 
where C.content_programmeid=P.programme_id) as total 
FROM `in_programme` P where DATE(P.programme_enddatetime)>='".$cur_date."'
order by total desc limit $limit");
        $query = $this->db->query("SELECT P.programme_id,P.programme_createddatetime,P.programme_name,
(select count(user_programme_id) as total from in_user_programme where user_programme_programmeid=P.programme_id) as total

FROM `in_programme` P
inner join in_organisation O on O.organisation_id=P.programme_organisationid
where 1=1  AND  DATE(P.programme_enddatetime)>='" . $cur_date . "'
order by total desc limit $limit");
        $result = $query->result();
        return $result;
    }
    public function get_popular_content($limit=10)
    {

        $query = $this->db->query("SELECT content_id,content_title,(select count(logid) as total from in_newsviews where contentid=content_id and section=1) as total FROM `in_content` where content_messagetypeid=2 and  content_title!=''  order by total desc limit $limit");
        $result = $query->result();
        return $result;
    }

    public function get_activities_enrollment($limit=5)
    {

        $query = $this->db->query("SELECT CC.content_id,CC.content_title,
(select count(reg_id) as total from in_user_events UE
        inner join in_content_event CE on CE.content_event_id=UE.event_id
                    inner join in_content C on C.content_id=CE.content_contentid 
        where (UE.event_id = CE.content_event_id and CC.content_id = C.content_id)) as total FROM `in_content` CC 

where CC.content_messagetypeid=2 order by total desc  limit  $limit");
        $result = $query->result();
        return $result;
    }


    public function viewnews() {

        $query = $this->db->query("Select *,(select count(logid) as views_total from in_newsviews where contentid=content_id and section=2) as views_total from in_content,in_organisation,in_messagetype where content_organisationid=organisation_id and content_messagetypeid=messagetype_id and content_messagetypeid=4 order by `content_publisheddatetime` desc ");
        $result = $query->result();
        return $query->result();
    }
    public function view_activities() {

        $query = $this->db->query("SELECT
    *, (
        SELECT
            count(logid) AS views_total
        FROM
            in_newsviews
        WHERE
            contentid = content_id
        AND section = 1
    ) AS views_total,
    (
        SELECT
            count(like_id) AS likes_total
        FROM
            in_likes
        WHERE
            news_id = content_id
        AND section = 1
    ) AS likes_total,

(SELECT
    count(UE.reg_id) AS enrolled_total
FROM
    in_user_events UE
left JOIN in_content_event E ON E.content_event_id= UE.event_id
left JOIN in_content C ON C.content_id = E.content_contentid where C.content_id = DD.content_id) as enrolled_total

FROM
    in_content DD,
    in_organisation,
    in_messagetype
WHERE
    content_organisationid = organisation_id
AND content_messagetypeid = messagetype_id
AND content_messagetypeid = 2
ORDER BY
    `content_id` DESC ");
        $result = $query->result();
        return $query->result();
    }
    public function activity_details($id=0) {

        $query = $this->db->query("select V.*,U.user_fname,U.user_lname from in_newsviews V
        inner join in_user U on U.user_id= V.userid
        where V.contentid= ? and section=1 order by V.date desc ",array($id));
        //$result = $query->result();
        return $query->result();
    }
    public function activity_like_details($id=0) {

        $query = $this->db->query("select V.*,U.user_fname,U.user_lname from in_likes V
        inner join in_user U on U.user_id= V.user_id
        where V.news_id= ? and section=1 order by V.date desc ",array($id));
        //$result = $query->result();
        return $query->result();
    }


    public function get_message_type() {
        $query = $this->db->query("Select messagetype_id,messagetype_name from in_messagetype WHERE messagetype_active!=0 ");
        return $query->result();
    }

    public function registercontent() {





        if ($_FILES ['company_logo']['name']) {
            $string = preg_replace('/\s+/', '', $_FILES ['company_logo']['name']); // removing white spaces
            $log_name = time() . "_" . mt_rand() . "_" . $string; // changing file name
            $fileName = $log_name;

            $upload_dir = site_url("upload/adminlogos/");
            /* if (! file_exists ( $upload_dir ))
              {
              @mkdir ( $upload_dir );
              }
              $file_path = $upload_dir . "/" . $fileName;
             */

            move_uploaded_file($_FILES ['company_logo']["tmp_name"], ("upload/adminlogos") . "/" . ($fileName));
        } else {
            $fileName = '';
        }


        $only_time=date("H:i:s",time());

        $publish_date=$this->input->post('publishdate').' '.$only_time;


        $messagetypes = $this->input->post('message_type');
        $revision_organisationid = $this->input->post('nscharityprojid');
        $content = array(
            'content_messagetypeid' => $this->input->post('message_type'),
            'content_title' => $this->input->post('title'),
            'content_publisheddatetime' => $publish_date,
            'content_programmeid' => $this->input->post('programmeid'),
            'content_type' => $this->input->post('status')?2:1,
            'is_sticky' => $this->input->post('status')?1:0,
            'content_body' => $this->input->post('content'),
            'content_searchtags' => $this->input->post('hashtags'),
            'content_organisationid' => $this->input->post('nscharityprojid'),
            'content_location' => $this->input->post('location'),
            'content_locationradius' => $this->input->post('radius'),
            'is_notification' => $this->input->post('is_notification'),
            'content_active' => $this->input->post('nsapproved'),
            'content_langid' => $this->input->post('lang_id'),
            'lat' => $this->input->post('lat'),
            'lng' => $this->input->post('lng'),
            'content_image' => $fileName,
            'content_createddatetime' => date('Y-m-d H:i:s'),
            'content_updateddatetime' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('in_content', $content);
        $contentid = $this->db->insert_id();
        table_log('content');

        if ($this->input->post('is_notification') == 1) {
            $messagetype = '';
            if ($this->input->post('message_type') == 1) {
                $messagetype = 'Agenda';
            }

            if ($this->input->post('message_type') == 2) {
                $messagetype = 'Activity';
            }

            if ($this->input->post('message_type') == 4) {
                $messagetype = 'Learning Content';
            }

            if ($this->input->post('message_type') == 3) {
                $messagetype = 'Message';
            }

            $message = array
                (
                'message' => $this->input->post('title'),
                'title' => $messagetype
            );



            $calculation=array(
                'program_id'=>$this->input->post('programmeid'),
                'lat'=>$this->input->post('lat'),
                'lng'=>$this->input->post('lng'),
                'lang_id'=>$this->input->post('lang_id'),
                'radius'=>$this->input->post('radius'),
                'hash_tags'=>$this->input->post('hashtags'),

            );
            //$registeredids = $this->getregisteredids($this->input->post('programmeid'));
            $registeredids = $this->get_distance_idx($calculation);
            $pushnotification = $this->sendPushNotification($registeredids, $message);
        }



        if ($this->input->post('message_type') == '2') {
            $event = array(
                'content_contentid' => $contentid,
                'content_event_type' => $this->input->post('eventtype'),
                'content_event_capacity' => $this->input->post('capacity'),
                'content_event_startdatetime' => $this->input->post('startdate'),
                'content_event_enddatetime' => $this->input->post('enddate'),
                'content_start_time' => $this->input->post('start_time'),
                'content_end_time' => $this->input->post('end_time'),
                //'content_event_adminname' => $this->input->post('adminname'),
                //'content_event_admincontact' => $this->input->post('admincontactnumber'),
                //'content_event_adminemail' => $this->input->post('adminemail'),
            );
            $this->db->insert('in_content_event', $event);
            $activity_id = $this->db->insert_id();
            $admin_names = $this->input->post('adminname');
            $admin_emails = $this->input->post('adminemail');
            $admin_contacts = $this->input->post('admincontactnumber');
            $admin_images = $_FILES['admin_image'];
            $total_organizar=count($admin_names);

            for($g=0; $g<$total_organizar; $g++)
            {
                if (isset($admin_images['name'][$g]) && $admin_images['name'][$g]!='') {
                    $string = preg_replace('/\s+/', '', $admin_images['name'][$g]); // removing white spaces
                    $log_name = time() . "_" . mt_rand() . "_" . $string; // changing file name
                    $fileName = $log_name;

                    $upload_dir = site_url("upload/organizarlogo/");
                    move_uploaded_file($admin_images["tmp_name"][$g], ("upload/organizarlogo") . "/" . ($fileName));
                } else {
                    $fileName = '';
                }

                if($admin_names[$g]!='' || $admin_emails[$g]!='' || $admin_contacts[$g]!='' || $fileName!='')
                {
                    $event = array(
                        'activity_id' => $activity_id,
                        'organizar_name' => $admin_names[$g],
                        'organizar_email' => $admin_emails[$g],
                        'organizar_contact' => $admin_contacts[$g],
                        'organizar_image' => $fileName,

                    );
                    $this->db->insert('in_event_organizar', $event);
                }

            }

        }
        $query = $this->db->query("Select revision_update from in_revision where revision_messagetypeid=" . $messagetypes . " and revision_organisationid=" . $revision_organisationid);
        if ($query->num_rows() > 0) {
            $result = $query->result();
            $revision_update = $result[0]->revision_update + 1;
            $this->db->where('revision_messagetypeid', $messagetypes);
            $this->db->where('revision_organisationid', $revision_organisationid);
            $this->db->update('in_revision', array('revision_update' => $revision_update, 'revision_updateddatetime' => date('Y-m-d H:i:s')));
        } else {
            $this->db->insert('in_revision', array('revision_update' => '1', 'revision_updateddatetime' => date('Y-m-d H:i:s'), 'revision_messagetypeid' => $messagetypes, 'revision_organisationid' => $revision_organisationid));
        }
    }

    public function updatecontent($id = 0) {
        $messagetypes = $this->input->post('message_type');
        $revision_organisationid = $this->input->post('nscharityprojid');
        $content = array(
            'content_messagetypeid' => $this->input->post('message_type'),
            'content_title' => $this->input->post('title'),
            'content_publisheddatetime' => $this->input->post('publishdate'),
            'content_programmeid' => $this->input->post('programmeid'),
            'content_type' => $this->input->post('status'),
            'is_sticky' => $this->input->post('status')?1:0,
            'content_body' => $this->input->post('content'),
            'content_searchtags' => $this->input->post('hashtags'),
            'content_organisationid' => $this->input->post('nscharityprojid'),
            'content_location' => $this->input->post('location'),
            'content_locationradius' => $this->input->post('radius'),
            'is_notification' => $this->input->post('is_notification'),
            'content_active' => $this->input->post('nsapproved'),
            'content_langid' => $this->input->post('lang_id'),
            'lat' => $this->input->post('lat'),
            'lng' => $this->input->post('lng'),
            //'content_createddatetime' => date('Y-m-d H:i:s'),
            'content_updateddatetime' => date('Y-m-d H:i:s'),
        );
        if ($_FILES ['company_logo']['name']) {
            $string = preg_replace('/\s+/', '', $_FILES ['company_logo']['name']); // removing white spaces
            $log_name = time() . "_" . mt_rand() . "_" . $string; // changing file name
            $fileName = $log_name;

            $upload_dir = site_url("upload/adminlogos/");
            /* if (! file_exists ( $upload_dir ))
              {
              @mkdir ( $upload_dir );
              }
              $file_path = $upload_dir . "/" . $fileName;
             */

            move_uploaded_file($_FILES ['company_logo']["tmp_name"], ("upload/adminlogos") . "/" . ($fileName));
            $content['content_image'] = $fileName;
        }

        $this->db->where('content_id', $id);
        $updates = $this->db->update('in_content', $content);
        if ($updates) {
            table_log('content');
        }


        if ($this->input->post('is_notification') == 1) {
            $messagetype = '';
            if ($this->input->post('message_type') == 1) {
                $messagetype = 'Agenda';
            }

            if ($this->input->post('message_type') == 2) {
                $messagetype = 'Activity';
            }

            if ($this->input->post('message_type') == 4) {
                $messagetype = 'Learning Content';
            }

            if ($this->input->post('message_type') == 3) {
                $messagetype = 'Message';
            }

            $message = array
                (
                'message' => $this->input->post('title'),
                'title' => $messagetype
            );
            $calculation=array(
                'program_id'=>$this->input->post('programmeid'),
                'lat'=>$this->input->post('lat'),
                'lng'=>$this->input->post('lng'),
                'lang_id'=>$this->input->post('lang_id'),
                'radius'=>$this->input->post('radius'),
                'hash_tags'=>$this->input->post('hashtags'),

            );
            $registeredids = $this->get_distance_idx($calculation);





            //$registeredids = $this->getregisteredids($this->input->post('programmeid'));
            $pushnotification = $this->sendPushNotification($registeredids, $message);
            /*print_r($registeredids);
        die();*/
        }
        $query = $this->db->query("Select * from in_content_event where content_contentid=$id limit 1");
        $event_data = $query->result();
        $total_events = $query->num_rows();
        if ($this->input->post('message_type') == '2') {

            $event = array(
                'content_event_type' => $this->input->post('eventtype'),
                'content_event_capacity' => $this->input->post('capacity'),
                'content_event_startdatetime' => $this->input->post('startdate'),
                'content_event_enddatetime' => $this->input->post('enddate'),
                'content_start_time' => $this->input->post('start_time'),
                'content_end_time' => $this->input->post('end_time'),
                //'content_event_adminname' => $this->input->post('adminname'),
                //'content_event_adminemail' => $this->input->post('adminemail'),
                //'content_event_admincontact' => $this->input->post('admincontactnumber'),
            );
            if ($total_events > 0) {
                $this->db->where('content_contentid', $id);
                $this->db->update('in_content_event', $event);
            } else {
                $event['content_contentid'] = $id;
                $this->db->insert('in_content_event', $event);
            }

            $activity_id = isset($event_data[0]->content_event_id)?$event_data[0]->content_event_id:0;
            $admin_names = $this->input->post('adminname');
            $admin_emails = $this->input->post('adminemail');
            $admin_old_image = $this->input->post('old_image');
            $admin_contacts = $this->input->post('admincontactnumber');
            $admin_images = $_FILES['admin_image'];
            $total_organizar=count($admin_names);

            $this->db->where('activity_id', $activity_id);
            $this->db->delete('in_event_organizar');
            for($g=0; $g<$total_organizar; $g++)
            {
                if (isset($admin_images['name'][$g]) && $admin_images['name'][$g]!='') {
                    $string = preg_replace('/\s+/', '', $admin_images['name'][$g]); // removing white spaces
                    $log_name = time() . "_" . mt_rand() . "_" . $string; // changing file name
                    $fileName = $log_name;

                    $upload_dir = site_url("upload/organizarlogo/");
                    move_uploaded_file($admin_images["tmp_name"][$g], ("upload/organizarlogo") . "/" . ($fileName));
                } else {
                    $fileName = isset($admin_old_image[$g])?$admin_old_image[$g]:'';
                }

                if($admin_names[$g]!='' || $admin_emails[$g]!='' || $admin_contacts[$g]!='' || $fileName!='')
                {
                    $event = array(
                        'activity_id' => $activity_id,
                        'organizar_name' => $admin_names[$g],
                        'organizar_email' => $admin_emails[$g],
                        'organizar_contact' => $admin_contacts[$g],
                        'organizar_image' => $fileName,

                    );
                    $this->db->insert('in_event_organizar', $event);
                }

            }
        }
        $query = $this->db->query("Select revision_update from in_revision where revision_messagetypeid=" . $messagetypes . " and revision_organisationid=" . $revision_organisationid);
        if ($query->num_rows() > 0) {
            $result = $query->result();
            $revision_update = $result[0]->revision_update + 1;
            $this->db->where('revision_messagetypeid', $messagetypes);
            $this->db->where('revision_organisationid', $revision_organisationid);
            $this->db->update('in_revision', array('revision_update' => $revision_update, 'revision_updateddatetime' => date('Y-m-d H:i:s')));
        } else {
            $this->db->insert('in_revision', array('revision_update' => '1', 'revision_updateddatetime' => date('Y-m-d H:i:s'), 'revision_messagetypeid' => $messagetypes, 'revision_organisationid' => $revision_organisationid));
        }
    }

    /*     * **********************newsfeed***************************** */

    public function viewnewsfeed() {

        $query = $this->db->query("Select *, (select count(like_id) as likes_total from in_likes where news_id = newsfeed_id and section=2) as likes_total, (select count(logid) as views_total from in_newsviews where contentid = newsfeed_id and section=2) as views_total from in_newsfeed where social_media_id=0 order by `newsfeed_id` desc ");
        $result = $query->result();
        return $query->result();
    }
    public function newsfeed_like_details($id=0) {

        $query = $this->db->query("select V.*,U.user_fname,U.user_lname from in_likes V
        inner join in_user U on U.user_id= V.user_id
        where V.news_id= ? and section=2 order by V.date desc ",array($id));
        $result = $query->result();
        return $query->result();
    }
    public function newsfeed_details($id=0) {

        $query = $this->db->query("select V.*,U.user_fname,U.user_lname from in_newsviews V
        inner join in_user U on U.user_id= V.userid
        where V.contentid= ? and section=2 order by V.date desc ",array($id));
        $result = $query->result();
        return $query->result();
    }

    public function getnewsfeed($id = 0) {

        $query = $this->db->query("Select * from in_newsfeed where newsfeed_id=$id limit 1");
        return $query->row();
    }

    public function getcontent($id = 0) {

        $query = $this->db->query("Select * from in_content where content_id=$id limit 1");
        return $query->row();
    }

    public function get_event($id = 0) {

        $query = $this->db->query("Select * from in_content_event where content_contentid=$id limit 1");
        return $query->row();
    }

    public function registernewsfeed() {
        if ($_FILES ['company_logo']['name']) {
            $string = preg_replace('/\s+/', '', $_FILES ['company_logo']['name']); // removing white spaces
            $log_name = time() . "_" . mt_rand() . "_" . $string; // changing file name
            $fileName = $log_name;

            $upload_dir = site_url("upload/adminlogos/");
            /* if (! file_exists ( $upload_dir ))
              {
              @mkdir ( $upload_dir );
              }
              $file_path = $upload_dir . "/" . $fileName;
             */

            move_uploaded_file($_FILES ['company_logo']["tmp_name"], ("upload/adminlogos") . "/" . ($fileName));
        } else {
            $fileName = '';
        }

        $only_time=date("H:i:s",time());

        $publish_date=$this->input->post('publishdate').' '.$only_time;

        $data = array(
            'newsfeed_faicon' => $this->input->post('fonticon'),
            'newsfeed_publisheddate' => $publish_date,
            'newsfeed_body' => $this->input->post('content'),
            'newsfeed_searchtag' => $this->input->post('hashtags'),
            'newsfeed_hash' => $this->input->post('hashtags'),
            'newsfeed_organisationid' => $this->input->post('nscharityprojid'),
            'newsfeed_location' => $this->input->post('location'),
            'newsfeed_locationradius' => $this->input->post('radius'),
            'newsfeed_programmeid' => $this->input->post('programmeid'),
            'newsfeed_title' => $this->input->post('title'),
            'newsfeed_langid' => $this->input->post('lang_id'),
            'lat' => $this->input->post('lat'),
            'lng' => $this->input->post('lng'),
            'newsfeed_active' => $this->input->post('nsapproved'),
            'newsfeed_createddatetime' => date('Y-m-d H:i:s'),
            'newsfeed_updateddatetime' => date('Y-m-d H:i:s'),
            'newsfeed_image' => $fileName,
        );
        $this->db->insert('in_newsfeed', $data);
        table_log('newsfeed');
        if ($this->input->post('is_notification') == 1) {
            $message = array
                (
                'message' => $this->input->post('title'),
                'title' => 'News'
            );

            $calculation=array(
                'program_id'=>$this->input->post('programmeid'),
                'lat'=>$this->input->post('lat'),
                'lng'=>$this->input->post('lng'),
                'lang_id'=>$this->input->post('lang_id'),
                'radius'=>$this->input->post('radius'),
                'hash_tags'=>$this->input->post('hashtags'),

            );
            $registeredids = $this->get_distance_idx($calculation);

            //$registeredids = $this->getregisteredids($this->input->post('programmeid'));
            $pushnotification = $this->sendPushNotification($registeredids, $message);
        }
    }

    public function updatenewsfeed($id = 0) {

        $only_time=date("H:i:s",time());

        $publish_date=$this->input->post('publishdate').' '.$only_time;
        $data = array(
            'newsfeed_faicon' => $this->input->post('fonticon'),
            'newsfeed_publisheddate' => $publish_date,
            'newsfeed_body' => $this->input->post('content'),
            'newsfeed_searchtag' => $this->input->post('hashtags'),
            'newsfeed_hash' => $this->input->post('hashtags'),
            'newsfeed_organisationid' => $this->input->post('nscharityprojid'),
            'newsfeed_location' => $this->input->post('location'),
            'newsfeed_locationradius' => $this->input->post('radius'),
            'newsfeed_title' => $this->input->post('title'),
            'newsfeed_programmeid' => $this->input->post('programmeid'),
            'newsfeed_active' => $this->input->post('nsapproved'),
            'newsfeed_langid' => $this->input->post('lang_id'),
            'newsfeed_createddatetime' => date('Y-m-d H:i:s'),
            'newsfeed_updateddatetime' => date('Y-m-d H:i:s'),
            'lat' => $this->input->post('lat'),
            'lng' => $this->input->post('lng'),
            //'newsfeed_image'=>$fileName,
        );

        if ($_FILES ['company_logo']['name']) {
            $string = preg_replace('/\s+/', '', $_FILES ['company_logo']['name']); // removing white spaces
            $log_name = time() . "_" . mt_rand() . "_" . $string; // changing file name
            $fileName = $log_name;

            $upload_dir = site_url("upload/adminlogos/");
            /* if (! file_exists ( $upload_dir ))
              {
              @mkdir ( $upload_dir );
              }
              $file_path = $upload_dir . "/" . $fileName;
             */

            move_uploaded_file($_FILES ['company_logo']["tmp_name"], ("upload/adminlogos") . "/" . ($fileName));
            $data['newsfeed_image'] = $fileName;
        }


        $this->db->where('newsfeed_id', $id);
        $this->db->update('in_newsfeed', $data);
        table_log('newsfeed');
        if ($this->input->post('is_notification') == 1) {
            $message = array
                (
                'message' => $this->input->post('title'),
                'title' => 'News'
            );

            $calculation=array(
                'program_id'=>$this->input->post('programmeid'),
                'lat'=>$this->input->post('lat'),
                'lng'=>$this->input->post('lng'),
                'lang_id'=>$this->input->post('lang_id'),
                'radius'=>$this->input->post('radius'),
                'hash_tags'=>$this->input->post('hashtags'),

            );

            $registeredids = $this->get_distance_idx($calculation);

            /*print_r($registeredids);
            die();*/

            //$registeredids = $this->getregisteredids($this->input->post('programmeid'));
            $pushnotification = $this->sendPushNotification($registeredids, $message);
        }
    }

    function get_distance_idx($data=array()) {
        $registeredids = array();
        $lat=isset($data['lat'])?$data['lat']:0;
        $lng=isset($data['lng'])?$data['lng']:0;
        $program_id=isset($data['program_id'])?$data['program_id']:0;
        $lang_id=isset($data['lang_id'])?$data['lang_id']:0;
        $radius=isset($data['radius'])?$data['radius']:0;
        $having = $where ='';
        if($radius>0)
        {
            $KM =$radius/1.60934;
            $having .= " having distance<='".$KM."' ";
        }
        if($lang_id>0 && is_numeric($lang_id))
        {
            $where .= " and U.user_langid=$lang_id ";
        }
        if($program_id>0 && is_numeric($program_id))
        {
            $where .= " and P.user_programme_programmeid='".$program_id."' ";
        }

        if(isset($data['hash_tags']) && $data['hash_tags']!='' && is_string($data['hash_tags']))
        {
            $hash_arr=explode(' ',$data['hash_tags']);
            if(count($hash_arr)>0)
            {
                $where .= " and ( ";
                $j=0;
                foreach($hash_arr as $h_arr)
                {
                    $h_arr = "%$h_arr%";
                    if($j==0)
                    {
                        $where .= " U.user_hash like '".$h_arr."' ";
                    }else
                    {
                        $where .= " || U.user_hash like '".$h_arr."' ";
                    }

                    $j++;
                }


                $where .= " || U.user_hash='')";
            }

        }




        $query = $this->db->query("SELECT ROUND(3963 * ACOS(  SIN( $lat*PI()/180 ) * SIN( m1.history_latitude*PI()/180 )+ COS( $lat*PI()/180 ) * COS( m1.history_latitude*PI()/180 )  *  COS( (m1.history_longitude*PI()/180) - ($lng*PI()/180) )   ) , 1)AS distance,m1.*,U.user_langid,U.user_devicetoken,P.user_programme_id
FROM in_user_login_history m1 
LEFT JOIN in_user_login_history m2 ON (m1.history_userid = m2.history_userid AND m1.history_id < m2.history_id)
inner join in_user U on U.user_id = m1.history_userid
left join in_user_programme P on P.user_programme_userid = U.user_id
WHERE m2.history_id IS NULL  and (m1.history_latitude!='undefined' || m1.history_longitude!='undefined') $where $having");

        $query = $query->result();

        foreach ($query as $row) {

            if ($row->user_devicetoken != '') {
                array_push($registeredids, $row->user_devicetoken);
            }
        }


        return $registeredids;
    }

    function getregisteredids($programid) {
        $registeredids = array();

        $query = $this->db->query("Select * from in_user,in_user_programme where user_id=user_programme_userid and user_active=1 and user_programme_programmeid=$programid");
        $query = $query->result();

        foreach ($query as $row) {

            if ($row->user_devicetoken != '') {
                array_push($registeredids, $row->user_devicetoken);
            }
        }

        return $registeredids;
    }

    function sendPushNotification($registration_ids, $message) {


        // $registration_ids=array('d_aQwlFjum8:APA91bF0HFw24W-p73Y9kMwzM1Q2LcBMgz5VOPRnMrreNP9yz4tWq-ilTcli3E-un_01MmoKwkr7frJ2SVl6nFVxeyJHAs-rHhF76W7QZGEB_9KwMh8Urf2VJSwNoIplItFEGuI3PxV_');
        //$message = array('message' => 'here is a message3','title'=> 'This is a title');
        //$message='Hello Testing';
        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => $message,
        );

        define('GOOGLE_API_KEY', 'AIzaSyBUn02dpUP1jY9bs2UaEGhkX2sDhFFdlmc');

        $headers = array(
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        // echo json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        if ($result === false)
            die('Curl failed ' . curl_error());

        curl_close($ch);

        return $result;

    }

    public function update_admin()
    {
        $data = array(
            'lat' => $this->input->post('lat'),
            'lng' => $this->input->post('lng'),
            'lang_id' => $this->input->post('lang_id'),
            'address' => $this->input->post('address'),
            'admin_radius' => $this->input->post('admin_radius'),
            'admin_country' => $this->input->post('admin_country'),

        );
        $admin_id = $this->session->userdata('Insightsadmin_logged_in')['user_id']?$this->session->userdata('Insightsadmin_logged_in')['user_id']:0;

        $this->db->where('admin_id', $admin_id);
        $this->db->update('in_admin', $data);
    }
    public function get_admin($id=0)
    {

        $query = $this->db->query("select * from in_admin where admin_id='".$id."' limit 1");

        $result = $query->row();

        return $result;
    }

    public function get_programmes($pid=1)
    {
        $degree = $this->db->query("Select programme_id, programme_name as programme_title from in_programme where programme_active=1 and programme_organisationid=$pid ");


        return  $degree->result();
    }
    public function get_users_programme($user_id=0)
    {
        $degree = $this->db->query("Select * from in_user_programme where user_programme_active=1 and user_programme_userid='".$user_id."'");


        return  $degree->result();
    }
    public function update_user_details()
    {

        /*print_r($this->input->post());
        die();*/
        $data_arr=array();
        $user_id = $this->uri->segment(3);
        $this->db->where('user_id', $this->uri->segment(3));
        if(isset($_FILES['userfile']['name']) && $_FILES['userfile']['name']!='')
        {

            $string = preg_replace('/\s+/', '', $_FILES['userfile']['name']); // removing white spaces
            $log_name = time() . "_" . mt_rand() . "_" . $string; // changing file name
            $fileName = $log_name;

            $upload_dir = site_url("upload/userimage/");
            move_uploaded_file($_FILES['userfile']["tmp_name"], ("upload/userimage") . "/" . ($fileName));
            $data_arr['user_img']=$fileName;
        }
        $data_arr['user_hash']=$this->input->post('user_hash');

        $this->db->update('in_user', $data_arr);

        $this->db->where('user_programme_userid', $this->uri->segment(3));
        $this->db->delete('in_user_programme');
        $programmes_arr=$this->input->post('user_programme_programmeid');
        $total_programmes=count($programmes_arr);
        $date_time=date('Y-m-d H:i:s',time());


        for($i=0; $i<$total_programmes; $i++)
        {

            $data=array(
                'user_programme_userid'=>$user_id,
                'user_programme_programmeid'=>$programmes_arr[$i],
                'user_programme_createddatetime'=>$date_time,
                'user_programme_updateddatetime'=>$date_time,
                'user_programme_active'=>1
            );
            $this->db->insert('in_user_programme', $data);

        }

        //$this->db->where('user_programme_userid', $this->uri->segment(3));
        //$this->db->update('in_user_programme', array('user_programme_programmeid'=>$this->input->post('user_programme_programmeid')));
    }

    public function delete_program($id=0)
    {
        $this->db->where('programme_id', $id);
        $this->db->delete('in_programme');
    }
    public function delete_content($id=0)
    {
        $this->db->where('content_id', $id);
        $this->db->delete('in_content');
    }
    public function delete_content_event($id=0)
    {
        $this->db->where('content_contentid', $id);
        $this->db->delete('in_content_event');
    }
    public function delete_newsfeed($id=0)
    {
        $this->db->where('newsfeed_id', $id);
        $this->db->delete('in_newsfeed');
    }

    public function likes_report($data=array())
    {

        /*$where = $end_date = $start_date = '';*/

        /*if(isset($data['start_date']) && $data['start_date']!='')
        {
            $start_date = $data['start_date'];
            $start_date =date('Y-m-d',strtotime($start_date));

            $where .= " and DATE(L.date) >= '".$start_date."' ";

        }
        if(isset($data['end_date']) && $data['end_date']!='')
        {
            $end_date = $data['end_date'];
            $end_date =date('Y-m-d',strtotime($end_date));
            $where .= " and DATE(L.date) <= '".$end_date."' ";

        }*/
        $where = $like_start_date=$like_end_date=$views_start_date=$views_end_date='';

        if(isset($data['start_date']) && $data['start_date']!='')
        {
            $start_date = $data['start_date'];
            $start_date =date('Y-m-d',strtotime($start_date));

            $views_start_date=$like_start_date .= " and DATE(`date`) >= '".$start_date."' ";


        }
        if(isset($data['end_date']) && $data['end_date']!='')
        {
            $end_date = $data['end_date'];
            $end_date =date('Y-m-d',strtotime($end_date));
            $views_end_date=$like_end_date .= " and DATE(`date`) <= '".$end_date."' ";


        }
        if(isset($data['user_id']) && $data['user_id']>0)
        {
            $where .=" and U.user_id='".$data['user_id']."' ";
        }
        if(isset($data['org_id']) && $data['org_id']>0)
        {
            $where .=" and O.organisation_id='".$data['org_id']."' ";
        }
        /* $query=$this->db->query("select L.date,U.user_fname,U.user_lname,N.newsfeed_title from in_likes L
            inner join in_user U on U.user_id = L.user_id
            inner join in_newsfeed N on N.newsfeed_id = L.news_id
            where 1=1 $where order by L.date DESC
        ");*/

        $query=$this->db->query("select O.organisation_name,U.user_id,U.user_fname,U.user_lname,U.user_email,
(select count(like_id) as total_likes from in_likes where user_id=U.user_id $like_start_date $like_end_date) as total_likes,
(select count(logid) as total_views from in_newsviews where userid=U.user_id $views_start_date $views_end_date) as total_views
 from in_user U 
 inner join in_organisation O on O.organisation_id=U.user_organisationid
 where 1=1 $where ");
        return $query->result();
    }

    public function activity_report($data=array())
    {

        $where = $end_date = $start_date = '';

        if(isset($data['user_id']) && $data['user_id']>0)
        {
            $where .= " and UE.user_id='".$data['user_id']."' " ;
        }
        if(isset($data['org_id']) && $data['org_id']>0)
        {
            $where .= " and C.content_organisationid='".$data['org_id']."' " ;
        }
        if(isset($data['start_date']) && $data['start_date']!='')
        {
            $start_date = $data['start_date'];
            $start_date =date('Y-m-d',strtotime($start_date));

            $where .= " and DATE(E.content_event_startdatetime) >= '".$start_date."' ";

        }
        if(isset($data['end_date']) && $data['end_date']!='')
        {
            $end_date = $data['end_date'];
            $end_date =date('Y-m-d',strtotime($end_date));
            $where .= " and DATE(E.content_event_enddatetime) <= '".$end_date."' ";

        }




        $query=$this->db->query("SELECT
    C.content_id,
    C.content_title,
    E.content_event_startdatetime,E.content_event_enddatetime,
    (select count(reg_id) as total from in_user_events where event_id=E.content_event_id) as total
FROM
    `in_content` C
INNER JOIN in_content_event E ON E.content_contentid = C.content_id
where 1=1 $where
");
        return $query->result();
    }
    public function programme_report($data=array())
    {

        $where = $end_date = $start_date = '';


        if(isset($data['start_date']) && $data['start_date']!='')
        {
            $start_date = $data['start_date'];
            $start_date =date('Y-m-d',strtotime($start_date));

            $where .= " and DATE(P.programme_createddatetime) >= '".$start_date."' ";

        }
        if(isset($data['end_date']) && $data['end_date']!='')
        {
            $end_date = $data['end_date'];
            $end_date =date('Y-m-d',strtotime($end_date));
            $where .= " and DATE(P.programme_createddatetime) <= '".$end_date."' ";

        }
        if(isset($data['user_id']) && $data['user_id']>0)
        {
            //$where .= " and P.programme_createddatetime = '".$data['user_id']."' ";
        }
        if(isset($data['org_id']) && $data['org_id']>0)
        {
            $where .= " and O.organisation_id = '".$data['org_id']."' ";
        }


        $query=$this->db->query("SELECT P.programme_id,P.programme_createddatetime,P.programme_name,O.organisation_name, 
(select count(user_programme_id) as total from in_user_programme where user_programme_programmeid=P.programme_id) as total

FROM `in_programme` P
inner join in_organisation O on O.organisation_id=P.programme_organisationid
where 1=1 $where order by P.programme_id DESC
        ");
        return $query->result();
    }
    public function programme_enroled_users($id=0)
    {

        $query=$this->db->query("select U.user_id,U.user_fname,U.user_lname,U.user_email,UP.user_programme_createddatetime,P.programme_name from in_user U 
inner join in_user_programme UP on UP.user_programme_userid=U.user_id
inner join in_programme P on P.programme_id=UP.user_programme_programmeid
where 1=1 and UP.user_programme_programmeid='".$id."'
 order by P.programme_id DESC
        ");
        return $query->result();
    }
    public function enrolled_users_report($id=0)
    {

        $query=$this->db->query("select UE.*,U.user_fname,U.user_lname from in_user_events UE
inner join  in_content_event E on UE.event_id = E.content_event_id
inner join  in_content C on C.content_id= E.content_contentid
inner join  in_user U on U.user_id= UE.user_id
where C.content_id=?
        ",array($id));
        return $query->result();
    }

    public function user_likes($id=0)
    {

        $query=$this->db->query("SELECT
    *
FROM
    (
        (
            SELECT
                LK.like_id,
                C.content_title,
                U.user_fname,
                U.user_lname,
                LK.date,
                U.user_id,
                C.content_body,
                6 AS social_media_id,
                ADDTIME(LK.date,concat(CAST(CU.time_zone AS DECIMAL(11,0)),':00')) as date2
            FROM
                in_likes LK
            INNER JOIN in_user U ON U.user_id = LK.user_id
            INNER JOIN in_content C ON C.content_id = LK.news_id

            inner join in_countries CU on CU.CNT_name= U.user_country
            WHERE
                LK.section = 1
        )
        UNION ALL
            (
                SELECT
                    LK.like_id,
                    C.newsfeed_title AS content_title,
                    U.user_fname,
                    U.user_lname,
                    LK.date,
                    U.user_id,
                    C.newsfeed_body AS content_body,
                    C.social_media_id,
                    ADDTIME(LK.date,concat(CAST(CU.time_zone AS DECIMAL(11,0)),':00')) as date2
                FROM
                    in_likes LK
                INNER JOIN in_user U ON U.user_id = LK.user_id
                INNER JOIN in_newsfeed C ON C.newsfeed_id = LK.news_id
                inner join in_countries CU on CU.CNT_name= U.user_country
                WHERE
                    LK.section = 2
            )
    ) results
WHERE
    user_id = ?
ORDER BY
    date DESC
        ",array($id));
        return $query->result();
    }

    public function user_views($id=0)
    {

        $query=$this->db->query("SELECT
    *
FROM
    (
        (
            SELECT
                LK.logid,
                C.content_title,
                U.user_fname,
                U.user_lname,
                LK.date,
                U.user_id,
                C.content_body,
                6 AS social_media_id,
                ADDTIME(LK.date,concat(CAST(CU.time_zone AS DECIMAL(11,0)),':00')) as date2
            FROM
                in_newsviews LK
            INNER JOIN in_user U ON U.user_id = LK.userid
            INNER JOIN in_content C ON C.content_id = LK.contentid

            inner join in_countries CU on CU.CNT_name= U.user_country
            WHERE
                LK.section = 1
        )
        UNION ALL
            (
                SELECT
                    LK.logid,
                    C.newsfeed_title AS content_title,
                    U.user_fname,
                    U.user_lname,
                    LK.date,
                    U.user_id,
                    C.newsfeed_body AS content_body,
                    C.social_media_id,
                    ADDTIME(LK.date,concat(CAST(CU.time_zone AS DECIMAL(11,0)),':00')) as date2
                FROM
                    in_newsviews LK
                INNER JOIN in_user U ON U.user_id = LK.userid
                INNER JOIN in_newsfeed C ON C.newsfeed_id = LK.contentid
                inner join in_countries CU on CU.CNT_name= U.user_country
                WHERE
                    LK.section = 2
            )
    ) results
WHERE
    user_id = ?
ORDER BY
    date DESC
        ",array($id));
        return $query->result();
    }

    public function login_history_report($data=array())
    {

        $where = $end_date = $start_date = '';

        if(isset($data['start_date']) && $data['start_date']!='')
        {
            $start_date = $data['start_date'];
            $start_date =date('Y-m-d',strtotime($start_date));

            $where .= " and DATE(H.history_datetime) >= '".$start_date."' ";

        }
        if(isset($data['end_date']) && $data['end_date']!='')
        {
            $end_date = $data['end_date'];
            $end_date =date('Y-m-d',strtotime($end_date));
            $where .= " and DATE(H.history_datetime) <= '".$end_date."' ";

        }
        if(isset($data['org_id']))
        {
            $where .= " and O.organisation_id='".$data['org_id']."'";
        }

        $query=$this->db->query("select O.organisation_name,H.history_datetime,U.user_fname,U.user_lname,H.history_latitude,H.history_longitude,H.history_location from in_user_login_history H
            inner join in_user U on U.user_id = H.history_userid
            inner join in_organisation O on O.organisation_id= U.user_organisationid

            where  H.history_location!='undefined' $where order by H.history_datetime DESC
        ");
        return $query->result();
    }
    public function delete_enrollment($id)
    {
        $this->db->where('reg_id', $id);
        $this->db->delete('in_user_events');
    }
    public function update_password($data=array())
    {
        $org_id = 0;
        $new_password='';

        if(isset($data['user_id']) && $data['user_id']!='')
        {
            $org_id = $data['user_id'];

        }
        if(isset($data['old_password']) && $data['old_password']!='')
        {
            $old_password = md5($data['old_password']);

        }
        if(isset($data['password']) && $data['password']!='')
        {
            $new_password = md5($data['password']);

        }
        $query = $this->db->query("select admin_id from in_admin where admin_password=? and admin_id=?",array($old_password,$org_id));
        $total= $query->num_rows();
        if($total==1 && $new_password!='')
        {
            $data2['admin_password']=$new_password;
            $this->db->where('admin_id', $org_id);
            $this->db->update('in_admin', $data2);
            return true;
        }else
        {
            return false;
        }

    }



    //================================== This method displays All the Artists ==================================== 
    public function show_artists(){
        $this->db->select("*");
        $this->db->from('artist');
        $this->db->join('users','artist.user_id = users.user_id');
        $query = $this->db->get();
        return $query->result();
    }



    //================================== This method changes the button status =================================== 
    public function btnstatus($id,$is_active){

        $this->db->where('user_id', $id);
        $this->db->update('users',['is_active'=>$is_active]);
    }

    //=================================== This method displays All the Venues =====================================
    public function show_venues(){
        $this->db->select('*');
        $this->db->from('venues');
        $this->db->join('users','venues.user_id = users.user_id');
        $query = $this->db->get();
        return $query->result();
    }
    //=================================== This method selects settings data ========================================
    public function settings(){
        $this->db->select("*");
        $this->db->from("settings");
        $query =$this->db->get();

        return $query->result();
    }
    //=================================== This method updates the settings ========================================
    public function update_settings(){
            $id = $this->input->post('id');
            $paypal = $this->input->post('paypal');
            $paypal = $this->input->post('paypal_email');
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $perpage = $this->input->post('perpage');

    //dd($email);
            $data = array(

                'paypal'  => $paypal,
                'name'   => $name,
                'email'   => $email,
                'perpage' =>$perpage
                );
            $this->db->where('id',$this->input->post('id'));
            return $this->db->update('settings',$data);

    }
}
