<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('includes/head');?>
    <style>
    .spacer_medium {
     margin: 0 0 90px 0;
     }
    </style>
    <!-- for Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo site_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url(); ?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo site_url(); ?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo site_url('assets/favicon/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">
</head>

<body class="no-body-padd contact_bg">
<p id="alert"></p>
<header class="main-header">
        <!-- Static navbar -->
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>


            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <img src="<?php echo site_url(); ?>assets/images/logo-n.png" alt="Logo">
            </a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="search">
                  <!-- search form -->
                  <form id="search" action="<?php echo site_url('venue/search'); ?>" method="post">
                     <input type="search" name="search" placeholder="Search" autocomplete="off">
                  </form>
                  <!-- search form ends-->
              </li>
              <li><a href="<?php echo site_url(); ?>" style="color:#fff;">Artists</a></li>
              <li><a href="<?php echo site_url('venueBooking/booking'); ?>" style="color:#fff;">My bookings</a></li>
              <li><a href="<?php echo site_url('venueBooking/inbox'); ?>" style="color:#fff;">Inbox <span id="my_bookings" class="badge badge_upper inbox-count"></span></a></li>
              <li><a href="<?php echo site_url('venueBooking/my_requests'); ?>" style="color:#fff;">Requests <span id="my_bookings" class="badge badge_upper request-count"></span></a></li>
              <li class="favourite"><a href="<?php echo site_url("venue/favourties"); ?>" class="heart" style="color:#fff;"></a></li>
              <li>
                <div class="inset dropdown"> <?php if($image!=""){$img = site_url()."uploads/users/thumb/".$image; }else{$img = site_url()."assets/blank.png";} ?>                  
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img class="profile_img" src="<?php echo $img; ?>"> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url('venueProfile'); ?>">Profile</a></li>
                  <li><a href="<?php echo site_url('account/logout'); ?>">Logout</a></li>
                </ul>
                </div>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container -->
      </nav>

    </header>


    <!-- space just for index -->

    <div class="spacer_medium"></div>
    <!-- space just for index ends-->

     <div class="container">
        <h1 class="text-center text-white spacer">Contact Us</h1>

        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <!-- form -->
        
        <form id="contact-form" action="<?php echo site_url("venue/contact"); ?>" method="post">
          <div class="form-group">
          <?php if($this->session->flashdata('success')){ ?>
         <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
         </div>
        <?php } ?>
          </div>
          <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span>
                </span>
                <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name" />
              </div>
              <?php echo form_error('name','<p style="color:#fff;">','</p>'); ?>
              <span id="error-name"></span>
          </div><!-- form-group -->
          <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                </span>
                <input type="text" name="email" class="form-control" id="email" placeholder="Enter email" />
              </div>
              <?php echo form_error('email','<p style="color:#fff;">','</p>'); ?>
              <span id="error-email"></span>
          </div><!-- form-group -->
          <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon v-top"><span class="glyphicon glyphicon-edit"></span>
                </span>
                <textarea class="form-control" name="message" id="message" placeholder="Enter Message"></textarea>
              </div>
              <?php echo form_error('message','<p style="color:#fff;">','</p>'); ?>
              <span id="error-message"></span>
          </div><!-- form-group -->
          <div class="form-group">
            <input type="submit" name="submit" value="Send" class="btn btn-primary btn-block">
          </div>
        </form>
        <!-- form ends -->
          </div>
        </div>
</div><




    
<div class="spacer_hundred"></div>
<footer class="main-footer" style="position:fixed;bottom:0;left:0;right:0;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-left">
                    <p class="copyright">© <?php echo date('Y'); ?> Showspoon. All rights reserved.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                    <ul class="legallinks text-right">
                        <a href="<?php echo site_url('venue/privacy_policy'); ?>">Privacy Policy</a><span class="sep "> |</span>
                        <a href="<?php echo site_url('venue/terms'); ?>">Terms of Use </a><span class="sep "> |</span>
                        <a href="<?php echo site_url('venue/contact'); ?>">Contact Us</a>
                    </ul>
                </div>
            </div>
            <!-- row -->
        </div>
    </footer>
<?php //$this->load->view('includes/footer');?>

   <style>
     .red{border:1px solid #cc0000;}
   </style> 


    
    <script type="text/javascript">
        $(document).ready(function(){

        $('#contact-form').submit(function(e){

          var name = $('#name').val();
          var email = $('#email').val();
          var message = $('#message').val();
          if(name==''){
            $('#name').addClass('red');
            $('#error-name').text('This field is required.').css('color','#cc0000');
          }

          if(email==''){
            $('#email').addClass('red');
            $('#error-email').text('This field is required.').css('color','#cc0000');
          }

          if(!validateEmail(email)){
          $('#error-email').text('Enter valid email address.');
          $('#email').addClass('red');
         }

          if(message==''){
            $('#message').addClass('red');
            $('#error-message').text('This field is required.').css('color','#cc0000');
          }
          if(name!='' && email!='' && message!='' && validateEmail(email)){
            $(this).submit();
          }
          e.preventDefault();
          });
        $.ajax({
          url:'<?php echo site_url("venue/getHeartCount"); ?>',
          type:'POST',
          success:function(data){
            setTimeout(function(){
            $('li.favourite a.heart').text(data);
            },500);
          }
        });
        function getArtistRequestCount(){
          $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistRequestCount"); ?>',
                        type    :  'POST',
                        dataType:  'JSON',
                        success :  function(data){

                          if(data.status=='request' && data.cnt > 0 && data.is_read==0){
                            $('.request-count').html(data.cnt);
                          }
                        }
                   });
       }
       getArtistRequestCount();
        // var clear = setInterval(function(){
        // getArtistRequestCount(); 
        // },100);
       function getArtistInboxCount(){
        $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistInboxCount"); ?>',
                        type    :  'POST',
                        dataType:  'JSON',
                        success :  function(data){ 

                          if(data.status=='message' && data.cnt > 0 && data.is_read==0){
                            $('.inbox-count').html(data.cnt);
                          }
                        }
                   });
      }
      getArtistInboxCount();
       function getArtistNotification(){
         $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistNotification"); ?>',
                        type    :  'POST',
                        success :  function(data){

                            $('#alert').html(data);
                        }
                   });
      } getArtistNotification();
      $(document).on('click','.close',function(){
        var id = $(this).data('id');
        var mr = $(this).data('mr');
        $.ajax({
                        url     :  '<?php echo site_url("venueBooking/deleteArtistNotification"); ?>',
                        type    :  'POST',
                        data    :  {id:id},
                        success :  function(data){
                          if(mr=='request'){
                            $('.request-count').text('');
                          }else if(mr=='message'){
                            $('.inbox-count').text('');
                          }
                        }
                   });
      });
        });

        $('#name').bind('keyup , change',function(){

             $('#error-name').text('');
             $('#name').removeClass('red');
        });

        $('#email').bind('keyup , change',function(){

             $('#error-email').text('');
             $('#email').removeClass('red');
        });

        $('#message').bind('keyup , change',function(){

             $('#error-message').text('');
             $('#message').removeClass('red');
        });
        $('#email').bind('keyup , change',function(){
             var email = $(this).val();
             if(!validateEmail(email)){
              $('#error-email').text('Enter valid email address.');
              $('#email').addClass('red');
             }
        });
        function validateEmail(email){
          var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
          return emailReg.test( email );
        }

        $(document).on("scroll", function(){
        if
          ($(document).scrollTop() > 20){
              $(".main-header").addClass("shrink");
            }
            else
            {
                $(".main-header").removeClass("shrink");
            }
        });
    </script>
    
</body>

</html>