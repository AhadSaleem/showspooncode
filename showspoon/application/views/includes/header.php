<?php 
$user_favourites = isset($user_favourites)?$user_favourites:[];
$total_favourites = count($user_favourites);
if(isset($this->session->userdata('showspoon_artist')['user_id']))
{
    $search = 'Search Venue';

}else{
    $search = 'Search Artist';
}
?>


<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url();?>">
                <img src="<?php echo base_url();?>/assets/dist/images/logo.png" class="img-responsive" />
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right vertical-align">
                <?php 
                if(isset($this->session->userdata('showspoon_artist')['user_id']))
                {
                ?>
                <li><a href="<?php echo site_url('artist/search');?>">Search Venue</a></li>
                <li><a href="<?php echo site_url('artistBooking/booking');?>">My Bookings <span id="my_bookings" class="badge badge_upper hidden">5</span></a></li>
                <li><a href="<?php echo site_url('artistBooking/my_requests');?>">My Requests <span id="my_requests" class="badge badge_upper">5</span></a></li>
                <li><a href="<?php echo site_url('artist/favourites');?>" class="heart"><?php echo $total_favourites;?></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle prbtn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Profile <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo site_url('artistProfile');?>">Profile</a></li>

                        <li><a href="<?php echo site_url('account/logout');?>">Logout</a></li>
                    </ul>
                </li>

               <script>
                function Request_Notification(){
                     $.ajax({
                        type:'POST',
                        url: '<?php echo site_url('artist/get_artist_notifications');?>',
                        data:{user_id:<?php echo $this->session->userdata('showspoon_artist')['user_id'];?>},
                        dataType:'json',
                        success:function(data){
                           /* if(data.data){
                                delete_btn.closest('.artist_gallery').remove();
                            }*/

                        },
                        error: function(data){

                        }
                    });
                }
            $(document).ready(function(){
                
            });
            </script>
                <?php }else{?>
                <li><a href="<?php echo site_url('venue/search');?>">Search Artists</a></li>
                <li><a href="<?php echo site_url('venueBooking/booking');?>">My Bookings</a></li>
                <li><a href="<?php echo site_url('venueBooking/my_requests');?>">My Requests</a></li>
                <li><a href="<?php echo site_url('venue/favourites');?>" class="heart"><?php echo $total_favourites;?></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle prbtn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Profile <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo site_url('venueProfile');?>">Profile</a></li>

                        <li><a href="<?php echo site_url('account/logout');?>">Logout</a></li>
                    </ul>
                </li>
                <?php }?>
            </ul>
            <form class="navbar-form navbar-right">
                <div class="form-group">
                    <input type="text" name="quick_search" class="form-control" placeholder="Quick Search" />
                </div>
            </form>
            
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>


