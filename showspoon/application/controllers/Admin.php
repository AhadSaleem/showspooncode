<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class admin extends CI_Controller {

    private $loggedintime;
    private $admin_data;

    function __construct() {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
        parent::__construct();
        $this->load->model('admin_model');
        $this->data['title'] = 'admin';
        $this->data['page_title'] = "admin";
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        if (!$this->session->userdata('ShowspoonAdmin_logged_in')) {
            $this->session->set_flashdata('message', '');
            redirect(site_url('admin_login'));
        }
        //$this->admin_data = admin_data();
        $this->admin_data = $this->session->userdata('ShowspoonAdmin_logged_in');
        
        
    }

    public function index() {

        $this->data['title'] = 'Dashboard - Showspoon';
        //$this->data['user_login_history'] = $this->admin_model->get_login_history(6,0);
       /* $this->data['record_history'] = $this->admin_model->get_record_limit(6, 0);
        $this->data['sms_sent'] = $this->admin_model->get_sent_limit(6,0);

        $this->data['user_history'] = $this->admin_model->get_user_count_hisotry();

        //$this->data['user_history'] = $this->admin_model->get_users_history();


        $this->data['lc_history_month'] = $this->admin_model->get_user_login_hisotry_month();

        $this->data['user_history_month'] = $this->admin_model->get_users_history_month();
        $query = $this->db->query('select * from in_report order by report_id desc limit 1');
        $res = $query->row();
        $this->data['history_data'] = $res;
        $this->data['total_pages'] = $this->admin_model->get_total_pages();
        $this->data['total_users'] = $this->admin_model->get_total_users();

        $this->data['admin_data'] = $this->admin_data;*/
        
        $this->load->view('admin/dashboard', $this->data);
    }


    public function settings() {

            $this->data['title'] = 'Settings - Showspoon';
            $settings  = $this->admin_model->settings();
            $this->form_validation->set_rules('paypal','Paypal','required');
            $this->form_validation->set_rules('paypal_email','Paypal','required');
            $this->form_validation->set_rules('name','Name','required');
            $this->form_validation->set_rules('email','Email','required');
            $this->form_validation->set_rules('perpage','Perpage','required');
            if($this->form_validation->run()===TRUE){
                
                $this->admin_model->update_settings();
                $this->session->set_flashdata('updated','The settings has been updated');
                redirect("admin/settings");
              
            }
            
            $this->load->view('admin/settings',['settings'=>$settings]); 
    
    }

   public function update_settings(){
    

    //$this->admin_model->update_settings();
    
           // redirect("admin/settings");

   }

    public function venues(){
        $this->data['title'] = 'Venues - Showspoon';
        $venues =$this->admin_model->show_venues();
        
        $this->load->view('admin/venues',['venues'=>$venues]);
    }

    public function artists(){
        $this->data['title'] = 'Artists - Showspoon';
        $artist= $this->admin_model->show_artists();
        $this->load->view('admin/artists', ['artist'=>$artist]);
    }


    public function btnstatus1($id,$is_active){
        if ($is_active==1) {

            echo $is_active = 0;
        }
        else if($is_active == 0){
           echo  $is_active = 1;
          //redirect('admin/venues');  
        }
        $this->admin_model->btnstatus($id,$is_active);
        redirect('admin/artists');
    }





    public function login_history() {
        $start_date = $this->input->get('sd') ? $this->input->get('sd') : '';
        $end_date = $this->input->get('ed') ? $this->input->get('ed') : '';
        $org_id = $this->input->get('org_id') ? $this->input->get('org_id') : 0;

        $result = $this->admin_model->user_login_history();


        $this->data['results'] = $result;



        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/reports/login_history', $this->data);
    }

    public function report() {
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';
        $date = $this->input->get('date') ? $this->input->get('date', true) : date('Y-m-d', time());
        $type = $this->input->get('type') ? $this->input->get('type', true) : '';
        $delete = $this->input->get('delete') ? $this->input->get('delete', true) : 0;

        /*if($type!=''){
            $query = $this->db->query("select D.*,W.names from tbl_data D
        left join tbl_webs W on W.web_id = D.source
        where  source=? and DATE(`time_ago`)=? order by id desc", array($type, $date));
        }else{
            $query = $this->db->query("select D.*,W.names from tbl_data D
            left join tbl_webs W on W.web_id = D.source
            where phone!='' and  DATE(`time_ago`)=? order by id desc", array( $date));
        }
        $results = $query->result();*/
        $results=array();
        $this->data['query'] = $results;
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/viewusers', $this->data);
    }
    public function full_report() {
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';
        $date = $this->input->get('date') ? $this->input->get('date', true) : date('Y-m-d', time());
        $type = $this->input->get('type') ? $this->input->get('type', true) : '';
        $delete = $this->input->get('delete') ? $this->input->get('delete', true) : 0;

        /*if($type!=''){
            $query = $this->db->query("select D.*,W.names from tbl_data D
        left join tbl_webs W on W.web_id = D.source
        where  source=? and DATE(`time_ago`)=? order by id desc", array($type, $date));
        }else{
            $query = $this->db->query("select D.*,W.names from tbl_data D
            left join tbl_webs W on W.web_id = D.source
            where phone!='' and  DATE(`time_ago`)=? order by id desc", array( $date));
        }
        $results = $query->result();*/
        $results=array();
        $this->data['query'] = $results;
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/full_report', $this->data);
    }
    public function rec_total()
    {
        $web_id = $this->input->get('web_id')?$this->input->get('web_id'):0;
        $query = $this->db->query("select count(id) as total from tbl_data where source = $web_id and is_active=1 and is_complete=1");
        $row = $query->row();
        $total = isset($row->total)?$row->total:0;
        array_map('unlink', glob("upload/reports/csvfile_".$web_id.'*'));
        echo json_encode(array('total'=>$total));

    }
    public function load_by_limit()
    {
        $web_id = $this->input->get('web_id')?$this->input->get('web_id'):0;
        $counter = $this->input->get('counter')?$this->input->get('counter'):0;
        $offset = $this->input->get('offset')?$this->input->get('offset'):0;
        $limit = $this->input->get('limit')?$this->input->get('limit'):0;
        //'Listing ID',
        $th = array(
            'Ad_ID', 'Link', 'Title', 'Make',
            'Model', 'Machine Capacity', 'Variant',
            'Year', 'Mileage', 'Transmission',
            'Fuel', 'Color', 'Price','Features',
            'Description', 'Name', 'Phone', 'Email',
            'Province', 'City', 'Date_Scraped', 'Date_Posted', 'Seller_Type', 'Exact_Address', 'Status'

        );
        $fields = array('ad_id','ad_link','title','make','model','machine_capacity','variant','year','mileage','transmission','fuel','color','amount','features','description','owner','phone','email','province','city','created_datetime','time_ago','seller_type','address','is_active');


   
        $query = "select * from tbl_data where source='".(int)$web_id."' and is_active=1 and is_complete=1   order by id desc limit ".(int)$offset.",".(int)$limit;//
        $query = $this->db->query($query);


        $eoddata = $query->result();
        $this->db->last_query();

        $i=0;
        //$csv = implode(',',$th).' \n';


        $csv_handler = fopen ('upload/reports/csvfile_'.$web_id.$counter.'.csv','w');
        if($counter==0){
            fputcsv($csv_handler, $th);
        }
        foreach ($eoddata as $row) {
            $row_ar = (array)$row;


            $data=array();
            //$data[]=$i;
            foreach($fields as $fl)
            {
                if($fl=='created_datetime')
                {
                    $data[]=isset($row_ar[$fl])?date('Y-m-d',strtotime($row_ar[$fl])):'';
                }else
                {
                    $data[]=isset($row_ar[$fl])?$row_ar[$fl]:'';    
                }


            }


            fputcsv($csv_handler,$data);
           
            $i++;
        }



        fclose ($csv_handler);

        echo json_encode(array('data'=>true));
    }
    public function final_report()
    {
        $name = $this->input->get('name')?$this->input->get('name'):'OLX';
        $web_id = $this->input->get('web_id')?$this->input->get('web_id'):0;
        
        $this->load->helper('download');
        $glob = glob('upload/reports/csvfile_'.$web_id.'*.csv');
        $files = array();

        foreach($glob as $file)
        {
            $files[]= $file;
        }
        $this->joinFiles($files,'upload/'.$name.'.csv');

        //$path    =   file_get_contents("upload/final.csv");
        
        //$name .='.csv';
        ///force_download($name, $path);    
        echo json_encode(array('data'=>true,'path'=>site_url('upload/'.$name.'.csv')));

    }
    private function joinFiles(array $files, $result) {
        if(!is_array($files)) {
            throw new Exception('`$files` must be an array');
        }

        $wH = fopen($result, "w+");

        foreach($files as $file) {
            $fh = fopen($file, "r");
            while(!feof($fh)) {
                fwrite($wH, fgets($fh));
            }
            fclose($fh);
            unset($fh);
            fwrite($wH, "\n"); //usually last line doesn't have a newline
        }
        fclose($wH);
        unset($wH);
    }
    public function cron_report() {
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';
        $date = $this->input->get('date') ? $this->input->get('date', true) : date('Y-m-d', time());
        $type = $this->input->get('type') ? $this->input->get('type', true) : '';
        $delete = $this->input->get('delete') ? $this->input->get('delete', true) : 0;


        $query = $this->db->query("select * from tbl_crons  order by id desc");

        $results = $query->result();
        $this->data['query'] = $results;
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/cron_report', $this->data);
    }
    public function cron_report_details($id=0) {
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';



        $query = $this->db->query("select D.*,W.names from tbl_cron_details D 
        inner join tbl_webs W on W.web_id = D.web_id
        where D.cron_id=?  order by D.detail_id asc",array($id));

        $results = $query->result();
        $this->data['query'] = $results;
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/cron_report_details', $this->data);
    }

    public function setupsms() {
        if ($this->input->post()) {
            if ($this->input->post('content') && $this->input->post('content') != ' ') {
                $record = array(
                    'sms' => $this->input->post('content'),
                );
                $this->data['success'] = 'Setup SMS updated successfully ';
                $this->db->where('id', 1);
                $this->db->update('setupsms', $record);
                $this->session->set_flashdata('message', 'Setup SMS updated successfully');
                redirect('admin/setupsms');
            } else {
                $this->session->set_flashdata('message_error', 'Please a enter message');
                redirect('admin/setupsms');
            }
        } else {
            $query = $this->db->query("SELECT * FROM `setupsms`");
            $results = $query->row();
            $this->data['query'] = $results;
            $this->load->view('admin/setupsms', $this->data);
        }
    }

    public function import() {
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';

        if (isset($_FILES['csv']['name']) && $_FILES['csv']['name'] != '') {
            $pages_data = array();
            $query = $this->db->query("select page_id,title,page_code from in_pages order by page_id asc");
            $pages = $query->result();
            $total_pages = $query->num_rows();
            if ($total_pages > 0) {
                foreach ($pages as $row) {
                    $page_title = strtolower($row->title);
                    $pages_data[$page_title] = array(
                        'page_id' => $row->page_id,
                        'page_code' => $row->page_code,
                    );
                }
            }

            /* echo '<pre>';
              print_r($pages_data);
              echo '</pre>'; */

            $row = 1;
            if (($handle = fopen($_FILES['csv']['tmp_name'], "r")) !== FALSE) {

                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    $num = count($data);
                    //echo "<p> $num fields in line $row: <br /></p>\n";
                    if ($row > 1) {
                        $arr = array(
                            'email' => isset($data[0]) ? $data[0] : '',
                            'company' => isset($data[1]) ? $data[1] : '',
                            'page' => isset($data[2]) ? $data[2] : '',
                            'send_mail' => isset($data[3]) ? $data[3] : '',
                        );

                        $page = isset($arr['page']) ? strtolower($arr['page']) : '';
                        $email = isset($arr['email']) ? strtolower($arr['email']) : '';
                        $send_email = isset($arr['send_mail']) ? strtolower($arr['send_mail']) : 'No';
                        $page_id = isset($pages_data[$page]['page_id']) ? $pages_data[$page]['page_id'] : 0;
                        $page_code = isset($pages_data[$page]['page_code']) ? $pages_data[$page]['page_code'] : '';





                        if ($email != '' && $page_id > 0) {
                            $query = $this->db->query("select user_id,user_email from in_user where user_email=? limit 1", array($email));
                            $res = $query->row();


                            if (isset($res->user_id)) {
                                $record = array(
                                    'company' => isset($arr['company']) ? $arr['company'] : '',
                                    'p_id' => $page_id,
                                    'user_delete' => 0,
                                );
                                $this->db->where('user_id', $res->user_id);
                                $this->db->update('in_user', $record);
                            } else {
                                $record = array(
                                    'user_email' => $email,
                                    'company' => isset($arr['company']) ? $arr['company'] : '',
                                    'p_id' => $page_id,
                                );





                                $lst_id = mt_rand(1000, 100000);
                                $record['login_code'] = md5($lst_id);
                                $record['user_active'] = 1;
                                $record['created_datetime'] = date("Y-m-d H:i:s", time());
                                $this->db->insert('in_user', $record);
                                $insert_id = $this->db->insert_id();

                                $email_body = '';
                                if ($send_email == 'yes') {

                                    //$link =site_url('register/'.$page_code);
                                    $link = base_url();
                                    //$first_name=isset($record['user_fname'])?$record['user_fname']:'';
                                    //$last_name=isset($record['user_lname'])?$record['user_lname']:'';
                                    //$this->data['full_name']=$first_name.' '.$last_name;
                                    $this->data['login_link'] = $link;
                                    $email_body = $this->load->view('email/welcome', $this->data, true);

                                    include_once APPPATH . 'sendgrid-php/vendor/autoload.php';
                                    $sendgrid = new SendGrid("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");
                                    $emailbody = new SendGrid\Email();
                                    $emailbody->addTo($email)
                                        ->setFrom("info@xoomworks.com")
                                        ->setFromName("Xoomworks Procurement")
                                        ->setSubject('Registration')
                                        ->setHtml($email_body);
                                    // $result = $sendgrid->send($emailbody);
                                }
                            }
                        }
                    }
                    $row++;
                }
                fclose($handle);
                $this->session->set_flashdata('success', 'Users has been successfully imported.');
                redirect('admin/import');
            }
        }

        $this->data['query'] = $this->admin_model->get_users();
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/import_csv', $this->data);
    }

    public function add_user() {

        $this->data['admin_data'] = $this->admin_data;
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';
        $query = $this->db->query("select user_id from in_user order by user_id desc limit 1");
        $last_row = $query->row();
        if (isset($last_row->user_id) && $last_row->user_id > 0) {
            $lst_id = $this->data['last_id'] = $last_row->user_id + 1;
        } else {
            $lst_id = $this->data['last_id'] = 1;
        }
        if ($this->input->post('user_email')) {
            $email = $this->input->post('user_email');
            $res = $this->admin_model->get_user_by_email($email);
            if (!isset($res->user_id)) {
                $code = mt_rand(100000, 10000000);
                $valid_arr = array('user_fname', 'user_lname', 'user_email', 'user_password', 'company', 'p_id');
                $record = array();
                foreach ($this->input->post() as $key => $val) {
                    if (in_array($key, $valid_arr)) {
                        if ($key == 'user_password') {
                            $record[$key] = md5($val);
                        } else {
                            $record[$key] = $val;
                        }
                    }
                }
                if (count($record) > 0) {

                    //$record['login_code']=md5($code);
                    $record['login_code'] = md5($lst_id);
                    $record['user_active'] = ($this->input->post('user_active') == 1) ? 1 : 0;
                    $record['created_datetime'] = date("Y-m-d H:i:s", time());
                    $this->db->insert('in_user', $record);
                    $insert_id = $this->db->insert_id();

                    $email_body = '';
                    if ($this->input->post('send_mail') == 1) {
                        //$link=site_url('login/user_login/'.$insert_id.'/'.md5($code));
                        //$link=site_url('login/user_login/'.$insert_id.'/'.md5($lst_id));
                        //$link =$this->input->post('email_link');
                        $link = base_url();
                        $first_name = isset($record['user_fname']) ? $record['user_fname'] : '';
                        $last_name = isset($record['user_lname']) ? $record['user_lname'] : '';
                        $this->data['full_name'] = $first_name . ' ' . $last_name;
                        $this->data['login_link'] = $link;
                        $email_body = $this->load->view('email/welcome', $this->data, true);

                        include_once APPPATH . 'sendgrid-php/vendor/autoload.php';
                        $sendgrid = new SendGrid("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");
                        $emailbody = new SendGrid\Email();
                        $emailbody->addTo($email)
                            ->setFrom("info@xoomworks.com")
                            ->setFromName("Xoomworks Procurement")
                            ->setSubject('Registration')
                            ->setHtml($email_body);
                        $result = $sendgrid->send($emailbody);
                    }

                    $this->session->set_flashdata('success', 'New user has been successfully created.');
                }

                redirect('admin/users');
            } else {
                $this->data['error'] = 'This email already exists.';
            }
        }
        $this->load->view('admin/add_user', $this->data);
    }

    public function edit_user($id = 0) {

        $result = $this->data['row'] = $this->admin_model->get_user_by_id($id);
        if (!isset($result->user_id)) {
            show_404();
        }
        $this->data['admin_data'] = $this->admin_data;
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';
        if ($this->input->post('user_email')) {
            $email = $this->input->post('user_email');
            $res = $this->admin_model->get_user_by_email($email);

            if (!isset($res->user_id) || (isset($res->user_id) && $res->user_email == $result->user_email)) {
                $valid_arr = array('user_fname', 'user_lname', 'user_email', 'user_password', 'company', 'p_id');
                $record = array();
                foreach ($this->input->post() as $key => $val) {
                    if (in_array($key, $valid_arr) && $val != '') {
                        if ($key == 'user_password' && $val != '') {
                            $record[$key] = md5($val);
                        } else {
                            $record[$key] = $val;
                        }
                    }
                }
                if (count($record) > 0) {


                    $record['user_active'] = ($this->input->post('user_active') == 1) ? 1 : 0;
                    $record['created_datetime'] = date("Y-m-d H:i:s", time());
                    $this->db->where('user_id', $result->user_id);
                    $this->db->update('in_user', $record);

                    $this->session->set_flashdata('success', 'User has been successfully updated.');
                }

                redirect(site_url('admin/users'));
            } else {
                $this->data['error'] = 'This email already exists.';
            }
        }
        $this->load->view('admin/edit_user', $this->data);
    }

    public function pages() {
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';
        $delete = $this->input->get('delete') ? $this->input->get('delete', true) : 0;
        if ($delete > 0) {
            $this->db->where('page_id', $delete);
            $this->db->update('in_pages', array('is_delete' => 1));

            $this->session->set_flashdata('success', 'Page has been successfully deleted.');
            redirect(site_url('admin/pages'));
        }
        $this->data['query'] = $this->admin_model->get_pages();
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/pages', $this->data);
    }

    public function add_page() {


        $this->data['admin_data'] = $this->admin_data;
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';
        if ($this->input->post('title')) {
            /* echo '<pre>';
              print_r($_FILES);
              print_r($this->input->post());
              die(); */
            $fileName_logo = '';
            if (isset($_FILES['logo']['name']) && $_FILES['logo']['name'] != '') {

                $string = preg_replace('/\s+/', '', $_FILES ['logo']['name']);
                $log_name = time() . "_" . mt_rand() . "_" . $string;
                $fileName_logo = $log_name;

                $upload_dir = site_url("upload/admin_logos/");


                move_uploaded_file($_FILES ['logo']["tmp_name"], ("upload/admin_logos") . "/" . ($fileName_logo));
            }
            $left_image = '';
            if (isset($_FILES['left_image']['name']) && $_FILES['left_image']['name'] != '') {

                $string = preg_replace('/\s+/', '', $_FILES ['left_image']['name']);
                $log_name = time() . "_" . mt_rand() . "_" . $string;
                $left_image = $log_name;

                $upload_dir = site_url("upload/admin_logos/");


                move_uploaded_file($_FILES ['left_image']["tmp_name"], ("upload/admin_logos") . "/" . ($left_image));
            }

            $page_slg = $this->input->post('slug');
            if ($page_slg != '') {
                $page_slg = create_slug($page_slg);
            } else {
                $page_slg = create_slug($this->input->post('title'));
            }
            $page_slg = $this->validate_slug($page_slg);

            $record = array(
                'title' => $this->input->post('title'),
                'slug' => $page_slg,
                'description' => $this->input->post('description'),
                'logo' => $fileName_logo,
                'left_image' => $left_image,
                'lb_news' => $this->input->post('news'),
                'lb_why' => $this->input->post('why_lb'),
                'lb_when' => $this->input->post('when'),
                'lb_how' => $this->input->post('how'),
                'lb_help' => $this->input->post('help'),
                'lb_home' => $this->input->post('home'),
                'lb_events' => $this->input->post('events'),
                'lb_welcome' => $this->input->post('welcome'),
                'lb_resources' => $this->input->post('resources'),
                'lb_readmore' => $this->input->post('readmore'),
                'lb_myaccount' => $this->input->post('myaccount'),
                'lb_changepassword' => $this->input->post('ChangePassword'),
                'lb_currentpassword' => $this->input->post('CurrentPassword'),
                'lb_newpasword' => $this->input->post('NewPassword'),
                'lb_updatepassword' => $this->input->post('UpdatePassword'),
                'lb_repeatpassword' => $this->input->post('RepeatPassword'),
                'lb_logout' => $this->input->post('Logout'),
            );

            if (count($record) > 0) {

                $record['show_news'] = ($this->input->post('show_news') == 1) ? 1 : 0;
                $code = mt_rand(10000, 1000000);
                $record['page_code'] = $code;
                $record['created_datetime'] = date("Y-m-d H:i:s", time());
                $this->db->insert('in_pages', $record);
                $insert_id = $this->db->insert_id();
                $why_record = array();
                $why = $this->input->post('why');
                foreach ($why as $val) {
                    $why_record[] = array(
                        'p_id' => $insert_id,
                        'why_description' => $val,
                        'why_datetime' => date('Y-m-d H:i:s', time())
                    );
                }
                if (count($why_record) > 0) {
                    $this->db->insert_batch('in_why', $why_record);
                }


                $when_record = array();
                $when_title = $this->input->post('when_title');
                $when_text = $this->input->post('when_text');
                $when_date = $this->input->post('when_date');
                $total_titles = count($when_title);
                for ($i = 0; $i < $total_titles; $i++) {
                    $when_record[] = array(
                        'p_id' => $insert_id,
                        'when_text' => isset($when_text[$i]) ? $when_text[$i] : '',
                        'when_description' => isset($when_title[$i]) ? $when_title[$i] : '',
                        'when_date' => isset($when_date[$i]) ? $when_date[$i] : '',
                        'when_datetime' => date('Y-m-d H:i:s', time())
                    );
                }
                if (count($when_record) > 0) {
                    $this->db->insert_batch('in_when', $when_record);
                }


                $help_record = array();
                $help_title = $this->input->post('help_title');
                $help_url = $this->input->post('help_url');
                $total_helps = count($help_title);
                for ($i = 0; $i < $total_helps; $i++) {
                    $help_record[] = array(
                        'p_id' => $insert_id,
                        'help_description' => isset($help_title[$i]) ? $help_title[$i] : '',
                        'help_url' => isset($help_url[$i]) ? $help_url[$i] : '',
                        'help_datetime' => date('Y-m-d H:i:s', time())
                    );
                }
                if (count($help_record) > 0) {
                    $this->db->insert_batch('in_help', $help_record);
                }


                $how_record = array();
                $how_title = $this->input->post('how_title');
                $how_file_name = $this->input->post('how_file_name');
                $how_thumb_name = $this->input->post('how_thumb_name');
                $how_url = $this->input->post('how_url');
                $total_titles = count($how_title);

                foreach ($how_title as $key => $val) {
                    $thumb_name = $how_type = '';
                    $how_url_single = isset($how_url[$key]) ? $how_url[$key] : '';

                    if ($how_url_single != '') {
                        $how_type = 'url';
                    } else {
                        $fileName = '';

                        $how_url_single = isset($how_file_name[$key]) ? $how_file_name[$key] : '';
                        $thumb_name = isset($how_thumb_name[$key]) ? $how_thumb_name[$key] : '';

                        $how_type = 'link';
                    }
                    $how_record[] = array(
                        'p_id' => $insert_id,
                        'how_description' => $val,
                        'how_url' => $how_url_single,
                        'how_datetime' => date('Y-m-d H:i:s', time()),
                        'how_type' => $how_type,
                        'how_thumb' => $thumb_name,
                    );
                }
                if (count($how_record) > 0) {
                    $this->db->insert_batch('in_how', $how_record);
                }


                $this->session->set_flashdata('success', 'New page has been successfully created.');
            }

            redirect(site_url('admin/pages'));
        } else {
            $this->db->query("delete from in_temp");
        }
        $this->load->view('admin/add_page', $this->data);
    }

    public function add_page_fr() {


        $this->data['admin_data'] = $this->admin_data;
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';
        if ($this->input->post('title')) {
            /* echo '<pre>';
              print_r($_FILES);
              print_r($this->input->post());
              die(); */
            $fileName_logo = '';
            if (isset($_FILES['logo']['name']) && $_FILES['logo']['name'] != '') {

                $string = preg_replace('/\s+/', '', $_FILES ['logo']['name']);
                $log_name = time() . "_" . mt_rand() . "_" . $string;
                $fileName_logo = $log_name;

                $upload_dir = site_url("upload/admin_logos/");


                move_uploaded_file($_FILES ['logo']["tmp_name"], ("upload/admin_logos") . "/" . ($fileName_logo));
            }
            $left_image = '';
            if (isset($_FILES['left_image']['name']) && $_FILES['left_image']['name'] != '') {

                $string = preg_replace('/\s+/', '', $_FILES ['left_image']['name']);
                $log_name = time() . "_" . mt_rand() . "_" . $string;
                $left_image = $log_name;

                $upload_dir = site_url("upload/admin_logos/");


                move_uploaded_file($_FILES ['left_image']["tmp_name"], ("upload/admin_logos") . "/" . ($left_image));
            }

            $page_slg = $this->input->post('slug');
            if ($page_slg != '') {
                $page_slg = create_slug($page_slg);
            } else {
                $page_slg = create_slug($this->input->post('title'));
            }
            $page_slg = $this->validate_slug($page_slg);

            $record = array(
                'fr_title' => $this->input->post('title'),
                'slug' => $page_slg,
                'fr_description' => $this->input->post('description'),
                'fr_logo' => $fileName_logo,
                'fr_left_image' => $left_image,
                'lb_news_fr' => $this->input->post('news'),
                'lb_why_fr' => $this->input->post('why_lb'),
                'lb_when_fr' => $this->input->post('when'),
                'lb_how_fr' => $this->input->post('how'),
                'lb_help_fr' => $this->input->post('help'),
                'lb_home_fr' => $this->input->post('home'),
                'lb_welcome_fr' => $this->input->post('welcome'),
                'lb_events_fr' => $this->input->post('events'),
                'lb_resources_fr' => $this->input->post('resources'),
                'lb_readmore_fr' => $this->input->post('readmore'),
                'lb_myaccount_fr' => $this->input->post('myaccount'),
                'lb_changepassword_fr' => $this->input->post('ChangePassword'),
                'lb_currentpassword_fr' => $this->input->post('CurrentPassword'),
                'lb_newpasword_fr' => $this->input->post('NewPassword'),
                'lb_updatepassword_fr' => $this->input->post('UpdatePassword'),
                'lb_repeatpassworfr' => $this->input->post('RepeatPassword'),
                'lb_logout_fr' => $this->input->post('Logout'),
            );

            if (count($record) > 0) {

                $record['show_news'] = ($this->input->post('show_news') == 1) ? 1 : 0;
                $record['created_datetime'] = date("Y-m-d H:i:s", time());
                $this->db->insert('in_pages', $record);
                $insert_id = $this->db->insert_id();
                $why_record = array();
                $why = $this->input->post('why');
                foreach ($why as $val) {
                    $why_record[] = array(
                        'p_id' => $insert_id,
                        'why_description' => $val,
                        'why_datetime' => date('Y-m-d H:i:s', time()),
                        'language' => 'fr'
                    );
                }
                if (count($why_record) > 0) {
                    $this->db->insert_batch('in_why', $why_record);
                }


                $when_record = array();
                $when_title = $this->input->post('when_title');
                $when_text = $this->input->post('when_text');
                $when_date = $this->input->post('when_date');
                $total_titles = count($when_title);
                for ($i = 0; $i < $total_titles; $i++) {
                    $when_record[] = array(
                        'p_id' => $insert_id,
                        'when_text' => isset($when_text[$i]) ? $when_text[$i] : '',
                        'when_description' => isset($when_title[$i]) ? $when_title[$i] : '',
                        'when_date' => isset($when_date[$i]) ? $when_date[$i] : '',
                        'when_datetime' => date('Y-m-d H:i:s', time()),
                        'language' => 'fr'
                    );
                }
                if (count($when_record) > 0) {
                    $this->db->insert_batch('in_when', $when_record);
                }


                $help_record = array();
                $help_title = $this->input->post('help_title');
                $help_url = $this->input->post('help_url');
                $total_helps = count($help_title);
                for ($i = 0; $i < $total_helps; $i++) {
                    $help_record[] = array(
                        'p_id' => $insert_id,
                        'help_description' => isset($help_title[$i]) ? $help_title[$i] : '',
                        'help_url' => isset($help_url[$i]) ? $help_url[$i] : '',
                        'help_datetime' => date('Y-m-d H:i:s', time()),
                        'language' => 'fr'
                    );
                }
                if (count($help_record) > 0) {
                    $this->db->insert_batch('in_help', $help_record);
                }


                $how_record = array();
                $how_title = $this->input->post('how_title');
                $how_file_name = $this->input->post('how_file_name');
                $how_thumb_name = $this->input->post('how_thumb_name');
                $how_url = $this->input->post('how_url');
                $total_titles = count($how_title);

                foreach ($how_title as $key => $val) {
                    $thumb_name = $how_type = '';
                    $how_url_single = isset($how_url[$key]) ? $how_url[$key] : '';

                    if ($how_url_single != '') {
                        $how_type = 'url';
                    } else {
                        $fileName = '';

                        $how_url_single = isset($how_file_name[$key]) ? $how_file_name[$key] : '';
                        $thumb_name = isset($how_thumb_name[$key]) ? $how_thumb_name[$key] : '';

                        $how_type = 'link';
                    }
                    $how_record[] = array(
                        'p_id' => $insert_id,
                        'how_description' => $val,
                        'how_url' => $how_url_single,
                        'how_datetime' => date('Y-m-d H:i:s', time()),
                        'how_type' => $how_type,
                        'how_thumb' => $thumb_name,
                        'language' => 'fr'
                    );
                }
                if (count($how_record) > 0) {
                    $this->db->insert_batch('in_how', $how_record);
                }


                $this->session->set_flashdata('success', 'New page has been successfully created.');
            }

            redirect(site_url('admin/pages'));
        } else {
            $this->db->query("delete from in_temp");
        }
        $this->load->view('admin/add_page_fr', $this->data);
    }

    public function edit_page($id = 0) {

        $this->data['admin_data'] = $this->admin_data;
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';
        $page_data = $this->data['page_data'] = $this->admin_model->get_page($id);
        if (!isset($page_data->page_id)) {
            show_404();
        }
        $lg = 'en';
        $this->data['hows'] = $this->admin_model->get_how($lg, $id);
        $this->data['whens'] = $this->admin_model->get_when($lg, $id);
        $this->data['whys'] = $this->admin_model->get_why($lg, $id);
        $this->data['helps'] = $this->admin_model->get_help($lg, $id);

        if ($this->input->post('title')) {
            /* echo '<pre>';
              print_r($_FILES);
              print_r($this->input->post());
              die(); */

            $fileName_logo = '';
            if (isset($_FILES['logo']['name']) && $_FILES['logo']['name'] != '') {

                $string = preg_replace('/\s+/', '', $_FILES ['logo']['name']);
                $log_name = time() . "_" . mt_rand() . "_" . $string;
                $fileName_logo = $log_name;

                $upload_dir = site_url("upload/admin_logos/");

                move_uploaded_file($_FILES ['logo']["tmp_name"], ("upload/admin_logos") . "/" . ($fileName_logo));
            } else {
                $fileName_logo = $this->input->post('hidden_logo');
            }
            $left_image = '';
            if (isset($_FILES['left_image']['name']) && $_FILES['left_image']['name'] != '') {

                $string = preg_replace('/\s+/', '', $_FILES ['left_image']['name']);
                $log_name = time() . "_" . mt_rand() . "_" . $string;
                $left_image = $log_name;

                $upload_dir = site_url("upload/admin_logos/");


                move_uploaded_file($_FILES ['left_image']["tmp_name"], ("upload/admin_logos") . "/" . ($left_image));
            } else {

                $left_image = $this->input->post('hidden_left_image');
            }


            $page_slg = $this->input->post('slug');
            if ($page_slg != '') {
                $page_slg = create_slug($page_slg);
            } else {
                $page_slg = create_slug($this->input->post('title'));
            }
            $page_slg = htmlentities($page_slg);
            //$page_slg = $this->validate_slug($page_slg);

            $record = array(
                'title' => $this->input->post('title'),
                'slug' => $page_slg,
                'description' => $this->input->post('description'),
                'logo' => $fileName_logo,
                'left_image' => $left_image,
                'lb_news' => $this->input->post('news'),
                'lb_why' => $this->input->post('why_lb'),
                'lb_when' => $this->input->post('when'),
                'lb_how' => $this->input->post('how'),
                'lb_help' => $this->input->post('help'),
                'lb_home' => $this->input->post('home'),
                'lb_welcome' => $this->input->post('welcome'),
                'lb_events' => $this->input->post('events'),
                'lb_resources' => $this->input->post('resources'),
                'lb_readmore' => $this->input->post('readmore'),
                'lb_myaccount' => $this->input->post('myaccount'),
                'lb_changepassword' => $this->input->post('ChangePassword'),
                'lb_currentpassword' => $this->input->post('CurrentPassword'),
                'lb_newpasword' => $this->input->post('NewPassword'),
                'lb_updatepassword' => $this->input->post('UpdatePassword'),
                'lb_repeatpassword' => $this->input->post('RepeatPassword'),
                'lb_logout' => $this->input->post('Logout'),
            );


            if (count($record) > 0) {

                /* $sql = "UPDATE in_pages SET title = ?, description = ?, logo =?, left_image=? WHERE id = ?";
                  $this->db->query($sql, $record); */
                //$record['created_datetime']=date("Y-m-d H:i:s",time());
                $record['show_news'] = ($this->input->post('show_news') == 1) ? 1 : 0;
                $this->db->where('page_id', $id);
                $this->db->update('in_pages', $record);
                $insert_id = $id;


                $this->db->where(array('language' => $lg, 'p_id' => $id));
                $this->db->delete('in_why');
                $why_record = array();
                $why = $this->input->post('why');
                foreach ($why as $val) {
                    $why_record[] = array(
                        'p_id' => $insert_id,
                        'why_description' => $val,
                        'why_datetime' => date('Y-m-d H:i:s', time())
                    );
                }
                if (count($why_record) > 0) {
                    $this->db->insert_batch('in_why', $why_record);
                }


                $this->db->where(array('language' => $lg, 'p_id' => $id));
                $this->db->delete('in_when');
                $when_record = array();
                $when_title = $this->input->post('when_title');
                $when_text = $this->input->post('when_text');
                $when_date = $this->input->post('when_date');
                $total_titles = count($when_title);
                for ($i = 0; $i < $total_titles; $i++) {
                    $when_record[] = array(
                        'p_id' => $insert_id,
                        'when_text' => isset($when_text[$i]) ? $when_text[$i] : '',
                        'when_description' => isset($when_title[$i]) ? $when_title[$i] : '',
                        'when_date' => isset($when_date[$i]) ? $when_date[$i] : '',
                        'when_datetime' => date('Y-m-d H:i:s', time())
                    );
                }
                if (count($when_record) > 0) {
                    $this->db->insert_batch('in_when', $when_record);
                }


                $this->db->where(array('language' => $lg, 'p_id' => $id));
                $this->db->delete('in_help');
                $help_record = array();
                $help_title = $this->input->post('help_title');
                $help_url = $this->input->post('help_url');
                $total_helps = count($help_title);
                for ($i = 0; $i < $total_helps; $i++) {
                    $help_record[] = array(
                        'p_id' => $insert_id,
                        'help_description' => isset($help_title[$i]) ? $help_title[$i] : '',
                        'help_url' => isset($help_url[$i]) ? $help_url[$i] : '',
                        'help_datetime' => date('Y-m-d H:i:s', time())
                    );
                }
                if (count($help_record) > 0) {
                    $this->db->insert_batch('in_help', $help_record);
                }


                $this->db->where(array('language' => $lg, 'p_id' => $id));
                $this->db->delete('in_how');
                $how_record = array();
                $how_title = $this->input->post('how_title');
                $how_file_name = $this->input->post('how_file_name');
                $how_thumb_name = $this->input->post('how_thumb_name');
                $how_url = $this->input->post('how_url');
                $hidden_how = $this->input->post('hidden_how');
                $total_titles = count($how_title);

                foreach ($how_title as $key => $val) {
                    $how_type = '';
                    $how_url_single = isset($how_url[$key]) ? $how_url[$key] : '';


                    if ($how_url_single != '') {
                        $how_url_single = $how_url_single;
                        $how_type = 'url';
                    } else {
                        $fileName = '';

                        $how_url_single = isset($how_file_name[$key]) ? $how_file_name[$key] : '';
                        $thumb_name = isset($how_thumb_name[$key]) ? $how_thumb_name[$key] : '';



                        $how_type = 'link';
                    }
                    $how_record[] = array(
                        'p_id' => $insert_id,
                        'how_description' => $val,
                        'how_url' => $how_url_single,
                        'how_datetime' => date('Y-m-d H:i:s', time()),
                        'how_type' => $how_type,
                        'how_thumb' => $thumb_name,
                    );
                }
                if (count($how_record) > 0) {
                    $this->db->insert_batch('in_how', $how_record);
                }


                $this->session->set_flashdata('success', 'New page has been successfully created.');
            }

            redirect(site_url('admin/pages'));
        }
        $this->load->view('admin/edit_page', $this->data);
    }

    public function edit_page_fr($id = 0) {

        $this->data['admin_data'] = $this->admin_data;
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';
        $page_data = $this->data['page_data'] = $this->admin_model->get_page($id);
        // print_r($page_data);exit;
        if (!isset($page_data->page_id)) {
            show_404();
        }
        $lg = 'fr';
        $this->data['hows'] = $this->admin_model->get_how($lg, $id);
        $this->data['whens'] = $this->admin_model->get_when($lg, $id);
        $this->data['whys'] = $this->admin_model->get_why($lg, $id);
        $this->data['helps'] = $this->admin_model->get_help($lg, $id);

        if ($this->input->post('title')) {
            /* echo '<pre>';
              print_r($_FILES);
              print_r($this->input->post());
              die(); */

            $fileName_logo = '';
            if (isset($_FILES['logo']['name']) && $_FILES['logo']['name'] != '') {

                $string = preg_replace('/\s+/', '', $_FILES ['logo']['name']);
                $log_name = time() . "_" . mt_rand() . "_" . $string;
                $fileName_logo = $log_name;

                $upload_dir = site_url("upload/admin_logos/");

                move_uploaded_file($_FILES ['logo']["tmp_name"], ("upload/admin_logos") . "/" . ($fileName_logo));
            } else {
                $fileName_logo = $this->input->post('hidden_logo');
            }
            $left_image = '';
            if (isset($_FILES['left_image']['name']) && $_FILES['left_image']['name'] != '') {

                $string = preg_replace('/\s+/', '', $_FILES ['left_image']['name']);
                $log_name = time() . "_" . mt_rand() . "_" . $string;
                $left_image = $log_name;

                $upload_dir = site_url("upload/admin_logos/");


                move_uploaded_file($_FILES ['left_image']["tmp_name"], ("upload/admin_logos") . "/" . ($left_image));
            } else {

                $left_image = $this->input->post('hidden_left_image');
            }


            $page_slg = $this->input->post('slug');
            if ($page_slg != '') {
                $page_slg = create_slug($page_slg);
            } else {
                $page_slg = create_slug($this->input->post('title'));
            }
            $page_slg = htmlentities($page_slg);
            //$page_slg = $this->validate_slug($page_slg);

            $record = array(
                'fr_title' => $this->input->post('title'),
                'slug' => $page_slg,
                'fr_description' => $this->input->post('description'),
                'fr_logo' => $fileName_logo,
                'fr_left_image' => $left_image,
                'lb_news_fr' => $this->input->post('news'),
                'lb_why_fr' => $this->input->post('why_lb'),
                'lb_when_fr' => $this->input->post('when'),
                'lb_how_fr' => $this->input->post('how'),
                'lb_help_fr' => $this->input->post('help'),
                'lb_home_fr' => $this->input->post('home'),
                'lb_welcome_fr' => $this->input->post('welcome'),
                'lb_events_fr' => $this->input->post('events'),
                'lb_resources_fr' => $this->input->post('resources'),
                'lb_readmore_fr' => $this->input->post('readmore'),
                'lb_myaccount_fr' => $this->input->post('myaccount'),
                'lb_changepassword_fr' => $this->input->post('ChangePassword'),
                'lb_currentpassword_fr' => $this->input->post('CurrentPassword'),
                'lb_newpasword_fr' => $this->input->post('NewPassword'),
                'lb_updatepassword_fr' => $this->input->post('UpdatePassword'),
                'lb_repeatpassworfr' => $this->input->post('RepeatPassword'),
                'lb_logout_fr' => $this->input->post('Logout'),
            );


            if (count($record) > 0) {

                /* $sql = "UPDATE in_pages SET title = ?, description = ?, logo =?, left_image=? WHERE id = ?";
                  $this->db->query($sql, $record); */
                //$record['created_datetime']=date("Y-m-d H:i:s",time());
                $record['show_news'] = ($this->input->post('show_news') == 1) ? 1 : 0;
                $this->db->where('page_id', $id);
                $this->db->update('in_pages', $record);
                $insert_id = $id;


                $this->db->where(array('language' => $lg, 'p_id' => $id));
                $this->db->delete('in_why');
                $why_record = array();
                $why = $this->input->post('why');
                foreach ($why as $val) {
                    $why_record[] = array(
                        'p_id' => $insert_id,
                        'why_description' => $val,
                        'why_datetime' => date('Y-m-d H:i:s', time()),
                        'language' => 'fr'
                    );
                }
                if (count($why_record) > 0) {
                    $this->db->insert_batch('in_why', $why_record);
                }


                $this->db->where(array('language' => $lg, 'p_id' => $id));
                $this->db->delete('in_when');
                $when_record = array();
                $when_title = $this->input->post('when_title');
                $when_text = $this->input->post('when_text');
                $when_date = $this->input->post('when_date');
                $total_titles = count($when_title);
                for ($i = 0; $i < $total_titles; $i++) {
                    $when_record[] = array(
                        'p_id' => $insert_id,
                        'when_text' => isset($when_text[$i]) ? $when_text[$i] : '',
                        'when_description' => isset($when_title[$i]) ? $when_title[$i] : '',
                        'when_date' => isset($when_date[$i]) ? $when_date[$i] : '',
                        'when_datetime' => date('Y-m-d H:i:s', time()),
                        'language' => 'fr'
                    );
                }
                if (count($when_record) > 0) {
                    $this->db->insert_batch('in_when', $when_record);
                }


                $this->db->where(array('language' => $lg, 'p_id' => $id));
                $this->db->delete('in_help');
                $help_record = array();
                $help_title = $this->input->post('help_title');
                $help_url = $this->input->post('help_url');
                $total_helps = count($help_title);
                for ($i = 0; $i < $total_helps; $i++) {
                    $help_record[] = array(
                        'p_id' => $insert_id,
                        'help_description' => isset($help_title[$i]) ? $help_title[$i] : '',
                        'help_url' => isset($help_url[$i]) ? $help_url[$i] : '',
                        'help_datetime' => date('Y-m-d H:i:s', time()),
                        'language' => 'fr'
                    );
                }
                if (count($help_record) > 0) {
                    $this->db->insert_batch('in_help', $help_record);
                }


                $this->db->where(array('language' => $lg, 'p_id' => $id));
                $this->db->delete('in_how');
                $how_record = array();
                $how_title = $this->input->post('how_title');
                $how_file_name = $this->input->post('how_file_name');
                $how_thumb_name = $this->input->post('how_thumb_name');
                $how_url = $this->input->post('how_url');
                $hidden_how = $this->input->post('hidden_how');
                $total_titles = count($how_title);

                foreach ($how_title as $key => $val) {
                    $how_type = '';
                    $how_url_single = isset($how_url[$key]) ? $how_url[$key] : '';


                    if ($how_url_single != '') {
                        $how_url_single = $how_url_single;
                        $how_type = 'url';
                    } else {
                        $fileName = '';

                        $how_url_single = isset($how_file_name[$key]) ? $how_file_name[$key] : '';
                        $thumb_name = isset($how_thumb_name[$key]) ? $how_thumb_name[$key] : '';



                        $how_type = 'link';
                    }
                    $how_record[] = array(
                        'p_id' => $insert_id,
                        'how_description' => $val,
                        'how_url' => $how_url_single,
                        'how_datetime' => date('Y-m-d H:i:s', time()),
                        'how_type' => $how_type,
                        'how_thumb' => $thumb_name,
                        'language' => 'fr'
                    );
                }
                if (count($how_record) > 0) {
                    $this->db->insert_batch('in_how', $how_record);
                }


                $this->session->set_flashdata('success', 'New page has been successfully created.');
            }

            redirect(site_url('admin/pages'));
        }
        $this->load->view('admin/edit_page_fr', $this->data);
    }

    private function validate_slug($slug = '') {
        $res = $this->admin_model->validate_slug($slug);
        if (isset($res->page_id)) {
            $new_slug = $res->slug . '-1';
            return $this->validate_slug($new_slug);
        } else {
            return $slug;
        }
    }

    public function news() {
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';
        $delete = $this->input->get('delete') ? $this->input->get('delete', true) : 0;
        if ($delete > 0) {
            $this->db->where('news_id', $delete);
            $this->db->update('in_news', array('is_delete' => 1));

            $this->session->set_flashdata('success', 'News has been successfully deleted.');
            redirect(site_url('admin/news'));
        }
        $this->data['query'] = $this->admin_model->get_news();
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/news', $this->data);
    }

    public function add_news() {

        $this->data['admin_data'] = $this->admin_data;
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';
        if ($this->input->post('news_title')) {


            $valid_arr = array('news_title', 'news_description', 'news_date', 'news_excerpt');
            $record = array();
            foreach ($this->input->post() as $key => $val) {
                if (in_array($key, $valid_arr)) {
                    if ($key == 'user_password') {
                        $record[$key] = md5($val);
                    } else {
                        $record[$key] = $val;
                    }
                }
            }
            if (count($record) > 0) {
                $fileName_logo = '';
                if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {

                    $string = preg_replace('/\s+/', '', $_FILES ['image']['name']);
                    $log_name = time() . "_" . mt_rand() . "_" . $string;
                    $fileName_logo = $log_name;

                    $upload_dir = site_url("upload/news/");


                    move_uploaded_file($_FILES ['image']["tmp_name"], ("upload/news") . "/" . ($fileName_logo));
                    copy('./upload/news/' . $fileName_logo, './upload/news/thumb/' . $fileName_logo);
                    $record['news_image'] = $fileName_logo;
                    $this->load->library('image_lib');

                    $config['image_library'] = 'gd2';
                    $config['source_image'] = 'upload/news/thumb/' . $fileName_logo;

                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 150;
                    $config['height'] = 150;

                    $this->image_lib->clear();
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();
                }



                $record['is_active'] = ($this->input->post('is_active') == 1) ? 1 : 0;
                $record['created_datetime'] = date("Y-m-d H:i:s", time());
                $this->db->insert('in_news', $record);
                $insert_id = $this->db->insert_id();

                $fileName_attachment = '';
                if (isset($_FILES['attachment']['name']) && count($_FILES['attachment']['name']) > 0) {
                    //$total_attachs = count($_FILES['attachment']['name']);
                    $total_attachs = $this->input->post('total_attachments') ? $this->input->post('total_attachments') : 0;
                    $attach_text = $this->input->post('attach_text') ? $this->input->post('attach_text') : array();
                    for ($x = 0; $x < $total_attachs; $x++) {
                        if (isset($_FILES['attachment']['name'][$x]) && $_FILES['attachment']['name'][$x] != '') {
                            $att = array();

                            $string = preg_replace('/\s+/', '', $_FILES ['attachment']['name'][$x]);
                            $log_name = time() . "_" . mt_rand() . "_" . $string;
                            $fileName_attachment = $log_name;

                            $upload_dir = site_url("upload/news/");


                            move_uploaded_file($_FILES ['attachment']["tmp_name"][$x], ("upload/news") . "/" . ($fileName_attachment));

                            $att['attach_text'] = isset($attach_text[$x]) ? $attach_text[$x] : '';
                            $att['attach_title'] = $fileName_attachment;
                            $att['lang'] = 'en';
                            $att['n_id'] = $insert_id;
                            if (count($att) > 0) {
                                $this->db->insert('in_attachments', $att);
                            }
                        }
                    }
                }

                $this->session->set_flashdata('success', 'News has been successfully added.');
            }

            redirect(site_url('admin/news'));
        }
        $this->load->view('admin/add_news', $this->data);
    }

    public function add_news_fr() {

        $this->data['admin_data'] = $this->admin_data;
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';
        if ($this->input->post('fr_news_title')) {


            $valid_arr = array('fr_news_title', 'fr_news_description', 'news_date', 'news_excerpt');
            $record = array();
            foreach ($this->input->post() as $key => $val) {
                if (in_array($key, $valid_arr)) {
                    if ($key == 'user_password') {
                        $record[$key] = md5($val);
                    } else {
                        $record[$key] = $val;
                    }
                }
            }
            if (count($record) > 0) {
                $fileName_logo = '';
                if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {

                    $string = preg_replace('/\s+/', '', $_FILES ['image']['name']);
                    $log_name = time() . "_" . mt_rand() . "_" . $string;
                    $fileName_logo = $log_name;

                    $upload_dir = site_url("upload/news/");


                    move_uploaded_file($_FILES ['image']["tmp_name"], ("upload/news") . "/" . ($fileName_logo));
                    copy('./upload/news/' . $fileName_logo, './upload/news/thumb/' . $fileName_logo);
                    $record['fr_news_image'] = $fileName_logo;
                    $this->load->library('image_lib');

                    $config['image_library'] = 'gd2';
                    $config['source_image'] = 'upload/news/thumb/' . $fileName_logo;

                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 150;
                    $config['height'] = 150;

                    $this->image_lib->clear();
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();
                }


                $record['is_active'] = ($this->input->post('is_active') == 1) ? 1 : 0;
                $record['created_datetime'] = date("Y-m-d H:i:s", time());
                $this->db->insert('in_news', $record);
                $insert_id = $this->db->insert_id();
                $fileName_attachment = '';
                if (isset($_FILES['attachment']['name']) && count($_FILES['attachment']['name']) > 0) {
                    //$total_attachs = count($_FILES['attachment']['name']);
                    $total_attachs = $this->input->post('total_attachments') ? $this->input->post('total_attachments') : 0;
                    $attach_text = $this->input->post('attach_text') ? $this->input->post('attach_text') : array();
                    for ($x = 0; $x < $total_attachs; $x++) {
                        if (isset($_FILES['attachment']['name'][$x]) && $_FILES['attachment']['name'][$x] != '') {
                            $att = array();

                            $string = preg_replace('/\s+/', '', $_FILES ['attachment']['name'][$x]);
                            $log_name = time() . "_" . mt_rand() . "_" . $string;
                            $fileName_attachment = $log_name;

                            $upload_dir = site_url("upload/news/");


                            move_uploaded_file($_FILES ['attachment']["tmp_name"][$x], ("upload/news") . "/" . ($fileName_attachment));

                            $att['attach_text'] = isset($attach_text[$x]) ? $attach_text[$x] : '';
                            $att['attach_title'] = $fileName_attachment;
                            $att['lang'] = 'fr';
                            $att['n_id'] = $insert_id;
                            if (count($att) > 0) {
                                $this->db->insert('in_attachments', $att);
                            }
                        }
                    }
                }
                $this->session->set_flashdata('success', 'News has been successfully added.');
            }

            redirect(site_url('admin/news'));
        }
        $this->load->view('admin/add_news_fr', $this->data);
    }

    public function edit_news($id = 0) {

        $this->data['admin_data'] = $this->admin_data;
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';
        $this->data['row'] = $res = $this->admin_model->get_news_by_id($id);
        if (!isset($res->news_id)) {
            show_404();
        }
        if ($this->input->post('news_title')) {

            /* echo '<pre>';
              print_r($this->input->post());
              print_r($_FILES);
              die(); */
            $valid_arr = array('news_title', 'news_description', 'news_date', 'news_excerpt');
            $record = array();
            foreach ($this->input->post() as $key => $val) {
                if (in_array($key, $valid_arr)) {
                    if ($key == 'user_password') {
                        $record[$key] = md5($val);
                    } else {
                        $record[$key] = $val;
                    }
                }
            }
            if (count($record) > 0) {
                $fileName_logo = '';
                if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {

                    $string = preg_replace('/\s+/', '', $_FILES ['image']['name']);
                    $log_name = time() . "_" . mt_rand() . "_" . $string;
                    $fileName_logo = $log_name;

                    $upload_dir = site_url("upload/news/");


                    move_uploaded_file($_FILES ['image']["tmp_name"], ("upload/news") . "/" . ($fileName_logo));
                    copy('./upload/news/' . $fileName_logo, './upload/news/thumb/' . $fileName_logo);
                    $record['news_image'] = $fileName_logo;
                    $this->load->library('image_lib');

                    $config['image_library'] = 'gd2';
                    $config['source_image'] = 'upload/news/thumb/' . $fileName_logo;

                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 150;
                    $config['height'] = 150;

                    $this->image_lib->clear();
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();

                    if (isset($res->news_image) && $res->news_image != '') {
                        $old_image_path = 'upload/news/' . $res->news_image;
                        $old_image_path_thumb = 'upload/news/thumb/' . $res->news_image;
                        @unlink($old_image_path);
                        @unlink($old_image_path_thumb);
                    }
                }
                $fileName_attachment = '';
                /* $record['attachment']='';
                  if(isset($_FILES['attachment']['name']) && $_FILES['attachment']['name']!='')
                  {

                  $string = preg_replace('/\s+/', '', $_FILES ['attachment']['name']);
                  $log_name = time() . "_" . mt_rand() . "_" . $string;
                  $fileName_attachment = $log_name;

                  $upload_dir = site_url("upload/news/");


                  move_uploaded_file($_FILES ['attachment']["tmp_name"], ("upload/news") . "/" . ($fileName_attachment));

                  $record['attachment']=$fileName_attachment;
                  if(isset($res->attachment) && $res->attachment!='')
                  {
                  $old_image_path = 'upload/news/'.$res->attachment;
                  @unlink($old_image_path);
                  }

                  }elseif($this->input->post('old_attachment'))
                  {
                  if($this->input->post('old_attachment')!='')
                  {
                  $record['attachment']=$this->input->post('old_attachment');
                  }
                  } */

                $fileName_attachment = '';
                $this->db->where(array('n_id' => $id, 'lang' => 'en'));
                $this->db->delete('in_attachments');
                if (isset($_FILES['attachment']['name']) && count($_FILES['attachment']['name']) > 0) {
                    $old_files = $this->input->post('old_attachments') ? $this->input->post('old_attachments') : array();
                    $total_attachments = $this->input->post('total_attachments') ? $this->input->post('total_attachments') : 0;
                    $attach_text = $this->input->post('attach_text') ? $this->input->post('attach_text') : array();

                    for ($x = 0; $x < $total_attachments; $x++) {
                        if (isset($_FILES['attachment']['name'][$x]) && $_FILES['attachment']['name'][$x] != '') {
                            $att = array();

                            $string = preg_replace('/\s+/', '', $_FILES ['attachment']['name'][$x]);
                            $log_name = time() . "_" . mt_rand() . "_" . $string;
                            $fileName_attachment = $log_name;

                            $upload_dir = site_url("upload/news/");


                            move_uploaded_file($_FILES ['attachment']["tmp_name"][$x], ("upload/news") . "/" . ($fileName_attachment));
                            $att['attach_text'] = isset($attach_text[$x]) ? $attach_text[$x] : '';
                            $att['attach_title'] = $fileName_attachment;
                            $att['lang'] = 'en';
                            $att['n_id'] = $id;
                            if (count($att) > 0) {
                                $this->db->insert('in_attachments', $att);
                            }
                        } else if (isset($old_files[$x]) && $old_files[$x] != '') {
                            $att = array();
                            $att['attach_text'] = isset($attach_text[$x]) ? $attach_text[$x] : '';
                            $att['attach_title'] = $old_files[$x];
                            $att['lang'] = 'en';
                            $att['n_id'] = $id;
                            if (count($att) > 0) {
                                $this->db->insert('in_attachments', $att);
                            }
                        }
                    }
                }

                $record['is_active'] = ($this->input->post('is_active') == 1) ? 1 : 0;
                //$record['created_datetime']=date("Y-m-d H:i:s",time());
                $this->db->where('news_id', $id);
                $this->db->update('in_news', $record);

                $this->session->set_flashdata('success', 'News has been successfully updated.');
            }

            redirect(site_url('admin/news'));
        }
        $this->load->view('admin/edit_news', $this->data);
    }

    public function edit_news_fr($id = 0) {

        $this->data['admin_data'] = $this->admin_data;
        $this->data['success'] = $this->session->flashdata('success') ? $this->session->flashdata('success') : '';
        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : '';
        $this->data['row'] = $res = $this->admin_model->get_news_by_id($id);
        if (!isset($res->news_id)) {
            show_404();
        }
        if ($this->input->post('fr_news_title')) {


            $valid_arr = array('fr_news_title', 'fr_news_description', 'news_date', 'fr_news_excerpt');
            $record = array();
            foreach ($this->input->post() as $key => $val) {
                if (in_array($key, $valid_arr)) {
                    if ($key == 'user_password') {
                        $record[$key] = md5($val);
                    } else {
                        $record[$key] = $val;
                    }
                }
            }
            if (count($record) > 0) {
                $fileName_logo = '';
                if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {

                    $string = preg_replace('/\s+/', '', $_FILES ['image']['name']);
                    $log_name = time() . "_" . mt_rand() . "_" . $string;
                    $fileName_logo = $log_name;

                    $upload_dir = site_url("upload/news/");


                    move_uploaded_file($_FILES ['image']["tmp_name"], ("upload/news") . "/" . ($fileName_logo));
                    copy('./upload/news/' . $fileName_logo, './upload/news/thumb/' . $fileName_logo);
                    $record['fr_news_image'] = $fileName_logo;
                    $this->load->library('image_lib');

                    $config['image_library'] = 'gd2';
                    $config['source_image'] = 'upload/news/thumb/' . $fileName_logo;

                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 150;
                    $config['height'] = 150;

                    $this->image_lib->clear();
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();

                    if (isset($res->fr_news_image) && $res->fr_news_image != '') {
                        $old_image_path = 'upload/news/' . $res->fr_news_image;
                        $old_image_path_thumb = 'upload/news/thumb/' . $res->fr_news_image;
                        @unlink($old_image_path);
                        @unlink($old_image_path_thumb);
                    }
                }

                $fileName_attachment = '';
                $this->db->where(array('n_id' => $id, 'lang' => 'fr'));
                $this->db->delete('in_attachments');
                if (isset($_FILES['attachment']['name']) && count($_FILES['attachment']['name']) > 0) {
                    $old_files = $this->input->post('old_attachments') ? $this->input->post('old_attachments') : array();
                    $total_attachments = $this->input->post('total_attachments') ? $this->input->post('total_attachments') : 0;
                    $attach_text = $this->input->post('attach_text') ? $this->input->post('attach_text') : array();

                    for ($x = 0; $x < $total_attachments; $x++) {
                        if (isset($_FILES['attachment']['name'][$x]) && $_FILES['attachment']['name'][$x] != '') {
                            $att = array();

                            $string = preg_replace('/\s+/', '', $_FILES ['attachment']['name'][$x]);
                            $log_name = time() . "_" . mt_rand() . "_" . $string;
                            $fileName_attachment = $log_name;

                            $upload_dir = site_url("upload/news/");


                            move_uploaded_file($_FILES ['attachment']["tmp_name"][$x], ("upload/news") . "/" . ($fileName_attachment));

                            $att['attach_text'] = isset($attach_text[$x]) ? $attach_text[$x] : '';
                            $att['attach_title'] = $fileName_attachment;
                            $att['lang'] = 'fr';
                            $att['n_id'] = $id;
                            if (count($att) > 0) {
                                $this->db->insert('in_attachments', $att);
                            }
                        } else if (isset($old_files[$x]) && $old_files[$x] != '') {
                            $att = array();
                            $att['attach_text'] = isset($attach_text[$x]) ? $attach_text[$x] : '';
                            $att['attach_title'] = $old_files[$x];
                            $att['lang'] = 'fr';
                            $att['n_id'] = $id;
                            if (count($att) > 0) {
                                $this->db->insert('in_attachments', $att);
                            }
                        }
                    }
                }


                $record['is_active'] = ($this->input->post('is_active') == 1) ? 1 : 0;
                // print_r($record);exit;
                //$record['created_datetime']=date("Y-m-d H:i:s",time());
                $this->db->where('news_id', $id);
                $this->db->update('in_news', $record);

                $this->session->set_flashdata('success', 'News has been successfully updated.');
            }

            redirect(site_url('admin/news'));
        }
        $this->load->view('admin/edit_news_fr', $this->data);
    }

    public function programme_stat() {


        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/stats/programme_stat', $this->data);
    }

    public function content_stat() {

        $this->data['admin_data'] = $this->admin_data;

        $this->load->view('admin/stats/content_stat', $this->data);
    }

    public function live_stat() {
        //date_default_timezone_set('Asia/Karachi');
        //date_default_timezone_set('UTC');

        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/stats/live_stat', $this->data);
    }

    public function demographics() {

        $this->data['admin_data'] = $this->admin_data;
        $this->data['map_data'] = $this->admin_model->map_data();
        $this->load->view('admin/stats/demographics', $this->data);
    }

    public function organisations() {
        $this->data['admin_data'] = $this->admin_data;
        $this->data['query'] = $this->admin_model->get_all_organization();
        $this->load->view('admin/organization/organization', $this->data);
    }

    public function addorganisation() {
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/organization/addorganization');
    }

    public function setorganization() {
        //echo '<pre>';
        //print_r($this->input->post());
        echo '<meta charset="utf-8" />';
        $inputArray = $this->input->post('lang_rus');
        //$inputArray = array_map("utf8_encode", $inputArray );
        //$inputArray = array_map("utf8_decode", $inputArray );
        //print_r($inputArray);


        $this->admin_model->insert_organization();
        //die();
        $this->session->set_flashdata('message', 'Organisation added successfully');
        redirect(site_url('admin/organisations'));
    }

    public function editorganisation() {

        $editdata = $this->admin_model->get_organization_byid($this->uri->segment(3));
        if (sizeof($editdata) > 0) {
            $this->data['query'] = $editdata;
            $this->data['admin_data'] = $this->admin_data;
            $this->load->view('admin/organization/editorganization', $this->data);
        } else {
            redirect(site_url('admin/organisations'));
        }
    }

    public function updateorganisation() {
        $this->admin_model->update_organization();
        $this->session->set_flashdata('message', 'Organisation updated successfully');
        redirect(site_url('admin/organisations'));
    }

    public function loginhistory() {
        $this->data['query'] = $this->admin_model->get_loginhistory($this->uri->segment(3));
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/vewuserloginhistory', $this->data);
    }

    public function userdetails() {
        if ($this->input->post('user_programme_programmeid')) {

            $this->admin_model->update_user_details();
            $this->data['success'] = 'User updated successfully';
        }
        $this->data['query'] = $this->admin_model->get_users_details($this->uri->segment(3));
        $this->data['user_programme'] = $this->admin_model->get_users_programme($this->uri->segment(3));
        $this->data['admin_data'] = $this->admin_data;
        $this->data['programmes'] = $this->admin_model->get_programmes();
        $this->load->view('admin/viewuserdetails', $this->data);
    }

    public function programme() {
        $delete_id = $this->input->get('delete') ? $this->input->get('delete') : 0;
        if ($delete_id > 0) {
            $this->admin_model->delete_program($delete_id);
            $this->session->set_flashdata('message', 'Programme deleted successfully');
            redirect(site_url('admin/programme'));
        }
        $this->data['query'] = $this->admin_model->viewprogramme();
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/programme/viewprogramme', $this->data);
    }

    public function likes_report() {
        $start_date = $this->input->get('sd') ? $this->input->get('sd') : '';
        $end_date = $this->input->get('ed') ? $this->input->get('ed') : '';
        $user_id = $this->input->get('user_id') ? $this->input->get('user_id') : 0;
        $org_id = $this->input->get('org_id') ? $this->input->get('org_id') : 0;
        if ($start_date != '' || $end_date != '' || $user_id > 0 || $org_id > 0) {
            $filter = array(
                'start_date' => $start_date,
                'end_date' => $end_date,
                'user_id' => $user_id,
                'org_id' => $org_id,
            );
            $result = $this->admin_model->likes_report($filter);
        } else {
            $filter = array(
                'start_date' => date('Y-m-d', time()),
            );
            $result = $this->admin_model->likes_report($filter);
        }
        $this->data['results'] = $result;


        $this->data['query'] = $this->admin_model->viewprogramme();
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/reports/likes', $this->data);
    }

    public function activity_report() {
        $start_date = $this->input->get('sd') ? $this->input->get('sd') : '';
        $end_date = $this->input->get('ed') ? $this->input->get('ed') : '';
        $user_id = $this->input->get('user_id') ? $this->input->get('user_id') : '';
        $org_id = $this->input->get('org_id') ? $this->input->get('org_id') : 0;
        if ($start_date != '' || $end_date != '' || $org_id > 0) {
            $filter = array(
                'start_date' => $start_date,
                'end_date' => $end_date,
                //'user_id' => $user_id,
                'org_id' => $org_id,
            );
            $result = $this->admin_model->activity_report($filter);
        } else {
            $filter = array(
                'start_date' => date('Y-m-d', time()),
            );
            $result = $this->admin_model->activity_report($filter);
        }
        $this->data['results'] = $result;


        $this->data['query'] = $this->admin_model->viewprogramme();
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/reports/activity_report', $this->data);
    }

    public function enrolled_users($id = 0) {

        $delete_id = $this->input->get('delete_id') ? $this->input->get('delete_id') : 0;
        if ($delete_id > 0) {
            $this->admin_model->delete_enrollment($delete_id);
            $this->session->set_flashdata('message', 'Enrollment deleted successfully');
            redirect(site_url('admin/enrolled_users/' . $id));
        }
        $result = $this->admin_model->enrolled_users_report($id);

        $this->data['results'] = $result;
        $this->data['content_id'] = $id;



        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/reports/enrolled_users', $this->data);
    }

    public function user_likes($id = 0) {


        $result = $this->admin_model->user_likes($id);


        $this->data['results'] = $result;
        $this->data['content_id'] = $id;



        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/reports/user_likes', $this->data);
    }

    public function user_views($id = 0) {


        $result = $this->admin_model->user_views($id);



        $this->data['results'] = $result;
        $this->data['content_id'] = $id;



        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/reports/user_views', $this->data);
    }

    public function programme_report() {
        $start_date = $this->input->get('sd') ? $this->input->get('sd') : '';
        $end_date = $this->input->get('ed') ? $this->input->get('ed') : '';
        $user_id = $this->input->get('user_id') ? $this->input->get('user_id') : '';
        $org_id = $this->input->get('org_id') ? $this->input->get('org_id') : '';
        if ($start_date != '' || $end_date != '' || $org_id > 0) {
            $filter = array(
                'start_date' => $start_date,
                'end_date' => $end_date,
                'org_id' => $org_id
            );
            $result = $this->admin_model->programme_report($filter);
        } else {
            $filter = array(
                'start_date' => date('Y-m-d', time()),
            );
            $result = $this->admin_model->programme_report($filter);
        }
        $this->data['results'] = $result;


        $this->data['query'] = $this->admin_model->viewprogramme();
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/reports/programme_report', $this->data);
    }

    public function programme_enrollment() {
        $programme_id = $this->input->get('id') ? $this->input->get('id') : 0;



        $result = $this->admin_model->programme_enroled_users($programme_id);

        $this->data['results'] = $result;


        //$this->data['query'] = $this->admin_model->viewprogramme();
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/reports/programme_enroled_users', $this->data);
    }

    public function editprogramme() {
        $editdata = $this->admin_model->get_programme_byid($this->uri->segment(3));
        $programmeid = $this->uri->segment(3);
        if (sizeof($editdata) > 0) {
            $this->data['query'] = $editdata;
            $this->data['orgs'] = $this->admin_model->get_allorganisation_byidname();
            $this->data['members'] = $this->admin_model->get_programmemembers($programmeid);
            $this->data['lang'] = $this->admin_model->get_language_dropdown();
            $this->data['admin_data'] = $this->admin_data;
            $this->load->view('admin/programme/editprogramme', $this->data);
        } else {
            redirect(site_url('admin/programme'));
        }
    }

    public function setprogramme() {
        $documentfile = '';
        if ($_FILES ['documentfile']['name']) {
            $allowed = array('csv');
            $path_doc = $_FILES['documentfile']['name'];
            $ext_doc = pathinfo($path_doc, PATHINFO_EXTENSION);
            $doc_ext_chk = array('csv');
            if (in_array($ext_doc, $allowed)) {
                $string = preg_replace('/\s+/', '', $_FILES ['documentfile']['name']); // removing white spaces
                $log_name = time() . "_" . mt_rand() . "_" . $string; // changing file name
                $fileName = $log_name;
                $upload_dir = site_url("upload/programmecsv/");
                move_uploaded_file($_FILES ['documentfile']["tmp_name"], ("./upload/programmecsv/") . "/" . ($fileName));
                $documentfile = $fileName;
                // $this->session->set_flashdata('flash_message', 'pdfs');
            } else {
                $data['status'] = 'error';
                $data['msg'] = 'This file type is not allowed';
                $documentfile = '';
                // $this->session->set_flashdata('flash_msg_error', $data['msg']);
            }
        }
        $this->admin_model->set_organisation($documentfile);
        $this->session->set_flashdata('message', 'Programme added successfully');
        redirect(site_url('admin/programme'));
    }

    public function updateprogramme() {

        $this->admin_model->update_programme();
        $this->session->set_flashdata('message', 'Programme updated successfully');
        redirect(site_url('admin/programme'));
    }

    public function getcsvfile() {
        //  $path='D://xampp/htdocs/insights-app/csv.csv';
        $csv = $_POST['csv'];
        if ($csv) {
            $row = 1;
            if (($handle = fopen("$csv", "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    $num = count($data);
                    // echo "<p> $num fields in line $row: <br /></p>\n";
                    $row++;
                    $st = array();
                    for ($c = 0; $c < $num; $c++) {
                        $st[] = $data[$c];
                    }
                    $newk = '';
                    $newk = implode(", ", $st);
                    $key[] = $newk;
                }

                fclose($handle);
                foreach ($key as $row) {
                    echo $row . '&#10';
                }
            } else {
                exit;
            }
        } else {
            exit;
        }
    }

    /*     * ******************************************content*********************************** */

    public function content() {

        $delete_id = $this->input->get('delete') ? $this->input->get('delete') : 0;
        if ($delete_id > 0) {
            $this->admin_model->delete_content($delete_id);
            $this->session->set_flashdata('message', 'Record deleted successfully');
            redirect(site_url('admin/content'));
        }

        $data['query'] = $this->admin_model->viewnews();
        $data['admin_data'] = $this->admin_data;
        $this->load->view('admin/news/viewnews', $data);
    }

    public function activities() {
        $delete_id = $this->input->get('delete') ? $this->input->get('delete') : 0;
        if ($delete_id > 0) {
            $this->admin_model->delete_content($delete_id);
            $this->admin_model->delete_content_event($delete_id);
            $this->session->set_flashdata('message', 'Record deleted successfully');
            redirect(site_url('admin/activities'));
        }
        $data['query'] = $this->admin_model->view_activities();
        $data['admin_data'] = $this->admin_data;
        $this->load->view('admin/activities/viewnews', $data);
    }

    public function activity_details($id = 0) {

        $data['query'] = $this->admin_model->activity_details($id);
        $data['content_id'] = $id;
        $data['admin_data'] = $this->admin_data;
        $this->load->view('admin/activities/activity_details', $data);
    }

    public function activity_like_details($id = 0) {

        $data['query'] = $this->admin_model->activity_like_details($id);
        $data['content_id'] = $id;
        $data['admin_data'] = $this->admin_data;
        $this->load->view('admin/activities/activity_like_details', $data);
    }

    public function contentviews() {
        $data['query'] = $this->admin_model->get_newsviews($this->uri->segment(3));
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/news/viewnewscount', $data);
    }

    public function contentviewsall() {
        $data['query'] = $this->admin_model->get_newsviewsall($this->uri->segment(3));
        $data['admin_data'] = $this->admin_data;
        $this->load->view('admin/news/viewnewscountall', $data);
    }

    public function contentrating() {
        $data['query'] = $this->admin_model->get_newsrating($this->uri->segment(3));
        $data['admin_data'] = $this->admin_data;
        $this->load->view('admin/news/viewnewsrating', $data);
    }

    /*     * ************add news***************************** */

    public function addcontent() {
        $admin_id = $this->session->userdata('ShowspoonAdmin_logged_in')['user_id'] ? $this->session->userdata('ShowspoonAdmin_logged_in')['user_id'] : 0;

        $this->data['admins'] = $this->admin_model->get_admin($admin_id);

        $this->data['query'] = $this->admin_model->get_allorganisation_byidname();
        $this->data['messagetype'] = $this->admin_model->get_message_type();
        $this->data['lang'] = $this->admin_model->get_language_dropdown();



        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/news/addnews', $this->data);
    }

    public function addactivity() {
        $admin_id = $this->session->userdata('ShowspoonAdmin_logged_in')['user_id'] ? $this->session->userdata('ShowspoonAdmin_logged_in')['user_id'] : 0;

        $this->data['admins'] = $this->admin_model->get_admin($admin_id);
        $this->data['query'] = $this->admin_model->get_allorganisation_byidname();
        $this->data['messagetype'] = $this->admin_model->get_message_type();
        $this->data['lang'] = $this->admin_model->get_language_dropdown();
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/activities/addnews', $this->data);
    }

    public function updatecontent($id = 0) {
        $this->admin_model->updatecontent($id);
        $this->session->set_flashdata('message', 'Activity added successfully');
        redirect(site_url('admin/content'));
    }

    public function updateactivity($id = 0) {
        $this->admin_model->updatecontent($id);
        $this->session->set_flashdata('message', 'Activity updated successfully');
        redirect(site_url('admin/activities'));
    }

    public function editcontent($id = 0) {

        $admin_id = $this->session->userdata('ShowspoonAdmin_logged_in')['user_id'] ? $this->session->userdata('ShowspoonAdmin_logged_in')['user_id'] : 0;

        $this->data['admins'] = $this->admin_model->get_admin($admin_id);
        $this->data['query'] = $this->admin_model->get_allorganisation_byidname();
        $this->data['messagetype'] = $this->admin_model->get_message_type();
        $this->data['content'] = $this->admin_model->getcontent($id);
        $this->data['content_event'] = $this->admin_model->get_event($id);
        $this->data['lang'] = $this->admin_model->get_language_dropdown();
        /* print_r($this->data['content']);
          die(); */
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/news/editnews', $this->data);
    }

    public function editactivity($id = 0) {
        $admin_id = $this->session->userdata('ShowspoonAdmin_logged_in')['user_id'] ? $this->session->userdata('ShowspoonAdmin_logged_in')['user_id'] : 0;

        $this->data['admins'] = $this->admin_model->get_admin($admin_id);
        $this->data['query'] = $this->admin_model->get_allorganisation_byidname();
        $this->data['messagetype'] = $this->admin_model->get_message_type();
        $this->data['content'] = $this->admin_model->getcontent($id);
        $this->data['content_event'] = $this->admin_model->get_event($id);
        $this->data['lang'] = $this->admin_model->get_language_dropdown();
        /* print_r($this->data['content']);
          die(); */
        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/activities/editnews', $this->data);
    }

    public function getprogramme() {
        $pid = $this->input->post('pid');
        $programme_id = $this->input->post('hid_programme') ? $this->input->post('hid_programme') : 0;

        $degree = $this->db->query("Select programme_id, programme_name as programme_title from in_programme where programme_active=1 and programme_organisationid='" . $pid . "'  ");

        if ($degree->num_rows() > 0) {
            $degree = $degree->result();

            foreach ($degree as $row) {
                $selected = '';
                if ($row->programme_id == $programme_id) {
                    $selected = 'selected';
                }
                echo '<option ' . $selected . ' value="' . $row->programme_id . '">' . $row->programme_title . '</option>';
            }
        } else {
            echo '<option value="" >No Programmes Available</option>';
        }
    }

    public function registercontent() {
        $this->admin_model->registercontent();
        $this->session->set_flashdata('message', 'Content added successfully');
        redirect(site_url('admin/content'));
    }

    public function registeractivity() {
        /* echo '<pre>';
          print_r($this->input->post());
          print_r($_FILES);
          die(); */
        $this->admin_model->registercontent();
        $this->session->set_flashdata('message', 'Activity added successfully');
        redirect(site_url('admin/activities'));
    }

    public function viewenrollment() {
        $id = $this->uri->segment(3);
        $this->data['admin_data'] = $this->admin_data;
        $this->data['query'] = $this->admin_model->get_eventenrollment($id);
        $this->load->view('admin/news/viewenrollment', $this->data);
    }

    public function deleteenrollment() {
        $id = $this->uri->segment(3);
        $userid = $this->input->post('userid');
        $this->admin_model->delete_eventenrollment($userid);
        $this->data['query'] = $this->admin_model->get_eventenrollment($id);
        $this->load->view('admin/news/viewenrollment', $this->data);
    }

    public function notifications() {
        $id = $this->uri->segment(3);
        $this->data['admin_data'] = $this->admin_data;
        $this->data['query'] = $this->admin_model->viewnotifications();
        $this->load->view('admin/news/viewnotifications', $this->data);
    }

    public function addnotification() {
        $this->data['query'] = $this->admin_model->get_allorganisation_byidname();
        // $this->data['messagetype']=$this->admin_model->get_message_type();
        $this->data['lang'] = $this->admin_model->get_language_dropdown();

        $admin_id = $this->session->userdata('ShowspoonAdmin_logged_in')['user_id'] ? $this->session->userdata('ShowspoonAdmin_logged_in')['user_id'] : 0;

        $this->data['admins'] = $this->admin_model->get_admin($admin_id);


        $this->data['admin_data'] = $this->admin_data;
        $this->load->view('admin/news/addnotification', $this->data);
    }

    public function registernotification() {
        $this->data['query'] = $this->admin_model->registernotification();
        $this->session->set_flashdata('message', 'Push notification sent successfully');
        redirect(site_url('admin/notifications'));
    }

    public function newsfeed() {
        $delete_id = $this->input->get('delete') ? $this->input->get('delete') : 0;
        if ($delete_id > 0) {
            $this->admin_model->delete_newsfeed($delete_id);
            $this->session->set_flashdata('message', 'Record deleted successfully');
            redirect(site_url('admin/newsfeed'));
        }

        $data['admin_data'] = $this->admin_data;
        $data['query'] = $this->admin_model->viewnewsfeed();
        $this->load->view('admin/newsfeed/viewnews', $data);
    }

    public function news_details($id = 0) {


        $data['content_id'] = $id;
        $data['admin_data'] = $this->admin_data;
        $data['query'] = $this->admin_model->newsfeed_details($id);
        $this->load->view('admin/newsfeed/news_details', $data);
    }

    public function news_like_details($id = 0) {


        $data['admin_data'] = $this->admin_data;
        $data['content_id'] = $id;
        $data['query'] = $this->admin_model->newsfeed_like_details($id);
        $this->load->view('admin/newsfeed/news_like_details', $data);
    }

    public function addnewsfeed() {
        $admin_id = $this->session->userdata('ShowspoonAdmin_logged_in')['user_id'] ? $this->session->userdata('ShowspoonAdmin_logged_in')['user_id'] : 0;

        $this->data['admin_data'] = $this->admin_data;
        $this->data['admins'] = $this->admin_model->get_admin($admin_id);
        $this->data['query'] = $this->admin_model->get_allorganisation_byidname();
        $this->data['messagetype'] = $this->admin_model->get_message_type();
        $this->data['lang'] = $this->admin_model->get_language_dropdown();
        $this->load->view('admin/newsfeed/addnews', $this->data);
    }

    public function editnewsfeed($id = 0) {
        $admin_id = $this->session->userdata('ShowspoonAdmin_logged_in')['user_id'] ? $this->session->userdata('ShowspoonAdmin_logged_in')['user_id'] : 0;

        $this->data['admin_data'] = $this->admin_data;
        $this->data['admins'] = $this->admin_model->get_admin($admin_id);
        $this->data['query'] = $this->admin_model->get_allorganisation_byidname();
        $this->data['messagetype'] = $this->admin_model->get_message_type();
        $this->data['lang'] = $this->admin_model->get_language_dropdown();
        $this->data['newsfeed'] = $this->admin_model->getnewsfeed($id);
        /* echo '<pre>';

          print_r($this->data['newsfeed']);
          die(); */
        $this->load->view('admin/newsfeed/editnews', $this->data);
    }

    public function registernewsfeed() {
        $this->admin_model->registernewsfeed();
        $this->session->set_flashdata('message', 'News added successfully');
        redirect(site_url('admin/newsfeed'));
    }

    public function updatenewsfeed($id = 0) {

        $this->admin_model->updatenewsfeed($id);
        $this->session->set_flashdata('message', 'News updated successfully');
        redirect(site_url('admin/newsfeed'));
    }

    /*     * **********************************end content******************************** */

    function logout() {
        $this->session->unset_userdata('ShowspoonAdmin_logged_in');
        redirect('admin');
    }

    
        

    function convert_time() {


        /* $date_local = new \DateTime();
          $date_utc = new \DateTime(null, new \DateTimeZone("UTC"));

          $date_local->format(\DateTime::RFC850); # Saturday, 18-Apr-15 13:23:46 AEST
          echo $date_utc->format(\DateTime::RFC850); # Saturday, 18-Apr-15 03:23:46 UTC

          die(); */

        $query = $this->db->query("select CNT_ISO from in_countries order by CNT_ISO asc");
        $results = $query->result();
        foreach ($results as $row) {
            /* $UTCObj = new DateTime("2016-09-26 10:06:00", new DateTimeZone("Europe/London"));
              $UTCObj->format("F d, Y g:i a") ;
              $LocalObj = $UTCObj; */
            $timezone = DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, $row->CNT_ISO);
            //$LocalObj->setTimezone(new DateTimeZone("$timezone[0]"));
            //print_r($timezone);
            $time_zone = isset($timezone[0]) ? $timezone[0] : 'Europe/London';
            date_default_timezone_set($time_zone);
            $this_tz_str = date_default_timezone_get();
            $this_tz = new DateTimeZone($this_tz_str);
            $now = new DateTime("now", $this_tz);
            $offset = $this_tz->getOffset($now);

            $hours = number_format($offset / 3600, 2);
            /* if(($hours<10 && $hours>0) || ($hours>-10 && $hours<0))
              {
              $hours='0'.$hours;
              } */
            if ($hours > 0) {
                $hours = '+' . $hours;
            }


            $this->db->where('CNT_ISO', $row->CNT_ISO);
            $this->db->update('in_countries', array('zone_name' => $time_zone));
        }
        //die();
    }

    public function get_live_data2() {
        // Set the JSON header



        header("Content-type: text/json");

        date_default_timezone_set('UTC');

        $query = $this->db->query("SELECT
    history_datetime,
    history_id,
    count(history_id) as total
FROM in_user_login_history

GROUP BY
UNIX_TIMESTAMP(history_datetime) DIV 3600  order by history_datetime desc limit 5");
        $result = $query->result();
        $total = $query->num_rows();

        $dat_enr_arr = $dat_like_arr = $dat_views_arr = $dat_arr = array();
        $dat = array();
        $current_time = time();
        $current_time2 = date("Y-m-d H:00:00", time());
        $current_time = strtotime($current_time2);


        if ($total > 0) {

            if ($total > 0) {
                foreach ($result as $row) {
                    $date_min = date('y-m-d-H', strtotime($row->history_datetime));
                    $dat_arr[$date_min] = $row->total;
                }
            }
            //print_r($dat_arr);
            //$times=$time = strtotime("-1 hours",$current_time);
        }

        $query = $this->db->query("SELECT
    date,
    like_id,
    count(like_id) as total
FROM in_likes

GROUP BY
UNIX_TIMESTAMP(date) DIV 3600  order by date desc limit 5");
        $result = $query->result();
        $total = $query->num_rows();

        if ($total > 0) {
            foreach ($result as $row) {
                $date_min = date('y-m-d-H', strtotime($row->date));
                $dat_like_arr[$date_min] = $row->total;
            }
        }
        $query = $this->db->query("SELECT
    date,
    logid,
    count(logid) as total
FROM in_newsviews

GROUP BY
UNIX_TIMESTAMP(date) DIV 3600  order by date desc limit 5");
        $result = $query->result();
        $total = $query->num_rows();

        if ($total > 0) {
            foreach ($result as $row) {
                $date_min = date('y-m-d-H', strtotime($row->date));
                $dat_views_arr[$date_min] = $row->total;
            }
        }

        $query = $this->db->query("SELECT
    insert_time,
    reg_id,
    count(reg_id) as total
FROM in_user_events

GROUP BY
UNIX_TIMESTAMP(insert_time) DIV 3600  order by insert_time desc limit 5");
        $result = $query->result();
        $total = $query->num_rows();

        if ($total > 0) {
            foreach ($result as $row) {
                $date_min = date('y-m-d-H', strtotime($row->insert_time));
                $dat_enr_arr[$date_min] = $row->total;
            }
        }

        $times = $node_session = $node_like = $node_view = $node_enr = 0;

        $times = $time = $current_time;
        $new_format = date('y-m-d-H', $times);
        if (isset($dat_arr[$new_format])) {
            $node_session = (int) $dat_arr[$new_format];
        }
        if (isset($dat_like_arr[$new_format])) {
            $node_like = (int) $dat_like_arr[$new_format];
        }
        if (isset($dat_views_arr[$new_format])) {
            $node_view = (int) $dat_views_arr[$new_format];
        }
        if (isset($dat_enr_arr[$new_format])) {
            $node_enr = (int) $dat_enr_arr[$new_format];
        }



        $m_k = $times * 1000;
        $final_data = array(
            'n_key' => $new_format,
            'new_time' => $times,
            'node_session' => $node_session,
            'session_point' => array($m_k, $node_session),
            'node_like' => $node_like,
            'like_point' => array($m_k, $node_like),
            'node_view' => $node_view,
            'view_point' => array($m_k, $node_view),
            'node_enr' => $node_enr,
            'enr_point' => array($m_k, $node_enr)
        );
        echo json_encode($final_data);
    }

    public function get_live_data() {
        // Set the JSON header
        //date_default_timezone_set('UTC');
        //date_default_timezone_set('Europe/London');
        //date_default_timezone_set('Asia/Karachi');
        date_default_timezone_set($this->admin_data->zone_name);

        header("Content-type: text/json");

        $current_time = time();
        $current_time2 = date("Y-m-d H:00:00", time());
        $current_time = strtotime($current_time2);

        $c_d = date("Y-m-d", strtotime('-2 days', time()));
        $c_d2 = date("Y-m-d", strtotime('+1 days', time()));
        $query = $this->db->query("SELECT * FROM `in_user_data` where DATE(datetime)>='" . $c_d . "' and  DATE(datetime)<='" . $c_d2 . "'");
        $result = $query->result();
        $total = $query->num_rows();

        if ($total > 0) {
            foreach ($result as $row) {
                $date_min = date('y-m-d-H', strtotime($row->datetime));
                $dat_arr[$date_min] = $row->total;
            }
        }

        $query = $this->db->query("SELECT
    date as date2,
ADDTIME(date,concat(CAST(C.time_zone AS DECIMAL(11,0)),':00')) as date,
    like_id,
    count(like_id) as total
FROM in_likes L
inner join in_user U on U.user_id = L.user_id
inner join in_countries C on C.CNT_name= U.user_country
GROUP BY
UNIX_TIMESTAMP(date) DIV 3600  order by date desc limit 30");
        $result = $query->result();
        $total = $query->num_rows();

        if ($total > 0) {
            foreach ($result as $row) {
                $date_min = date('y-m-d-H', strtotime($row->date));
                $dat_like_arr[$date_min] = $row->total;
            }
        }
        $query = $this->db->query("SELECT
    date as date2,
ADDTIME(date,concat(CAST(C.time_zone AS DECIMAL(11,0)),':00')) as date,
    logid,
    count(logid) as total
FROM in_newsviews V
inner join in_user U on U.user_id = V.userid
inner join in_countries C on C.CNT_name= U.user_country

GROUP BY
UNIX_TIMESTAMP(date) DIV 3600  order by date desc limit 30");
        $result = $query->result();
        $total = $query->num_rows();

        if ($total > 0) {
            foreach ($result as $row) {
                $date_min = date('y-m-d-H', strtotime($row->date));
                $dat_views_arr[$date_min] = $row->total;
            }
        }

        $query = $this->db->query("SELECT
    insert_time as date2,
ADDTIME(insert_time,concat(CAST(C.time_zone AS DECIMAL(11,0)),':00')) as insert_time,
    reg_id,
    count(reg_id) as total
FROM in_user_events UE
inner join in_user U on U.user_id = UE.user_id
inner join in_countries C on C.CNT_name= U.user_country

GROUP BY
UNIX_TIMESTAMP(insert_time) DIV 3600  order by insert_time desc limit 30");
        $result = $query->result();
        $total = $query->num_rows();

        if ($total > 0) {
            foreach ($result as $row) {
                $date_min = date('y-m-d-H', strtotime($row->insert_time));
                $dat_enr_arr[$date_min] = $row->total;
            }
        }

        $times = $node_session = $node_like = $node_view = $node_enr = 0;

        $times = $time = $current_time;
        $new_format = date('y-m-d-H', $times);
        if (isset($dat_arr[$new_format])) {
            $node_session = (int) $dat_arr[$new_format];
        }
        if (isset($dat_like_arr[$new_format])) {
            $node_like = (int) $dat_like_arr[$new_format];
        }
        if (isset($dat_views_arr[$new_format])) {
            $node_view = (int) $dat_views_arr[$new_format];
        }
        if (isset($dat_enr_arr[$new_format])) {
            $node_enr = (int) $dat_enr_arr[$new_format];
        }


        $current_time = time();
        $current_time = date("Y-m-d H:00:00", time());
        $current_time = strtotime($current_time);

        for ($j = 0; $j < 20; $j++) {
            $minutes = 19 - $j;
            $times = $time = strtotime("-$minutes hours", $current_time);
            $times2 = date('y-m-d-H', $times);
            $times2 = strtotime($times2);
            $times24 = strtotime("+12 hour", $times2);
            $query_time = date('y-m-d-H', $times24);
            $query_time22 = date('Y-m-d', $time);
            //$r_ar[$query_time]=isset($dat_arr[$query_time])?(int)$dat_arr[$query_time]:0;

            $time_arr[] = $query_time22;
            $dat[] = isset($dat_arr[$query_time]) ? (int) $dat_arr[$query_time] : 0;
            $dat_like[] = isset($dat_like_arr[$query_time]) ? (int) $dat_like_arr[$query_time] : 0;
            $dat_views[] = isset($dat_views_arr[$query_time]) ? (int) $dat_views_arr[$query_time] : 0;
            $dat_enr[] = isset($dat_enr_arr[$query_time]) ? (int) $dat_enr_arr[$query_time] : 0;
        }
        //print_r($r_ar);
        /*        array_reverse($time_arr);
          array_reverse($dat);
          array_reverse($dat_like);
          array_reverse($dat_views);
          array_reverse($dat_enr);
          $encoded_time =  json_encode($time_arr);
          $encoded_string =  json_encode($dat);
          $encoded_string_lk =  json_encode($dat_like);
          $encoded_string_vw =  json_encode($dat_views);
          $encoded_string_enr =  json_encode($dat_enr); */

        $date_cur = date('Y-m-d', time());
        //$total_q=$this->db->query("select sum(total) as total from in_user_data where DATE(datetime)='".$date_cur."'");
        $total_q = $this->db->query("select total as total from in_user_data order by datetime desc limit 1");
        $total_r = $total_q->row();
        $session_total = isset($total_r->total) ? $total_r->total : 0;

        $total_q = $this->db->query("SELECT
    date as date2,
ADDTIME(date,concat(CAST(C.time_zone AS DECIMAL(11,0)),':00')) as date,
    like_id,
    count(like_id) as total
FROM in_likes L
inner join in_user U on U.user_id = L.user_id
inner join in_countries C on C.CNT_name= U.user_country
where DATE(date)='" . $date_cur . "'");
        $total_r = $total_q->row();
        $like_total = isset($total_r->total) ? $total_r->total : 0;

        $total_q = $this->db->query("SELECT
    date as date2,
ADDTIME(date,concat(CAST(C.time_zone AS DECIMAL(11,0)),':00')) as date,
    logid,
    count(logid) as total
FROM in_newsviews V
inner join in_user U on U.user_id = V.userid
inner join in_countries C on C.CNT_name= U.user_country

where DATE(date)='" . $date_cur . "'  order by date desc limit 30");
        $total_r = $total_q->row();
        $views_total = isset($total_r->total) ? $total_r->total : 0;

        $total_q = $this->db->query("SELECT
    insert_time as date2,
ADDTIME(insert_time,concat(CAST(C.time_zone AS DECIMAL(11,0)),':00')) as insert_time,
    reg_id,
    count(reg_id) as total
FROM in_user_events UE
inner join in_user U on U.user_id = UE.user_id
inner join in_countries C on C.CNT_name= U.user_country
where DATE(insert_time)='" . $date_cur . "'
  order by insert_time desc limit 30");
        $total_r = $total_q->row();
        $enrol_total = isset($total_r->total) ? $total_r->total : 0;


        $m_k = $times * 1000;
        $final_data = array(
            'n_key' => $new_format,
            'new_time' => $times,
            /*
              'node_session'=>$node_session,
              'session_point'=>array($m_k,$node_session),

              'node_like'=>$node_like,
              'like_point'=>array($m_k,$node_like),

              'node_view'=>$node_view,
              'view_point'=>array($m_k,$node_view),
             */
            'session_total' => $session_total,
            'like_total' => $like_total,
            'views_total' => $views_total,
            'enrol_total' => $enrol_total,
            'node_enr' => $node_enr,
            'enr_point' => array($m_k, $node_enr),
            'enc_session' => $dat,
            'enc_like' => $dat_like,
            'enc_views' => $dat_views,
            'enc_enr' => $dat_enr,
        );
        echo json_encode($final_data);
    }

    public function get_live_data_details() {
        date_default_timezone_set($this->admin_data->zone_name);
        $date = $this->input->post('date') ? $this->input->post('date') : '';
        $line_type = $this->input->post('line_type') ? $this->input->post('line_type') : '';
        $html = '';
        if ($date > 0) {
            $date = $time_stamp = $date / 1000;
            $date = date("Y-m-d H:i:s", $time_stamp);
            $date22 = date("Y-m-d H:i:s", strtotime("-1 hour", $time_stamp));
        }
        if ($line_type == 0) {


            $total_q = $this->db->query("SELECT U.user_fname,U.user_lname,

ADDTIME(user_lastlogindatetime,concat(CAST(C.time_zone AS DECIMAL(11,0)),':00')) as date

FROM  in_user U
inner join in_countries C on C.CNT_name= U.user_country
where U.user_session=1
group by U.user_id


 order by date ");
            /* having ( date>='".$date22."' and  date<='".$date."')            */
            $results = $total_q->result();

            $html.='<h3>Active Devices</h3>';

            $html .='<table class="footable table toggle-square-filled live_stat_table" id="filterTable1" data-page-size="25" data-filter="#filter">
        <thead>
            <tr>
                <th>Full Name</th>
                <th>Date</th>

            </tr>
            </thead>
            <tbody>';
            if (count($results) > 0) {
                foreach ($results as $result) {
                    $html.='<tr>
                <td>' . $result->user_fname . ' ' . $result->user_lname . '</td>
                <td>' . date("Y-m-d H:i:s", strtotime($result->date)) . '</td>

            </tr>';
                }
            } else {
                $html.='<tr>
                <td colspan="2" align="center">No record found</td>
                </tr>';
            }
            $html.='</tbody>
        <tfoot>                                                <tr>                                                    <td colspan="10">                                                        <div class="pagination pagination-right hide-if-no-paging"></div>                                                    </td>                                                </tr>                                            </tfoot>
        </table>
        <script>
    $(function () {
        $(".footable").footable();
    });
</script>';
        } elseif ($line_type == 2) {






            $total_q = $this->db->query("SELECT
    date as date2,
ADDTIME(date,concat(CAST(C.time_zone AS DECIMAL(11,0)),':00')) as date,
    logid,U.user_fname,U.user_lname,

    (
   CASE
      WHEN V.section = 2 THEN (select newsfeed_title from in_newsfeed where newsfeed_id = V.contentid)
      ELSE (SELECT content_title from in_content where content_id = V.contentid)
   END
) AS content_title,
(
   CASE
      WHEN V.section = 2 THEN (select newsfeed_body from in_newsfeed where newsfeed_id = V.contentid)
      ELSE (SELECT content_body from in_content where content_id = V.contentid)
   END
) AS content_body

FROM in_newsviews V
inner join in_user U on U.user_id = V.userid
inner join in_countries C on C.CNT_name= U.user_country

 having ( date>='" . $date22 . "' and  date<='" . $date . "')  order by date desc ");
            $results = $total_q->result();

            $html.='<h3>Views</h3>';

            $html .='<table class="footable table toggle-square-filled live_stat_table" id="filterTable1" data-page-size="25" data-filter="#filter">
        <thead>
            <tr>
                <th>Title</th>
                <th>Date</th>

            </tr>
            </thead>
            <tbody>';
            if (count($results) > 0) {
                foreach ($results as $result) {
                    if ($result->content_title != '') {
                        $title = $result->content_title;
                    } else {
                        $title = $result->content_body;
                        $title = strip_tags($title);
                        $title = substr($title, 0, 25);
                    }
                    $html.='<tr>
                <td>' . $title . '</td>
                <td>' . date("Y-m-d H:i:s", strtotime($result->date)) . '</td>

            </tr>';
                }
            } else {
                $html.='<tr>
                <td colspan="2" align="center">No record found</td>
                </tr>';
            }
            $html.='</tbody>
        <tfoot>                                                <tr>                                                    <td colspan="10">                                                        <div class="pagination pagination-right hide-if-no-paging"></div>                                                    </td>                                                </tr>                                            </tfoot>
        </table>
        <script>
    $(function () {
        $(".footable").footable();
    });
</script>';
        } elseif ($line_type == 1) {





            $total_q = $this->db->query("SELECT
    date as date2,
ADDTIME(date,concat(CAST(C.time_zone AS DECIMAL(11,0)),':00')) as date,
    like_id,U.user_fname,U.user_lname,

    (
   CASE
      WHEN V.section = 2 THEN (select newsfeed_title from in_newsfeed where newsfeed_id = V.news_id)
      ELSE (SELECT content_title from in_content where content_id = V.news_id)
   END
) AS content_title,
(
   CASE
      WHEN V.section = 2 THEN (select newsfeed_body from in_newsfeed where newsfeed_id = V.news_id)
      ELSE (SELECT content_body from in_content where content_id = V.news_id)
   END
) AS content_body

FROM in_likes V
inner join in_user U on U.user_id = V.user_id
inner join in_countries C on C.CNT_name= U.user_country

 having ( date>='" . $date22 . "' and  date<='" . $date . "')  order by date desc ");
            $results = $total_q->result();

            $html.='<h3>Likes</h3>';

            $html .='<table class="footable table toggle-square-filled live_stat_table" id="filterTable1" data-page-size="25" data-filter="#filter">
        <thead>
            <tr>
                <th>Title</th>
                <th>Date</th>

            </tr>
            </thead>
            <tbody>';
            if (count($results) > 0) {
                foreach ($results as $result) {
                    if ($result->content_title != '') {
                        $title = $result->content_title;
                    } else {
                        $title = $result->content_body;
                        $title = strip_tags($title);
                        $title = substr($title, 0, 25);
                    }
                    $html.='<tr>
                <td>' . $title . '</td>
                <td>' . date("Y-m-d H:i:s", strtotime($result->date)) . '</td>

            </tr>';
                }
            } else {
                $html.='<tr>
                <td colspan="2" align="center">No record found</td>
                </tr>';
            }
            $html.='</tbody>
        <tfoot>                                                <tr>                                                    <td colspan="10">                                                        <div class="pagination pagination-right hide-if-no-paging"></div>                                                    </td>                                                </tr>                                            </tfoot>
        </table>
        <script>
    $(function () {
        $(".footable").footable();
    });
</script>';
        } elseif ($line_type == 3) {





            $total_q = $this->db->query("SELECT
    insert_time as date2,
ADDTIME(insert_time,concat(CAST(C.time_zone AS DECIMAL(11,0)),':00')) as date,
    reg_id,U.user_fname,U.user_lname,CC.content_title


FROM in_user_events V
inner join in_user U on U.user_id = V.user_id
inner join in_content_event E on E.content_event_id= V.event_id
inner join in_content CC on CC.content_id= E.content_contentid
inner join in_countries C on C.CNT_name= U.user_country

 having ( date>='" . $date22 . "' and  date<='" . $date . "')  order by date desc ");
            $results = $total_q->result();

            $html.='<h3>Enrollment</h3>';

            $html .='<table class="footable table toggle-square-filled live_stat_table" id="filterTable1" data-page-size="25" data-filter="#filter">
        <thead>
            <tr>
                <th>Title</th>
                <th>Date</th>

            </tr>
            </thead>
            <tbody>';
            if (count($results) > 0) {
                foreach ($results as $result) {

                    $html.='<tr>
                <td>' . $result->content_title . '</td>
                <td>' . date("Y-m-d H:i:s", strtotime($result->date)) . '</td>

            </tr>';
                }
            } else {
                $html.='<tr>
                <td colspan="2" align="center">No record found</td>
                </tr>';
            }
            $html.='</tbody>
        <tfoot>                                                <tr>                                                    <td colspan="10">                                                        <div class="pagination pagination-right hide-if-no-paging"></div>                                                    </td>                                                </tr>                                            </tfoot>
        </table>
        <script>
    $(function () {
        $(".footable").footable();
    });
</script>';
        }
        echo json_encode(array('data' => $html, 'date' => $date));
    }

    public function change_password() {


        if ($this->input->post()) {
            $userdata = $this->session->userdata('ShowspoonAdmin_logged_in');
            $user_id = isset($userdata['user_id']) ? $userdata['user_id'] : 0;
            $update_password = array(
                'old_password' => $this->input->post('old_password'),
                'password' => $this->input->post('password'),
                'user_id' => $user_id
            );

            $res = $this->admin_model->update_password($update_password);
            if ($res == true) {
                $this->data['success'] = 'Password has been successfully updated.';
            } else {
                $this->data['error'] = 'Old password do not match.';
            }
        }
        $this->load->view('admin/organization/change_password', $this->data);
    }

    public function test() {
        //echo $this->validate_slug('page-slug');
    }

    public function upload_file() {


        /*        print_r($_FILES['file']['name'][0]);
          die(); */
        $json = array();
        if (isset($_FILES['file']['name'][0])) {

            $string = preg_replace('/\s+/', '', $_FILES ['file']['name'][0]);
            $log_name = time() . "_" . mt_rand() . "_" . $string;
            $fileName = $log_name;

            $upload_dir = site_url("upload/attachments/");


            move_uploaded_file($_FILES ['file']["tmp_name"][0], ("upload/attachments") . "/" . ($fileName));

            $record = array('temp_name' => $fileName);
            $this->db->insert('in_temp', $record);
            $json['success'] = 'Success';
            $json['file'] = $fileName;
        }
        echo json_encode($json);
        /* session_start();

          $key = ini_get("session.upload_progress.prefix") . "first";
          if (!empty($_SESSION[$key])) {
          $current = $_SESSION[$key]["bytes_processed"];
          $total = $_SESSION[$key]["content_length"];
          echo $current < $total ? ceil($current / $total * 100) : 100;
          }
          else {
          echo 100;
          } */
    }

    public function remove_file() {

        $json = array();
        $file_name = $this->input->post('temp_id') ? $this->input->post('temp_id') : '';
        if ($file_name != '') {

            $old_image_path = 'upload/attachments/' . $file_name;
            @unlink($old_image_path);
            $this->db->where('temp_name', $file_name);
            $this->db->delete('in_temp');

            $json['success'] = 'Success';
        }
        echo json_encode($json);
    }

    public function run_scrapper()
    {

        $this->load->view('admin/run_scrapper', $this->data);
    }

    public function download_csv() {


        $date = $this->input->get('date') ? $this->input->get('date', true) : date('Y-m-d', time());
        $dateEnd = $this->input->get('dateEnd') ? $this->input->get('dateEnd', true) : date('Y-m-d', time());
        $type = $this->input->get('type') ? $this->input->get('type', true) : '';
        $sheet_name='noname-';
        if($type==1){
            $sheet_name='OlX-';
        }elseif($type==2){
            $sheet_name='CARMUDI-';
        }elseif($type==3){
            $sheet_name='MOBIL123-';
        }elseif($type==4){
            $sheet_name='MOBILBEKAS-';
        }elseif($type==5){
            $sheet_name='JUALBELI-';
        }elseif($type==6){
            $sheet_name='JUALO-';
        }

        $model_filter='';
        if($type==1)
        {
            $model_filter=' and (D.model is not null || D.year is not null) ';
        }


        if($type!=''){
            $query = $this->db->query("select D.*,W.names from tbl_data D
        left join tbl_webs W on W.web_id = D.source
        where  source=? and DATE(`time_ago`)>=? and DATE(`time_ago`)<=? $model_filter order by id desc", array($type, $date,$dateEnd));
        }else{
            $query = $this->db->query("select D.*,W.names from tbl_data D
            left join tbl_webs W on W.web_id = D.source
            where    DATE(`time_ago`)>=? and DATE(`time_ago`)<=?  $model_filter order by id desc", array($date,$dateEnd));
        }
        $eoddata = $query->result();

        $th = array(
            'Listing ID', 'Ad_ID', 'Link', 'Title', 'Make',
            'Model', 'Machine Capacity', 'Variant',
            'Year', 'Mileage', 'Transmission',
            'Fuel', 'Color', 'Price','Features',
            'Description', 'Name', 'Phone', 'Email',
            'Province', 'City', 'Date_Scraped', 'Date_Posted', 'Seller_Type', 'Exact_Address', 'Status'

        );
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=data-' .$sheet_name. date("y-m-d-H-i", time()) . '.csv');
        $output = fopen("php://output", "w");
        fputcsv($output, $th);



        $i = 1;
        $print=array();
        foreach ($eoddata as $row) {
            $row_ar = (array)$row;
            $fields = array('ad_id','ad_link','title','make','model','machine_capacity','variant','year','mileage','transmission','fuel','color','amount','features','description','owner','phone','email','province','city','created_datetime','time_ago','seller_type','address','is_active');

            $data=array();
            $data[]=$i;
            foreach($fields as $fl)
            {
                if($fl=='created_datetime')
                {
                    $data[]=isset($row_ar[$fl])?date('Y-m-d',strtotime($row_ar[$fl])):'';
                }else
                {
                    $data[]=isset($row_ar[$fl])?$row_ar[$fl]:'';    
                }

            }
            /*$data = array(
                $i,
                $row->ad_id,
                $row->ad_link,
                $row->title,
                $row->make,
                $row->model,
                $row->machine_capacity,
                $row->variant,
                $row->year,
                $row->mileage,
                $row->transmission,
                $row->fuel,
                $row->color,
                $row->amount,
                $row->features,
                $row->description,
                $row->owner,
                $row->phone,
                $row->email,
                $row->province,
                $row->city,
                $row->neighborhood,
                $row->created_datetime,
                $row->time_ago,
                $row->seller_type,
                $row->address,
                $row->is_active,

            );*/


            fputcsv($output, $data);
            $i++;
        }
        fclose($output);
    }


    public function ajax_load()
    {
        $fields = array('is_active','ad_id','title','make','model','machine_capacity','variant','year','mileage','transmission','fuel','color','amount','features','description','owner','phone','email','province','city','time_ago','created_datetime','seller_type','address','source');
        $fields_order = array('','is_active','ad_id','ad_link','title','make','model','machine_capacity','variant','year','mileage','transmission','fuel','color','amount','features','description','owner','phone','email','province','city','time_ago','created_datetime','seller_type','address','source');
        $json=array();
        $total = 0;
        $offset = $this->input->post('start')?$this->input->post('start'):0;
        $perpage = $this->input->post('length')?$this->input->post('length'):50;
        $order = $this->input->post('order') ? $this->input->post('order') : array();
        $search = $this->input->post('search') ? $this->input->post('search') : array();
        $date = $this->input->post('date') ? $this->input->post('date', true) : date('Y-m-d', strtotime('-1 day',time()));
        $dateEnd = $this->input->post('dateEnd') ? $this->input->post('dateEnd', true) : date('Y-m-d', strtotime('-1 day',time()));
        $type = $this->input->post('type') ? $this->input->post('type', true) : '';

        $filter = array(
            'offset' => $offset,
            'limit' => $perpage,
        );




        $where = '';


        if($type!='')
        {
            $where .= ' and source = '.$type.' ';
        }
        if(isset($search['value']) && $search['value']!='')
        {
            $where .= ' and ( ';
            $st=0;
            foreach($fields as $val)
            {

                if($st==0)
                {
                    if($val=='is_active')
                    {
                        $active_filter = '-';
                        if(strtolower($search['value'])=='active'){$active_filter=1;}
                        if(strtolower($search['value'])=='expired'){$active_filter=0;}
                        if(strtolower($search['value'])=='modified'){$active_filter=2;}
                        $where .= ' D.'.$val.' = "'.$active_filter.'" ';        
                    }else
                    {
                        $where .= ' D.'.$val.' like "%'.$search['value'].'%" ';        
                    }

                }else
                {
                    if($val=='is_active')
                    {
                        $active_filter = '-';
                        if(strtolower($search['value'])=='active'){$active_filter=1;}
                        if(strtolower($search['value'])=='expired'){$active_filter=0;}
                        if(strtolower($search['value'])=='modified'){$active_filter=2;}
                        $where .= ' OR D.'.$val.' = "'.$active_filter.'" ';    

                    }else
                    {
                        $where .= ' OR D.'.$val.' like "%'.$search['value'].'%" ';        
                    }
                }

                $st++;
            }
            $where .= ' ) ';

        }
        $order_by = '  id  ';

        if (isset($order[0]['column'], $order[0]['dir'])) {

            if(isset($fields_order[$order[0]['column']]) && $fields_order[$order[0]['column']]!='')
            {
                $order_by = $fields_order[$order[0]['column']];
            }


            if ($order[0]['dir'] == 'asc') {

                $sort_order = '  ASC ';
            } else {
                $sort_order = '  desc ';
            }
        }
        $model_filter='';
        if($type==1)
        {
            $model_filter=' and (D.model is not null || D.year is not null) ';
        }



        $query = $this->db->query("select D.*,W.names from tbl_data D
        left join tbl_webs W on W.web_id = D.source
        where  DATE(`time_ago`)>=? and DATE(`time_ago`)<=?   $model_filter $where order by $order_by $sort_order limit $offset,$perpage", array($date,$dateEnd));
        $query_total = $this->db->query("select D.id from tbl_data D
        left join tbl_webs W on W.web_id = D.source
        where  DATE(`time_ago`)>=? and DATE(`time_ago`)<=? $model_filter $where order by id desc", array( $date,$dateEnd));

        $results = $query->result();


        $total = $query_total->num_rows();
        $loop=$offset+1;
        foreach($results as $row)
        {
            $description = isset($row->description)?strip_tags($row->description):'';
            if(strlen($description)>300)
            {
                $description = substr($description,0,300).'...';
            }
            $description = htmlentities($description);
            $features='';
            if($type!=3)
            {
                $features = $row->features;
            }
            $status = '<label class="label label-success">Active</label>';
            if($row->is_active==1)
            {
                $status = '<label class="label label-success">Active</label>';
            }elseif($row->is_active==2)
            {
                $status = '<label class="label label-warning">Modified</label>';

            }elseif($row->is_active==0)
            {
                $status = '<label class="label label-danger">Expired</label>';

            }
            $json[]=array(
                $row->id,
                $status,
                $row->ad_id,
                '<a target="_blank" href="'.$row->ad_link.'">Link</a>',
                $row->title,
                $row->make,
                $row->model,
                $row->machine_capacity,
                $row->variant,
                $row->year,
                $row->mileage,
                $row->transmission,
                $row->fuel,
                $row->color,
                $row->amount,
                $features,
                $description,
                $row->owner,
                $row->phone,
                $row->email,
                $row->province,
                $row->city,
                $row->time_ago,
                date('Y-m-d',strtotime($row->created_datetime)),
                $row->seller_type,
                $row->address,
                $row->names,
            );    
            $loop++;
        }




        echo json_encode(array('data' => $json, 'recordsTotal' => ($total), 'recordsFiltered' => ($total)));

    }




}

/* End of file welcome.php */
/* Location: ./application/controlle  rs  /welcome.php */


