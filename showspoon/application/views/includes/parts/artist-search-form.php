<form method="get" action="" id="search-artist">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Select Genre</label>
            <div class="clearfix"></div>
            <select data-placeholder="Please Select" name="genre[]" multiple class="form-control select">
                <?php
                $get_genre = $this->input->get('genre')?$this->input->get('genre',true):[];
                
                if(count($genres)>0){
                    foreach($genres as $key=>$row){
                        $selected='';
                        if(in_array($key,$get_genre)){
                            $selected='selected';
                        }
                        echo '<option '.$selected.' value="'.$key.'">'.$row.'</option>';
                    }
                }

                ?>

            </select>
        </div><!-- form-group -->
    </div><!-- col-lg-3 -->
    <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Select Type</label>
            <div class="clearfix"></div>
            <select  data-placeholder="Please Select" name="band" class="form-control select">
                <option></option>
                <?php
                $band = $this->input->get('band')?$this->input->get('band',true):'';
                
                if(count($band_types)>0){
                    foreach($band_types as $key=>$row){
                        $selected='';
                        if($band==$key){
                            $selected='selected';
                        }
                        echo '<option '.$selected.' value="'.$key.'">'.$row.'</option>';
                    }
                }

                ?>

            </select>
        </div><!-- form-group -->
    </div><!-- col-lg-3 -->
    <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Select City</label>
            <div class="clearfix"></div>
            <select data-placeholder="Please Select" name="city" class="form-control select">
                <option></option>
                <?php
                $city = $this->input->get('city')?$this->input->get('city',true):0;

                
                if(count($cities)>0){
                    foreach($cities as $key=>$row){
                        $selected='';
                        if($city==$key){
                            $selected='selected';
                        }
                        echo '<option '.$selected.' value="'.$key.'">'.$row.'</option>';
                    }
                }

                ?>

            </select>
        </div><!-- form-group -->
    </div><!-- col-lg-3 -->
    <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">

            <input type="submit" value="Search" class="btn btn-primary search-btn btn-block">
        </div><!-- form-group -->
    </div><!-- col-lg-3 -->
    <div class="col-lg-2 col-lg-push-1 col-md-2 col-md-push-1 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Sort By</label>
            <div class="clearfix"></div>
            <?php
            $sort = $this->input->get('sort')?$this->input->get('sort',true):'name';
            
            ?>
            <select id="sort" name="sort" class="form-control">
                <option value="name" <?php if($sort=='name'){echo 'selected';}?> >Name</option>
                <option value="rating"  <?php if($sort=='rating'){echo 'selected';}?>>Rating</option>
            </select>
        </div><!-- form-group -->
    </div><!-- col-lg-3 -->
</form>
<script type="application/javascript">
    $(function(){
        $('#sort').on('change',function(){
           $('form#search-artist').submit();
        });
    });
</script>