<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class artistBooking extends MY_Controller {

    private $loggedintime;
    private $user_data;
    private $data;

    function __construct() {

        parent::__construct();


        $this->data['page_title'] = "";
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model(array('Account_model','Artist_model','Venue_model','Booking_model','Notification_model'));
        //$this->session->set_userdata('showspoon_artist.is_complete',1);



        $this->user_data=$this->Artist_Session();
         $this->data['session_user']=$this->user_data;

    }

    public function index() {

        //$this->load->view('venue/dashboard', $this->data);
    }
    public function check()
    {   
        $user_id = $this->user_data['user_id'];
        $user_email = $this->user_data['user_email']; 
        $hidden  = $this->input->post('hidden'); 
        $this->load->model('admin_model');
        $row = $this->admin_model->getCommission(); 
        $commission = $row->commission; 
        $result  = $this->Booking_model->get_venue_details_for_payment($hidden); 
        if(!empty($_POST['stripeToken']))
        {
            //get token, card and user info from the form
            $token  = $_POST['stripeToken'];
            $user_id=$user_id;
            $booking_id=$result->id;
            $amount=explode('.',$result->amount);  
            $pay = (int)($amount[0] / 100) * (int)$commission; 
            // echo $amount[0] + $pay; exit;
            $name=$result->artist_name;
            $email=$user_email;
            $cc_number=$this->input->post('card_num');
            $exp_month=$this->input->post('exp_month');
            $exp_year=$this->input->post('exp_year');
            $cvc=$this->input->post('cvc');
            $payment_datetime=date('Y-m-d H:i:s',time());
            //include Stripe PHP library
            require_once APPPATH."third_party/stripe/init.php";
            
            //set api key
            $stripe = array(
              "secret_key"      => "sk_test_IPFyAD6YkhFhl6QrqwqYFHus",
              "publishable_key" => "pk_test_NWhYzPP5DpSh6D2xHGQXwQr8"
            );
            
            \Stripe\Stripe::setApiKey($stripe['secret_key']);
            
            //add customer to stripe
            $customer = \Stripe\Customer::create(array(
                'email' => $email,
                'source'  => $token
            ));
            
            //item information
            $itemName = "Stripe Donation";
            $itemNumber = "PS123456";
            $itemPrice = $amount[0] + $pay;
            $currency = "usd";
            $orderID = "SKA92712382139";
            
            //charge a credit or a debit card
            $charge = \Stripe\Charge::create(array(
                'customer' => $customer->id,
                'amount'   => $itemPrice,
                'currency' => $currency,
                'description' => $itemNumber,
                'metadata' => array(
                'item_id' => $itemNumber
                )
            ));
            
            //retrieve charge details
            $chargeJson = $charge->jsonSerialize();

            //check whether the charge is successful
            if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1)
            {
                //order details 
                $amount = $chargeJson['amount'];
                $balance_transaction = $chargeJson['balance_transaction'];
                $currency = $chargeJson['currency'];
                $status = $chargeJson['status'];
                $date = date("Y-m-d H:i:s");
            
                
                //insert tansaction data into the database
                $dataDB = array(
                    'user_id'=>$user_id ,
                    'booking_id'=>$result->id,
                    'amount'=>$itemPrice,
                    'name'=>$result->artist_name,
                    'email'=>$user_email,
                    'cc_number'=>$this->input->post('card_num'),
                    'exp_month'=>$this->input->post('exp_month'),
                    'exp_year'=>$this->input->post('exp_year'),
                    'ccv'=>$this->input->post('cvc'),
                    'payment_datetime'=>date('Y-m-d H:i:s',time()),
                    'status'=>'succeeded',
                    'type'=>'1 to 2'
                );
                $data = $this->Venue_model->transaction($dataDB);
                $this->db->or_where('to_id',$user_id);
                $this->db->or_where('from_id',$user_id);
                $this->db->where(['is_complete'=>1,'btn'=>'success']);
                $this->db->update('bookings',['btn'=>'paid']);
                if ($data > 0) {
                        $user_data = array(
                        'output'=>$result

                        ); 
                    $venue_email_body  = $this->load->view('email/artist_payment_send', $user_data,true);
                    $artist_email_body = $this->load->view('email/venue_payment_receive', $user_data,true);   

                    $this->Send_Mail($result->venue_email,FROM_EMAIL,FROM_NAME,'Payment',$venue_email_body);
                    $this->Send_Mail($result->artist_email,FROM_EMAIL,FROM_NAME,'Payment',$artist_email_body);
                    echo '<div class="alert alert-success alert-dismissible">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Success!</strong> Transaction has been successful.
                          </div>';
                }
                else
                {
                    echo '<div class="alert alert-danger alert-dismissible">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong>Warning!</strong> Transaction has been failed.
                          </div>';
                }

            }
            else
            {
                echo "Invalid Token";
                $statusMsg = "";
            }
        }
    }
    function convertCurrency($amount, $from, $to){
        $conv_id = "{$from}_{$to}";
        $string = file_get_contents("http://free.currencyconverterapi.com/api/v3/convert?q=$conv_id&compact=ultra");
        $json_a = json_decode($string, true);

        return $amount * round($json_a[$conv_id], 2);
    }
    public function book($id=0){
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $this->data['image'] = $this->user_data['profile_image']?$this->user_data['profile_image']:0;
        $venue = $this->Venue_model->get_venue_details_by_id($id); //echo $venue->email;
        $this->data['request'] = $this->Booking_model->get_artist_booking_count($user_id);
        $artist_sql = $this->db->query("select name,email from artist where user_id=$user_id");
        $sql = $this->db->query("select * from artist inner join artist_details on artist_details.artist_id = artist.artist_id where artist.user_id = $user_id");
        $currency = $sql->row()->artist_currency;
        //dd($venue);
        if(!isset($venue->user_id)){
            show_404();
        }
        $this->data['venue']=$venue;  
        if($this->input->post("submit")=="Sent Request"){ 
            
            $datetime = $this->Booking_model->check_artist_today_date($user_id,$this->input->post('event_date')); 
            $today = date('m/d/Y');
            $datetime = date('m/d/Y',strtotime($datetime));
            if($datetime==$today){
                $hidden = $this->input->post('id');
                $this->session->set_flashdata('error','You have already booked the venue on this date please select another date');
                redirect(site_url("artistBooking/book/$hidden"));
            }
            else{  
            $this->load->model('admin_model');
            $commission = $this->admin_model->getCommission();
            $amount = explode('.',$this->input->post('venue_rate'));
            //$pay = $amount[0]+(($amount[0] / 100) * $commission->commission); 
            $pay = $this->convertCurrency($this->input->post('venue_amount'), $this->input->post('venue_currency'), $currency) + (($this->convertCurrency($this->input->post('venue_amount'), $this->input->post('venue_currency'), $currency)) / 100 * $commission->commission); 
            $start_time = $this->input->post('start_time');
            $end_time   = $this->input->post('end_time'); 


            $booking=[
                'event_date'=>$this->input->post('event_date')?$this->input->post('event_date'):'',
                'start_time'=>$start_time,
                'end_time'=>$end_time,
                //'sound_check'=>$this->input->post('sound-check')?$this->input->post('sound-check'):'',
                'message'=>$this->input->post('message')?$this->input->post('message'):'',
                'from_id'=>$user_id,
                'to_id'=>$venue->user_id,
                'amount'=>$pay,
                'type'=>1,
                'status'=>1,
                'btn'=>'pending'

            ];  
            
            $booking_id =$this->Booking_model->create_booking($booking);
            $notification=[
                'type'=>1,
                'message'=>REQUEST_SENT,
                'from_id'=>$user_id,
                'to_id'=>$venue->user_id?$venue->user_id:0,
                'booking_id'=>$booking_id,
                'status'=>'request'
            ]; 
            $notification_id =$this->Notification_model->create_notification($notification);
            $email_data = array(
                         'artist'=>$artist_sql->row()->name,
                         'venue'=>$venue->name
                      );
            $artist_email_body = $this->load->view('email/send_request_from_artist', $email_data,true);   
            $this->Send_Mail($venue->email,FROM_EMAIL,FROM_NAME,'New Request',$artist_email_body);
            //dd($booking);
            //$this->load->view('booking/thankyou', $this->data);
            $this->load->view('booking/thanks', $this->data);
           }
        }else{
            //$this->load->view('booking/thankyou', $this->data);
            // $this->load->view('booking/artist-booking', $this->data);   
            $this->load->view('booking/invite_artist', $this->data); 
        }



    } 
    public function my_requests(){


        //$this->load->library("pagination"); 
        $user_email = $this->user_data['user_email']; 
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $sql = $this->db->query("select * from artist inner join artist_details on artist_details.artist_id = artist.artist_id where artist.user_id = $user_id");
        $this->data['currency'] = $sql->row()->artist_currency;
        $this->data['image'] = $this->user_data['profile_image']?$this->user_data['profile_image']:0;
        /*$filter=array(
            'genre'=>$genre,
            'city_id'=>$city,
            'band_type_id'=>$band,
        );*/
        // $this->data['genres'] = $this->Artist_model->get_genres();
        $filter=[
            //'is_complete'=>0,
            'from_id'=>$user_id,
            'type'=>1
        ];
        $received=[
            
            'to_id'=>$user_id,
            'type'=>2
        ];

        $decline=[
            
            'to_id'=>$user_id,
            'type'=>2,
            'decline'=>1
        ];

        $this->load->model('admin_model');
        $row = $this->admin_model->getCommission(); 
        $this->data['data'] = $row;
        $this->data['user_email'] = $user_email;
        // $sort='id';
        // $page = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
        // $count_record=$this->Booking_model->get_venue_bookings_count($filter);
        // $config["base_url"] = site_url() . "/artist/search/";
        // $config["total_rows"] = $count_record; 
        // $config["per_page"] = 9;
        // $config["uri_segment"] = 3;
        // $choice = $config["total_rows"] / $config["per_page"];
        // $config["num_links"] = round($choice);

        // $custom_pagination = $this->Artist_model->ci_custom_pagination($config["base_url"], $config["total_rows"], $config["per_page"], $config["uri_segment"]);

        // $offset = $page*$config["per_page"];
        // $this->pagination->initialize($custom_pagination);
        // $this->data['requests']=$this->Booking_model->get_venue_bookings($filter,$offset,$config["per_page"],$sort,'DESC');
        // //dd($this->data['artists']);
        // $this->data["links"] = $this->pagination->create_links();
        $this->data['id'] = $user_id;
        $this->data['status'] = $this->Booking_model->get_artist_status($user_id);
        $this->data['requests'] = $this->Booking_model->get_venue_booking($filter);
        $this->data['received'] = $this->Booking_model->get_venue_booking($received);
        $this->data['request']  = $this->Booking_model->get_artist_booking_count($user_id);
        $this->data['decline'] = $this->Booking_model->get_venue_decline_by_id($decline);
        $this->load->view('booking/artist_requests', $this->data);

        //$this->load->view('booking/my_requests', $this->data);    

    }
    public function inbox(){
        
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        // $received=[
            
        //     'to_id'=>$user_id,
        //     'type'=>2
        // ]; 
        $this->data['id'] = $user_id;
        $this->data['list'] = $this->Booking_model->getVenueName();
        $this->data['image'] = $this->user_data['profile_image']?$this->user_data['profile_image']:0;
        $this->load->view('inbox/artist_inbox',$this->data);
    }
    public function getInboxCount(){
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $inbox_count = $this->Booking_model->inboxArtistCount($user_id);
        echo $inbox_count->count;
    }
    public function getTrashCount(){
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $trash_count = $this->Booking_model->trashArtistCount($user_id);
        echo $trash_count->count;
    }
    public function inboxArtistRead(){
        $id = $this->input->post("id");
        $this->db->where('id',$id);
        $this->db->update('chat',['is_read'=>1,'recd'=>0]);
        $this->db->where('chat_id',$id);
        $this->db->update('notifications',['is_read'=>1]);
    }
    public function inbox_cnt(){
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $sql = $this->db->query("select COUNT(*) AS count from chat C where C.to=$user_id and C.is_delete=0 and C.is_read=1");
        echo $sql->row()->count;
    }
    public function getInboxResult(){
        $output = '';
        $active = '';
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        if($this->input->post('page')){
            $page = $this->input->post('page');
        }else{
            $page = 1;
        }
        $per_page = 10;
        $offset = ($page - 1) * $per_page;
        $inbox_count = $this->Booking_model->inboxArtistCount($user_id);
        $received = $this->Booking_model->chat_artist_received($user_id,$offset,$per_page); 
        //$received = $this->Booking_model->get_n($user_id,$offset,$per_page); //echo '<pre>'; print_r($received); exit;
        if(count($received) > 0 && $received!=""){ //isset($row->count)&&$row->count!=""? '(' .$row->count. ') ' :''
            $output .= '<div class="col-md-9">
                <div class="spacer">
                    <div class="row">
                      <div class="col-sm-4 col-sm-push-8">
                      <button class="btn btn-sm btn-danger pull-right" id="delete" title="Delete" style="display:none;"><span class="glyphicon glyphicon-trash"></span></button>
                      </div>';
        $total = ceil($inbox_count->count / $per_page); 
        $one = 1;   
        if($page < $total){
            $next = $page + 1;
            $one  = ($page * $next) - 1;
            $two  = ($per_page * $next) - $per_page;
        }else{
            $next = $total; 
            $two  = $inbox_count->count;
        }
        if($page > 1){
            $prev = $page - 1;
        }else{
            $prev = 1;
        } 

        $output .=    '<div class="col-sm-8 col-sm-pull-4">
                            <div class="btn-group inbox_pagination_wrap" role="group" aria-label="...">
                              <button type="button" class="btn btn-sm btn-default" id="prev" data-id="'.$prev.'"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></button>
                              <button type="button" class="btn btn-sm btn-default" id="next" data-id="'.$next.'"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button> &nbsp; <span class="info">'.$one.'-'.$two.' of '.$inbox_count->count.'</span>
                            </div> 
                        </div>

                    </div>
                </div>
            ';
            foreach($received as $row){ if($row->is_read==0){$active = "active_class";}else{$active = "";}
            $output .= '<div class="inbox_row clearfix" id="remove'.$row->id.'">'; 
            if($row->recd==1){$output .= '<div class="inbox_row_col_one" style="width:7%;"><span class="badge gold-badge badge'.$row->id.'">new</span></div>';}else{$output .= '';}              
            $output .= '<div class="inbox_row_col_two '.$active.'" style="width:15%;text-align:center;">
                          '.$row->venue_name.' 
                          </div>
                          <div class="inbox_row_col_three '.$active.'" style="width:20%;text-align:center;">
                          '.substr($row->subject,0,20).'...
                          </div>
                          <div class="inbox_row_col_four '.$active.'" style="width:35%;text-align:center;">
                          '.substr($row->message,0,30).'...
                          </div>
                          <div class="inbox_row_col_five '.$active.'">
                          '.date('H:i a',strtotime($row->sent)).'
                          </div>
                          <div style="cursor:pointer;" class="new inbox_row_col_six" data-toggle="collapse" data-target="#collapsable_sent_req_one'.$row->id.'" data-new="'.$row->id.'">
                          <i class="fa fa-caret-right" aria-hidden="true"></i> view details
                          </div>
                          <div class="inbox_row_col_seven">
                          <input type="checkbox" name="" class="check_box" value="'.$row->id.'">
                          </div> 
                          </div>';
           $output .= '<div id="collapsable_sent_req_one'.$row->id.'" class="collapse">
                       <div class="req_parent_row_collapse">
                       <div class="row">
                       <div class="col-sm-4">
                       <div style="width:40px;height:40px;float:left;">
                       <img src="'.site_url().'uploads/users/thumb/'.$row->image.'" class="img-circle" style="width:40px;height:40px;">
                       </div>
                       <div style="margin-left:5px;float:left;width:160px;height:40px;">
                       <span style="display:block;font-weight:bold;">'.$row->venue_name.'</span> to me
                       </div>
                       </div>
                       <div class="col-sm-4">
                       <p>'.$row->message.'</p>
                       </div>
                       <div class="col-sm-2">
                       '.date('jS M H:i a',strtotime($row->sent)).'
                       </div>
                       <div class="col-sm-2">
                       <a href="javascript:;" data-id="'.$row->id.'" data-artist="'.$user_id.'" data-venue="'.$row->venue_id .'" data-subject="'.$row->subject.'" data-name="'.$row->venue_name.'" style="color:#323232;" id="reply_msg" title="reply"><i class="fas fa-reply"></i></a> &nbsp;&nbsp;
                       <a href="javascript:;" style="color:#323232;" id="delete_msg" data-delete="'.$row->id.'" title="delete"><i class="fas fa-trash-alt"></i></a>
                       </div>
                       </div>
                       <span class="txt_box'.$row->id.'"></span>
                       <span class="data_alert'.$row->id.'"></span>
                       </div>
                       </div>';
        }
        }
        else{
            $output .= '<p style="text-align:center;">No messages to show.</p>';
        }
        $output .= '</div>';
        $result = array(
             'inbox_count' => $inbox_count->count,
             'output' => $output
        );
        echo json_encode($result);
    }
    public function send_msg(){
        $output = '';
        $id = $this->input->post('id');
        $from = $this->input->post('artist');
        $to = $this->input->post('venue');
        $subject = $this->input->post('subject');
        $name = $this->input->post('name');
        $message =$this->input->post('message');
        $artist_sql = $this->db->query("select name,email from artist where user_id=$from");
        $venue_sql = $this->db->query("select name,email from venues where user_id=$to");
        $insert_arr = array(
            'from'=>$from,
            'to'=>$to,
            'subject'=>$subject,
            'message'=>$message,
            'recd'=>1
        );
        $this->db->insert('chat',$insert_arr);
        $insert_id = $this->db->insert_id();
        if($insert_id){
        	$notification=[
                'type'=>1,
                'message'=>'New message has been received',
                'from_id'=>$from,
                'to_id'=>$to,
                'status'=>'message',
                'chat_id'=>$insert_id
            ]; 
            $notification_id =$this->Notification_model->create_notification($notification);
            $email_data = array(
                         'artist'=>$artist_sql->row()->name,
                         'venue'=>$venue_sql->row()->name
                      );
            $artist_email_body = $this->load->view('email/send_message_from_artist', $email_data,true);   
            $this->Send_Mail($venue_sql->row()->email,FROM_EMAIL,FROM_NAME,'New Message',$artist_email_body);
            $output .= '<div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Success!</strong> Your message has been sent successfully
                  </div>';
        }
        echo json_encode(array('output'=>$output,'id'=>$id));
    }
    public function delete_msg(){
        $id = $this->input->post('row_id');
        $this->db->where('id',$id);
        $this->db->delete('chat');
    }
    public function deleteMessages(){
        $id = $this->input->post('id')?$this->input->post('id'):[];
        foreach($id as $val){
            $this->db->where('id',$val);
            $this->db->update('chat',array('is_delete'=>1));
        }
        echo json_encode(array('result'=>true));
    }
    public function trash(){
    	$user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $this->data['image'] = isset($this->user_data['profile_image'])?$this->user_data['profile_image']:'';
        $this->data['id'] = $user_id;
        $this->data['list'] = $this->Booking_model->getVenueName();
        $this->load->view('inbox/artist_trash',$this->data);
    }
    public function getTrashResult(){ 
        $output = '';
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        if($this->input->post('page')){
            $page = $this->input->post('page');
        }else{
            $page = 1;
        }
        $per_page = 10;
        $offset = ($page - 1) * $per_page;
        $trash_count = $this->Booking_model->trashArtistCount($user_id);
        $received = $this->Booking_model->trash_artist($user_id,$offset,$per_page); 
        if(count($received) > 0 && $received!=""){ //isset($row->count)&&$row->count!=""? '(' .$row->count. ') ' :''
            $output .= '<div class="col-md-9">
                <div class="spacer">
                    <div class="row">
                      <div class="col-sm-4 col-sm-push-8">
                      <button class="btn btn-sm btn-danger pull-right" id="trash" title="Trash" style="display:none;"><span class="glyphicon glyphicon-trash"></span></button>
                      </div>';
        $total = ceil($trash_count->count / $per_page); 
        $one = 1;   
        if($page < $total){
            $next = $page + 1;
            $one  = ($page * $next) - 1;
            $two  = ($per_page * $next) - $per_page;
        }else{
            $next = $total; 
            $two  = $trash_count->count;
        }
        if($page > 1){
            $prev = $page - 1;
        }else{
            $prev = 1;
        } 

        $output .=    '<div class="col-sm-8 col-sm-pull-4">
                            <!-- inbox pagination -->
                            <div class="btn-group inbox_pagination_wrap" role="group" aria-label="...">
                              <button type="button" class="btn btn-sm btn-default" id="prev" data-id="'.$prev.'"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></button>
                              <button type="button" class="btn btn-sm btn-default" id="next" data-id="'.$next.'"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button> &nbsp; <span class="info">'.$one.'-'.$two.' of '.$trash_count->count.'</span>
                            </div> 
                        </div>

                    </div>
                </div>
            ';
            foreach($received as $row){
            $output .= '<div class="inbox_row clearfix" id="remove'.$row->id.'">
                          <div class="inbox_row_col_one" style="width: 0%;">';  
            if($row->recd==1){$output .= '<span class="badge gold-badge">new</span>';}else{$output .= '';}              
            $output .=    '</div>
                          <div class="inbox_row_col_two" style="width: 20%;">
                          '.$row->venue_name.' 
                          </div>
                          <div class="inbox_row_col_three" style="width: 27%;">
                          '.date('j M Y',strtotime($row->sent)).'
                          </div>
                          <div class="inbox_row_col_four" style="width: 2%;">
                          </div>
                          <div class="inbox_row_col_five" style="width: 30%;">
                          
                          '.$row->message.'
                          </div>
                          <div class="inbox_row_col_seven" style="width: 5%;">
                          <input type="checkbox" name="" class="check_box" value="'.$row->id.'">
                          </div> 
                          </div>';
        }
        }
        else{
            $output .= '<p style="text-align:center;">No messages to show.</p>';
        }
        $output .= '</div>';
        //,COUNT(A.name) AS count
        $result = array(
             'trash_count' => $trash_count->count,
             'output' => $output
        );
        echo json_encode($result);
    }
    public function sendMessage(){ 
        $output = ''; 
        $from = $this->input->post('from');
        $to   = $this->input->post('to');
        $subject   = $this->input->post('subject');
        $message   = $this->input->post('message_body');
        $artist_sql = $this->db->query("select name,email from artist where user_id=$from");
        $venue_sql = $this->db->query("select name,email from venues where user_id=$to");
        $msg_array = array(
             'from'    => $from,
             'to'      => $to,
             'subject' => $subject,
             'message' => $message,
             //'recd'    => 1
        );
        $this->db->insert('chat',$msg_array);
        $insert_id = $this->db->insert_id();
        if($insert_id){
        	$notification=[
                'type'=>1,
                'message'=>'New message has been received',
                'from_id'=>$from,
                'to_id'=>$to,
                'status'=>'message',
                'chat_id'=>$insert_id
            ]; 
            $notification_id =$this->Notification_model->create_notification($notification);
            $email_data = array(
                         'artist'=>$artist_sql->row()->name,
                         'venue'=>$venue_sql->row()->name
                      );
            $artist_email_body = $this->load->view('email/send_message_from_artist', $email_data,true);   
            $this->Send_Mail($venue_sql->row()->email,FROM_EMAIL,FROM_NAME,'New Message',$artist_email_body);
            $output .= '<div class="alert alert-success">Your message has been sent.</div>';
        }else{
            $output .= '<div class="alert alert-danger">Message failed to sent.</div>';
        }
        echo $output;
    }
    public function trashForever(){
        $id = $this->input->post('id')?$this->input->post('id'):[];
        foreach($id as $val){
            $this->db->where('id',$val);
            $this->db->delete('chat');
        }
        echo json_encode(array('result'=>true));
    }
    public function getVenueNotification(){
    	$output = '';
    	$user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
    	$data = $this->Notification_model->get_venue_notification($user_id); 
    	$output .= '<div style="width:200px;position:fixed;top:30px;right:20px;z-index:999999;">';
    	foreach($data as $item){
    		$output .= '<div class="alert alert-success alert-dismissible" id="notification'.$item->chat_id.'"><a href="#" class="close" data-dismiss="alert" aria-label="close" data-id="'.$item->id.'" data-mr="'.$item->status.'">&times;</a>'.$item->message.'</div>';
    	}
    	$output .= '</div>';
    	echo $output;
    }
    public function deleteVenueNotification(){
    	$this->Notification_model->delete_venue_notification($this->input->post('id')); 
    }
    public function getVenueRequestCount(){
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $data = $this->Notification_model->get_venue_request_count($user_id); 
        echo json_encode($data);
    }
    public function getVenueInboxCount(){
    	$user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $data = $this->Notification_model->get_venue_inbox_count($user_id); 
        echo json_encode($data);
    }
    public function charge(){ 
        require_once('config.php');
        $token  = $_POST['stripeToken'];
        $email  = $_POST['stripeEmail'];
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $user_email = $this->user_data['user_email'];
        $amount = $_POST['amount'];
        $id = $_POST['id'];
        $venue_id = $this->input->post('venue_id');
        $sql = $this->db->query("select name from artist where user_id=$user_id");
        $venue_sql = $this->db->query("select name,email from venues where user_id=$venue_id");
        $name = $sql->row()->name; 
        $customer = \Stripe\Customer::create(array(
              'email' => $email,
              'source'  => $token
        ));

        $charge = \Stripe\Charge::create(array(
              'customer' => $customer->id,
              'amount'   => $amount * 100,
              'currency' => 'usd'
        ));
        if($charge){
            $dataDB = array(
                'name'      => $name,
                'email'     => $email,
                'user_id'   => $user_id,
                'user'      => $venue_id,
                'booking_id'=> $id,
                'amount'    => $amount,
                'cc_number' => '',
                'exp_month' => '',
                'ccv'       => '',
                'status'    => 'succeeded',
                'type'      => '1 to 2',
                'token'     => $token
            );
          $data = $this->Venue_model->transaction($dataDB);
          if($data){
            $this->db->where('id',$id)->update('bookings',['btn'=>'paid']);
            $email_data = array(
                 'artist'=>$name,
                 'venue'=>$venue_sql->row()->name
              );
            $artist_email_body = $this->load->view('email/booked_request_from_artist', $email_data,true);   
            $this->Send_Mail($venue_sql->row()->email,FROM_EMAIL,FROM_NAME,'Booked',$artist_email_body);
            $this->db->where('to_id',$user_id);
            $this->db->update('notifications',['is_read'=>1]);
            $notification=[
                    'type'=>1,
                    'message'=>'Booking Confirmed',
                    'from_id'=>$user_id,
                    'to_id'=>$venue_id,
                    'status'=>'request'
                ]; 
            $notification_id =$this->Notification_model->create_notification($notification);
            $this->session->set_flashdata("success","You have successfully booked the venue (".$venue_sql->row()->name.")");
            redirect('artistBooking/my_requests');
          }
        }
    }
    public function getVenueRequests(){
        
        $output = '';
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $received=[
            
            'to_id'=>$user_id,
            'type'=>2
        ];

        $this->load->library("pagination");
          $config = array();
          $config["base_url"] = "#";
          $config["total_rows"] = $this->Booking_model->venues_requests_count($received);
          $config["per_page"] = 10;
          $config["uri_segment"] = 3;
          $config["use_page_numbers"] = TRUE;
          $config["full_tag_open"] = '<ul class="pagination">';
          $config["full_tag_close"] = '</ul>';
          $config["first_tag_open"] = '<li>';
          $config["first_tag_close"] = '</li>';
          $config["last_tag_open"] = '<li>';
          $config["last_tag_close"] = '</li>';
          $config['next_link'] = '&gt;';
          $config["next_tag_open"] = '<li>';
          $config["next_tag_close"] = '</li>';
          $config["prev_link"] = "&lt;";
          $config["prev_tag_open"] = "<li>";
          $config["prev_tag_close"] = "</li>";
          $config["cur_tag_open"] = "<li class='active'><a href='#'>";
          $config["cur_tag_close"] = "</a></li>";
          $config["num_tag_open"] = "<li>";
          $config["num_tag_close"] = "</li>";
          //$config["num_links"] = 1;
          $this->pagination->initialize($config);
          $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
          $start = ($page - 1) * $config["per_page"];

          $output = array(
           'links'  => $this->pagination->create_links(),
           'data'   => $this->Booking_model->get_venues_booking($received,$start,$config["per_page"],$user_id)
          );
          echo json_encode($output);
    }
    public function artistDecline(){
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $artist_sql = $this->db->query("select name from artist where user_id=$user_id");
        $artist = $artist_sql->row()->name;
        $id = $this->input->post('id');
        $venue = $this->input->post('venue');
        $venue_sql = $this->db->query("select name,email,user_id from venues where venue_id=$venue");
        $venue = $venue_sql->row()->name;
        $user_data = array(
          'artist'=>$artist,
          'venue'=>$venue

        );
        $artist_email_body = $this->load->view('email/cancel_from_artist', $user_data,true);   
        $this->Send_Mail($venue_sql->row()->email,FROM_EMAIL,FROM_NAME,'Cancel Request',$artist_email_body);
        $this->Booking_model->artistDecline($id);
        $notification=[
                'type'=>1,
                'message'=>'Request has been cancelled',
                'from_id'=>$user_id,
                'to_id'=>$venue_sql->row()->user_id,
                'status'=>'request'
            ]; 
        $notification_id =$this->Notification_model->create_notification($notification);
    }
    public function btn_status(){
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $from_id = $this->input->post('from_id');
        $id   = $this->input->post('status');
        $artist_sql = $this->db->query("select name,email from artist where user_id=$user_id");
        $venue_sql = $this->db->query("select name,email from venues where user_id=$from_id");
        $this->db->where('id',$id)->update('bookings',['btn'=>'success']);
        $email_data = array(
                 'artist'=>$artist_sql->row()->name,
                 'venue'=>$venue_sql->row()->name
              );
        $artist_email_body = $this->load->view('email/accept_request_from_artist', $email_data,true);   
        $this->Send_Mail($venue_sql->row()->email,FROM_EMAIL,FROM_NAME,'Request Accepted',$artist_email_body);
        $notification=[
                'type'=>1,
                'message'=>'Request has been accepted',
                'from_id'=>$user_id,
                'to_id'=>$from_id,
                'status'=>'request'
            ]; 
        $notification_id =$this->Notification_model->create_notification($notification);
        $this->db->where('id',$id);
        $this->db->update('bookings',['status'=>1]);
    }
    public function artist_booked_image(){

        $output = '';
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $type = $this->input->post('type');
        $id = $this->input->post('id');
        $filter=[

            'to_id'=>$id,
            'type'=>$type
        ]; 
        $data = $this->Booking_model->artist_booked_image($filter); 
        if(count($data) > 0){
        $output .= '<div class="venue_info_box">
                    <figure>
                    <img src="'.site_url().'uploads/users/thumb/'.$data->profile_image.'" alt="Thumb">
                    <i class="fa fa-heart" aria-hidden="true"></i>
                    </figure>
                    <figcaption class="light-grey-bg">'.$data->name.'<br/>';
        $names = array();
        $explode = explode(',',$data->genre); 
        foreach($explode as $exp)
        {   
            $this->load->model('Artist_model');
            $exp_names = $this->Artist_model->getGenreNames($exp);
            foreach($exp_names as $exp_name)
            {
                $names[] = $exp_name->name;
            }
        } 
        $output .=  '<small>'.implode(',',$names).'</small>';
        $output .=  '<br/>';
        
        $output .= '<a href="javascript:;" class="action_btn_white req_accepted">Booked</a>
                    </figcaption>
                    </div>';
        
            }else{$output .= 'No request received';}
        echo $output;

    }
    public function hide(){
        $output = '';
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $type = $this->input->post('type');
        $filter=[

            'to_id'=>$user_id,
            'type'=>$type
        ]; 
        $data = $this->Booking_model->hide($filter); 
        if($data!=""){
        $sql = $this->db->query("select favourites from users where user_id=$data->user_id");
        $row = $sql->row();
        $exp = explode(',',$row->favourites);
        }
        if(count($data) > 0){
        $output .= '<div class="venue_info_box">
                    <figure>
                    <img src="'.site_url().'uploads/users/thumb/'.$data->profile_image.'" alt="Thumb">';
        if(in_array($data->venue_id,$exp)){
        $output .=  '<i class="fa fa-heart" aria-hidden="true"></i>';
        }else{$output .= '';}
        $output .=  '</figure>
                    <figcaption class="light-grey-bg">'.$data->name.'<br/>';
        $names = array();
        $explode = explode(',',$data->genre); 
        foreach($explode as $exp)
        {   
            $this->load->model('Artist_model');
            $exp_names = $this->Artist_model->getGenreNames($exp);
            foreach($exp_names as $exp_name)
            {
                $names[] = $exp_name->name;
            }
        } 
        $output .=  '<small>'.implode(',',$names).'</small>';
        $output .=  '<br/>';
                    if($data->btn=="pending"){
        $output    .= '<span class="m-badge  m-badge--pending m-badge--wide">
                          Pending
                          </span>
                    </figcaption>
                    </div>';
                }else if($data->btn=="paid"){
        $output    .= '<span class="m-badge  m-badge--success m-badge--wide" style="">
                            Booked
                            </span>
                    </figcaption>
                    </div>';
                }else if($data->btn=="cancelled"){
        $output    .= '<span class="m-badge  m-badge--cancelled m-badge--wide" style="">
                      Cancelled
                      </span>
                    </figcaption>
                    </div>';
                }else{
        $output .= '<span class="m-badge  m-badge--awaitingpayment m-badge--wide">
                          Awaiting Payment
                          </span>
                    </figcaption>
                    </div>';
                }
            }else{$output .= '<div class="spacer_big">
                                <div class="venue_info_box">
                                <figure>
                                <img src="'.site_url('assets/img_not_found.jpeg').'" alt="Thumb">
                                </figcaption>
                                </div>
                              </div>';}
        echo $output;
    }
    public function get_venue_booked_image(){
        $output = '';
        $id = $this->input->post('id');
        $data = $this->Booking_model->get_venue_image($id); 
        $output .= '<div class="venue_info_box">
                    <figure>
                    <img src="'.site_url().'uploads/users/thumb/'.$data->profile_image.'" alt="Thumb">
                    <i class="fa fa-heart" aria-hidden="true"></i>
                    </figure>
                    <figcaption class="light-grey-bg">'.$data->name.'<br/>';
        $names = array();
        $explode = explode(',',$data->genre); 
        foreach($explode as $exp)
        {   
            $this->load->model('Artist_model');
            $exp_names = $this->Artist_model->getGenreNames($exp);
            foreach($exp_names as $exp_name)
            {
                $names[] = $exp_name->name;
            }
        } 
        $output .=  '<small>'.implode(',',$names).'</small>';
        $output .=  '<br/>';
        $output .= '<a href="javascript:;" class="action_btn_white req_accepted">Booked</a>
                    </figcaption>
                    </div>';
        echo $output;
    }
    public function get_venue_image(){
        $output = '';
        $id = $this->input->post('id');
        $this->db->where("id",$id);
        $this->db->update("bookings",["status"=>0]);
        $new = $this->Booking_model->badgee($id);
        $data = $this->Booking_model->get_venue_image($id); 
        $sql = $this->db->query("select favourites from users where user_id=$data->user_id");
        $row = $sql->row();
        $exp = explode(',',$row->favourites);
        $output .= '<div class="venue_info_box">
                    <figure>
                    <img src="'.site_url().'uploads/users/thumb/'.$data->profile_image.'" alt="Thumb">';
        if(in_array($data->venue_id,$exp)){
        $output .= '<i class="fa fa-heart" aria-hidden="true"></i>';
    }else{$output .= '';}
        $output .= '</figure>
                    <figcaption class="light-grey-bg">'.$data->name.'<br/>';
        $names = array();
        $explode = explode(',',$data->genre); 
        foreach($explode as $exp)
        {   
            $this->load->model('Artist_model');
            $exp_names = $this->Artist_model->getGenreNames($exp);
            foreach($exp_names as $exp_name)
            {
                $names[] = $exp_name->name;
            }
        } 
        $output .=  '<small>'.implode(',',$names).'</small>';
        $output .=  '<br/>';
                    if($data->btn=="pending"){
        $output    .= '<span class="m-badge  m-badge--pending m-badge--wide">
                          Pending
                          </span>
                    </figcaption>
                    </div>';
                }else if($data->btn=="paid"){
        $output    .= '<span class="m-badge  m-badge--success m-badge--wide">
                          Booked
                          </span>
                    </figcaption>
                    </div>';
                }else if($data->btn=="cancelled"){
        $output    .= '<span class="m-badge  m-badge--cancelled m-badge--wide">
                          Cancelled
                          </span>
                    </figcaption>
                    </div>';
                }else{
        $output .= '<span class="m-badge  m-badge--awaitingpayment m-badge--wide">
                          Awaiting Payment
                          </span>
                    </figcaption>
                    </div>';
                }
        echo $output;
    }
    public function request_received(){



        $this->load->library("pagination");
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;

        /*$filter=array(
            'genre'=>$genre,
            'city_id'=>$city,
            'band_type_id'=>$band,
        );*/
        $this->data['genres'] = $this->Artist_model->get_genres();
        $filter=[
            'is_complete'=>0,
            'to_id'=>$user_id,
            //'type'=>1
        ];

        $sort='id';
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
        $count_record=$this->Booking_model->get_venue_bookings_count($filter);
        $config["base_url"] = site_url() . "/artist/search/";
        $config["total_rows"] = $count_record; 
        $config["per_page"] = 9;
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);

        $custom_pagination = $this->Artist_model->ci_custom_pagination($config["base_url"], $config["total_rows"], $config["per_page"], $config["uri_segment"]);

        $offset = $page*$config["per_page"];
        $this->pagination->initialize($custom_pagination);
        $this->data['requests']=$this->Booking_model->get_venue_bookings($filter,$offset,$config["per_page"],$sort,'ASC');
        //dd($this->data['artists']);
        $this->data["links"] = $this->pagination->create_links();




        $this->load->view('booking/request_received', $this->data);    




    }

    public function chat($id=0){
        @session_start();
        $this->load->model('Chat_model');
        //$this->session->set_userdata('last_id',0);
        $_SESSION['last_id']=0;
        //dd($this->user_data);
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;

        $booking = $this->Booking_model->get_booking_by_id($id);
        if(!isset($booking->id)){
            show_404();
        }elseif($booking->from_id!=$user_id && $booking->to_id!=$user_id){
            show_404();
        }
        $type = $booking->type;
        $from_id = $booking->from_id;
        $to_id = $booking->to_id;
        if($type==1){
            $artist_id = $from_id;
        }else{
            $artist_id = $to_id;
        }
        if($type==2){
            $venue_id = $from_id;
        }else{
            $venue_id = $to_id;
        }
        if(isset($booking->type) && $booking->type==1)
        {
            $this->data['to_id']=$booking->to_id;
        }else{
            $this->data['to_id']=$booking->from_id;
        }
        $filter=[
            'book_id'=>$booking->id,
            //'last_id'=>$last_id,
            'type'=>$type
        ];
        $html='';


        $this->data['chat']=$chat = $this->Chat_model->get_artist_chat($filter);
        $this->data['artist_details']=$artist_details=$this->Artist_model->get_artist_details($artist_id);
        $this->data['venue_details']=$venue_details=$this->Venue_model->get_venue_details($venue_id);
        $this->data['partner']=$venue_details;
        $this->data['booking']=$booking;
        $this->data['genres'] = $this->Artist_model->get_genres();
        $this->data['session_user'] = $this->user_data;
        //dd($this->data);
        $update=[
            'recd'=>1,
            'to'=>$user_id,
            'book_id'=>$booking->id,
        ];
        $this->Chat_model->update_chat($update);
        $this->load->view('inbox/artist_chat', $this->data);
        // $this->load->view('booking/chat', $this->data);    
    }
    public function message(){
        $this->load->library("pagination");
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $this->data['image'] = isset($this->user_data['profile_image'])?$this->user_data['profile_image']:'';
        // $filter=[
        //     'from_id'=>$user_id,
        //     'type'=>1
        // ];

        // $this->data['requests'] = $this->Booking_model->get_venue_booking($filter); 
        $this->data['list'] = $this->Booking_model->getVenueName(); 
        // $this->data['requests'] = $this->Booking_model->get_venue_message($user_id);
        $this->data['id'] = $user_id;
        $this->load->view('inbox/artist_message',$this->data);
    }
    public function getVenueMessage(){
    	$output = "";
    	$user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        if($this->input->post('page')){
            $page = $this->input->post('page');
        }else{
            $page = 1;
        }
        $per_page = 10;
        $offset = ($page - 1) * $per_page;
        $req_count = $this->Booking_model->getVenueReqCount($user_id);
        $requests = $this->Booking_model->get_venue_message($user_id,$offset,$per_page);
        $output .= '<div class="col-md-9">                
	                <div class="spacer">
	                    <div class="row">

	                      <div class="col-sm-4 col-sm-push-8">
                        </div>';
        $total = ceil($req_count / $per_page); 
        $one = 1;   
        if($page < $total){
            $next = $page + 1;
            $one  = ($page * $next) - 1;
            $two  = ($per_page * $next) - $per_page;
        }else{
            $next = $total; 
            $two  = $req_count;
        }
        if($page > 1){
            $prev = $page - 1;
        }else{
            $prev = 1;
        } 
        $output .= '<div class="col-sm-8 col-sm-pull-4">
                            <!-- inbox pagination -->
                            <div class="btn-group inbox_pagination_wrap" role="group" aria-label="...">
                              <button type="button" class="btn btn-sm btn-default" id="prev" data-id="'.$prev.'"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></button>
                              <button type="button" class="btn btn-sm btn-default" id="next" data-id="'.$next.'"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button> &nbsp; <span class="info">'.$one.'-'.$two.' of '.$req_count.'</span>
                            </div>
                        </div>
                        
                    </div>
                </div>';
          if(count($requests) > 0){ foreach($requests as $row){ 
              
          $output .=   '<div class="req_parent_row clearfix">
                      <div class="req_col_badge" style="width:0%;font-weight: 100;">
                        <!-- <span class="badge gold-badge">new</span> -->
                      </div> 
                      <div class="req_col_title" style="width:30%;font-weight: 100;"> 
                      '.$row->subject.'
                      </div> 
                      <div class="req_col_date" style="width:0%;font-weight: 100;">
                      '.substr($row->message,0,12).'...'.'
                      </div>
                      <div class="req_col_time" style="width:25%;font-weight: 100;">
                      '.date('jS M, Y',strtotime($row->sent)).'
                      </div>
                      <div class="req_col_date" style="width:0%;font-weight: 100;">
                      </div>
                      
                      <div class="req_col_decision" style="width:20%;font-weight: 100;">
                       <div style="cursor:pointer;" class="new inbox_row_col_six" data-toggle="collapse" data-target="#collapsable_sent_req_one'.$row->id.'" data-new="'.$row->id.'">
                      <i class="fa fa-caret-right" aria-hidden="true"></i> view details
                      </div>
                      </div>                   
                    </div> 
                    <div id="collapsable_sent_req_one'.$row->id.'" class="collapse">
                       <div class="req_parent_row_collapse">
                       <div class="row">
                       <div class="col-sm-4">
                       <div style="width:40px;height:40px;float:left;">
                       <img src="'.site_url().'uploads/users/thumb/'.$row->profile_image.'" class="img-circle" style="width:40px;height:40px;">
                       </div>
                       <div style="margin-left:5px;float:left;width:160px;height:40px;">
                       <span style="display:block;font-weight:bold;">'.$row->name.'</span>
                       </div>
                       </div>
                       <div class="col-sm-4">
                       <p>'.$row->message.'</p>
                       </div>
                       <div class="col-sm-3">
                       '.date('jS M H:i a',strtotime($row->sent)).'
                       </div>
                       <div class="col-sm-1">
                       </div>
                       </div>
                       </div>
                     </div>';                    
                 }}else{$output .= '<p class="text-center">No message to show</p>';}
            $output .= '</div>';
            echo $output;
    }
    public function send_message(){

        $this->load->model('Chat_model');
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $message = $this->input->post('message')?$this->input->post('message'):'';
        $to = $this->input->post('to')?$this->input->post('to'):0;
        $book_id = $this->input->post('book_id')?$this->input->post('book_id'):0;
        if($book_id>0 && $to>0 && $message!='' && $user_id>0){
            $chat=[
                'book_id'=>(int)$book_id,
                'to'=>(int)$to,
                'from'=>(int)$user_id,
                'message'=>$message,
                'sent'=>date('Y-m-d H:i:s',time()),
                'recd'=>1
            ];

            $this->Chat_model->create_chat($chat);
            $notification=[
                'type'=>1,
                'message'=>'New message received',
                'from_id'=>$user_id,
                'to_id'=>(int)$to,
            ];
            $notification_id =$this->Notification_model->create_notification($notification);
            $chat['session_user']=$this->user_data;
            $html = $this->load->view('includes/chat-left',['chat'=>$chat,'session_user'=>$this->user_data],true);

            echo json_encode(array('data'=>$html));
            exit();
        }
        echo json_encode(array('data'=>false));
    }
    public function message_thread(){
        @session_start();
        $this->load->model('Chat_model');


        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $book_id = $this->input->post('book_id')?$this->input->post('book_id'):0;
        $type = $this->input->post('type')?$this->input->post('type'):0;




        if($book_id>0 && $user_id>0){
            //$last_id = $this->session->userdata('last_id')?$this->session->userdata('last_id'):0;
            $last_id = $_SESSION['last_id']?$_SESSION['last_id']:0;
            $filter=[
                'book_id'=>$book_id,
                'last_id'=>$last_id,
                'user_id'=>$user_id,
                'recd'=>0,
                'type'=>$type
            ];
            $html='';

            $chat = $this->Chat_model->get_artist_chat($filter);
            //dd($chat);
            $counter=0;
            if(count($chat)>0){
                foreach($chat as $row){
                    if($counter==0){
                        //$this->session->set_userdata('last_id',$row->id);
                        $_SESSION['last_id']=$row->id;
                    }
                    if($row->from==$user_id){
                        $chat=(array)$row;
                        $chat['session_user']=$this->user_data;
                        $html .= $this->load->view('includes/chat-left',['chat'=>$chat,'session_user'=>$this->user_data],true);
                    }else{
                        $chat=(array)$row;
                        $chat['session_user']=$this->user_data;
                        $html .= $this->load->view('includes/chat-right',['chat'=>$chat,'session_user'=>$this->user_data],true);
                    }
                    $counter++;
                }
                $update=[
                    'recd'=>1,
                    'to'=>$user_id,
                    'book_id'=>$book_id,
                ];
                $this->Chat_model->update_chat($update);
            }
            // $last_id = $this->session->userdata('last_id')?$this->session->userdata('last_id'):0;
            $booking = $this->Booking_model->get_booking_by_id($book_id);
            $book_status = isset($booking->status)?$booking->status:0;
            echo json_encode(array('data'=>$html,'last_id'=>$last_id,'book_status'=>$book_status));
            exit();
        }
        echo json_encode(array('data'=>false));
    }

    // public function search()
    // {

    //     $this->data['genres'] = $this->Artist_model->get_genres();
    //     $this->data['cities'] = $this->Account_model->get_cities();
    //     $this->data['band_types'] = $this->Artist_model->get_band_types();

    //     $this->load->library("pagination");
    //     $genre = $this->input->get('genre')?$this->input->get('genre'):[];
    //     $band = $this->input->get('band')?$this->input->get('band'):'';
    //     $city = $this->input->get('city')?$this->input->get('city'):'';
    //     $sort = $this->input->get('sort')?$this->input->get('sort'):'name';
    //     $filter=array(
    //         'genre'=>$genre,
    //         'city_id'=>$city,
    //         'band_type_id'=>$band,
    //     );
    //     if($sort=='name'){
    //         $sort='name';
    //     }elseif($sort=='rating'){
    //         $sort='rating';
    //     }
    //     $page = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
    //     $count_record=$this->Venue_model->get_venues_count($filter);
    //     $config["base_url"] = site_url() . "/artist/search/";
    //     $config["total_rows"] = $count_record; 
    //     $config["per_page"] = 9;
    //     $config["uri_segment"] = 3;
    //     $choice = $config["total_rows"] / $config["per_page"];
    //     $config["num_links"] = round($choice);

    //     $custom_pagination = $this->Artist_model->ci_custom_pagination($config["base_url"], $config["total_rows"], $config["per_page"], $config["uri_segment"]);

    //     $offset = $page*$config["per_page"];
    //     $this->pagination->initialize($custom_pagination);
    //     $this->data['artists']=$this->Venue_model->get_venues($filter,$offset,$config["per_page"],$sort,'ASC');
    //     //dd($this->data['artists']);
    //     $this->data["links"] = $this->pagination->create_links();
    //     $this->load->view('artist/search', $this->data);
    // }


    public function logout(){

        $this->session->unset_userdata('showspoon_artist');
        @session_destroy();
        redirect('account/artist-login');
    }
    public function details($id=0){

        $this->load->model('Comments_model','comments');
        $venue = $this->Venue_model->get_venue_details_by_id($id);

        //dd($venue);

        if(!isset($venue->user_id)){
            show_404();
        }
        $this->data['venue']=$venue;
        $this->data['genres'] = $this->Artist_model->get_genres();
        $this->data['cities'] = $this->Account_model->get_cities();

        $user_id = isset($venue->user_id)?$venue->user_id:0;
        $this->data['gallery']=$this->Venue_model->get_venue_gallery($user_id);
        $this->data['videos']=$this->Venue_model->get_venue_videos($user_id,['YouTube']);
        $this->data['audios']=$this->Venue_model->get_venue_videos($user_id,['Vimeo']);
        $this->data['sound_cloud']=$this->Venue_model->get_venue_videos($user_id,['soundcloud']);

        $this->data['comments']=$this->comments->get_comments_venue($user_id);

        $this->load->view('artist/venue_details', $this->data);
    }
    public function update_event(){
        $this->load->model('Chat_model');
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $id = $this->input->post('id')?$this->input->post('id'):'';
        $to = $this->input->post('to')?$this->input->post('to'):0;
        $date = $this->input->post('date')?$this->input->post('date'):'';
        $time = $this->input->post('time')?$this->input->post('time'):'';
        $hours = $this->input->post('hours')?$this->input->post('hours'):'';
        $amount = $this->input->post('amount')?$this->input->post('amount'):'';
        if($user_id>0 && $id>0){
            $update=[
                'event_date'=>$date,
                'time'=>$time,
                'hours'=>$hours,
                'amount'=>$amount,
            ];
            $minutes = $hours*60;
            $this->db->where('id',$id)->update('bookings',$update);
            $update['book_id']=$id;
            $this->Booking_model->create_booking_history($update);
            $last_id = $this->db->insert_id();
            $event_date='';
            $event_date.=date('d M Y',strtotime($date));
            $event_date.=' '.date('h:i A',strtotime($time));
            $event_date.=' - '.date('h:i A',strtotime("+ $minutes minutes",strtotime($time)));
            $message='';
            $message.='<div><strong>Amount: </strong>'.number_format($amount,0).'</div>';
            $message.='<div><strong>Event Date: </strong>'.$event_date.'</div>';;
            $chat=[
                'update_event'=>(int)1,
                'book_id'=>(int)$id,
                'to'=>(int)$to,
                'from'=>(int)$user_id,
                'message'=>$message,
                'sent'=>date('Y-m-d H:i:s',time()),
            ];

            $this->Chat_model->create_chat($chat);
            $chat['session_user']=$this->user_data;
            $booking = $this->Booking_model->get_booking_by_id($id);
            $html = $this->load->view('includes/chat-left',['booking'=>$booking,'chat'=>(object)$chat,'session_user'=>$this->user_data],true);
            
            echo json_encode(array('data'=>$html));
        }else{
            echo json_encode(array('status'=>false));
        }
    }
    public function accept_event(){
        $this->load->model('Chat_model');
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $id = $this->input->post('id')?$this->input->post('id'):'';
        $to = $this->input->post('to')?$this->input->post('to'):0;
        
        if($user_id>0 && $id>0){
            $this->db->where('id',$id)->update('bookings',['status'=>1]);
            $booking = $this->Booking_model->get_booking_by_id($id);
            if($booking->who_pay==$this->user_data['type_id']){
                
            }
            $update=[
                'event_date'=>$date,
                'time'=>$time,
                'hours'=>$hours,
                'amount'=>$amount,
            ];
            $minutes = $hours*60;
            $this->db->where('id',$id)->update('bookings',$update);
            $update['book_id']=$id;
            $this->Booking_model->create_booking_history($update);
            $last_id = $this->db->insert_id();
            $event_date='';
            $event_date.=date('d M Y',strtotime($date));
            $event_date.=' '.date('h:i A',strtotime($time));
            $event_date.=' - '.date('h:i A',strtotime("+ $minutes minutes",strtotime($time)));
            $message='';
            $message.='<div><strong>Amount: </strong>'.number_format($amount,0).'</div>';
            $message.='<div><strong>Event Date: </strong>'.$event_date.'</div>';;
            $chat=[
                'update_event'=>(int)2,
                'book_id'=>(int)$id,
                'to'=>(int)$to,
                'from'=>(int)$user_id,
                'message'=>$message,
                'sent'=>date('Y-m-d H:i:s',time()),
            ];

            $this->Chat_model->create_chat($chat);
            $chat['session_user']=$this->user_data;
            
            $html = $this->load->view('includes/chat-left',['booking'=>$booking,'chat'=>(object)$chat,'session_user'=>$this->user_data],true);
            
            echo json_encode(array('data'=>$html));
        }else{
            echo json_encode(array('status'=>false));
        }
    }


    public function payment(){
        $from_id = $this->input->post('from_id');
        $book_id = $this->input->post('book_id');
        $artist = $this->Artist_model->get_artist_details($from_id);

        $booking = $this->Booking_model->get_booking_by_id($book_id);
        $data=[
                'artist'=>$artist,
                'book' => $booking, 
            ];
         $html =  $this->load->view('booking/payment',$data,true);

         echo json_encode(array('data'=>$html));
    }

    public function cancel_payment(){
        $this->load->view("booking/cancel_payment");
    }
    public function success_payment(){
        $this->load->view("booking/success_payment");
    }


    public  function pdf_generation($book_id=0){

        $result = $this->Booking_model->get_booking_by_id($book_id);

        //dd($result);
        $show = $result->from_id;
        $id = $result->id;
        //dd($show);

        if ($result->type  == 2) {

            /*echo "SELECT name,email FROM venues WHERE user_id = $show";
            die();*/
            $query =  $this->db->query("SELECT name,email FROM venues WHERE user_id = $show");
            $data['artist'] =  $query->result_array();

            $email= $data['artist'][0]['email'];
           //dd($email);
            
            $booking = $this->db->query("SELECT * FROM bookings WHERE id = $id");
            $data['booking'] =  $booking->result_array();

            
            $user_id = $this->data['session_user']=$this->user_data['user_id'];
            $emaill = $this->data['session_user'] = $this->user_data['user_email'];

            //dd($emaill);
            //dd($user_id);
            $chat= $this->db->query("SELECT * FROM chat WHERE  `book_id`= $show  ORDER BY message DESC" );
           /* echo "SELECT * FROM chat WHERE  `book_id`= $show";
            die();*/
            $data['chat'] =  $chat->result_array();

            $data['name'] = 'Sadam Hussain';   
            $this->load->view('artist/pdf_view',$data);
                // Get output html
            $html = $this->output->get_output();       
                // Load library
            $this->load->library('dompdf_gen');                
                // Convert to PDF
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $output = $this->dompdf->output();

             file_put_contents("uploads/artist-pdf/Brochure-$book_id.pdf", $output);
             file_put_contents("uploads/venues-pdf/Brochure-$book_id.pdf", $output);
              $this->Send_Mail($email,FROM_EMAIL,FROM_NAME,'Agreement','Hello',FCPATH."uploads/artist-pdf/Brochure-$book_id.pdf");
              $this->Send_Mail($emaill,FROM_EMAIL,FROM_NAME,'Agreement','Hello',FCPATH."uploads/artist-pdf/Brochure-$book_id.pdf");
            $data['Attachment']=false;

            $this->dompdf->stream("welcome.pdf",$data);
            

            if($booking->status == 0){
                $this->db->set('status',1);
                $this->db->set('is_complete',1);
                $this->db->where('id',$id);
                $this->db->update('bookings');
              
            }
            
            //$this->data['success']=$this->lang->line('account_created',false);
         }   

    }


    public  function pdf_generation1($book_id=0){

        $result = $this->Booking_model->get_booking_by_id($book_id);
        //dd($result);
        $show = $result->from_id;
        $id = $result->id;
       // dd($show);        
        if ($result->type  == 1 ) {

            $query =  $this->db->query("SELECT name ,email FROM artist WHERE user_id = $show");
            $data['artist'] =  $query->result_array();

             $email= $data['artist'][0]['email'];

            //dd($email);

            $booking = $this->db->query("SELECT * FROM bookings WHERE id = $id");

            $data['booking'] =  $booking->result_array();
            $user_id = $this->data['session_user']=$this->user_data['user_id'];
            $emaill = $this->data['session_user'] = $this->user_data['user_email'];

            //dd($emaill);
            
             $user_id = $this->data['session_user']=$this->user_data['user_id'];
            $chat= $this->db->query("SELECT * FROM chat WHERE  `book_id`= $show  ORDER BY message DESC");
           
            $data['chat'] =  $chat->result_array();
           
    
            $data['name'] = 'Sadam Hussain';   
            $this->load->view('venue/pdf_view',$data);
                // Get output html
            $html = $this->output->get_output();
            

                // Load library
            $this->load->library('dompdf_gen');
                
                // Convert to PDF
            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $output = $this->dompdf->output();

            file_put_contents("uploads/venues-pdf/Brochure-$book_id.pdf", $output);
            file_put_contents("uploads/artist-pdf/Brochure-$book_id.pdf", $output);
            $this->Send_Mail($email,FROM_EMAIL,FROM_NAME,'Agreement','Hello',FCPATH."uploads/venues-pdf/Brochure-$book_id.pdf");
              $this->Send_Mail($emaill,FROM_EMAIL,FROM_NAME,'Agreement','Hello',FCPATH."uploads/venues-pdf/Brochure-$book_id.pdf");
            $data['Attachment']=false;
            $this->dompdf->stream("welcome.pdf",$data);

            if($booking->status == 0){
                $this->db->set('status',1);
                $this->db->set('is_complete',1);
                $this->db->where('id',$id);
                $this->db->update('bookings');
            }
             
        
         }   



    }

   public function transaction(){   
       
    $user_id = $this->data['session_user']=$this->user_data['user_id'];
    $book_id = $this->input->post('id');
    
    $result = $this->Booking_model->get_booking_by_id($book_id);

        $transaction=array(
                'user_id'=>$user_id ,
                'booking_id'=>$this->input->post('id'),
                'amount'=>$result->amount,
                'cc_number'=>$this->input->post('credit_card'),
                'expiry_date'=>$this->input->post('exp'),
                'ccv'=>$this->input->post('ccv'),
                'payment_datetime'=>date('Y-m-d H:i:s',time()),
            );
       //dd($transaction);
        $this->Venue_model->transaction($transaction);  

        echo  json_encode(array('data'=>$transaction));  
        
    }


    public function agreement(){
      
        $this->load->model('Chat_model');
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
   
       //dd($chat['session_user']=$this->user_data);
      
        $id = $this->input->post('id')?$this->input->post('id'):'';
        $to = $this->input->post('to')?$this->input->post('to'):0;
        $date = $this->input->post('date')?$this->input->post('date'):'';
        $time = $this->input->post('time')?$this->input->post('time'):'';
        $loc = $this->input->post('loc')?$this->input->post('loc'):'';
        $amount = $this->input->post('amount')?$this->input->post('amount'):'';

        $booking = $this->db->query("SELECT * FROM bookings WHERE id = $id");
        $chat['booking'] =  $booking->result_array();
        $status= $chat['booking'][0]['status'];
       
        $who_pay = $chat['booking'][0]['who_pay'];
        $type = $chat['booking'][0]['type']; 
        $to_id = $chat['booking'][0]['to_id'];
        $from_id = $chat['booking'][0]['from_id'];
        //dd($from_id);
        
        /*dd($to);*/
        $event_date='';
        $event_date.=date('d M Y',strtotime($date));
        $event_date.=' '.date('h:i A',strtotime($time));
    
        $message='';
        $message.='<div><strong>Amount: </strong>'.$amount.'</div>';
        $message.='<div><strong>Event Date: </strong>'.$event_date.'</div>';
       //dd($chat['session_user']=$this->user_data);
        $message .=  anchor("#", 'Payment', ['class'=>'btn btn-default pull-right','id'=>'post_payment','data-target'=>"#myModal_payment",'data-toggle'=>"modal"]);
      

        $chat=[
                //'update_event'=>(int)1,
                'book_id'=>(int)$id,
                'to'=>(int)$to,
                'from'=>(int)$user_id,
                'message'=>$message,
                'sent'=>date('Y-m-d H:i:s',time()),
        ];
         
    
        $this->Chat_model->create_chat($chat);     
        echo  json_encode(array('data'=>$chat));

        if($status == 0){
            $this->db->set('status',1);
            $this->db->where('id',$id);
            $this->db->update('bookings');   
        }
    }

    public function get_calendar()
    {
        $this->load->library("pagination");
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $this->data['genres'] = $this->Artist_model->get_genres();
        // $filter=[
        //     'is_complete'=>1,
        //     'from_id'=>$user_id,
        //     //'type'=>2
        // ]; 
        $requests=$this->Booking_model->get_venue_calendar($user_id);  
        echo json_encode($requests);
    }
    public function get_calendar_profile()
    {
        $output = '';
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0; 
        $id = $this->input->post('id');   
        $data = $this->Booking_model->get_calendar_profilee($id,$user_id);  //echo '<pre>'; print_r($data); exit;
        if(count($data) > 0 && $data!=""){
        $output .= '<div class="col-md-4">
           <div class="white_box rounded ">
            <div class="padd_thrty">
               <div class="spacer_big">
                 <div class="clearfix member_box">
                    <figure>
                        <img src="'.site_url().'uploads/users/thumb/'.$data->profile_image.'" alt="Member">
                    </figure>
                    <figcaption>
                        '.$data->venue_name.' <br>
                        <span>'.$data->venue_name.'</span>
                        <br/>
                        <a href="#myProfile" data-toggle="modal" id="viewprofile" data-id="'.$id.'">View Profile</a>
                    </figcaption>
                 </div>
               </div>
               <p class="text-capitalize">
                  performance date <br/>
                  <strong>'.date('F j,Y',strtotime($data->datetime)). '|' .$data->start_time .'-'. $data->end_time .'</strong>
               </p>
               <p class="text-capitalize">
                  performance time <br/>
                  <strong>'.$data->start_time.'-'.$data->end_time.'</strong>
               </p>
               <div class="row">
                 <div class="col-sm-6">
                   <p class="text-capitalize">
                      capacity <br/>
                      <strong>'.$data->capacity.'</strong>
                   </p>
                 </div>
                 <div class="col-sm-6">
                   <p class="text-capitalize">
                      total payout <br/>
                      <strong>$'.$data->amount.'</strong>
                   </p>
                 </div>
               </div>
               <div>
                <p class="text-capitalize">Booking Notes
                <strong><br>'. $data->message.'</strong>
           </div>

             <hr class="light_grey_ruler no-marg" />

             <div class="padd_twenty">
               <ul class="booking_conf_list clearfix">
                
                 <li>
                   <a href="javascript:;" id="cancel_booking" data-cancel="'.$data->book_id.'">
                     Cancel Booking
                   </a>
                 </li>
               </ul>
             </div>
           </div>
         </div>';
     }else{$output .= "No bookings found";}
         echo $output;
    }
    public function cancel_booking(){
        $id = $this->input->post('id');
        $this->db->where('id',$id)->update('bookings',['btn'=>'cancelled']);
    }
    public function addNote(){
        $id = $this->input->post('id');
        $note = $this->input->post('note'); 
        $this->db->where(['id'=>$id]);
        $this->db->update('bookings',['note'=>$note]);
    }
    public function view_profile()
    {
    	$output = '';
    	$id = $this->input->post('id');
    	$data = $this->Booking_model->view_profile($id);
    	$output .= '<div class="col-md-12">
					<div class="white_box rounded ">
					<div class="padd_thrty">
					<div class="spacer_big">
					<div class="clearfix member_box">
					<figure>
					<img src="'.site_url().'uploads/users/thumb/'.$data->profile_image.'" alt="Member">
					</figure>
					<figcaption>
					'.$data->name.' <br>
					<br/>

					</figcaption>
					</div>
					</div>
					<p class="text-capitalize">
					Age <br/>
					<strong>'.$data->age.'</strong>
					</p>
					<p class="text-capitalize">
					Email address <br/>
					<strong>'.$data->email.'</strong>
					</p>
					<p class="text-capitalize">
					<p class="text-capitalize">
					Mobile <br/>
					<strong>'.$data->mobile.'</strong>
					</p>
					<p class="text-capitalize">
					Rate <br/>
					<strong>'.$data->venue_rate.'</strong>
					</p>
					Location <br/>
					<strong>'.$data->location.'</strong>
					</p>
					<p class="text-capitalize">
					Website <br/>
					<strong><a href="'.$data->website.'" target="_blank">'.$data->website.'</a></strong>
					</p>
					<p class="text-capitalize">
					Facebook <br/>
					<strong><a href="'.$data->facebook.'" target="_blank">'.$data->facebook.'</a></strong>
					</p>
					<p class="text-capitalize">
					Twitter <br/>
					<strong><a href="'.$data->twitter.'" target="_blank">'.$data->twitter.'</a></strong>
					</p>
					<p class="text-capitalize">
					Instagram <br/>
					<strong><a href="'.$data->instagram.'" target="_blank">'.$data->instagram.'</a></strong>
					</p>
					</div>
					<hr class="light_grey_ruler no-marg" />
					</div>
					</div>';
		echo $output;
    }

    public function booking(){


        $this->load->library("pagination");
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $this->data['image'] = $this->user_data['profile_image']?$this->user_data['profile_image']:0;
        $this->data['profile'] = $this->Booking_model->profile_two($user_id);
        /*$filter=array(
            'genre'=>$genre,
            'city_id'=>$city,
            'band_type_id'=>$band,
        );*/
        $this->data['genres'] = $this->Artist_model->get_genres();
        $filter=[
            'is_complete'=>1,
            'from_id'=>$user_id,
            'type'=>1
        ];
        $this->db->select('*');
        $this->db->where('from_id',$user_id);
        $this->db->order_by('id','desc');
        $sql = $this->db->get('bookings');
        //$this->data['id'] = $sql->row()->id;
        $this->data['requests']=$this->Booking_model->get_venue_bookings($filter);
        $this->load->view('booking/artist_booking_calendar', $this->data);
    }


    public function received_bookings(){

        $this->load->library("pagination");
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;

        /*$filter=array(
            'genre'=>$genre,
            'city_id'=>$city,
            'band_type_id'=>$band,
        );*/
        $this->data['genres'] = $this->Artist_model->get_genres();
        $filter=[
            'is_complete'=>1,
            'to_id'=>$user_id,
            //'type'=>1
        ];

        $sort='id';
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
        $count_record=$this->Booking_model->get_venue_bookings_count($filter);
        $config["base_url"] = site_url() . "/artist/search/";
        $config["total_rows"] = $count_record; 
        $config["per_page"] = 9;
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);

        $custom_pagination = $this->Artist_model->ci_custom_pagination($config["base_url"], $config["total_rows"], $config["per_page"], $config["uri_segment"]);

        $offset = $page*$config["per_page"];
        $this->pagination->initialize($custom_pagination);
        $this->data['requests']=$this->Booking_model->get_venue_bookings($filter,$offset,$config["per_page"],$sort,'ASC');
        //dd($this->data['artists']);
        $this->data["links"] = $this->pagination->create_links();




        $this->load->view('booking/received_bookings', $this->data);
    }


    public function download($id){
       /*echo "uploads/artist-pdf/Brochure-$id.pdf";
       die();*/
        $this->load->helper("download");
        $data = file_get_contents(base_url()."uploads/artist-pdf/Brochure-$id.pdf");
        $name = "agreement.pdf";
        force_download($name, $data);
    }
    public function search()
    {
         $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
         $data['genres'] = $this->Artist_model->getGenre(); 
         $data['cities'] = $this->Artist_model->getCities();
         $search = $this->input->post('search');
         $this->load->library("pagination");
         $config = array();
         $config["base_url"] = site_url('artist/search');
         $config["total_rows"] = $this->Artist_model->search_count($search);
         $config["per_page"] = 5;
         $config["uri_segment"] = 3;
         $config["use_page_numbers"] = TRUE;
         $config["full_tag_open"] = '<ul class="pagination">';
         $config["full_tag_close"] = '</ul>';
         $config["first_tag_open"] = '<li>';
         $config["first_tag_close"] = '</li>';
         $config["last_tag_open"] = '<li>';
         $config["last_tag_close"] = '</li>';
         $config['next_link'] = '&raquo;';
         $config["next_tag_open"] = '<li>';
         $config["next_tag_close"] = '</li>';
         $config["prev_link"] = "&laquo";
         $config["prev_tag_open"] = "<li>";
         $config["prev_tag_close"] = "</li>";
         $config["cur_tag_open"] = "<li class='active'><a href='#'>";
         $config["cur_tag_close"] = "</a></li>";
         $config["num_tag_open"] = "<li>";
         $config["num_tag_close"] = "</li>";
         $config["num_links"] = 1;
         $this->pagination->initialize($config);
         $page = ($this->uri->segment(3))?$this->uri->segment(3):1;
         $start = ($page - 1) * $config["per_page"];
         $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0; 
         $data['location'] = $this->Artist_model->get_artist_lat_long($user_id);
         $data['venue_details'] = $this->Artist_model->getVenueDetails();
         $data['venues'] = $this->Artist_model->getVenues(); 
         $data['request'] = $this->Booking_model->get_artist_booking_count($user_id);
         $data['data'] = $this->Artist_model->get_search_results($search,$start,$config["per_page"]);
         $data['links'] = $this->pagination->create_links();
         $this->load->view('artist/artist_search', $data);
    }
}

