<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class managers_model extends CI_Model {

    protected $fillable = ['f_name','l_name','email','is_delete','is_active','type','password'];
    function __construct() {
        parent::__construct();


        $this->load->helper('site');
    }



    public function get_managers($admin_id=0) {
        if($admin_id==0){
            $query=$this->db->query("select * from admin where is_delete=0 order by id desc");
            return $query->result();
        }else{
            $query=$this->db->query("select * from admin where is_delete=0 and id=".(int)$admin_id." limit 1");
            return $query->row();
        }

    }
    public function get_manager_by_email($email='') {
        if($email!=''){

            $query=$this->db->query("select * from admin where email='".$email."' limit 1");
            return $query->row();
        }
        return false;

    }
    public function update_manager($data=[]) {
        $id=0;
        if(isset($data['id']) && is_numeric($data['id']))
        {
            $id=$data['id'];
        }
        $update=[];
        foreach($data as $key=>$val){

            if(isset($data[$key]) && in_array($key,$this->fillable)){
                if($key=='password'){
                    if($val!=''){
                        $update[$key]=$val;
                    }
                }else{

                    $update[$key]=$val;
                }
            }
        }
        //dd($update);
        if(count($update)>0){
            return $this->db->where('id',$id)->update('admin',$update);
        }
        return false;


    }
    public function create_manager($data=[]) {

        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->fillable)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0){
            $this->db->insert('admin',$insert);
            return $this->db->insert_id();
        }
        return false;


    }


}
