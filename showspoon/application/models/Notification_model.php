<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class notification_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->helper('site');
    }
    private $notification=['message','is_active','from_id','to_id','is_read','created_datetime','link','type'];
    
    
    
    
    public function get_artist_notifications($data=[],$offset=0,$limit=10,$order='id',$sort='DESC') {
        $where='';
        if(isset($data['from_id']) && is_numeric($data['from_id'])) {
            $where .= " and from_id=".(int)$data['from_id']." ";
        }
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " and to_id=".(int)$data['to_id']." ";
        }
        
        if(isset($data['is_read']) && is_numeric($data['is_read'])) {
            $where .= " and is_read=".(int)$data['is_read']." ";
        }
        if(isset($data['type']) && is_numeric($data['type'])) {
            $where .= " and type=".(int)$data['type']." ";
        }
        
        $query=$this->db->query("select N.*,V.name as name from notifications N
        inner join venues V on V.user_id = N.from_id
        where  N.is_active=1  $where order by $order $sort limit $offset,$limit");
        return $query->result();
    }
    public function get_venue_notifications($data=[],$offset=0,$limit=10,$order='id',$sort='DESC') {
        $where='';
        if(isset($data['from_id']) && is_numeric($data['from_id'])) {
            $where .= " and from_id=".(int)$data['from_id']." ";
        }
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " and to_id=".(int)$data['to_id']." ";
        }
        
        if(isset($data['is_read']) && is_numeric($data['is_read'])) {
            $where .= " and is_read=".(int)$data['is_read']." ";
        }
        if(isset($data['type']) && is_numeric($data['type'])) {
            $where .= " and type=".(int)$data['type']." ";
        }
        
        $query=$this->db->query("select N.*,V.name as name from notifications N
        inner join artist V on V.user_id = N.from_id
        where  N.is_active=1  $where order by $order $sort limit $offset,$limit");
        return $query->result();
    }
    public function get_venue_notification($id){
        $query = $this->db->query("select * from notifications where to_id=$id and type=2 and is_read=0");
        return $query->result();
    }
    public function delete_venue_notification($id)
    {
        $this->db->where('id',$id)->update('notifications',['is_read'=>1]);
    }
    public function get_artist_notification($id){
        $query = $this->db->query("select * from notifications where to_id=$id and type=1 and is_read=0");
        return $query->result();
    }
    public function get_artist_request_count($id)
    {
        $query = $this->db->query("select status,is_read,count(*) as cnt from notifications where to_id = $id and status='request' and is_read=0");
        return $query->row();
    }
    public function get_artist_inbox_count($id)
    {
        $query = $this->db->query("select status,is_read,count(*) as cnt from notifications where to_id = $id and status='message' and is_read=0");
        return $query->row();
    }
    public function get_venue_request_count($id)
    {
        $query = $this->db->query("select status,is_read,count(*) as cnt from notifications where to_id = $id and status='request' and is_read=0");
        return $query->row();
    }
    public function get_venue_inbox_count($id)
    {
        $query = $this->db->query("select status,is_read,count(*) as cnt from notifications where to_id = $id and status='message' and is_read=0");
        return $query->row();
    }

    public function delete_artist_notification($id)
    {
        $this->db->where('id',$id)->update('notifications',['is_read'=>1]);
    }
    
    public function get_notification_details($id=0) {
        $query=$this->db->query("select *,A.venue_id as row_id from venues A
        left join venue_details AD on A.venue_id = AD.venue_id
        where A.is_delete=0 and A.is_active=1 and A.user_id=".(int)$id." limit 1");
        return $query->row();
    }
    public function get_notification_details_by_id($id=0) {
        $query=$this->db->query("select *,A.venue_id as row_id from venues A
        left join venue_details AD on A.venue_id = AD.venue_id
        where A.is_delete=0 and A.is_active=1 and A.venue_id=".(int)$id." limit 1");
        return $query->row();
    }
    
    
    
    
    
    public function create_notification($data){

        // $insert=[];
        // foreach($data as $key=>$val){
        //     if(isset($data[$key]) && in_array($key,$this->notification)){
        //         $insert[$key]=$data[$key];
        //     }
        // }
        // if(count($insert)>0){
        //     $insert['created_datetime']=date('Y-m-d H:i:s',time());
        //     $this->db->insert('notifications',$insert);
        //     return  $this->db->insert_id();
            
        // }
        // return false;
        $this->db->insert('notifications',$data);
        return  $this->db->insert_id();

    }

    public function update_notification($data=[]) {
        $id=0;
        if(isset($data['user_id']) && $data['user_id']>0){
            $id=$data['user_id'];
        }
        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->notification)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0){

            $this->db->where('to_id',$id)->update('notifications',$insert);
            

        }
        return false;


    }
    
    
    
    
    
}
