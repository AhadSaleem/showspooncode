<!DOCTYPE html>
<!-- saved from url=(0049)http://165.227.110.185/showspoon/artist/dashboard -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Showspoon</title>
    <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/custom-develop.css" rel="stylesheet">
    <!-- new wireframe style -->
    <link href="<?php echo base_url(); ?>assets/css/custom_style.css" rel="stylesheet">
    <!-- for bootstrap live search -->
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.css"> -->
    <!-- for ekko lightbox -->
    <link href="<?php echo base_url(); ?>assets/css/ekko-lightbox.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- for flexslider -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flexslider.css" type="text/css" media="screen" />




    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>
    <!-- for Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo site_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url(); ?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo site_url(); ?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo site_url('assets/favicon/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">
    <style>
    .navbar-toggle{background:none !important;border:1px solid #ab7b15 !important;}
    .navbar-toggle > .icon-bar{background:#ab7b15 !important;}
    /*#myNavbar > .float-center > .nav > li > a{color:#000 !important;}*/
    .spacer_medium {
     margin: 0 0 90px 0;
     }
    </style>
    <!--<script src="http://165.227.110.185/showspoon//assets/dist/js/bootstrap_multiselect.js"></script>-->
</head>

<body class="no-body-padd login_bg">

    <header class="main-header landing-page">
      <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">

        <!-- navbar for login link only -->
        <div class="clearfix">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
          <div class="float-left">
            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <img src="<?php echo site_url('assets/images/logo-n.png'); ?>" alt="Logo">
            </a>
          </div>
        </div>
          <!-- <div class="float-right">
            <a class="nav-btn" href="<?php echo base_url(); ?>account/login">Login</a>
          </div> -->
          <div class="collapse navbar-collapse" id="myNavbar">
          <div class="float-center">
          <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo site_url('about'); ?>" style="color:#fff;">About</a></li>
          <li><a href="<?php echo site_url('showspoon-works'); ?>" style="color:#fff;">How Showspoon Works</a></li>
          <li><a href="<?php echo site_url('contact'); ?>" style="color:#fff;">Contact Us</a></li>
          <li><a href="<?php echo site_url('account/login'); ?>" style="color:#fff;">Login</a></li>
          </ul>
          </div>
        </div>
        </div>
        <!-- navbar for login link only ends -->

        
        <!-- <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo site_url(); ?>">
              <img src="<?php echo site_url(); ?>assets/images/logo-n.png" alt="Logo">
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a style="color: #fff;" href="<?php echo base_url(); ?>account/login">Login</a></li>
          </ul>
        </div> -->

      </div>
    </nav>
</header> 




<div class="spacer_medium"></div>

    <div class="container">
      <div class="row loginbox">
        <div class="loginContainer">
          <h3 class="text-center m-t-lg m-b-lg">Contact Us</h3>
          <!-- form -->
          <form id="contact-form" action="<?php echo site_url("account/contact"); ?>" method="post">
            <div class="well well-md m-b-none npt">

          <div class="form-group">
          <?php if($this->session->flashdata('success')){ ?>
         <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?php echo $this->session->flashdata('success'); ?>
         </div>
        <?php } ?>
          </div>
          <div class="form-group">
                <input type="text" name="name" class="form-control" id="name" value="<?php echo set_value('name'); ?>" placeholder="Enter Name" />
              <span class="text-danger"><?php echo form_error('name'); ?></span>
              <span id="error-name"></span>
          </div><!-- form-group -->
          <div class="form-group">
                <input type="text" name="email" class="form-control" id="email" value="<?php echo set_value('email'); ?>" placeholder="Enter email" />
              <span class="text-danger"><?php echo form_error('email'); ?></span>
              <span id="error-email"></span>
          </div><!-- form-group -->
          <div class="form-group">
                <textarea class="form-control" name="message" id="message" value="<?php echo set_value('message'); ?>" placeholder="Enter Message"></textarea>
              <span class="text-danger"><?php echo form_error('message'); ?></span>
              <span id="error-message"></span>
          </div><!-- form-group -->
          <div class="form-group">
            <div class="g-recaptcha" data-sitekey="6LcbK1sUAAAAANQ6Ex0cagPA2VFqJG5_l91E4Q2j"></div>
            <span class="text-danger"><?php echo form_error('g-recaptcha-response'); ?></span>
          </div>
          <div class="form-group">
            <input type="submit" name="submit" value="Send" class="btn btn-primary btn-block">
          </div>

        </div><!-- well -->
        </form>
          <!-- form ends -->
        </div><!-- loginContainer -->
      </div>
    </div><!-- container -->

     
      
<div style="height: 100px;"></div>



    
<div class="spacer_hundred"></div>

<?php $this->load->view('includes/footer'); ?>

   <style>
     .red{border:1px solid #cc0000;}
   </style> 


    
    <script type="text/javascript">
        $(document).ready(function(){

            // $('#contact-form').submit(function(e){

            //     var name = $('#name').val();
            //     var email = $('#email').val();
            //     var message = $('#message').val();
            //     if(name==''){
            //       $('#name').addClass('red');
            //       $('#error-name').text('This field is required.').css('color','#cc0000');
            //     }

            //     if(email==''){
            //       $('#email').addClass('red');
            //       $('#error-email').text('This field is required.').css('color','#cc0000');
            //     }

            //     if(!validateEmail(email)){
            //     $('#error-email').text('Enter valid email address.');
            //     $('#email').addClass('red');
            //    }

            //     if(message==''){
            //       $('#message').addClass('red');
            //       $('#error-message').text('This field is required.').css('color','#cc0000');
            //     }
            //     if(name!='' && email!='' && message!='' && validateEmail(email)){
            //       $('#contact-form').submit();
            //     }
            //     e.preventDefault();
            // });
        });


        $('#name').bind('keyup , change',function(){

             $('#error-name').text('');
             $('#name').removeClass('red');
        });

        $('#email').bind('keyup , change',function(){

             $('#error-email').text('');
             $('#email').removeClass('red');
        });

        $('#message').bind('keyup , change',function(){

             $('#error-message').text('');
             $('#message').removeClass('red');
        });
        $('#email').bind('keyup , change',function(){
             var email = $(this).val();
             if(!validateEmail(email)){
              $('#error-email').text('Enter valid email address.');
              $('#email').addClass('red');
             }
        });
        function validateEmail(email){
          var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
          return emailReg.test( email );
        }
        $(document).on("scroll", function(){
        if
          ($(document).scrollTop() > 20){
              $(".main-header").addClass("shrink");
            }
            else
            {
                $(".main-header").removeClass("shrink");
            }
        });
    </script>
    
</body>

</html>