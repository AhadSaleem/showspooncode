<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    private $WB_AKEY='2091049220';
    private $WB_SKEY='52bbb7c4b4c540298855f595a5c55864';
    private $WB_CALLBACK_URL='http://178.62.77.181/tbanalytics/account/weibo_authenticate';
    private $WB_CODE='a3e98ceef5d0cd47424c0178407ef8a1';


    public function __construct() {
        parent::__construct();
        require_once('application/libraries/weibo/saetv2.ex.class.php');
    }

    public function index() {

    }

    public function fetchData2($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    private function feed_history_send_email() {
         $this->load->model('Api_model');
         $email="aqeel.mashkraft@gmail.com";
         $data['feed_history_record']=$this->Api_model->get_feed_history();
         $message =$this->load->view('emails/history_record', $data,true);
        // echo $message;
         
        include_once APPPATH.'/libraries/sendgrid-php/vendor/autoload.php';
        $sendgrid = new SendGrid("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");
        $emailbody   = new SendGrid\Email();
        $emailbody->addTo($email)
            ->setFrom("info@touchbase-inc.com")
            ->setReplyTo($email)
            ->setFromName("Touchbase")
            ->setSubject('Touchbase Feed History Detail')
            ->setHtml($message);

        $sendgrid->send($emailbody);

    }

    public function social_api() {
        //Start Time;
         $data_array = array(
            "cron_service" => "social_api",
            "start_date_time" => date('Y-m-d H:i:s')
        );
        $this->db->insert('cron_records', $data_array);
        $cron_records_id=$this->db->insert_id();
        //Instagram Api 
        $this->instagram_api();
        //Facebook Api
        $this->facebook_api();
        /* $json_encode = $this->facebook_api();
        $json = json_encode($json_encode);
        $result = json_decode($json);

        $fb_html = '';
        foreach ($result as $key => $row) {
            $fb_html.='Artist:' . $row->artistname . '<br>New posts:' . $row->total_new_post . '<br>New Comments:' . $row->total_new_comment . '<br><br>';
        }
        $fb_msg = 'SERVER NAME:' . $_SERVER['SERVER_NAME'] . '<br><br>Service:Facebook Api<br><br>Date:' . date('Y-m-d H:i:s') . '<br><br>' . $fb_html;

        $email = 'asifriazminhas@gmail.com';

        $subject = 'Touchbase [Dev Server]| Social Feed Service : ' . date('Y-m-d H:i:s');*/

        //Twitter Api
        $this->twitter_api();
        /* $json_encodetw = $this->twitter_api();
        $json_tw = json_encode($json_encodetw);
        $result_tw = json_decode($json_tw);
        $tw_html = '';
        foreach ($result_tw as $key_tw => $row_tw) {
            $tw_html.='Artist:' . $row_tw->artistname . '<br><br>New Tweets:' . $row_tw->total_tweets . '<br><br>';
        }
        $tw_msg = 'SERVER NAME:' . $_SERVER['SERVER_NAME'] . '<br><br>Service:Twitter Api<br><br>Date:' . date('Y-m-d H:i:s') . '<br><br>' . $tw_html;*/
        //Youtube Api
        $this->youtube_api();

        /* $json_encode_youtube = $this->youtube_api();
        $json_youtube = json_encode($json_encode_youtube);
        $result_youtube = json_decode($json_youtube);

        $youtube_html = '';
        foreach ($result_youtube as $key_youtube => $row_youtube) {
            $youtube_html.='Artist:' . $row_youtube->artistname . '<br>New posts:' . $row_youtube->total_new_post . '<br>New Comments:' . $row_youtube->total_new_comment . '<br><br>';
        }

        $youtube_msg = 'SERVER NAME:' . $_SERVER['SERVER_NAME'] . '<br><br>Service:Youtube Api<br><br>Date:' . date('Y-m-d H:i:s') . '<br><br>' . $youtube_html;


        $message = $fb_msg . '<br>' . $tw_msg . '<br>' . $youtube_msg;*/
        // Facebook Page Likes
        //$this->facebook_page_likes();
        //$this->youtube_api();
        /* $this->sent_email($email, $message, $subject);
        $data_array = array(
            "cron_service" => "social_api",
            "date_time" => date('Y-m-d H:i:s')
        );
        $this->db->insert('cron_records', $data_array);*/


        $this->weibo_api();
        
        //End Time
        $data_array_update = array(
            "end_date_time" => date('Y-m-d H:i:s')
        );
        $this->db->where(array('id'=>$cron_records_id));
        $this->db->update('cron_records', $data_array_update);
        //$this->feed_history_send_email();
        
    }

    public function social_analytics_api() {
        // Facrbook apis
        $this->facebook_page_likes();
        // $this->facebook_page_insights();
        $this->facebook_page_fans_country();
        //Instagram api
        $this->instagram_page_about();
        //Youtube apis
        $this->youtube_page_about();
        $this->weibo_followers_friends();
        $this->twitter_friends_followers();
        $this->facebook_page_fans_gender_age();
        $this->facebook_page_posts_impressions();
        $this->facebook_page_engaged_users();
        $this->facebook_page_views_total();
        $this->facebook_page_impressions();
    }

    public function facebook_page_likes() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $query = $this->db->query("SELECT c.id,smc.alias FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=1 AND smc.alias!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $fbalias = $value['alias'];
            $feed_url = "https://graph.facebook.com/$fbalias/?fields=fan_count,talking_about_count,name&access_token=".FACEBOOK_API_TOEKN;
            $c = curl_init();
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($c, CURLOPT_URL, $feed_url);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            $data = curl_exec($c);
            curl_close($c);
            $result = json_decode($data);
            if(isset($result)){
                if(isset($result->fan_count)){
                    $likes = $result->fan_count;
                    $talking_about_count = $result->talking_about_count;
                    $query_cf = $this->db->query("SELECT * FROM `client_analytics_feed` WHERE client_id=$client_id AND social_media_id=1  AND `social_api_id`=1 ORDER BY `id` DESC");
                    if ($query_cf->num_rows() > 0) {
                        $row_client_info = $query_cf->row();    
                        $likesdb = $row_client_info->likes;
                        if ($likes > $likesdb) {

                            $data_array = array(
                                'client_id' => $client_id,
                                'likes' => $likes,
                                'people' => $talking_about_count,
                                'social_media_id' => 1,
                                'inserted_time' => date('Y-m-d H:i:s'),
                                'feeds' => $data,
                                'social_api_id' => 1
                            );
                            $this->db->insert('client_analytics_feed', $data_array);
                            echo 'Found';
                        }
                    } 
                    else {
                        $data_array = array(
                            'client_id' => $client_id,
                            'likes' => $likes,
                            'people' => $talking_about_count,
                            'social_media_id' => 1,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feeds' => $data,
                            'social_api_id' => 1
                        );
                        $this->db->insert('client_analytics_feed', $data_array);
                        echo 'Insert First';
                    }

                }       

            }
        }
    }

    public function facebook_page_insights() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $query = $this->db->query("SELECT c.id,smc.alias FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=1 AND smc.alias!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $fbalias = $value['alias'];
            $feed_url = "https://graph.facebook.com/$fbalias/insights?fields=description,period,title,values,name,id&access_token=".FACEBOOK_API_TOEKN;
            $c = curl_init();
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($c, CURLOPT_URL, $feed_url);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            $data = curl_exec($c);
            curl_close($c);
            $query_cf = $this->db->query("SELECT * FROM `client_analytics_feed` WHERE client_id=$client_id AND social_media_id=1  AND `social_api_id`=2 ORDER BY `id` DESC");
            if ($query_cf->num_rows() > 0) {

            } else {
                $data_array = array(
                    'client_id' => $client_id,
                    //'likes' => $likes,
                    //'people' => $talking_about_count,
                    'social_media_id' => 1,
                    'inserted_time' => date('Y-m-d H:i:s'),
                    'feeds' => $data,
                    'social_api_id' => 2
                );
                $this->db->insert('client_analytics_feed', $data_array);
                echo 'Insert First';
                // print_r($data);
            }
        }
    }

    public function facebook_profile_picture($client_id, $fbalias) {     
        $feed_url = "https://graph.facebook.com/$fbalias?fields=picture.width(160).height(160)&access_token=".FACEBOOK_API_TOEKN;
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_URL, $feed_url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($c);
        curl_close($c);

        $result = json_decode($data);
        if(isset($result->picture->data->url)){

            $profile_image = $result->picture->data->url;

            $data_array = array(
                'profile_image' => $profile_image,
                'updated_time' => date('Y-m-d H:i:s')
            );

            $this->db->where('id', $client_id);
            $this->db->update('client', $data_array);

        }

        //}
    }
    // Facebook api
    public function facebook_api() {        
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $keyword = 0;
        $new_comment = 0;
        $html = '';
        $facebookArr = array();
        $feed_history_detail = array();
        $multi = 0;
        $feed_translated_char = 0;       
        $query = $this->db->query("SELECT smc.id as smic_id,c.id,smc.alias,c.first_name,c.last_name,cc.lang_code FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id LEFT OUTER JOIN country AS cc ON cc.id=c.location WHERE  smc.social_media_id=1 AND smc.alias!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $smc_id = $value['smic_id'];
            $fbalias = $value['alias'];
            $source_lang=$value['lang_code'];
            $newpost_combine=0;
            $newpost_delta_combine=0;
            $total_char_feed_delta_combine=0;
            $total_char_feed_combine=0;
            //Get Facebook profile picture 
            $this->facebook_profile_picture($client_id, $fbalias);
            $feed_url = "https://graph.facebook.com/$fbalias/posts?limit=100&order=reverse_chronological&access_token=".FACEBOOK_API_TOEKN;

            $data=$this->fetchData($feed_url);

            $result_cmt = json_decode($data);
            /*echo '<pre>';
            print_r($result_cmt);
            die();
            exit();*/

            //print_r($result_cmt->error->message);
            /* if(isset($result_cmt->error->message) && $result_cmt->error->message=='Invalid OAuth access token')
            {
                $this->db->where('id', $smc_id);
                $this->db->delete('social_media_client');

            }else{

            }*/
            if(isset($result_cmt->data[0])){
                if(isset($result_cmt->data[0]->created_time)){
                    $created_time = $result_cmt->data[0]->created_time;
                    // Check feeds if already exists
                    $query_cf = $this->db->query("SELECT * FROM `client_feed` WHERE client_id=$client_id AND social_media_id=1 ORDER BY `last_post_created_time` DESC");
                    if ($query_cf->num_rows() > 0) {
                        $row = $query_cf->row();
                        $last_post_created_time = $row->last_post_created_time;
                        $date_2 = date_create($created_time);
                        $created_time_2 = date_format($date_2, "Y-m-d H:i:s");
                        $new_data = array();
                        $new_data_tr = array();
                        $total_char_feed_delta = 0;
                        $newpost_delta = 0;
                        // Check feed for delta insertion
                        foreach ($result_cmt->data as $array) {
                            if (isset($array->message)) {
                                $date_3 = date_create($array->created_time);
                                if (date_format($date_3, "Y-m-d H:i:s") > $last_post_created_time) {
                                    $translated_message_detla = $this->language_translate($array->message,$source_lang);
                                    $new_data2 = array('message' => $array->message, 'created_time' => $array->created_time, 'id' => $array->id);
                                    $new_data2_tr = array('message' => $translated_message_detla, 'created_time' => $array->created_time, 'id' => $array->id);
                                    array_push($new_data, $new_data2);
                                    array_push($new_data_tr, $new_data2_tr);
                                    $total_char_feed_delta+=strlen($translated_message_detla);
                                    $newpost_delta++;
                                }
                            }
                        }
                        $newpost_delta_combine+=$newpost_delta;
                        $total_char_feed_delta_combine+=$total_char_feed_delta;
                        if ($created_time_2 > $last_post_created_time) {
                            $data_array_2 = array(
                                'client_id' => $client_id,
                                'feed' => json_encode(array("data" => $new_data)),
                                'social_media_id' => 1,
                                'last_post_created_time' => $created_time,
                                'inserted_time' => date('Y-m-d H:i:s'),
                                'feed_en' => json_encode(array("data" => $new_data_tr))
                            );

                            $this->db->insert('client_feed', $data_array_2);
                            echo'insert Delta';
                            echo '<br>';
                        }
                    } 
                    else {
                        echo'Insert First Time';
                        echo '<br>';
                        $data_translate = array();
                        $total_char_feed=0;
                        $newpost=0;
                        foreach ($result_cmt->data as $array_val) {
                            if (isset($array_val->message)) {
                                $translated_message = $this->language_translate($array_val->message,$source_lang);
                                $data_translate2 = array('message' => $translated_message, 'created_time' => $array_val->created_time, 'id' => $array_val->id);
                                array_push($data_translate, $data_translate2);
                                $total_char_feed+=strlen($translated_message);
                                $newpost++;
                            }
                        }

                        $data_array = array(
                            'client_id' => $client_id,
                            'feed' => $data,
                            'social_media_id' => 1,
                            'last_post_created_time' => $created_time,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feed_en' => json_encode(array("data" => $data_translate))
                        );
                        $this->db->insert('client_feed', $data_array);
                        $newpost_combine+=$newpost;
                        $total_char_feed_combine+=$total_char_feed;
                    }
                    //Comments get and insert 
                    $result_cmts = json_decode($data);
                    $newcomment_combine_delta=0;
                    $total_char_cmt_combine_delta=0;
                    $total_char_cmt_combine=0;
                    $newcomment_combine=0;
                    if(isset($result_cmts->data)){
                        if(count($result_cmts->data)>0){
                            foreach ($result_cmts->data as $array_cmt) {
                                $post_id = $array_cmt->id;
                                echo '<br>';
                                echo '<br>';
                                $comments_url = "https://graph.facebook.com/$post_id/comments?limit=900&order=reverse_chronological&summary=1&access_token=".FACEBOOK_API_TOEKN;
                                $data_cmt=$this->fetchData($comments_url);
                                $result_cf = json_decode($data_cmt);
                                $total_count_comment = $result_cf->summary->total_count;
                                // Check client_comments if already exists
                                $query_client_comments = $this->db->query("SELECT * FROM `client_comments` WHERE 1 AND post_id='$post_id' ORDER BY `client_comments`.`total_count` DESC");
                                if ($query_client_comments->num_rows() > 0) {
                                    $row_client_comments = $query_client_comments->row();
                                    $total_count_cmt = $row_client_comments->total_count;
                                    if ($total_count_comment > $total_count_cmt) {
                                        $limit = $total_count_comment - $total_count_cmt;
                                        $new_comment = $limit;
                                        $comments_url_delta = "https://graph.facebook.com/$post_id/comments?limit=$limit&summary=1&order=reverse_chronological&access_token=".FACEBOOK_API_TOEKN;
                                        $data_cmt_delta=$this->fetchData($comments_url_delta);
                                        $result_cf_delta = json_decode($data_cmt_delta);
                                        $data_translate_cmt_delta = array();
                                        $total_char_cmt_delta = 0;
                                        $newcomment_delat = 0;
                                        // Check comments for delta insertion
                                        if (isset($result_cf_delta->data[0])) {
                                            foreach ($result_cf_delta->data as $array_val_cmt_delta) {
                                                if (isset($array_val_cmt_delta->message)) {
                                                    $translated_message_cmt_detla = $this->language_translate($array_val_cmt_delta->message,$source_lang);
                                                    $data_translate2_cmt_delta = array('created_time' => $array_val_cmt_delta->created_time, 'message' => $translated_message_cmt_detla, 'id' => $array_val_cmt_delta->id);
                                                    array_push($data_translate_cmt_delta, $data_translate2_cmt_delta);                               
                                                    $total_char_cmt_delta+=strlen($translated_message_cmt_detla);
                                                    $newcomment_delat++;
                                                }
                                            }
                                        }
                                        // $newcomment_combine+=$newcomment;
                                        $total_char_cmt_combine_delta+=$total_char_cmt_delta;
                                        $newcomment_combine_delta+=$newcomment_delat;
                                        if (isset($result_cf_delta->data[0])) {
                                            $total_count_comment_delta = $result_cf_delta->summary->total_count;
                                            $created_time_cmts_delta = $result_cf_delta->data[0]->created_time;
                                            $data_array_cmt_delta = array(
                                                'client_id' => $client_id,
                                                'post_id' => $post_id,
                                                'comment' => $data_cmt_delta,
                                                'social_media_id' => '1',
                                                'last_comment_created_time' => $created_time_cmts_delta,
                                                'total_count' => $total_count_comment_delta,
                                                'comment_en' => json_encode(array("data" => $data_translate_cmt_delta)),
                                                'inserted_time' => date('Y-m-d H:i:s')
                                            );
                                            $this->db->insert('client_comments', $data_array_cmt_delta);                         
                                        }
                                        echo 'Delta found';
                                    }
                                    echo 'found';
                                    echo '<br>';
                                    echo '<br>';
                                    //}
                                } else {
                                    echo 'Not found';
                                    echo '<br>';
                                    echo '<br>';
                                    $data_translate_cmt = array();
                                    $newcomment=0;
                                    $total_char_cmt=0;
                                    if (isset($result_cf->data[0])) {
                                        foreach ($result_cf->data as $array_val_cmt) {
                                            if (isset($array_val_cmt->message)) {
                                                $translated_message_cmt=$this->language_translate($array_val_cmt->message,$source_lang);
                                                $data_translate2_cmt = array('created_time' => $array_val_cmt->created_time, 'message' =>$translated_message_cmt, 'id' => $array_val_cmt->id);
                                                array_push($data_translate_cmt, $data_translate2_cmt);
                                                $total_char_cmt+=strlen($translated_message_cmt);
                                                $newcomment++;
                                            }
                                        }
                                    }
                                    if (isset($result_cf->data[0])) {
                                        $created_time_cmts = $result_cf->data[0]->created_time;
                                        $data_array_cmt = array(
                                            'client_id' => $client_id,
                                            'post_id' => $post_id,
                                            'comment' => $data_cmt,
                                            'social_media_id' => '1',
                                            'last_comment_created_time' => $created_time_cmts,
                                            'total_count' => $total_count_comment,
                                            'comment_en' => json_encode(array("data" => $data_translate_cmt)),
                                            'inserted_time' => date('Y-m-d H:i:s')
                                        );
                                        $this->db->insert('client_comments', $data_array_cmt);
                                    }
                                    $newcomment_combine+=$newcomment;
                                    $total_char_cmt_combine+=$total_char_cmt;
                                }
                            }
                        }
                    }
                    $first_name = $value['first_name'];
                    $last_name = $value['last_name'];
                    $total_post = $newpost_delta_combine+$newpost_combine;
                    $total_comment = ($newcomment_combine_delta)+($newcomment_combine);
                    $html.=$first_name . '<br>' . $last_name . '<br>' . $total_post . '<br>' . $total_comment;
                    $facebookArr[$multi] = array(
                        'artistname' => $first_name . ' ' . $last_name,
                        'total_new_post' => $total_post,
                        'total_new_comment' => $total_comment,
                    );

                    $feed_history_detail[$multi] = array(
                        'client_id' => $client_id,
                        'post_count' => $newpost_delta_combine+$newpost_combine,
                        'comment_count' => ($newcomment_combine_delta)+($newcomment_combine),
                    );
                    $feed_translated_char+=($total_char_cmt_combine + $total_char_feed_combine+$total_char_feed_delta_combine+$total_char_cmt_combine_delta);

                    $multi++;
                    //      print_r(json_encode($feed_history_detail));

                }//date check

            }//check data

        }

        $data_array_feed_history = array(
            'feed_history_detail' => json_encode($feed_history_detail),
            'feed_translated_character_count' => $feed_translated_char,
            'inserted_time' => date("Y-m-d H:i:s"),
            'social_media_id' => '1'
        );

        $this->db->insert('feed_history', $data_array_feed_history);

        //print_r(json_encode($facebookArr));
        return ($facebookArr);
        //return $html;
        //echo  $html;
    }


    public function weibo_api() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $keyword = 0;
        $new_comment = 0;
        $html = '';
        $facebookArr = array();
        $feed_history_detail = array();
        $multi = 0;
        $feed_translated_char = 0;       
        $query = $this->db->query("SELECT smc.access_token,c.id,smc.alias,c.first_name,c.last_name,cc.lang_code FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id LEFT OUTER JOIN country AS cc ON cc.id=c.location WHERE  smc.social_media_id=5  and smc.access_token!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            //$fbalias = $value['alias'];
            $access_tkn = $value['access_token'];
            $source_lang=$value['lang_code'];
            $newpost_combine=0;
            $newpost_delta_combine=0;
            $total_char_feed_delta_combine=0;
            $total_char_feed_combine=0;
            //Get Facebook profile picture 
            //$this->facebook_profile_picture($client_id, $fbalias);
            //$feed_url = "https://graph.facebook.com/$fbalias/posts?limit=100&order=reverse_chronological&access_token=".FACEBOOK_API_TOEKN;

            //$feed_url = "https://api.weibo.com/2/statuses/user_timeline.json?access_token=$access_tkn&count=5";
            $feed_url = "https://api.weibo.com/2/statuses/user_timeline.json?access_token=$access_tkn";
            $data=$this->fetchData($feed_url);


            $result_cmt = json_decode($data);
            /*            
            echo '<pre>';
            print_r($result_cmt);
            echo '</pre>';

            die();*/


            /*$created_time="Wed Jun 08 13:08:11 +0800 2016";
            $date_2 = date_create($created_time);
            $created_time_2 = date_format($date_2, "Y-m-d H:i:s");
*/
            if(isset($result_cmt->statuses[0])){

                $created_time00 = $result_cmt->statuses[0]->created_at;
                $date_2 = strtotime($created_time00);
                $created_time = date("Y-m-d H:i:s",$date_2);

                // Check feeds if already exists

                $query_cf = $this->db->query("SELECT * FROM `client_feed` WHERE client_id=$client_id AND social_media_id=5 ORDER BY `last_post_created_time` DESC");

                if ($query_cf->num_rows() > 0) {
                    //die('00000');
                    $row = $query_cf->row();
                    $last_post_created_time = $row->last_post_created_time;
                    $date_2 = date_create($created_time);
                    $created_time_2 = date_format($date_2, "Y-m-d H:i:s");
                    $new_data = array();
                    $new_data_tr = array();
                    $total_char_feed_delta = 0;
                    $newpost_delta = 0;
                    // Check feed for delta insertion
                    foreach ($result_cmt->statuses as $array) {
                        /*echo '<pre>';
                    print_r($array);
                    die();*/
                        if (isset($array->text)) {
                            $date_3 = date_create($array->created_at);
                            if (date_format($date_3, "Y-m-d H:i:s") > $last_post_created_time) {
                                $translated_message_detla = $this->language_translate($array->text,$source_lang);
                                $new_data2 = array('text' => $array->text, 'created_at' => $array->created_at, 'id' => $array->mid);
                                $new_data2_tr = array('text' => $translated_message_detla, 'created_at' => $array->created_at, 'id' => $array->mid);
                                array_push($new_data, $new_data2);
                                array_push($new_data_tr, $new_data2_tr);
                                $total_char_feed_delta+=strlen($translated_message_detla);
                                $newpost_delta++;
                            }
                        }
                    }
                    $newpost_delta_combine+=$newpost_delta;
                    $total_char_feed_delta_combine+=$total_char_feed_delta;
                    if ($created_time_2 > $last_post_created_time) {
                        $data_array_2 = array(
                            'client_id' => $client_id,
                            'feed' => json_encode(array("data" => $new_data)),
                            'social_media_id' => 1,
                            'last_post_created_time' => $created_time,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feed_en' => json_encode(array("data" => $new_data_tr))
                        );

                        $this->db->insert('client_feed', $data_array_2);
                        echo'insert Delta';
                        echo '<br>';
                    }
                } 
                else {
                    // die('aaaaaa');
                    echo'Insert First Time';
                    echo '<br>';
                    $data_translate = array();
                    $data_orig = array();
                    $total_char_feed=0;
                    $newpost=0;
                    foreach ($result_cmt->statuses as $array_val) {
                        if (isset($array_val->text)) {
                            $translated_message = $this->language_translate($array_val->text,$source_lang);
                            $data_translate2 = array('text' => $translated_message, 'created_at' => $array_val->created_at, 'id' => $array_val->mid);
                            $data_array = array('text' => $array_val->text, 'created_at' => $array_val->created_at, 'id' => $array_val->mid);
                            array_push($data_translate, $data_translate2);
                            array_push($data_orig, $data_array);
                            $total_char_feed+=strlen($translated_message);
                            $newpost++;
                        }
                    }

                    $data_array = array(
                        'client_id' => $client_id,
                        'feed' => json_encode(array("data" => $data_orig)),
                        'social_media_id' => 5,
                        'last_post_created_time' => $created_time,
                        'inserted_time' => date('Y-m-d H:i:s'),
                        'feed_en' => json_encode(array("data" => $data_translate))
                    );
                    $this->db->insert('client_feed', $data_array);
                    $newpost_combine+=$newpost;
                    $total_char_feed_combine+=$total_char_feed;
                }
                //Comments get and insert 
                /*exit();
            die('exit');*/
                $result_cmts = json_decode($data);
                $newcomment_combine_delta=0;
                $total_char_cmt_combine_delta=0;
                $total_char_cmt_combine=0;
                $newcomment_combine=0;
                foreach ($result_cmts->statuses as $array_cmt) {
                    $post_id = $array_cmt->mid;
                    //echo '<br>';
                    //echo '<br>';
                    /*$comments_url = "https://graph.facebook.com/$post_id/comments?limit=900&order=reverse_chronological&summary=1&access_token=".FACEBOOK_API_TOEKN;*/
                    $comments_url = "https://api.weibo.com/2/comments/show.json?id=$post_id&access_token=$access_tkn&count=200";
                    $data_cmt=$this->fetchData($comments_url);
                    $result_cf = json_decode($data_cmt);
                    $total_count_comment = $result_cf->total_number;
                    // Check client_comments if already exists
                    $query_client_comments = $this->db->query("SELECT * FROM `client_comments` WHERE 1 AND post_id='$post_id' ORDER BY `client_comments`.`total_count` DESC");
                    if ($query_client_comments->num_rows() > 0) {
                        $row_client_comments = $query_client_comments->row();
                        $total_count_cmt = $row_client_comments->total_count;
                        if ($total_count_comment > $total_count_cmt) {
                            $limit = $total_count_comment - $total_count_cmt;
                            $new_comment = $limit;
                            $comments_url = "https://api.weibo.com/2/comments/show.json?id=$post_id&access_token=$access_tkn&count=$new_comment";
                            $data_cmt_delta=$this->fetchData($comments_url);
                            $result_cf_delta = json_decode($data_cmt_delta);
                            $data_translate_cmt_delta = array();
                            $comment_orig = array();
                            $total_char_cmt_delta = 0;
                            $newcomment_delat = 0;
                            // Check comments for delta insertion
                            foreach ($result_cf_delta->comments as $array_val_cmt_delta) {
                                if (isset($array_val_cmt_delta->text)) {
                                    $translated_message_cmt_detla = $this->language_translate($array_val_cmt_delta->text,$source_lang);
                                    $data_translate2_cmt_delta = array('created_at' => $array_val_cmt_delta->created_at, 'text' => $translated_message_cmt_detla, 'id' => $array_val_cmt_delta->id);

                                    $comment_orig_data = array('created_at' => $array_val_cmt_delta->created_at, 'text' => $array_val_cmt_delta->text, 'id' => $array_val_cmt_delta->id);

                                    array_push($data_translate_cmt_delta, $data_translate2_cmt_delta);                               
                                    array_push($comment_orig, $comment_orig_data);                               
                                    $total_char_cmt_delta+=strlen($translated_message_cmt_detla);
                                    $newcomment_delat++;
                                }
                            }
                            // $newcomment_combine+=$newcomment;
                            $total_char_cmt_combine_delta+=$total_char_cmt_delta;
                            $newcomment_combine_delta+=$newcomment_delat;
                            if (isset($result_cf_delta->comments[0])) {
                                $total_count_comment_delta = $result_cf_delta->total_number;
                                $first_comment_time = $result_cf_delta->comments[0]->created_at;
                                $time_string=strtotime($first_comment_time);
                                $created_time_cmts_delta=date('Y-m-d H:i:s',$time_string);
                                $data_array_cmt_delta = array(
                                    'client_id' => $client_id,
                                    'post_id' => $post_id,
                                    'comment' => json_encode(array("data" => $comment_orig)),
                                    'social_media_id' => '5',
                                    'last_comment_created_time' => $created_time_cmts_delta,
                                    'total_count' => $total_count_comment_delta,
                                    'comment_en' => json_encode(array("data" => $data_translate_cmt_delta)),
                                    'inserted_time' => date('Y-m-d H:i:s')
                                );
                                $this->db->insert('client_comments', $data_array_cmt_delta);                         
                            }
                            echo 'Delta found';
                        }
                        echo 'found';
                        echo '<br>';
                        echo '<br>';
                        //}
                    } else
                    {
                        echo 'Not found';
                        echo '<br>';
                        echo '<br>';
                        $data_translate_cmt = array();
                        $comment_orig = array();
                        $newcomment=0;
                        $total_char_cmt=0;
                        foreach ($result_cf->comments as $array_val_cmt) {
                            if (isset($array_val_cmt->text)) {
                                $translated_message_cmt=$this->language_translate($array_val_cmt->text,$source_lang);
                                $data_translate2_cmt = array('created_at' => $array_val_cmt->created_at, 'text' =>$translated_message_cmt, 'id' => $array_val_cmt->id);
                                array_push($data_translate_cmt, $data_translate2_cmt);

                                $comment_orig_data = array('created_at' => $array_val_cmt->created_at, 'text' => $array_val_cmt->text, 'id' => $array_val_cmt->id);


                                array_push($comment_orig, $comment_orig_data);                               

                                $total_char_cmt+=strlen($translated_message_cmt);
                                $newcomment++;
                            }
                        }
                        if (isset($result_cf->comments[0])) {
                            $created_time_cmts = $result_cf->comments[0]->created_at;
                            $data_array_cmt = array(
                                'client_id' => $client_id,
                                'post_id' => $post_id,
                                'comment' => json_encode(array("data" => $comment_orig)),
                                'social_media_id' => '5',
                                'last_comment_created_time' => $created_time_cmts,
                                'total_count' => $total_count_comment,
                                'comment_en' => json_encode(array("data" => $data_translate_cmt)),
                                'inserted_time' => date('Y-m-d H:i:s')
                            );
                            $this->db->insert('client_comments', $data_array_cmt);
                        }
                        $newcomment_combine+=$newcomment;
                        $total_char_cmt_combine+=$total_char_cmt;
                    }
                }
                $first_name = $value['first_name'];
                $last_name = $value['last_name'];
                $total_post = $newpost_delta_combine+$newpost_combine;
                $total_comment = ($newcomment_combine_delta)+($newcomment_combine);
                $html.=$first_name . '<br>' . $last_name . '<br>' . $total_post . '<br>' . $total_comment;
                $facebookArr[$multi] = array(
                    'artistname' => $first_name . ' ' . $last_name,
                    'total_new_post' => $total_post,
                    'total_new_comment' => $total_comment,
                );

                $feed_history_detail[$multi] = array(
                    'client_id' => $client_id,
                    'post_count' => $newpost_delta_combine+$newpost_combine,
                    'comment_count' => ($newcomment_combine_delta)+($newcomment_combine),
                );
                $feed_translated_char+=($total_char_cmt_combine + $total_char_feed_combine+$total_char_feed_delta_combine+$total_char_cmt_combine_delta);

                $multi++;
                //      print_r(json_encode($feed_history_detail));

            }
        }
        $data_array_feed_history = array(
            'feed_history_detail' => json_encode($feed_history_detail),
            'feed_translated_character_count' => $feed_translated_char,
            'inserted_time' => date("Y-m-d H:i:s"),
            'social_media_id' => '5'
        );

        $this->db->insert('feed_history', $data_array_feed_history);
        //print_r(json_encode($facebookArr));
        return ($facebookArr);
        //return $html;
        //echo  $html;
    }


    public function weibo_followers_friends() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $keyword = 0;
        $new_comment = 0;
        $html = '';
        $facebookArr = array();
        $feed_history_detail = array();
        $multi = 0;
        $feed_translated_char = 0;       
        $query = $this->db->query("SELECT smc.access_token,c.id,smc.alias,c.first_name,c.last_name,cc.lang_code FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id LEFT OUTER JOIN country AS cc ON cc.id=c.location WHERE  smc.social_media_id=5  and smc.access_token!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            //$fbalias = $value['alias'];
            $access_tkn = $value['access_token'];
            $source_lang=$value['lang_code'];
            $newpost_combine=0;
            $newpost_delta_combine=0;
            $total_char_feed_delta_combine=0;
            $total_char_feed_combine=0;

            $c = new SaeTClientV2( $this->WB_AKEY , $this->WB_SKEY , $access_tkn );

            $uid_get = $c->get_uid();
            $uid = $uid_get['uid'];


            $result =$c->show_user_by_id($uid);
            /*$feed_url = "https://api.weibo.com/2/friendships/followers.json?access_token=$access_tkn&uid=$uid";
            $data=$this->fetchData($feed_url);
            $result = json_decode($data);*/
            /* echo '<pre>';
            print_r($result);
            die();*/


            $followers=isset($result['followers_count'])?$result['followers_count']:-1;
            $friends=isset($result['friends_count'])?$result['friends_count']:-1;
            $name=isset($result['name'])?$result['name']:'';



            //$talking_about_count = $result->total_number;
            $query_cf = $this->db->query("SELECT * FROM `client_analytics_feed` WHERE client_id=$client_id AND social_media=5  AND `social_api_id`=5 ORDER BY `id` DESC");
            if ($query_cf->num_rows() > 0) {
                $row_client_info = $query_cf->row();
                $likesdb = $row_client_info->likes;
                if ($followers != $likesdb) {

                    $data=array(
                        'followers'=>$followers,
                        'friends'=>$friends,
                        'name'=>$name,
                        'id'=>$uid
                    );
                    $data_array = array(
                        'client_id' => $client_id,
                        'likes' => $followers,
                        'people' => $friends,
                        'social_media' => 5,
                        'inserted_time' => date('Y-m-d H:i:s'),
                        'feeds' => json_encode($data),
                        'social_api_id' => 5
                    );
                    $this->db->insert('client_analytics_feed', $data_array);
                    echo 'Found';
                }
            } 
            else {
                $data=array(
                    'followers'=>$followers,
                    'friends'=>$friends,
                    'name'=>$name,
                    'id'=>$uid
                );
                $data_array = array(
                    'client_id' => $client_id,
                    'likes' => $followers,
                    'people' => $friends,
                    'social_media' => 5,
                    'inserted_time' => date('Y-m-d H:i:s'),
                    'feeds' => json_encode($data),
                    'social_api_id' => 5
                );
                $this->db->insert('client_analytics_feed', $data_array);
                echo 'Insert First';
            }

        }

    }


    // Twitter Api
    public function twitter_api() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $total_tweet = 0;
        $multi = 0;
        $feed_translated_char = 0;
        $feed_history_detail = array();
        $twitterArr=array();
        $query = $this->db->query("SELECT c.id,smc.alias,first_name,last_name,cc.lang_code FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id LEFT OUTER JOIN country AS cc ON cc.id=c.location WHERE smc.social_media_id=3 AND smc.alias!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $tw_screen_name = $value['alias'];
            $source_lang=$value['lang_code'];
            $newpost_combine=0;
            $newpost_delta_combine=0;
            $total_char_feed_delta_combine=0;
            $total_char_feed_combine=0;

            $data = $this->returnTweet($tw_screen_name, '100');
            $data_tw_feed = json_decode($data);
            /*print_r($data_tw_feed);
            die();*/
            if(isset($data_tw_feed[0])){
                if($data_tw_feed[0]->created_at){
                    $created_time = $data_tw_feed[0]->created_at;
                    $data_translate = array();

                    //print_r($data_array);
                    $query_cf = $this->db->query("SELECT * FROM `client_feed` WHERE client_id=$client_id AND social_media_id=3 ORDER BY `last_post_created_time` DESC");
                    if ($query_cf->num_rows() > 0) {
                        $row = $query_cf->row();
                        $last_post_created_time = $row->last_post_created_time;
                        $date_2 = date_create($created_time);
                        $created_time_2 = date_format($date_2, "Y-m-d H:i:s");
                        $new_data = array();
                        $data_translate_cmt = array();
                        $total_char_feed_delta = 0;
                        $newpost_delta = 0;
                        foreach ($data_tw_feed as $array) {
                            $date_3 = date_create($array->created_at);
                            // Check feed for delta insertion
                            if (date_format($date_3, "Y-m-d H:i:s") > $last_post_created_time) {
                                if($array->text!='')
                                {
                                    $translated_message_detla= $this->language_translate($array->text,$source_lang);
                                    $new_data2 = array('created_at' => $array->created_at, "id" => $array->id, "id_str" => $array->id_str, "text" => $array->text);
                                    $data_translate_cmt2 = array('created_at' => $array->created_at, "id" => $array->id, "id_str" => $array->id_str, "text" => $translated_message_detla);
                                    array_push($new_data, $new_data2);
                                    array_push($data_translate_cmt, $data_translate_cmt2);
                                    $total_char_feed_delta+=strlen($translated_message_detla);
                                    $newpost_delta++;
                                }
                            }
                        }
                        $newpost_delta_combine+=$newpost_delta;
                        $total_char_feed_delta_combine+=$total_char_feed_delta;
                        if ($created_time_2 > $last_post_created_time) {
                            if(count($new_data)>0)
                            {
                                $data_array_delta = array(
                                    'client_id' => $client_id,
                                    'feed' => json_encode($new_data),
                                    'social_media_id' => 3,
                                    'last_post_created_time' => date_format(date_create($created_time), "Y-m-d H:i:s"),
                                    'inserted_time' => date('Y-m-d H:i:s'),
                                    'feed_en' => json_encode($data_translate_cmt)
                                );
                                $this->db->insert('client_feed', $data_array_delta);
                            }
                            echo'ok';
                            echo'Delta';
                            echo'<br>';
                        }
                        //print_r(json_encode($new_data));
                    } else {
                        $total_char_feed=0;
                        $newpost=0;
                        $data_translate = array();
                        foreach ($data_tw_feed as $array) {
                            $translated_message=$this->language_translate($array->text,$source_lang);
                            $data_translate2 = array('created_at' => $array->created_at, "id" => $array->id, "id_str" => $array->id_str, "text" =>$translated_message );
                            array_push($data_translate, $data_translate2);
                            $total_char_feed+=strlen($translated_message);
                            $newpost++;
                        }
                        $data_array = array(
                            'client_id' => $client_id,
                            'feed' => $data,
                            'social_media_id' => 3,
                            'last_post_created_time' => date_format(date_create($created_time), "Y-m-d H:i:s"),
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feed_en' => json_encode($data_translate)
                        );
                        $this->db->insert('client_feed', $data_array);
                        $newpost_combine+=$newpost;
                        $total_char_feed_combine+=$total_char_feed;
                        echo'insert';
                        echo'<br>';
                    }
                    $first_name = $value['first_name'];
                    $last_name = $value['last_name'];
                    $total_post = $total_tweet;
                    $twitterArr[$multi] = array(
                        'artistname' => $first_name . ' ' . $last_name,
                        'total_tweets' => $total_post,
                    );

                    $feed_history_detail[$multi] = array(
                        'client_id' => $client_id,
                        'post_count' => $newpost_delta_combine+$newpost_combine,
                        'comment_count' =>'0',
                    );
                    $feed_translated_char+=( $total_char_feed_combine+$total_char_feed_delta_combine);
                    $multi++;
                } 

            }
        }
        $data_array_feed_history = array(
            'feed_history_detail' => json_encode($feed_history_detail),
            'feed_translated_character_count' => $feed_translated_char,
            'inserted_time' => date("Y-m-d H:i:s"),
            'social_media_id' => '3'
        );

        $this->db->insert('feed_history', $data_array_feed_history);
        return ($twitterArr);
    }






    public function twitter_friends_followers() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $query = $this->db->query("SELECT c.id,smc.alias FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=3 AND smc.alias!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $tw_screen_name = $value['alias'];
            $data = $this->return_twitter_profile($tw_screen_name);
            $result=  json_decode($data);
            if(isset($result[0]->user)){
                $likes = $result[0]->user->followers_count;
                $query_cf = $this->db->query("SELECT * FROM `client_analytics_feed` WHERE client_id=$client_id AND social_media_id=3  AND `social_api_id`=6 ORDER BY `id` DESC");
                if ($query_cf->num_rows() > 0) {
                    $row_client_info = $query_cf->row();
                    $likesdb = $row_client_info->likes;
                    if ($likes > $likesdb) {

                        $data_array = array(
                            'client_id' => $client_id,
                            'likes' => $likes,
                            //'people' => $talking_about_count,
                            'social_media_id' => 3,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feeds' => $data,
                            'social_api_id' => 6
                        );
                        $this->db->insert('client_analytics_feed', $data_array);
                        echo 'Found';
                    }
                } 
                else {
                    $data_array = array(
                        'client_id' => $client_id,
                        'likes' => $likes,
                        //'people' => $talking_about_count,
                        'social_media_id' => 3,
                        'inserted_time' => date('Y-m-d H:i:s'),
                        'feeds' => $data,
                        'social_api_id' => 6
                    );
                    $this->db->insert('client_analytics_feed', $data_array);
                    echo 'Insert First';
                }
            }
        }

    }

    public function buildBaseString($baseURI, $method, $params) {
        $r = array();
        ksort($params);
        foreach ($params as $key => $value) {
            $r[] = "$key=" . rawurlencode($value);
        }
        return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
    }

    public function buildAuthorizationHeader($oauth) {
        $r = 'Authorization: OAuth ';
        $values = array();
        foreach ($oauth as $key => $value)
            $values[] = "$key=\"" . rawurlencode($value) . "\"";
        $r .= implode(', ', $values);
        return $r;
    }

    public function returnTweet($screen_name, $limit) {
        $oauth_access_token = T_OAUTH_ACCESS_TOKEN;
        $oauth_access_token_secret = T_OAUTH_ACCESS_TOKEN_SECRET;
        $consumer_key = T_CONSUMER_KEY;
        $consumer_secret = T_CONSUMER_SECRET;

        $twitter_timeline = "user_timeline";  //  mentions_timeline / user_timeline / home_timeline / retweets_of_me
        //  create request
        $request = array(
            'screen_name' => "$screen_name",
            'count' => "$limit",
            'include_entities' => 1,
            'trim_user' => true,
            'exclude_replies' => true,
            'contributor_details' => true,
            //'include_rts'=>0
        );

        $oauth = array(
            'oauth_consumer_key' => $consumer_key,
            'oauth_nonce' => time(),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_token' => $oauth_access_token,
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0'
        );

        //  merge request and oauth to one array
        $oauth = array_merge($oauth, $request);

        //  do some magic
        $base_info = $this->buildBaseString("https://api.twitter.com/1.1/statuses/$twitter_timeline.json", 'GET', $oauth);
        $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;

        //  make request
        $header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
        $options = array(CURLOPT_HTTPHEADER => $header,
                         CURLOPT_HEADER => false,
                         CURLOPT_URL => "https://api.twitter.com/1.1/statuses/$twitter_timeline.json?" . http_build_query($request),
                         CURLOPT_RETURNTRANSFER => true,
                         CURLOPT_SSL_VERIFYPEER => false);

        $feed = curl_init();
        curl_setopt_array($feed, $options);
        $json = curl_exec($feed);
        curl_close($feed);
        //print_r($json);
        return $json;
    }


    public function return_twitter_profile($tw_screen_name) {
        $oauth_access_token = T_OAUTH_ACCESS_TOKEN;
        $oauth_access_token_secret = T_OAUTH_ACCESS_TOKEN_SECRET;
        $consumer_key = T_CONSUMER_KEY;
        $consumer_secret = T_CONSUMER_SECRET;

        $twitter_timeline = "user_timeline";  //  mentions_timeline / user_timeline / home_timeline / retweets_of_me
        //  create request
        $request = array(
            'screen_name' => "$tw_screen_name",
            'count' => "1",
        );

        $oauth = array(
            'oauth_consumer_key' => $consumer_key,
            'oauth_nonce' => time(),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_token' => $oauth_access_token,
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0'
        );

        //  merge request and oauth to one array
        $oauth = array_merge($oauth, $request);

        //  do some magic
        $base_info = $this->buildBaseString("https://api.twitter.com/1.1/statuses/$twitter_timeline.json", 'GET', $oauth);
        $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;

        //  make request
        $header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
        $options = array(CURLOPT_HTTPHEADER => $header,
                         CURLOPT_HEADER => false,
                         CURLOPT_URL => "https://api.twitter.com/1.1/statuses/$twitter_timeline.json?" . http_build_query($request),
                         CURLOPT_RETURNTRANSFER => true,
                         CURLOPT_SSL_VERIFYPEER => false);

        $feed = curl_init();
        curl_setopt_array($feed, $options);
        $json = curl_exec($feed);
        curl_close($feed);
        //print_r($json);
        return $json;
    }


    // Youtube Api
    public function youtube_api() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $newpost = 0;
        $new_comment = 0;
        $multi = 0;
        $keyApi = 'AIzaSyDXZg2w8TQG93xmt8UGuvactJeC1l1ZzbQ';
        $feed_history_detail=array();
        $youtubeArr=array();
        $multi = 0;
        $feed_translated_char = 0;
        $query = $this->db->query("SELECT c.id,smc.alias,first_name,last_name,cc.lang_code FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id LEFT OUTER JOIN country AS cc ON cc.id=c.location WHERE  smc.social_media_id=4 AND smc.alias!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $youtube_forUsername = $value['alias'];
            $source_lang=$value['lang_code'];
            $newpost_combine=0;
            $newpost_delta_combine=0;
            $total_char_feed_delta_combine=0;
            $total_char_feed_combine=0;

            $channels_url = "https://www.googleapis.com/youtube/v3/channels?key=$keyApi&forUsername=$youtube_forUsername&part=id";
            $data = $this->fetchData($channels_url);
            $result = json_decode($data);
            if(isset($result->items[0]->id)){
                $channelId = $result->items[0]->id;

                $feed_url = "https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId=$channelId&maxResults=50&key=$keyApi";
                $data_feed = $this->fetchData($feed_url);
                $result_feed = json_decode($data_feed);
                if(isset($result_feed->items[0])){
                    if(isset($result_feed->items[0]->snippet->publishedAt)){

                        $created_time = $result_feed->items[0]->snippet->publishedAt;

                        $query_cf = $this->db->query("SELECT * FROM `client_feed` WHERE client_id=$client_id AND social_media_id=4 ORDER BY `last_post_created_time` DESC");
                        if ($query_cf->num_rows() > 0) {

                            $row = $query_cf->row();
                            $last_post_created_time = $row->last_post_created_time;
                            $date_2 = date_create($created_time);
                            $created_time_2 = date_format($date_2, "Y-m-d H:i:s");
                            $new_data = array();
                            $new_data_tr = array();
                            $total_char_feed = 0;
                            $newpost = 0;
                            foreach ($result_feed->items as $array) {
                                $date_3 = date_create($array->snippet->publishedAt);
                                // Check feed for delta insertion
                                if (date_format($date_3, "Y-m-d H:i:s") > $last_post_created_time) {
                                    if(isset($array->snippet->title)){
                                        $title=$array->snippet->title;
                                        $translated_message_detla=$this->language_translate($array->snippet->title,$source_lang);  
                                    }else{
                                        $translated_message_detla='';
                                        $title='';
                                    }                       
                                    $new_data2 = array('id' => array('kind' => $array->id->kind, 'videoId' => $array->id->videoId), 'snippet' => array("publishedAt" => $array->snippet->publishedAt, "title" =>$title, "description" => $array->snippet->description), 'id' => $array->id);
                                    $new_data2_tr = array('id' => array('kind' => $array->id->kind, 'videoId' => $array->id->videoId), 'snippet' => array("publishedAt" => $array->snippet->publishedAt, "title" =>$translated_message_detla, "description" => $this->language_translate($array->snippet->description,$source_lang)), 'id' => $array->id);
                                    array_push($new_data, $new_data2);
                                    array_push($new_data_tr, $new_data2_tr);
                                    $total_char_feed+=strlen($translated_message_detla);
                                    $newpost++;

                                }
                            }
                            $newpost_delta_combine+=$newpost_delta;
                            $total_char_feed_delta_combine+=$total_char_feed_delta;
                            if ($created_time_2 > $last_post_created_time) {
                                $data_array_2 = array(
                                    'client_id' => $client_id,
                                    'feed' => json_encode(array("items" => $new_data)),
                                    'social_media_id' => 4,
                                    'last_post_created_time' => $created_time,
                                    'inserted_time' => date('Y-m-d H:i:s'),
                                    'feed_en' => json_encode(array("items" => $new_data_tr))
                                );
                                $this->db->insert('client_feed', $data_array_2);
                                echo'insert Delta';
                                echo '<br>';
                                echo '<br>';
                            }
                            //print_r(json_encode(array("items" => $new_data)));
                        } else {
                            echo'Insert First Time';
                            echo '<br>';
                            echo '<br>';
                            $data_translate = array();
                            $total_char_feed=0;
                            $newpost=0;
                            foreach ($result_feed->items as $array) {
                                if(isset($array->snippet->title)){
                                    $translated_message=$this->language_translate($array->snippet->title,$source_lang);
                                }else{
                                    $translated_message='';  
                                }

                                $data_translate2 = array('id' => array('kind' => $array->id->kind, 'videoId' => $array->id->videoId), 'snippet' => array("publishedAt" => $array->snippet->publishedAt, "title" => $translated_message, "description" => $this->language_translate($array->snippet->description,$source_lang)), 'id' => $array->id);
                                array_push($data_translate, $data_translate2);
                                $total_char_feed+=strlen($translated_message);
                                $newpost++;
                            }

                            $data_array = array(
                                'client_id' => $client_id,
                                'feed' => $data_feed,
                                'social_media_id' => 4,
                                'last_post_created_time' => $created_time,
                                'inserted_time' => date('Y-m-d H:i:s'),
                                'feed_en' => json_encode(array("items" => $data_translate))
                            );
                            $this->db->insert('client_feed', $data_array);
                            $newpost_combine+=$newpost;
                            $total_char_feed_combine+=$total_char_feed;
                        }

                        $newcomment_combine_delta=0;
                        $total_char_cmt_combine_delta=0;
                        $total_char_cmt_combine=0;
                        $newcomment_combine=0;

                        foreach ($result_feed->items as $array_cmt) {
                            if (isset($array_cmt->id->videoId)) {
                                $videoid = $array_cmt->id->videoId;
                                $comment_url = "https://www.googleapis.com/youtube/v3/commentThreads?part=snippet,replies&maxResults=100&videoId=$videoid&order=time&key=$keyApi";
                                $data_comment = $this->fetchData($comment_url);
                                $result_cmt = json_decode($data_comment);
                                if (isset($result_cmt->items)) {
                                    if(isset($result_cmt->items[0])){
                                        $created_time_cmts_delta = $result_cmt->items[0]->snippet->topLevelComment->snippet->publishedAt;
                                        $query_client_comments = $this->db->query("SELECT * FROM `client_comments` WHERE 1 AND post_id='$videoid' AND social_media_id=4 ORDER BY `last_comment_created_time` DESC");
                                        if ($query_client_comments->num_rows() > 0) {
                                            $row = $query_client_comments->row();
                                            $last_comment_created_time = $row->last_comment_created_time;
                                            $date_2 = date_create($created_time_cmts_delta);
                                            $created_time_2_cmt = date_format($date_2, "Y-m-d H:i:s");
                                            $new_data_cmt = array();
                                            $new_data_cmt_tr = array();
                                            $total_char_cmt_delta = 0;
                                            $newcomment_delat = 0;
                                            // Check comments for delta insertion
                                            foreach ($result_cmt->items as $array_cmt) {
                                                $date_3 = date_create($array_cmt->snippet->topLevelComment->snippet->publishedAt);
                                                if (date_format($date_3, "Y-m-d H:i:s") > $last_comment_created_time) {
                                                    $translated_message_cmt_detla=$this->language_translate(str_replace('\ufeff', '', htmlspecialchars($array_cmt->snippet->topLevelComment->snippet->textDisplay)),$source_lang);
                                                    $new_data2_cmt = array('snippet' => array("videoId" => $array_cmt->snippet->videoId, "topLevelComment" => array("kind" => $array_cmt->snippet->topLevelComment->kind, "snippet" => array("textDisplay" => str_replace('\ufeff', '', htmlspecialchars($array_cmt->snippet->topLevelComment->snippet->textDisplay)), "likeCount" => $array_cmt->snippet->topLevelComment->snippet->likeCount, "publishedAt" => $array_cmt->snippet->topLevelComment->snippet->publishedAt))));
                                                    $new_data2_cmt_tr = array('snippet' => array("videoId" => $array_cmt->snippet->videoId, "topLevelComment" => array("kind" => $array_cmt->snippet->topLevelComment->kind, "snippet" => array("textDisplay" =>$translated_message_cmt_detla , "likeCount" => $array_cmt->snippet->topLevelComment->snippet->likeCount, "publishedAt" => $array_cmt->snippet->topLevelComment->snippet->publishedAt))));
                                                    array_push($new_data_cmt, $new_data2_cmt);
                                                    array_push($new_data_cmt_tr, $new_data2_cmt_tr);
                                                    $total_char_cmt_delta+=strlen($translated_message_cmt_detla);
                                                    $newcomment_delat++;
                                                }
                                            }
                                            $total_char_cmt_combine_delta+=$total_char_cmt;
                                            $newcomment_combine_delta+=$newcomment_delat;
                                            if ($created_time_2_cmt > $last_comment_created_time) {
                                                $data_array_cmt_delta = array(
                                                    'client_id' => $client_id,
                                                    'post_id' => $videoid,
                                                    'comment' => json_encode(array("items" => $new_data_cmt)),
                                                    'social_media_id' => '4',
                                                    'last_comment_created_time' => $created_time_cmts_delta,
                                                    'total_count' => 0,
                                                    'comment_en' => json_encode(array("items" => $new_data_cmt_tr)),
                                                    'inserted_time' => date('Y-m-d H:i:s')
                                                );
                                                $this->db->insert('client_comments', $data_array_cmt_delta);
                                                echo'insert Delta Comment';
                                                echo '<br>';
                                                echo '<br>';
                                                //print_r(json_encode(array("items" => $new_data_cmt)));  
                                            }
                                        } else {
                                            echo '<br>';
                                            echo '<br>';
                                            echo 'comments insert';
                                            $data_translate_cmt_tr = array();
                                            $newcomment=0;
                                            $total_char_cmt=0;
                                            foreach ($result_cmt->items as $array_cmt) {
                                                $translated_message_cmt=$this->language_translate(str_replace('\ufeff', '', htmlspecialchars($array_cmt->snippet->topLevelComment->snippet->textDisplay)),$source_lang);
                                                $data_translate_cmt2_tr = array('snippet' => array("videoId" => $array_cmt->snippet->videoId, "topLevelComment" => array("kind" => $array_cmt->snippet->topLevelComment->kind, "snippet" => array("textDisplay" => $translated_message_cmt, "likeCount" => $array_cmt->snippet->topLevelComment->snippet->likeCount, "publishedAt" => $array_cmt->snippet->topLevelComment->snippet->publishedAt))));
                                                array_push($data_translate_cmt_tr, $data_translate_cmt2_tr);
                                                $total_char_cmt+=strlen($translated_message_cmt);
                                                $newcomment++;
                                            }
                                            $data_array_cmt = array(
                                                'client_id' => $client_id,
                                                'post_id' => $videoid,
                                                'comment' => $data_comment,
                                                'social_media_id' => '4',
                                                'last_comment_created_time' => $created_time_cmts_delta,
                                                'total_count' => 0,
                                                'comment_en' => json_encode(array("items" => $data_translate_cmt_tr)),
                                                'inserted_time' => date('Y-m-d H:i:s')
                                            );
                                            echo'<br>';
                                            echo'<br>';
                                            // print_r($data_array_cmt);
                                            echo'<br>';
                                            echo'<br>';
                                            $this->db->insert('client_comments', $data_array_cmt);
                                            $newcomment_combine+=$newcomment;
                                            $total_char_cmt_combine+=$total_char_cmt;
                                        }
                                    }
                                }
                            }     //print_r($data_comment);
                        }
                        $first_name = $value['first_name'];
                        $last_name = $value['last_name'];
                        $total_post = $newpost_delta_combine+$newpost_combine;
                        $total_comment = ($newcomment_combine_delta)+($newcomment_combine);
                        $youtubeArr[$multi] = array(
                            'artistname' => $first_name . ' ' . $last_name,
                            'total_new_post' => $total_post,
                            'total_new_comment' => $total_comment,
                        );
                        $feed_history_detail[$multi] = array(
                            'client_id' => $client_id,
                            'post_count' => $newpost_delta_combine+$newpost_combine,
                            'comment_count' => ($newcomment_combine_delta)+($newcomment_combine),
                        );
                        $feed_translated_char+=($total_char_cmt_combine + $total_char_feed_combine+$total_char_feed_delta_combine+$total_char_cmt_combine_delta);

                        $multi++;

                    }
                }
            }//channel id check
        }
        $data_array_feed_history = array(
            'feed_history_detail' => json_encode($feed_history_detail),
            'feed_translated_character_count' => $feed_translated_char,
            'inserted_time' => date("Y-m-d H:i:s"),
            'social_media_id' => '4'
        );

        $this->db->insert('feed_history', $data_array_feed_history);
        // print_r($youtubeArr);
        return ($youtubeArr);
    }

    public function youtube_page_about() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $keyApi = 'AIzaSyDXZg2w8TQG93xmt8UGuvactJeC1l1ZzbQ';

        $query = $this->db->query("SELECT c.id,smc.alias FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=4 AND smc.alias!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $youtube_forUsername = $value['alias'];
            $channels_url = "https://www.googleapis.com/youtube/v3/channels?part=snippet,contentDetails,statistics&forUsername=$youtube_forUsername&key=$keyApi";
            $data = $this->fetchData($channels_url);
            $result_json = json_decode($data);
            if(isset($result_json->items[0])){
                $likes = $result_json->items[0]->statistics->subscriberCount;
                $query_cf = $this->db->query("SELECT * FROM `client_analytics_feed` WHERE client_id=$client_id AND social_media_id=4   AND  `social_api_id`=4  ORDER BY `id` DESC");
                if ($query_cf->num_rows() > 0) {
                    $row_client_info = $query_cf->row();
                    $likesdb = $row_client_info->likes;
                    if ($likes > $likesdb) {

                        $data_array = array(
                            'client_id' => $client_id,
                            'likes' => $likes,
                            //'people' => $talking_about_count,
                            'social_media_id' => 4,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feeds' => $data,
                            'social_api_id' => 4
                        );
                        $this->db->insert('client_analytics_feed', $data_array);
                        echo 'Found';
                    }
                } else {
                    $data_array = array(
                        'client_id' => $client_id,
                        'likes' => $likes,
                        //'people' => $talking_about_count,
                        'social_media_id' => 4,
                        'inserted_time' => date('Y-m-d H:i:s'),
                        'feeds' => $data,
                        'social_api_id' => 4
                    );
                    $this->db->insert('client_analytics_feed', $data_array);
                    echo 'Insert First Time';
                    echo '<br>';
                }
            }
        }
    }

    public function fetchData($url) {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($c);
        curl_close($c);
        return $data;
    }

    public function sent_email($email, $message, $subject) {

        $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = 'roomexploreruk@gmail.com';
        $config['smtp_pass'] = 'Trekkie2007';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to
        $this->email->initialize($config);
        $this->email->from('roomexploreruk@gmail.com', 'Touchbase');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
    }
    // Instagram recursive function
    public function getPost($url, $i) {
        static $posts = array();
        $json = file_get_contents($url);
        $data = json_decode($json);
        $ins_links = array();
        $page = $data->pagination;
        $pagearray = json_decode(json_encode($page), true);
        $pagecount = count($pagearray);

        foreach ($data->data as $user_data) {
            $posts[$i++] = $user_data;
        }

        if ($pagecount > 0)
            return $this->getPost($page->next_url, $i);
        else
            return $posts;
    }
    // INstagram api
    public function test()
    {
        $access_token = INSTAGRAM_API_TOEKN;
        $search_url = "https://api.instagram.com/v1/users/search?q=qamashkraft&access_token=3437623201.94d60ad.1ed364e912684fc48c2f2c43763fa817";
            $data = $this->fetchData($search_url);
            $result = json_decode($data);
    }
    public function instagram_api() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $this->load->database();
        $this->db->reconnect();
        $feed_translated_char = 0;
        $feed_history_detail = array();
        $multi = 0;
        $access_token = INSTAGRAM_API_TOEKN;
        $query = $this->db->query("SELECT c.id,smc.alias,cc.lang_code,smc.access_token FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id LEFT OUTER JOIN country AS cc ON cc.id=c.location WHERE  smc.social_media_id=2 AND smc.alias!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $instagram_Username = $value['alias'];
            $source_lang=$value['lang_code'];       
            $newpost_combine=0;
            $newpost_delta_combine=0;
            $total_char_feed_delta_combine=0;
            $total_char_feed_combine=0;
            if(isset($value['access_token'])){
                if(!empty($value['access_token'])){
                    $access_token=$value['access_token'];  
                }
            }
            $search_url = "https://api.instagram.com/v1/users/search?q=$instagram_Username&access_token=$access_token";
            $data = $this->fetchData($search_url);
            $result = json_decode($data);
            if(isset($result->data[0]->id)){
                $users_media_id = $result->data[0]->id;
                //$posts=$this->getPost("https://api.instagram.com/v1/users/$users_media_id/media/recent?access_token=$access_token&count=33",0);
                // $data_media=json_encode(array("data"=>$posts));
                //$result_media=json_decode($data_media);
                
                $media_url = "https://api.instagram.com/v1/users/$users_media_id/media/recent?access_token=$access_token&count=33";
                $data_media = $this->fetchData($media_url);
                $result_media = json_decode($data_media);
                if(isset($result_media->data[0])){
                    $created_time = date('Y-m-d H:i:s', $result_media->data[0]->created_time);
                    $query_cf = $this->db->query("SELECT * FROM `client_feed` WHERE client_id=$client_id AND social_media_id=2 ORDER BY `last_post_created_time` DESC");
                    if ($query_cf->num_rows() > 0) {
                        $row = $query_cf->row();
                        $last_post_created_time = $row->last_post_created_time;
                        $date_2 = date_create($created_time);
                        $created_time_2 = date_format($date_2, "Y-m-d H:i:s");
                        $new_data = array();
                        $data_translate_tr = array();
                        $newpost = 0;
                        $newpost_delta=0;
                        $total_char_feed_delta=0;
                        if(isset($result_media->data))
                        {
                            foreach ($result_media->data as $array) {
                                // Check feed for delta insertion
                                if (isset($array->created_time)) {
                                    $date_3 = date_create(date('Y-m-d H:i:s', $array->created_time));
                                    if (date_format($date_3, "Y-m-d H:i:s") > $last_post_created_time) {
                                        if(isset($array->caption->text)){
                                            $translated_message_detla=$this->language_translate($array->caption->text,$source_lang);   
                                        }  else {
                                            $translated_message_detla='';
                                        }
                                        if(isset($array->caption->text,$array->caption->created_time)){
                                            $new_data2 = array('caption' => array("created_time" => $array->caption->created_time, "text" => $array->caption->text), "id" => $array->id);


                                            $new_data2_tr = array('caption' => array("created_time" => $array->caption->created_time, "text" =>$translated_message_detla), "id" => $array->id);
                                            array_push($new_data, $new_data2);
                                            array_push($data_translate_tr, $new_data2_tr);
                                            $total_char_feed_delta+=strlen($translated_message_detla);
                                            $newpost_delta++;
                                        }
                                    }
                                }
                            }
                        }
                        $newpost_delta_combine+=$newpost_delta;
                        $total_char_feed_delta_combine+=$total_char_feed_delta;
                        if ($created_time_2 > $last_post_created_time) {
                            $data_array_2 = array(
                                'client_id' => $client_id,
                                'feed' => json_encode(array("data" => $new_data)),
                                'social_media_id' => 2,
                                'last_post_created_time' => $created_time,
                                'inserted_time' => date('Y-m-d H:i:s'),
                                'feed_en' => json_encode(array("data" => $data_translate_tr)),
                            );

                            $this->db->insert('client_feed', $data_array_2);
                            echo'insert Delta';
                            echo '<br>';
                            echo '<br>';
                        }
                    } else {

                        echo'Insertes First Time feed';
                        echo '<br>';
                        echo '<br>';
                        $data_translate = array();
                        $total_char_feed=0;
                        $newpost=0;
                        if(isset($result_media->data))
                        {
                        foreach ($result_media->data as $array) {
                            if(isset($array->caption->text)){
                                $translated_message=$this->language_translate($array->caption->text,$source_lang); 
                            }  else {
                                $translated_message=''; 
                            }
                            if(isset($array->caption->created_time))
                            {
                                $data_translate2 = array('caption' => array("created_time" => $array->caption->created_time, "text" => $translated_message), "id" => $array->id);
                                array_push($data_translate, $data_translate2);
                                $total_char_feed+=strlen($translated_message);
                                $newpost++;
                            }
                        }
                        }
                        $data_array = array(
                            'client_id' => $client_id,
                            'feed' => $data_media,
                            'social_media_id' => 2,
                            'last_post_created_time' => $created_time,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feed_en' => json_encode(array("data" => $data_translate)),
                        );
                        $this->db->insert('client_feed', $data_array);
                        $newpost_combine+=$newpost;
                        $total_char_feed_combine+=$total_char_feed;
                    }
                    $newcomment_combine_delta=0;
                    $total_char_cmt_combine_delta=0;
                    $total_char_cmt_combine=0;
                    $newcomment_combine=0;
                    foreach ($result_media->data as $med) {
                        $media_id = $med->id;
                        $comment_url = "https://api.instagram.com/v1/media/$media_id/comments?access_token=$access_token";                      
                        $data_comment = $this->fetchData($comment_url);
                        $result_cmt = json_decode($data_comment);
                        
                        if (isset($result_cmt->data[0])) {
                           $result_cmt=array_reverse($result_cmt->data);
                           //echo '<pre>';
                           //print_r($result_cmt);
                              $created_time_cmts_delta = $result_cmt[0]->created_time;
                            //echo '<br>';
                            $query_client_comments = $this->db->query("SELECT * FROM `client_comments` WHERE 1 AND post_id='$media_id'  AND social_media_id=2  AND client_id=$client_id ORDER BY `last_comment_created_time` DESC");
                            if ($query_client_comments->num_rows() > 0) {
                                $row = $query_client_comments->row();
                                $last_comment_created_time = $row->last_comment_created_time;
                                $date_2_cmt = date_create(date('Y-m-d H:i:s', $created_time_cmts_delta));
                                $created_time_2_cmt = date_format($date_2_cmt, "Y-m-d H:i:s");
                                $new_data_cmt = array();
                                $data_translate_cmt = array();
                                $total_char_cmt_delta = 0;
                                $newcomment_delat = 0;
                                foreach ($result_cmt as $array_cmt) {
                                    // Check comments for delta insertion
                                    //echo $array_cmt->created_time.'<br>';
                                    $date_3_cmt = date_create(date('Y-m-d H:i:s', $array_cmt->created_time));
                                    if (date_format($date_3_cmt, "Y-m-d H:i:s") > $last_comment_created_time) {
                                        $new_data2_cmt = array('created_time' => $array_cmt->created_time, 'text' => $array_cmt->text);
                                        $translated_message_cmt_detla=$this->language_translate($array_cmt->text,$source_lang);
                                        $data_translate2_cmt_tr = array('created_time' => $array_cmt->created_time, 'text' =>$translated_message_cmt_detla );
                                        array_push($new_data_cmt, $new_data2_cmt);
                                        array_push($data_translate_cmt, $data_translate2_cmt_tr);
                                        $total_char_cmt_delta+=strlen($translated_message_cmt_detla);
                                        $newcomment_delat++;
                                    }
                                }
                                $total_char_cmt_combine_delta+=$total_char_cmt_delta;
                                $newcomment_combine_delta+=$newcomment_delat;
                                if ($created_time_2_cmt > $last_comment_created_time) {

                                    $data_array_cmt_delta = array(
                                        'client_id' => $client_id,
                                        'post_id' => $media_id,
                                        'comment' => json_encode(array("data" => $new_data_cmt)),
                                        'social_media_id' => '2',
                                        'last_comment_created_time' => date('Y-m-d H:i:s', $created_time_cmts_delta),
                                        'total_count' => 0,
                                        'comment_en' => json_encode(array("data" => $data_translate_cmt)),
                                        'inserted_time' => date('Y-m-d H:i:s')
                                    );
                                    $this->db->insert('client_comments', $data_array_cmt_delta);
                                    echo'insert Delta Comment';
                                    echo '<br>';
                                    echo '<br>';
                                    //print_r(json_encode(array("items" => $new_data_cmt)));  
                                }
                            } else {

                                echo '<br>';
                                echo '<br>';
                                echo 'Insertes First Time comments';
                                $data_translate_cmt = array();
                                $newcomment=0;
                                $total_char_cmt=0;
                                foreach ($result_cmt as $array_cmt) {
                                    $translated_message_cmt=$this->language_translate($array_cmt->text,$source_lang);
                                    $new_data2_cmt = array('created_time' => $array_cmt->created_time, 'text' =>$translated_message_cmt );
                                    array_push($data_translate_cmt, $new_data2_cmt);
                                    $total_char_cmt+=strlen($translated_message_cmt);
                                    $newcomment++;
                                }
                                $data_array_cmt = array(
                                    'client_id' => $client_id,
                                    'post_id' => $media_id,
                                    'comment' => $data_comment,
                                    'social_media_id' => '2',
                                    'last_comment_created_time' => date('Y-m-d H:i:s', $created_time_cmts_delta),
                                    'total_count' => 0,
                                    'comment_en' => json_encode(array("data" => $data_translate_cmt)),
                                    'inserted_time' => date('Y-m-d H:i:s')
                                );
                                $this->db->insert('client_comments', $data_array_cmt);
                                $newcomment_combine+=$newcomment;
                                $total_char_cmt_combine+=$total_char_cmt;
                            }
                        }
                    }
                    $feed_history_detail[$multi] = array(
                        'client_id' => $client_id,
                        'post_count' => $newpost_delta_combine+$newpost_combine,
                        'comment_count' => ($newcomment_combine_delta)+($newcomment_combine),
                    );
                    $feed_translated_char+=($total_char_cmt_combine + $total_char_feed_combine+$total_char_feed_delta_combine+$total_char_cmt_combine_delta);
                    $multi++;


                   

                }//check is data is not empty

            }//check users_media_id 
        }
         $data_array_feed_history = array(
            'feed_history_detail' => json_encode($feed_history_detail),
            'feed_translated_character_count' => $feed_translated_char,
            'inserted_time' => date("Y-m-d H:i:s"),
            'social_media_id' => '2'
        );

        $this->db->insert('feed_history', $data_array_feed_history);
        
    }
    // Get information of instagram page
    public function instagram_page_about() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $access_token = INSTAGRAM_API_TOEKN;
        $query = $this->db->query("SELECT c.id,smc.alias,smc.access_token FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=2 AND smc.alias!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $instagram_Username = $value['alias']; 
            if(isset($value['access_token'])){
                if(!empty($value['access_token'])){
                    $access_token=$value['access_token'];   
                }
            }

            $search_url = "https://api.instagram.com/v1/users/search?q=$instagram_Username&access_token=$access_token";
            $data = $this->fetchData($search_url);
            $resultjs = json_decode($data);
            if(isset($resultjs->data[0]->id)){
                $users_media_id = $resultjs->data[0]->id;
                $media_url = "https://api.instagram.com/v1/users/$users_media_id?access_token=$access_token&count=33";
                $data_media = $this->fetchData($media_url);
                $result_media = json_decode($data_media);
                $likes = $result_media->data->counts->followed_by;
                $query_cf = $this->db->query("SELECT * FROM `client_analytics_feed` WHERE client_id=$client_id AND social_media_id=2 AND  `social_api_id`=3  ORDER BY `id` DESC");
                if ($query_cf->num_rows() > 0) {
                    $row_client_info = $query_cf->row();
                    $likesdb = $row_client_info->likes;
                    if ($likes > $likesdb) {

                        $data_array = array(
                            'client_id' => $client_id,
                            'likes' => $likes,
                            //'people' => $talking_about_count,
                            'social_media_id' => 2,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feeds' => $data_media,
                            'social_api_id' => 3
                        );
                        $this->db->insert('client_analytics_feed', $data_array);
                        echo 'Found';
                    }
                } else {
                    $data_array = array(
                        'client_id' => $client_id,
                        'likes' => $likes,
                        //'people' => $talking_about_count,
                        'social_media_id' => 2,
                        'inserted_time' => date('Y-m-d H:i:s'),
                        'feeds' => $data_media,
                        'social_api_id' => 3
                    );
                    $this->db->insert('client_analytics_feed', $data_array);
                    echo 'Insert First Time';
                    echo '<br>';
                }
            }
        }
    }
    // Translate feed/comment
    public function language_translate($text,$source_lang) {
        $key = "AIzaSyAOm6Qtl_Uo6jg5kysa_YEIWTGVyQ8q-Fo";
        //$apiData = array('key' => $key,'source'=>$source_lang, 'target' => 'en', 'q' => $text);
        $apiData = array('key' => $key, 'target' => 'en', 'q' => $text);//
        $apiHost = 'https://www.googleapis.com/language/translate/v2';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiHost);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($apiData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-HTTP-Method-Override: GET'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        $jsonData = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($jsonData);
        if(isset($json->data->translations[0]->translatedText)){
            $translated_text = $json->data->translations[0]->translatedText;  
        }else{
            $translated_text=$text;
        }

        return $translated_text;
    }

    public function language_translate_bk($text) {
        $key = "AIzaSyAOm6Qtl_Uo6jg5kysa_YEIWTGVyQ8q-Fo";
        $add = "Sometimes it's hard to do the right thing when the pressures coming down like lightning";
        $url = "https://www.googleapis.com/language/translate/v2?key=$key&target=en&q=" . urlencode($text);
        // $json =json_decode(file_get_contents($url));
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, 'https://www.googleapis.com/language/translate/v2');
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($handle, CURLOPT_SAFE_UPLOAD, false);
        curl_setopt($handle, CURLOPT_POSTFIELDS, array('key' => $key, 'target' => 'en', 'q' => $text));
        curl_setopt($handle, CURLOPT_HTTPHEADER, array('X-HTTP-Method-Override: GET'));
        $response = curl_exec($handle);
        $json = json_decode($response);
        // print_r($this->fetchData($url));
        //print_r($json);
        $translated_text = $json->data->translations[0]->translatedText;
        return $translated_text;
        // echo $translated_text;
    }

    public function translation_feeds() {
        $query = $this->db->query("SELECT c.id,smc.alias,c.first_name,c.last_name FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=1 AND smc.alias!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $query_cf = $this->db->query("SELECT * FROM `client_feed` WHERE client_id=$client_id AND social_media_id=1 ORDER BY `last_post_created_time` DESC");
            if ($query_cf->num_rows() > 0) {
                $row_result = $query_cf->result_array();
                foreach ($row_result as $key_f => $value_f) {
                    $new_data_tr = array();
                    $fbfeed = $value_f['feed'];
                    $id = $value_f['id'];
                    $data_feed = json_decode($fbfeed);
                    foreach ($data_feed->data as $array) {
                        $new_data2_tr = array('message' => $this->language_translate($array->message), 'created_time' => $array->created_time, 'id' => $array->id);
                        array_push($new_data_tr, $new_data2_tr);
                    }
                    $data_array_2 = array(
                        'feed_en' => json_encode(array("data" => $new_data_tr))
                    );
                    $array_w = array('client_id' => $client_id, 'social_media_id' => '1', 'id' => $id);
                    $this->db->where($array_w);
                    $this->db->update('client_feed', $data_array_2);
                }
            }
        }
    }

    public function translation_comments() {
        $query = $this->db->query("SELECT c.id,smc.alias,c.first_name,c.last_name FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=1 AND smc.alias!='' AND c.id=10");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $query_client_comments = $this->db->query("SELECT * FROM `client_comments` WHERE client_id=$client_id AND social_media_id=1");
            if ($query_client_comments->num_rows() > 0) {
                $row_result = $query_client_comments->result_array();
                foreach ($row_result as $key_f => $value_f) {
                    $data_translate_cmt_delta = array();
                    $fbfeed = $value_f['comment'];
                    $id = $value_f['id'];
                    $data_feed = json_decode($fbfeed);
                    foreach ($data_feed->data as $array_val_cmt_delta) {
                        if (isset($array_val_cmt_delta->message)) {
                            $data_translate2_cmt_delta = array('created_time' => $array_val_cmt_delta->created_time, 'message' => $this->language_translate($array_val_cmt_delta->message), 'id' => $array_val_cmt_delta->id);
                            array_push($data_translate_cmt_delta, $data_translate2_cmt_delta);
                        }
                    }
                    $data_array_2 = array(
                        'comment_en' => json_encode(array("data" => $data_translate_cmt_delta))
                    );
                    $array_w = array('client_id' => $client_id, 'social_media_id' => '1', 'id' => $id);
                    $this->db->where($array_w);
                    $this->db->update('client_comments', $data_array_2);
                }
            }
        }
    }

    public function translation_feeds_youtube() {
        $query = $this->db->query("SELECT c.id,smc.alias,c.first_name,c.last_name FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=4 AND smc.alias!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $query_cf = $this->db->query("SELECT * FROM `client_feed` WHERE client_id=$client_id AND social_media_id=4");
            if ($query_cf->num_rows() > 0) {
                $row_result = $query_cf->result_array();
                foreach ($row_result as $key_f => $value_f) {
                    $new_data_tr = array();
                    $fbfeed = $value_f['feed'];
                    $id = $value_f['id'];
                    $data_feed = json_decode($fbfeed);
                    foreach ($data_feed->items as $array) {
                        $new_data2_tr = array('id' => array('kind' => $array->id->kind, 'videoId' => $array->id->videoId), 'snippet' => array("publishedAt" => $array->snippet->publishedAt, "title" => $this->language_translate($array->snippet->title), "description" => $this->language_translate($array->snippet->description)), 'id' => $array->id);
                        array_push($new_data_tr, $new_data2_tr);
                    }
                    $data_array_2 = array(
                        'feed_en' => json_encode(array("items" => $new_data_tr))
                    );
                    $array_w = array('client_id' => $client_id, 'social_media_id' => '4', 'id' => $id);
                    $this->db->where($array_w);
                    $this->db->update('client_feed', $data_array_2);
                }
            }
        }
    }

    public function translation_comments_youtube() {
        $query = $this->db->query("SELECT c.id,smc.alias,c.first_name,c.last_name FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=1 AND smc.alias!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $query_client_comments = $this->db->query("SELECT * FROM `client_comments` WHERE client_id=$client_id AND social_media_id=4");
            if ($query_client_comments->num_rows() > 0) {
                $row_result = $query_client_comments->result_array();
                foreach ($row_result as $key_f => $value_f) {
                    $new_data_cmt_tr = array();
                    $fbfeed = $value_f['comment'];
                    $id = $value_f['id'];
                    $data_feed = json_decode($fbfeed);
                    foreach ($data_feed->items as $array_cmt) {
                        $new_data2_cmt_tr = array('snippet' => array("videoId" => $array_cmt->snippet->videoId, "topLevelComment" => array("kind" => $array_cmt->snippet->topLevelComment->kind, "snippet" => array("textDisplay" => $this->language_translate(str_replace('\ufeff', '', htmlspecialchars($array_cmt->snippet->topLevelComment->snippet->textDisplay))), "likeCount" => $array_cmt->snippet->topLevelComment->snippet->likeCount, "publishedAt" => $array_cmt->snippet->topLevelComment->snippet->publishedAt))));
                        array_push($new_data_cmt_tr, $new_data2_cmt_tr);
                    }
                    $data_array_2 = array(
                        'comment_en' => json_encode(array("items" => $new_data_cmt_tr))
                    );
                    $array_w = array('client_id' => $client_id, 'social_media_id' => '4', 'id' => $id);
                    $this->db->where($array_w);
                    $this->db->update('client_comments', $data_array_2);
                }
            }
        }
    }

    public function translation_feeds_instagram() {
        $query = $this->db->query("SELECT c.id,smc.alias,c.first_name,c.last_name FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=2 AND smc.alias!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $query_cf = $this->db->query("SELECT * FROM `client_feed` WHERE client_id=$client_id AND social_media_id=2");
            if ($query_cf->num_rows() > 0) {
                $row_result = $query_cf->result_array();
                foreach ($row_result as $key_f => $value_f) {
                    $data_translate_tr = array();
                    $fbfeed = $value_f['feed'];
                    $id = $value_f['id'];
                    $data_feed = json_decode($fbfeed);
                    foreach ($data_feed->data as $array) {
                        $new_data2_tr = array('caption' => array("created_time" => $array->caption->created_time, "text" => $this->language_translate($array->caption->text)), "id" => $array->id);
                        array_push($data_translate_tr, $new_data2_tr);
                    }
                    $data_array_2 = array(
                        'feed_en' => json_encode(array("data" => $data_translate_tr)),
                    );
                    $array_w = array('client_id' => $client_id, 'social_media_id' => '2', 'id' => $id);
                    $this->db->where($array_w);
                    $this->db->update('client_feed', $data_array_2);
                }
            }
        }
    }

    public function translation_comments_instagram() {
        $query = $this->db->query("SELECT c.id,smc.alias,c.first_name,c.last_name FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=2 AND smc.alias!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $query_client_comments = $this->db->query("SELECT * FROM `client_comments` WHERE client_id=$client_id AND social_media_id=2");
            if ($query_client_comments->num_rows() > 0) {
                $row_result = $query_client_comments->result_array();
                foreach ($row_result as $key_f => $value_f) {
                    $data_translate_cmt = array();
                    $fbfeed = $value_f['comment'];
                    $id = $value_f['id'];
                    $data_feed = json_decode($fbfeed);
                    foreach ($data_feed->data as $array_cmt) {
                        $data_translate2_cmt_tr = array('created_time' => $array_cmt->created_time, 'text' => $this->language_translate($array_cmt->text));
                        array_push($data_translate_cmt, $data_translate2_cmt_tr);
                    }
                    $data_array_2 = array(
                        'comment_en' => json_encode(array("data" => $data_translate_cmt)),
                    );
                    $array_w = array('client_id' => $client_id, 'social_media_id' => '2', 'id' => $id);
                    $this->db->where($array_w);
                    $this->db->update('client_comments', $data_array_2);
                }
            }
        }
    }

    public function clean($string) {
        //$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        //return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $string);
        ; // Removes special chars.
    }

    public function removeEmoji($text) {

        $clean_text = "";

        // Match Emoticons
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clean_text = preg_replace($regexEmoticons, '', $text);

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clean_text = preg_replace($regexSymbols, '', $clean_text);

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clean_text = preg_replace($regexTransport, '', $clean_text);

        // Match Miscellaneous Symbols
        $regexMisc = '/[\x{2600}-\x{26FF}]/u';
        $clean_text = preg_replace($regexMisc, '', $clean_text);

        // Match Dingbats
        $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
        $clean_text = preg_replace($regexDingbats, '', $clean_text);

        return $clean_text;
    }
    public function facebook_page_fans_country() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $date=date("y-m-d");
        $since=strtotime($date);
        $query = $this->db->query("SELECT c.id,smc.alias FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=1 AND smc.alias!=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $fbalias = $value['alias'];
            $feed_url = "https://graph.facebook.com/$fbalias/insights/page_fans_country?access_token=".FACEBOOK_API_TOEKN."&since=$since";
            $data=$this->fetchData($feed_url); 
            $result_page_fans_country_fb=json_decode($data);
            //print_r($data);
            $query_cf = $this->db->query("SELECT * FROM `client_analytics_feed` WHERE client_id=$client_id AND social_media_id=1  AND `social_api_id`=2 ORDER BY `id` DESC");
            if ($query_cf->num_rows() > 0) {
                $row=$query_cf->row();
                $id=$row->id;
                $data_array = array(
                    'client_id' => $client_id,
                    'social_media_id' => 1,
                    'inserted_time' => date('Y-m-d H:i:s'),
                    'feeds' => $data,
                    'social_api_id' => 2
                );
                if(isset($result_page_fans_country_fb->data[0]->values[0]->value)){
                    $this->db->where('id', $id);
                    $this->db->update('client_analytics_feed', $data_array);
                }

            } else {
                $data_array = array(
                    'client_id' => $client_id,
                    'social_media_id' => 1,
                    'inserted_time' => date('Y-m-d H:i:s'),
                    'feeds' => $data,
                    'social_api_id' => 2
                );
                if(isset($result_page_fans_country_fb->data[0]->values[0]->value)){
                    $this->db->insert('client_analytics_feed', $data_array);
                    echo 'Insert First';
                }
            }
        }
    }
    public function facebook_page_fans_gender_age() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $date=date("y-m-d");
        $since=strtotime($date);
        $query = $this->db->query("SELECT c.id,smc.alias,smc.access_token FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=1 AND smc.alias!='' AND smc.access_token !=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $fbalias = $value['alias'];
            $access_token=$value['access_token'];
            //$feed_url = "https://graph.facebook.com/v2.6/$fbalias/insights/page_fans_gender_age?access_token=$access_token&since=$since";
            $feed_url = "https://graph.facebook.com/v2.6/$fbalias/insights/page_fans_gender_age?access_token=$access_token";
            $data=$this->fetchData($feed_url);
            $result_feed=  json_decode($data);
            if(isset( $result_feed->data)){
                if(count($result_feed->data)>0){
                    $query_cf = $this->db->query("SELECT * FROM `client_analytics_feed` WHERE client_id=$client_id AND social_media_id=1  AND `social_api_id`=7 ORDER BY `id` DESC");
                    if ($query_cf->num_rows() > 0) {
                        $row=$query_cf->row();
                        $id=$row->id;
                        $data_array = array(
                            'client_id' => $client_id,
                            'social_media_id' => 1,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feeds' => $data,
                            'social_api_id' => 7
                        );
                        $this->db->where('id', $id);
                        $this->db->update('client_analytics_feed', $data_array);
                    } else {
                        $data_array = array(
                            'client_id' => $client_id,
                            'social_media_id' => 1,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feeds' => $data,
                            'social_api_id' => 7
                        );
                        $this->db->insert('client_analytics_feed', $data_array);
                        echo 'Insert First';
                    }
                }

            }}

    }
    public function facebook_page_posts_impressions() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $date=date("y-m-d");
        $since=strtotime($date);
        $query = $this->db->query("SELECT c.id,smc.alias,smc.access_token FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=1 AND smc.alias!='' AND smc.access_token !=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $fbalias = $value['alias'];
            $access_token=$value['access_token'];
            $date_analysis=date('Y-m-d', strtotime(' +1 day'));
            $until=strtotime($date_analysis);
            $no_days = 7; // No of days 
            $date_currrent=date('Y-m-d');
            $bdate = strtotime("-" . $no_days . " days", strtotime($date_currrent));
            $days7off = date("Y-m-d", $bdate);
            $since=strtotime($days7off);
            $feed_url = "https://graph.facebook.com/v2.6/$fbalias/insights/page_posts_impressions?access_token=$access_token&since=$since&until=$until";
            $data=$this->fetchData($feed_url); 
            $result_feed=  json_decode($data);
            if(isset( $result_feed->data)){
                if(count($result_feed->data)>0){
                    $query_cf = $this->db->query("SELECT * FROM `client_analytics_feed` WHERE client_id=$client_id AND social_media_id=1  AND `social_api_id`=8 ORDER BY `id` DESC");
                    if ($query_cf->num_rows() > 0) {
                        $row=$query_cf->row();
                        $id=$row->id;
                        $data_array = array(
                            'client_id' => $client_id,
                            'social_media_id' =>1,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feeds' => $data,
                            'social_api_id' => 8
                        );
                        $this->db->where('id', $id);
                        $this->db->update('client_analytics_feed', $data_array);
                    } else {
                        $data_array = array(
                            'client_id' => $client_id,
                            'social_media_id' =>1,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feeds' => $data,
                            'social_api_id' =>8
                        );
                        $this->db->insert('client_analytics_feed', $data_array);
                        echo 'Insert First';
                    }
                }

            }
        }   
    }
    public function facebook_page_engaged_users() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $date=date("y-m-d");
        $since=strtotime($date);
        $query = $this->db->query("SELECT c.id,smc.alias,smc.access_token FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=1 AND smc.alias!='' AND smc.access_token !=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $fbalias = $value['alias'];
            $access_token=$value['access_token'];
            $date_analysis=date('Y-m-d', strtotime(' +1 day'));
            $until=strtotime($date_analysis);
            $no_days = 7; // No of days 
            $date_currrent=date('Y-m-d');
            $bdate = strtotime("-" . $no_days . " days", strtotime($date_currrent));
            $days7off = date("Y-m-d", $bdate);
            $since=strtotime($days7off);
            $feed_url = "https://graph.facebook.com/v2.6/$fbalias/insights/page_engaged_users?access_token=$access_token&since=$since&until=$until";
            $data=$this->fetchData($feed_url); 
            $result_feed=  json_decode($data);
            if(isset( $result_feed->data)){
                if(count($result_feed->data)>0){
                    $query_cf = $this->db->query("SELECT * FROM `client_analytics_feed` WHERE client_id=$client_id AND social_media_id=1  AND `social_api_id`=9 ORDER BY `id` DESC");
                    if ($query_cf->num_rows() > 0) {
                        $row=$query_cf->row();
                        $id=$row->id;
                        $data_array = array(
                            'client_id' => $client_id,
                            'social_media_id' =>1,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feeds' => $data,
                            'social_api_id' => 9
                        );
                        $this->db->where('id', $id);
                        $this->db->update('client_analytics_feed', $data_array);
                    } else {
                        $data_array = array(
                            'client_id' => $client_id,
                            'social_media_id' =>1,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feeds' => $data,
                            'social_api_id' =>9
                        );
                        $this->db->insert('client_analytics_feed', $data_array);
                        echo 'Insert First';
                    }
                } 
            }

        }
    }
    public function facebook_page_views_total() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $date=date("y-m-d");
        $since=strtotime($date);
        $query = $this->db->query("SELECT c.id,smc.alias,smc.access_token FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=1 AND smc.alias!='' AND smc.access_token !=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $fbalias = $value['alias'];
            $access_token=$value['access_token'];
            $date_analysis=date('Y-m-d', strtotime(' +1 day'));
            $until=strtotime($date_analysis);
            $no_days = 7; // No of days 
            $date_currrent=date('Y-m-d');
            $bdate = strtotime("-" . $no_days . " days", strtotime($date_currrent));
            $days7off = date("Y-m-d", $bdate);
            $since=strtotime($days7off);
            $feed_url = "https://graph.facebook.com/v2.6/$fbalias/insights/page_views_total?access_token=$access_token&since=$since&until=$until";
            $data=$this->fetchData($feed_url); 
            $result_feed=  json_decode($data);
            if(isset( $result_feed->data)){
                if(count($result_feed->data)>0){
                    $query_cf = $this->db->query("SELECT * FROM `client_analytics_feed` WHERE client_id=$client_id AND social_media_id=1  AND `social_api_id`=10 ORDER BY `id` DESC");
                    if ($query_cf->num_rows() > 0) {
                        $row=$query_cf->row();
                        $id=$row->id;
                        $data_array = array(
                            'client_id' => $client_id,
                            'social_media_id' =>1,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feeds' => $data,
                            'social_api_id' => 10
                        );
                        $this->db->where('id', $id);
                        $this->db->update('client_analytics_feed', $data_array);
                    } else {
                        $data_array = array(
                            'client_id' => $client_id,
                            'social_media_id' =>1,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feeds' => $data,
                            'social_api_id' =>10
                        );
                        $this->db->insert('client_analytics_feed', $data_array);
                        echo 'Insert First';
                    }
                } 
            }

        }
    }
    public function facebook_page_impressions() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $date=date("y-m-d");
        $since=strtotime($date);
        $query = $this->db->query("SELECT c.id,smc.alias,smc.access_token FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=1 AND smc.alias!='' AND smc.access_token !=''");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $fbalias = $value['alias'];
            $access_token=$value['access_token'];
            $date_analysis=date('Y-m-d', strtotime(' +1 day'));
            $until=strtotime($date_analysis);
            $no_days = 7; // No of days 
            $date_currrent=date('Y-m-d');
            $bdate = strtotime("-" . $no_days . " days", strtotime($date_currrent));
            $days7off = date("Y-m-d", $bdate);
            $since=strtotime($days7off);
            $feed_url = "https://graph.facebook.com/v2.6/$fbalias/insights/page_impressions?access_token=$access_token&since=$since&until=$until";
            $data=$this->fetchData($feed_url); 
            $result_feed=  json_decode($data);
            if(isset( $result_feed->data)){
                if(count($result_feed->data)>0){
                    $query_cf = $this->db->query("SELECT * FROM `client_analytics_feed` WHERE client_id=$client_id AND social_media_id=1  AND `social_api_id`=11 ORDER BY `id` DESC");
                    if ($query_cf->num_rows() > 0) {
                        $row=$query_cf->row();
                        $id=$row->id;
                        $data_array = array(
                            'client_id' => $client_id,
                            'social_media_id' =>1,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feeds' => $data,
                            'social_api_id' => 11
                        );
                        $this->db->where('id', $id);
                        $this->db->update('client_analytics_feed', $data_array);
                    } else {
                        $data_array = array(
                            'client_id' => $client_id,
                            'social_media_id' =>1,
                            'inserted_time' => date('Y-m-d H:i:s'),
                            'feeds' => $data,
                            'social_api_id' =>11
                        );
                        $this->db->insert('client_analytics_feed', $data_array);
                        echo 'Insert First';
                    }
                } 
            }

        }
    }
    public function instagram_api_token() {

        $this->load->library('instagram/instagram', array(
            'apiKey' => '94d60ad9c48045e5a4f6436cb2340868',
            'apiSecret' => 'ae09678fa1a6432a8faa0296edfab515',
            'apiCallback' => 'http://localhost/touchbase/api/instagram_api_token/'));
        echo "<a href='{$this->instagram->getLoginUrl(array(
            'basic',
            'likes',
            'public_content'
        ))}'>Login with Instagram</a>";
        if (isset($_GET['code'])) {
            echo $code = $_GET['code'];
            $data = $this->instagram->getOAuthToken($code);
            print_r($data);

            echo 'Your username is: ' . $data->user->username;
        }
    }
    public function loop() {
        @ini_set('max_execution_time', 123456);
        @ini_set('memory_limit', '-1');
        @set_time_limit(0);
        $keyword = 0;
        //$newpost=0;
        $new_comment = 0;      
        $html = '';
        $facebookArr = array();
        $feed_history_detail = array();
        $multi = 0;
        $feed_translated_char = 0;
        $query = $this->db->query("SELECT c.id,smc.alias,c.first_name,c.last_name FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=1 AND smc.alias!='' and c.id=5");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $fbalias = $value['alias'];

            $feed_url = "https://graph.facebook.com/$fbalias/posts?limit=3&order=reverse_chronological&access_token=".FACEBOOK_API_TOEKN;
            $data = $this->fetchData($feed_url);
            $result_cmt = json_decode($data);

            $created_time = $result_cmt->data[0]->created_time;

            // Check feeds if already exists
            $query_cf = $this->db->query("SELECT * FROM `client_feed` WHERE client_id=$client_id AND social_media_id=1 ORDER BY `last_post_created_time` DESC");
            if ($query_cf->num_rows() > 0) {
                $row = $query_cf->row();
                $last_post_created_time = $row->last_post_created_time;
                $date_2 = date_create($created_time);
                $created_time_2 = date_format($date_2, "Y-m-d H:i:s");
                $new_data = array();
                $new_data_tr = array();
                $total_char_feed = 0;
                $newpost = 0;
                // Check feed for delta insertion
                foreach ($result_cmt->data as $array) {
                    if (isset($array->message)) {
                        $date_3 = date_create($array->created_time);
                        if (date_format($date_3, "Y-m-d H:i:s") > $last_post_created_time) {                         
                            $new_data2 = array('message' => $array->message, 'created_time' => $array->created_time, 'id' => $array->id);
                            array_push($new_data, $new_data2);
                            //$newpost+=count($new_data_tr);
                            $total_char_feed+=strlen($array->message);
                            $newpost++;
                        }
                    }
                }


            } else {


            }
            //Comments get and insert 
            $result_cmts = json_decode($data);
            $newcomment_combine=0;
            $total_char_cmt_combine=0;
            foreach ($result_cmts->data as $array_cmt) {  
                echo $post_id = $array_cmt->id;
                echo 'Postid<br><br>';
                $comments_url = "https://graph.facebook.com/$post_id/comments?limit=3&order=reverse_chronological&summary=1&access_token=".FACEBOOK_API_TOEKN;
                $data_cmt = $this->fetchData($comments_url);
                $result_cf = json_decode($data_cmt);
                $total_count_comment = $result_cf->summary->total_count;
                // Check client_comments if already exists
                $query_client_comments = $this->db->query("SELECT * FROM `client_comments` WHERE 1 AND post_id='$post_id' ORDER BY `client_comments`.`total_count` DESC");
                if ($query_client_comments->num_rows() > 0) {
                    $row_client_comments = $query_client_comments->row();
                    $total_count_cmt = $row_client_comments->total_count;
                    if ($total_count_comment > $total_count_cmt) {
                        $limit = $total_count_comment - $total_count_cmt;
                        $new_comment = $limit;
                        $comments_url_delta = "https://graph.facebook.com/$post_id/comments?limit=$limit&summary=1&order=reverse_chronological&access_token=".FACEBOOK_API_TOEKN;
                        $data_cmt_delta = $this->fetchData($comments_url_delta);                      

                        $result_cf_delta = json_decode($data_cmt_delta);
                        $data_translate_cmt_delta = array();
                        $total_char_cmt = 0;
                        $newcomment = 0;
                        // Check comments for delta insertion
                        foreach ($result_cf_delta->data as $array_val_cmt_delta) {
                            if (isset($array_val_cmt_delta->message)) {
                                $translated_message_cmt_detla =$array_val_cmt_delta->message;
                                $data_translate2_cmt_delta = array('created_time' => $array_val_cmt_delta->created_time, 'message' => $translated_message_cmt_detla, 'id' => $array_val_cmt_delta->id);
                                array_push($data_translate_cmt_delta, $data_translate2_cmt_delta);                               
                                $total_char_cmt+=strlen($translated_message_cmt_detla);
                                //$newcomment+=count($data_translate_cmt_delta);
                                echo $array_val_cmt_delta->id.'comment id:<br><br>';
                                //$newcomment++;
                            }
                        }
                        // $newcomment_combine+=$newcomment;
                        //echo $newcomment.'new comment <br><br>';
                        // echo $total_char_cmt.'<br>';
                        echo  $newcomment=count($data_translate_cmt_delta).'new comment <br><br>';
                        $total_char_cmt_combine+=$total_char_cmt;
                        $newcomment_combine+=$newcomment;
                        echo $newcomment_combine.'c<br>';

                        echo 'Delta found';
                    }
                    echo 'found';
                    echo '<br>';
                    echo '<br>';
                    //}
                } else {
                    echo 'Not found';
                    echo '<br>';
                    echo '<br>';
                }
            }


            $feed_history_detail[$multi] = array(
                'client_id' => $client_id,
                'post_count' => $newpost,
                'comment_count' => $newcomment_combine,
            );
            $feed_translated_char+=($total_char_cmt_combine + $total_char_feed);

            $multi++;
            print_r(json_encode($feed_history_detail));
        }
        $data_array_feed_history = array(
            'feed_history_detail' => json_encode($feed_history_detail),
            'feed_translated_character_count' => $feed_translated_char,
            'inserted_time' => date("Y-m-d H:i:s"),
            'social_media_id' => '1'
        );

        // $this->db->insert('feed_history', $data_array_feed_history);
        //print_r(json_encode($facebookArr));
        // return ($facebookArr);
    }
    public function renewtoken() {
        $query = $this->db->query("SELECT c.id,smc.alias,smc.access_token FROM `client` As c LEFT OUTER JOIN social_media_client AS smc ON smc.client_id=c.id WHERE  smc.social_media_id=1 AND smc.alias!='' AND smc.access_token !='' and c.id=84");
        $result = $query->result_array();
        foreach ($result as $key => $value) {
            $client_id = $value['id'];
            $fbalias = $value['alias'];
            $access_token=$value['access_token'];
            $url=  urlencode("http://178.62.77.181/tbanalytics/");
            //$feed_url = "https://graph.facebook.com/oauth/client_code?access_token=$access_token&amp;client_secret=c555699cb45bf777721e04641309573a&amp;redirect_uri=$url&amp;client_id=1788504024704883";
            $feed_url = "https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&amp;client_id=1788504024704883&amp;client_secret=c555699cb45bf777721e04641309573a&amp;fb_exchange_token=$access_token";
            $data=$this->fetchData($feed_url);
            print_r($data);
        }
    }

}
