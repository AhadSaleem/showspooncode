<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class venueProfile extends MY_Controller {

    private $loggedintime;
    private $user_data;
    private $data;

    function __construct() {

        parent::__construct();

        $this->user_data=$this->Venue_Session();
        $this->data['title'] = 'admin';
        $this->data['page_title'] = "admin";
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model(['Artist_model','Venue_model','Account_model']);
        $this->session->set_userdata('showspoon_artist.is_complete',1);
        //$this->lang->load('account','english');
        //$this->lang->load('artist','english');
        $this->data['session_user']=$this->user_data;


        /*$this->user_data = array_merge($this->user_data,['is_complete'=>0]);
        $this->session->set_userdata('showspoon_artist',$this->user_data);
        dd($this->user_data);*/
        //dd('sf');


    }

    public function index() {
        
        $step = 1;
        $this->profile_steps(1);
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $this->data['image'] = $this->user_data['profile_image'];
        $this->data['id']=$this->user_data['user_id'];
        //$this->data['name']=$this->user_data['name'];
        $artist = $this->Venue_model->get_venue_details($user_id);
        $this->data['bid'] = $this->Venue_model->check_bid($user_id);
        //dd($artist);
        if(!isset($artist->user_id)){
            show_404();
        }
        // $this->data['success'] = $this->session->flashdata('success');
        // $this->data['error'] = $this->session->flashdata('error');
        // $this->form_validation->set_rules('venue_name', 'Artist Name', 'required');
        // $this->form_validation->set_rules('city_id', 'City', 'required');
        // $this->form_validation->set_rules('capacity', 'Band Type', 'required');
        // $this->form_validation->set_rules('payment_account', 'Payment Account', 'required');
        if($this->input->post("submit")=="Next" || $this->input->post('submit')=='Update'){
            // $this->Venue_model->step1($user_id,$step);
            $address=$this->input->post('address')?$this->input->post('address'):'';
            $genre = $this->input->post('genre')?$this->input->post('genre'):'';
            $genre = implode(',',$genre);
            $artist_fields=[
                //'user_id'=>$user_id,
                'genre'=>($genre!='')?$genre:'',
                'location'=>($address!='')?$address:'',
                'latitude'=>$this->input->post('hidden_lat')?$this->input->post('hidden_lat'):0,
                'longitude'=>$this->input->post('hidden_long')?$this->input->post('hidden_long'):0,
                'name'=>$this->input->post('venue_name')?$this->input->post('venue_name'):'',
                'city_id'=>$this->input->post('city_id')?$this->input->post('city_id'):'',
                'age'=>$this->input->post('age')?$this->input->post('age'):'',
                'profile_image'=>$_FILES['profile_image']['name'],
                'website'=>$this->input->post('website')?$this->input->post('website'):'',
                'new'=>'new'
            ]; 

            $image_name = $_FILES["profile_image"]["name"];
            $image_tmp  = $_FILES["profile_image"]["tmp_name"]; 
            move_uploaded_file($image_tmp,'./uploads/users/thumb/'.$image_name);
            if($_FILES["profile_image"]["name"]==""){

                 $artist_fields=[
                //'user_id'=>$user_id,
                'genre'=>($genre!='')?$genre:'',
                'location'=>($address!='')?$address:'',
                'latitude'=>$this->input->post('hidden_lat')?$this->input->post('hidden_lat'):0,
                'longitude'=>$this->input->post('hidden_long')?$this->input->post('hidden_long'):0,
                'name'=>$this->input->post('venue_name')?$this->input->post('venue_name'):'',
                'city_id'=>$this->input->post('city_id')?$this->input->post('city_id'):'',
                'age'=>$this->input->post('age')?$this->input->post('age'):'',
                //'website'=>$this->input->post('website')?$this->input->post('website'):''                
            ];
            } 

            $this->Venue_model->update_venue($artist_fields,$user_id);

            $artist_details_fields=[
                //'venue_id'=>isset($artist->row_id)?$artist->row_id:0,
                'address'=>$address,
                
                'contact_person'=>$this->input->post('contact_person')?$this->input->post('contact_person'):'',
                'capacity'=>$this->input->post('capacity')?$this->input->post('capacity'):'',
                //'phone'=>$this->input->post('phone')?$this->input->post('phone'):'',
                'zip'=>$this->input->post('zip')?$this->input->post('zip'):'',
                'payment_account'=>$this->input->post('payment_account')?$this->input->post('payment_account'):'',
                'mobile'=>$this->input->post('mobile')?$this->input->post('mobile'):'',
                'description'=>$this->input->post('description')?$this->input->post('description'):'',
                'technical_rider'=>$this->input->post('technical_rider')?$this->input->post('technical_rider'):'',
                'venue_rate'=>$this->input->post('venue_rate')?$this->input->post('venue_rate'):'',
                'venue_currency'=>$this->input->post('venue_currency')?$this->input->post('venue_currency'):''
            ];  
            $this->Venue_model->update_venue_details($artist->row_id,$artist_details_fields);
            //dd($this->input->post());
            $this->session->set_flashdata('success',$this->lang->line('profile_update',false));
            
            //dd($this->input->post());
            if($this->Venue_Profile()<$this->total_steps){

                $this->Venue_Session_Update(1);
                
                redirect('venueProfile');    
            }else{
                $this->Venue_model->step1($user_id,$step);
                $this->db->where('user_id',$user_id);
                $this->db->update('venues',['one'=>0]);
                redirect('venueProfile');
            }
            
        }

        $this->data['genres'] = $this->Artist_model->get_genres();
        $this->data['cities'] = $this->Account_model->get_cities();
        $this->data['band_types'] = $this->Artist_model->get_band_types();



        $this->data['artist']=$artist;

        $this->data['gallery']=$this->Venue_model->get_venue_galleryy($user_id);
        $this->data['videos']=$this->Venue_model->get_venue_videos($user_id,['YouTube','Vimeo']);
        $this->data['sound_cloud']=$this->Venue_model->get_venue_videos($user_id,['soundcloud','spotify']);
        $this->data['payment_info'] = $this->Venue_model->get_venue_payment_info($user_id);
        $this->data['id'] = $user_id;
        $this->load->view('venue/venue_activation', $this->data); 
    }
    public function update_media_bid(){
        $id = $this->input->post('id');
        $this->db->where('user_id',$id);
        $this->db->update('users',array('bid'=>2));
        $this->db->where('user_id',$id);
        $this->db->update('venues',['two'=>0]);
    }
    public function search()
    {
        

        $this->data['genres'] = $this->Artist_model->get_genres();
        $this->data['cities'] = $this->Account_model->get_cities();
        $this->data['band_types'] = $this->Artist_model->get_band_types();

        $this->load->library("pagination");
        $genre = $this->input->get('genre')?$this->input->get('genre'):[];
        $band = $this->input->get('band')?$this->input->get('band'):'';
        $city = $this->input->get('city')?$this->input->get('city'):'';
        $sort = $this->input->get('sort')?$this->input->get('sort'):'name';
        $filter=array(
            'genre'=>$genre,
            'city_id'=>$city,
            'band_type_id'=>$band,
        );
        if($sort=='name'){
            $sort='name';
        }elseif($sort=='rating'){
            $sort='rating';
        }
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
        $count_record=$this->Artist_model->get_artists_count($filter);
        $config["base_url"] = site_url() . "/artist/search/";
        $config["total_rows"] = $count_record; 
        $config["per_page"] = 9;
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);

        $custom_pagination = $this->Artist_model->ci_custom_pagination($config["base_url"], $config["total_rows"], $config["per_page"], $config["uri_segment"]);

        $offset = $page*$config["per_page"];
        $this->pagination->initialize($custom_pagination);
        $this->data['artists']=$this->Artist_model->get_artists($filter,$offset,$config["per_page"],$sort,'ASC');
        //dd($this->data['artists']);
        $this->data["links"] = $this->pagination->create_links();
        $this->load->view('venue/search', $this->data);
    }
        
       public function socialmedia(){

        $step = 3;
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $artist = $this->Venue_model->get_venue_details($user_id);
        //dd($artist);
        if(!isset($artist->user_id)){
            show_404();
        }
        $this->data['success'] = $this->session->flashdata('success');
        $this->data['error'] = $this->session->flashdata('error');
        
        if($this->input->post()){
            $venue_id = isset($artist->venue_id)?$artist->venue_id:0; 
            $artist_details=array(
                //'venue_id'=>$venue_id,  
                'facebook'=>$this->input->post('facebook')?$this->input->post('facebook'):'',  
                'twitter'=>$this->input->post('twitter')?$this->input->post('twitter'):'',  
                'instagram'=>$this->input->post('instragram')?$this->input->post('instragram'):'',
                'soundcloud'=>$this->input->post('soundcloud')?$this->input->post('soundcloud'):''  

                 );  
            $this->Venue_model->step3($user_id,$step);
            $this->db->where('user_id',$user_id);
            $this->db->update('venues',['three'=>0]);
            $this->Venue_model->update_venue_details($venue_id,$artist_details);

            $this->session->set_flashdata('success',$this->lang->line('profile_update',false));
            //redirect('VenueProfile');

        }





        //dd($this->data['videos']);
        //$this->load->view('venue/socialmedia', $this->data);
    }
    public function media() {

        $this->profile_steps(2);
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $artist = $this->Venue_model->get_venue_details($user_id);
        //dd($artist);
        if(!isset($artist->user_id)){
            show_404();
        }
        $this->data['success'] = $this->session->flashdata('success');
        $this->data['error'] = $this->session->flashdata('error');
        /*$this->form_validation->set_rules('name', 'Artist Name', 'required');
        $this->form_validation->set_rules('city_id', 'City', 'required');
        $this->form_validation->set_rules('band_type_id', 'Band Type', 'required');
        $this->form_validation->set_rules('payment_account', 'Payment Account', 'required');*/




        $this->data['gallery']=$this->Venue_model->get_venue_gallery($user_id);
        $this->data['videos']=$this->Venue_model->get_venue_videos($user_id,['YouTube','Vimeo']);
        $this->data['sound_cloud']=$this->Venue_model->get_venue_videos($user_id,['soundcloud','spotify']);

        //dd($this->data['videos']);
        $this->load->view('venue/media', $this->data);
    }
    public function upload_gallery() {

        $step = 2;
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        //dd($_FILES);
        $json = array();
        $directory = FCPATH . 'uploads/gallery';
        // Check user has permission

        // Check its a directory
        if (!is_dir($directory)) {
            $json['error'] = 'Folder does not exists.';
        }

        if (!$json) {
            /*print_r($json);
            print_r($_FILES['file']);
            die();*/
            if(count($_FILES['file']['name'])>0){
                if (!empty($_FILES['file']['name'][0]) && is_file($_FILES['file']['tmp_name'][0])) {
                    $total_files =count($_FILES['file']['name']);
                    for($v=0; $v<$total_files; $v++){
                        // Sanitize the filename
                        $filename = basename(html_entity_decode($_FILES['file']['name'][$v], ENT_QUOTES, 'UTF-8'));

                        $file_arr = explode('.',$filename);

                        $file_ext = end($file_arr);
                        $orignal_name = seo_Url($file_arr[0]);
                        $filename = time().'_'.mt_rand(1000,100000).$v.'.'.$file_ext;

                        // Validate the filename length
                        if ((mb_strlen($filename) < 3) || (mb_strlen($filename) > 255)) {
                            $json['error'] = 'File Name too large max length 254, min length 3';
                        }

                        // Allowed file extension types
                        $allowed = array(
                            'jpg',
                            'jpeg',
                            'gif',
                            'png'
                        );

                        if (!in_array(mb_strtolower(mb_substr(strrchr($filename, '.'), 1)), $allowed)) {
                            $json['error'] = 'Invalid file extension';
                        }

                        // Allowed file mime types
                        $allowed = array(
                            'image/jpeg',
                            'image/pjpeg',
                            'image/png',
                            'image/x-png',
                            'image/gif'
                        );

                        if (!in_array($_FILES['file']['type'][$v], $allowed)) {
                            $json['error'] = 'Invalid File extension.';
                        }

                        // Check to see if any PHP files are trying to be uploaded
                        $content = file_get_contents($_FILES['file']['tmp_name'][$v]);

                        if (preg_match('/\<\?php/i', $content)) {
                            $json['error'] = 'File type error.';
                        }

                        // Return any upload error
                        if ($_FILES['file']['error'][$v] != UPLOAD_ERR_OK) {
                            $json['error'] = 'Something went wrong.';
                        }
                    }
                } else {
                    $json['error'] = 'error in file upload';
                }
            }
        }
        $view='';
        if (!$json) {

            if(count($_FILES['file']['name'])>0){
                $this->load->library('image_lib');
                $total_files =count($_FILES['file']['name']);
                for($v=0; $v<$total_files; $v++){
                    $filename = basename(html_entity_decode($_FILES['file']['name'][$v], ENT_QUOTES, 'UTF-8'));
                    $file_arr = explode('.',$filename);
                    $file_ext = end($file_arr);
                    $orignal_name = seo_Url($file_arr[0]);
                    $filename = $orignal_name.'-'.mt_rand(1000,100000).$v.'.'.$file_ext;
                    if(move_uploaded_file($_FILES['file']['tmp_name'][$v], $directory . '/' . $filename))
                    {

                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $directory . '/' . $filename;
                        $config['new_image'] = $directory . '/thumbs/' . $filename;
                        $config['create_thumb'] = FALSE;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = 300;
                        $config['height'] = 200;
                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();
                        //$this->upload->do_upload();
                        $this->image_lib->clear();
                        $insert=array(
                            'url'=>$filename,
                            'venue_id'=>$user_id,

                        );

                        $ins_id = $this->Venue_model->add_picture($insert);
                        // $img_path=site_url('uploads/gallery/thumbs/'.$filename);
                        // $view .= $this->load->view('includes/artist-gallery-part',['id'=>$ins_id,'img_path'=>$img_path],true);
                    }
                }
                $json['view']=$view;

                $json['success'] = "Image uploaded successfully.";
            }
        }
         redirect('VenueProfile');
        //echo json_encode($json);
    }
    public function delete_gallery()
    {
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $row_id = $this->input->post('row_id')?$this->input->post('row_id'):0;
        if($row_id>0 && $user_id>0){
            $row = $this->Venue_model->get_gallery_by_id($row_id);
            
            if(isset($row->venue_id) && $row->venue_id==$user_id){
                if($row->url!=''){
                    $img_path='./uploads/gallery/thumbs/'.$row->url;    
                    @unlink($img_path);
                    $img_path='./uploads/gallery/'.$row->url;    
                    @unlink($img_path);

                }
                $this->db->where('id',$row_id)->delete('venue_gallery');

            }
            echo json_encode(array('data'=>$row_id));
            exit();
        }
        echo json_encode(array('data'=>false));
    }
    public function add_video()
    {  
        $this->load->library('upload');
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $url=$this->input->post('url')?$this->input->post('url'):'';
        $caption=$this->input->post('caption')?$this->input->post('caption'):'';
        $type=$this->input->post('type')?$this->input->post('type'):'';

        if ($type == 'YouTube') {

            @$json = file_get_contents('https://www.youtube.com/oembed?url=' . $url . '&format=json');
        } elseif ($type == 'Vimeo') {
            @$json = file_get_contents('https://vimeo.com/api/oembed.xml?url=' . $url . '&format=json');
        } elseif ($type == 'soundcloud') {
            //@$json = file_get_contents('https://vimeo.com/api/oembed.xml?url=' . $url . '&format=json');
            @$getValues = file_get_contents('http://soundcloud.com/oembed?format=js&url=' . $url . '&iframe=true&maxheight=100');
            //Clean the Json to decode
            $json = substr($getValues, 1, -2);
            //json decode to convert it as an array

        }
        $video_data='';



        $insert=[
            'url'=>$url,
            'caption'=>$caption,
            'type'=>$type,
            'venue_id'=>$user_id,
            'vide_data'=>$video_data
        ];
        if(isset($json) && $json!=''){
            //$json = json_decode($json);

            $video_data = ($json);
            $insert['video_data']=$video_data;


        }
        //dd($video_data);

        $html='';
        $insert_id = $this->Venue_model->create_video($insert);
        $video = $this->Venue_model->get_venue_videos($user_id,['YouTube','Vimeo']);
        if($insert_id>0){
            
            if($type=='soundcloud'){
                $html=$this->load->view('includes/artist-soundcloud-part',['id'=>$insert_id,'title'=>$caption,'url'=>$url,'video_data'=>$video_data],true); 
                $step = 2;
                //$this->Venue_model->step2($user_id,$step);   
            }elseif($type=='spotify'){
                $html=$this->load->view('includes/artist-spotify-part',['id'=>$insert_id,'title'=>$caption,'url'=>$url,'video_data'=>$video_data],true); 
                $step = 2;
                //$this->Venue_model->step2($user_id,$step);   
            }else{
                $html=$this->load->view('includes/artist-video-part',['id'=>$insert_id,'title'=>$caption,'url'=>$url,'type'=>$type,'video_data'=>$video_data],true); 
            }

        } //redirect('VenueProfile');
        echo json_encode(array('data'=>$html));
    }
    public function delete_video(){
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $row_id = $this->input->post('id')?$this->input->post('id'):0;

        if($row_id>0 && $user_id>0){
            $row = $this->Venue_model->get_video_by_id($row_id);

            if(isset($row->venue_id) && $row->venue_id==$user_id){

                $this->db->where('id',$row_id)->delete('venue_media');

            }
            echo json_encode(array('data'=>$row_id));
            exit();
        }
        echo json_encode(array('data'=>false));
    }
    
  

     private function profile_steps($step=1){

         //dd($this->Venue_Profile());
        if($this->Venue_Profile()==0 && $step!=1){
            redirect('venueProfile');
        }/*elseif($this->Artist_Profile()==1 && $step>2){
            redirect('artistProfile/members');
        }elseif($this->Artist_Profile()==2 && $step>3){
            redirect('artistProfile/media');
        }elseif($this->Artist_Profile()==3 && $step>4){
            redirect('artistProfile/socialmedia');
        }*/
       


    }

    public function upload_temp() {


        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        //dd($_FILES);
        $json = array();
        $directory = FCPATH . 'uploads/temp';
        // Check user has permission

        // Check its a directory
        if (!is_dir($directory)) {
            $json['error'] = 'Folder does not exists.';
        }

        if (!$json) {
            /*print_r($json);
            print_r($_FILES['file']);
            die();*/
            if(count($_FILES['file']['name'])>0){
                if (!empty($_FILES['file']['name'][0]) && is_file($_FILES['file']['tmp_name'][0])) {
                    $total_files =count($_FILES['file']['name']);
                    for($v=0; $v<$total_files; $v++){
                        // Sanitize the filename
                        $filename = basename(html_entity_decode($_FILES['file']['name'][$v], ENT_QUOTES, 'UTF-8'));

                        $file_arr = explode('.',$filename);

                        $file_ext = end($file_arr);
                        $orignal_name = seo_Url($file_arr[0]);
                        $filename = time().'_'.mt_rand(1000,100000).$v.'.'.$file_ext;

                        // Validate the filename length
                        if ((mb_strlen($filename) < 3) || (mb_strlen($filename) > 255)) {
                            $json['error'] = 'File Name too large max length 254, min length 3';
                        }

                        // Allowed file extension types
                        $allowed = array(
                            'jpg',
                            'jpeg',
                            'gif',
                            'png'
                        );

                        if (!in_array(mb_strtolower(mb_substr(strrchr($filename, '.'), 1)), $allowed)) {
                            $json['error'] = 'Invalid file extension';
                        }

                        // Allowed file mime types
                        $allowed = array(
                            'image/jpeg',
                            'image/pjpeg',
                            'image/png',
                            'image/x-png',
                            'image/gif'
                        );

                        if (!in_array($_FILES['file']['type'][$v], $allowed)) {
                            $json['error'] = 'Invalid File extension.';
                        }

                        // Check to see if any PHP files are trying to be uploaded
                        $content = file_get_contents($_FILES['file']['tmp_name'][$v]);

                        if (preg_match('/\<\?php/i', $content)) {
                            $json['error'] = 'File type error.';
                        }

                        // Return any upload error
                        if ($_FILES['file']['error'][$v] != UPLOAD_ERR_OK) {
                            $json['error'] = 'Something went wrong.';
                        }
                    }
                } else {
                    $json['error'] = 'error in file upload';
                }
            }
        }
        $view='';
        if (!$json) {

            if(count($_FILES['file']['name'])>0){
                $this->load->library('image_lib');
                $total_files =count($_FILES['file']['name']);
                for($v=0; $v<$total_files; $v++){
                    $filename = basename(html_entity_decode($_FILES['file']['name'][$v], ENT_QUOTES, 'UTF-8'));
                    $file_arr = explode('.',$filename);
                    $file_ext = end($file_arr);
                    $orignal_name = seo_Url($file_arr[0]);
                    $filename = $orignal_name.'-'.mt_rand(1000,100000).$v.'.'.$file_ext;
                    if(move_uploaded_file($_FILES['file']['tmp_name'][$v], $directory . '/' . $filename))
                    {

                    }
                }
                $json['file_name']=site_url('uploads/temp/'.$filename);
                $json['name']=($filename);

                $json['success'] = "Image uploaded successfully.";
            }
        }

        echo json_encode($json);
    }
    public function paymentinfo()
    {
        $step = 4;
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0; 
        $payment_fields = array(
                                    'account_number' =>  $this->input->post('account_number')?$this->input->post('account_number'):'',
                                    //'bank_name'      =>  $this->input->post('bank_name')?$this->input->post('bank_name'):'',
                                    'swift_code'     =>  $this->input->post('swift_code')?$this->input->post('swift_code'):'',
                                    'account_name'   =>  $this->input->post('account_name')?$this->input->post('account_name'):'',
                                    'bank'           =>  $this->input->post('bank')?$this->input->post('bank'):''
                               ); 
        $this->Venue_model->update_payment_info($user_id,$payment_fields);
        $this->Venue_model->step4($user_id,$step);
        redirect('venue/dashboard');
    }

    public function override_img()
    {
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $row_id = $this->input->post('row_id')?$this->input->post('row_id'):0;
        $this->Venue_model->override_img(array('venue_id'=>$row_id,'user_id'=>$user_id));
    }
    public function uploadPhotos()
    {
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $name = $_FILES['file']['name'];  
        $tmp  = $_FILES['file']['tmp_name'];
        move_uploaded_file($tmp,"./uploads/gallery/thumbs/".$name);
        $insert=array(
                            'url'=>$name,
                            'venue_id'=>$user_id,

                        );

        $this->Venue_model->add_picture($insert); 
        $this->db->select('*');
        $this->db->where(['venue_id'=>$user_id]);
        $sql = $this->db->get('venue_gallery');
        foreach($sql->result() as $row){
            if(isset($row->url) && $row->url!=''){
            $img_path=base_url().'uploads/gallery/thumbs/'.$row->url;
            }else{
            $img_path=base_url().'assets/images/placeholder.jpg';
            }
            $this->load->view('includes/artist-gallery-part',['id'=>$row->id,'img_path'=>$img_path]);
            }
    }
    public function city_list_data(){
        $output  = '';
        $country = $this->input->post('country');
        if($country!="" && count($country) > 0){
            $where = " where CountryID = ".$country;
        }else{
            $where = " where CountryID IN(1,2,3,4) ";
        }
        $sql = $this->db->query("select * from cities $where order by CityName ASC");
        foreach($sql->result() as $row){
            $output .= '<option value="'.$row->CityID.'">'.$row->CityName.'</option>';
        }
        echo $output;
    }

}
