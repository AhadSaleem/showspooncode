<!DOCTYPE html>
<!-- saved from url=(0049)http://165.227.110.185/showspoon/artist/dashboard -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Showspoon</title>
    <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/custom-develop.css" rel="stylesheet">
    <!-- new wireframe style -->
    <link href="<?php echo base_url(); ?>assets/css/custom_style.css" rel="stylesheet">
    <!-- for bootstrap live search -->
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.css"> -->
    <!-- for ekko lightbox -->
    <link href="<?php echo base_url(); ?>assets/css/ekko-lightbox.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- for flexslider -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flexslider.css" type="text/css" media="screen" />




    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>
    <style>
    .navbar-toggle{background:none !important;border:1px solid #ab7b15 !important;}
    .navbar-toggle > .icon-bar{background:#ab7b15 !important;}
    .spacer_medium {
     margin: 0 0 90px 0;
     }
    </style>
    <!-- for Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo site_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url(); ?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo site_url(); ?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo site_url('assets/favicon/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">
    <!--<script src="http://165.227.110.185/showspoon//assets/dist/js/bootstrap_multiselect.js"></script>-->
</head>

<body class="no-body-padd">
    <header class="main-header">
      <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">

        <!-- navbar for login link only -->
        <div class="clearfix">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
          <div class="float-left">
            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <img src="<?php echo site_url('assets/images/logo-n.png'); ?>" alt="Logo">
            </a>
          </div>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <div class="float-center">
          <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo site_url('about'); ?>">About</a></li>
          <li><a href="<?php echo site_url('showspoon-works'); ?>">How Showspoon Works</a></li>
          <li><a href="<?php echo site_url('contact'); ?>">Contact Us</a></li>
          <li><a href="<?php echo site_url('account/login'); ?>">Login</a></li>
          </ul>
          <!-- <div class="float-right">
            <a class="nav-btn" href="<?php echo base_url(); ?>account/login">Login</a>
          </div> -->
        </div>
      </div>
    </div>
        <!-- navbar for login link only ends -->

        
        <!-- <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo site_url(); ?>">
              <img src="<?php echo site_url(); ?>assets/images/logo-n.png" alt="Logo">
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo base_url(); ?>Account/login">Login</a></li>
          </ul>
        </div> -->

      </div>
    </nav>
</header> 



    <!-- space just for index -->

    <div class="spacer_medium"></div>
    <!-- space just for index ends-->

    <div class="container">
      <h2>Registrar</h2>
      <p>
      Showspoon AS <br>
      <a href="#">www.showspoon.no</a><br>
      support(a)showspoon.com
      </p>
      <h2>Contact person</h2>
      <p>
      Ahmed Fouikri <br>
      +47 463 57 941 <br>
      ahmed(a)showspoon.com
      </p>
      <h2>Name of the register</h2>
      <p>User register of the Showspoon service</p>
      <h2>Use of personal details (purpose of register)</h2>
      <p>
      Personal details are collected to make communication and use of service possible. Details can
      be used for communication between service providers and users and also for direct
      communication between users. Some personal details are visible on the profile page of the
      user, but those details are voluntary (except name).
      The handling of personal details is not outsourced, but the register data is stored on a server
      that is rented from a third party company.
      </p>
      <h2>Information content of the register</h2>
      <p>
      The following information may be stored in the register:
      </p>
      <ul>
      <li>Personal details: Name, email address, phone number, street address</li>
      <li>Account details: username, password (stored in encrypted format)</li>
      <li>The description text that the user may write about him/herself</li>
      <li>The offers and requests the user has posted to the service</li>
      <li>The given and received feedback and badges</li>
      <li>Statistical data about service usage, e.g. number times the user has logged in</li>
      </ul>
      <h2>Regular sources of information</h2>
      <p>
      Personal details are given by the user on registration to the service or when using it later.
      </p>
      <h2>Regular handovers of the information</h2>
      <p>
      The information may be handed over for research purposes as described in the <a href="#">Betingelser for
      bruk</a> that the user accepts before starting to use the service. The researchers may not publish
      any research results so that identifying information would be revealed or that any specific user
      could be detected.Information considering users of a single Lydelig community may be handed over to the
      client who has founded that community or to the community administrators appointed by that
      client.
      </p>
      <h2>Transfers of the information outside the EU and the European Economic Area</h2>
      <p>
      Information may be stored to a server that may be located inside or outside of the EU and the
      European Economic Area
      </p>
      <h2>Register protection principles</h2>
      <p>
      The information is stored on computers. The access to the information is restricted with
      passwords and physical access to the computers is restricted by the server hosting company.
      </p>
    </div>




    <div class="spacer_hundred"></div>
    <?php $this->load->view('includes/footer');?>

    


    <!-- for bootstrap select -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-select.js"></script>
    <script type="text/javascript">
        // $(document).on("scroll", function(){
        // if
        //   ($(document).scrollTop() > 20){
        //       $(".main-header").addClass("shrink");
        //     }
        //     else
        //     {
        //         $(".main-header").removeClass("shrink");
        //     }
        // });
    </script>
    
</body>

</html>