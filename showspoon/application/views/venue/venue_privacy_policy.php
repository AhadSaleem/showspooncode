<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('includes/head');?>
    <!-- for Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo site_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url(); ?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo site_url(); ?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo site_url('assets/favicon/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">
</head>

<body>
<p id="alert"></p>

<header class="main-header">
        <!-- Static navbar -->
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>


            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <img src="<?php echo site_url(); ?>assets/images/logo-n.png" alt="Logo">
            </a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="search">
                  <!-- search form -->
                  <form id="search" action="<?php echo site_url('venue/search'); ?>" method="post">
                     <input type="search" name="search" placeholder="Search" autocomplete="off">
                  </form>
                  <!-- search form ends-->
              </li>
              <li><a href="<?php echo site_url(); ?>">Artists</a></li>
              <li><a href="<?php echo site_url('venueBooking/booking'); ?>">My bookings</a></li>
              <li><a href="<?php echo site_url('venueBooking/inbox'); ?>">Inbox <span id="my_bookings" class="badge badge_upper inbox-count"></span></a></li>
              <li><a href="<?php echo site_url('venueBooking/my_requests'); ?>">Requests <span id="my_bookings" class="badge badge_upper request-count"></span></a></li>
              <li class="favourite"><a href="<?php echo site_url("venue/favourties"); ?>" class="heart"></a></li>
              <li>
                <div class="inset dropdown"> <?php if($image!=""){$img = site_url()."uploads/users/thumb/".$image; }else{$img = site_url()."assets/blank.png";} ?>                 
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img class="profile_img" src="<?php echo $img; ?>"> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url('venueProfile'); ?>">Profile</a></li>
                  <li><a href="<?php echo site_url('account/logout'); ?>">Logout</a></li>
                </ul>
                </div>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container -->
      </nav>

    </header>



    <!-- space just for index -->

    <div class="spacer_medium"></div>
    <!-- space just for index ends-->

    <div class="container">
      <h2>Registrar</h2>
      <p>
      Showspoon AS <br>
      <a href="#">www.showspoon.no</a><br>
      support@showspoon.com
      </p>
      <h2>Contact person</h2>
      <p>
      Ahmed Fouikri <br>
      +47 463 57 941 <br>
      ahmed@showspoon.com
      </p>
      <h2>Name of the register</h2>
      <p>User register of the Showspoon service</p>
      <h2>Use of personal details (purpose of register)</h2>
      <p>
      Personal details are collected to make communication and use of service possible. Details can
      be used for communication between service providers and users and also for direct
      communication between users. Some personal details are visible on the profile page of the
      user, but those details are voluntary (except name).
      The handling of personal details is not outsourced, but the register data is stored on a server
      that is rented from a third party company.
      </p>
      <h2>Information content of the register</h2>
      <p>
      The following information may be stored in the register:
      </p>
      <ul>
      <li>Personal details: Name, email address, phone number, street address</li>
      <li>Account details: username, password (stored in encrypted format)</li>
      <li>The description text that the user may write about him/herself</li>
      <li>The offers and requests the user has posted to the service</li>
      <li>The given and received feedback and badges</li>
      <li>Statistical data about service usage, e.g. number times the user has logged in</li>
      </ul>
      <h2>Regular sources of information</h2>
      <p>
      Personal details are given by the user on registration to the service or when using it later.
      </p>
      <h2>Regular handovers of the information</h2>
      <p>
      The information may be handed over for research purposes as described in the <a href="#">Betingelser for
      bruk</a> that the user accepts before starting to use the service. The researchers may not publish
      any research results so that identifying information would be revealed or that any specific user
      could be detected.Information considering users of a single Lydelig community may be handed over to the
      client who has founded that community or to the community administrators appointed by that
      client.
      </p>
      <h2>Transfers of the information outside the EU and the European Economic Area</h2>
      <p>
      Information may be stored to a server that may be located inside or outside of the EU and the
      European Economic Area
      </p>
      <h2>Register protection principles</h2>
      <p>
      The information is stored on computers. The access to the information is restricted with
      passwords and physical access to the computers is restricted by the server hosting company.
      </p>
    </div>




    <div class="spacer_hundred"></div>
    <?php //$this->load->view('includes/footer');?>
    <footer class="main-footer" style="position:fixed;bottom:0;left:0;right:0;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-left">
                    <p class="copyright">© <?php echo date('Y'); ?> Showspoon. All rights reserved.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                    <ul class="legallinks text-right">
                        <a href="<?php echo site_url('venue/privacy_policy'); ?>">Privacy Policy</a><span class="sep "> |</span>
                        <a href="<?php echo site_url('venue/terms'); ?>">Terms of Use </a><span class="sep "> |</span>
                        <a href="<?php echo site_url('venue/contact'); ?>">Contact Us</a>
                    </ul>
                </div>
            </div>
            <!-- row -->
        </div>
    </footer>
    


    <!-- for bootstrap select -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-select.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $.ajax({
          url:'<?php echo site_url("venue/getHeartCount"); ?>',
          type:'POST',
          success:function(data){
            setTimeout(function(){
            $('li.favourite a.heart').text(data);
            },500);
          }
        });
        function getArtistRequestCount(){
          $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistRequestCount"); ?>',
                        type    :  'POST',
                        dataType:  'JSON',
                        success :  function(data){

                          if(data.status=='request' && data.cnt > 0 && data.is_read==0){
                            $('.request-count').html(data.cnt);
                          }
                        }
                   });
       }
       getArtistRequestCount();
        // var clear = setInterval(function(){
        // getArtistRequestCount(); 
        // },100);
       function getArtistInboxCount(){
        $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistInboxCount"); ?>',
                        type    :  'POST',
                        dataType:  'JSON',
                        success :  function(data){ 

                          if(data.status=='message' && data.cnt > 0 && data.is_read==0){
                            $('.inbox-count').html(data.cnt);
                          }
                        }
                   });
      }
      getArtistInboxCount();
       function getArtistNotification(){
         $.ajax({
                        url     :  '<?php echo site_url("venueBooking/getArtistNotification"); ?>',
                        type    :  'POST',
                        success :  function(data){

                            $('#alert').html(data);
                        }
                   });
      }getArtistNotification();
      $(document).on('click','.close',function(){
        var id = $(this).data('id');
        var mr = $(this).data('mr');
        $.ajax({
                        url     :  '<?php echo site_url("venueBooking/deleteArtistNotification"); ?>',
                        type    :  'POST',
                        data    :  {id:id},
                        success :  function(data){
                          if(mr=='request'){
                            $('.request-count').text('');
                          }else if(mr=='message'){
                            $('.inbox-count').text('');
                          }
                        }
                   });
      });
      });
        $(document).on("scroll", function(){
        if
          ($(document).scrollTop() > 20){
              $(".main-header").addClass("shrink");
            }
            else
            {
                $(".main-header").removeClass("shrink");
            }
        });
    </script>
    
</body>

</html>