<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('admin/includes/head');?>

        <script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/forms/selects/select2.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/forms/styling/switch.min.js"></script>

    </head>
    <body class="navbar-top">

        <!-- Main navbar -->
        <?php $this->load->view('admin/includes/header');?>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <?php $this->load->view('admin/includes/sidebar');?>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Admins</span> </h4>
                            </div>


                        </div>


                    </div>
                    <!-- /page header -->


                    <div class="content">
                         <?php if(isset($error) && $error!=''){ ?>
                        <div class="alert alert-danger">
                            <button class="close" data-close="alert"></button>
                            <span><?php echo $error; ?></span>
                        </div>
                        <?php } ?>
                        <?php if(isset($success) && $success!=''){ ?>
                        <div class="alert alert-success">
                            <button class="close" data-close="alert"></button>
                            <span><?php echo $success; ?></span>
                        </div>
                        <?php } ?>

                        <!-- Page length options -->
                        <div class="panel panel-flat">

                            <div class="panel-body">
                                <form id="update"  method="post" action="<?php echo site_url('managers/create');?>">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input type="text" value="<?php echo $this->input->post('f_name');?>" name="f_name" id="f_name" class="form-control">
                                            </div>       
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input type="text" value="<?php echo  $this->input->post('l_name');?>" name="l_name" id="l_name" class="form-control">
                                            </div>       
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input required value=" <?php echo  $this->input->post('email');?>" type="email" name="email" id="email" class="form-control">
                                            </div>       
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input  type="password" name="password" id="password" class="form-control">
                                            </div>       
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Repeat Password</label>
                                                <input  type="password" name="rpassword" id="rpassword" class="form-control">
                                            </div>       
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Type</label>
                                                <select name="type" id="type" class="form-control select2">
                                                    <option <?php if( $this->input->post('type')==1){echo 'selected';}?> value="1">Admin</option>
                                                    <option <?php if( $this->input->post('type')==2){echo 'selected';}?> value="2">Sub Admin</option>
                                                </select>
                                            </div>       
                                        </div>
                                    </div>
                                    <div class="checkbox checkbox-switchery">
                                        <label>
                                            <input name="is_active" checked type="checkbox" class="switchery switch" value="1">
                                            Status
                                        </label>
                                    </div>
                                    <div class="form-group">

                                        <input type="submit" id="submit" name="submit" value="Update" class="btn btn-primary">
                                    </div>  



                                </form>
                            </div>



                        </div>
                        <!-- /page length options -->

                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Modal Header</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Some text in the modal.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>



                        <!-- Footer -->
                        <?php $this->load->view('admin/includes/footer');?>
                        <!-- /footer -->

                    </div>

                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
        <script type="application/javascript">
            $(function(){
                // $(".switch").bootstrapSwitch();
                if (Array.prototype.forEach) {
                    var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
                    elems.forEach(function(html) {
                        var switchery = new Switchery(html);
                    });
                }
                else {
                    var elems = document.querySelectorAll('.switchery');
                    for (var i = 0; i < elems.length; i++) {
                        var switchery = new Switchery(elems[i]);
                    }
                }
            });
            $(document).ready(function () {

                // $.validator.setDefaults({ ignore: ":hidden:not(.w-full)" })
                $('#submit').on('click', function () {
                    $("form#update").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            email: {
                                required: true,
                            },
                            password: {
                                required: true,
                                //minlength: 8
                            },
                            rpassword: {
                                //required: true,
                                equalTo: '#password'
                            }

                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            form.submit();
                        }
                    });
                });
            });
        </script>

    </body>
</html>
