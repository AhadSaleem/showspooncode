<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Immerse</title>
    </head>
    <body>
        <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody><tr>
                <td style="padding-top:20px" class="m_1623530055828948975m_7716617004042174243whiteMobile" width="100%" valign="top" bgcolor="#ebebeb">
                    <table class="m_1623530055828948975m_7716617004042174243deviceWidth" width="570" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tbody><tr>
                            <td style="padding:0" class="m_1623530055828948975m_7716617004042174243whiteMobile" valign="top" bgcolor="#ebebeb">
                                <a href="#m_1623530055828948975_m_7716617004042174243_">
                                    <img class="m_1623530055828948975m_7716617004042174243deviceWidth CToWUd" src="https://docs.google.com/uc?export=download&id=0ByfHHxs88eyGY18zVXhLaEcwOWs" alt="" style="display:table;border-radius:10px;padding-bottom:10px; margin-left:auto; margin-right:auto; margin-top:30px; margin-bottom:30px;" width="300" border="0" height="auto"></a>
                            </td>
                            </tr>
                            <tr>
                                <td style="border-radius:10px;line-height:24px;vertical-align:top;padding:20px 20px 32px 32px;word-break:break-all" bgcolor="#FFFFFF">
                                    <p style="font-family:Trebuchet MS,Arial,sans-serif;font-weight:400;font-size:12px;color:#323232;word-break:normal">Hello <?php echo isset($full_name)?$full_name:'';?>,</p>
                                    <p style="font-family:Trebuchet MS,Arial,sans-serif;font-weight:400;font-size:12px;color:#323232;word-break:normal">A new News item has been posted for you at Immerse Investors Portal.</p>
                                    <p style="font-family:Trebuchet MS,Arial,sans-serif;font-weight:400;font-size:12px;color:#323232;word-break:normal">The Investors portal provides a host of information to Investors with useful resources and documents.</p>
                                    <?php if(isset($excerpt) && $excerpt!=''){?>
                                    <p style="font-family:Trebuchet MS,Arial,sans-serif;font-weight:400;font-size:12px;color:#323232;word-break:normal"><?php echo isset($excerpt)?$excerpt:'';?></p>
                                    <?php }?>
                                    
                                    <p style="font-family:Trebuchet MS,Arial,sans-serif;font-weight:400;font-size:12px;color:#323232;word-break:normal">You can view news by clicking on the button below:</p>
                                    <p style="font-family:Trebuchet MS,Arial,sans-serif;font-weight:400;font-size:12px;color:#323232;word-break:normal"><a style="text-decoration: none;border: 1px solid #ccc;padding: 8px 7px;border-color: #5bc0de;background-color: #5bc0de;color: #fff" href="<?php echo isset($news_link)?$news_link:'';?>">Read More</a></p>
                                    <p style="font-family:Trebuchet MS,Arial,sans-serif;font-weight:400;font-size:12px;color:#323232;word-break:normal;margin-bottom: 0;margin-top: 30px">Thank you,</p>
                                    <p style="font-family:Trebuchet MS,Arial,sans-serif;font-weight:400;font-size:12px;color:#323232;word-break:normal;margin-top: 0px">Immerse Team</p>
                                </td>
                            </tr>
                        </tbody></table>
                    <div style="height:25px">&nbsp;</div>
                </td>
                </tr>
            </tbody></table>
    </body>
</html>
