<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_login extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('admin_model');
        // if($this->session->userdata('admin_id')){
        //     redirect('admin_dashboard');
        // }
    }

    public function index() {
        $this->data['title'] = 'Login';
        if($this->input->post('signin')=='Sign in'){
            $this->form_validation->set_rules('email','email','trim|required|valid_email');
            $this->form_validation->set_rules('password','password','trim|required');
            if($this->form_validation->run()==FALSE){
                $this->load->view('admin/login', $this->data);
            }else{
                $email    = $this->input->post('email');
                $password = md5($this->input->post('password')); 
                $admin_login = array(
                      'email'    => $email,
                      'password' => $password
                );
                $admin = $this->admin_model->checkAdminLogin($admin_login); 
                if($admin && $admin->is_active==1){
                    $admin_array = array('admin_id'=>$admin->id,'admin_email'=>$admin->email,'f_name'=>$admin->f_name,'l_name'=>$admin->l_name);
                    $admin_session = $this->session->set_userdata($admin_array);
                    redirect('admin_dashboard');
                }else{
                    $this->session->set_flashdata('error','Invalid credentials');
                    redirect('admin_login');
                }
            }
        }else{

            $this->load->view('admin/login', $this->data);
        }
    }
    protected function Send_Mail($to='',$from='',$from_name='',$subject='',$message='',$attachment=''){
        include_once APPPATH.'/libraries/sendgrid-php/vendor/autoload.php';
        $sendgrid = new SendGrid(SENDGRID_KEY);
        $emailbody   = new SendGrid\Email();
        $emailbody->addTo($to)
            ->setFrom($from)
            ->setReplyTo($from)
            ->setFromName($from_name)
            ->setSubject($subject)
            ->setHtml($message);
        if ($attachment != '') {

               $emailbody->setAttachment($attachment,'Showspoon.pdf');
            }  

        

       $sendgrid->send($emailbody);
    }
    public function login_password_recover(){
        $this->data['title'] = 'Recover Password';
        if($this->input->post('reset')=='reset'){
            $this->form_validation->set_rules('email','email','trim|required');
            if($this->form_validation->run()==FALSE){
                $this->load->view('admin/login_password_recover', $this->data);
            }else{
            $email = $this->input->post('email');
            $row = $this->admin_model->check_reset_password($email);
            if(count($row) > 0){

                    $user_data = array(
                        'data'=>$row

                    );
                $email_body = $this->load->view('email/forget_password',$user_data,true);
                $this->Send_Mail($email,FROM_EMAIL,FROM_NAME,'Forgot Password',$email_body);
                $this->session->set_flashdata('success','Your email has been successfully send, check your mailbox.');
                redirect('admin_login/login_password_recover');
            }else{
               $this->session->set_flashdata('error','Invalid email address.');
               redirect('admin_login/login_password_recover');
            }
            }
        }else{

            $this->load->view('admin/login_password_recover',$this->data);
        }
    }
    public function recover_password($id=0){
        $this->data['title'] = 'Recover Password';
        $this->data['row'] = $this->admin_model->getAdminEmail($id);
        if($this->input->post('reset')=='reset'){

            $this->form_validation->set_rules('email','email','trim|required');
            $this->form_validation->set_rules('password','password','trim|required');
            if($this->form_validation->run()==FALSE){
                $this->load->view('admin/recover_password', $this->data);
            }else{
                $email = $this->input->post('email');
                $password = md5($this->input->post('password'));
                $this->db->where('email',$email);
                $sql = $this->db->update('admin',array('password'=>$password));
                if($sql){
                	$this->session->set_flashdata('update','Your password has been updated');
                    redirect('admin_login');
                }
            }
        }else{

            $this->load->view('admin/recover_password',$this->data);
        }
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect('admin_login');
    }

    ////////////////////////////////Authentication///////////////////////////////////
    // public function auth() {

    //     $email = $this->input->post('email');
    //     $password = md5($this->input->post('password'));
    //     $password1 = ($this->input->post('password1'));
    //     $fname = $this->input->post('fname');
    //     $lname = ($this->input->post('lname'));
    //     $table_name = 'admin';
        
        
    //     $result = $this->admin_model->check_auth($email, $password, $table_name);
        
    //     if ($result) {
    //         $sess_array = array();
    //         foreach ($result as $row) {


    //             $sess_array = array(
    //                 'user_id' => $row->id,
    //                 //'user_name' => $row->admin_username,
    //                 'email' => $row->email,
    //                 'first_name' => $row->f_name,
    //                 'last_name' => $row->l_name,
    //                     //'supervisors_email' => $row->supervisors_email_id
    //             );
    //             $this->session->set_userdata('ShowspoonAdmin_logged_in', $sess_array);
    //         }
    //         redirect('admin');
    //         return TRUE;
    //     } else {

    //         $this->session->set_flashdata('message', 'Sorry! Email or Password is incorrect');
    //         redirect('admin_login');
    //         //return false;
    //     }
    // }

}

?>