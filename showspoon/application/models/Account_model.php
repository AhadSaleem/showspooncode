<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class account_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->helper('site');
    }

    private $users=['user_email','user_password','verification_key','status','type_id','created_datetime','verified','email_check','is_active','user_token','is_delete','user_image'];
    public function check_reset_password($email)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where(array('user_email'=>$email));
        $sql = $this->db->get();
        return $sql->row();
    }
    public function get_email_for_recover($id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where(array('user_id'=>$id));
        $sql = $this->db->get();
        return $sql->row()->user_email;
    }
    public function recover_password($email,$password)
    {
        $this->db->where('user_email',$email)->update('users',array('user_password'=>$password));
    }
    public function get_user_by_id($id=0) {

        $query="select * from users where user_id=".(int)$id."  limit 1";
        $query=$this->db->query($query);
        return $query->row();
    }
    public function get_user_by_email($email='') {
        if($email!=''){
            $query="select * from users where user_email=?  limit 1";
            $query=$this->db->query($query,array($email));
            return $query->row();
        }
        return false;
    }
    public function get_auth($email='',$password='') {
        if($email!='' && $password!=''){
            $query="select * from users where user_email=? and user_password=?  limit 1";
            $query=$this->db->query($query,array($email,$password));
            return $query->row();
        }
        return false;
    }
    public function get_cities(){
        $data=[];
        $query = $this->db->query("select * from cities order by CityName asc");
         $results = $query->result();
        if(count($results)>0){
            foreach($results as $row){
                $data[$row->CityID]=$row->CityName;
            }
        }
        return $data;
    }
    public function create_user($data=[]) {

        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->users)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0){
            $this->db->insert('users',$insert);
            return $this->db->insert_id();
        }
        return false;


    }
    
    public function update_user($data=[]) {
        $id=0;
        if(isset($data['user_id']) && $data['user_id']>0){
            $id=$data['user_id'];
        }
        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->users)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0){
            
            $this->db->where('user_id',$id)->update('users',$insert);
            
        }
        return false;


    }
    public function email_exists($email)
    {
        $query = $this->db
                 ->select('*')
                 ->from('users')
                 ->where(['user_email'=>$email])
                 ->get();
        if($query->num_rows() > 0)
        {
            return 'Email Already Exists';
        }
    }
}
