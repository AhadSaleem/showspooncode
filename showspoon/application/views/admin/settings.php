<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('admin/includes/head');?>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/pages/datatables_advanced.js"></script>
        <style>
			label{
				font-weight: 500;
				font-size: 12px;
			}
        </style>
    </head>
    <body class="navbar-top">

        <!-- Main navbar -->
        <?php $this->load->view('admin/includes/header');?>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <?php $this->load->view('admin/includes/sidebar');?>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Settings</span> </h4>
                            </div>

                            
                        </div>

                        
                    </div>
                    <!-- /page header -->

                   <div class="content">

					<!-- Page length options -->
					<div class="panel panel-flat">
						<div class="col-md-8 col-md-offset-2"><br>
						<div id="message"></div>
						<?php if($this->session->flashdata('updated')) :  ?>
						<?php echo '<p class="alert alert-success">'.$this->session->flashdata('updated').'</p>' ?>
						<?php endif; ?>
						<?php foreach($settings as $row) :  ?>
						<?php echo form_open("admin/settings",['class'=>'form-horizontal','id'=>'settings','method'=>'post']); ?>
								

								<input type="hidden" name="id" value="<?php echo $row->id;?>">
								<fieldset>
									<legend><strong>Paypal Settings</strong></legend>
									<div class="form-group">
								    <label class="col-sm-2 control-label">Method</label>
								    <div class="col-sm-10">
								      <label class="radio-inline">
										  Sandbox<input type="radio" name="paypal" value="0" 
										   <?php if($row->paypal==0){echo "checked";}?> /> 
										</label>
										<label class="radio-inline">
										  <input type="radio" name="paypal" value="1" <?php if($row->paypal==1){echo "checked";}?>>Live
										</label>
								    </div>
								  </div>
                                  <div class="form-group">
                                    <label  class="col-sm-2">Paypal Email</label>
                                    <div class="col-sm-10">
                                      <input type="text" class="form-control" value="<?php echo $row->paypal_email?>" name="paypal_email" required placeholder="Payment Email">
                                    </div>
                                  </div>

								</fieldset>
								<fieldset>
									<legend><strong>General Settings</strong></legend>

									<div class="form-group">
								    <label class="col-sm-2">From Name</label>
								    <div class="col-sm-10">
								      <input type="text" name="name" value="<?php echo $row->name?>" class="form-control" required placeholder="Name">
								    </div>
								  </div>

								  <div class="form-group">
								    <label  class="col-sm-2">From Email</label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control" value="<?php echo $row->email?>" name="email" required placeholder="Payment Email">
								    </div>
								  </div>
								</fieldset>

							
								  <fieldset>
								  	<legend><strong>Records per page</strong></legend>
								  	<div class="form-group">
								    <label  class="col-sm-2">Per page</label>
								    <div class="col-sm-10">
								      <select id="perpage" class="form-control" required name="perpage">
								      <option value="<?php echo $row->perpage;?>">
								      <?php echo $row->perpage ; ?> &nbsp;Records</option>
                                        <?php
                                        for($v=1;$v<=10;$v++){

                                            $vp=$v*5;    
                                            
                                                echo '<option value="'.$vp.'" required>'.$vp.' Records</option>';
                                           

                                        }
                                        ?>
                                    </select>


								    </div>
								  </div>
								  <div class="form-group">
								    <div class="col-sm-offset-2 col-sm-10">
							
				 <?php //echo anchor("admin/update_settings/{$row->id}",'Submit',['class'=>'btn btn-info']) ?>
								       <input type="submit" id="btn-settings" value="Submit" class="btn btn-lg btn-info">

								    </div>
								  </div>
								  </fieldset>  
								  <?php endforeach; ?>
								</form>
						</div>
						
						



					</div>
					<!-- /page length options -->




					<!-- Footer -->
					 <?php $this->load->view('admin/includes/footer');?>
					<!-- /footer -->

				</div>
                    
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
        <script>

        	$('#btn-settings').on('click', function () {
                    $("form#settings").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            email: {
                                required: true,
                            }                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                           // Send_MSG(form); 

                           form.submit();
                          /* var paypal = form.paypal.value;
                           var name = form.name.value;
                           var email = form.email.value;
                           var perpage = form.perpage.value;

                           $.ajax({
				                    type:'POST',
				                    url: '<?php// echo site_url('admin/update_settings');?>',
				                    data:{paypal:paypal,name:name,email:email,perpage:perpage,id:<?php echo $row->id?>},
				                    dataType:'html',
				                   
				                    success:function(data){
				                        $("#message").html(data);

				                        
				                    },
				                    error: function(data){
				                        //$('#btn-1').removeAttr('disabled');
				                        //alert('Something went wrong!');
				                    }
				                });*/
                        }
                    });
                });

        </script>




    </body>
</html>
