<?php //echo validation_errors();?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('includes/head');?>
     <style>
    .navbar-toggle{background:none !important;border:1px solid #ab7b15 !important;}
    .navbar-toggle > .icon-bar{background:#ab7b15 !important;}
    .error{border:1px solid #d9a432;}
    @import url("https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
      label{
        position: relative;
        cursor: pointer;
        color: #555;
        font-size: 18px;
        margin-left:3px;
      }

      input[type="checkbox"]{
        position: absolute;
        right: 9000px;
      }

      /*Check box*/
      input[type="checkbox"] + .label-text:before{
        content: "\f096";
        font-family: "FontAwesome";
        speak: none;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        -webkit-font-smoothing:antialiased;
        width: 1em;
        display: inline-block;
        margin-right: 5px;
        font-size: 18px;
        vertical-align: middle;
      }

      input[type="checkbox"]:checked + .label-text:before{
        content: "\f14a";
        color: #d9a432;
        animation: effect 250ms ease-in;
      }

      input[type="checkbox"]:disabled + .label-text{
        color: #aaa;
      }

      input[type="checkbox"]:disabled + .label-text:before{
        content: "\f0c8";
        color: #ccc;
      }
      @keyframes effect{
        0%{transform: scale(0);}
        25%{transform: scale(1.3);}
        75%{transform: scale(1.4);}
        100%{transform: scale(1);}
      }
    </style>
    <!-- for Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo site_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url(); ?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo site_url(); ?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo site_url('assets/favicon/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">
    </head>
    <!-- <body style="background:url('<?php //echo base_url();?>/assets/images/loginpage.jpg') center top no-repeat; background-size:cover;"> -->
        <body class="no-body-padd">

        <?php $this->load->view('includes/header2');?>
    <div class="login_bg">
        <div class="container">
            
            
            
            
            
            <div class="row signupbox vertical-align v2">
                
                <div class="loginContainer v2">
                    
                    <h3 class="text-center m-t-lg m-b-sm">Signup to Your Account</h3>


                    <div class="well well-md m-b-none npt">
                        <?php if(isset($error) && $error!=''){?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <strong>Warning! </strong><?php echo $error;?>
                        </div>
                        <?php }?>
                        <?php if(isset($success) && $success!=''){?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <strong>Success! </strong><?php echo $success;?>
                        </div>
                        <?php }?>

                        <?php if($this->session->flashdata('success')){?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <strong>Success! </strong><?php echo $this->session->flashdata('success');?>
                        </div>
                        <?php }?>

                        <form id="register" method="post">
                            <?php
                            $type = $this->input->get('type')?$this->input->get('type',true):1;

                            ?>
                            <div class="form-group text-center">
                                <label class="">
                                    <input class="user_type" <?php if($type==1){echo 'checked';}?> type="radio" name="type" value="1"> <span class="small">Artist</span>
                                </label> &nbsp; 
                                <label>
                                    <input class="user_type"  <?php if($type==2){echo 'checked';}?> type="radio" name="type" value="2"> <span class="small">Venue</span>
                                </label>
                            </div><!-- form-group -->
                            <div class="form-group">
                                <input required type="email" name="email" id="email" class="form-control" placeholder="Your Email" required="required" />
                            </div><!-- form-group --> 
                            <span id="emailMsg"></span>
                            <div class="form-group">
                                <input required type="password" id="password" name="password" class="form-control" placeholder="Password" required="required" />
                            </div><!-- form-group -->
                            <div class="form-group">
                                <input required type="password" id="rpassword" name="rpassword" class="form-control" placeholder="Repeat Password" required="required"/>
                            </div><!-- form-group -->
                            <span id="passwordMsg"></span> 
                            <div class="form-group">
                            <div class="custom_select_box">
                            <div class="form-check">
                            <label>
                            <input type="checkbox" name="terms_use" id="terms_use" value="0"> <span class="label-text" style="font-size: 13px; font-weight: normal;">I accept the Terms of Use and the Privacy Policy of Showspoon.</span>
                            </label>
                            </div>
                            </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" id="submit" class="btn btn-primary btn-block" value="Signup">
                            </div><!-- form-group -->
                            <p class="text-center text-muted text-sm">or Sign Up using</p>
                            <div class="form-group">
                                <a href="<?php echo site_url('account/facebook_signup/?page=signup&type='.$type);?>" class="btn btn-facebook btn-block"><i class="fa fa-facebook m-r-sm"></i>Signup via Facebook</a>
                                <!--<a href="<?php echo site_url('account/google_signup/?page=signup&type='.$type);?>" class="btn btn-gmail btn-block disabled"><i class="fa fa-google-plus m-r-sm"></i>Signup via Gmail</a>-->
                            </div><!-- form-group -->
                            <p class="small text-center">Already have an account? <a href="<?php echo site_url('account/login'); ?>">Click here</a> to Login</p>
                        </form>
                    </div>

                </div><!-- col-lg-4 -->

            </div><!-- row -->
        </div><!-- container -->
    </div>

    <?php $this->load->view('includes/footer');?>
    
        <!-- <script src="<?php echo site_url();?>assets/js/jquery.min.js"></script>
        <script src="<?php echo site_url();?>assets/js/bootstrap.min.js"></script> -->
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/js/jquery.validate.js"></script>
        <script>
            $(document).ready(function () {

                $('.user_type').on('change',function(){
                    var user_type = $(this).val();
                    var social_lnk = '<?php echo site_url('account/google_signup/?');?>';

                    if(user_type==1){
                        $('.btn-facebook').attr('href',social_lnk+'page=signup&type=1');
                    }else{
                        $('.btn-facebook').attr('href',social_lnk+'page=signup&type=2');
                    }
                });
                $('#submit').on('click', function () {

                    $("form#register").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            email: {
                                required: true,
                            },
                            password: {
                                required: true,
                                //minlength: 8
                            },
                            rpassword: {
                                required: true,
                                equalTo: '#password'
                                //minlength: 8
                            },
                            terms_use: {
                                required: true,
                            }

                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            form.submit();
                        }
                    });
                });

                $('#email').bind('keyup , change',function(){

                var email = $('#email').val(); 
                $.ajax({
                            url      :  '<?php echo site_url("account/email_exists"); ?>',
                            type     :  'POST',
                            data     :  {email:email},
                            success  :  function(data)
                            {
                                 if(data=='Email Already Exists')
                                 {
                                    $('#emailMsg').html(data+'<br><br>').css('color','#d9a432');
                                    $('#submit').prop('disabled',true);
                                    $('#password').prop('disabled',true);
                                    $('#rpassword').prop('disabled',true);
                                 }
                                 else
                                 {
                                    $('#emailMsg').html('');
                                    $('#submit').prop('disabled',false);
                                    $('#password').prop('disabled',false);
                                    $('#rpassword').prop('disabled',false);
                                 }
                            }
                      });
                   });

                // $('#rpassword').keyup(function(){

                //        if($('#password').val()==$('#rpassword').val())
                //        {
                //            $('#passwordMsg').html('');
                //            $('#submit').prop('disabled',false);
                //        }
                //        else
                //        {
                //            $('#passwordMsg').html('Password Do not Match'+'<br><br>').css('color','#d9a432');
                //            $('#submit').prop('disabled',true);
                //        }
                // });
            });
        </script>

    </body>
</html>