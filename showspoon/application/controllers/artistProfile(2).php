<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class artistProfile extends MY_Controller {

    private $loggedintime;
    private $user_data;
    private $data;

    function __construct() {

        parent::__construct();

        $this->user_data=$this->Artist_Session();
        $this->data['title'] = 'admin';
        $this->data['page_title'] = "admin";
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model(['artist_model','venue_model','account_model']);
        $this->session->set_userdata('showspoon_artist.is_complete',1);
        $this->lang->load('account','english');
        $this->lang->load('artist','english');


        /*$this->user_data = array_merge($this->user_data,['is_complete'=>0]);
        $this->session->set_userdata('showspoon_artist',$this->user_data);
        dd($this->user_data);*/
        //dd('sf');


    }

    public function index() {

        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $artist = $this->artist_model->get_artist_details($user_id);
        //dd($artist);
        if(!isset($artist->user_id)){
            show_404();
        }
        $this->data['success'] = $this->session->flashdata('success');
        $this->data['error'] = $this->session->flashdata('error');
        $this->form_validation->set_rules('artist_name', 'Artist Name', 'required');
        $this->form_validation->set_rules('city_id', 'City', 'required');
        $this->form_validation->set_rules('band_type_id', 'Band Type', 'required');
        $this->form_validation->set_rules('payment_account', 'Payment Account', 'required');
        if ($this->form_validation->run() !== FALSE)
        {
            $genre = $this->input->post('genre')?$this->input->post('genre'):'';
            $genre = implode(',',$genre);
            $artist_fields=[
                'user_id'=>$user_id,
                'genre'=>($genre!='')?$genre:'',
                'artist_name'=>$this->input->post('artist_name')?$this->input->post('artist_name'):'',
                'city_id'=>$this->input->post('city_id')?$this->input->post('city_id'):'',
                'band_type_id'=>$this->input->post('band_type_id')?$this->input->post('band_type_id'):'',
            ];
            $this->artist_model->update_artist($artist_fields);
            $artist_details_fields=[
                'artist_id'=>isset($artist->row_id)?$artist->row_id:0,
                'contact_person'=>$this->input->post('contact_person')?$this->input->post('contact_person'):'',
                'phone'=>$this->input->post('phone')?$this->input->post('phone'):'',
                'zip'=>$this->input->post('zip')?$this->input->post('zip'):'',
                'payment_account'=>$this->input->post('payment_account')?$this->input->post('payment_account'):'',
                'mobile'=>$this->input->post('mobile')?$this->input->post('mobile'):'',
                'biography'=>$this->input->post('biography')?$this->input->post('biography'):'',
            ];
            $this->artist_model->update_artist_details($artist_details_fields);
            $this->session->set_flashdata('success',$this->lang->line('profile_update',false));
            $this->Artist_Session_Update(1);
            redirect('artistProfile/members');
        }

        $this->data['genres'] = $this->artist_model->get_genres();
        $this->data['cities'] = $this->account_model->get_cities();
        $this->data['band_types'] = $this->artist_model->get_band_types();



        $this->data['artist']=$artist;
        $this->load->view('artist/profile', $this->data);
    }
    public function members() {

        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $artist = $this->artist_model->get_artist_details($user_id);
        //dd($artist);
        if(!isset($artist->user_id)){
            show_404();
        }
        $this->data['success'] = $this->session->flashdata('success');
        $this->data['error'] = $this->session->flashdata('error');
        $this->form_validation->set_rules('artist_name', 'Artist Name', 'required');
        $this->form_validation->set_rules('city_id', 'City', 'required');
        $this->form_validation->set_rules('band_type_id', 'Band Type', 'required');
        $this->form_validation->set_rules('payment_account', 'Payment Account', 'required');




        $this->data['members']=$this->artist_model->get_artist_members($user_id);


        $this->load->view('artist/members', $this->data);
    }
    public function search()
    {

        $this->data['genres'] = $this->artist_model->get_genres();
        $this->data['cities'] = $this->account_model->get_cities();
        $this->data['band_types'] = $this->artist_model->get_band_types();

        $this->load->library("pagination");
        $genre = $this->input->get('genre')?$this->input->get('genre'):[];
        $band = $this->input->get('band')?$this->input->get('band'):'';
        $city = $this->input->get('city')?$this->input->get('city'):'';
        $sort = $this->input->get('sort')?$this->input->get('sort'):'artist_name';
        $filter=array(
            'genre'=>$genre,
            'city_id'=>$city,
            'band_type_id'=>$band,
        );
        if($sort=='name'){
            $sort='artist_name';
        }elseif($sort=='rating'){
            $sort='rating';
        }
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
        $count_record=$this->artist_model->get_artists_count($filter);
        $config["base_url"] = site_url() . "/artist/search/";
        $config["total_rows"] = $count_record; 
        $config["per_page"] = 9;
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);

        $custom_pagination = $this->artist_model->ci_custom_pagination($config["base_url"], $config["total_rows"], $config["per_page"], $config["uri_segment"]);

        $offset = $page*$config["per_page"];
        $this->pagination->initialize($custom_pagination);
        $this->data['artists']=$this->artist_model->get_artists($filter,$offset,$config["per_page"],$sort,'ASC');
        //dd($this->data['artists']);
        $this->data["links"] = $this->pagination->create_links();
        $this->load->view('venue/search', $this->data);
    }



    public function add_member()
    {
        $this->load->library('upload');
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $name=$this->input->post('name')?$this->input->post('name'):'';
        $alias=$this->input->post('alias')?$this->input->post('alias'):'';
        $role=$this->input->post('role')?$this->input->post('role'):'';
        $insert=[
            'name'=>$name,
            'role'=>$role,
            'alias'=>$alias,
            'artist_id'=>$user_id
        ];

        if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = './uploads/members/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            //$config['max_size']     = '100';
            //$config['max_width'] = '1024';
            //$config['max_height'] = '768';
            $this->upload->initialize($config);


            if ( $this->upload->do_upload('file'))//Check if upload is unsuccessful
            {
                
                ////[ THUMB IMAGE ]
                $config2['image_library'] = 'gd2';
                $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                $config2['new_image'] = './uploads/members/thumbs';
                $config2['maintain_ratio'] = TRUE;
                $config2['create_thumb'] = TRUE;
                $config2['thumb_marker'] = '';
                $config2['width'] = 300;
                $config2['height'] = 300;
                $this->load->library('image_lib',$config2); 
                
                if (!$this->image_lib->resize()){
                    //$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));   
                }
                $insert['image_url']=$this->upload->file_name;
            }
            
            

            //print_r($_POST);
            //print_r($_FILES);
        }
        $html='';
            $insert_id = $this->artist_model->create_member($insert);
            if($insert_id>0){
                if(isset($insert['image_url'])){
                    $img_path=base_url().'uploads/members/thumbs/'.$insert['image_url'];
                }else{
                    $img_path=base_url().'assets/images/placeholder.jpg';
                }
                $html='<div data-id="'.$insert_id.'" class="col-lg-3 col-md-6 member_list">
                                    <div class="panel panel-body b">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#" data-popup="lightbox">
                                                    <img src="'.$img_path.'" style="width: 70px; height: 70px;" class="img-circle" alt="">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <p class="text-muted m-b-xs">'.$name.'</p>
                                                <h6 class="media-heading">'.$alias.'</h6>
                                                <h6 class="media-heading">'.$role.'</h6>
                                                <div class="">
                                                    <a  href="javascript:;" class="edit_btn btn btn-info btn-sm r-2x">Edit</a>
                                                    <a href="javascript:;" class="delete_btn btn btn-danger btn-sm r-2x">Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>';
            }
            echo json_encode(array('data'=>$html));
    }
    public function delete_member()
    {
        $user_id = isset($this->user_data['user_id'])?$this->user_data['user_id']:0;
        $row_id = $this->input->post('row_id')?$this->input->post('row_id'):0;
        if($row_id>0 && $user_id>0){
            $row = $this->artist_model->get_member_by_id($row_id);
            
            if(isset($row->artist_id) && $row->artist_id==$user_id){
                if($row->image_url!=''){
                    $img_path='./uploads/members/thumbs/'.$row->image_url;    
                    @unlink($img_path);
                    $img_path='./uploads/members/'.$row->image_url;    
                    @unlink($img_path);
                    
                }
                $this->db->where('id',$row_id)->delete('artist_member');
                
            }
             echo json_encode(array('data'=>$row_id));
            exit();
        }
         echo json_encode(array('data'=>false));
    }
}
