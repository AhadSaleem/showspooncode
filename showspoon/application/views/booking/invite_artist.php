<!DOCTYPE html>
<!-- saved from url=(0049)http://165.227.110.185/showspoon/artist/dashboard -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Invite Venue</title>
    <!-- for Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url(); ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url(); ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url(); ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url(); ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url(); ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url(); ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url(); ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url(); ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url(); ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo site_url(); ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url(); ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url(); ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url(); ?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo site_url(); ?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo site_url('assets/favicon/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">
    <link href="<?php echo site_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?php echo site_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/custom-develop.css" rel="stylesheet">
    <!-- new wireframe style -->
    <link href="<?php echo site_url(); ?>assets/css/custom_style.css" rel="stylesheet">
    <!-- for bootstrap live search -->
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.css"> -->
    <!-- for ekko lightbox -->
    <link href="<?php echo site_url(); ?>assets/css/ekko-lightbox.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- for flexslider -->
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/flexslider.css" type="text/css" media="screen" />
    <!-- for date picker -->
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/jquery-ui.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="<?php echo site_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/select2.min.js"></script>
    <!--<script src="http://165.227.110.185/showspoon//assets/dist/js/bootstrap_multiselect.js"></script>-->
</head>

<body>

<p id="alert" style="margin: 0;"></p>

    <header class="main-header">
        <!-- Static navbar -->
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>

            <!-- <button type="button" class="navbar-toggle collapsed filter_btn" data-toggle="collapse" data-target="#sidebar" aria-expanded="false" aria-controls="sidebar">
              <i class="fa fa-filter" aria-hidden="true"></i>
            </button> -->


            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <img src="<?php echo site_url(); ?>assets/images/logo-n.png" alt="Logo">
            </a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="search">
                  <!-- search form -->
                  <form id="search" action="<?php echo site_url('artist/search'); ?>" method="post">
                     <input type="search" name="search" placeholder="Search" autocomplete="off">
                  </form>
                  <!-- search form ends-->
              </li>
              <li><a href="<?php echo site_url(); ?>">Venues</a></li>
              <li><a href="<?php echo site_url('artistBooking/booking'); ?>">My bookings</a></li>
              <li><a href="<?php echo site_url('artistBooking/inbox'); ?>">Inbox <span id="my_bookings" class="badge badge_upper inbox-count"></span></a></li>
              <li><a href="<?php echo site_url('artistBooking/my_requests'); ?>">Requests <span id="my_bookings" class="badge badge_upper request-count"></span></a></li>
              <li class="favourite"><a href="<?php echo site_url("artist/favourties"); ?>" class="heart"></a></li>
              <li>
                <div class="inset dropdown"> <?php if($image!=""){$img = site_url()."uploads/users/thumb/".$image; }else{$img = site_url()."assets/blank.png";} ?>                 
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img class="profile_img" src="<?php echo $img; ?>"> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url('artistProfile'); ?>">Profile</a></li>
                  <li><a href="<?php echo site_url('account/logout'); ?>">Login</a></li>
                </ul>
                </div>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container -->
      </nav>

    </header>


    <!-- content area -->
    <div class="container">
      

      <div>
        <h1 class="text-center marg_thrty_topper">Booking Request</h1>
        <div class="spacer_medium">
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <?php if($this->session->flashdata('error')){ ?>
              <div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Warning!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?>
              <div class="invite_box">
                <div class="clearfix member_box">
                  <figure>
                      <?php
                      $user_image=site_url('assets/dist/images/noimage2.jpg');
                      if(isset($venue->profile_image) && $venue->profile_image!=''){
                          $user_image=site_url('uploads/users/thumb/'.$venue->profile_image);
                      }
                      ?>
                      <img src="<?php echo $user_image; ?>" alt="Member">
                  </figure>
                  <figcaption>
                      <?php echo isset($venue->name)?$venue->name:'';?> <br>
                      <!-- <span>Guitarist</span> -->
                  </figcaption>
                </div><!-- member_box -->
              </div>
            </div> 
          </div>
        </div><!-- spacer_medium -->
        <div class="invitation_form_wrap"> 
          <form class="invitation_form" action="<?php echo site_url(); ?>artistBooking/book/<?php echo $venue->venue_id; ?>" method="post">
            <input type="hidden" name="id" value="<?php echo $venue->venue_id; ?>"> 
            <input type="hidden" name="venue_currency" value="<?php echo $venue->venue_currency; ?>">
            <input type="hidden" name="venue_amount" value="<?php echo $venue->venue_rate; ?>">
            <div class="row">
              <div class="col-md-6 col-md-offset-3">
                <!-- form -->
                <div class="spacer">
                    <div class="row">
                      <div class="col-sm-5">
                        <label for="event-date">
                          event date
                        </label>  
                      </div>
                      <div class="col-sm-7"> <input type="hidden" name="venue_rate" value="<?php echo $venue->venue_rate; ?>">
                        <!-- <input type="text" name="" id="event-date" class="form-control" placeholder="Date"> -->
                        <!-- date -->
                        <div class="form-group">
                          <div class='input-group date' id='datetimepicker1'>
                              <input type='text' name="event_date" class="form-control" id="datepicker" autocomplete="off" />
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div> <span id="event-date"></span>
                      </div> 
                        <!-- date -->
                      </div>
                    </div>
                </div><!-- spacer -->

                <div class="spacer">
                    <div class="row">
                      <div class="col-sm-5">
                        <label for="event-time">
                          performance time
                        </label>
                      </div>
                      <div class="col-sm-7">
                        <div class="row">
                          <div class="col-sm-5">
                            <div class="form-group">
                                <div class='input-group'>
                                  <input id="start" value="" name="start_time" style="width: 100%;" autocomplete="off" />
                                    <span id="start-time"></span>
                                </div> 
                            </div>
                          </div>
                          <div class="col-sm-2 text-center lh_forty">
                            To
                          </div>
                          <div class="col-sm-5">
                            <div class="form-group">
                                <div class='input-group'>
                                  <input id="end" value="" style="width: 100%;" name="end_time" autocomplete="off" />
                                    <span id="end-time"></span>
                                </div> 
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div><!-- spacer -->

                <!-- <div class="spacer">
                    <div class="row">
                      <div class="col-sm-5">
                        <label for="event-time">
                          sound check
                        </label>
                      </div>
                      <div class="col-sm-7">
                        <input type="radio" id="sound-check-one" name="sound-check" value="1" checked="">
                          <label for="sound-check-one">Yes</label> &nbsp;&nbsp;
                          <input type="radio" id="sound-check-two" name="sound-check" value="2">
                          <label for="sound-check-two">no</label>
                      </div>
                    </div>
                </div> -->

                
                  <div class="spacer">
                    <div class="row">
                      <div class="col-sm-5">
                        <label for="event-time">
                          message
                        </label>
                      </div>
                      <div class="col-sm-7">
                        <div class="form-group">
                        <textarea class="form-control" name="message" id="msg"></textarea>
                        <span id="message"></span>
                      </div>
                      </div> 
                    </div>
                </div><!-- spacer -->


                <div class="spacer">
                    <div class="row">
                      <div class="col-sm-7 col-sm-offset-5">
                        <input type="submit" name="submit" value="Sent Request" class="btn btn-primary">
                      </div>
                    </div>
                </div><!-- spacer -->

                <!-- form ends -->
              </div>  
            </div><!-- row -->

          </form><!-- invitation_form --> 
        </div><!-- invitation_form_wrap -->
      </div>


    </div>  
    <!-- content area ends -->


    <!-- extra space -->
    <!-- <div class="clearfix" style="height: 300px;"></div> -->
    <!-- extra space ends-->

    <div class="spacer_hundred"></div>
    <script src="<?php echo site_url(); ?>assets/js/pnotify.min.js"></script>

    <footer class="main-footer" style="position:fixed;bottom:0;left:0;right:0;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-left">
                    <p class="copyright">© <?php echo date('Y'); ?> Showspoon. All rights reserved.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                    <ul class="legallinks text-right">
                        <a href="<?php echo site_url('artist/privacy_policy'); ?>">Privacy Policy</a><span class="sep "> |</span>
                        <a href="<?php echo site_url('artist/terms'); ?>">Terms of Use </a><span class="sep "> |</span>
                        <a href="<?php echo site_url('artist/contact'); ?>">Contact Us</a>
                    </ul>
                </div>
            </div>
            <!-- row -->
        </div>
    </footer>


    <!-- for bootstrap select -->
    <script src="<?php echo site_url(); ?>assets/js/bootstrap-select.js"></script>
    <!-- for smooth scroll -->
    <script type="text/javascript">
        // Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
    </script>
<style>.red{border:1px solid #a94442;}</style>
    <!-- for back to top -->
    <script type="text/javascript">
     $(document).ready(function(){
     $('.invitation_form').submit(function(e){
      if($("input[name='event_date']").val()==""){
        $('#event-date').text('This field is required.').css('color','#a94442');
      }
      if($("input[name='start_time']").val()==""){
        $('#start-time').text('This field is required.').css('color','#a94442');
      }
      if($("input[name='end_time']").val()==""){
        $('#end-time').text('This field is required.').css('color','#a94442');
      }
      // if($("#msg").val()==""){
      //   $('#message').text('This field is required.').css('color','#a94442');
      // }
      if($("input[name='event_date']").val()!="" && $("input[name='start_time']").val()!="" && $("input[name='end_time']").val()!="" && $("input[name='amount']").val()!=""){
        $('.invitation_form').submit();
      }
      e.preventDefault();
     });
     $("input[name='event_date']").bind('change , keyup',function(){
      $('#event-date').text('');
     });
     $("input[name='start_time']").bind('change , keyup',function(){
      $('#end-time').text('');
      $('#start-time').text('');
     });
     $("input[name='end_time']").bind('change , keyup',function(){
      $('#end-time').text('');
     });
     $("#msg").on('keyup',function(){ 
      $('#message').text('');
     });
     $.ajax({
        url:'<?php echo site_url("artist/getHeartCount"); ?>',
        type:'POST',
        success:function(data){
          setTimeout(function(){
          $('li.favourite a.heart').text(data);
          },500);
        }
      });
     function getVenueRequestCount(){
          $.ajax({
                        url     :  '<?php echo site_url("artistBooking/getVenueRequestCount"); ?>',
                        type    :  'POST',
                        dataType:  'JSON',
                        success :  function(data){

                          if(data.status=='request' && data.cnt > 0 && data.is_read==0){
                            $('.request-count').html(data.cnt);
                          }
                        }
                   });
       }
       getVenueRequestCount(); 
        // var clear = setInterval(function(){
        // getVenueRequestCount(); 
        // },100);
      function getVenueInboxCount(){
        $.ajax({
                        url     :  '<?php echo site_url("artistBooking/getVenueInboxCount"); ?>',
                        type    :  'POST',
                        dataType:  'JSON',
                        success :  function(data){ 

                          if(data.status=='message' && data.cnt > 0 && data.is_read==0){
                            $('.inbox-count').html(data.cnt);
                          }
                        }
                   });
      }
      getVenueInboxCount();
      $(document).on('click','.close',function(){
        var id = $(this).data('id'); 
        var mr = $(this).data('mr');
        $.ajax({
                        url     :  '<?php echo site_url("artistBooking/deleteVenueNotification"); ?>',
                        type    :  'POST',
                        data    :  {id:id},
                        success :  function(data){

                          if(mr=='request'){
                            $('.request-count').text('');
                          }else if(mr=='message'){
                            $('.inbox-count').text('');
                          }
                        } 
                   });
      });
      function getVenueNotification(){
         $.ajax({
                        url     :  '<?php echo site_url("artistBooking/getVenueNotification"); ?>',
                        type    :  'POST',
                        success :  function(data){

                            $('#alert').html(data);
                        }
                   });
      }
      getVenueNotification();

      // var clear = setInterval(function(){ 
      //   getVenueNotification(); 
      // },100);

     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('.back-to-top').fadeIn();
            } else {
                $('.back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('.back-to-top').click(function () {
            $('.back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('.back-to-top').tooltip('show');

});
    </script>

    <!-- for ekko lightbox  -->
    <script src="<?php echo site_url(); ?>assets/js/ekko-lightbox.js"></script>
    <script type="text/javascript">
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });
    </script>

    <!-- for flexslider -->
    <!-- FlexSlider -->
  <script defer src="<?php echo site_url(); ?>assets/js/jquery.flexslider.js"></script>

  <script type="text/javascript">
    $(function(){
      SyntaxHighlighter.all();
    });
    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>


  <!-- for message modal  -->


  <!-- Modal -->
  <div class="modal fade message_modal" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="clearfix member_box">
                <figure>
                    <img src="<?php echo site_url(); ?>assets/images/male.jpg" alt="Member">
                </figure>
                <figcaption>
                   Band Name Here
                </figcaption>
            </div>
        </div>
        <div class="modal-body">
          <form class="">
              <textarea class="form-control" placeholder="Write your message here..."></textarea>
              <div class="pull-right">
                  <input type="submit" name="submit" value="Send Message" class="profile_save_btn_small">
              </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


<!-- for date picker -->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="<?php echo site_url(); ?>assets/js/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker").datepicker({
       minDate: new Date('<?php echo date('Y'); ?>', '<?php echo date('m'); ?>' - 1,'<?php echo date('d'); ?>'),
    });
  });
  </script>
<!-- for header shrink -->
    <script type="text/javascript">
        $(document).on("scroll", function(){
        if
          ($(document).scrollTop() > 20){
              $(".main-header").addClass("shrink");
            }
            else
            {
                $(".main-header").removeClass("shrink");
            }
        });
    </script>
</body>
</html>

<base href="https://demos.telerik.com/kendo-ui/timepicker/rangeselection">
<style>html { font-size: 14px; font-family: Arial, Helvetica, sans-serif; }</style>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css" />
<script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
                
<script>
$(document).ready(function() {
function startChange() {
var startTime = start.value();

if (startTime) {
startTime = new Date(startTime);

end.max(startTime);

startTime.setMinutes(startTime.getMinutes() + this.options.interval);

end.min(startTime);
end.value(startTime);
}
}

//init start timepicker
var start = $("#start").kendoTimePicker({
change: startChange
}).data("kendoTimePicker");

//init end timepicker
var end = $("#end").kendoTimePicker().data("kendoTimePicker");

//define min/max range
start.min("12:00 AM");
start.max("11:30 PM");

//define min/max range
end.min("12:00 AM");
end.max("12:30 PM");
});
</script>