<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }
    protected $total_steps=1;
    protected function Send_Mail($to='',$from='',$from_name='',$subject='',$message='',$attachment='')
    {
        include_once APPPATH.'/libraries/sendgrid-php/vendor/autoload.php';
        $sendgrid = new SendGrid(SENDGRID_KEY);
        $emailbody   = new SendGrid\Email();
        $emailbody->addTo($to)
            ->setFrom($from)
            ->setReplyTo($from)
            ->setFromName($from_name)
            ->setSubject($subject)
            ->setHtml($message);
        if ($attachment != '') {

               $emailbody->setAttachment($attachment,'Showspoon.pdf');
            }  

        

       $sendgrid->send($emailbody);
    }
    protected function Artist_Session(){
        if (!$this->session->userdata('showspoon_artist')){
            redirect('account/login');
        }
        return $this->session->userdata('showspoon_artist');
    }
    protected function Artist_Profile(){
        if (!$this->session->userdata('showspoon_artist')){
            redirect('account/login');
        }
        $data =  $this->session->userdata('showspoon_artist');
        return isset($data['is_complete'])?$data['is_complete']:0;
    }
    protected function Venue_Session(){
        if (!$this->session->userdata('showspoon_venue')) {
            redirect('account/login');
        }
        return $this->session->userdata('showspoon_venue');
    }
    protected function Venue_Profile(){
        if (!$this->session->userdata('showspoon_venue')) {
            redirect('account/login');
        }
        $data =  $this->session->userdata('showspoon_venue');
        return isset($data['is_complete'])?$data['is_complete']:0;
    }
    protected function Admin_Session(){
        if (!$this->session->userdata('ShowspoonAdmin_logged_in')) {
            redirect('admin_login');
        }
        return $this->session->userdata('ShowspoonAdmin_logged_in');
    }
    protected function Artist_Session_Update($level=0)
    {
        $data  =$this->Artist_Session();
        if($data['is_complete']<$this->total_steps){
            $data = array_merge($data,['is_complete'=>$level]);
            $this->session->set_userdata('showspoon_artist',$data);
            //echo $level;
            $this->db->where('user_id',$data['user_id'])->update('artist',['is_complete'=>$level]);
        }
    }
    protected function Venue_Session_Update($level=0)
    {
        $data  =$this->Venue_Session();
        if($data['is_complete']<$this->total_steps){
            $data = array_merge($data,['is_complete'=>$level]);
            $this->session->set_userdata('showspoon_venue',$data);
            //echo $level;
            $this->db->where('user_id',$data['user_id'])->update('venues',['is_complete'=>$level]);
        }
    }
    protected function Get_Favourties(){
        
        if($this->session->userdata('showspoon_artist')){
            return user_likes('artist');
            exit();
        }elseif($this->session->userdata('showspoon_venue')){
            return user_likes('venue');
            exit();
        }
        
        redirect('account/login');
    }
}
