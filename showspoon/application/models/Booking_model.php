<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class booking_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->helper('site');
    }
    private $booking=['created_datetime','name','event_date','time','hours','message','who_pay','type','status','is_active','from_id','is_delete','to_id','amount','location'];
    
    private $booking_history=['created_datetime','event_date','time','hours','amount','book_id'];

    public function get_btn_status($where)
    {
        $this->db->where($where)->update('bookings',array('btn'=>'success'));
        $this->db->select('*');
        $this->db->where($where);
        $query = $this->db->get('bookings');
        return $query->row()->btn;
    }
    public function get_btnn_status($where=[])
    {
        $this->db->where($where)->update('bookings',array('btn'=>'success'));
        $this->db->select('*');
        $this->db->where($where);
        $query = $this->db->get('bookings');
        return $query->row()->btn;
    }
    public function get_venue_status($id)
    {
        $query = $this->db->query("select * from bookings where from_id=$id and is_complete=1");
        return $query->row();
    }
    public function get_artist_status($id)
    {
        $query = $this->db->query("select * from artist where user_id = $id");
        return $query->result();
    }
    public function get_artist_bookings($data=[]) {
        
        $where='';
        if(isset($data['from_id']) && is_numeric($data['from_id'])) {
            $where .= " and from_id=".(int)$data['from_id']." ";
        }
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " and to_id=".(int)$data['to_id']." ";
        }

        if(isset($data['is_complete']) && is_numeric($data['is_complete'])) {
            $where .= " and B.is_complete=".(int)$data['is_complete']." ";
        }

        $query=$this->db->query("select B.*,A.artist_id as artist_id,A.name as name,A.profile_image,A.genre,A.new as new,A.user_id as user_id from bookings B
        
        inner join artist A on (A.user_id = B.from_id  || A.user_id = B.to_id )
        where B.is_delete=0 and B.is_active=1 $where order by B.id DESC"); 
        return $query->result(); 
    }
    public function profile_one($id)
    {
        $query = $this->db->query("select id as id from bookings where from_id=".(int)$id." order by to_id desc");
        return $query->row(); 
    }
    public function profile_two($id)
    {
        $query = $this->db->query("select to_id as id from bookings where from_id=".(int)$id." and is_complete=1 order by to_id desc");
        return $query->row();
    }
    public function check_venue_today_date($id,$event_date)
    {
        $this->db->select('*');
        $this->db->where(array('from_id'=>$id,'event_date'=>$event_date));
        $this->db->order_by('event_date','DESC');
        $query = $this->db->get('bookings');
        return @$query->row()->event_date;
    }
    public function check_artist_today_date($id,$event_date)
    {
        $this->db->select('*');
        $this->db->where(array('from_id'=>$id,'event_date'=>$event_date));
        $this->db->order_by('event_date','DESC');
        $query = $this->db->get('bookings');
        return @$query->row()->event_date;
    }
    public function get_artist_calendar($id=0)
    {
        $data = array();
        // $query = $this->db->query("select * from bookings where from_id=$id");
        // foreach($query->result() as $row){
        //     $to_id = $row->to_id;
        //     $sql = $this->db->query("select B.*,A.artist_id as artist_id,A.name as name,A.profile_image,A.genre,A.new as new,A.user_id as user_id from bookings B
        
        // inner join artist A on (A.user_id = B.to_id)
        // where B.is_delete=0 and B.is_active=1 and B.from_id=$id and B.to_id=$to_id and B.is_complete=1 order by B.id DESC");
        //     foreach($sql->result() as $item){
        //         $date = explode('/',$item->event_date); 
        //         $data[] = array(
        //                            'id' => $item->user_id,
        //                            'title'=>$item->name,
        //                            'start'=>$date[2].'/'.$date[0].'/'.$date[1].' '.$item->start_time
        //                        );
        //     }
        // }
        // select B.*, A.artist_id as artist_id,A.name as name,A.user_id as user_id from bookings B inner join artist A on A.user_id=B.to_id where B.event_date=$event_date and B.from_id=$id

            $sql = $this->db->query("select * from bookings B inner join artist A on A.user_id=B.to_id where B.from_id=$id and B.btn='paid'");
            foreach($sql->result() as $item){
                $date = explode('/',$item->event_date); 
                $data[] = array(
                                   'id' => $item->id,
                                    'title'=>$item->name,
                                   'start'=>$date[2].'/'.$date[0].'/'.$date[1].' '.$item->start_time,
                                   'end'=>$date[2].'/'.$date[0].'/'.$date[1].' '.$item->end_time
                               );
            }
        return $data;
    }

    public function get_venue_calendar($id=0)
    {
        // $data = array();
        // $query = $this->db->query("select * from bookings where from_id=$id");
        // foreach($query->result() as $row){
        //     $to_id = $row->to_id; 
        //     $sql = $this->db->query("select B.*,V.venue_id as venue_id,V.name as name,V.profile_image,V.genre,V.new as new,V.user_id as user_id from bookings B
        
        // inner join venues V on (V.user_id = B.to_id )
        // where B.is_delete=0 and B.is_active=1 and B.from_id=$id and B.to_id=$to_id and B.is_complete=1 order by B.id DESC");
        //     foreach($sql->result() as $item){
        //         $date = explode('/',$item->event_date);
        //         $time = explode(' ',$item->start_time); 
        //         $data[] = array(
        //                            'id' => $item->user_id,
        //                            'title'=>$item->name,
        //                            'start'=>$date[2].'/'.$date[0].'/'.$date[1].' '.$item->start_time
        //                        );
        //     }
        // }
        // return $data;
        $sql = $this->db->query("select * from bookings B inner join venues V on V.user_id=B.to_id where B.from_id=$id and B.btn='paid'");
            foreach($sql->result() as $item){
                $date = explode('/',$item->event_date); 
                $data[] = array(
                                   'id' => $item->id,
                                   'title'=>$item->name,
                                   'start'=>$date[2].'/'.$date[0].'/'.$date[1].' '.$item->start_time
                               );
            }
        return $data;
    }

    public function get_calendar_profile($id,$user_id)
    {
        // $query = $this->db->query("select * from bookings where from_id=$id");
        // foreach($query->result() as $row){
        //     $to_id = $row->to_id;
        //     $sql = $this->db->query("select artist.name as artist_name,artist.profile_image as profile_image,artist_band_type.name as name,artist_details.capacity as capacity,bookings.created_datetime as datetime from artist inner join artist_details on artist_details.artist_id=artist.artist_id inner join artist_band_type on artist_band_type.id=artist.band_type_id inner join bookings on bookings.to_id=artist.user_id where bookings.is_delete=0 and bookings.is_active=1 and bookings.from_id=$id and bookings.is_complete=1 order by bookings.id DESC");
        // }
        // return $sql->row();
        // $query = $this->db->query("select artist.name as artist_name,artist.profile_image as profile_image,artist_band_type.name as name,artist_details.capacity as capacity,bookings.created_datetime as datetime,bookings.id as book_id,transaction.amount as amount from artist inner join artist_details on artist_details.artist_id=artist.artist_id inner join artist_band_type on artist_band_type.id=artist.band_type_id inner join bookings on bookings.to_id=artist.user_id inner join transaction on transaction.booking_id=bookings.id where bookings.is_delete=0 and bookings.is_active=1 and bookings.from_id=$user_id and bookings.to_id=$id and bookings.is_complete=1 order by bookings.id DESC");
        $query = $this->db->query("select A.name as artist_name,A.profile_image as profile_image,AD.capacity as capacity,B.created_datetime as datetime,B.start_time as start_time, B.end_time as end_time,B.message as message, B.id as book_id,B.amount as amount,AB.name as name from bookings B inner join artist A on A.user_id=B.to_id inner join artist_details AD on AD.artist_id=A.artist_id inner join artist_band_type AB on AB.id=A.band_type_id where B.id=$id and B.btn='paid'");
        return $query->row();
    }
    public function get_calendar_profilee($id,$user_id)
    {
        $query = $this->db->query("select V.name as venue_name,V.profile_image as profile_image,VD.capacity as capacity,B.created_datetime as datetime,B.event_date as event_date,B.start_time as start_time,B.end_time as end_time,B.message as message,B.id as book_id,B.amount as amount  from bookings B inner join venues V on V.user_id=B.to_id inner join venue_details VD on VD.venue_id=V.venue_id where B.from_id=$user_id and B.id=$id and B.btn='paid' order by B.id desc");
        return $query->row();
    }
    public function view_profile($id)
    {
        // $this->db->select('*');
        // $this->db->from('venues');
        // $this->db->join('venue_details','venue_details.venue_id = venues.venue_id');
        // $this->db->where('venues.user_id',$id);
        // $query = $this->db->get();
        // return $query->row();
        $query = $this->db->query("select * from bookings B inner join venues V on V.user_id=B.to_id inner join venue_details VD on VD.venue_id=V.venue_id where B.id=$id");
        return $query->row();
    }
    public function view_profilee($id)
    {
        // $this->db->select('*');
        // $this->db->from('artist');
        // $this->db->join('artist_details','artist_details.artist_id = artist.artist_id');
        // $this->db->where('artist.user_id',$id);
        // $query = $this->db->get();
        $query = $this->db->query("select * from bookings B inner join artist A on A.user_id=B.to_id inner join artist_details AD on AD.artist_id=A.artist_id where B.id=$id");
        return $query->row();
    }
    public function paymentBtnAction($id=0){
        $this->db->select('status');
        $this->db->where(['user_id'=>$id]);
        $sql = $this->db->get('transaction');
        return $sql->row()->status;
    }
    public function get_artist_booking($data=[])
    {
        $where='';
        if(isset($data['from_id']) && is_numeric($data['from_id'])) {
            $where .= " and B.from_id=".(int)$data['from_id']." ";
        }
        /*if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " and to_id=".(int)$data['to_id']." ";
        }*/
        // if(isset($data['type']) && is_numeric($data['type'])) {
        //     $where .= " and type=".(int)$data['type']." ";
        // }
        
        $query=$this->db->query("select B.*,A.artist_id as artist_id,A.user_id as user_id,A.name as name,A.profile_image,A.genre,A.new as new,AD.mobile as mobile,AD.artist_rate as artist_rate,AD.artist_currency as artist_currency from bookings B
        
        inner join artist A on (A.user_id = B.from_id  || A.user_id = B.to_id )
        inner join artist_details AD on (AD.artist_id = A.artist_id)
        where B.is_delete=0 $where order by id DESC");
        //inner join venues V on V.user_id = B.to_id   
        return $query->result();
    }
    public function get_artist_messages($id=0,$start,$limit){
        $sql = $this->db->query("select * from chat left join artist on artist.user_id=chat.to where chat.from=$id limit $start , $limit");
        return $sql->result();
    }
    public function get_venue_message($id=0,$start,$limit){
         $sql = $this->db->query("select * from chat left join venues on venues.user_id=chat.to where chat.from=$id order by chat.id desc limit $start , $limit");
         return $sql->result();
    }
    public function getVenueReqCount($id=0){
        $sql = $this->db->query("select * from chat where chat.from=".(int)$id."");
        return $sql->num_rows();
    }
    public function getArtistReqCount($id=0){
        $sql = $this->db->query("select * from chat where chat.from=".(int)$id."");
        return $sql->num_rows();
    }
    public function chat_venue_received($id=0,$limit,$offset){
        $where = '';
        // $query = $this->db->query("select * from chat where chat.from=".(int)$id." order by chat.id desc");
        // foreach($query->result() as $key=>$item){  
        // if($key==0){
        //     $where .= " C.to= ".$item->to;
        // }else{
        //     $where .= " or C.to= ".$item->to;
        // }
        // }
        $sql  = $this->db->query("select C.*,A.name as artist_name,V.name as venue_name,A.profile_image as image,A.user_id as artist_id,V.user_id as venue_id from chat C left join artist A on A.user_id=C.from left join venues V on V.user_id=C.from where C.to=$id and C.is_delete=0 order by C.id desc limit $limit , $offset");
        $received = $sql->result();
        return $received;
    }
    public function chat_artist_received($id=0,$limit,$offset){
        $sql  = $this->db->query("select C.*,V.name as venue_name,A.name as artist_name,V.profile_image as image,A.user_id as artist_id,V.user_id as venue_id from chat C left join artist A on A.user_id=C.from left join venues V on V.user_id=C.from where C.to=$id and C.is_delete=0 order by C.id desc limit $limit , $offset");
        $received = $sql->result();
        return $received;
    }
    // public function get_n($id=0,$limit,$offset){
    //     $sql  = $this->db->query("select * from chat where chat.to=$id");
    //     foreach($sql->result() as $item){
    //         $from = $item->from;
    //         $sql  = $this->db->query("select C.*,COUNT(C.to) as c , V.name as name from chat C inner join venues V on V.user_id=C.from where C.from=$from order by C.id desc limit $limit , $offset");
    //         $received = $sql->result();
    //         return $received;
    //     }
    // }
    public function chat_venue_count($id=0){
        $this->db->select('*');
        $this->db->where('to',$id);
        $sql = $this->db->get('chat');
        return $sql->num_rows();
    }
    public function inboxVenueCount($id=0){
        $sql = $this->db->query("select COUNT(*) AS count from chat C where C.to='$id' and C.is_delete=0 ");
        return $sql->row();
    }

      public function inboxVenueNewCount($id=0){
        $sql = $this->db->query("select COUNT(*) AS count from chat C where C.to='$id' and C.is_delete=0 and C.is_read=0");
        return $sql->row();
    }
    public function inboxArtistCount($id=0){
        $sql = $this->db->query("select count(*) AS count from chat C where C.to=$id and C.is_delete=0 and C.is_read=0");
        return $sql->row();
    }
    public function trashArtistCount($id=0){
        $sql = $this->db->query("select count(*) as count from chat C where C.to=$id and is_delete=1");
        return $sql->row();
    }
    public function trashVenueCount($id=0){
        $sql = $this->db->query("select count(*) as count from chat C where C.from=$id and is_delete=1");
        return $sql->row();
    }
    public function trash_artist($id=0,$limit,$offset){
        $sql  = $this->db->query("select C.*,V.name as venue_name from chat C left join venues V on V.user_id=C.from where C.to=$id and C.is_delete=1 order by C.id desc limit $limit , $offset");
        $received = $sql->result();
        return $received;
    }
    public function trash_venue($id=0,$limit,$offset){
        // $where = '';
        // $query = $this->db->query("select * from chat where chat.from=".(int)$id." order by chat.id desc");
        // foreach($query->result() as $key=>$item){  
        // if($key==0){
        //     $where .= " C.to= ".$item->to;
        // }else{
        //     $where .= " or C.to= ".$item->to;
        // }
        // }
        $sql  = $this->db->query("select C.*,A.name as artist_name from chat C left join artist A on A.user_id=C.from where C.to=$id and C.is_delete=1 order by C.id desc limit $limit , $offset");
        $received = $sql->result();
        return $received;
    }
    public function getVenueName()
    {
        $this->db->select('*');
        $sql = $this->db->get('venues');
        return $sql->result();
    }
    public function getArtistName()
    {
        $this->db->select('*');
        $sql = $this->db->get('artist');
        return $sql->result();
    }
    public function convertCurrency($amount, $from, $to){
    @$conv_id = "{$from}_{$to}";
    $string = @file_get_contents("http://free.currencyconverterapi.com/api/v3/convert?q=$conv_id&compact=ultra");
    $json_a = @json_decode($string, true);

    return @$amount * round($json_a[$conv_id], 2);
    }
     public function get_artists_booking($data=[],$start,$limit,$user_id){
        $this->load->model('admin_model');
        $com = $this->admin_model->getCommission();
        $where='';
        $output = '';
        // if(isset($data['from_id']) && is_numeric($data['from_id'])) {
        //     $where .= " and B.from_id=".(int)$data['from_id']." ";
        // }
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " B.to_id=".(int)$data['to_id']." ";
        }
         if(isset($data['type']) && is_numeric($data['type'])) {
             $where .= " and B.type=".(int)$data['type']." ";
         }
        
        // echo 'where:'.$where;
        // exit;
         $query = "select B.*,A.artist_id as artist_id,A.name as name,A.profile_image,A.genre,A.new as new,A.user_id as user_id,AD.mobile as mobile,AD.artist_rate as artist_rate,AD.artist_currency as artist_currency from bookings B
        
        inner join artist A on (A.user_id = B.from_id )
        inner join artist_details AD on (AD.artist_id = A.artist_id)
        where $where order by B.id DESC limit $start , $limit";

        $query=$this->db->query($query);
        //inner join venues V on V.user_id = B.to_id   
        $received = $query->result(); 
        $sql = $this->db->query("select * from venues inner join venue_details on venue_details.venue_id = venues.venue_id where venues.user_id = $user_id");
        $currency = $sql->row()->venue_currency;
        if(count($received) > 0){ foreach($received as $row){ 
            $pay = explode('.',$row->amount); 
            if($row->status==1){
                $active_class = "bold_class";
              }else{
                $active_class = "light_class";
              }
            $output  .='<div class="req_parent_row clearfix get_image" id="remove'.$row->artist_id.'">
                      <div class="req_col_badge '.$active_class.'">
                      <img src="'.site_url().'uploads/users/thumb/'.$row->profile_image.'" class="img-circle" style="width:40px;height:40px;margin-left:10px;" id="view-details" data-id="'.$row->id.'">
                        <span class=" gold-badge" id="label'.$row->artist_id.'"></span>
                      </div>
                      <div class="req_col_title '.$active_class.'">
                        '.$row->name.'
                      </div>
                      <div class="clearfix-sm"></div>
                      <div class="req_col_date '.$active_class.'">
                        '.$row->event_date.'
                      </div>
                      <div class="req_col_time '.$active_class.'">
                        '.$row->start_time.'-'.$row->end_time.'
                      </div>
                      <div class="clearfix-sm"></div>
                      <div style="cursor:pointer;" class="req_col_detail hidden-sm hidden-xs '.$active_class.'" id="view-details" data-id="'.$row->id.'" data-toggle="collapse" data-target="#collapsable_row_one'.$row->id.'">
                        <i class="fa fa-caret-right" aria-hidden="true"></i> view details
                      </div>
                      <div class="req_col_decision text-left">';
        
        if($row->btn=="pending"){
        $output .= '<div class="float-left mr-2 req_badge_wid">
                            <span class="m-badge  m-badge--pending m-badge--wide mt-5p">Pending</span>
                          </div>';
                  }else{ if($row->btn=="success"){
        $output .=   '<div class="float-left mr-2 mt-5p req_badge_wid">
                          <span class="m-badge  m-badge--awaitingpayment m-badge--wide">
                          Awaiting Payment
                          </span>
                        </div>';
                    }else{
                      $output .= '<span class="m-badge  m-badge--cancelled m-badge--wide" style="float:left;">
                                  Canceled
                                 </span>';  
                    }
                  }
                  
            $output .= '
            <div class="clearfix-sm"></div>
                      </div>';
            $output .= '<div class="req_col_decision_2">';
            if($row->btn=="pending"){
                $output .= '<div class="clearfix"><div class="float-left">';
                $output .=   '<button class="btn btn-sm btn-warning status" data-new="'.$user_id.'" data-artist="'.$row->artist_id.'" id="'.$row->id.'" data-status="'.$row->id.'" data-user="'.$row->user_id.'" data-name="'.$row->name.'">
                      <span class="ok'.$row->artist_id.'"></span> accept
                      </button><span class="tmp-btn'.$row->artist_id.'"></span>
                      <button class="btn btn-danger btn-sm cancel" id="'.$row->id.'" data-toggle="modal" data-target="#myModal">Decline</button>';
            }else if($row->btn=="paid"){
                         $output .= '<div class="float-left mr-2 mt-5p req_badge_wid"><span class="m-badge  m-badge--success m-badge--wide" style="float:left;">
                            Booked
                            </span></div>
                            <button type="button" class="btn-danger btn-sm float-left btn btn-wid-sixty cancel" data-artist="'.$row->artist_id.'" id="'.$row->id.'" data-toggle="modal" data-target="#myModal">Decline</button>';
                    }
            $output .= '</div>';
            $output .= '</div><div class="clearfix-sm"></div>';
            $output .= '</div>';
            $output .= '<div class="clearfix"></div>
                     <div style="cursor:pointer;" class="req_col_detail  hidden-md hidden-lg '.$active_class.'" id="view-details" data-id="'.$row->id.'" data-toggle="collapse" data-target="#collapsable_row_one'.$row->id.'">
                        <i class="fa fa-caret-right" aria-hidden="true"></i> view details
                      </div>                     
                    </div>

                    <div class="clearfix"></div>
                      <div id="collapsable_row_one'.$row->id.'" class="collapse">
                        <div class="req_parent_row_collapse">
                          <div class="row">
                            <div class="col-sm-4">
                              <div class="event_info_card">
                                <h3>information</h3>
                                <ul>
                                  <li>Performance time: <strong>'.$row->start_time.'</strong></li>
                                  <li>Set length: <strong>'.$row->start_time.' - '.$row->end_time.'</strong></li>
                                  <li>Artist Amount: <strong>'.$this->convertCurrency($row->artist_rate, $row->artist_currency, $currency).' '.$currency.'</strong></li>
                                  <li>Charge Fee: <strong>Charge fee for '.$com->commission.'% is '.$this->convertCurrency($row->artist_rate, $row->artist_currency, $currency) / 100 * $com->commission.' '.$currency.'</strong></li>
                                  <li>Total: <strong>$'.$pay[0].' '.$currency.'</strong></li>
                                </ul>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="req_parent_row_collapse_space"></div>
                              
                              <div class="spacer_medium">
                                <h3>address</h3>
                                <p>
                                 '.$row->location.'
                                </p>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="req_parent_row_collapse_space"></div>
                              <div class="spacer_medium">
                                <h3>notes</h3>
                                <p class="smallpara">
                                  '.$row->message.'
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>';
                     } }else{$output .='<p style="text-align:center;">No requests found</p>';}
         return $output;

    }
    public function artists_requests_count($data=[])
    {
        $where='';
        if(isset($data['from_id']) && is_numeric($data['from_id'])) {
            $where .= " and B.from_id=".(int)$data['from_id']." ";
        }
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " and B.to_id=".(int)$data['to_id']." ";
        }
        if(isset($data['type']) && is_numeric($data['type'])) {
            $where .= " and B.type=".(int)$data['type']." ";
        }
        
        $query=$this->db->query("select B.*,A.artist_id as artist_id,A.name as name,A.profile_image,A.genre,A.new as new from bookings B
        
        inner join artist A on (A.user_id = B.from_id  || A.user_id = B.to_id )
        where B.is_delete=0 and B.is_active=1 and decline=0 $where order by B.id DESC");
        //inner join venues V on V.user_id = B.to_id   
        return $query->num_rows();

    }
    public function get_artist_decline_by_id($data=[])
    {
        $where='';
        if(isset($data['from_id']) && is_numeric($data['from_id'])) {
            $where .= " and from_id=".(int)$data['from_id']." ";
        }
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " and to_id=".(int)$data['to_id']." ";
        }
        if(isset($data['type']) && is_numeric($data['type'])) {
            $where .= " and type=".(int)$data['type']." ";
        }
        
        $query=$this->db->query("select B.*,A.artist_id as artist_id,A.name as name,A.profile_image,A.genre,A.new as new from bookings B
        
        inner join artist A on (A.user_id = B.from_id  || A.user_id = B.to_id )
        where B.is_delete=0 and B.is_active=1 and A.decline=1 $where order by id DESC");
        //inner join venues V on V.user_id = B.to_id 
        return $query->result();
    }
    public function get_venue_decline_by_id($data=[])
    {
        $where='';
        if(isset($data['from_id']) && is_numeric($data['from_id'])) {
            $where .= " and from_id=".(int)$data['from_id']." ";
        }
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " and to_id=".(int)$data['to_id']." ";
        }
        if(isset($data['type']) && is_numeric($data['type'])) {
            $where .= " and type=".(int)$data['type']." ";
        }
        
        $query=$this->db->query("select B.*,V.venue_id as venue_id,V.name as name,V.profile_image,V.genre,V.new as new from bookings B
        
        inner join venues V on (V.user_id = B.from_id  || V.user_id = B.to_id )
        where B.is_delete=0 and B.is_active=1 and V.decline=1 $where order by id DESC");
        //inner join venues V on V.user_id = B.to_id 
        return $query->result();
    }
    public function get_artist_bookings_count($data=[]) {
        $where='';
        if(isset($data['event_date']) && is_numeric($data['event_date'])) {
            $where .= " and event_date=".(int)$data['event_date']." ";
        }
        if(isset($data['hours']) && is_numeric($data['hours'])) {
            $where .= " and hours=".(int)$data['hours']." ";
        }
        if(isset($data['from_id']) && is_numeric($data['from_id'])) {
            $where .= " and from_id=".(int)$data['from_id']." ";
        }
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " and to_id=".(int)$data['to_id']." ";
        }
        if(isset($data['type']) && is_numeric($data['type'])) {
            $where .= " and type=".(int)$data['type']." ";
        }
        if(isset($data['is_complete']) && is_numeric($data['is_complete'])) {
            $where .= " and B.is_complete=".(int)$data['is_complete']." ";
        }


        $query=$this->db->query("select count(id) as total from bookings B
        
        inner join artist A on (A.user_id = B.from_id  || A.user_id = B.to_id )
        where B.is_delete=0 and B.is_active=1 $where ");//inner join venues V on V.user_id = B.to_id 

        $row = $query->row();
        return isset($row->total)?$row->total:0;
    }
    public function get_artist_booking_count($id=0)
    {
        $query = $this->db
                 ->select('COUNT(id) AS total')
                 ->from('bookings')
                 ->where(array('from_id'=>$id,'is_delete'=>0,'is_active'=>1))
                 ->get();
        return $query->row();
    }
    public function venueDecline($id){
        $this->db->where('id',$id)->update('bookings',array('btn'=>'cancelled'));
    }
    public function artistDecline($id){
        $this->db->where('id',$id)->update('bookings',array('btn'=>'cancelled'));
    }


    public function get_venue_bookings($data=[]) {
        $where='';
        // if(isset($data['event_date']) && is_numeric($data['event_date'])) {
        //     $where .= " and event_date=".(int)$data['event_date']." ";
        // }
        // if(isset($data['hours']) && is_numeric($data['hours'])) {
        //     $where .= " and hours=".(int)$data['hours']." ";
        // }
        if(isset($data['from_id']) && is_numeric($data['from_id'])) {
            $where .= " and from_id=".(int)$data['from_id']." ";
        }
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " and to_id=".(int)$data['to_id']." ";
        }
        if(isset($data['type']) && is_numeric($data['type'])) {
            $where .= " and type=".(int)$data['type']." ";
        }
        if(isset($data['is_complete']) && is_numeric($data['is_complete'])) {
            $where .= " and B.is_complete=".(int)$data['is_complete']." ";
        }


        $query=$this->db->query("select B.*,V.name as name,V.profile_image,V.genre,V.venue_id as venue_id,V.user_id as user_id from bookings B
        inner join venues V on (V.user_id = B.from_id || V.user_id = B.to_id )
        where B.is_delete=0 and B.is_active=1 $where order by B.id DESC");
        //inner join artist A on A.user_id = B.to_id 
        return $query->result();
    }
    public function get_venue_booking($data=[])
    {
        $where='';
        if(isset($data['from_id']) && is_numeric($data['from_id'])) {
            $where .= " and from_id=".(int)$data['from_id']." ";
        }
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " and to_id=".(int)$data['to_id']." ";
        }
        if(isset($data['type']) && is_numeric($data['type'])) {
            $where .= " and type=".(int)$data['type']." ";
        }

        $query=$this->db->query("select B.*,V.venue_id as venue_id,V.name as name,V.profile_image,V.genre,V.location as location,V.new as new,V.user_id as user_id,VD.mobile as mobile,VD.venue_rate as venue_rate,VD.venue_currency as venue_currency from bookings B  
        inner join venues V on (V.user_id = B.from_id || V.user_id = B.to_id )  
        inner join venue_details VD on (VD.venue_id=V.venue_id)
        where B.is_delete=0 and B.is_active=1 and decline=0 $where order by id DESC");
        //inner join artist A on A.user_id = B.to_id 
        return $query->result();
    }
    public function venue_booked($where)
    {
        $this->db->where($where)->update('bookings',array('is_complete'=>1));
    }
    public function artist_booked($where)
    {
        $this->db->where($where)->update('bookings',array('is_complete'=>1));
    }

    public function get_venues_booking($data=[],$start,$limit,$user_id){
        $this->load->model('admin_model');
        $com = $this->admin_model->getCommission();
        $where='';
        $output = '';
        /*if(isset($data['from_id']) && is_numeric($data['from_id'])) {
            $where .= " and from_id=".(int)$data['from_id']." ";
        }*/
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " to_id=".(int)$data['to_id']." ";
        }
        if(isset($data['type']) && is_numeric($data['type'])) {
            $where .= " and type=".(int)$data['type']." ";
        }

        $query=$this->db->query("select B.*,V.venue_id as venue_id,V.name as name,V.profile_image,V.genre,V.location as location,V.new as new,V.user_id as user_id,VD.mobile as mobile,VD.venue_rate as venue_rate,VD.venue_currency as venue_currency from bookings B
        inner join venues V on (V.user_id = B.from_id || V.user_id = B.to_id ) 
        inner join venue_details VD on (VD.venue_id = V.venue_id)
        where $where order by id DESC limit $start , $limit");
        //inner join artist A on A.user_id = B.to_id 
        $received = $query->result();
        $sql = $this->db->query("select * from artist inner join artist_details on artist_details.artist_id = artist.artist_id where artist.user_id = $user_id");
        $currency = $sql->row()->artist_currency;
        if(count($received) > 0){ foreach($received as $row){ 
            $pay = explode('.',$row->amount); 
            if($row->status==1){
                $active_class = "bold_class";
            }else{
                $active_class = "light_class";
            }
            $output  .='<div class="req_parent_row clearfix get_image" id="remove'.$row->venue_id.'">
                      <div class="req_col_badge '.$active_class.'">
                      <img src="'.site_url().'uploads/users/thumb/'.$row->profile_image.'" class="img-circle" style="width:40px;height:40px;margin-left:10px;" id="view-details" data-id="'.$row->id.'">
                        <span class=" gold-badge" id="label'.$row->venue_id.'"></span>
                      </div>
                      <div class="req_col_title '.$active_class.'">
                        '.$row->name.'
                      </div>
                      <div class="req_col_date '.$active_class.'">
                        '.$row->event_date.'
                      </div>
                      <div class="req_col_time '.$active_class.'">
                        '.$row->start_time.'-'.$row->end_time.'
                      </div>
                      <div class="clearfix-sm"></div>
                      <div style="cursor:pointer;" class="req_col_detail '.$active_class.'" id="view-details" data-id="'.$row->id.'" data-toggle="collapse" data-target="#collapsable_row_one'.$row->id.'">
                        <i class="fa fa-caret-right" aria-hidden="true"></i> view details
                      </div>
                      <div class="req_col_decision">';
                      if($row->btn=="pending"){
        $output .= '<div class="float-left mr-2 req_badge_wid text-left">
                            <span class="m-badge  m-badge--pending m-badge--wide mt-5p">Pending</span>
                          </div>';
       
                  }else{ if($row->btn=="success"){
        $output .= '<div class="float-left mr-2 mt-5p req_badge_wid">
                          <span class="m-badge  m-badge--awaitingpayment m-badge--wide">
                          Awaiting Payment
                          </span>
                        </div>';
                    }else{
                      $output .= '<span class="m-badge  m-badge--cancelled m-badge--wide" style="float:left;">
                                  Canceled
                                 </span>';  
                    }
                  }
            $output .= '<div class="clearfix-sm"></div>
               </div>';
            $output .= '<div class="req_col_decision_2">';
                        if($row->btn=="pending"){
            $output .= '<div class="clearfix"><div class="float-left">';
            $output .= '<button class="btn btn-sm btn-warning status" data-new="'.$user_id.'" id="'.$row->id.'" data-status="'.$row->id.'" data-user="'.$row->user_id.'" data-name="'.$row->name.'">
                      <span class="ok'.$row->venue_id.'"></span> accept
                      </button><span class="tmp-btn'.$row->venue_id.'"></span>';
            $output .= '</div><button class="btn btn-sm btn-danger cancel" id="'.$row->id.'" data-venue="'.$row->venue_id.'" data-toggle="modal" data-target="#myModal">Decline</button>';
                      }else if($row->btn=="paid"){
                         $output .= '<div class="float-left mr-2 mt-5p req_badge_wid"><span class="m-badge  m-badge--success m-badge--wide" style="float:left;">
                            Booked
                            </span></div>
                            <button type="button" class="btn-danger btn-sm float-left btn btn-wid-sixty cancel" data-venue="'.$row->venue_id.'" id="'.$row->id.'" data-toggle="modal" data-target="#myModal">Decline</button>';
                    }                     
            $output .= '</div>';
            $output .= '</div><div class="clearfix-sm"></div>';
            $output .= '</div>';
            $output .= '<div class="clearfix"></div>
            <div style="cursor:pointer;" class="req_col_detail  hidden-md hidden-lg '.$active_class.'" id="view-details" data-id="'.$row->id.'" data-toggle="collapse" data-target="#collapsable_row_one'.$row->id.'">
                        <i class="fa fa-caret-right" aria-hidden="true"></i> view details
                      </div>                     
                    </div>

                    <div class="clearfix"></div>
                      <div id="collapsable_row_one'.$row->id.'" class="collapse">
                        <div class="req_parent_row_collapse">
                          <div class="row">
                            <div class="col-sm-4">
                              <div class="event_info_card">
                                <h3>information</h3>
                                <ul>
                                  <li>Performance time: <strong>'.$row->start_time.'</strong></li>
                                  <li>Set length: <strong>'.$row->start_time.' - '.$row->end_time.'</strong></li>
                                  <li>Venue Amount: <strong>'.$this->convertCurrency($row->venue_rate, $row->venue_currency, $currency).' '.$currency.'</strong></li>
                                  <li>Charge Fee: <strong>Charge fee for '.$com->commission.'% is '.$this->convertCurrency($row->venue_rate, $row->venue_currency, $currency) / 100 * $com->commission.' '.$currency.'</strong></li>
                                  <li>Total: <strong>'.$pay[0].' '.$currency.'</strong></li>
                                </ul>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="spacer_medium">
                                <h3>address</h3>
                                <p>
                                 '.$row->location.'
                                </p>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="req_parent_row_collapse_space"></div>
                              <div class="spacer_medium">
                                <h3>notes</h3>
                                <p class="smallpara">
                                  '.$row->message.'
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>';
                     } }else{$output .='<p style="text-align:center;">No requests found</p>';}
         return $output;
    }
    public function venues_requests_count($data=[])
    {
        $where='';
        if(isset($data['from_id']) && is_numeric($data['from_id'])) {
            $where .= " and from_id=".(int)$data['from_id']." ";
        }
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " and to_id=".(int)$data['to_id']." ";
        }
        if(isset($data['type']) && is_numeric($data['type'])) {
            $where .= " and type=".(int)$data['type']." ";
        }

        $query=$this->db->query("select B.*,V.venue_id as venue_id,V.name as name,V.profile_image,V.genre,V.location as location,V.new as new,V.user_id as user_id from bookings B
        inner join venues V on (V.user_id = B.from_id || V.user_id = B.to_id ) 
        where B.is_delete=0 and B.is_active=1 and decline=0 $where order by id DESC");
        //inner join artist A on A.user_id = B.to_id 
        return $query->num_rows();
    }
    public function hide($data=[])
    {   
        $where = '';
        if($data['type']==2){
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " and to_id=".(int)$data['to_id']." ";
        }
        if(isset($data['type']) && is_numeric($data['type'])) {
            $where .= " and type=".(int)$data['type']." ";
        }
        $query=$this->db->query("select B.*,V.venue_id as venue_id,V.name as name,V.profile_image as profile_image,V.genre,V.location as location,V.user_id as user_id from bookings B
        inner join venues V on (V.user_id = B.from_id || V.user_id = B.to_id ) 
        where B.is_delete=0 and B.is_active=1 $where order by id DESC"); 
    }
    else{
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " and to_id=".(int)$data['to_id']." ";
        }
        if(isset($data['type']) && is_numeric($data['type'])) {
            $where .= " and type=".(int)$data['type']." ";
        }
        $query=$this->db->query("select B.*,A.artist_id as artist_id,A.name as name,A.profile_image as profile_image,A.genre,A.user_id as user_id from bookings B inner join artist A on (A.user_id = B.from_id  || A.user_id = B.to_id ) where B.is_delete=0 and B.is_active=1 $where order by id DESC");

    }  
        return $query->row();
    }
    public function artist_booked_image($data=[])
    {   
        $where = '';
        if($data['type']==2){
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " where user_id=".(int)$data['to_id']." ";
        }
        // if(isset($data['type']) && is_numeric($data['type'])) {
        //     $where .= " and type=".(int)$data['type']." ";
        // }
        $query=$this->db->query("select * from venues $where "); 
    }
    else{
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " where user_id=".(int)$data['to_id']." ";
        }
        // if(isset($data['type']) && is_numeric($data['type'])) {
        //     $where .= " and type=".(int)$data['type']." ";
        // }
        $query=$this->db->query("select * from artist $where ");

    }  
        return $query->row();
    }
    public function get_venue_image($id=0){
        $query = $this->db->query("select * from bookings inner join venues on (venues.user_id = bookings.from_id || venues.user_id = bookings.to_id) where bookings.id=$id");
        return $query->row();
    }
    public function get_artist_image($id){

        $query = $this->db->query("select * from bookings inner join artist on (artist.user_id = bookings.from_id || artist.user_id = bookings.to_id) where bookings.id=$id");
        return $query->row();
    }
    public function badge($id)
    {
        $this->db->where('artist_id',$id)->update('artist',array('new'=>''));
    }
    public function badgee($id)
    {
        $this->db->where('venue_id',$id)->update('venues',array('new'=>''));
    }
    public function get_venue_bookings_count($data=[]) {
        $where='';
        if(isset($data['event_date']) && is_numeric($data['event_date'])) {
            $where .= " and event_date=".(int)$data['event_date']." ";
        }
        if(isset($data['hours']) && is_numeric($data['hours'])) {
            $where .= " and hours=".(int)$data['hours']." ";
        }
        if(isset($data['from_id']) && is_numeric($data['from_id'])) {
            $where .= " and from_id=".(int)$data['from_id']." ";
        }
        if(isset($data['to_id']) && is_numeric($data['to_id'])) {
            $where .= " and to_id=".(int)$data['to_id']." ";
        }
        if(isset($data['type']) && is_numeric($data['type'])) {
            $where .= " and type=".(int)$data['type']." ";
        }
        if(isset($data['is_complete']) && is_numeric($data['is_complete'])) {
            $where .= " and B.is_complete=".(int)$data['is_complete']." ";
        }

        $query=$this->db->query("select count(id) as total from bookings B
        inner join venues V on (V.user_id = B.from_id || V.user_id = B.from_id )
        
        where B.is_delete=0 and B.is_active=1 $where ");//inner join artist A on A.user_id = B.to_id 

        $row = $query->row();
        return isset($row->total)?$row->total:0;
    }


    public function get_booking_by_id($id=0) {
        $query=$this->db->query("select * from bookings A where A.is_delete=0 and A.is_active=1 and A.id=".(int)$id." limit 1");
        return $query->row();
    }
    public function get_booking_details($id=0) {
        $query=$this->db->query("select *,A.venue_id as row_id from venues A
        left join venue_details AD on A.venue_id = AD.venue_id
        where A.is_delete=0 and A.is_active=1 and A.user_id=".(int)$id." limit 1");
        return $query->row();
    }
    public function get_booking_details_by_id($id=0,$user='artist') {
        if($user=='venue'){
            $query=$this->db->query("select B.*,V.name as user_name from bookings B
        left join venues V on V.venue_id = B.to_id
        where B.is_delete=0 and B.is_active=1 and B.id=".(int)$id." limit 1");    
        }else{
            $query=$this->db->query("select B.*,A.name as user_name from bookings B
        left join artist A on A.artist_id = B.to_id
        where B.is_delete=0 and B.is_active=1 and B.id=".(int)$id." limit 1");    
        }

        return $query->row();
    }





    public function create_booking($data=[]) {
         
         $this->db->insert('bookings',$data);
         return  $this->db->insert_id();
        // $insert=[];
        // foreach($data as $key=>$val){
        //     if(isset($data[$key]) && in_array($key,$this->booking)){
        //         $insert[$key]=$data[$key];
        //     }
        // }
        // if(count($insert)>0){
        //     $insert['created_datetime']=date('Y-m-d H:i:s',time());
        //     $this->db->insert('bookings',$insert);
        //     return  $this->db->insert_id();

        // }
        // return false;


    }
    public function create_booking_history($data=[]) {

        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->booking_history)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0){
            $insert['created_datetime']=date('Y-m-d H:i:s',time());
            $this->db->insert('booking_history',$insert);
            return  $this->db->insert_id();

        }
        return false;


    }

    public function update_booking($data=[]) {
        $id=0;
        if(isset($data['user_id']) && $data['user_id']>0){
            $id=$data['user_id'];
        }
        $insert=[];
        foreach($data as $key=>$val){
            if(isset($data[$key]) && in_array($key,$this->booking)){
                $insert[$key]=$data[$key];
            }
        }
        if(count($insert)>0){

            $this->db->where('user_id',$id)->update('bookings',$insert);


        }
        return false;


    }
    public function get_artist_details_for_payment($id=0){

        $sql = $this->db->query("select B.id as id,B.amount as amount,B.created_datetime as datetime,A.name as artist_name,A.email as artist_email,V.user_id as user_id,V.name as venue_name,V.email as venue_email from bookings B inner join artist A on (A.user_id=B.from_id || A.user_id=B.to_id) inner join venues V on (V.user_id=B.to_id || V.user_id=B.from_id) where B.id=".(int)$id."");
        return $sql->row();
    }
    public function get_venue_details_for_payment($id=0){

        $sql = $this->db->query("select B.id as id,B.amount as amount,B.created_datetime as datetime,A.user_id as user_id,A.name as artist_name,A.email as artist_email,V.name as venue_name,V.email as venue_email from bookings B inner join artist A on (A.user_id=B.from_id || A.user_id=B.to_id) inner join venues V on (V.user_id=B.to_id || V.user_id=B.from_id) where B.id=".(int)$id."");
        return $sql->row();
    }
}
