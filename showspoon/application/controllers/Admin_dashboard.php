<?php 
class admin_dashboard extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		if(!$this->session->userdata('admin_id')){
			redirect('admin_login');
		}
	}
	public function index(){
		$this->data['title']  = 'Admin Dashboard';
		$this->data['artist'] = $this->admin_model->getArtistCount();
		$this->data['venue']  = $this->admin_model->getVenueCount(); 
		$this->data['admin']  = $this->admin_model->getAdminCount();
		$this->data['transaction'] = $this->admin_model->getTransactionCount();
		$this->load->view('admin/admin_dashboard',$this->data);
	}
	public function artist(){
		$this->data['title'] = 'Artist';
		$this->data['artist'] = $this->admin_model->getArtist();
		$this->load->view('admin/artist_view',$this->data);
	}
	public function artist_status($id=0,$state){
		$status = array();
		if($state==0 && $id!==""){
			$status['is_active'] = 0;
		}else{
			$status['is_active'] = 1;
		}
		$this->db->where('user_id',$id);
		$this->db->update('users',$status);
		$this->db->where('user_id',$id);
		$this->db->update('artist',$status);
		redirect('admin_dashboard/artist');
	}
	public function artist_delete($id=0){
		$this->db->where('user_id',$id);
		$this->db->delete('users');
		$this->db->where('user_id',$id);
		$this->db->delete('artist');
		redirect('admin_dashboard/artist');
	}
	public function artist_view($id=0){
		$this->data['title'] = 'Artist Details';
		$genre  = $this->admin_model->getArtistDetails($id);
		@$output = $this->admin_model->getGenres($genre->genre);
		$this->data['genres']  = $output;
		$this->data['details'] = $this->admin_model->getArtistDetails($id);
		$this->data['gallery'] = $this->admin_model->getArtistGallery($id);
		$this->data['media']   = $this->admin_model->getArtistMedia($id);
		$this->data['member']  = $this->admin_model->getArtistMember($id);
		$this->load->view('admin/artist_details_view',$this->data);
	}
	public function venue(){
		$this->data['title'] = 'Venue';
		$this->data['venue'] = $this->admin_model->getVenue();
		$this->load->view('admin/venue_view',$this->data);
	}
	public function venue_status($id=0,$state){
		$status = array();
		if($state==0 && $id!==""){
			$status['is_active'] = 0;
		}else{
			$status['is_active'] = 1;
		}
		$this->db->where('user_id',$id);
		$this->db->update('users',$status);
		$this->db->where('user_id',$id);
		$this->db->update('venues',$status);
		redirect('admin_dashboard/venue');
	}
	public function venue_delete($id=0){
		$this->db->where('user_id',$id);
		$this->db->delete('users');
		$this->db->where('user_id',$id);
		$this->db->delete('venues');
		redirect('admin_dashboard/venue');
	}
	public function venue_view($id=0){
		$this->data['title'] = 'Venue Details';
		$genre  = $this->admin_model->getVenueDetails($id);
		@$output = $this->admin_model->getGenres($genre->genre);
		$this->data['genres']  = $output;
		$this->data['details'] = $this->admin_model->getVenueDetails($id);
		$this->data['gallery'] = $this->admin_model->getVenueGallery($id);
		$this->data['media']   = $this->admin_model->getVenueMedia($id);
		$this->load->view('admin/venue_details_view',$this->data);
	}
	public function transection(){
		$this->data['title'] = 'Transactions';
		$this->data['transaction'] = $this->admin_model->getTransactions();
		$this->load->view('admin/transection_view',$this->data);
	}
	public function transaction_details($id=0){
		$this->data['title'] = 'Transaction Details';
		$this->data['id'] = $id;
		$this->data['details'] =$this->admin_model->getTransactionRow($id);
		$this->data['row'] = $this->admin_model->getCommission();
		$this->load->view('admin/transaction_details',$this->data);
	}
	public function update_transaction_details($id=0){
		$medium = $this->input->post('forward_amount');
		$this->db->where('transaction_id',$id);
		$this->db->update('transaction',array('status'=>'forwarded','medium'=>$medium));
		redirect("admin_dashboard/transaction_details/$id");
	}
	public function transfer($id=0){
		$this->data['title'] = 'Transfer Payment';
		$this->data['id'] = $id;
		if($this->input->post('submit')=='submit'){
			$this->form_validation->set_rules('amount','amount','trim|required');
			if($this->form_validation->run()==FALSE){
				$this->load->view('admin/transfer_view',$this->data);
			}else{
				$amount = $this->input->post('amount');
				$this->db->where('transaction_id',$id);
				$update = $this->db->update('transaction',array('amount'=>$amount,'status'=>'forwarded'));
				if($update){
					redirect('admin_dashboard/transection');
				}
			}
		}else{
            
            $this->data['row'] = $this->admin_model->getTransactionsRow($id);
			$this->load->view('admin/transfer_view',$this->data);
		}
	}
	public function transfer_payment(){
		$amount = $this->input->post('amount');
	    $token  = $_POST['stripeToken']; echo $token;
		// //include Stripe PHP library
		// require_once APPPATH."third_party/stripe/init.php";
		
		// //set api key
		// $stripe = array(
		//   "secret_key"      => "sk_test_IPFyAD6YkhFhl6QrqwqYFHus",
  //         "publishable_key" => "pk_test_NWhYzPP5DpSh6D2xHGQXwQr8"
		// );
		
		// \Stripe\Stripe::setApiKey($stripe['secret_key']);
		
		// //add customer to stripe
		// $customer = \Stripe\Customer::create(array(
		// 	'source'  => $token
		// ));
	}
	public function settings(){
		$this->data['title'] = 'Settings';
		if($this->input->post('submit')=='submit'){
			$this->form_validation->set_rules('percentage','percentage','required|trim');
			if(!$this->form_validation->run()){
				$this->load->view('admin/settings_view',$this->data);
			}else{
				$commission = array('commission'=>$this->input->post('percentage'));
				$update = $this->db->update('settings',$commission);
				if($update){
					$this->session->set_flashdata('msg','Percentage commission has been updated');
					redirect("admin_dashboard/settings");
				}
			}
		}else{
            
            $this->data['row'] = $this->admin_model->getCommission();
			$this->load->view('admin/settings_view',$this->data);
		}
	}
	public function admin(){
		$this->data['title'] = 'Admin';
		$this->data['admin'] = $this->admin_model->getAdminDetails();
		$this->load->view('admin/admin_view',$this->data);
	}
	public function admin_status($id=0,$state){
		$status = array();
		if($state==0 && $id!==""){
			$status['is_active'] = 0;
		}else{
			$status['is_active'] = 1;
		}
		$this->db->where('id',$id);
		$this->db->update('admin',$status);
		redirect('admin_dashboard/admin');
	}
	public function create_admin(){
		$this->data['title'] = 'Create Admin';
		if($this->input->post('submit')=='submit'){
			$this->form_validation->set_rules('f_name','first name','trim|required');
			$this->form_validation->set_rules('l_name','last name','trim|required');
			$this->form_validation->set_rules('email','email','trim|required|valid_email');
			$this->form_validation->set_rules('password','password','trim|required');
			if(!$this->form_validation->run()){
				$this->load->view('admin/create_admin',$this->data);
			}else{
				$count = $this->admin_model->checkAdminEmail($this->input->post('email'));
				if($count){
					$this->session->set_flashdata('error','Email already exist');
					$this->load->view('admin/create_admin',$this->data);
					//redirect("admin_dashboard/create_admin/$id");
				}else{
				$create_admin = array(
                    'f_name'    => $this->input->post('f_name'),
                    'l_name'    => $this->input->post('l_name'),
                    'email'     => $this->input->post('email'),
                    'password'  => md5($this->input->post('password')),
                    'is_active' => 1
				);
				$insert_id = $this->admin_model->createAdmin($create_admin);
				if($insert_id){
					redirect('admin_dashboard/admin');
				}
			  }
			}
		}else{

			$this->load->view('admin/create_admin',$this->data);
		}
	}
	public function admin_delete($id=0){
        $this->db->where('id',$id);
        $delete = $this->db->delete('admin');
        if($delete){
            redirect("admin_dashboard/admin");
        }
    }
    public function reviews(){
    	$this->data['title'] = 'Reviews';
    	$this->data['reviews'] = $this->admin_model->getReviews();
    	$this->load->view('admin/reviews_view',$this->data);
    }
    public function reviews_status($id=0,$state){
    	$status = array();
		if($state==0 && $id!==""){
			$status['is_active'] = 0;
		}else{
			$status['is_active'] = 1;
		}
    	$this->db->where('id',$id);
    	$this->db->update('comments',$status);
    	redirect("admin_dashboard/reviews");
    }
	public function admin_profile(){
        $this->data['title'] = 'Admin Profile';
        $id = $this->session->userdata('admin_id');
        if($this->input->post('submit')=='submit'){
        	$admin = array(
                 'f_name'   =>  $this->input->post('f_name'),
                 'l_name'   =>  $this->input->post('l_name'),
                 'email'    =>  $this->input->post('email'),
                 'password' =>  md5($this->input->post('password'))
        	);
        	$true = $this->admin_model->updateAdminProfile($id,$admin);
        	if($true){
        		$this->session->set_flashdata('msg','Your profile has been updated');
        		redirect("admin_dashboard/admin_profile");
        	}
        }else{
            
            $this->data['profile'] = $this->admin_model->getAdminProfile($id);
        	$this->load->view('admin/admin_profile',$this->data);
        }
	}
}
?>