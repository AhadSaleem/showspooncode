<!DOCTYPE html>
<html lang="en">
    <head>

        <?php $this->load->view('includes/head');?>
        <link href="<?php echo base_url();?>/assets/dist/plugins/iCheck/all.css" rel="stylesheet">
        <script src="<?php echo base_url();?>/assets/dist/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/plugins/select2/select2.min.js"></script>
        <!--<script src="<?php echo base_url();?>/assets/dist/js/bootstrap_multiselect.js"></script>-->

        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.time.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.date.js"></script>

        <!--<script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/legacy.js"></script>-->
    </head>
    <body>

        <?php $this->load->view('includes/header');?>




        <div  class="container">
            <div class="row m-b-md">
                <div class="col-lg-8 col-lg-offset-2 col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">

                    <?php
                    $user_image=site_url('assets/dist/images/noimage2.jpg');
                    if(isset($artist->profile_image) && $artist->profile_image!=''){
                        $user_image=site_url('uploads/users/thumb/'.$artist->profile_image);
                    }
                    ?>
                    <div class="media MiddleAvatar">
                        <div class="media-left">
                            <img src="<?php echo $user_image;?>" class="media-object middleimg" />
                        </div>
                    </div><!-- media -->
                    <h3 class="text-center">Book <?php echo isset($artist->name)?$artist->name:'';?></h3>

                    <form id="form-1" method="post">
                        <ul class="progresssteps m-t-md m-b-md">
                            <li class="active"> </li>
                            <li> </li>
                            <li> </li>
                        </ul> 

                        <div class="form-group">
                            <label>Name of your Event</label>
                            <input name="name" id="name" value="" type="text" class="form-control" required />
                        </div><!-- form-group -->

                        <div class="form-group">
                            <label>Location</label>
                            <input required name="location" id="location" value="" type="text" class="form-control"  />
                        </div><!-- form-group -->
                        <div class="form-group" style="position:relative;">
                            <label>Event Date</label>
                            <input name="date" id="date" value="" type="text" class="form-control pickadate" required />
                        </div><!-- form-group -->

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Start Time</label>
                                    <input id="time" name="time" type="text" class="form-control pickatime" required />
                                </div><!-- form-group -->
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>How long will the vendor be needed?</label>
                                    <!--<input name="hours" type="text" class="form-control " required />-->
                                    <select id="hours" class="form-control" name="hours">
                                        <?php
                                        for($v=1;$v<=30;$v++){


                                            $vp=$v*0.5;    
                                            if($vp<=1){
                                                echo '<option value="'.$vp.'">'.$vp.' Hour</option>';    
                                            }else{
                                                echo '<option value="'.$vp.'">'.$vp.' Hours</option>';
                                            }



                                        }
                                        ?>
                                    </select>
                                </div><!-- form-group -->
                            </div>
                        </div><!-- row -->

                        <div class="form-group">
                            <label>What else you would like this venue to know?</label>
                            <textarea id="description" name="description" required class="form-control" rows="4"></textarea>
                        </div><!-- form-group -->

                        <input type="submit" id="btn-1" name="submit" value="Next"  class="btn btn-primary btn-lg pull-right">

                    </form>

                    <div class="clearfix"></div>
                    <form style="display:none;" id="form-2" method="post">
                        <ul class="progresssteps m-t-md m-b-md">
                            <li class=""> </li>
                            <li class="active"> </li>
                            <li class=""> </li>
                        </ul> 

                        <div class="form-group">
                            <label>Who will pay for this event?</label>

                            <div class="radio">
                                <label>
                                    <input type="radio" class="who_pay" name="optionsRadios" id="optionsRadios1" value="1" checked>
                                    I will pay
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio"  class="who_pay" name="optionsRadios" id="optionsRadios2" value="2">
                                    Artist will pay
                                </label>
                            </div>
                        </div><!-- form-group -->

                        <div class="form-group m-b-xxl m-t-xxl">
                            <label>Request Amount</label>
                            <input id="amount" type="text" class="form-control" placeholder="NOK" name="amount" />
                        </div><!-- form-group -->


                        <input type="submit" id="btn-2" name="submit2" value="Next" class="btn btn-primary btn-lg pull-right m-l-xs">
                        <a href="javascript:;" onclick="backPage('form-2','form-1')" class="btn btn-primary btn-lg pull-right">Back</a>
                    </form>
                    <div class="clearfix"></div>
                    <form style="display:none;" id="form-3" method="post">
                        <ul class="progresssteps m-t-md m-b-md">
                            <li class=""> </li>
                            <li class=""> </li>
                            <li class="active"> </li>
                        </ul> 

                        <h4 class="l-h">You are ready to submit your requrest for booking to Skambant Mantra. Please note that booking will be finalized once the artist approve this request and the payment has been made.</h4>

                        <div class="form-group m-t-xxl">
                            <label>Terms and Condition</label>
                            <textarea class="form-control" rows="5" placeholder="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. "></textarea>
                        </div><!-- form-group -->

                        <div class="checkbox m-b-xxl">
                            <div class="form-group">
                                <div class="custom_select_box ">
                                    <label>
                                        <input required name="terms" type="checkbox" value="1">
                                        I Agree to Terms and Conditions
                                    </label>
                                </div>
                            </div>
                        </div>

                        <input type="submit" name="submi3" id="btn-3" class="btn btn-primary btn-green btn-lg pull-right m-l-xs" value="Submit Booking">
                        <a href="javascript:;" onclick="backPage('form-3','form-2')" class="btn btn-primary btn-lg pull-right">Back</a>
                    </form>
                </div>
            </div><!-- row -->
        </div><!-- container -->


<?php $this->load->view('includes/footer');?>

        <form method="post" id="hidden_form">
            <input type="hidden" id="hidden_description" name="description" value="">
            <input type="hidden" id="hidden_name" name="name" value="">
            <input type="hidden" id="hidden_date" name="date" value="">
            <input type="hidden" id="hidden_location" name="location" value="">
            <input type="hidden" id="hidden_time" name="time" value="">
            <input type="hidden" id="hidden_hours" name="hours" value="">
            <input type="hidden" id="hidden_amount" name="amount" value="">
            <input type="hidden" id="hidden_who_pay" name="who_pay" value="">
            <input type="hidden" id="hidden_venue" name="venue" value="<?php echo isset($artist->user_id)?$artist->user_id:0;?>">

        </form>
        <script src="<?php echo base_url(); ?>assets/dist/plugins/iCheck/icheck.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
        <script>
            function backPage(form_hide,form_show){
                $('#'+form_hide).hide();
                $('#'+form_show).show();
            }
            $(document).ready(function () {


                $('.pickadate').pickadate({
                    formatSubmit: 'yyyy/mm/dd',
                    format: 'yyyy-mm-dd',
                    today: '',
                    close: '',
                    clear: '',
                    min: [<?php echo date('Y');?>,<?php echo date('m');?>,<?php echo date('d',strtotime('+1 day',time()));?>]
                });
                $('.pickatime').pickatime();
                $('#btn-1').on('click', function () {
                    $("form#form-1").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            email: {
                                required: true,
                            }                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            //form.submit();
                            $('#form-1').hide();
                            $('#form-2').show();
                            $('input').iCheck({
                                checkboxClass: 'icheckbox_square',
                                radioClass: 'iradio_square',
                                increaseArea: '20%' // optional
                            });
                        }
                    });
                });

                $('#btn-2').on('click', function () {
                    $("form#form-2").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            amount: {
                                required: true,
                                number:true
                            }
                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            //form.submit();
                            $('#form-2').hide();
                            $('#form-3').show();
                        }
                    });
                });

                $('#btn-3').on('click', function () {
                    $("form#form-3").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            amount: {
                                required: true,
                                number:true
                            }
                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            //form.submit();
                            //$('#form-2').hide();
                            //$('#form-3').show();
                            var booking_name = $('#name').val();
                            var booking_date = $('#date').val();
                            var booking_location = $('#location').val();
                            var booking_time = $('#time').val();
                            var booking_hours = $('#hours').val();
                            var booking_description = $('#description').val();
                            var booking_amount = $('#amount').val();
                            var booking_pay = $('.who_pay:checked').val();



                            $('#hidden_description').val(booking_description);
                            $('#hidden_name').val(booking_name);
                            $('#hidden_location').val(booking_location);
                            $('#hidden_date').val(booking_date);
                            $('#hidden_time').val(booking_time);
                            $('#hidden_hours').val(booking_hours);
                            $('#hidden_amount').val(booking_amount);
                            $('#hidden_who_pay').val(booking_pay);
                            $('#hidden_venue').val(<?php echo isset($artist->user_id)?$artist->user_id:0;?>);
                            $('#hidden_form').submit();


                        }
                    });
                });
            });
        </script>
        <style>
            .search-btn{
                border-radius: 5px;
                margin-top: 29px;
                padding: 10px 60px;
            }
        </style>
    </body>
</html>