<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('admin/includes/head');?>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/pages/datatables_advanced.js"></script>
    </head>
    <body class="navbar-top">

        <!-- Main navbar -->
        <?php $this->load->view('admin/includes/header');?>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <?php $this->load->view('admin/includes/sidebar');?>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Admins</span> </h4>
                              
                            </div>

                            <div class="heading-elements">
                                <div class="heading-btn-group">
                                    <div class="pull-right">
                                        <a href="<?php echo site_url('managers/create');?>" class="btn btn-sm btn-primary pull-right"><i class="fa fa-plus"></i> Add Admin</a>
                                    </div>
                                </div>
                            </div>


                        </div>


                    </div>
                    <!-- /page header -->



                    <div class="content">

                        <?php if(isset($error) && $error!=''){ ?>
                        <div class="alert alert-danger">
                            <button class="close" data-close="alert"></button>
                            <span><?php echo $error; ?></span>
                        </div>
                        <?php } ?>
                        <?php if(isset($success) && $success!=''){ ?>
                        <div class="alert alert-success">
                            <button class="close" data-close="alert"></button>
                            <span><?php echo $success; ?></span>
                        </div>
                        <?php } ?>
                        <!-- Page length options -->
                        <div class="panel panel-flat">




                            <table class="table datatable-show-all">
                                <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(isset($admins) && count($admins)>0){
    foreach($admins as $row){
        $type='Admin';
        $status = '<span class="label label-default">Inactive</span>';
        if($row->is_active==1){
            $status = '<span class="label label-success">Active</span>';
        }
        if($row->type==2){
            $type='Sub Admin';
        }

                                    ?>

                                    <tr>
                                        <td><?php echo $row->f_name;?></td>
                                        <td><?php echo $row->l_name;?></td>

                                        <td><a href="mailto:<?php echo $row->email;?>"><?php echo $row->email;?></a></td>

                                        <td><?php echo $type?></td>
                                        <td><?php echo $status?></td>
                                        <td class="text-center">
                                            <ul class="icons-list">
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="icon-menu9"></i>
                                                    </a>

                                                    <ul class="dropdown-menu dropdown-menu-right">

                                                        <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="icon-user-plus"></i> Roles</a></li>
                                                        <li><a href="<?php echo site_url('managers/edit/'.$row->id);?>"><i class="icon-user"></i> Edit</a></li>
                                                        <li><a href="<?php echo site_url('managers/index/?delete='.$row->id);?>"><i class="icon-trash"></i> Delete</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <?php }?>
                                    <?php }?>



                                </tbody>
                            </table>
                        </div>
                        <!-- /page length options -->

                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Modal Header</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Some text in the modal.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>



                        <!-- Footer -->
                        <?php $this->load->view('admin/includes/footer');?>
                        <!-- /footer -->

                    </div>

                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
