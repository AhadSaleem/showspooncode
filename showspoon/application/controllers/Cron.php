<?php
class cron extends MY_Controller{
	public function __construct(){ 
		parent::__construct(); 
		$this->load->model('Notification_model');
	}
	public function auto_send_artist(){
        $sql = $this->db->query("SELECT * FROM bookings WHERE created_datetime >= ( CURDATE() - INTERVAL 2 DAY ) AND type = 1 AND btn = 'success'");
        foreach($sql->result() as $row){
            $artist_sql = $this->db->query("select name,email from artist where user_id=$row->from_id");
            $venue_sql = $this->db->query("select name,email from venues where user_id=$row->to_id");
            $this->db->where('id',$row->id)->update('bookings',['btn'=>'cancelled']);
            $email_data = array(
                 'artist'=>$artist_sql->row()->name,
                 'venue'=>$venue_sql->row()->name
              ); 
            $artist_email_body = $this->load->view('email/auto_request_from_artist', $email_data,true);   
            $this->Send_Mail($venue_sql->row()->email,FROM_EMAIL,FROM_NAME,'Request Cancelled',$artist_email_body);
            $notification=[
                    'type'=>1,
                    'message'=>'Request has been cancelled',
                    'from_id'=>$row->from_id,
                    'to_id'=>$row->to_id,
                    'status'=>'request'
                ]; 
            $notification_id =$this->Notification_model->create_notification($notification);
            $this->db->where('id',$row->id);
            $this->db->update('bookings',['status'=>1]);
        }
    }
    public function auto_send_venue(){
        $sql = $this->db->query("SELECT * FROM bookings WHERE created_datetime >= ( CURDATE() - INTERVAL 2 DAY ) AND type = 2 AND btn = 'success'");
        foreach($sql->result() as $row){
            $artist_sql = $this->db->query("select name,email from artist where user_id=$row->to_id");
            $venue_sql = $this->db->query("select name,email from venues where user_id=$row->from_id");
            $this->db->where('id',$row->id)->update('bookings',['btn'=>'cancelled']);
            $email_data = array(
                 'artist'=>$artist_sql->row()->name,
                 'venue'=>$venue_sql->row()->name
              ); 
            $artist_email_body = $this->load->view('email/auto_request_from_venue', $email_data,true);   
            $this->Send_Mail($artist_sql->row()->email,FROM_EMAIL,FROM_NAME,'Request Cancelled',$artist_email_body);
            $notification=[
                    'type'=>2,
                    'message'=>'Request has been cancelled',
                    'from_id'=>$row->from_id,
                    'to_id'=>$row->to_id,
                    'status'=>'request'
                ]; 
            $notification_id =$this->Notification_model->create_notification($notification);
            $this->db->where('id',$row->id);
            $this->db->update('bookings',['status'=>1]);
        }
    }
}
?>