<header class="main-header">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- navbar for login link only -->
        <div class="clearfix">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
          <div class="float-left">
            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <img src="<?php echo site_url('assets/images/logo-n.png'); ?>" alt="Logo">
            </a>
          </div>
        </div>
          <!-- <div class="float-left">
            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <img src="<?php echo site_url(); ?>assets/images/logo-n.png" alt="Logo">
            </a>
          </div> -->
          <!-- <div class="float-right">
            <a class="nav-btn" href="<?php echo base_url(); ?>account/login">Login</a>
          </div> -->
          <div class="collapse navbar-collapse" id="myNavbar">
          <div class="float-center">
          <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo site_url('about'); ?>">About</a></li>
          <li><a href="<?php echo site_url('showspoon-works'); ?>">How Showspoon works</a></li>
          <li><a href="<?php echo site_url('contact'); ?>">Contact Us</a></li>
          <li><a href="<?php echo site_url('account/login'); ?>">Login</a></li>
          </ul>
        </div>
        </div>
        </div>
        <!-- navbar for login link only ends -->

        
        <!-- <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url();?>">
                <img src="<?php echo site_url();?>/assets/images/logo-n.png" class="img-responsive"/>
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right vertical-align">
               <li><a href="<?php echo site_url('account/login');?>" class="">Login</a></li>
            </ul>            
        </div> -->
    </div><!-- /.container-fluid -->
</nav>
</header>