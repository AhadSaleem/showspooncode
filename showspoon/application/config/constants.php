<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code




//SITE CUSTOM CONSTANTS
defined('LINK_VER')      OR define('LINK_VER', 3); // Sendgrid secret key for sending email
defined('SENDGRID_KEY')      OR define('SENDGRID_KEY', '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'); // Sendgrid secret key for sending email

defined('FROM_EMAIL')      OR define('FROM_EMAIL', 'support@showspoon.com');
defined('FROM_NAME')      OR define('FROM_NAME', 'Showspoon');


//FACEBOOK
defined('FACEBOOK_CLIENT_ID')        OR define('FACEBOOK_CLIENT_ID', '155504055167484'); // no errors
defined('FACEBOOK_CLIENT_SECRET')        OR define('FACEBOOK_CLIENT_SECRET', 'fad70df04719d8f640ac1c81de5d9d37'); // no 


//GOOGLE
defined('GOOGLE_API_KEY')        OR define('GOOGLE_API_KEY', '1079830864831-lmdnmo2mae7kaldd2jseo2m6b28cijtv.apps.googleusercontent.com'); 
defined('GOOGLE_API_SECRET')        OR define('GOOGLE_API_SECRET', 'NtJMGl84c4PVAEz9yJbARRzi'); // no errors

//TWITTER
defined('T_OAUTH_ACCESS_TOKEN')      OR define('T_OAUTH_ACCESS_TOKEN', '760471429448081408-DMANURw4NOQXb2yq6CDfhJmbYewQ1FN'); 
defined('T_OAUTH_ACCESS_TOKEN_SECRET')      OR define('T_OAUTH_ACCESS_TOKEN_SECRET', 'pOen9w1HuXvwuGYMFMRquUjy5XHq6ZhAI4fbucPloFwKJ'); 
defined('T_CONSUMER_KEY')      OR define('T_CONSUMER_KEY', 'm2RXOsvY3XGR5cyMnXrjOdMze'); 
defined('T_CONSUMER_SECRET')      OR define('T_CONSUMER_SECRET', 'gAXFvrDHLxIj02HcuHoc0BKYrrVEKKs8kznwkzaPa5FyN1cDTC'); 

//INSTAGRAM
//defined('INSTAGRAM_API_TOEKN')      OR define('INSTAGRAM_API_TOEKN', '3110967165.94d60ad.14edcc437c8a432ea739dcc80c346ac7'); 
defined('INSTAGRAM_API_TOKEN')      OR define('INSTAGRAM_API_TOKEN', '7644019937.58bea79.30bc94198af64436b4369befaf07590c');  //7644019937.58bea79.30bc94198af64436b4369befaf07590c
defined('INSTAGRAM_API_KEY')      OR define('INSTAGRAM_API_KEY', '58bea793fb9b4020a60706a7a1850d20'); 
defined('INSTAGRAM_API_SECRET')      OR define('INSTAGRAM_API_SECRET', 'b667856592cd4be6a272905fc745c9a7'); 

//Notifications
defined('REQUEST_SENT')        OR define('REQUEST_SENT', 'New request has been received.'); // no errors
defined('REQUEST_ACCEPTED')        OR define('REQUEST_ACCEPTED', 'Your proposal has been accepted.'); // no errors


// defined('FACEBOOK_API_TOEKN')      OR define('FACEBOOK_API_TOEKN', '1788504024704883|c555699cb45bf777721e04641309573a'); 
// defined('FACEBOOK_API_ID')         OR define('FACEBOOK_API_ID', '1788504024704883'); 
// defined('FACEBOOK_API_SECRET')     OR define('FACEBOOK_API_SECRET', 'c555699cb45bf777721e04641309573a'); 


defined('FACEBOOK_API_TOEKN')      OR define('FACEBOOK_API_TOEKN', '155504055167484|fad70df04719d8f640ac1c81de5d9d37'); 
defined('FACEBOOK_API_ID')         OR define('FACEBOOK_API_ID', '155504055167484'); 
defined('FACEBOOK_API_SECRET')     OR define('FACEBOOK_API_SECRET', 'fad70df04719d8f640ac1c81de5d9d37'); 

