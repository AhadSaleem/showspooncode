
<?php $this->load->view('admin/includes/admin_header'); ?>
<style>.text{padding:10px 0px;}</style>
<body>

<!-- Main navbar -->
<div class="navbar navbar-default header-highlight">
<div class="navbar-header">
<a class="navbar-brand" href="<?php echo site_url('admin_dashboard'); ?>"><img src="<?php echo site_url('assets/images/logo-n.png'); ?>" alt=""></a>

<ul class="nav navbar-nav visible-xs-block">
<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
</ul>
</div>

<div class="navbar-collapse collapse" id="navbar-mobile">
<ul class="nav navbar-nav navbar-right">
<li class="dropdown dropdown-user">
<a class="dropdown-toggle" data-toggle="dropdown">
<img src="<?php echo site_url(); ?>assets/images/placeholder.jpg" alt="">
<span><?php echo ($this->session->userdata('f_name'))?$this->session->userdata('f_name'):'Admin'; ?></span>
<i class="caret"></i>
</a>

<ul class="dropdown-menu dropdown-menu-right">
<li><a href="<?php echo site_url('admin_dashboard/admin_profile'); ?>"><i class="icon-user-plus"></i> My profile</a></li>
<li><a href="<?php echo site_url('admin_login/logout'); ?>"><i class="icon-switch2"></i> Logout</a></li>
</ul>
</li>
</ul>
</div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

<!-- Page content -->
<div class="page-content">

<!-- Main sidebar -->
<div class="sidebar sidebar-main">
<div class="sidebar-content">

<!-- Main navigation -->
<div class="sidebar-category sidebar-category-visible">
<div class="category-content no-padding">
<ul class="navigation navigation-main navigation-accordion">

<!-- Main -->
<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
<li><a href="<?php echo site_url('admin_dashboard'); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/artist'); ?>"><i class="icon-width"></i> <span>Artist</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/venue'); ?>"><i class="icon-width"></i> <span>Venues</span></a></li>
<li class="active"><a href="<?php echo site_url('admin_dashboard/transection'); ?>"><i class="icon-width"></i> <span>Transactions</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/admin'); ?>"><i class="icon-user"></i> <span>Admin</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/reviews'); ?>"><i class="icon-comments"></i> <span>Reviews</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/settings'); ?>"><i class="icon-user"></i> <span>Settings</span></a></li>
<li><a href="<?php echo site_url('admin_dashboard/admin_profile'); ?>"><i class="icon-user-plus"></i> <span>Profile</span></a></li>
</ul>
</div>
</div>
<!-- /main navigation -->

</div>
</div>
<!-- /main sidebar -->


<!-- Main content -->
<div class="content-wrapper">

<!-- Content area -->
<div class="content">

<!-- Main charts -->
<div class="row">
<div class="col-lg-12">
<div class="panel panel-flat">
<div class="panel-heading">
<h5 class="panel-title">Transaction Details</h5>
<div class="heading-elements">
<ul class="icons-list">
<li><a data-action="collapse"></a></li>
<li><a data-action="reload"></a></li>
<li><a data-action="close"></a></li>
</ul>
</div>
</div>

<div class="panel-body">
<div class="row">
<div class="col-md-4">
<div class="col-md-12 text"><strong>Event Date: </strong> <?php echo date('jS M Y',strtotime($details->event_date)); ?> </div>
<div class="col-md-12 text"><strong>Who Paid: </strong>
<?php 
$exp = explode(" ",$details->t_type);
if($exp[0]==1){echo isset($details->from_name)?$details->from_name.' <b>(Artist)</b>':'';}
else{echo isset($details->to_name)?$details->to_name.' <b>(Venue)</b>':'';}
?>
</div>
<div class="col-md-12 text"><strong>Amount: </strong> <?php $real_amount = $details->t_amount - $row->commission; echo '$'.$real_amount; ?> </div>
<div class="col-md-12 text"><strong>Commission: </strong> <?php echo '$'.$row->commission; ?> </div>
<div class="col-md-12 text"><strong>Total: </strong> <?php echo '$'.$details->t_amount; ?> </div>
<div class="col-md-12 text"><strong>Paid To: </strong>
<?php 
$exp = explode(" ",$details->t_type);
if($exp[0]==2){echo isset($details->from_name)?$details->from_name.' <b>(Artist)</b>':'';}
else{echo isset($details->to_name)?$details->to_name.' <b>(Venue)</b>':'';}
?>
</div>
<div class="col-md-12 text"><strong>Stauts: </strong> 
<?php if($details->status=='succeeded'){echo '<span class="label label-warning">Received</span>'; }else{echo '<span class="label label-success">Paid</span>'; } ?> 
</div>
<div class="col-md-12 text"><strong>Token: </strong> 
<?php echo $details->token; ?>
</div>
<?php if($details->status=='succeeded' && $details->t_medium==""){ ?>
<form action="<?php echo site_url(); ?>admin_dashboard/update_transaction_details/<?php echo $id; ?>" method="post">
<div class="col-md-12 text"><strong>Forward Amount: </strong>
<select name="forward_amount" class="form-control" style="width:250px;">
<option value="By check">By check</option>
<option value="By money account transfer">By money account transfer</option>
<option value="By bank account transfer">By bank account transfer</option>
</select>
</div>
<div class="col-md-12 text"><strong> </strong> <input type="submit" name="submit" value="Submit" class="btn btn-primary"> </div>
</form>
<?php }else{echo '';} ?>
<?php /*if($details->medium!=""){ ?>
<div class="col-md-12 text"><strong>Medium: </strong> 
<?php echo $details->medium; ?>
</div>
<?php }else{echo '';}*/ ?>
</div>
</div>
</div>  
</div>
</div>
</div>
<!-- Footer -->
<div class="footer text-muted">
&copy; <?php echo date('Y'); ?>. <a href="#">Showspoon</a> by <a href="http://www.mashkraft.com" target="_blank">Mashkraft</a>
</div>
<!-- /footer -->

</div>
<!-- /content area -->

</div>
<!-- /main content -->

</div>
<!-- /page content -->

</div>
<!-- /page container -->
<?php $this->load->view('admin/includes/admin_footer'); ?>
</body>
</html>
