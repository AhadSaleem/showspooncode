<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('includes/head');?>
        <link href="<?php echo base_url();?>/assets/dist/plugins/iCheck/all.css" rel="stylesheet">
        <script src="<?php echo base_url();?>/assets/dist/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/plugins/select2/select2.min.js"></script>
        <!--<script src="<?php echo base_url();?>/assets/dist/js/bootstrap_multiselect.js"></script>-->
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.time.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.date.js"></script>
        <!--<script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/legacy.js"></script>-->
    </head>
    <body>
        <?php $this->load->view('includes/header');
        $class = $this->router->fetch_class();
        $method = $this->router->fetch_method();
        ?>
        <div class="container-fluid defaultpad lightgrey " id="chat_box">
            <div class="container">
                <div class="row">
                    <?php
                    $session_user = isset($session_user)?(object)$session_user:[];
                    $user_image=site_url('assets/dist/images/noimage2.jpg');
                    if(isset($session_user->profile_image) && $session_user->profile_image!=''){
                    $user_image=site_url('uploads/users/thumb/'.$session_user->profile_image);
                    }
                    ?>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 ChatSession">
                        <div class="row msg_container base_receive hidden">
                            <div class="col-md-1 col-xs-1 avatar no-gutter">
                                <img src="<?php echo $user_image;?>" class=" img-responsive">
                            </div>
                            <div class="col-md-11 col-xs-11 no-gutter">
                                <form id="form-11" method="post">
                                    <div class="messages msg_receive">
                                        <div class="form-group">
                                            <textarea id="text-message2" name="message" required class="form-control" rows="4"></textarea>
                                            
                                        </div>
                                        <div class="row m-t-sm">
                                            <div class="col-sm-6 col-xs-6 hidden">
                                                <input  id="input-1a" type="file" class="file" data-show-preview="false" placeholder="Upload File" value="Attach Image or File">
                                                <small>Attach image or file</small>
                                            </div>
                                            <div class="col-sm-push-6 col-sm-6 col-xs-6 text-right">
                                                <input type="submit" id="btn-11" value="Send Message" class="btn btn-message m-b-sm">
                                                <!--<button type="button" class="btn btn-message m-b-sm">Send Quote</button>-->
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <ul id="" class="messages_thread">
                            <li class="message left appeared">
                                <div class="avatar"><img src="<?php echo $user_image;?>" class=" img-responsive img-circle"></div>
                                <div class="text_wrapper">
                                    <div class="text">
                                        <form id="form-1" method="post">
                                            <div class="form-group">
                                                <textarea id="text-message" name="message" required class="form-control" rows="4"></textarea>
                                            </div>
                                            <div class="clearfix"></div>
                                            &nbsp;<input type="submit" id="btn-1" value="SEND" class="btn btn-message btn-rounded m-b-none pull-right thread_send">
                                            <?php if($session_user->type_id!=$booking->type && $booking->status==0){?>
                                            <input data-toggle="modal" data-target="#myModal_accept" type="button" id="btn-2" value="Accept" class="btn btn-message btn-rounded m-b-none pull-right thread_send m-r-sm">
                                            
                                            <?php }?>
                                        </form>
                                        <div class="modal fade" id="myModal_accept" tabindex="-1" role="dialog" >
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header ">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Event Details</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        
                                                        <h5 class=" m-b-xs">
                                                        <div class="bold" style="margin-left: 15px;"> <?php echo date('D,M d, Y',strtotime($booking->event_date));;?></div>
                                                        <?php $minutes = ($booking->hours*60);
                                                        $who_pay='';
                                                        if(isset($booking->type) && $booking->type==1 && ($session_user->type_id==1 || $session_user->type_id==2)){
                                                        $who_pay='I will pay';
                                                        }elseif(isset($booking->type) && $booking->type==2 && ($session_user->type_id==1)){
                                                        $who_pay='Venue will pay';
                                                        }elseif(isset($booking->type) && $booking->type==2 && ($session_user->type_id==2)){
                                                        $who_pay='Artist will pay';
                                                        }
                                                        ?>
                                                        <div class="m-b-sm bold" style="margin-left: 15px;"><?php echo date('h:i A',strtotime($booking->name));;?> - <?php echo date('h:i A',strtotime("+$minutes minutes",strtotime($booking->name)));;?> (<?php echo $booking->hours;?> hour)</div>
                                                        
                                                        <div class="m-b-sm bold"><span style="margin-left: 15px;">Location: &nbsp;</span><?php echo $booking->location ?></div>
                                                        
                                                        
                                                        <div class="m-b-sm bold"><span style="margin-left: 15px;">Amount: &nbsp;</span><?php echo number_format($booking->amount,0);?> NOK, <?php echo $who_pay;?></div>
                                                        </h5>
                                                        <br>
                                                        <form class="form" id="transaction_form" action="">
                                                            <?php if($booking->who_pay==2 && $booking->type==2  || $booking->who_pay==2 && $booking->type==1): ?>
                                                            <div class="col-lg-4">
                                                                <div class="form-inline">
                                                                    <label class="control-label">Credit Card Number</label>
                                                                    <input  required class="form-control input-sm" type="text" name="credit_card" id="credit_card">
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="col-lg-4">
                                                                <div class="form-inline">
                                                                    <label class="control-label">Exp</label>
                                                                    <input required class="form-control input-sm" type="text" name="exp" id="exp">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <div class="form-inline">
                                                                    <label class="control-label">CVV</label>
                                                                    <input required class="form-control input-sm" type="text" name="cvv" id="ccv">
                                                                </div>
                                                            </div>
                                                            <?php endif; ?>
                                                            <p><br></p>
                                                            <p><br></p>
                                                            <div class="form-group">
                                                                <?php if($booking->who_pay==2 && $booking->type==2  || $booking->who_pay==2 && $booking->type==1): ?>
                                                                <input id="accept_proposal" name="submit" value="Yes" type="submit" class="btn b btn-primary pull-right">
                                                                <?php else: ?>
                                                                <input type="button" id="agreement" value="Agreement" class="btn pull-right btn-danger">
                                                                <?php endif; ?>
                                                                
                                                            </div>
                                                        </form>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <ul id="Message-Box" class="messages_thread">
                            <?php
                            $session_user = (array)$session_user;
                            if(count($chat)>0){
                            //dd($booking);
                            foreach($chat as $row){
                            if($row->from==$session_user['user_id']){
                            $this->load->view('includes/chat-left',['chat'=>$row,'session_user'=>$session_user]);
                            }else{
                            $this->load->view('includes/chat-right',['chat'=>$row,'session_user'=>$session_user]);
                            }
                            }
                            }
                            ?>
                            <?php
                            $minutes = ($booking->hours*60);
                            $who_pay='';
                            if(isset($booking->type) && $booking->type==1 && ($session_user['type_id']==1 || $session_user['type_id']==2)){
                            $who_pay='I will pay';
                            }elseif(isset($booking->type) && $booking->type==2 && ($session_user['type_id']==1)){
                            $who_pay='Venue will pay';
                            }elseif(isset($booking->type) && $booking->type==2 && ($session_user['type_id']==2)){
                            $who_pay='Artist will pay';
                            }
                            if($session_user['type_id']==1){
                            $booked_party = $venue_details;
                            }else{
                            $booked_party = $artist_details;
                            }
                            if(isset($booking->type) && (($session_user['type_id']==1 && $booking->type==1) ||  ($booking->type==2  && $session_user['type_id']==2))){
                            //dd($artist_details);
                            ?>
                            <li class="message left appeared first_request">
                                <div class="avatar">
                                    <img src="<?php echo $user_image;?>" class=" img-responsive img-circle">
                                </div>
                                <div class="text_wrapper">
                                    <div class="text">
                                        <h4 class="m-t-none">Request Sent</h4>
                                        <?php echo $booked_party->name;?><br>
                                        <?php echo $booking->name;?> - <?php echo date('D,M d, Y',strtotime($booking->event_date));;?> - West Liberty , IA - <?php echo date('h:i A',strtotime($booking->name));;?> - <?php echo date('h:i A',strtotime("+$minutes minutes",strtotime($booking->name)));;?> (<?php echo $booking->hours;?> hour)<br>
                                        <?php echo number_format($booking->amount,0);?> NOK , <?php echo $who_pay;?>
                                    </div>
                                </div>
                            </li>
                            <?php }else{
                            $who_pay='';
                            $secon_user_image=site_url('assets/dist/images/noimage2.jpg');
                            if(($booking->type==1 || $booking->type==2) && $booking->who_pay==1){
                            $who_pay='Artist will pay';
                            if(isset($artist_details->profile_image) && $artist_details->profile_image){
                            if(isset($artist_details->profile_image) && $artist_details->profile_image!=''){
                            $secon_user_image=site_url('uploads/users/thumb/'.$artist_details->profile_image);
                            }
                            }
                            }elseif(($booking->type==1 || $booking->type==2) && $booking->who_pay==2){
                            $who_pay='I will pay';
                            if(isset($venue_details->profile_image) && $venue_details->profile_image){
                            if(isset($venue_details->profile_image) && $venue_details->profile_image!=''){
                            $secon_user_image=site_url('uploads/users/thumb/'.$venue_details->profile_image);
                            }
                            }
                            }
                            ?>
                            <?php
                            ?>
                            <li class="message right appeared first_request">
                                <div class="avatar">
                                    <img src="<?php echo $secon_user_image;?>" class=" img-responsive img-circle">
                                </div>
                                <div class="text_wrapper">
                                    <div class="text">
                                        <h4 class="m-t-none">Request Received</h4>
                                        <?php echo $booked_party->name;?><br>
                                        <?php echo $booking->name;?> - <?php echo date('D,M d, Y',strtotime($booking->event_date));;?> - <?php echo $booking->location;?> - <?php echo date('h:i A',strtotime($booking->name));;?> - <?php echo date('h:i A',strtotime("+$minutes minutes",strtotime($booking->name)));;?> (<?php echo $booking->hours;?> hour)<br>
                                        <?php echo number_format($booking->amount,0);?> NOK , <?php echo $who_pay;?>
                                    </div>
                                </div>
                            </li>
                            <?php }?>
                        </ul>
                        <div class="row msg_container base_receive hidden">
                            <div class="col-md-1 col-xs-1 avatar no-gutter">
                                <img src="dist/images/victoria_small.jpg" class=" img-responsive">
                            </div>
                            <div class="col-md-11 col-xs-11 no-gutter">
                                <div class="messages msg_receive">
                                    <h4>I'm sending you my Quote see below</h4>
                                    <h4>Price: 10000 NOK</h4>
                                    <a href="" class="btn btn-primary pull-right m-t-sm m-b-sm">Accept &amp; Book</a>
                                </div>
                            </div>
                        </div>
                        </div><!-- ChatSession -->
                        <?php $this->load->view('includes/chat-part',['who_pay'=>$who_pay]);?>
                        </div><!-- row -->
                        </div><!-- container -->
                        </div><!-- container-fluid -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel"></h4>
                                    </div>
                                    <div class="modal-body">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="myModal_payment" tabindex="-1" role="dialog" >
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header ">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Event Details</h4>
                                    </div>
                                    <div class="modal-body">
                                        
                                        <h5 class=" m-b-xs">
                                        <div class="bold" style="margin-left: 15px;"> <?php echo date('D,M d, Y',strtotime($booking->event_date));;?></div>
                                        <?php $minutes = ($booking->hours*60);
                                        $who_pay='';
                                        
                                        ?>
                                        <div class="m-b-sm bold" style="margin-left: 15px;"><?php echo date('h:i A',strtotime($booking->name));;?> - <?php echo date('h:i A',strtotime("+$minutes minutes",strtotime($booking->name)));;?> (<?php echo $booking->hours;?> hour)</div>
                                        
                                        <div class="m-b-sm bold"><span style="margin-left: 15px;">Location: &nbsp;</span><?php echo $booking->location ?></div>
                                        
                                        
                                        <div class="m-b-sm bold"><span style="margin-left: 15px;">Amount: &nbsp;</span><?php echo number_format($booking->amount,0);?> NOK, <?php echo $who_pay;?></div>
                                        </h5>
                                        <br>
                                        <form class="form" id="transaction_form1" action="">
                                            <div class="col-lg-4">
                                                <div class="form-inline">
                                                    <label class="control-label">Credit Card Number</label>
                                                    <input  required class="form-control input-sm" type="text" name="credit_card" id="credit_card1">
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-4">
                                                <div class="form-inline">
                                                    <label class="control-label">Exp</label>
                                                    <input required class="form-control input-sm" type="text" name="exp" id="exp1">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-inline">
                                                    <label class="control-label">CVV</label>
                                                    <input required class="form-control input-sm" type="text" name="cvv" id="ccv1">
                                                </div>
                                            </div>
                                            
                                            <p><br></p>
                                            <p><br></p>
                                            <div class="form-group">
                                                
                                                <input id="accept_proposal1" name="submit" value="Yes" type="submit" class="btn b btn-primary pull-right">
                                            </div>
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <?php $this->load->view('includes/footer');?>
                        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
                        <script>
                        var myInterval;
                        var interval_delay = 2000;
                        var is_interval_running = false; //Optional
                        function backPage(form_hide,form_show){
                        $('#'+form_hide).hide();
                        $('#'+form_show).show();
                        }
                        Update_Message_Box();
                        function Update_Message_Box(){
                        is_interval_running = true;
                        $.ajax({
                        type:'POST',
                        url: '<?php echo site_url($class.'/message_thread');?>',
                        data:{book_id:'<?php echo isset($booking->id)?$booking->id:0;?>',type:'<?php echo isset($booking->type)?$booking->type:0;?>'},
                        dataType:'json',
                        beforeSend:function(){
                        // $('#btn-1').attr('disabled',true);
                        },
                        success:function(data){
                        //$('#btn-1').removeAttr('disabled');
                        if(data.data){
                        $('#Message-Box').prepend(data.data);
                        //$('#text-message').val('');
                        }
                        if(data.book_status==1){
                        $('#edit_event').attr('disabled','disabled');
                        $('#myModal2').remove();
                        }
                        },
                        error: function(data){
                        //$('#btn-1').removeAttr('disabled');
                        //alert('Something went wrong!');
                        }
                        });
                        }
                        function Send_MSG(form){
                        var message = form.message.value;
                        $.ajax({
                        type:'POST',
                        url: '<?php echo site_url($class.'/send_message');?>',
                        data:{
                        message:message,
                        to:'<?php echo isset($to_id)?$to_id:0;?>',book_id:'<?php echo isset($booking->id)?$booking->id:0;?>'
                        },
                        dataType:'json',
                        beforeSend:function(){
                        $('#btn-1').attr('disabled',true);
                        },
                        success:function(data){
                        $('#btn-1').removeAttr('disabled');
                        if(data.data){
                        $('#Message-Box').prepend(data.data);
                        
                        }
                        $('#text-message').val('');
                        },
                        error: function(data){
                        $('#btn-1').removeAttr('disabled');
                        // alert('Something went wrong!');
                        }
                        });
                        }
                        $(document).ready(function () {
                        $('.pickadate').pickadate({
                        formatSubmit: 'yyyy/mm/dd',
                        format: 'yyyy-mm-dd',
                        today: '',
                        close: '',
                        clear: '',
                        min: [<?php echo date('Y');?>,<?php echo date('m');?>,<?php echo date('d',strtotime('+1 day',time()));?>]
                        });
                        $('.pickatime').pickatime();
                        myInterval = setInterval(Update_Message_Box, interval_delay);
                        $(window).focus(function () {
                        clearInterval(myInterval); // Clearing interval if for some reason it has not been cleared yet
                        if  (!is_interval_running) //Optional
                        myInterval = setInterval(Update_Message_Box, interval_delay);
                        }).blur(function () {
                        clearInterval(myInterval); // Clearing interval on window blur
                        is_interval_running = false; //Optional
                        });
                        //setInterval(Update_Message_Box,2000);
                        $('#btn-1').on('click', function () {
                        $("form#form-1").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                        email: {
                        required: true,
                        }                        },
                        highlight: function (element) {
                        $(element)
                        .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                        label.closest('.form-group').removeClass('has-error');
                        label.remove();
                        },
                        invalidHandler: function (form, validator) {
                        if (!validator.numberOfInvalids())
                        return;
                        /*$('html, body').animate({
                        scrollTop: $(validator.errorList[0].element).parent().offset().top
                        }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                        if (element.closest('.i-checks').size() === 1) {
                        error.insertAfter(element.closest('.i-checks'));
                        } else {
                        error.insertAfter(element);
                        }
                        if (element.closest('.i-select').size() === 1) {
                        error.insertAfter(element.closest('.i-select'));
                        } else {
                        error.insertAfter(element);
                        }
                        if (element.closest('.custom_select_box').size() === 1) {
                        error.insertAfter(element.closest('.custom_select_box'));
                        } else {
                        error.insertAfter(element);
                        }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                        Send_MSG(form);
                        }
                        });
                        });
                        $('#btn-modal').on('click', function () {
                        $("form#form-modal").validate({
                        debug: true,
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                        amount:{
                        number:true
                        }
                        },
                        highlight: function (element) {
                        $(element)
                        .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                        label.closest('.form-group').removeClass('has-error');
                        label.remove();
                        },
                        invalidHandler: function (form, validator) {
                        if (!validator.numberOfInvalids())
                        return;
                        /*$('html, body').animate({
                        scrollTop: $(validator.errorList[0].element).parent().offset().top
                        }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                        if (element.closest('.i-checks').size() === 1) {
                        error.insertAfter(element.closest('.i-checks'));
                        } else {
                        error.insertAfter(element);
                        }
                        if (element.closest('.i-select').size() === 1) {
                        error.insertAfter(element.closest('.i-select'));
                        } else {
                        error.insertAfter(element);
                        }
                        if (element.closest('.custom_select_box').size() === 1) {
                        error.insertAfter(element.closest('.custom_select_box'));
                        } else {
                        error.insertAfter(element);
                        }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                        //Send_MSG(form);
                        // form.submit();
                        var dt = form.date.value;
                        var tm = form.time.value;
                        var hrs = form.hours.value;
                        var amount = form.amount.value;
                        $.ajax({
                        type:'POST',
                        url: '<?php echo site_url($class.'/update_event');?>',
                        data:{date:dt,time:tm,hours:hrs,amount:amount,id:'<?php echo $booking->id;?>',to:'<?php echo isset($to_id)?$to_id:0;?>'},
                        dataType:'json',
                        beforeSend:function(){
                        // $('#btn-1').attr('disabled',true);
                        },
                        success:function(data){
                        //$('#btn-1').removeAttr('disabled');
                        if(data.data){
                        $('#myModal2 .modal-body .alert').remove();
                        $('#myModal2 .modal-body').prepend('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success! </strong>Event details has been updated successfully.</div>');
                        $('#Message-Box').prepend(data.data);
                        setTimeout(function(){
                        $('#myModal2').modal('hide');
                        },1000);
                        }
                        },
                        error: function(data){
                        //$('#btn-1').removeAttr('disabled');
                        //alert('Something went wrong!');
                        }
                        });
                        }
                        });
                        });
                        $('#myModal2').on('show.bs.modal',function(){
                        $('#myModal2').find('.alert').remove();
                        });
                        });
                        
                        $("#agreement").on('click',function(){
                        //var exp =  $("#exp").val();
                        var dt = "<?php echo date('D,M d, Y',strtotime($booking->event_date))?>";
                        
                        var tm = "<?php echo date('h:i A',strtotime($booking->name));;?> - <?php echo date('h:i A',strtotime("+$minutes minutes",strtotime($booking->name)));;?> <?php echo date('h:i A',strtotime("+$minutes minutes",strtotime($booking->name)));;?> (<?php echo $booking->hours;?> hour)";
                        var loc = "<?php echo $booking->location?>";
                        var amount = "<?php echo $booking->amount?> ,NOK";
                        // alert(dt+tm+loc+amount);
                        $.ajax({
                        type:'POST',
                        url: '<?php echo site_url($class.'/agreement');?>',
                        data:{date:dt,time:tm,loc:loc,amount:amount,id:'<?php echo $booking->id;?>',to:'<?php echo isset($to_id)?$to_id:0;?>'},
                        dataType:'json',
                        success:function(data){
                        if(data.data){
                        $('#Message-Box').prepend(data.data);
                        setTimeout(function(){
                        $('#myModal_accept').modal('hide');
                        },1000);
                        window.location = "<?php echo site_url($class.'/chat/'.$booking->id)?>";
                        }
                        },
                        error: function(data){
                        //$('#btn-1').removeAttr('disabled');
                        //alert('Something went wrong!');
                        }
                        });
                        
                        });
                        $('#accept_proposal').on('click', function () {
                        $("form#transaction_form").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                        
                        },
                        highlight: function (element) {
                        $(element)
                        .closest('.form-inline').addClass('has-error');
                        },
                        success: function (label) {
                        label.closest('.form-inline').removeClass('has-error');
                        label.remove();
                        },
                        
                        submitHandler: function (form) {
                        
                        var credit_card =  $("#credit_card").val();
                        var exp =  $("#exp").val();
                        var ccv =  $("#ccv").val();
                        
                        $.ajax({
                        type:'POST',
                        url: '<?php echo site_url($class.'/transaction');?>',
                        data:{id:'<?php echo $booking->id;?>',credit_card:credit_card,exp:exp,ccv:ccv},
                        dataType:'json',
                        beforeSend:function(){
                        // $('#btn-1').attr('disabled',true);
                        },
                        success:function(data){
                        //$('#btn-1').removeAttr('disabled');
                        if(data.data){
                        // $('#Message-Box').prepend(data.data);
                        $('form#transaction_form')[0].reset();
                        
                        $('#myModal_accept').modal('hide');
                        
                        window.location = "<?php echo site_url($class.'/pdf_generation/'.$booking->id)?>";
                        //$('#btn-2').remove();
                        
                        }
                        },
                        error: function(data){
                        //$('#btn-1').removeAttr('disabled');
                        //alert('Something went wrong!');
                        }
                        });
                        }
                        });
                        });
                        /*$("#post_payment").click(function(){
                        });*/
                        $('#accept_proposal1').on('click', function () {

                        $("form#transaction_form1").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                        
                        },
                        highlight: function (element) {
                        $(element)
                        .closest('.form-inline').addClass('has-error');
                        },
                        success: function (label) {
                        label.closest('.form-inline').removeClass('has-error');
                        label.remove();
                        },
                        
                        submitHandler: function (form) {
                        
                        var credit_card =  $("#credit_card1").val();
                        var exp =  $("#exp1").val();
                        var ccv =  $("#ccv1").val();
                        
                        $.ajax({
                        type:'POST',
                        url: '<?php echo site_url($class.'/transaction');?>',
                        data:{id:'<?php echo $booking->id;?>',credit_card:credit_card,exp:exp,ccv:ccv},
                        dataType:'json',
                        beforeSend:function(){
                        // $('#btn-1').attr('disabled',true);
                        },
                        success:function(data){
                        //$('#btn-1').removeAttr('disabled');
                        if(data.data){
                        // $('#Message-Box').prepend(data.data);
                        $('form#transaction_form1')[0].reset();
                        
                        $('#myModal_payment').modal('hide');
                        
                        window.location = "<?php echo site_url($class.'/pdf_generation1/'.$booking->id)?>";
                        //$('#btn-2').remove();
                        
                        }
                        },
                        error: function(data){
                        //$('#btn-1').removeAttr('disabled');
                        //alert('Something went wrong!');
                        }
                        });
                        }
                        });
                        });
                        
                        </script>
                        <style>
                        .search-btn{
                        border-radius: 5px;
                        margin-top: 29px;
                        padding: 10px 60px;
                        }
                        </style>
                    </body>
                </html>