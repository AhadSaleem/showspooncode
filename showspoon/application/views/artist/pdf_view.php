<!DOCTYPE html>
<html lang="en">
    <head>
        
        <!--<script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/legacy.js"></script>-->
        <style>
        p,li{
        font-size: 14px;
        font-family: Times New Roman;
        margin-top: 10px;
        }
        
        
        </style>
    </head>
    <body>
        <span class="yellow"><?php //echo $row['name'];?></span>
        <center><h3>BAND PERFORMANCE CONTRACT</h3>
        <hr style="width: 320px; height:0px; margin:0 auto; margin-top: 5px;">
        </center>
        <?php foreach ($artist as $artist) {
        
        $name = $artist['name'];
        } ?>
        <?php foreach ($booking as $row) {
        $from_id = $row['from_id'];
        $to_id = $row['to_id'];
        $venue_name =  $row['name'];
        $location =  $row['location'];
        $time =  $row['time'];
        $amount =  $row['amount'];
        $event_date =  $row['event_date'];
        $minutes = $row['hours']*60;
        $amount = $row['amount'];
        $type = $row['type'];
        } ?>
        <p style="margin-left: 25px;">  This contract (the "Agreement") is made on this day between <?php echo $venue_name; ?> (the "Venue") and <?php echo $name; ?> (the "Band") for the hiring of Band to perform (the "Show") at (the "Venue").</p>
        <p style="margin-left: 25px;">It is agreed as follows:</p>
        <ol>
            <li><b>Place, date, and time of Show.</b>The parties agree that the time and place of Show will be at the Venue, located at <?php echo $location ?>, on the <?php echo date('l',strtotime($event_date))  ?> day of <?php echo date('M d, Y',strtotime($event_date)) ?> at <?php echo $time; ?> </li>
            <li><b>Description of Show.</b>Show will be a musical performance with musical content decided by the Band. The Show will last of a minimum of <?php echo $minutes ?> minutes.</li>
            <li><b>Payment.</b>Compensation for the Show will be <?php echo $amount; ?> NOK, payable by <?php echo $venue_name; ?>/<?php echo $name; ?>. A 50% deposit of Fee is due on the signing of this contract. This is a required condition for the contract to proceed; if a 50% deposit of Fee is not tendered upon the signing of this contract, no further obligation for either party comes due. The remaining 50% of Fee is due immediately prior to the Band's Show, but may be made earlier. Transaction costs will be added to the compensation and is non-refundable.</li>
            <li><b>Cancellation</b>.The Venue is obliged to pay full contracted fee if the Venue cancels the Show.
                The Band is obliged to show a medical certificate if the Band cancels the Show due to illness.
            If the Show is cancelled due to force majeure (for instance: fire, strike, riot, crime, war, acts of God etc) neither party shall be entitled to make any claim against the other party for non-fulfillment of any outstanding obligations.</li>
            <li><b>Sound Systems Check.</b>A sound check conducted by the Band of Venue's sound system is required, at a time to be mutually arranged between the Band and the Venue.</li>
            <li><b>Security, Health, and Safety.</b>The Venue warrants that the location will be of sufficient size to safely conduct the Show, is of stable construction and sufficiently protected from weather, and that there will be adequate security and/or emergency medical responders available if foreseeably necessary. </li>
            <li><b>Indemnification.</b>The venue indemnifies and holds the Band harmless for any claims of property damage or bodily injury caused by the Show’s attendees.</li>
            <li><b>Arbitration settles disputes.</b>The parties shall endeavor to resolve all disputes regarding this Agreement amicably. However, in case of a disagreement the litigation will be resolved according to Norwegian law and jurisdiction. Showspoon assumes no responsibility for the content of this Agreement or any disputes that should arise.</li>
            <li><b>Severability.</b>If any portion of Agreement is in conflict with any applicable law, such portion will become inoperative, but all other portions of Agreement will remain in force.</li>
            <li><b>Copyright.</b>No performance on the engagement shall be recorded, reproduced or transmitted without permission from the Band. </li>
        </ol>
        <br><br>
        <div style="background: #f1f1f1; margin: 0 20px 0 40px ;">
            <div class="text">
                <h3 style="text-align: center; margin-top: 5px;">Request Received</h3>
                <p style="margin-left: 20px; font-size: 14px; font-weight: bold;"><?php echo $name;?></p> <br>
               <p style="margin-left: 20px; font-size: 14px;"><?php echo $venue_name;?> - <?php echo date('l',strtotime($event_date));?> day of <?php echo date('M d, Y',strtotime($event_date)) ?> at <?php echo $time; ?> <?php echo $location; ?><br><br><?php echo number_format($amount); ?> NOK</p>  
                
            </div>
        </div>
        <br><br><br><br>
        <br>
        
        <p style="margin-left: 38px; font-weight: bold; font-size: 16px;">Message history</p>
        <ul id="Message-Box" class="messages_thread" style=" list-style: none;">
            <?php foreach ($chat as $chat) {
            //dd($booking);
            $id =  $chat['id'];
            $from =  $chat['from'];
            $to =  $chat['to'];
            $message =  $chat['message'];
            $message_date =  $chat['sent'];
            
            ?>
            <li class="message left appeared">
                <div class="text_wrapper">
                    <?php
                    if($from == $from_id){
                    ?>
                    <span style="font-size:14px; font-weight: bold;"><?php echo $name; ?> :</span>
                    <span><?php echo ucfirst($message); ?></span>
                    <?php
                    }
                    else{
                    ?>
                    <span style="font-size:14px; font-weight: bold;"><?php echo $venue_name; ?> :</span>
                    <span><?php echo ucfirst($message); ?></span>
                    <?php }?>
                    
                </div>
                <time datetime="2009-11-13T20:00">
                <span style="font-size: 13px">Date : <?php echo date('M d, Y',strtotime($message_date)) ?></span></time>
                <br><br>
                <p style="border-bottom: 1px dotted #ccc;"></p>
            </li>
            <?php } ?>
            
        </ul>
        
    </body>
</html>