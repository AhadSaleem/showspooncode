<!DOCTYPE html>
<html lang="en">
    <head>

        <?php $this->load->view('includes/head');?>
        <link href="<?php echo base_url();?>/assets/dist/plugins/iCheck/all.css" rel="stylesheet">
        <script src="<?php echo base_url();?>/assets/dist/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/assets/dist/plugins/select2/select2.min.js"></script>
        <!--<script src="<?php echo base_url();?>/assets/dist/js/bootstrap_multiselect.js"></script>-->

        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.time.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/picker.date.js"></script>

        <!--<script type="text/javascript" src="<?php echo base_url();?>/assets/dist/plugins/pickers/pickadate/legacy.js"></script>-->
    </head>
    <body>

        <?php $this->load->view('includes/header');?>




        <div class="container">
			<div class="row m-b-md">
				<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-center">
					<?php
                    $user_image=site_url('assets/dist/images/noimage2.jpg');
                    if(isset($venue->profile_image) && $venue->profile_image!=''){
                        $user_image=site_url('uploads/users/thumb/'.$venue->profile_image);
                    }
                    ?>
                    <div class="media MiddleAvatar">
                        <div class="media-left">
                            <img src="<?php echo $user_image;?>" class="media-object middleimg" />
                        </div>
                    </div><!-- media -->
					<h3 class="text-center">Book <?php echo isset($venue->name)?$venue->name:'';?></h3>

					<ul class="progresssteps m-t-md m-b-md">
						<li class=""> </li>
						<li class=""> </li>
						<li class="active"> </li>
					</ul> 

					<h4 class="l-h">Thank You. Your Request has been Successfully Submitted</h4>
					<p class="text-center text-muted">You can see the status of your request<br />by clicking the link below</p>

					<a href="<?php echo site_url('venueBooking/my_requests');?>" class="btn btn-primary btn-green btn-lg m-t-xxl">View My Request</a>

				</div>
			</div><!-- row -->
		</div><!-- container -->

<?php $this->load->view('includes/footer');?>

        <script src="<?php echo base_url(); ?>assets/dist/plugins/iCheck/icheck.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
        <script>
            function backPage(form_hide,form_show){
                $('#'+form_hide).hide();
                $('#'+form_show).show();
            }
            $(document).ready(function () {


                $('.pickadate').pickadate({
                    formatSubmit: 'yyyy/mm/dd',
                    format: 'yyyy-mm-dd',
                    today: '',
                    close: '',
                    clear: '',
                    min: [<?php echo date('Y');?>,<?php echo date('m');?>,<?php echo date('d',strtotime('+1 day',time()));?>]
                });
                $('.pickatime').pickatime();
                $('#btn-1').on('click', function () {
                    $("form#form-1").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            email: {
                                required: true,
                            }                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            //form.submit();
                            $('#form-1').hide();
                            $('#form-2').show();
                            $('input').iCheck({
                                checkboxClass: 'icheckbox_square',
                                radioClass: 'iradio_square',
                                increaseArea: '20%' // optional
                            });
                        }
                    });
                });

                $('#btn-2').on('click', function () {
                    $("form#form-2").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            amount: {
                                required: true,
                                number:true
                            }
                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            //form.submit();
                            $('#form-2').hide();
                            $('#form-3').show();
                        }
                    });
                });

                $('#btn-3').on('click', function () {
                    $("form#form-3").validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        ignore: ":hidden:not(select)",
                        rules: {
                            amount: {
                                required: true,
                                number:true
                            }
                        },
                        highlight: function (element) {
                            $(element)
                                .closest('.form-group').addClass('has-error');
                        },
                        success: function (label) {
                            label.closest('.form-group').removeClass('has-error');
                            label.remove();
                        },
                        invalidHandler: function (form, validator) {
                            if (!validator.numberOfInvalids())
                                return;
                            /*$('html, body').animate({
                             scrollTop: $(validator.errorList[0].element).parent().offset().top
                             }, 0);*/
                        },
                        errorPlacement: function (error, element) {
                            if (element.closest('.i-checks').size() === 1) {
                                error.insertAfter(element.closest('.i-checks'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.i-select').size() === 1) {
                                error.insertAfter(element.closest('.i-select'));
                            } else {
                                error.insertAfter(element);
                            }
                            if (element.closest('.custom_select_box').size() === 1) {
                                error.insertAfter(element.closest('.custom_select_box'));
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        messages: {
                        },
                        submitHandler: function (form) {
                            //form.submit();
                            //$('#form-2').hide();
                            //$('#form-3').show();
                            var booking_name = $('#name').val();
                            var booking_date = $('#date').val();
                            var booking_time = $('#time').val();
                            var booking_hours = $('#hours').val();
                            var booking_description = $('#description').val();
                            var booking_amount = $('#amount').val();
                            var booking_pay = $('.who_pay:checked').val();
                            
                            
        
        $('#hidden_name').val(booking_name);
        $('#hidden_date').val(booking_date);
        $('#hidden_time').val(booking_time);
        $('#hidden_hours').val(booking_hours);
        $('#hidden_amount').val(booking_amount);
        $('#hidden_who_pay').val(booking_pay);
        $('#hidden_venue').val(<?php echo isset($venue->user_id)?$venue->user_id:0;?>);
                            $('#hidden_form').submit();
                            
                            
                        }
                    });
                });
            });
        </script>
        <style>
            .search-btn{
                border-radius: 5px;
                margin-top: 29px;
                padding: 10px 60px;
            }
        </style>
    </body>
</html>