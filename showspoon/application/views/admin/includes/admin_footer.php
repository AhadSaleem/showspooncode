
	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/plugins/pickers/daterangepicker.js"></script>

	<script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/core/app.js"></script>
	<!-- <script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/pages/dashboard.js"></script> -->
	<script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url(); ?>admin_assets/js/pages/datatables_basic.js"></script>
	<!-- /theme JS files -->